ALTER TABLE `categorias` ADD `title` VARCHAR(255)  NULL  DEFAULT ''  AFTER `slug_id`;
ALTER TABLE `categorias` ADD `meta_title` VARCHAR(255)  NULL  DEFAULT ''  AFTER `title`;
ALTER TABLE `produtos` ADD `title` VARCHAR(255)  NULL  DEFAULT ''  AFTER `slug_id`;
ALTER TABLE `produtos` ADD `meta_title` VARCHAR(255)  NULL  DEFAULT ''  AFTER `title`;
ALTER TABLE `paginas` ADD `title` VARCHAR(255)  NULL  DEFAULT ''  AFTER `slug_id`;
ALTER TABLE `paginas` ADD `meta_title` VARCHAR(255)  NULL  DEFAULT ''  AFTER `title`;
ALTER TABLE `paginas` ADD `meta_description` TEXT  NULL  AFTER `meta_title`;
