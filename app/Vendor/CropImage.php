<?php

class cropImage {

	private $imgSrc;
	private $myImage;
	private $thumb;
	private $width;
	private $height;

	public function setImage($image) {

		//Your Image
		$this->imgSrc = $image;

		//getting the image dimensions
		list($this->width, $this->height) = getimagesize($this->imgSrc);

		//create image from the jpeg
		$this->myImage = imagecreatefromjpeg($this->imgSrc) or die("Error: Imagem não encontrada!");

	}

	public function createThumb($destino, $w, $h) {

		if ($w == $h) {

			if($this->width > $this->height)
				$smallSide = $this->height; //find biggest length
			else
				$smallSide = $this->width;

			//The crop size will be half that of the largest side
			$cropWidth   = $smallSide;
			$cropHeight  = $smallSide;

		} else {
			$cropWidth   = $this->width;
			$cropHeight  = $this->height;
		}

		//getting the top left coordinate
		$x = ($this->width - $cropWidth) / 2;
		$y = ($this->height - $cropHeight) / 2;

	  	$thumbSizeW = $w;
	  	$thumbSizeH = $h;
	  	$this->thumb = imagecreatetruecolor($thumbSizeW, $thumbSizeH);

	  	imagecopyresampled($this->thumb, $this->myImage, 0, 0, $x, $y, $thumbSizeW, $thumbSizeH, $cropWidth, $cropHeight);
		$imagereturn = imagejpeg($this->thumb, $destino, 100);
	}

	public function renderImage() {
	   header('Content-type: image/jpeg');
	   imagejpeg($this->thumb);
	   imagedestroy($this->thumb);
	}
}

?>