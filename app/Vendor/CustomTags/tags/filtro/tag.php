<?php
	// Funções auxiliares. Devem, necessariamente, ficar antes da função principal.
	function __tipoo_filtro_get_query_array($view = null) {
		$query_string = parse_url($_SERVER['QUERY_STRING']);
		$query_string = $query_string['path'];

		if (!empty($query_string)) {
			$query_string = urldecode($query_string);
			parse_str($query_string, $query_array);
			return $query_array;
		}

		return false;
	}

	function __tipoo_filtro_existe($filtro, $view = null) {
		$query_array = __tipoo_filtro_get_query_array($view);

		if (isset($query_array['filtro'])) {
			$filtros = explode(';', $query_array['filtro']);

			return in_array($filtro, $filtros);
		}

		return false;
	}

	function __tipoo_filtro_adicionar($filtro, $view) {
		$query_array = __tipoo_filtro_get_query_array($view);

		if (!__tipoo_filtro_existe($filtro, $view)) {
			if (isset($query_array['filtro'])) {
				$filtros = explode(';', $query_array['filtro']);
				$filtros[] = $filtro;
				$filtros = implode(';', $filtros);
				$query_array['filtro'] = $filtros;
			} else {
				$query_array['filtro'] = $filtro;
			}
		}

		return $query_array;
	}

	function __tipoo_filtro_remover($filtro, $view) {
		$query_array = __tipoo_filtro_get_query_array($view);

		if (__tipoo_filtro_existe($filtro, $view)) {
			if (isset($query_array['filtro'])) {
				$filtros = explode(';', $query_array['filtro']);
				if(($key = array_search($filtro, $filtros)) !== false) {
					unset($filtros[$key]);
				}

				if (!count($filtros)) {
					unset($query_array['filtro']);
				} else {
					$filtros = implode(';', $filtros);
					$query_array['filtro'] = $filtros;
				}
			} else {
				$query_array['filtro'] = $filtro;
			}
		}

		return $query_array;
	}

	function __tipoo_filtro_montar_query_string($query_array, $view) {
		// Verificando se já tem busca_id, se não tiver, adiciona
		if (!array_key_exists('busca_id', $query_array)) {
			$buscaId = $view->getVar('controleProdutosBuscaId');
			$query_array['busca_id'] = $buscaId;
		}

		$query_string = '';
		foreach ($query_array as $key => $value) {
			// Caso esteja filtrando, a página deve ser removida
			if ($key != 'pagina') {
				$query_string .= $key.'='.$value.'&';
			}
		}

		$query_string = rtrim($query_string, '&');
		return $query_string;
	}

	function __tipoo_filtro_caracteristica($caracteristica_id, $filtros_caracteristicas, $attr, $view) {
		foreach ($filtros_caracteristicas as $key => $value) {
			if ($key == $caracteristica_id) {
				$caracteristica = $value;
			}
		}

		$html = '';
		if (isset($caracteristica)) {
			$html .= '<div class="'.$attr['class'].' caracteristica-id-'.$caracteristica['id'].'">';
			$html .= '<h5>'.$caracteristica['descricao'].'</h5>';
			$html .= '<ul>';

			foreach ($caracteristica['caracteristicas_valores'] as $key => $caracteristicas_valor) {

				$link = $view->Html->url(null, true);

				if (__tipoo_filtro_existe('c,'.strtolower($caracteristica['referente']).','.$caracteristica['id'].','.$caracteristicas_valor['id'], $view)) {
					$filtro_query_string = __tipoo_filtro_remover('c,'.strtolower($caracteristica['referente']).','.$caracteristica['id'].','.$caracteristicas_valor['id'], $view);
					$link .= '?' . __tipoo_filtro_montar_query_string($filtro_query_string, $view);
					$class = ' class="selecionado caracteristica-valor-id-'.$caracteristicas_valor['id'].'"';
				} else {
					$filtro_query_string = __tipoo_filtro_adicionar('c,'.strtolower($caracteristica['referente']).','.$caracteristica['id'].','.$caracteristicas_valor['id'], $view);
					$link .= '?' . __tipoo_filtro_montar_query_string($filtro_query_string, $view);
					$class = ' class="caracteristica-valor-id-'.$caracteristicas_valor['id'].'"';
				}
				$link = rtrim($link, '?');

				$html .= '<li'.$class.'>';
				$html .= $view->Html->link($caracteristicas_valor['descricao'].' <span>('.$caracteristicas_valor['qtd'].')</span>', Router::url($link), array('escape' => false));
				$html .= '</li>';
			}

			$html .= '</ul>';
			$html .= '</div>';
		}

		return $html;
	}

	function tipoo_filtro($tag) {
		$view = $tag['view_context'];
		$attr = get_object_vars($tag['attributes']);

		$padrao = array(
			'tipo' => 'caracteristica', // caracteristica, marca, preco
			'class' => 'controles-filtros'
		);

		$attr = array_merge($padrao, $attr);

		$filtros_caracteristicas = $view->getVar('controlesFiltrosCaracteristicas');
		$filtros_marcas = $view->getVar('controlesFiltrosMarcas');
		$filtros_precos = $view->getVar('controlesFiltrosPrecos');

		if ($attr['tipo'] == 'caracteristica') {
			if (isset($attr['id'])) {
				return __tipoo_filtro_caracteristica($attr['id'], $filtros_caracteristicas, $attr, $view);

			} else {
				$html = '';

				foreach ($filtros_caracteristicas as $id => $value) {
					$html .= __tipoo_filtro_caracteristica($id, $filtros_caracteristicas, $attr, $view);
				}

				return $html;
			}
		} else if ($attr['tipo'] == 'marca') {
			$html = '<div class="'.$attr['class'].' marca">';
			$html .= '<h5>Marca</h5>';
			$html .= '<ul>';

			foreach ($filtros_marcas as $marca) {
				$link = $view->Html->url(null, true);

				if (__tipoo_filtro_existe('m,'.$marca['id'], $view)) {
					$filtro_query_string = __tipoo_filtro_remover('m,'.$marca['id'], $view);
					$link .= '?' . __tipoo_filtro_montar_query_string($filtro_query_string, $view);
					$class = ' class="selecionado"';
				} else {
					$filtro_query_string = __tipoo_filtro_adicionar('m,'.$marca['id'], $view);
					$link .= '?' . __tipoo_filtro_montar_query_string($filtro_query_string, $view);
					$class = '';
				}
				$link = rtrim($link, '?');

				$html .= '<li'.$class.'>';
				$html .= $view->Html->link($marca['descricao'].' <span>('.$marca['qtd'].')</span>', Router::url($link), array('escape' => false));
				$html .= '</li>';
			}

			$html .= '</ul>';
			$html .= '</div>';

			return $html;

		} else if ($attr['tipo'] == 'preco') {
			$html = '<div class="'.$attr['class'].' preco">';
			$html .= '<h5>Preço</h5>';

			$html .= '<input type="hidden" name="filtro_valor_min" value="' . $filtros_precos['min'] . '">';
			$html .= '<input type="hidden" name="filtro_valor_max" value="' . $filtros_precos['max'] . '">';

			if (count($filtros_precos)) {
				$min_original = number_format($filtros_precos['faixas'][0]['min_original'], 2, '.', '');
				$max_original = number_format($filtros_precos['faixas'][0]['max_original'], 2, '.', '');
				$html .= '<input type="hidden" name="filtro_valor_original_min" value="' . $min_original . '">';
				$html .= '<input type="hidden" name="filtro_valor_original_max" value="' . $max_original . '">';
			}

			$html .= '<ul>';

			foreach ($filtros_precos['faixas'] as $key => $faixa) {
				$link = $view->Html->url(null, true);

				if ($key == 0 && $faixa['de'] == 0) {
					$texto = 'Até '.number_format($faixa['ate'], 2, ',', '.');
				} else if ($key == count($filtros_precos['faixas']) - 1 && $faixa['ate'] == $faixa['max_original']) {
					$texto = 'Acima '.number_format($faixa['de'], 2, ',', '.');
				} else {
					$texto = 'De '.number_format($faixa['de'], 2, ',', '.').' a '.number_format($faixa['ate'], 2, ',', '.');
				}

				$texto .= ' <span>('.$faixa['qtd'].')</span>';

				$de = number_format($faixa['de'], 2, '.', '');
				$ate = number_format($faixa['ate'], 2, '.', '');
				$min_original = number_format($faixa['min_original'], 2, '.', '');
				$max_original = number_format($faixa['max_original'], 2, '.', '');

				if (__tipoo_filtro_existe('p,'.$de.','.$ate.','.$min_original.','.$max_original, $view)) {
					$filtro_query_string = __tipoo_filtro_remover('p,'.$de.','.$ate.','.$min_original.','.$max_original, $view);
					$link .= '?' . __tipoo_filtro_montar_query_string($filtro_query_string, $view);
					$class = ' class="selecionado"';
				} else {
					$filtro_query_string = __tipoo_filtro_adicionar('p,'.$de.','.$ate.','.$min_original.','.$max_original, $view);
					$link .= '?' . __tipoo_filtro_montar_query_string($filtro_query_string, $view);
					$class = '';
				}
				$link = rtrim($link, '?');

				if ($faixa['qtd']) {
					$html .= '<li'.$class.'>';
					$html .= $view->Html->link($texto, Router::url($link), array('escape' => false));
					$html .= '</li>';
				} else {
					$html .= '<li class="desabilitado">';
					$html .= $texto;
					$html .= '</li>';
				}

			}

			$html .= '</ul>';
			$html .= '</div>';

			return $html;
		} else {
			return 'ERRO: A tag "filtro" deve conter o atributo "tipo" contendo um dos seguintes valores: "caracteristica", "marca", "preco".';
		}
	}
?>