<?php
	function tipoo_produtoFrete($tag) {
		$view = $tag['view_context'];
		$attr = get_object_vars($tag['attributes']);

		$padrao = array(
			'titulo' => 'Simular o valor do frete'
		);

		$attr = array_merge($padrao, $attr);

		$produto = $view->getVar('controleProduto');
		$cep = $view->getVar('cep');
		$frete_transportadoras = $view->getVar('frete_transportadoras');

		$view->Html->script(array(
			'Plugins/jquery.maskedinput.min',
			'Controles/tipoo.tags.frete.js'
		), array('inline' => false));

		$html = '<div class="frete">';
		$html .= 	'<div class="calcular-frete">';
		$html .=		'<h4>' . $attr['titulo'] . '</h4>';

		$html .=		$view->Form->create('Frete', array('class' => 't-calcular-frete-form'));
		$html .=		$view->Form->input('calc_valor_frete', array('type' => 'text', 'class' => 't-cep', 'label' => false, 'value' => $cep));
		$html .=		$view->Form->end('Calcular');

		$html .=		'<a href="http://www.buscacep.correios.com.br/" target="_blank">Não sei meu CEP</a>';
		$html .= 	'</div>';
		$html .= '</div>';

		// Transportadoras
		$html .= '<div class="t-frete-transportadoras">';
		$html .= 	$view->element('Catalogo/simular_frete_transportadoras');
		$html .= '</div>';

		return $html;
	}
?>