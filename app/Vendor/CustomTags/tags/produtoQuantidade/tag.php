<?php
	function tipoo_produtoQuantidade($tag) {
		$view = $tag['view_context'];
		$attr = get_object_vars($tag['attributes']);

		$padrao = array(
			'loadJs' => 'true'
		);

		$attr = array_merge($padrao, $attr);

		if ($attr['loadJs'] == 'true') {
			$view->Html->script(array('Plugins/jquery.numeric.js'), array('inline' => false));
		}

		$html = $view->Html->scriptBlock('
		 		if (typeof tipoo.data.produto === "undefined") {
		 			tipoo.data.produto = new Object();
		 		}
		 		tipoo.data.produto.quantidade = 1;
		 	', array('inline' => false));

		$html .= '<div class="t-quantidade">';
		$html .= $view->Form->input('SkuQuantidade', array('type' => 'text', 'value' => 1, 'class' => 't-sku-quantidade'));
		$html .= '<div class="t-quantidade-botoes">';
		$html .= '<span class="t-quantidade-adicionar" data-operador="+">+</span>';
		$html .= '<span class="t-quantidade-remover" data-operador="-">-</span>';
		$html .= '</div>';
		$html .= '</div>';

		return $html;
	}
?>