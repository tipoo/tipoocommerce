<?php

function __tipoo_data_carrinho($view) {
	$skus = $view->getVar('skus');

	$html = '';
	$html .= 'tipoo.data.carrinho = new Object();';

	if (count($skus) > 0) {
		$skus_em_pre_venda = $view->Carrinho->skus_em_pre_venda();
		$previsao_entrega_pre_venda = $view->Carrinho->data_entrega_pre_venda();

		$html .= 'tipoo.data.carrinho.skus = ' . json_encode($skus) . ';';
		$html .= 'tipoo.data.carrinho.pre_venda = new Object();';
		if ($skus_em_pre_venda) {
			$html .= 'tipoo.data.carrinho.pre_venda.skus = ' . json_encode($skus_em_pre_venda) . ';';
			$html .= 'tipoo.data.carrinho.pre_venda.previsao_entrega = ' . json_encode($view->Formatacao->data($previsao_entrega_pre_venda)) . ';';
		} else {
			$html .= 'tipoo.data.carrinho.pre_venda = false;';
		}
	} else {
		$html .= 'tipoo.data.carrinho = "Vazio"';
	}

	return $html;
}

function tipoo_data($tag) {

	$view = $tag['view_context'];

	$html = $view->Html->scriptBlock(
 		__tipoo_data_carrinho($view)
	);

	return $html;
}
?>