<?php
	function tipoo_produtoModelos($tag) {
		$view = $tag['view_context'];
		$attr = get_object_vars($tag['attributes']);

		$padrao = array(
			'tipo' => 'lista', // lista, select
			'tamanhoImagem' => 'thumb',
			'titulo' => 'Selecione um modelo',
			'mensagemAlerta' => 'Selecione um modelo.'
		);

		$attr = array_merge($padrao, $attr);

		$produto = $view->getVar('controleProduto');
		$caracteristicas_skus = $view->getVar('controleCaracteristicasSkus');

		$count_skus = count($produto['Sku']);

		$class_hide = '';
		if ($count_skus == 1) {
			$class_hide = ' hide'; // deixar o espaço ali para não dar problema ao concatenar com as outras classes
		}

		$html = '<input type="hidden" class="t-sku-id" value="">';

		$html .= $view->Html->scriptBlock('
		 		if (typeof tipoo.data.produto === "undefined") {
		 			tipoo.data.produto = new Object();
		 		}
		 		tipoo.data.produto.id = ' . $produto['Produto']['id'] . ';
		 	', array('inline' => false));

		$html .= '<div class="t-skus' . $class_hide . '">';
		$html .= '<h3>' . $attr['titulo'] . '</h3>';

		if ($caracteristicas_skus) {

			$html .= $view->Html->scriptBlock('
		 		tipoo.data.skus = new Object();
		 		tipoo.data.skus = ' . json_encode($caracteristicas_skus['json']) . ';
		 	', array('inline' => false));

			// Tira do array a chave "json" para usar a variável no foreach
			unset($caracteristicas_skus['json']);

			foreach ($caracteristicas_skus as $caracteristica) {

				$html .= '<div class="t-skus-caracteristicas ' . strtolower(Inflector::slug($caracteristica['Caracteristica']['descricao'], '-')) . '">';
				$html .= '<h4>' . $caracteristica['Caracteristica']['descricao'] . '</h4>';
				$html .= '<ul class="t-skus-caracteristicas-selecao" data-tipo="caracteristica">';

				foreach ($caracteristica['CaracteristicasValor'] as $caracteristica_valor) {

					$html .= '<li class="' . strtolower(Inflector::slug($caracteristica['Caracteristica']['descricao'], '-')) . ' ' . strtolower(Inflector::slug($caracteristica_valor['descricao'], '-')) . '">';

					if ($count_skus > 1) {
						$html .= '<input type="radio" name="sku-caracteristica-' . $caracteristica['Caracteristica']['id'] . '" class="t-sku-caracteristica-valor" id="sku-caracteristica-valor-' . $caracteristica_valor['id'] . '" value="' . $caracteristica_valor['id'] . '">';
					} else {
						$html .= '<input type="radio" name="sku-caracteristica-' . $caracteristica['Caracteristica']['id'] . '" class="t-sku-caracteristica-valor" id="sku-caracteristica-valor-' . $caracteristica_valor['id'] . '" value="' . $caracteristica_valor['id'] . '" checked="checked">';
					}

					$html .= '<label for="sku-caracteristica-valor-' . $caracteristica_valor['id'] . '">';
					$html .=  	$caracteristica_valor['descricao'];
					$html .= '</label>';
					$html .= '</li>';

				}

				$html .= '</ul>';
				$html .= '</div>';
			}

		} else {

			if ($attr['tipo'] == 'select') {

				$html .= '<select class="t-skus-selecao" data-tipo="select">';

				/* Se existir mais de um sku o cliente deve escolher o model, caso contrário o modelo já vem pré-selecionado */
				if ($count_skus > 1) {
					$html .= '<option value="">' . $attr['titulo'] . '</option>';
				}

				foreach ($produto['Sku'] as $sku) {
					$class_indisponivel = '';
					if ($sku['situacao_estoque'] == 'AviseMe') {
						$class_indisponivel = ' sku-indisponivel';
					}

					$html .= '<option class="t-sku' . $class_indisponivel . '" data-situacao-estoque="' . $sku['situacao_estoque'] . '" data-previsao-pre-venda="' . $view->Formatacao->data($sku['previsao_entrega_pre_venda']) . '" value="' . $sku['id'] . '">' . $sku['descricao'] . '</option>';
				}
				$html .= '</select>';

			} else if ($attr['tipo'] == 'lista') {
				$html .= '<ul class="t-skus-selecao" data-tipo="lista">';
				foreach ($produto['Sku'] as $sku) {
					$class_indisponivel = '';
					if ($sku['situacao_estoque'] == 'AviseMe') {
						$class_indisponivel = ' sku-indisponivel'; // deixar o espaço ali para não dar problema ao concatenar com as outras classes
					}

					$html .= '<li class="' . strtolower(Inflector::slug($sku['descricao'], '-')) . $class_indisponivel . '">';

					/* Se existir mais de um sku o cliente deve escolher o model, caso contrário o modelo já vem pré-selecionado */
					if ($count_skus > 1) {
						$html .= '<input data-situacao-estoque="' . $sku['situacao_estoque'] . '" data-previsao-pre-venda="' . $view->Formatacao->data($sku['previsao_entrega_pre_venda']) . '" type="radio" name="sku" class="t-sku" id="sku' . $sku['id'] . '" value="' . $sku['id'] . '"> ';
					} else {
						$html .= '<input data-situacao-estoque="' . $sku['situacao_estoque'] . '" data-previsao-pre-venda="' . $view->Formatacao->data($sku['previsao_entrega_pre_venda']) . '" type="radio" name="sku" class="t-sku" id="sku' . $sku['id'] . '" value="' . $sku['id'] . '" checked="checked"> ';
					}

					$html .= '<label for="sku' . $sku['id'] . '">';
					$html .= 	$sku['descricao'];
					$html .= '</label>';
					$html .= '</li>';

				}

				$html .= '</ul>';
			}
		}

		$html .= '<div class="mensagem-alerta t-mensagem-sku" style="display: none">';
		$html .= '<span>' . $attr['mensagemAlerta'] . '</span>';
		$html .= '</div>';
		$html .= '</div>';

		return $html;
	}
?>