<?php
	function tipoo_menu($tag) {
		$view = $tag['view_context'];
		$attr = get_object_vars($tag['attributes']);

		$padrao = array(
			'id' => '',
		);

		$attr = array_merge($padrao, $attr);

		return $view->ItemMenus->getMenu($attr['id']);

	}
?>