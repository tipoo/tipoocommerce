<?php
	function tipoo_produto($tag) {
		$view = $tag['view_context'];
		$attr = get_object_vars($tag['attributes']);

		$padrao = array(
			'campo' => 'descricao',
			'formato' => '' // nenhum, monetario, areaTexto
		);

		$attr = array_merge($padrao, $attr);

		$produto = $view->getVar('controleProduto');
		$campo = $attr['campo'];

		$retorno = '';

		if (in_array($campo, array('marca', 'categoria', 'slug', 'url'))) {
			switch ($campo) {
				case 'marca':
					$retorno = $produto['Marca']['descricao'];
					break;

				case 'categoria':
					$retorno = $produto['Categoria']['descricao'];
					break;

				case 'slug':
					$retorno = $produto['Slug']['url'];
					break;

				case 'url':
					$retorno = $view->Html->url($produto['Slug']['url'], true);
					break;
			}
		} else {
			$retorno = $produto['Produto'][$campo];
		}

		if ($attr['formato'] == 'monetario') {
			if (is_numeric($retorno)) {
				$retorno = number_format($retorno, 2, ',', '.');
			}
		} else if ($attr['formato'] == '') {
			$retorno = nl2br($retorno);
		}

		return $retorno;
	}
?>