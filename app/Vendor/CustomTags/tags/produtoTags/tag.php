<?php
	function tipoo_produtoTags($tag) {
		$view = $tag['view_context'];
		$attr = get_object_vars($tag['attributes']);

		$padrao = array(
			'url' => true, //true para tag com link ou false para tag com span
		);

		$attr = array_merge($padrao, $attr);

		$produto = $view->getVar('controleProduto');
		$tag_link = $attr['url'];


		$html = '<div class="tags">';
		if (isset($produto['TagsProduto'][0])) {
			foreach ($produto['TagsProduto'] as $tag) {

				if ($tag_link === true) {

					$html .= '<a href="' . Router::url($tag['Tag']['Slug']['url']) . '" class="tag tag-' . $tag['Tag']['nome'] . '-' . $tag['Tag']['id'] . '"> ' . $tag['Tag']['nome'] . '</a>';
				} else {

					$html .= '<span class="tag tag-' . $tag['Tag']['nome'] . '-' . $tag['Tag']['id'] . '"> ' . $tag['Tag']['nome'] . '</span>';
				}
				
			}
		}

		$html .= '</div>';

		return $html;

	}

?>