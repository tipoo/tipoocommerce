<?php
	function tipoo_mensagem($tag) {
		$view = $tag['view_context'];
		$attr = get_object_vars($tag['attributes']);

		if (!isset($attr['chave'])) {
			echo 'ERRO: É necessário especificar o parâmetro "chave" para a tag mensagem.';
		} else {
			if ($attr['chave'] != 'content') {
				return $view->Session->flash($attr['chave']);
			}
		}
	}
?>