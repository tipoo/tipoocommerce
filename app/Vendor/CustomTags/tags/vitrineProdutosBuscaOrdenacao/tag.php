<?php
	function __tipoo_vitrineProdutosBuscaOrdenacao_get_query_array($view = null) {
		$query_string = parse_url($_SERVER['QUERY_STRING']);
		$query_string = $query_string['path'];

		if (!empty($query_string)) {
			$query_string = urldecode($query_string);
			parse_str($query_string, $query_array);
			return $query_array;
		}

		return false;
	}

	function tipoo_vitrineProdutosBuscaOrdenacao($tag) {
		$view = $tag['view_context'];
		$attr = get_object_vars($tag['attributes']);

		$padrao = array(
			'tipo' => 'select', // tipo : lista, select
		);

		$attr = array_merge($padrao, $attr);

		$produtos = $view->getVar('controleProdutosBuscas');
		$buscaId = $view->getVar('controleProdutosBuscaId');
		$tipo = $attr['tipo'];

		$html = '';
		if (count($produtos)) {

			if ($tipo == 'lista') {

				$html .= '<ul class="produtos-buscas-ordenacao">';

				$query_array = __tipoo_vitrineProdutosBuscaPaginacao_get_query_array($view);

				$economia = '';
				$preco_menor = '';
				$preco_maior = '';
				$descricao_a_z = '';
				$descricao_z_a = '';

				if (isset($query_array['ordem']) && $query_array['ordem'] != '') {
					$selecionado = $query_array['ordem'] . '&ordem_dir=' . $query_array['ordem_dir'];

					if ($selecionado == 'economia&ordem_dir=asc') {
						$economia = 'active';
					}
					else if ($selecionado == 'preco&ordem_dir=asc') {
						$preco_menor = 'active';
					}
					else if ($selecionado == 'preco&ordem_dir=desc') {
						$preco_maior = 'active';
					}
					else if ($selecionado == 'descricao&ordem_dir=asc') {
						$descricao_a_z = 'active';
					}
					else if ($selecionado == 'descricao&ordem_dir=desc') {
						$descricao_z_a = 'active';
					}

				}

				if ($query_array && array_key_exists('filtro', $query_array)) {

					$html .= '<li class="'. $economia .'"><a href="?busca_id='.$buscaId.'&filtro='.$query_array['filtro'].'&ordem=economia&ordem_dir=asc">Maior economia</a></li>';
					$html .= '<li class="'. $preco_menor .'"><a href="?busca_id='.$buscaId.'&filtro='.$query_array['filtro'].'&ordem=preco&ordem_dir=asc">Menor preço</a></li>';
					$html .= '<li class="'. $preco_maior .'"><a href="?busca_id='.$buscaId.'&filtro='.$query_array['filtro'].'&ordem=preco&ordem_dir=desc">Maior preço</a></li>';
					$html .= '<li class="'. $descricao_a_z .'"><a href="?busca_id='.$buscaId.'&filtro='.$query_array['filtro'].'&ordem=descricao&ordem_dir=asc">Nome A-Z</a></li>';
					$html .= '<li class="'. $descricao_z_a .'"><a href="?busca_id='.$buscaId.'&filtro='.$query_array['filtro'].'&ordem=descricao&ordem_dir=desc">Nome Z-A</a></li>';
				} else {

					$html .= '<li class="'. $economia .'"><a href="?busca_id='.$buscaId.'&ordem=economia&ordem_dir=asc">Maior economia</a></li>';
					$html .= '<li class="'. $preco_menor .'"><a href="?busca_id='.$buscaId.'&ordem=preco&ordem_dir=asc">Menor preço</a></li>';
					$html .= '<li class="'. $preco_maior .'"><a href="?busca_id='.$buscaId.'&ordem=preco&ordem_dir=desc">Maior preço</a></li>';
					$html .= '<li class="'. $descricao_a_z .'"><a href="?busca_id='.$buscaId.'&ordem=descricao&ordem_dir=asc">Nome A-Z</a></li>';
					$html .= '<li class="'. $descricao_z_a .'"><a href="?busca_id='.$buscaId.'&ordem=descricao&ordem_dir=desc">Nome Z-A</a></li>';
				}

				$html .= '</ul>';

			} else if ($tipo == 'select') {

				$query_array = __tipoo_vitrineProdutosBuscaPaginacao_get_query_array($view);
				$selecionado = '';

				if ( isset($query_array['ordem']) && $query_array['ordem'] != '') {
					$selecionado = $query_array['ordem'] . '&ordem_dir=' . $query_array['ordem_dir'];
				}

				if ($query_array && array_key_exists('filtro', $query_array)) {

					$html .= '<select onchange="window.location.href=this.options[this.selectedIndex].value">';

					$selecione = '';
					$economia = '';
					$preco_menor = '';
					$preco_maior = '';
					$descricao_a_z = '';
					$descricao_z_a = '';


					if ($selecionado == '') {
						$selecione = 'selected';

					} else if ($selecionado == 'economia&ordem_dir=asc') {
						$economia = 'selected';
					}
					else if ($selecionado == 'preco&ordem_dir=asc') {
						$preco_menor = 'selected';
					}
					else if ($selecionado == 'preco&ordem_dir=desc') {
						$preco_maior = 'selected';
					}
					else if ($selecionado == 'descricao&ordem_dir=asc') {
						$descricao_a_z = 'selected';
					}
					else if ($selecionado == 'descricao&ordem_dir=desc') {
						$descricao_z_a = 'selected';
					}

					$html .= '<option value="?" ' . $selecione . '>Selecione</option>';

					$html .= '<option value="?busca_id=' . $buscaId . '&filtro=' . $query_array['filtro'] . '&ordem=economia&ordem_dir=asc" ' . $economia . '>Maior economia</option>';
					$html .= '<option value="?busca_id=' . $buscaId . '&filtro=' . $query_array['filtro'] . '&ordem=preco&ordem_dir=asc" ' . $preco_menor . '>Menor preço</option>';
					$html .= '<option value="?busca_id=' . $buscaId . '&filtro=' . $query_array['filtro'] . '&ordem=preco&ordem_dir=desc" ' . $preco_maior . '>Maior preço</option>';
					$html .= '<option value="?busca_id=' . $buscaId . '&filtro=' . $query_array['filtro'] . '&ordem=descricao&ordem_dir=asc" ' . $descricao_a_z . '>Nome A-Z</option>';
					$html .= '<option value="?busca_id=' . $buscaId . '&filtro=' . $query_array['filtro'] . '&ordem=descricao&ordem_dir=desc" ' . $descricao_z_a . '>Nome Z-A</option>';
				} else {
					$html .= '<select onchange="window.location.href=this.options[this.selectedIndex].value">';

					$selecione = '';
					$economia = '';
					$preco_menor = '';
					$preco_maior = '';
					$descricao_a_z = '';
					$descricao_z_a = '';

					if ($selecionado == '') {
						$selecione = 'selected';

					} else if ($selecionado == 'economia&ordem_dir=asc') {
						$economia = 'selected';
					}
					else if ($selecionado == 'preco&ordem_dir=asc') {
						$preco_menor = 'selected';
					}
					else if ($selecionado == 'preco&ordem_dir=desc') {
						$preco_maior = 'selected';
					}
					else if ($selecionado == 'descricao&ordem_dir=asc') {
						$descricao_a_z = 'selected';
					}
					else if ($selecionado == 'descricao&ordem_dir=desc') {
						$descricao_z_a = 'selected';
					}

					$html .= '<option value="?" ' . $selecione . '>Selecione</option>';

					$html .= '<option value="?busca_id=' . $buscaId . '&ordem=economia&ordem_dir=asc" ' . $economia . '>Maior economia</option>';
					$html .= '<option value="?busca_id=' . $buscaId . '&ordem=preco&ordem_dir=asc" ' . $preco_menor . '>Menor preço</option>';
					$html .= '<option value="?busca_id=' . $buscaId . '&ordem=preco&ordem_dir=desc" ' . $preco_maior . '>Maior preço</option>';
					$html .= '<option value="?busca_id=' . $buscaId . '&ordem=descricao&ordem_dir=asc" ' . $descricao_a_z . '>Nome A-Z</option>';
					$html .= '<option value="?busca_id=' . $buscaId . '&ordem=descricao&ordem_dir=desc" ' . $descricao_z_a . '>Nome Z-A</option>';
				}

				$html .= '</select>';

			}


		}
		return $html;
	}
?>
