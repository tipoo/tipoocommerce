<?php
function tipoo_formulario($tag) {
	$view = $tag['view_context'];

	$html = $view->Form->create('Contato', array('url' => array('controller' => 'formularios', 'action' => 'contato'), 'id' => array('id' => 'FormularioContatoIndexForm')));
	$html .= $view->Form->input('nome', array('label' => 'Nome'));
	$html .= $view->Form->input('sobrenome', array('label' => 'Sobrenome'));
	$html .= $view->Form->input('telefone', array('label' => 'Telefone'));
	$html .= $view->Form->input('email', array('label' => 'E-mail'));
	$html .= $view->Form->input('assunto', array('label' => 'Assunto'));
	$html .= $view->Form->input('mensagem', array('label' => 'Mensagem', 'type' => 'textarea'));
	$html .= $view->Session->flash('contato');
	$html .= $view->Form->submit('Enviar');
	$html .= $view->Form->end();
	return $html;
}
?>