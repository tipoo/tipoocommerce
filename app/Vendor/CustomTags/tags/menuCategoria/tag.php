<?php
	function tipoo_menuCategoria($tag) {
		$view = $tag['view_context'];
		$attr = get_object_vars($tag['attributes']);

		$padrao = array(
			'class' => '',
		);

		$attr = array_merge($padrao, $attr);

		return $view->MenuCategorias->getMenuCategoria($attr['class']);

	}
?>