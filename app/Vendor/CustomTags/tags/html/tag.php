<?php
	function tipoo_html($tag) {
		$view = $tag['view_context'];
		$attr = get_object_vars($tag['attributes']);

		$padrao = array(
			'tag' => 'div',
			'content' => '',
		);

		$attr = array_merge($padrao, $attr);

		$attr_exclude = array('tag', 'content');

		// Manipulando tags específicas para, por exemplo, regularizar caminhos.
		if ($attr['tag'] == 'a') {
			if (!isset($attr['href'])) {
				return 'ERRO: O atributo "href" da tag "a" é obrigatório.';
			}
			$attr['href'] = Router::url($attr['href']);

		} else if ($attr['tag'] == 'img') {
			if (!isset($attr['src'])) {
				return 'ERRO: O atributo "src" da tag "img" é obrigatório.';
			}
			$attr['src'] = Router::url('/theme/'.PROJETO.'/img/'.$attr['src']);
		}

		$atributos = '';
		foreach ($attr as $key => $value) {
			if (!in_array($key, $attr_exclude)) {
				$atributos .= ' '.$key.'="'.$value.'"';
			}
		}

		return '<'.$attr['tag'].$atributos.'>'.$tag['content'].'</'.$attr['tag'].'>';
	}
?>