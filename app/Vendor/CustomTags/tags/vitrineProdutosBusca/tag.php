<?php
	function tipoo_vitrineProdutosBusca($tag) {
		$view = $tag['view_context'];
		$attr = get_object_vars($tag['attributes']);
		$content = $tag['content'];

		$padrao = array(
			'produtosPorLinha' => 3,
			'tamanhoImagem' => 'normal', // Tamanhos: thumb, small, normal, medium, big
			'quantidadeMininaParcelas' => 1,
			'tagsUrl' => true,
			'separador' => 'true',
			'wrap' => '',
			'classWrap' => ''
		);

		$attr = array_merge($padrao, $attr);
		$produtos = $view->getVar('controleProdutosBuscas');

		$html = '';
		if (count($produtos)) {
			$i = 0;

			if ($attr['wrap'] != '') {
				$html .= '<' . $attr['wrap'] . ' class="' . $attr['classWrap'] . '">';
			}

			foreach ($produtos as $produto) {

				if ($attr['wrap'] != '' && $i % $attr['produtosPorLinha'] == 0 && $i != 0) {
					$html .= '</' . $attr['wrap'] . '>';
					$html .= '<' . $attr['wrap'] . ' class="' . $attr['classWrap'] . '">';
				}

				$html .= $view->VitrineProdutos->gerarHtmlVitrineProduto($content, $attr, $produto);

				$i++;
				if ($i % $attr['produtosPorLinha'] == 0 && $attr['separador'] == 'true') {
					$html .= '<div class="produto-busca-separador" style="clear:both"></div>';
				}
			}

			if ($attr['wrap'] != '') {
				$html .= '</' . $attr['wrap'] . '>';
			}
		} else {
			$html .= '<div class="produtos-buscas-nao-encontrados">';

			$html .= '</div>';
		}

		return $html;
	}
?>