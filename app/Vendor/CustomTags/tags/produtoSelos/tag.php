<?php
	function tipoo_produtoSelos($tag) {
		$view = $tag['view_context'];
		$attr = get_object_vars($tag['attributes']);

		$produto = $view->getVar('controleProduto');

		$html = '<div class="selos">';

		if (isset($produto['Promocao']['melhor_desconto_produto'])) {
			// Melhor promoção de desconto no produto.
			if ($produto['Promocao']['melhor_desconto_produto']['promocao']['Promocao']['selo']) {
				$html .= '<span class="selo selo-desconto-produto selo-' . strtolower(Inflector::slug($produto['Promocao']['melhor_desconto_produto']['promocao']['Promocao']['descricao'], '-')) . '-' . $produto['Promocao']['melhor_desconto_produto']['promocao']['Promocao']['id'] . '">' . $produto['Promocao']['melhor_desconto_produto']['promocao']['Promocao']['descricao'] . '</span>';
			}
		}

		if (count($produto['Promocao']['leve_x_pague_y']) > 0) {
			// Promoção leve X e pague Y, mostra todas as promoção e não verifica qual a melhor promoção.
			foreach ($produto['Promocao']['leve_x_pague_y'] as $promocao) {
				if ($promocao['Promocao']['selo']) {
					$html .= '<span class="selo selo-leve-x-pague-y selo-' . strtolower(Inflector::slug($promocao['Promocao']['descricao'], '-')) . '-' . $promocao['Promocao']['id'] . '">' . $promocao['Promocao']['descricao'] . '</span>';
				}
			}
		}

		if (count($produto['Promocao']['desconto_frete']) > 0) {
			// Promoção desconto no frete, mostra todas as promoção e não verifica qual a melhor promoção.
			foreach ($produto['Promocao']['desconto_frete'] as $promocao) {
				if ($promocao['Promocao']['selo']) {
					$html .= '<span class="selo selo-desconto-frete selo-' . strtolower(Inflector::slug($promocao['Promocao']['descricao'], '-')) . '-' . $promocao['Promocao']['id'] . '">' . $promocao['Promocao']['descricao'] . '</span>';
				}
			}
		}

		$html .= '</div>';

		return $html;

	}

?>