<?php
	function tipoo_produtoCompreJunto($tag) {
		$view = $tag['view_context'];
		$attr = get_object_vars($tag['attributes']);
		$content = $tag['content'];

		$padrao = array(
			'titulo' => 'Compre Junto e Ganhe Desconto',
			'tamanhoImagem' => 'normal', // Tamanhos: thumb, small, normal, medium, big
			'quantidadeMininaParcelas' => 1,
			'textoBtnComprar' => 'Comprar',
			'tagsUrl' => true
		);

		$attr = array_merge($padrao, $attr);
		$compre_junto_produtos = $view->getVar('controleCompreJunto');
		$produto_principal = $view->getVar('controleProduto');

		/*
			TODO :
				- Mensagem de pré-venda na hora de selecionar o sku
		*/

		$html = '';

		if (count($compre_junto_produtos)) {
			$i = 0;
			foreach ($compre_junto_produtos as $produtos) {
				// se ao menos um produto estiver esgotado não mostra o compre junto
				if (!$produto_principal['Produto']['esgotado'] && !$produtos['esgotado']) {
					$produto_principal['Produto']['preco_por'] = $produtos['produto_principal_por'];
					$produto_principal['Produto']['economize'] = $produtos['produto_principal_economize'];

					$html .= '<div class="t-produtos-compre-junto">';
					$html .= '<h2 class="titulo-compre-junto">' . $attr['titulo'] . '</h2>';
					$html .= '<ul>';

					$html .= '<li class="produto-principal">';
					$html .= $view->VitrineProdutos->gerarHtmlVitrineProduto($content, $attr, $produto_principal);
					$html .= '</li>';

					foreach ($produtos['produtos_secundarios'] as $produto_secundario) {
						$html .= '<li class="icon-plus">+</li>';
						$html .= '<li class="produto-compre-junto">';
						$html .= $view->VitrineProdutos->gerarHtmlVitrineProduto($content, $attr, $produto_secundario);
						$html .= '</li>';
					}

					$html .= '<li class="icon-equal">=</li>';
					$html .= '<li class="compre-junto">';

					$html .= '<div class="valores-compre-junto">';
					$html .= '<div class="compre-junto-preco-de"><span class="prefixo-preco-de">De: </span><span class="cifrao-preco-de">R$</span><span class="valor-preco-de">' . number_format($produtos['preco_total'], 2, ',', '.') . '</span></div>';
					$html .= '<div class="compre-junto-preco-por"><span class="prefixo-preco-por">Por: </span><span class="cifrao-preco-por">R$</span><span class="valor-preco-por">' . number_format($produtos['preco_total_com_desconto'], 2, ',', '.') . '</span></div>';
					$html .= '<div class="compre-junto-parcelamento"><span class="prefixo-parcelamento">ou em até </span><strong>' . $produtos['parcelamento']['parcelas'] . '<span class="vezes-parcelamento">X</span></strong> de <strong><span class="cifrao-parcelamento">R$</span>' . number_format($produtos['parcelamento']['valor_parcelas'], 2, ',', '.') . '</strong></div>';
					$html .= '<div class="compre-junto-economize"><span class="prefixo-economize">Compre junto e economize: </span><span class="cifrao-preco-economize">R$</span><span class="valor-economize">' . number_format($produtos['economize'], 2, ',', '.') . '</span></div>';
					$html .= '</div>';

					$html .= '<a href="#" class="t-compre-junto-btn-comprar btn-comprar">' . $attr['textoBtnComprar'] . '</a>';

					$html .= '</li>';

					$html .= '</ul>';
					$html .= '</div>';
					$i++;
				} else {
					$html .= '<!-- AVISO: Ao menos um dos produtos do compre junto está esgotado -->';
				}
			}
		} else {
			$html = '<!-- AVISO: Nenhum compre junto cadastrado para este produto -->';
		}

		return $html;

	}
?>