<?php
	function tipoo_title($tag) {
		$view = $tag['view_context'];
		return '<title>' . $view->getVar('title_for_layout') . '</title>';
	}
?>