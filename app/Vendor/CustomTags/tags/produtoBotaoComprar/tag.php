<?php
	function tipoo_produtoBotaoComprar($tag) {
		$view = $tag['view_context'];
		$attr = get_object_vars($tag['attributes']);

		$padrao = array(
			'texto' => 'Comprar',
			'tituloAviseMe' => 'Avise-me quando chegar',
			'loadJs' => 'true'
		);

		$attr = array_merge($padrao, $attr);
		$produto = $view->getVar('controleProduto');

		$html = '';

		$display_comprar = 'none';
		$display_pre_venda = 'none';
		$display_avise_me = 'none';

		/* Se existir mais de um sku ele mostra o botão comprar como default e muda de acordo com a escolha do sku. */
		if (count($produto['Sku']) > 1) {
			/* Botão comprar */
			$display_comprar = 'block';
		} else {
			if ($produto['Sku'][0]['situacao_estoque'] == 'Disponivel') {
				/* Botão comprar */
				$display_comprar = 'block';
			} else if ($produto['Sku'][0]['situacao_estoque'] == 'PreVenda') {
				/* Botão pré-venda */
				$display_pre_venda = 'block';
			} else {
				/* Avise-me */
				$display_avise_me = 'block';
			}
		}

		if ($attr['loadJs'] == 'true') {
			$view->Html->script(array('Controles/tipoo.tags.comprar.js'), array('inline' => false));
		}

		/* Botão comprar */
		$html .= '<a class="t-btn-comprar" href="#" data-situacao-estoque="Disponivel" style="display:' . $display_comprar . '">' . $attr['texto'] . '</a>';

		/* Botão pré-venda */
		$html .= '<div data-situacao-estoque="PreVenda" style="display:' . $display_pre_venda . '" class="t-pre-venda">';
		$html .= 	'<a href="#" class="t-btn-pre-venda">' . $attr['texto'] . '</a>';
		$html .= 	'<p class="t-mensagem-pre-venda">Este produto está em pré-venda (sob-encomenda). A previsão de entrega é para <span class="t-previsao-pre-venda"></span>. Compre e garanta já o seu.</p>';
		$html .= '</div>';

		/* Avise-me */
		$html .= '<p class="produto-esgotado t-avise-me" style="display:' . $display_avise_me . '">Produto esgotado.</p>';
		$html .= $view->Form->create('AviseMe', array('url' => array('controller' => 'aviseme', 'action' => 'enviar'), 'id' => 'AviseMeIndexForm', 'data-situacao-estoque' => 'AviseMe', 'class' => 't-avise-me', 'style' => 'display:' . $display_avise_me));
		$html .= '<h6>' . $attr['tituloAviseMe'] . '</h6>';
		$html .= $view->Form->input('sku_id', array('type' => 'hidden'));
		$html .= $view->Form->input('nome', array('label' => 'Nome: ', 'placeholder' => 'Digite seu nome'));
		$html .= $view->Form->input('email', array('label' => 'E-mail: ', 'placeholder' => 'Digite seu e-mail'));
		$html .= $view->Form->submit('Enviar');
		$html .= $view->Form->end();

		$html .= '<div class="mensagem-alerta t-mensagem-avise-me">';
		$html .= 	$view->Session->flash('avise_me');
		$html .= '</div>';

		return $html;
	}
?>