<?php
function tipoo_bemVindo($tag) {
	/*
	* trocar link em http por https
	* $linkLogin = $view->Html->link($attr['txtLogin'], str_replace('http://', 'https://', $view->Html->url(array('controller' => 'clientes', 'action' => 'login', '?' => array('redirect' => '/')), true)), array('class' => 'login'));
	* $linkCadastro = $view->Html->link($attr['txtCadastro'], str_replace('http://', 'https://', $view->Html->url(array('controller' => 'clientes', 'action' => 'cadastrar', '?' => array('redirect' => '/')), true)), array('class' => 'cadastre-se'));
	*/
	$view = $tag['view_context'];
	$attr = get_object_vars($tag['attributes']);
	$content = $tag['content'];
	$usuarioAutenticado = $view->getVar('usuarioAutenticado');

	$html = $tag['content'];

	if ($usuarioAutenticado) {
		$login_content = explode('#{if:autenticado}', $html);
		$login_content = explode('#{end:autenticado}', $login_content[1]);
		$logout_content = $login_content[1];
		$login_base = $login_content = $login_content[0];

		$html = str_replace('#{nome}', $usuarioAutenticado['nome'], $html);
		$html = str_replace('#{link_logout}', Router::url('/clientes/logout?redirect=%2F', true), $html);
		$html = str_replace('#{if:autenticado}', '', $html);
		$html = str_replace('#{end:autenticado}', '', $html);
		$html = str_replace($logout_content, '', $html);

		$html = str_replace($login_base, $login_content, $html);

		return $html;
	} else {
		$logout_content = explode('#{if:not:autenticado}', $html);
		$login_content = $logout_content[0];
		$logout_content = explode('#{end:not:autenticado}', $logout_content[1]);
		$logout_base = $logout_content = $logout_content[0];

		$html = str_replace('#{link_login}', Router::url('/clientes/login?redirect=%2F', true), $html);
		$html = str_replace('#{link_cadastro}', Router::url('/clientes/cadastrar?redirect=%2F', true), $html);
		$html = str_replace('#{if:not:autenticado}', '', $html);
		$html = str_replace('#{end:not:autenticado}', '', $html);
		$html = str_replace($login_content, '', $html);

		$html = str_replace($logout_base, $logout_content, $html);
		return $html;
	}
}
?>