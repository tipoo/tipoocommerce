<?php
	function tipoo_produtoThumbs($tag) {
		$view = $tag['view_context'];
		$attr = get_object_vars($tag['attributes']);

		$padrao = array(
			'tamanhoImagem' => 'small',
			'carousel' => 'true'
		);

		$attr = array_merge($padrao, $attr);

		$produto = $view->getVar('controleProduto');
		$produtoImagens = $view->getVar('controleProdutoImagens');

		if ($attr['carousel'] == 'true') {
			$view->Html->css(array('jcarousel.connected-carousels'), 'stylesheet', array('inline' => false));
			$view->Html->script(array('jquery.jcarousel.min', 'jcarousel.connected-carousels'), array('inline' => false));

			$html = '<div class="connected-carousels">';
			$html .= '<div class="navigation">';
			$html .= '<a href="#" class="prev prev-navigation">&lsaquo;</a>';
			$html .= '<a href="#" class="next next-navigation">&rsaquo;</a>';
			$html .= '<div class="carousel carousel-navigation">';
			$html .= '<ul>';

			$primeiro = true;
			foreach ($produtoImagens as $imagem) {
				$html .= '<li>';
				if ($primeiro) {
					$html .= '<a class="zoomThumbActive" href="javascript:void(0);" rel="{gallery: \'gal1\', smallimage: \''. $view->webroot . 'files/' . PROJETO . '/produtos/medium_' . $imagem['Imagem']['imagem'] .'\',largeimage: \''. $view->webroot . 'files/' . PROJETO . '/produtos/big_' . $imagem['Imagem']['imagem'] .'\'}">';
				} else {
					$html .= '<a href="javascript:void(0);" rel="{gallery: \'gal1\', smallimage: \''. $view->webroot . 'files/' . PROJETO . '/produtos/medium_' . $imagem['Imagem']['imagem'] .'\',largeimage: \''. $view->webroot . 'files/' . PROJETO . '/produtos/big_' . $imagem['Imagem']['imagem'] . '\'}">';
				}

				$html .= '<img alt="' . $produto['Produto']['descricao'] . '" title="' . $produto['Produto']['descricao'] . '" src="' . $view->webroot . 'files/' . PROJETO . '/produtos/' . $attr['tamanhoImagem'] . '_' . $imagem['Imagem']['imagem'] . '">';
				$html .= '</a>';
				$html .= '</li>';
				$primeiro = false;
			}

			$html .= '</ul>';
			$html .= '</div>';
			$html .= '</div>';
			$html .= '</div>';
		} else {
			$html  = '<div class="produto-thumbs">';
			$html .= '<ul>';

			$primeiro = true;
			foreach ($produtoImagens as $imagem) {
				$html .= '<li>';
				if ($primeiro) {
					$html .= '<a class="zoomThumbActive" href="javascript:void(0);" rel="{gallery: \'gal1\', smallimage: \''. $view->webroot . 'files/' . PROJETO . '/produtos/medium_' . $imagem['Imagem']['imagem'] .'\',largeimage: \''. $view->webroot . 'files/' . PROJETO . '/produtos/big_' . $imagem['Imagem']['imagem'] .'\'}">';
				} else {
					$html .= '<a href="javascript:void(0);" rel="{gallery: \'gal1\', smallimage: \''. $view->webroot . 'files/' . PROJETO . '/produtos/medium_' . $imagem['Imagem']['imagem'] .'\',largeimage: \''. $view->webroot . 'files/' . PROJETO . '/produtos/big_' . $imagem['Imagem']['imagem'] . '\'}">';
				}

				$html .= '<img alt="' . $produto['Produto']['descricao'] . '" title="' . $produto['Produto']['descricao'] . '" src="' . $view->webroot . 'files/' . PROJETO . '/produtos/' . $attr['tamanhoImagem'] . '_' . $imagem['Imagem']['imagem'] . '">';
				$html .= '</a>';
				$html .= '</li>';
				$primeiro = false;
			}

			$html .= '</ul>';
			$html .= '</div>';
		}

		return $html;
	}
?>