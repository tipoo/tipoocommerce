<?php
	function tipoo_vitrineProdutosBuscaTotalProdutos($tag) {
		$view = $tag['view_context'];
		$attr = get_object_vars($tag['attributes']);

		$padrao = array(
			'texto' => 'Foram encontrados <span>#{totalProdutos}</span> produtos.'
		);

		$attr = array_merge($padrao, $attr);

		$produtos = $view->getVar('controleProdutosBuscas');
		$totalProdutos = $view->getVar('controleProdutosTotal');

		$html = '';
		if (count($produtos)) {
			$html .= '<p class="produtos-buscas-qtd">';
			$html .= str_replace('#{totalProdutos}', $totalProdutos, $attr['texto']);
			$html .= '</p>';
		}
		return $html;
	}
?>