<?php
	function tipoo_vitrineProdutosBuscaForm($tag) {
		$view = $tag['view_context'];
		$attr = get_object_vars($tag['attributes']);

		$padrao = array(
			'placeholder' => 'Buscar por nome do produto ou marca',
			'submit' => 'Buscar'
		);

		$attr = array_merge($padrao, $attr);

		$html = $view->Form->create('ControleBusca', array('url' => '/busca', 'type' => 'get'));
		$html .= $view->Form->input('q', array('label' => 'Buscar: ', 'placeholder' => $attr['placeholder'], 'name' => 'q'));
		$html .= $view->Form->submit($attr['submit']);
		$html .= $view->Form->end();

		return $html;
	}
?>