<?php
	function tipoo_produtoCaracteristicas($tag) {
		$view = $tag['view_context'];

		$produto = $view->getVar('controleProduto');

		$html = '<table>';

		foreach ($produto['CaracteristicasValoresSelecionado'] as $caracteristicas) {
			$html .= '<tr>';
			$html .= 	'<th class="caracteristica-titulo ' . Inflector::slug($caracteristicas['Caracteristica']['descricao'], '-') . '">' . $caracteristicas['Caracteristica']['descricao'] . '</th>';
			$html .= 	'<td class="caracteristica-valor ' . Inflector::slug($caracteristicas['Caracteristica']['descricao'], '-') . '">';
			$html .= 		$caracteristicas['descricao'];
			$html .= 		nl2br($caracteristicas['descricao_grande']);

			if (isset($caracteristicas['CaracteristicasValor']['descricao'])) {
				$html .=	$caracteristicas['CaracteristicasValor']['descricao'];
			}

			$html .= 	'</td>';
			$html .= '</tr>';
		}

		$html .= '</table>';

		return $html;
	}
?>