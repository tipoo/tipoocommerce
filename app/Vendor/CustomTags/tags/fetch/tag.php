<?php
	function tipoo_fetch($tag) {
		$view = $tag['view_context'];
		$attr = get_object_vars($tag['attributes']);

		if (!isset($attr['tipo'])) {
			echo 'ERRO: É necessário especificar o parâmetro "tipo" para a tag fetch.';
		} else {
			if ($attr['tipo'] != 'content') {
				return $view->fetch($attr['tipo']);
			}
		}
	}
?>