<?php
	function tipoo_script($tag) {
		$view = $tag['view_context'];
		$attr = get_object_vars($tag['attributes']);

		$inline = isset($attr['inline']) && $attr['inline'] != 'false' ? true : false;

		if ($inline) {
			if (isset($attr['src'])) {
				return $view->Html->script(array($attr['src']));
			} else {
				return $view->Html->scriptBlock($tag['content']);
			}
		}
	}
?>