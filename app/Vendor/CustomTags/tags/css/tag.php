<?php
	function tipoo_css($tag) {
		$view = $tag['view_context'];
		$attr = get_object_vars($tag['attributes']);

		$inline = isset($attr['inline']) && $attr['inline'] != 'false' ? true : false;

		if ($inline) {
			if (isset($attr['src'])) {
				return $view->Html->css(array($attr['src']));
			} else {
				echo 'ERRO: A tag css não aceita blocos de código. O atributo  "src" deve ser especificado';
			}
		}
	}
?>