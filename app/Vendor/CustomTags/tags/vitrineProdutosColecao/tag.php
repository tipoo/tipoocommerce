<?php
	function tipoo_vitrineProdutosColecao($tag) {
		$view = $tag['view_context'];
		$attr = get_object_vars($tag['attributes']);
		$content = $tag['content'];

		$padrao = array(
			'chave' => '', // Chave dinâmica
			'produtosPorLinha' => 3,
			'tamanhoImagem' => 'normal', // Tamanhos: thumb, small, normal, medium, big
			'quantidadeMininaParcelas' => 1,
			'tagsUrl' => true,
			'separador' => 'true',
			'wrap' => '',
			'classWrap' => '',
			'class' => ''
		);

		$attr = array_merge($padrao, $attr);
		$colecoes = $view->getVar('controleColecoes');

		if (count($colecoes)) {

			foreach ($colecoes as $colecao) {

				if ($colecao['ColecoesLocal']['chave'] == $attr['chave']) {

					if (count($colecao['Colecao']['ColecoesProduto'])) {

						// Deixa a coleção aleatória
						if ($colecao['ColecoesLocal']['rand']) {
							shuffle($colecao['Colecao']['ColecoesProduto']);
						}

						$html = '<div class="produtos-colecao ' . strtolower(Inflector::slug($attr['chave'], '-')) . ' ' . $attr['class'] . '">';
	 					$html .= '<h2 class="titulo-colecoes">' . $colecao['ColecoesLocal']['descricao'] . '</h2>';

						$i = 0;
						if ($attr['wrap'] != '') {
							$html .= '<' . $attr['wrap'] . ' class="' . $attr['classWrap'] . '">';
						}

						foreach ($colecao['Colecao']['ColecoesProduto'] as $produto) {

							if ($attr['wrap'] != '' && $i % $attr['produtosPorLinha'] == 0 && $i != 0) {
								$html .= '</' . $attr['wrap'] . '>';
								$html .= '<' . $attr['wrap'] . ' class="' . $attr['classWrap'] . '">';
							}

							$html .= $view->VitrineProdutos->gerarHtmlVitrineProduto($content, $attr, $produto);

							$i++;
							if ($i % $attr['produtosPorLinha'] == 0 && $attr['separador'] == 'true') {
								$html .= '<div class="produto-busca-separador" style="clear:both"></div>';
							}

							// Quantidade máxima de produtos na página
							if ($colecao['ColecoesLocal']['qtd_max_produtos'] == $i) {
								break;
							}

						}

						if ($attr['wrap'] != '') {
							$html .= '</' . $attr['wrap'] . '>';
						}

						$html .= '</div>';

					} else {
						$html = '<!-- AVISO: Nenhum produto foi adicionado a esta coleção -->';
					}

					break;
				} else {
					$html = '<!-- AVISO: Nenhuma coleção cadastrada para esta chave -->';
				}
			}
		} else {
			$html = '<!-- AVISO: Nenhuma coleção encontrada -->';
		}

		return $html;

	}
?>
