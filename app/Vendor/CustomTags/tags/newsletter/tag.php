<?php
function tipoo_newsletter($tag) {
	$view = $tag['view_context'];
	$attr = get_object_vars($tag['attributes']);

	$padrao = array(
		'loadJs' => 'true',
		'titulo' => 'Receba ofertas exclusivas',
		'placeHolderNome' => 'Informe seu nome',
		'placeHolderEmail' => 'Informe seu e-mail',
		'submit' => 'Enviar'
	);

	$attr = array_merge($padrao, $attr);

	if ($attr['loadJs'] == 'true') {
		$view->Html->script(array('Controles/tipoo.newsletter'), array('inline' => false));
	}

	$html = $view->Form->create('Newsletter', array('url' => array('controller' => 'newsletters', 'action' => 'enviar'), 'id' => 'NewsletterIndexForm', 'class' => 't-newsletter'));
	$html .= '<i class="icon-news"></i><strong>'.$attr['titulo'].'</strong>';
	$html .= $view->Form->input('nome', array('label' => 'Nome: ', 'placeholder' => $attr['placeHolderNome']));
	$html .= $view->Form->input('email', array('label' => 'E-mail: ', 'placeholder' => $attr['placeHolderEmail']));
	$html .= $view->Form->submit($attr['submit']);
	$html .= $view->Form->end();
	return $html;
}
?>