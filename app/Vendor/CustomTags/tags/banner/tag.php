<?php
function tipoo_banner($tag) {

	$view = $tag['view_context'];
	$attr = get_object_vars($tag['attributes']);
	$banners = $view->getVar('controleBanners');
	$banners_generico = $view->getVar('controleBannersGenerico');

	$padrao = array(
		'chave' => '', // Chave dinâmica
		'tipo' => 'unico' // unico, lista, slide
	);

	$attr = array_merge($padrao, $attr);

	$html = '';

	if ($banners != '') {
		foreach ($banners as $banner) {
			if ($banner['BannerTipo']['chave'] == $attr['chave']) {
				$html .= $view->Banners->gerarHtmlBanners($banner, $attr);
				break;
			}
		}
	}

	if ($banners_generico != '') {
		foreach ($banners_generico as $banner) {
			if ($banner['BannerTipo']['chave'] == $attr['chave']) {
				$html .= $view->Banners->gerarHtmlBanners($banner, $attr);
				break;
			}
		}
	}

	return $html;
}

?>