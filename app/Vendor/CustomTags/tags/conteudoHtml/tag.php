<?php
	function tipoo_conteudoHtml($tag) {
		$view = $tag['view_context'];
		$attr = get_object_vars($tag['attributes']);

		$padrao = array(
			'chave' => '', // Chave dinâmica
		);

		$attr = array_merge($padrao, $attr);
		$htmls = $view->getVar('controleHtml');

		if (count($htmls)) {

			foreach ($htmls as $html) {

				if ($html['HtmlsLocal']['chave'] == $attr['chave']) {

					$html = $html['HtmlsLocal']['html'];

					break;
				} else {
					$html = '<!-- AVISO: Nenhum conteúdo encontrado para esta chave -->';
				}
			}
		} else {
			$html = '<!-- AVISO: Nenhum conteúdo encontrado -->';
		}

		return $html;

	}
?>
