<?php
	function tipoo_categoria($tag) {
		$view = $tag['view_context'];
		$attr = get_object_vars($tag['attributes']);

		$padrao = array(
			'campo' => 'descricao'
		);

		$attr = array_merge($padrao, $attr);

		$categoria = $view->getVar('categoria');
		$campo = $attr['campo'];

		return $categoria['Categoria'][$campo];
	}
?>