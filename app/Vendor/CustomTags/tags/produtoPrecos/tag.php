<?php
	function tipoo_produtoPrecos($tag) {
		$view = $tag['view_context'];
		$attr = get_object_vars($tag['attributes']);

		$padrao = array(
			'campo' => 'preco_por', // preco_de, preco_por
			'label' => '',
			'cifrao' => 'true'
		);

		$attr = array_merge($padrao, $attr);

		$produto = $view->getVar('controleProduto');
		$label = $attr['label'];
		$campo = $attr['campo'];
		$cifrao = $attr['cifrao'];

		$preco = number_format($produto['Produto'][$campo], 2, ',', '.');

		$html = '';

		$html .= $view->Html->scriptBlock('
		 		if (typeof tipoo.data.produto === "undefined") {
		 			tipoo.data.produto = new Object();
		 		}
		 		tipoo.data.produto.preco_por = ' . $produto['Produto']['preco_por'] . ';
		 	', array('inline' => false));

		if (!empty($produto['Produto'][$campo])) {
			if (!empty($label)) {
				$html .= '<span class="produto-label-' . str_replace('_', '-', $campo) . '">' . $label . ' </span>';
			}

			if ($cifrao === 'true') {
				$html .= '<span class="produto-cifrao-' . str_replace('_', '-', $campo) . '">R$ </span>';
			}

			$html .= '<span class="produto-valor' . str_replace('_', '-', $campo) . '">' . $preco . '</span>';
		}

		return $html;
	}
?>