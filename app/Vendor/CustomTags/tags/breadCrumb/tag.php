<?php
	function tipoo_breadCrumb($tag) {
		$view = $tag['view_context'];
		$attr = get_object_vars($tag['attributes']);

		$padrao = array(
			'class' => 'breadcrumb',
		);

		$attr = array_merge($padrao, $attr);

		$bcCategoria = $view->getVar('controlesBreadCrumbCategoria');
		$bcMarca = $view->getVar('controlesBreadCrumbMarca');
		$bcProduto = $view->getVar('controleProduto');

		if (isset($bcCategoria) && !empty($bcCategoria)) {
			$breadCrumb = $bcCategoria;
		} else if (isset($bcMarca) && !empty($bcMarca)) {
			$breadCrumb = $bcMarca;
		}

		if (isset($breadCrumb)) {
			$html = '<ul class="'.$attr['class'].'">';

			$html .= '<li class="home">';
			$html .= $view->Html->link('Home', '/', array('escape' => false));
			$html .= '</li>';

			$count = 1;
			foreach ($breadCrumb as $bc_url => $bc_descricao) {
				if (count($breadCrumb) == $count && (!isset($bcProduto) && empty($bcProduto))) {
					$html .= '<li class="active">';
					$html .= $bc_descricao;
					$html .= '</li>';
				} else {
					$html .= '<li>';
					$html .= $view->Html->link($bc_descricao, $bc_url, array('escape' => false));
					$html .= '</li>';
				}

				$count++;
			}

			if (isset($bcProduto) && !empty($bcProduto)) {
				$html .= '<li class="active">';
				$html .= $bcProduto['Produto']['descricao'];
				$html .= '</li>';
			}

			$html .= '</ul>';

			return $html;
		}
	}
?>