<?php
function tipoo_produtoTabelaParcelamento($tag) {
	$view = $tag['view_context'];
	$attr = get_object_vars($tag['attributes']);

	$padrao = array(
		'quantidadeMininaParcelas' => 1,
		'ordem' => 'ASC'
	);

	$attr = array_merge($padrao, $attr);
	$produto = $view->getVar('controleProduto');
	$controleParcelamento = $view->getVar('controleParcelamento');

	// $parcelamento = __tipoo_calcular_parcelamento_maximo($produto['Produto']['preco_por'], $view->Configuracoes->get('parc_parcelas'), $view->Configuracoes->get('parc_valor_min_parcela'), $view->Configuracoes->get('parc_juros'));
	$parcelamento = new Parcelamento();
	$parcelamento->setParcelas($controleParcelamento);
	$parcelamento = $parcelamento->coeficiente((float)$produto['Produto']['preco_por'], (float)$view->Configuracoes->get('parc_valor_min_parcela'), $view->Configuracoes->get('parc_juros_am'));

	if ($attr['ordem'] == 'DESC') {
		$parcelamento = array_reverse($parcelamento, true);
	}

	if ($produto['Produto']['parcelas'] >= $attr['quantidadeMininaParcelas'] && count($parcelamento) > 0) {

		$html = '<div class="tabela-parcelamento">';
		$html .=	 '<table>';
		$html .=		'<thead>';
		$html .=			'<tr>';
		$html .=				'<th>';
		$html .=					'Nº de parcelas';
		$html .=				'</th>';
		$html .=				'<th>';
		$html .=					'Valor das parcelas';
		$html .=				'</th>';
		$html .=			'</tr>';
		$html .=		'</thead>';
		$html .=		'<tbody>';

		foreach ($parcelamento as $parcela) {
			$html .=		'<tr>';
			$html .=			'<td>';

			if ($parcela['parcelas'] == 1) {
				$html .=			'<span class="parcelas">à vista</span>';
			} else {
				$html .=			'<span class="parcelas">' . $parcela['parcelas'] . 'x</span> <span class="juros">' . $parcela['txt_juros'] . '</span>';
			}

			$html .=			'</td>';
			$html .=			'<td>';
			$html .=				'<span class="valor-parcelas">R$ ' . $parcela['valor'] . '</span>';
			$html .=			'</td>';
			$html .=		'</tr>';
		}

		$html .=		'</tbody>';
		$html .= 	'</table>';
		$html .= '</div>';

		return $html;
	}
}
?>