<?php
	function tipoo_produtoFotoPrincipal($tag) {
		$view = $tag['view_context'];
		$attr = get_object_vars($tag['attributes']);

		$padrao = array(
			'tamanhoImagem' => 'medium',
			'zoom' => 'true'
		);

		$attr = array_merge($padrao, $attr);

		$produto = $view->getVar('controleProduto');

		// Zoom
		if ($attr['zoom'] == 'true') {
			$view->Html->css(array('jqzoom/css/jquery.jqzoom'), 'stylesheet', array('inline' => false));
			$view->Html->script(array('jquery.jqzoom'), array('inline' => false));
			$view->Html->scriptBlock("$(document).ready(function() {  var options = {title: false, zoomWidth: 520, zoomHeight: 430}; $('.imagem-zoom').jqzoom(options); });", array('inline' => false));
		}

		$html  = '<a href="' . $view->webroot . 'files/' . PROJETO . '/produtos/' . 'big_' . $produto['Imagem'][0]['imagem'] . '" class="imagem-zoom" rel="gal1">';
		$html .= '<img id="produto-imagem-destaque" alt="' . $produto['Produto']['descricao'] . '" title="' . $produto['Produto']['descricao'] . '" src="' . $view->webroot . 'files/' . PROJETO . '/produtos/' . $attr['tamanhoImagem'] . '_' . $produto['Imagem'][0]['imagem'] . '">';
		$html .= '</a>';

		return $html;
	}
?>