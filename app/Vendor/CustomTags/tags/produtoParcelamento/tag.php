<?php
	function tipoo_produtoParcelamento($tag) {
		$view = $tag['view_context'];
		$attr = get_object_vars($tag['attributes']);

		$padrao = array(
			'quantidadeMininaParcelas' => 1,
		);

		$attr = array_merge($padrao, $attr);
		$produto = $view->getVar('controleProduto');
		$qtd_min_parcelas = $attr['quantidadeMininaParcelas'];

		if ($produto['Produto']['parcelas'] >= $qtd_min_parcelas) {

			$html = $tag['content'];

			$html = str_replace('#{parcelas}', $produto['Produto']['parcelas'], $html);
			$html = str_replace('#{valor_parcelas}', number_format($produto['Produto']['valor_parcelas'], 2, ',', '.'), $html);

			return $html;
		}
	}
?>