<?php
	function __tipoo_vitrineProdutosBuscaPaginacao_get_query_array($view = null) {
		$query_string = parse_url($_SERVER['QUERY_STRING']);
		$query_string = $query_string['path'];

		if (!empty($query_string)) {
			$query_string = urldecode($query_string);
			parse_str($query_string, $query_array);
			return $query_array;
		}

		return false;
	}

	function tipoo_vitrineProdutosBuscaPaginacao($tag) {
		$view = $tag['view_context'];
		$attr = get_object_vars($tag['attributes']);

		$produtos = $view->getVar('controleProdutosBuscas');
		$totalProdutos = $view->getVar('controleProdutosTotal');
		$buscaId = $view->getVar('controleProdutosBuscaId');

		$pagina_atual = 1;
		if (isset($view->request->query['pagina'])) {
			$pagina_atual = $view->request->query['pagina'];
		}

		$html = '';
		if (count($produtos)) {

			$total_paginas = (int) $totalProdutos / $view->Configuracoes->get('cat_qtd_produtos_pagina');;
			if (((int) $totalProdutos % $view->Configuracoes->get('cat_qtd_produtos_pagina')) != 0) {
				$total_paginas += 1;
			}

			$html .= '';
			$html .= '<div class="produto-busca-paginacao">';
			$html .= '<ul>';

			for ($i = 1; $i <= $total_paginas; $i++) {
				if ($i == $pagina_atual) {
					$html .= '<li class="atual">';
				} else {
					$html .= '<li>';
				}

				// Verificando se tem filtro e adicionando ao link
				$query_array = __tipoo_vitrineProdutosBuscaPaginacao_get_query_array($view);
				if ($query_array && array_key_exists('filtro', $query_array)) {
					$html .= '<a href="?busca_id='.$buscaId.'&pagina='.$i.'&filtro='.$query_array['filtro'].'">'.$i.'</a>';
				} else {
					$html .= '<a href="?busca_id='.$buscaId.'&pagina='.$i.'">'.$i.'</a>';
				}

				$html .= '</li>';
			}

			$html .= '</ul>';
			$html .= '</div>';
		}

		return $html;
	}
?>