<?php
	function tipoo_produtoAvaliacao($tag) {
		$view = $tag['view_context'];
		$attr = get_object_vars($tag['attributes']);

		$view->Html->css(array(
			'Controles/tipoo.tags.avaliar.css',
		 ), 'stylesheet', array('inline' => false));


		$view->Html->script(array(
			'raty/jquery.raty',
			'Controles/tipoo.tags.avaliar.js',
		 ), array('inline' => false));



		$padrao = array(
			'formulario' => 'true', // true => mostrar formulário, false => mostra média de pontuação
		);

		$attr = array_merge($padrao, $attr);
		$produto = $view->getVar('controleProduto');

		if ($attr['formulario'] == 'true') {

			$html = $view->Form->create('Avaliacao', array('class' => 't-avaliacao-produto-form'));
			$html .= $view->Form->input('produto_id', array('type' => 'hidden', 'value' => $produto['Produto']['id']));
			$html .= $view->Form->input('nome', array('label' => 'Nome*: ', 'class' => 'avaliacao-nome', 'placeholder' => 'Digite seu nome'));
			$html .= $view->Form->input('email', array('class' => 'avaliacao-email', 'label' => 'E-mail*: ', 'placeholder' => 'Digite seu email'));
			$html .= '<div class="avaliacao">';
			$html .= 	'<label>Avaliação</label>';
			$html .= 	'<div class="star"></div>';
			$html .= '</div>';	
			$html .= $view->Form->submit('Avaliar');
			$html .= $view->Form->end();	
		} else {

			if ($produto['Produto']['avaliacao'] == '') {
				$media_avaliacao = 0;

			} else {
				$media = $produto['Produto']['avaliacao'];
				$decimais = explode('.', $media);

				if ($decimais[1] >= 50) {
					$media_avaliacao = ($decimais[0] + 1) * 10;
				} else {
					$media_avaliacao = $decimais[0] * 10;
				}
			}

			$html = '<div class="avaliacao">';
			$html .= 	'<label>Avaliação dos Consumidores</label>';
			$html .= 	'<span class="media-avaliacao media-' . $media_avaliacao . '"  data-produto="' . $produto['Produto']['id'] . '"></span>';
			$html .= '</div>';	
		}

		return $html;

	}
?>