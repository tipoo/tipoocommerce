<?php
	function tipoo_menuMarcas($tag) {
		$view = $tag['view_context'];
		$attr = get_object_vars($tag['attributes']);

		$padrao = array(
			'class' => 'menu-marcas',
		);

		$attr = array_merge($padrao, $attr);

		$marcas = $view->getVar('controlesMenuMarcas');

		$html = '<ul class="'.$attr['class'].'">';
		foreach ($marcas as $marca) {
			$html .= '<li>';

			$link = '';
			if (!empty($marca['Marca']['imagem'])) {
				$link .= '<img src="' . $view->webroot . 'files/' . PROJETO . '/marcas/' .  $marca['Marca']['imagem'] . '">';
			}
			if (!empty($marca['Marca']['descricao'])) {
				$link .= '<span>'.$marca['Marca']['descricao'].'</span>';
			}
			$html .= $view->Html->link($link, $marca['Slug']['url'], array('escape' => false));
			$html .= '</li>';
		}
		$html .= '</ul>';

		return $html;
	}
?>