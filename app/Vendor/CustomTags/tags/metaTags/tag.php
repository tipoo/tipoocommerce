<?php
function tipoo_metaTags($tag) {

	$view = $tag['view_context'];

	$meta_description = $view->getVar('meta_description');
	$meta_title = $view->getVar('meta_title');

	$html = $view->Html->meta('description', $meta_description);

	$html .= '<meta property="og:title" content="' . $meta_title . '">';
	$html .= '<meta property="og:description" content="' . $meta_description . '">';
	$html .= '<meta property="og:site_name" content="' . Configure::read('Loja.nome') . '">';

	return $html;
}
?>