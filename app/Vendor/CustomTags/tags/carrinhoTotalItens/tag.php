<?php
function tipoo_carrinhoTotalItens($tag) {

	$view = $tag['view_context'];
	$skus = $view->Session->read('Carrinho.Skus');

	$qtd_itens = 0;

	if (count($skus)) {
		foreach ($skus as $sku) {
			$qtd_itens += $sku['qtd'];
		}
	}

	return $qtd_itens;
}
?>