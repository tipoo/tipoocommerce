<?php
	function tipoo_produtoEconomize($tag) {
		$view = $tag['view_context'];
		$attr = get_object_vars($tag['attributes']);

		$produto = $view->getVar('controleProduto');

		if ($produto['Produto']['economize'] > 0) {

			$html = $tag['content'];

			$valor = number_format($produto['Produto']['economize'], 2, ',', '.');

			$html = str_replace('#{economize}', $valor, $html);

			return $html;
		}

	}

?>



