<?php
	function tipoo_lojas($tag) {
		$view = $tag['view_context'];
		$attr = get_object_vars($tag['attributes']);

		$lojas = $view->getVar('lojas');

		$html = '<div class="lojas-fisicas">';
		$html .= 	'<ul>';

		foreach ($lojas as $loja) {
			$html .= 	'<li>';
			$html .= 		'<strong>' . $loja['Loja']['descricao'] .'</strong>';
			$html .= 		'<p>' . $loja['Loja']['endereco'] . ', ' . $loja['Loja']['numero'] . '<p>';
			$html .= 		'<p>' . $loja['Loja']['bairro'] . ', ' . $loja['Cidade']['nome'] . ' - ' . $loja['Cidade']['Estado']['uf'] . '<p>';
			$html .= 	'</li>';
		}

		$html .= 	'</ul>';
		$html .= '</div>';

		return $html;
	}
?>