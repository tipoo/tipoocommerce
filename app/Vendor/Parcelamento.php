<?php
class Parcelamento {
	static $parcelas;

	function setParcelas ($parcelas) {
		$_parcelas = array();

		if (!empty($parcelas)) {
			foreach ($parcelas['Parcela'] as $parcela) {
				$_parcelas[$parcela['parcelas']] = $parcela['juros'];
			}
		}

		self::$parcelas = $_parcelas;
	}

	function _simplifica ($valor) {
		return preg_replace('/[^\d]/', '', $valor);
	}

	function _formatar ($formato='', $valores) {
		preg_match_all ('@\{\w+\}@', $formato, $matches);
		foreach ($matches[0] as $value) {
			$key = substr($value, 1, -1);
			if (array_key_exists($key, $valores)) {
			$formato = str_replace($value, $valores[$key], $formato);
			}
		}
		return $formato;
	}

	function coeficiente ($valor, $maximo = 10, $juros_am = false) {
		list ($valor, $maximo) = array (self::_simplifica($valor), self::_simplifica($maximo));

		$res = array ();
		foreach (self::$parcelas as $parcela => $coef) {
			if (!$parcela) continue;
			$res[$parcela]['parcelas'] = $parcela;

			if ($coef == '0') {
				$res[$parcela]['valor'] = number_format($valor / $parcela, 2, ',', '');
				$res[$parcela]['total'] = number_format($valor, 2, ',', '');
				$res[$parcela]['txt_juros'] = 'sem juros';
			} else {
				if (!$juros_am) {
					$res[$parcela]['valor'] = number_format($valor * (1 + $coef) / $parcela, 2, ',', '');
					$res[$parcela]['total'] = number_format($valor * (1 + $coef), 2, ',', '');
				} else {
					$valor_parcela = ($valor * $coef) / (1 - (1 / pow(1 + $coef, $parcela)));

					$res[$parcela]['valor'] = number_format($valor_parcela, 2, ',', '');
					$res[$parcela]['total'] = number_format($valor_parcela * $parcela, 2, ',', '');
				}

				$res[$parcela]['txt_juros'] = 'com juros';
			}
		}
		$res = array_filter($res, create_function('$i', 'return $i["valor"]>'.$maximo.';'));
		return $res;
	}

	function parcelas ($data=array ()) {
		$default = array (
			'valor' => 0,
			'maximo' => 10,
			'formato' => '{parcelas} x {valor} = {total}',
			'parcela' => 'todas',
		);
		$data += $default; // Mescla os dois, não sobrescrevendo o data
		extract($data, EXTR_SKIP);
		$parcelas = self::coeficiente($valor, $maximo);
		$res = array ();
		foreach ($parcelas as $item) {
			$res[$item['parcelas']] = self::_formatar($formato, $item);
		}
		if ($parcela=='todas')  return $res;
		if ($parcela=='ultima') return array_pop($res);
		if ($parcela=='primeira') return array_shift($res);
		if (array_key_exists($parcela, $res)) return $res[$parcela];
		return '';
	}
}
?>