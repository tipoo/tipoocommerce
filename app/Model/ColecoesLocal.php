<?php
App::uses('AppModel', 'Model');

class ColecoesLocal extends AppModel {

	public $actsAs = array(
		'CustomTime' => array(
			'date' => array(),
			'datetime' => array('data_inicio', 'data_fim')
		),
		'Containable'
	);

	public $belongsTo = array(
		'Colecao' => array(
			'className' => 'Colecao',
			'foreignKey' => 'colecao_id',
			'conditions' => array('Colecao.ativo' => true)
		),
		'Pagina' => array(
			'className' => 'Pagina',
			'foreignKey' => 'pagina_id',
		),
		'Categoria' => array(
			'className' => 'Categoria',
			'foreignKey' => 'categoria_id',
		),
		'Marca' => array(
			'className' => 'Marca',
			'foreignKey' => 'marca_id',
		),
		'Produto' => array(
			'className' => 'Produto',
			'foreignKey' => 'produto_id',
		)
	);
}
?>