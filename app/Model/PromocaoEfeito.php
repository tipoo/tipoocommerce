<?php
App::uses('AppModel', 'Model');

class PromocaoEfeito extends AppModel {

	public $actsAs = array('Containable');

	public $belongsTo = array(
		'Promocao' => array(
			'className' => 'Promocao',
			'foreignKey' => 'promocao_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

}
?>