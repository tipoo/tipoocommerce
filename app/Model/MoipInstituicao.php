<?php
App::uses('AppModel', 'Model');

class MoipInstituicao extends AppModel {
	public $actsAs = array('Containable');

	public $hasMany = array(
		'Parcela' => array(
			'className' => 'Parcela',
			'foreignKey' => 'moip_bandeira_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
}
?>