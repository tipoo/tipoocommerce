<?php
App::uses('AppModel', 'Model');

class TagsProduto extends AppModel {

	public $actsAs = array(
		'Containable'
	);

	public $belongsTo = array(
		'Produto' => array(
			'className' => 'Produto',
			'foreignKey' => 'produto_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),

		'Tag' => array(
			'className' => 'Tag',
			'foreignKey' => 'tag_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),

	);
}
?>