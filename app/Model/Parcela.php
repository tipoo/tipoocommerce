<?php
App::uses('AppModel', 'Model');

class Parcela extends AppModel {

	public $actsAs = array('Containable');

	public $displayField = 'parcelas';

	public $belongsTo = array(
		'CieloBandeira' => array(
			'className' => 'CieloBandeira',
			'foreignKey' => 'cielo_bandeira_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

}
?>