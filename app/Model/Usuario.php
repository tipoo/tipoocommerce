<?php

App::uses('AppModel', 'Model');

class Usuario extends AppModel {

	public $displayField = 'nome';

	public $belongsTo = array(
		'Perfil' => array(
			'className' => 'Perfil',
			'foreignKey' => 'perfil_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	public function beforeSave($options = array()) {
		if (isset($this->data[$this->alias]['senha'])) {
			$this->data[$this->alias]['senha'] = AuthComponent::password($this->data[$this->alias]['senha']);
		}
		return true;
	}
}
?>