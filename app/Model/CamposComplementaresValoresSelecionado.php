<?php
App::uses('AppModel', 'Model');

class CamposComplementaresValoresSelecionado extends AppModel {

	public $actsAs = array('Containable');

	public $belongsTo = array(
		'CamposComplementar' => array(
			'className' => 'CamposComplementar',
			'foreignKey' => 'campos_complementar_id',
		),
		'CamposComplementaresValor' => array(
			'className' => 'CamposComplementaresValor',
			'foreignKey' => 'campos_complementares_valor_id',
		)
	);

}
?>