<?php
App::uses('AppModel', 'Model');

class ItemMenu extends AppModel {

	public $belongsTo = array(
		'Menu' => array(
			'className' => 'Menu',
			'foreignKey' => 'menu_id',
		)
	);

}
?>