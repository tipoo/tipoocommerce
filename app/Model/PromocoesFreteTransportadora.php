<?php
App::uses('AppModel', 'Model');

class PromocoesFreteTransportadora extends AppModel {

	public $actsAs = array(
		'Containable'
	);

	public $belongsTo = array(
		'Promocao' => array(
			'className' => 'Promocao',
			'foreignKey' => 'promocao_id',
			'conditions' => array('Promocao.ativo' => true)
		),
		'FreteTransportadora' => array(
			'className' => 'FreteTransportadora',
			'foreignKey' => 'frete_transportadora_id',
		)
	);
}
?>