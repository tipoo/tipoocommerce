<?php
App::uses('AppModel', 'Model');

class CaracteristicasValoresSelecionado extends AppModel {

	public $actsAs = array('Containable');

	public $belongsTo = array(
		'Caracteristica' => array(
			'className' => 'Caracteristica',
			'foreignKey' => 'caracteristica_id',
		),
		'CaracteristicasValor' => array(
			'className' => 'CaracteristicasValor',
			'foreignKey' => 'caracteristicas_valor_id',
		)
	);

}
?>