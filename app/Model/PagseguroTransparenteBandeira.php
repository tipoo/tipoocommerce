<?php
App::uses('AppModel', 'Model');

class PagseguroTransparenteBandeira extends AppModel {

	public $actsAs = array('Containable');

	public $hasMany = array(
		'Parcela' => array(
			'className' => 'Parcela',
			'foreignKey' => 'pagseguro_transparente_bandeira_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
?>