<?php
App::uses('AppModel','Model');

class Tag extends AppModel {

	public $displayField ='nome';

	public $actsAs = array(
		'Containable'
	);

	public $belongsTo = array(
		'Slug'
	);

	public $hasMany = array(
		'TagsProduto' => array(
			'className' => 'TagsProduto',
			'foreignKey' => 'tag_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}