<?php
App::uses('AppModel', 'Model');

class AfinidadeProduto extends AppModel {

	public $actsAs = array(
		'Containable'
	);

	public $belongsTo = array(
		'Produto' => array(
			'className' => 'Produto',
			'foreignKey' => 'produto_secundario_id',
		),
	);

}
?>