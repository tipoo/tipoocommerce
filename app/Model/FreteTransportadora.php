<?php
App::uses('AppModel', 'Model');

class FreteTransportadora extends AppModel {

	public $actsAs = array('Containable');

	public $hasMany = array(
		'PromocoesFreteTransportadora' => array(
			'className' => 'PromocoesFreteTransportadora',
			'foreignKey' => 'frete_transportadora_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'FreteValor' => array(
			'className' => 'FreteValor',
			'foreignKey' => 'frete_transportadora_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
?>