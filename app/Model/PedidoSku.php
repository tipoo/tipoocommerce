<?php
App::uses('AppModel', 'Model');

class PedidoSku extends AppModel {

	public $actsAs = array(
		'Containable'
	);

	public $belongsTo = array(
		'Sku' => array(
			'className' => 'Sku',
			'foreignKey' => 'sku_id',
		),
		'Pedido' => array(
			'className' => 'Pedido',
			'foreignKey' => 'pedido_id',
		)
	);

	public $hasMany = array(
		'PedidoCaracteristica' => array(
			'className' => 'PedidoCaracteristica',
			'foreignKey' => 'pedido_sku_id',
			'dependent' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Promocao' => array(
			'className' => 'Promocao',
			'foreignKey' => 'pedido_sku_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'CompreJuntoPromocao' => array(
			'className' => 'CompreJuntoPromocao',
			'foreignKey' => 'pedido_sku_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'AfinidadeProduto' => array(
			'className' => 'AfinidadeProduto',
			'foreignKey' => 'pedido_sku_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'PedidoSkuServico' => array(
			'className' => 'PedidoSkuServico',
			'foreignKey' => 'pedido_sku_id',
			'dependent' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
}
?>