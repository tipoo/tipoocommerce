<?php
App::uses('AppModel', 'Model');

class Caracteristica extends AppModel {

	public $actsAs = array('Containable');

	public $belongsTo = array(
		'Categoria' => array(
			'className' => 'Categoria',
			'foreignKey' => 'categoria_id',
		)
	);

	public $hasMany = array(
		'CaracteristicasValor' => array(
			'className' => 'CaracteristicasValor',
			'foreignKey' => 'caracteristica_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
	);

}
?>