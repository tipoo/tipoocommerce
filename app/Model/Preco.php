<?php
App::uses('AppModel', 'Model');

class Preco extends AppModel {

	public $actsAs = array('Containable');

	public $belongsTo = array(
		'Produto' => array(
			'className' => 'Produto',
			'foreignKey' => 'produto_id',
		)
	);

}
?>