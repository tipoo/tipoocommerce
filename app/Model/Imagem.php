<?php
App::uses('AppModel', 'Model');

class Imagem extends AppModel {

	public $actsAs = array(
		'Containable'
	);

	public $belongsTo = array(
		'Produto' => array(
			'className' => 'Produto',
			'foreignKey' => 'produto_id',
		),
		'Sku' => array(
			'className' => 'Sku',
			'foreignKey' => 'sku_id',
		)
	);

}
?>