<?php
App::uses('AppModel', 'Model');

class Banner extends AppModel {

	public $actsAs = array(
		'CustomTime' => array(
			'date' => array(),
			'datetime' => array('data_inicio', 'data_fim')
		),
		'Containable'
	);

	public $belongsTo = array(
		'BannerTipo' => array(
			'className' => 'BannerTipo',
			'foreignKey' => 'banner_tipo_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

}
?>