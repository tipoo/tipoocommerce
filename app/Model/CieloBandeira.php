<?php
App::uses('AppModel', 'Model');

class CieloBandeira extends AppModel {

	public $actsAs = array('Containable');
	
	public $displayField = 'descricao';

	public $hasMany = array(
		'Parcela' => array(
			'className' => 'Parcela',
			'foreignKey' => 'cielo_bandeira_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
?>