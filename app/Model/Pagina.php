<?php
App::uses('AppModel', 'Model');

class Pagina extends AppModel {

	public $actsAs = array('Containable');

	public $belongsTo = array(
		'Slug' => array(
			'className' => 'Slug',
			'foreignKey' => 'slug_id',
		)
	);

	public $hasMany = array(
		'PaginasProduto' => array(
			'className' => 'PaginasProduto',
			'foreignKey' => 'pagina_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'ColecoesLocal' => array(
			'className' => 'ColecoesLocal',
			'foreignKey' => 'pagina_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'HtmlsLocal' => array(
			'className' => 'HtmlsLocal',
			'foreignKey' => 'pagina_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
	);

}
?>