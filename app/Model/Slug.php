<?php
App::uses('AppModel', 'Model');

class Slug extends AppModel {

	public $actsAs = array('Containable');

	public $hasMany = array(
		'Pagina' => array(
			'className' => 'Pagina',
			'foreignKey' => 'slug_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
?>