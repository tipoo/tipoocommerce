<?php
App::uses('AppModel', 'Model');

class HtmlsLocal extends AppModel {

	public $actsAs = array(
		'Containable'
	);

	public $belongsTo = array(
		'Pagina' => array(
			'className' => 'Pagina',
			'foreignKey' => 'pagina_id',
		),
		'Categoria' => array(
			'className' => 'Categoria',
			'foreignKey' => 'categoria_id',
		),
		'Marca' => array(
			'className' => 'Marca',
			'foreignKey' => 'marca_id',
		),
		'Produto' => array(
			'className' => 'Produto',
			'foreignKey' => 'produto_id',
		)
	);

	public function get($opts = array()) {
		$padrao = array(
			'pagina_id' => null,
			'categoria_id' => null,
			'produto_id' => null,
			'marca_id' => null
		);

		$opts = array_merge($padrao, $opts);

		$htmls = $this->find('all', array(
			'contain' => false,
			'conditions' => array(
				'HtmlsLocal.categoria_id' => $opts['categoria_id'],
				'HtmlsLocal.pagina_id' => $opts['pagina_id'],
				'HtmlsLocal.produto_id' => $opts['produto_id'],
				'HtmlsLocal.marca_id' => $opts['marca_id'],
				'HtmlsLocal.ativo' => true
			),
			'order' => array(
				'HtmlsLocal.id' => 'DESC'
			)
		));

		return $htmls;
	}
}
?>