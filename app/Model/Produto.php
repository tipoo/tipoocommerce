<?php
App::uses('AppModel', 'Model');

class Produto extends AppModel {

	public $displayField = 'descricao';

	public $actsAs = array('Containable');

	public $virtualFields = array(
		'economize' => 'Produto.preco_de - Produto.preco_por'
	);

	public $belongsTo = array(
		'Slug' => array(
			'className' => 'Slug',
			'foreignKey' => 'slug_id',
		),
		'Categoria' => array(
			'className' => 'Categoria',
			'foreignKey' => 'categoria_id',
		),
		'Marca' => array(
			'className' => 'Marca',
			'foreignKey' => 'marca_id',
		)
	);

	public $hasMany = array(
		'CaracteristicasValoresSelecionado' => array(
			'className' => 'CaracteristicasValoresSelecionado',
			'foreignKey' => 'produto_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Imagem' => array(
			'className' => 'Imagem',
			'foreignKey' => 'produto_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Sku' => array(
			'className' => 'Sku',
			'foreignKey' => 'produto_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'ColecoesLocal' => array(
			'className' => 'ColecoesLocal',
			'foreignKey' => 'produto_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'HtmlsLocal' => array(
			'className' => 'HtmlsLocal',
			'foreignKey' => 'produto_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'ColecoesProduto' => array(
			'className' => 'ColecoesProduto',
			'foreignKey' => 'produto_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'TagsProduto' => array(
			'className' => 'TagsProduto',
			'foreignKey' => 'produto_id',
			'dependent' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'CompreJuntoPromocao' => array(
			'className' => 'CompreJuntoPromocao',
			'foreignKey' => 'produto_id',
			'dependent' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
	);

	public $hasAndBelongsToMany = array(
		'Servico' => array(
			'className' => 'Servico',
			'joinTable' => 'servicos_produtos',
			'foreignKey' => 'produto_id',
			'associationForeignKey' => 'servico_id',
			'with' => 'ServicosProduto',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => ''
		)
	);
}
?>
