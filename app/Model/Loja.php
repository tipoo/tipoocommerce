<?php
App::uses('AppModel', 'Model');

class Loja extends AppModel {

	public $actsAs = array('Containable');

	public $belongsTo = array(
		'Cidade' => array(
			'className' => 'Cidade',
			'foreignKey' => 'cidade_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),

	);

}
?>