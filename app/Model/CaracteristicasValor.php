<?php
App::uses('AppModel', 'Model');

class CaracteristicasValor extends AppModel {

	public $actsAs = array('Containable');

	public $belongsTo = array(
		'Caracteristica' => array(
			'className' => 'Caracteristica',
			'foreignKey' => 'caracteristica_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
?>