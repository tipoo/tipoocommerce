<?php
App::uses('AppModel', 'Model');

class Promocao extends AppModel {

	public $actsAs = array('Containable');

	public $hasOne = array('PromocaoCausa', 'PromocaoEfeito');

	public $hasMany = array(
		'PromocoesFreteTransportadora' => array(
			'className' => 'PromocoesFreteTransportadora',
			'foreignKey' => 'promocao_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
?>