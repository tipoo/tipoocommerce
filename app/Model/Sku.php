<?php
App::uses('AppModel', 'Model');

class Sku extends AppModel {

	public $actsAs = array(
		'Containable',
		'CakePtbr.AjusteData' => array('previsao_entrega_pre_venda'),
	);

	public $virtualFields = array(
		'previsao_entrega_pre_venda' => 'IF(Sku.permite_pre_venda = true AND Sku.previsao_entrega_pre_venda IS NULL, date(now()+interval Sku.carencia_pre_venda day), Sku.previsao_entrega_pre_venda)'
	);

	public $belongsTo = array(
		'Produto' => array(
			'className' => 'Produto',
			'foreignKey' => 'produto_id',
		)
	);

	public $hasMany = array(
		'CaracteristicasValoresSelecionado' => array(
			'className' => 'CaracteristicasValoresSelecionado',
			'foreignKey' => 'sku_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Imagem' => array(
			'className' => 'Imagem',
			'foreignKey' => 'sku_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
}
?>