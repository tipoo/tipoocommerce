<?php
App::uses('AppModel', 'Model');

class Pedido extends AppModel {

	public $actsAs = array(
		'Containable',
		'CustomTime' => array(
			'datetime' => array('data_hora', 'data_hora_pagamento')
		)
	);

	public $belongsTo = array(
		'Endereco' => array(
			'className' => 'Endereco',
			'foreignKey' => 'endereco_entrega_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'EnderecoEntrega' => array(
			'className' => 'Endereco',
			'foreignKey' => 'endereco_entrega_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'EnderecoCobranca' => array(
			'className' => 'Endereco',
			'foreignKey' => 'endereco_entrega_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Cliente' => array(
			'className' => 'Cliente',
			'foreignKey' => 'cliente_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'StatusPedido' => array(
			'className' => 'StatusPedido',
			'foreignKey' => 'status_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'FreteTransportadora' => array(
			'className' => 'FreteTransportadora',
			'foreignKey' => 'frete_transportadora_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),

	);

	public $hasMany = array(
		'PedidoSku' => array(
			'className' => 'PedidoSku',
			'foreignKey' => 'pedido_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Promocao' => array(
			'className' => 'Promocao',
			'foreignKey' => 'pedido_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

	public function get($id) {
		$pedido = $this->find('first', array(
			'contain' => array(
				'StatusPedido',
				'PedidoSku' => array(
					'PedidoCaracteristica',
					'CompreJuntoPromocao',
					'Promocao',
					'Sku.Produto.Imagem'
				),
				'Cliente',
				'EnderecoCobranca' => array(
					'Cidade.Estado'
				),
				'EnderecoEntrega' => array(
					'Cidade.Estado'
				),
				'FreteTransportadora',
				'Promocao' => array(
					'PromocaoCausa',
					'PromocaoEfeito'
				)
			),
			'conditions' => array(
				'Pedido.id' => $id
			)
		));

		return $pedido;
	}
}
?>