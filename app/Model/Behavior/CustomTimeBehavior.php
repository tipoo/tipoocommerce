<?php
/**
 * 	var $actsAs = array(
 *		'CustomTime' => array(
 *			'date' => array(),
 *			'datetime' => array()
 *		),
 *  );
 */

App::import('Behavior', 'CakePtbr.AjusteData');
class CustomTimeBehavior extends AjusteDataBehavior {

	public $camposDateTime;

	public function setup(Model $model, $config = array()) {
		parent::setup($model, isset($config['date']) ? $config['date'] : null);

		if (empty($config) || !isset($config['datetime'])) {
			// Caso não seja informado os campos datetime, ele irá buscar no schema
			$this->camposDateTime[$model->name] = $this->_buscaCamposDateTime($model);
		} elseif (!is_array($config)) {
			$this->camposDateTime[$model->name] = array($config);
		} else {
			$this->camposDateTime[$model->name] = $config['datetime'];
		}
	}

	function beforeSave(Model $model, $config = array()) {
		parent::beforeSave($model); // Ajusta campos do tipo date
		$this->_ajustarDataHora($model);
		return true;
	}

	function _ajustarDataHora($model) {
		$data = $model->data[$model->name];
		foreach ($this->camposDateTime[$model->name] as $campo) {
			if (isset($data[$campo])) {
				$nova_data = str_replace('/', '', $data[$campo]);
				$nova_data = str_replace(':', '', $nova_data);
				$nova_data = str_replace(' ', '', $nova_data);

				$dia = substr($nova_data, 0, 2);
				$mes = substr($nova_data, 2, 2);
				$ano = substr($nova_data, 4, 4);
				$hora = substr($nova_data, 8, 2);
				$minuto = substr($nova_data, 10, 2);
				if (strlen($nova_data) > 12) {
					$segundo = substr($nova_data, 12, 2);
				} else {
					$segundo = '00';
				}

				$model->data[$model->name][$campo] = "$ano-$mes-$dia $hora:$minuto:$segundo";

			}
		}
	}

	function _buscaCamposDateTime($model) {
		if (!is_array($model->_schema)) {
			return array();
		}
		$saida = array();
		foreach ($model->_schema as $campo => $infos) {
			if ($infos['type'] == 'datetime' && !in_array($campo, array('created', 'updated', 'modified'))) {
				$saida[] = $campo;
			}
		}
		return $saida;
	}

}
?>