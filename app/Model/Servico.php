<?php
App::uses('AppModel', 'Model');

class Servico extends AppModel {

	public $actsAs = array(
		'Containable'
	);

	public $hasMany = array(
		'ServicoAnexo' => array(
			'className' => 'ServicoAnexo',
			'foreignKey' => 'servico_id',
			'dependent' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
?>