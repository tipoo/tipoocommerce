<?php
App::uses('AppModel', 'Model');

class PedidoCaracteristica extends AppModel {

	public $actsAs = array(
		'Containable'
	);

	public $belongsTo = array(
		'PedidoSku' => array(
			'className' => 'PedidoSku',
			'foreignKey' => 'pedido_sku_id',
		)
	);
}
?>