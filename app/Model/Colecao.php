<?php
App::uses('AppModel', 'Model');

class Colecao extends AppModel {

	public $actsAs = array('Containable');

	public $hasMany = array(
		'ColecoesProduto' => array(
			'className' => 'ColecoesProduto',
			'foreignKey' => 'colecao_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'ColecoesLocal' => array(
			'className' => 'ColecoesLocal',
			'foreignKey' => 'colecao_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
?>