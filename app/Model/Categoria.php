<?php
App::uses('AppModel', 'Model');

class Categoria extends AppModel {

	public $actsAs = array('Containable');

	public $displayField = 'descricao';

	public $belongsTo = array(
		'Slug' => array(
			'className' => 'Slug',
			'foreignKey' => 'slug_id',
		),
		'CategoriaPai' => array(
			'className' => 'Categoria',
			'foreignKey' => 'categoria_pai_id',
		)
	);

	public $hasMany = array(
		'Caracteristica' => array(
			'className' => 'Caracteristica',
			'foreignKey' => 'categoria_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'ColecoesLocal' => array(
			'className' => 'ColecoesLocal',
			'foreignKey' => 'categoria_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'HtmlsLocal' => array(
			'className' => 'HtmlsLocal',
			'foreignKey' => 'categoria_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'CategoriaFilha' => array(
			'className' => 'Categoria',
			'foreignKey' => 'categoria_pai_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
}
?>