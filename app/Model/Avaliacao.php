<?php
App::uses('AppModel', 'Model');

class Avaliacao extends AppModel {

	public $actsAs = array(
		'Containable'
	);

	public $belongsTo = array(
		'Produto' => array(
			'className' => 'Produto',
			'foreignKey' => 'produto_id',
		),
	);



}
?>