<?php
App::uses('AppModel', 'Model');

class AviseMe extends AppModel {

	public $actsAs = array(
		'Containable'
	);

	public $useTable = 'avise_me';

	public $belongsTo = array(
		'Sku' => array(
			'className' => 'Sku',
			'foreignKey' => 'sku_id',
		),
	);

}
?>