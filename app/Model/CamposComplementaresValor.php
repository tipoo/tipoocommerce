<?php
App::uses('AppModel', 'Model');

class CamposComplementaresValor extends AppModel {

	public $actsAs = array('Containable');

	public $belongsTo = array(
		'CamposComplementar' => array(
			'className' => 'CamposComplementar',
			'foreignKey' => 'campos_complementar_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
?>