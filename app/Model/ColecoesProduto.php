<?php
App::uses('AppModel', 'Model');

class ColecoesProduto extends AppModel {

	public $actsAs = array('Containable');

	public $belongsTo = array(
		'Colecao' => array(
			'className' => 'Colecao',
			'foreignKey' => 'colecao_id',
		),
		'Produto' => array(
			'className' => 'Produto',
			'foreignKey' => 'produto_id',
		)
	);
}
?>