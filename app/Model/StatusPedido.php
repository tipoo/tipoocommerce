<?php
App::uses('AppModel', 'Model');

class StatusPedido extends AppModel {

	public $displayField = 'descricao';

	public $actsAs = array(
		'Containable',
	);

	public $hasMany = array(
		'Pedido' => array(
			'className' => 'Pedido',
			'foreignKey' => 'status_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
}
?>