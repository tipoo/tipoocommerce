<?php
App::uses('AppModel', 'Model');

class PaginasProduto extends AppModel {

	public $actsAs = array('Containable');

	public $belongsTo = array(
		'Produto' => array(
			'className' => 'Produto',
			'foreignKey' => 'produto_id',
		),
		'Pagina' => array(
			'className' => 'Pagina',
			'foreignKey' => 'pagina_id',
		)
	);
}
?>