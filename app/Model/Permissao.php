<?php
App::uses('AppModel', 'Model');

class Permissao extends AppModel {
	
	public $belongsTo = array(
		'Perfil' => array(
			'className' => 'Perfil',
			'foreignKey' => 'perfil_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Objeto' => array(
			'className' => 'Objeto',
			'foreignKey' => 'objeto_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
	
}
?>