<?php
App::uses('AppModel', 'Model');

class Configuracao extends AppModel {

	private $configuracao;

	public function get($chave, $recarregar = false) {
		if (!$this->configuracao || $recarregar) {
			$this->configuracao = $this->find('first');
		}

		return $this->configuracao['Configuracao'][$chave];
	}

}
?>