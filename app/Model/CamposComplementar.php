<?php
App::uses('AppModel', 'Model');

class CamposComplementar extends AppModel {

	public $actsAs = array('Containable');

	public $hasMany = array(
		'CamposComplementaresValor' => array(
			'className' => 'CamposComplementaresValor',
			'foreignKey' => 'campos_complementar_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
	);
}
?>