<?php
App::uses('AppModel', 'Model');

class ServicoAnexo extends AppModel {

	public $actsAs = array(
		'Containable'
	);

	public $belongsTo = array(
		'Servico' => array(
			'className' => 'Servico',
			'foreignKey' => 'servico_id',
		)
	);

}
?>