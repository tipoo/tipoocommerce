<?php
App::uses('AppModel', 'Model');

class Marca extends AppModel {

	public $actsAs = array('Containable');

	public $displayField = 'descricao';

	public $belongsTo = array(
		'Slug' => array(
			'className' => 'Slug',
			'foreignKey' => 'slug_id',
		)
	);

	public $hasMany = array(
		'ColecoesLocal' => array(
			'className' => 'ColecoesLocal',
			'foreignKey' => 'marca_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'HtmlsLocal' => array(
			'className' => 'HtmlsLocal',
			'foreignKey' => 'marca_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
	);
}
?>