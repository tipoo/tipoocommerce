<?php
App::uses('AppModel', 'Model');

class FreteValor extends AppModel {

	public $actsAs = array('Containable');

	public $virtualFields = array(
		'prazo_carencia' => 'FreteValor.prazo'
	);

	public $belongsTo = array(
		'FreteTransportadora' => array(
			'className' => 'FreteTransportadora',
			'foreignKey' => 'frete_transportadora_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

}
?>