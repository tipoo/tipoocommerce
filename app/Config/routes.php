<?php
	// WebService
	Router::parseExtensions('json');

	// Rota das páginas raízes /
	Router::connect('/', array('controller' => 'catalogo', 'action' => 'index'));
	Router::connect('/admin', array('controller' => 'usuarios', 'action' => 'login', 'admin' => true));

	// Rotas personalizadas
	Router::connect('/busca', array('controller' => 'catalogo', 'action' => 'busca'));

	// Rotas com Slug personalizado e dinâmico. Rotas para webservice.
	App::uses('CustomRoute', 'Routing/Route');
	Router::connect(
		'/*',
		array('controller' => ':controller', 'action' => ':action'),
		array('routeClass' => 'CustomRoute')
	);

	Router::connect('/robots.txt', array('controller' => 'configuracoes', 'action' => 'robots'));

	Router::connect('/sitemap.xml', array('controller' => 'configuracoes', 'action' => 'sitemap'));
	Router::connect('/sitemap-categorias.xml', array('controller' => 'configuracoes', 'action' => 'sitemap_categorias'));
	Router::connect('/sitemap-marcas.xml', array('controller' => 'configuracoes', 'action' => 'sitemap_marcas'));
	Router::connect('/sitemap-produtos.xml', array('controller' => 'configuracoes', 'action' => 'sitemap_produtos'));
	Router::connect('/sitemap-tags.xml', array('controller' => 'configuracoes', 'action' => 'sitemap_tags'));
	Router::connect('/sitemap-paginas.xml', array('controller' => 'configuracoes', 'action' => 'sitemap_paginas'));

/**
 * Load all plugin routes.  See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
	CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
	require CAKE . 'Config' . DS . 'routes.php';
