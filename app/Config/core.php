<?php
App::uses('Mobile_Detect', 'Vendor/MobileDetect');

/**
 * Customizações TipooCommerce
 */
define('FLASH_SUCCESS', 'Mensagens/flash_success');
define('FLASH_ERROR', 'Mensagens/flash_error');
define('FLASH_INFO', 'Mensagens/flash_info');
define('FLASH_WARNING', 'Mensagens/flash_warning');
define('AMBIENTE_PRODUCAO', 'AmbienteProducao');
define('AMBIENTE_DEV', 'AmbienteDesenvolvimento');

// echo $_SERVER['SERVER_NAME'];
// exit();

if (strpos($_SERVER['SERVER_NAME'], "m.penseavanti.com.br") !== FALSE ||
	strpos($_SERVER['SERVER_NAME'], "avantimobile.tipoocommerce.com.br") !== FALSE) {
	/* ATENÇÃO: NUNCA ALTERAR AS CONSTANTES ABAIXO*/
	define('PROJETO', 'avantimobile');
	define('AMBIENTE', AMBIENTE_PRODUCAO);

	// Verificando se está acessando o blog. Caso esteja redireciona para o subdominio
	$uri = $_SERVER['REQUEST_URI'];
	if (strpos($uri, '/blog') === 0) {
		$url = 'http://blog.penseavanti.com.br';
		$uri = str_replace('/blog', '', $uri);
		if (strpos($uri, '/') !== 0) {
				$url .= '/';
		}
		$url .= $uri;

		header("HTTP/1.1 301 Moved Permanently");
		header("Location: " . $url);
		exit();
	}

} else if (strpos($_SERVER['SERVER_NAME'], "penseavanti.com.br") !== FALSE ||
	strpos($_SERVER['SERVER_NAME'], "avanti.tipoocommerce.com.br") !== FALSE) {

	$detect = new Mobile_Detect();
	if($detect->isMobile() && !$detect->isTablet()){
		header("Location: http://m.penseavanti.com.br");
	}

	/* ATENÇÃO: NUNCA ALTERAR AS CONSTANTES ABAIXO*/
	define('PROJETO', 'avanti');
	define('AMBIENTE', AMBIENTE_PRODUCAO);

	// Verificando se está acessando o blog. Caso esteja redireciona para o subdominio
	$uri = $_SERVER['REQUEST_URI'];
	if (strpos($uri, '/blog') === 0) {
		$url = 'http://blog.penseavanti.com.br';
		$uri = str_replace('/blog', '', $uri);
		if (strpos($uri, '/') !== 0) {
				$url .= '/';
		}
		$url .= $uri;

		header("HTTP/1.1 301 Moved Permanently");
		header("Location: " . $url);
		exit();
	}

} else if (strpos($_SERVER['SERVER_NAME'], "carfashion.com.br") !== FALSE ||
	strpos($_SERVER['SERVER_NAME'], "carfashion.tipoocommerce.com.br") !== FALSE) {
	/* ATENÇÃO: NUNCA ALTERAR AS CONSTANTES ABAIXO*/
	define('PROJETO', 'carfashion');
	define('AMBIENTE', AMBIENTE_PRODUCAO);

} else if (strpos($_SERVER['SERVER_NAME'], "brunasurfistinha.com.br") !== FALSE ||
	strpos($_SERVER['SERVER_NAME'], "brunasurfistinha.tipoocommerce.com.br") !== FALSE) {
	/* ATENÇÃO: NUNCA ALTERAR AS CONSTANTES ABAIXO*/
	define('PROJETO', 'brunasurfistinha');
	define('AMBIENTE', AMBIENTE_PRODUCAO);

} else if (strpos($_SERVER['SERVER_NAME'], "cariocacalcados.com.br") !== FALSE ||
	strpos($_SERVER['SERVER_NAME'], "carioca.tipoocommerce.com.br") !== FALSE) {
	/* ATENÇÃO: NUNCA ALTERAR AS CONSTANTES ABAIXO*/
	define('PROJETO', 'carioca');
	define('AMBIENTE', AMBIENTE_PRODUCAO);

} else if (strpos($_SERVER['SERVER_NAME'], "lacelab.com.br") !== FALSE ||
	strpos($_SERVER['SERVER_NAME'], "homologacao.tipoocommerce.com.br") !== FALSE || // Homologação do novo cluster. Apagar ao finalizar
	strpos($_SERVER['SERVER_NAME'], "lacelab.tipoocommerce.com.br") !== FALSE) {
	/* ATENÇÃO: NUNCA ALTERAR AS CONSTANTES ABAIXO*/
	define('PROJETO', 'lacelab');
	define('AMBIENTE', AMBIENTE_PRODUCAO);

} else if (strpos($_SERVER['SERVER_NAME'], "lojasscdecor.com.br") !== FALSE ||
	strpos($_SERVER['SERVER_NAME'], "scdecor.tipoocommerce.com.br") !== FALSE) {
	/* ATENÇÃO: NUNCA ALTERAR AS CONSTANTES ABAIXO*/
	define('PROJETO', 'scdecor');
	define('AMBIENTE', AMBIENTE_PRODUCAO);

} else if (strpos($_SERVER['SERVER_NAME'], "midiamoveis.com.br") !== FALSE ||
	strpos($_SERVER['SERVER_NAME'], "midiamoveis.tipoocommerce.com.br") !== FALSE) {
	/* ATENÇÃO: NUNCA ALTERAR AS CONSTANTES ABAIXO*/
	define('PROJETO', 'midiamoveis');
	define('AMBIENTE', AMBIENTE_PRODUCAO);

} else if (strpos($_SERVER['SERVER_NAME'], "oticabiz.com.br") !== FALSE ||
	strpos($_SERVER['SERVER_NAME'], "oticabiz.tipoocommerce.com.br") !== FALSE) {
	/* ATENÇÃO: NUNCA ALTERAR AS CONSTANTES ABAIXO*/
	define('PROJETO', 'oticabiz');
	define('AMBIENTE', AMBIENTE_PRODUCAO);

	/* Configurações de teste cielo */
	Configure::write('Cielo',array(
		'testando' => false, #Se vai utilizar o ambiente de testes da Cielo
		'buy_page_cielo' => false,
		'loja_id' => 1054242159,
		'loja_chave' => '46d33dc20d84c0b2cdc8a1a834a20e33a72e5b947c35061cebb4cbe8bef9ca04',
		'caminho_certificado_ssl' => APP . DS . 'Plugin' . DS . 'Cielo' . DS . 'VeriSignClass3PublicPrimaryCertificationAuthority-G5.crt'
	));

} else if (strpos($_SERVER['SERVER_NAME'], "pulp.com.br") !== FALSE ||
	strpos($_SERVER['SERVER_NAME'], "pulp.tipoocommerce.com.br") !== FALSE) {
	/* ATENÇÃO: NUNCA ALTERAR AS CONSTANTES ABAIXO*/
	define('PROJETO', 'pulp');
	define('AMBIENTE', AMBIENTE_PRODUCAO);

} else if (strpos($_SERVER['SERVER_NAME'], "rebecaficinski.com.br") !== FALSE ||
	strpos($_SERVER['SERVER_NAME'], "rebecaficinski.tipoocommerce.com.br") !== FALSE) {
	/* ATENÇÃO: NUNCA ALTERAR AS CONSTANTES ABAIXO*/
	define('PROJETO', 'rebecaficinski');
	define('AMBIENTE', AMBIENTE_PRODUCAO);

} else if (strpos($_SERVER['SERVER_NAME'], "rockerbaby.com.br") !== FALSE ||
	strpos($_SERVER['SERVER_NAME'], "rockerbaby.tipoocommerce.com.br") !== FALSE) {
	/* ATENÇÃO: NUNCA ALTERAR AS CONSTANTES ABAIXO*/
	define('PROJETO', 'rockerbaby');
	define('AMBIENTE', AMBIENTE_PRODUCAO);

} else if (strpos($_SERVER['SERVER_NAME'], "sorellina.com.br") !== FALSE ||
	strpos($_SERVER['SERVER_NAME'], "sorellina.tipoocommerce.com.br") !== FALSE) {
	/* ATENÇÃO: NUNCA ALTERAR AS CONSTANTES ABAIXO*/
	define('PROJETO', 'sorellina');
	define('AMBIENTE', AMBIENTE_PRODUCAO);

} else if (strpos($_SERVER['SERVER_NAME'], "tipoocommerce.com.br") !== FALSE ||
	strpos($_SERVER['SERVER_NAME'], "tipoo.com.br") !== FALSE) {
	/* ATENÇÃO: NUNCA ALTERAR AS CONSTANTES ABAIXO*/
	define('PROJETO', 'tipoocommerce');
	define('AMBIENTE', AMBIENTE_PRODUCAO);

	/* Configurações de teste cielo */
	Configure::write('Cielo',array(
		'testando' => true, #Se vai utilizar o ambiente de testes da Cielo
		'buy_page_cielo' => false,
		'loja_id' => 1006993069,
		'loja_chave' => '25fbb99741c739dd84d7b06ec78c9bac718838630f30b112d033ce2e621b34f3',
		'caminho_certificado_ssl' => APP . DS . 'Plugin' . DS . 'Cielo' . DS . 'VeriSignClass3PublicPrimaryCertificationAuthority-G5.crt'
	));

} else if (strpos($_SERVER['SERVER_NAME'], "vindadopapa.com.br") !== FALSE ||
			strpos($_SERVER['SERVER_NAME'], "visitadopapa.com.br") !== FALSE) {
	/* ATENÇÃO: NUNCA ALTERAR AS CONSTANTES ABAIXO*/
	define('PROJETO', 'vindadopapa');
	define('AMBIENTE', AMBIENTE_PRODUCAO);

	/* Configurações de produção Cielo */
	Configure::write('Cielo',array(
		'testando' => false, #Se vai utilizar o ambiente de testes da Cielo
		'buy_page_cielo' => false,
		'loja_id' => 1017009845,
		'loja_chave' => 'f7a7fef41fbef215cfc9d5d6c4a9f104b0cbf33114b77037e307f9304ee5aec6',
		'caminho_certificado_ssl' => APP . DS . 'Plugin' . DS . 'Cielo' . DS . 'VeriSignClass3PublicPrimaryCertificationAuthority-G5.crt'
	));

} else {
	define('PROJETO', 'tipoo'); //CONFIGURAR AQUI O NOME DO PROJETO PARA O AMBIENTE DE TESTES
	define('AMBIENTE', AMBIENTE_DEV);
	define('SQL_DUMP', false);

	/* Configurações de teste cielo */
	Configure::write('Cielo',array(
		'testando' => true, #Se vai utilizar o ambiente de testes da Cielo
		'buy_page_cielo' => false,
		'loja_id' => 1006993069,
		'loja_chave' => '25fbb99741c739dd84d7b06ec78c9bac718838630f30b112d033ce2e621b34f3',
		'caminho_certificado_ssl' => APP . DS . 'Plugin' . DS . 'Cielo' . DS . 'VeriSignClass3PublicPrimaryCertificationAuthority-G5.crt'
	));
}

define('PROJETO_DIR', APP."Projetos".DS.PROJETO.DS);
define('PROJETO_DIR_CONFIG', PROJETO_DIR."Config".DS);



/**
 * This is core configuration file.
 *
 * Use it to configure core behavior of Cake.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

/**
 * CakePHP Debug Level:
 *
 * Production Mode:
 * 	0: No error messages, errors, or warnings shown. Flash messages redirect.
 *
 * Development Mode:
 * 	1: Errors and warnings shown, model caches refreshed, flash messages halted.
 * 	2: As in 1, but also with full debug messages and SQL output.
 *
 * In production mode, flash messages redirect after a time interval.
 * In development mode, you need to click the flash message to continue.
 */
	Configure::write('debug', 2);

/**
 * Configure the Error handler used to handle errors for your application.  By default
 * ErrorHandler::handleError() is used.  It will display errors using Debugger, when debug > 0
 * and log errors with CakeLog when debug = 0.
 *
 * Options:
 *
 * - `handler` - callback - The callback to handle errors. You can set this to any callable type,
 *   including anonymous functions.
 *   Make sure you add App::uses('MyHandler', 'Error'); when using a custom handler class
 * - `level` - int - The level of errors you are interested in capturing.
 * - `trace` - boolean - Include stack traces for errors in log files.
 *
 * @see ErrorHandler for more information on error handling and configuration.
 */
	Configure::write('Error', array(
		'handler' => 'ErrorHandler::handleError',
		'level' => E_ALL & ~E_DEPRECATED,
		'trace' => true
	));

/**
 * Configure the Exception handler used for uncaught exceptions.  By default,
 * ErrorHandler::handleException() is used. It will display a HTML page for the exception, and
 * while debug > 0, framework errors like Missing Controller will be displayed.  When debug = 0,
 * framework errors will be coerced into generic HTTP errors.
 *
 * Options:
 *
 * - `handler` - callback - The callback to handle exceptions. You can set this to any callback type,
 *   including anonymous functions.
 *   Make sure you add App::uses('MyHandler', 'Error'); when using a custom handler class
 * - `renderer` - string - The class responsible for rendering uncaught exceptions.  If you choose a custom class you
 *   should place the file for that class in app/Lib/Error. This class needs to implement a render method.
 * - `log` - boolean - Should Exceptions be logged?
 *
 * @see ErrorHandler for more information on exception handling and configuration.
 */
	Configure::write('Exception', array(
		'handler' => 'ErrorHandler::handleException',
		'renderer' => 'ExceptionRenderer',
		'log' => true
	));

/**
 * Application wide charset encoding
 */
	Configure::write('App.encoding', 'UTF-8');

/**
 * To configure CakePHP *not* to use mod_rewrite and to
 * use CakePHP pretty URLs, remove these .htaccess
 * files:
 *
 * /.htaccess
 * /app/.htaccess
 * /app/webroot/.htaccess
 *
 * And uncomment the App.baseUrl below:
 */
	//Configure::write('App.baseUrl', env('SCRIPT_NAME'));

/**
 * Uncomment the define below to use CakePHP prefix routes.
 *
 * The value of the define determines the names of the routes
 * and their associated controller actions:
 *
 * Set to an array of prefixes you want to use in your application. Use for
 * admin or other prefixed routes.
 *
 * 	Routing.prefixes = array('admin', 'manager');
 *
 * Enables:
 *	`admin_index()` and `/admin/controller/index`
 *	`manager_index()` and `/manager/controller/index`
 *
 */
	Configure::write('Routing.prefixes', array('admin'));

/**
 * Turn off all caching application-wide.
 *
 */
	//Configure::write('Cache.disable', true);

/**
 * Enable cache checking.
 *
 * If set to true, for view caching you must still use the controller
 * public $cacheAction inside your controllers to define caching settings.
 * You can either set it controller-wide by setting public $cacheAction = true,
 * or in each action using $this->cacheAction = true.
 *
 */
	//Configure::write('Cache.check', true);

/**
 * Enable cache view prefixes.
 *
 * If set it will be prepended to the cache name for view file caching. This is
 * helpful if you deploy the same application via multiple subdomains and languages,
 * for instance. Each version can then have its own view cache namespace.
 * Note: The final cache file name will then be `prefix_cachefilename`.
 */
	//Configure::write('Cache.viewPrefix', 'prefix');

/**
 * Defines the default error type when using the log() function. Used for
 * differentiating error logging and debugging. Currently PHP supports LOG_DEBUG.
 */
	define('LOG_ERROR', LOG_ERR);

/**
 * Session configuration.
 *
 * Contains an array of settings to use for session configuration. The defaults key is
 * used to define a default preset to use for sessions, any settings declared here will override
 * the settings of the default config.
 *
 * ## Options
 *
 * - `Session.cookie` - The name of the cookie to use. Defaults to 'CAKEPHP'
 * - `Session.timeout` - The number of minutes you want sessions to live for. This timeout is handled by CakePHP
 * - `Session.cookieTimeout` - The number of minutes you want session cookies to live for.
 * - `Session.checkAgent` - Do you want the user agent to be checked when starting sessions? You might want to set the
 *    value to false, when dealing with older versions of IE, Chrome Frame or certain web-browsing devices and AJAX
 * - `Session.defaults` - The default configuration set to use as a basis for your session.
 *    There are four builtins: php, cake, cache, database.
 * - `Session.handler` - Can be used to enable a custom session handler.  Expects an array of of callables,
 *    that can be used with `session_save_handler`.  Using this option will automatically add `session.save_handler`
 *    to the ini array.
 * - `Session.autoRegenerate` - Enabling this setting, turns on automatic renewal of sessions, and
 *    sessionids that change frequently. See CakeSession::$requestCountdown.
 * - `Session.ini` - An associative array of additional ini values to set.
 *
 * The built in defaults are:
 *
 * - 'php' - Uses settings defined in your php.ini.
 * - 'cake' - Saves session files in CakePHP's /tmp directory.
 * - 'database' - Uses CakePHP's database sessions.
 * - 'cache' - Use the Cache class to save sessions.
 *
 * To define a custom session handler, save it at /app/Model/Datasource/Session/<name>.php.
 * Make sure the class implements `CakeSessionHandlerInterface` and set Session.handler to <name>
 *
 * To use database sessions, run the app/Config/Schema/sessions.php schema using
 * the cake shell command: cake schema create Sessions
 *
 */

	// SESSIONS USANDO BANCO DE DADOS
	/*Configure::write('Session', array(
		'defaults' => 'database',
		'cookie' => 'tipoocommerce_' . PROJETO,
		'ini' => array(
			'session.cookie_secure' => false
		)
	));*/

	// SESSIONS USANDO CONFIGURAÇÕES DO PHP
	Configure::write('Session', array(
		'defaults' => 'php',
		'cookie' => 'tipoocommerce_' . PROJETO,
		'ini' => array(
			'session.cookie_secure' => false
		)
	));

	/*// SESSIONS USANDO CONFIGURAÇÕES DO PHP
	Configure::write('Security.cookie', 'tipoocommerce_' . PROJETO);
	Configure::write('Session.cookie', 'tipoocommerce_' . PROJETO);
	Configure::write('Session.cookieTimeout', 0);
	Configure::write('Session.checkAgent', false);
	Configure::write('Session.cookie_secure',false);
	Configure::write('Session.referer_check' ,false);
	Configure::write('Session.defaults', 'php');*/

/**
 * A random string used in security hashing methods.
 */
	Configure::write('Security.salt', '93bgDYqyJVoUuhGuWwvn0bfIxfs2iR2G0FgaC9mi');

/**
 * A random numeric string (digits only) used to encrypt/decrypt strings.
 */
	Configure::write('Security.cipherSeed', '46859796574535302364574949668');

/**
 * Apply timestamps with the last modified time to static assets (js, css, images).
 * Will append a querystring parameter containing the time the file was modified. This is
 * useful for invalidating browser caches.
 *
 * Set to `true` to apply timestamps when debug > 0. Set to 'force' to always enable
 * timestamping regardless of debug value.
 */
	//Configure::write('Asset.timestamp', true);

/**
 * Compress CSS output by removing comments, whitespace, repeating tags, etc.
 * This requires a/var/cache directory to be writable by the web server for caching.
 * and /vendors/csspp/csspp.php
 *
 * To use, prefix the CSS link URL with '/ccss/' instead of '/css/' or use HtmlHelper::css().
 */
	//Configure::write('Asset.filter.css', 'css.php');

/**
 * Plug in your own custom JavaScript compressor by dropping a script in your webroot to handle the
 * output, and setting the config below to the name of the script.
 *
 * To use, prefix your JavaScript link URLs with '/cjs/' instead of '/js/' or use JavaScriptHelper::link().
 */
	//Configure::write('Asset.filter.js', 'custom_javascript_output_filter.php');

/**
 * The classname and database used in CakePHP's
 * access control lists.
 */
	Configure::write('Acl.classname', 'DbAcl');
	Configure::write('Acl.database', 'default');

/**
 * Uncomment this line and correct your server timezone to fix
 * any date & time related errors.
 */
	//date_default_timezone_set('UTC');

/**
 * Configure the cache handlers that CakePHP will use for internal
 * metadata like class maps, and model schema.
 *
 * By default File is used, but for improved performance you should use APC.
 *
 * Note: 'default' and other application caches should be configured in app/Config/bootstrap.php.
 *       Please check the comments in boostrap.php for more info on the cache engines available
 *       and their setttings.
 */
$engine = 'File';

// In development mode, caches should expire quickly.
$duration = '+999 days';
if (Configure::read('debug') >= 1) {
	$duration = '+10 seconds';
}

// Prefix each application on the same server with a different string, to avoid Memcache and APC conflicts.
$prefix = 'myapp_';

/**
 * Configure the cache used for general framework caching.  Path information,
 * object listings, and translation cache files are stored with this configuration.
 */
Cache::config('_cake_core_', array(
	'engine' => $engine,
	'prefix' => $prefix . 'cake_core_',
	'path' => CACHE . 'persistent' . DS,
	'serialize' => ($engine === 'File'),
	'duration' => $duration
));

/**
 * Configure the cache for model and datasource caches.  This cache configuration
 * is used to store schema descriptions, and table listings in connections.
 */
Cache::config('_cake_model_', array(
	'engine' => $engine,
	'prefix' => $prefix . 'cake_model_',
	'path' => CACHE . 'models' . DS,
	'serialize' => ($engine === 'File'),
	'duration' => $duration
));
