var bootstrapValidateOptions = {
	ignore: '',
	errorElement: 'span',
	errorClass: 'help-inline',
	highlight: function(element, errorClass, validClass) {
		$(element).addClass('error');
		$(element).parents('.form-group').addClass('has-error').addClass('text-danger');
	},
	unhighlight: function(element, errorClass, validClass) {
		$(element).removeClass('error');
		$(element).parents('.form-group').removeClass('has-error').removeClass('text-danger');
	}
}