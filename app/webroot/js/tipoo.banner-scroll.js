var bannerWindowFocus = true;
var banners_container = [];

bannerTrocar = function(banner, duracao, banner_container_n) {
	clearTimeout(banners_container[banner_container_n]['bannerTimer']);

	banners_container[banner_container_n]['bannerAtual'] = banner;
	var pos = (parseInt(banner) - 1) * $('.banner-scroll').eq(banner_container_n).width();
	$('.banner-scroll').eq(banner_container_n).find('.banner-container').scrollTo(pos, { duration: duracao });

	$('.banner-scroll').eq(banner_container_n).find('.aba-banner-atual').removeClass('aba-banner-atual');
	$('.banner-scroll').eq(banner_container_n).find('.abas[rel="' + banner + '"]').addClass('aba-banner-atual');

	bannerAgendar(banner_container_n);
};

bannerAgendar = function(banner_container_n) {

	if (bannerWindowFocus) {

		banners_container[banner_container_n]['proximo'] = parseInt(banners_container[banner_container_n]['bannerAtual']) + 1;
		var duracao = 800;

		if (banners_container[banner_container_n]['proximo'] > banners_container[banner_container_n]['qtd_banners']) {
			banners_container[banner_container_n]['proximo'] = 1;
			duracao = 300;
		}

		banners_container[banner_container_n]['bannerTimer'] = setTimeout("bannerTrocar(" + banners_container[banner_container_n]['proximo'] + ", " + duracao + ", " + banner_container_n + ")", 8000);
	}
}

bannerIniciar = function(banner_container_n) {

	banners_container[banner_container_n]['qtd_banners'] = $('.banner-scroll').eq(banner_container_n).find('.banner').length;

	$('.banner-scroll').eq(banner_container_n).find('.banner-content').css('width', (banners_container[banner_container_n]['qtd_banners'] * 100) + '%');
	$('.banner-scroll').eq(banner_container_n).find('.banner').css('width', (100 / banners_container[banner_container_n]['qtd_banners']) + '%');

	$('.banner-scroll').eq(banner_container_n).find('.banner-nav a.abas').on('click', function() {
		bannerTrocar($(this).attr('rel'), 800, banner_container_n);
		return false;
	});

	$('.banner-scroll').eq(banner_container_n).find('.banner-arrow a.arrow').on('click', function() {
		var duracao = 800;

		if ($(this).attr('rel') == "-") {
			banners_container[banner_container_n]['proximo'] = parseInt(banners_container[banner_container_n]['bannerAtual']) - 1;
			if (banners_container[banner_container_n]['proximo'] < 1) {
				banners_container[banner_container_n]['proximo'] = banners_container[banner_container_n]['qtd_banners'];
				duracao = 800;
			}
		} else {
			banners_container[banner_container_n]['proximo'] = parseInt(banners_container[banner_container_n]['bannerAtual']) + 1;
			if (banners_container[banner_container_n]['proximo'] > banners_container[banner_container_n]['qtd_banners']) {
				banners_container[banner_container_n]['proximo'] = 1;
				duracao = 800;
			}
		}

		bannerTrocar(banners_container[banner_container_n]['proximo'], duracao, banner_container_n);
		return false;
	});
}

$(document).ready(function() {

	$('.banner-scroll').each(function(e) {
		banners_container[e] = '';
		banners_container[e] = [];

		banners_container[e]['bannerAtual'] = 1;
		banners_container[e]['bannerTimer'];
		banners_container[e]['qtd_banners'];

		bannerIniciar(e);
		bannerAgendar(e);
	});

	$(window).focus(function() {
		bannerWindowFocus = true;

		$('.banner-scroll').each(function(e) {
			bannerAgendar(e);
		});

	}).blur(function() {
		bannerWindowFocus = false;
	});

	$('.banner-container').scrollTo(0);

});