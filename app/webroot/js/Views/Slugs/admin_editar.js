$(document).ready(function() {
	$('#SlugAdminEditarForm').validate(bootstrapValidateOptions);

	$("#SlugController").rules("add", {
		required: true
	});

	$("#SlugAction").rules("add", {
		required: true
	});

	$("#SlugCustomParserClass").rules("add", {
		required: true
	});

});