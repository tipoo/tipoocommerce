$(document).ready(function() {

	$("#FiltrarBuscaSku").click(function() {

		var sku = $('#FiltroSku').val();

		var url = WEBROOT + 'admin/relatorios/estoque/';

		if (sku != '') {
			url += 'sku:' + sku + '/';
		}

		document.location.href = url;

	});

	$("#FiltrarBuscaProduto").click(function() {

		var produto = $('#FiltroProduto').val();

		var url = WEBROOT + 'admin/relatorios/estoque/';

		if (produto != '') {
			url += 'produto:' + produto + '/';
		}

		document.location.href = url;

	});

	$("#baixar-csv").click(function(e) {
		e.preventDefault();

		var sku = $('#FiltroSku').val();
		var produto = $('#FiltroProduto').val();

		var url = WEBROOT + 'admin/relatorios/baixar_relatorio_estoque/';

		if (sku != '') {
			url += 'sku:' + sku + '/';
		}

		if (produto != '') {
			url += 'produto:' + produto + '/';
		}

		document.location.href = url;

	});

	$("#btnLimpar").click(function(e) {
		var url = WEBROOT + 'admin/relatorios/estoque/';
		document.location.href = url;

	});


});