$(document).ready(function() {
	$('#CaracteristicaAdminAdicionarForm').validate(bootstrapValidateOptions);

	$("#CaracteristicaReferente").rules("add", {
		required: true
	});

	$("#CaracteristicaDescricao").rules("add", {
		required: true
	});

	$("#CaracteristicaTipo").rules("add", {
		required: true
	});

	$('.campo-mascara').hide();

	$('#CaracteristicaMascara').closest('.control-group').hide();
	$('#CaracteristicaTipo').change(function() {
		if ($(this).val() == 'N') {
			$('.campo-mascara').show();
			$('#CaracteristicaMascara').closest('.control-group').show();
			$("#CaracteristicaMascara").rules("add", {
				required: true
			});
		} else {
			$('.campo-mascara').hide();
			$('#CaracteristicaMascara').val('');
			$('#CaracteristicaMascara').closest('.control-group').hide();
			$("#CaracteristicaMascara").rules("add", {
				required: false
			});
		}
	});

});