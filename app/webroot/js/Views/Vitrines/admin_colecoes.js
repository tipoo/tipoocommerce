$(document).ready(function() {

	// <---------- Adicionar Vitrine de Coleção ------------->

	$('.adicionar-vitrine-colecao').click(function(e) {
		var url = $(this).attr('href');
		var chave = $(this).closest('table').find('.descricao-chave').text();
		e.preventDefault();

		$.ajax({
			url: url,
			type: 'GET',
			success: function(data){

				bootbox.dialog({
					message:data,
					title: 'Adicionar Vitrine de Coleção - ' + chave,
					buttons: {
						main: {
							label: 'Fechar X',
							className: 'btn-danger'
						}
					}
				});

			},
			error: function(data){
				alert ('Ocorreu um erro ao tentar ao tentar buscar os dados da coleção.');
			}
		});
	});

	// <---------- Editar Vitrine de Coleção ------------->

	$('.editar-vitrine-colecao').click(function(e) {
		var url = $(this).attr('href');
		var chave = $(this).closest('table').find('.descricao-chave').text();
		e.preventDefault();

		$.ajax({
			url: url,
			type: 'GET',
			success: function(data) {

				bootbox.dialog({
					message:data,
					title: 'Editar Vitrine de Coleção - ' + chave,
					buttons: {
						main: {
							label: 'Fechar X',
							className: 'btn-danger'
						}
					}
				});
			},
			error: function(data){
				alert ('Ocorreu um erro ao tentar ao tentar buscar os dados da coleção.');
			}
		});
	});

	$(this).bind("contextmenu",function(e){
		return false;
	});

	$('.editar-vitrine-colecao').mousedown(function(e){

		if( e.button == 2 ) {
			var id_vitrine = $('.modal_').data('id');
			$(this).trigger("click");

		  return false;

		}
		return true;
	});

});