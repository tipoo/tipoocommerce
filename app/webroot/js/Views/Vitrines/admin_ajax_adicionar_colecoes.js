$(document).ready(function() {

	$('#ColecoesLocalAdminAjaxAdicionarColecoesForm').validate(bootstrapValidateOptions);

	$("#ColecoesLocalColecaoId").rules("add", {
		required: true,
		messages: {
			required: 'A coleção escolhida não existe.'
		}
	});

	$("#ColecoesLocalDescricao").rules("add", {
		required: true
	});

	$("#ColecoesLocalColecao").rules("add", {
		required: true
	});

	$("#ColecoesLocalDataInicio").rules("add", {
		required: true
	});
	$('#ColecoesLocalQtdMaxProdutos').numeric({decimal : false, negative: false});

	$('#ColecoesLocalDataInicio').mask('99/99/9999 99:99:99');
	$('#ColecoesLocalDataFim').mask('99/99/9999 99:99:99');

	// <---------- Autocomplete para adicionar novos colecoes a coleção ------------->
	$('.typeahead').typeahead({
		name: 'produtos',
		remote: WEBROOT + 'admin/vitrines/ajax_buscar_colecoes/%QUERY',
		template: '<strong>{{descricao}}</strong> - #{{id}}',
		engine: Hogan
	}).on('typeahead:selected', function (obj, datum) {
		$("#ColecoesLocalColecaoId").val( datum.id );
	});
});