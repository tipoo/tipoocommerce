$(document).ready(function() {

	$('#FreteValorAdminImportarForm').validate(bootstrapValidateOptions);

	$("#FreteValorArquivoCsv").rules("add", {
		required: true,
		accept: 'csv',
		messages: {
			accept: 'O arquivo deve estar em formato csv.'
		}
	});


});