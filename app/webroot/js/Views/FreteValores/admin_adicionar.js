$(document).ready(function() {
	$('#FreteValorAdminAdicionarForm').validate(bootstrapValidateOptions);

	$('#FreteValorCepInicio').rules("add", {
		required: true
	});
	$('#FreteValorCepFim').rules("add", {
		required: true
	});
	$('#FreteValorPesoInicio').rules("add", {
		required: true
	});
	$('#FreteValorPesoFim').rules("add", {
		required: true
	});
	$('#FreteValorValor').rules("add", {
		required: true
	});
	$('#FreteValorPrazo').rules("add", {
		required: true
	});

	$('#FreteValorPrazo').numeric({decimal : ","});

	$('#FreteValorPesoInicio').numeric({decimal : ","});

	$('#FreteValorPesoFim').numeric({decimal : ","});

	$('#FreteValorCepInicio').mask('99999-999');

	$('#FreteValorCepFim').mask('99999-999');

	$('#FreteValorValor').priceFormat({
		prefix: '',
		clearPrefix: true,
		centsSeparator: ',',
		thousandsSeparator: '.'
	});

	$('#FreteValorValor').blur(function() {
		if ($(this).val() == '0,00') {
			$(this).val('');
		}
	});

});