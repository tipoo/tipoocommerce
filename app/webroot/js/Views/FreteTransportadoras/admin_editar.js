$(document).ready(function() {
	$('#FreteTransportadoraAdminEditarForm').validate(bootstrapValidateOptions);

	$('#FreteTransportadoraDescricao').rules("add", {
		required: true
	});
	$('#FreteTransportadoraFreteTipo').rules("add", {
		required: true
	});

	$('#FreteTransportadoraPrazoCarencia').rules("add", {
		required: true
	});

	$('#FreteTransportadoraPrazoCarencia').numeric({decimal : ","});

	$('#FreteTransportadoraFreteTipo').on('change', function(){

		var tipo_servico = $(this).val();

		if(tipo_servico == 'C') {
			$('.codigo-servico-correio').show();
			$('.lojas').hide();
		} else if (tipo_servico == 'T') {
			$('.codigo-servico-correio').hide();
			$('.lojas').show();
		} else {
			$('.codigo-servico-correio').hide();
			$('.lojas').hide();
		}

	});

	$('#FreteTransportadoraCepOrigem').mask('99999-999');


	$('#FreteTransportadoraAdminEditarForm').on('submit', function() {
		if ($('#FreteTransportadoraAdminEditarForm').valid() && $('#FreteTransportadoraFreteTipo').val() == 'T') {
			if (!$('.lojas input[type="checkbox"]').is(':checked')) {
				$('.btn-action-save-default').button('reset');
				alert('Você deve selecionar no mínimo uma loja.')
				return false;
			}
		}
	});

});