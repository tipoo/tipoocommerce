$(document).ready(function() {
	$('#ImagemAdminEditarForm').validate(bootstrapValidateOptions);

	$("#ImagemImagem").rules("add", {
		accept: 'jpg|jpeg',
		messages: {
			accept: 'A imagem deve ser jpg ou jpeg.'
		}
	});

});