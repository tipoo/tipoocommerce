$(document).ready(function() {
	$('#ImagemAdminAdicionarForm').validate(bootstrapValidateOptions);

	$("#ImagemImagem").rules("add", {
		required: true,
		accept: 'jpg|jpeg',
		messages: {
			accept: 'A imagem deve ser jpg ou jpeg.'
		}
	});

});