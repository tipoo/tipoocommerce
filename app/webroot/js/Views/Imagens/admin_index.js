$(document).ready(function() {

	$(".generic-modal").click(function(e) {
		e.preventDefault();
		var srcValue = $(this).attr('href');
		var descricao = $(this).attr('rel');

		bootbox.dialog({
			message:'<img class="img-responsive" src="' + srcValue + '" alt=""/>',
			title: descricao,
			buttons: {
				main: {
					label: 'Fechar X',
					className: 'btn-danger'
				}
			}
		});
	});

	var fixHelperModified = function(e, tr) {
		var $originals = tr.children();
		var $helper = tr.clone();
		$helper.children().each(function(index)
		{
			$(this).width($originals.eq(index).width())
		});
		return $helper;
	};

	$("#sortable").sortable({
		cursor: "move",
		helper: fixHelperModified,
		axis: "y",
		key: "sort",
		opacity: 0.6,
		tolerance: 'pointer',
		stop: function( event, ui ) {

			var referente_id = $('#referente_id').val();
			var referente = $('#referente').val();

			var url = WEBROOT + 'admin/imagens/ajax_ordenar/' + referente_id + '/referente:' +referente+ '/';

			$('#sortable .ui-state-default').each(function(e) {
				var ordem = e + 1;
				var id_img = $(this).data('id');

				url += 'ordem_' + ordem + ':' + id_img +'/';
			});

			$.ajax({
				url: url,
				type: 'POST',
				success: function(data) {

					var retorno = jQuery.parseJSON(data);

					if (!retorno.sucesso) {
						alert(retorno.mensagem);
					}
				},
				error: function(data){
					alert('Ocorreu um erro ao tentar ordenar as imagens. Por favor, tente novamente.');
				}
			});
		}
	}).disableSelection();

	$('.ordenacao-desce').on('click', function() {

		var referente_id = $('#referente_id').val();
		var referente = $('#referente').val();

	  	var $row = $(this).closest('tr');

		$row.next().after($row);

		var url = WEBROOT + 'admin/imagens/ajax_ordenar/' + referente_id + '/referente:' +referente+ '/';

		$('#sortable .ui-state-default').each(function(e) {
			var ordem = e + 1;
			var id_produto = $(this).data('id');

			url += 'ordem_' + ordem + ':' + id_produto +'/';

		});

		$.ajax({
			url: url,
			type: 'POST',
			success: function(data) {

				var retorno = jQuery.parseJSON(data);

				if (!retorno.sucesso) {
					alert(retorno.mensagem);
				}
			},
			error: function(data){
				alert('Ocorreu um erro ao tentar ordenar as Imagens. Por favor, tente novamente.');
			}
		});

	});

	$('.ordenacao-sobe').on('click', function() {

		var referente_id = $('#referente_id').val();
		var referente = $('#referente').val();

	  	var $row = $(this).closest('tr');

		$row.prev().before($row);

		var url = WEBROOT + 'admin/imagens/ajax_ordenar/' + referente_id + '/referente:' +referente+ '/';

		$('#sortable .ui-state-default').each(function(e) {
			var ordem = e + 1;
			var id_produto = $(this).data('id');

			url += 'ordem_' + ordem + ':' + id_produto +'/';
			//console.log(url);

		});

		$.ajax({
			url: url,
			type: 'POST',
			success: function(data) {

				var retorno = jQuery.parseJSON(data);

				if (!retorno.sucesso) {
					alert(retorno.mensagem);
				}
			},
			error: function(data){
				alert('Ocorreu um erro ao tentar ordenar as Imagens. Por favor, tente novamente.');
			}
		});

	});

	if ($('.seta-ordenacao').is((':visible'))) {
		$('.alert-info').hide();
		$('.info-imagem-principal').show();

	} else {
		$('.alert-info').show();
		$('.info-imagem-principal').hide();
	}

	$(window).resize(function() {

		if ($('.seta-ordenacao').is((':visible'))) {
			$('.alert-info').hide();
			$('.info-imagem-principal').show();

		} else {
			$('.alert-info').show();
			$('.info-imagem-principal').hide();
		}

	});

});