$(document).ready(function() {
	$('#ConfiguracaoAdminEditarForm').validate(bootstrapValidateOptions);


	if ($('#ConfiguracaoParcParcelas').length) {

		$("#ConfiguracaoParcParcelas").rules("add", {
			required: true
		});

		$("#ConfiguracaoParcValorMinParcela").rules("add", {
			required: true
		});

		$("#ConfiguracaoParcJuros").rules("add", {
			required: true
		});

		$("#ConfiguracaoParcValorMinParcela").priceFormat({
			prefix: '',
			clearPrefix: true,
			centsSeparator: ',',
			thousandsSeparator: '.'
		});

		$("#ConfiguracaoParcJuros").priceFormat({
			prefix: '',
			clearPrefix: true,
			centsSeparator: ',',
			thousandsSeparator: '.'
		});

		$('#ConfiguracaoParcParcelas').numeric({decimal : false, negative: false});

		$('#ConfiguracaoParcValorMinParcela').numeric({decimal : ","});

		$('#ConfiguracaoParcJuros').numeric({decimal : ","});

	}


	if ($('#ConfiguracaoImgThumbW').length) {

		$("#ConfiguracaoImgThumbW").rules("add", {
			required: true
		});

		$("#ConfiguracaoImgThumbH").rules("add", {
			required: true
		});

		$("#ConfiguracaoImgSmallW").rules("add", {
			required: true
		});

		$("#ConfiguracaoImgSmallH").rules("add", {
			required: true
		});

		$("#ConfiguracaoImgNormalW").rules("add", {
			required: true
		});

		$("#ConfiguracaoImgNormalH").rules("add", {
			required: true
		});

		$("#ConfiguracaoImgMediumW").rules("add", {
			required: true
		});

		$("#ConfiguracaoImgMediumH").rules("add", {
			required: true
		});

		$("#ConfiguracaoImgBigW").rules("add", {
			required: true
		});

		$("#ConfiguracaoImgBigH").rules("add", {
			required: true
		});

		$("#ConfiguracaoImgCarrinho").rules("add", {
			required: true
		});

		$('#ConfiguracaoImgThumbW').numeric({decimal : false, negative: false});
		$('#ConfiguracaoImgThumbH').numeric({decimal : false, negative: false});
		$('#ConfiguracaoImgSmallW').numeric({decimal : false, negative: false});
		$('#ConfiguracaoImgSmallH').numeric({decimal : false, negative: false});
		$('#ConfiguracaoImgNormalW').numeric({decimal : false, negative: false});
		$('#ConfiguracaoImgNormalH').numeric({decimal : false, negative: false});
		$('#ConfiguracaoImgMediumW').numeric({decimal : false, negative: false});
		$('#ConfiguracaoImgMediumH').numeric({decimal : false, negative: false});
		$('#ConfiguracaoImgBigW').numeric({decimal : false, negative: false});
		$('#ConfiguracaoImgBigH').numeric({decimal : false, negative: false});


	 	$('#ConfiguracaoImgThumbW').blur(function() {
			if ($(this).val() == '0') {
				$(this).val('');
			}
		});

	 	$('#ConfiguracaoImgThumbH').blur(function() {
			if ($(this).val() == '0') {
				$(this).val('');
			}
		});

	 	$('#ConfiguracaoImgSmallW').blur(function() {
			if ($(this).val() == '0') {
				$(this).val('');
			}
		});

	 	$('#ConfiguracaoImgSmallH').blur(function() {
			if ($(this).val() == '0') {
				$(this).val('');
			}
		});

	 	$('#ConfiguracaoImgNormalW').blur(function() {
			if ($(this).val() == '0') {
				$(this).val('');
			}
		});

	 	$('#ConfiguracaoImgNormalH').blur(function() {
			if ($(this).val() == '0') {
				$(this).val('');
			}
		});
		 	$('#ConfiguracaoImgMediumW').blur(function() {
			if ($(this).val() == '0') {
				$(this).val('');
			}
		});

	 	$('#ConfiguracaoImgMediumH').blur(function() {
			if ($(this).val() == '0') {
				$(this).val('');
			}
		});
		 	$('#ConfiguracaoImgBigW').blur(function() {
			if ($(this).val() == '0') {
				$(this).val('');
			}
		});

	 	$('#ConfiguracaoImgBigH').blur(function() {
			if ($(this).val() == '0') {
				$(this).val('');
			}
		});

	}




	if ($('#ConfiguracaoFreteCepOrigem').length) {

		$("#ConfiguracaoFreteCepOrigem").rules("add", {
			required: true
		});

		$('#ConfiguracaoFreteCepOrigem').mask('99999-999');

	}



	if ($('#ConfiguracaoGoogleAnalytics').length) {

		$("#ConfiguracaoGoogleAnalytics").rules("add", {
				required: true
		});

	}



	if ($('#ConfiguracaoMetaDescription').length) {

		$("#ConfiguracaoMetaDescription").rules("add", {
			required: true
		});

		var num_de_caracteres = $('#ConfiguracaoMetaDescription').val().length;

		$('#num_de_caracteres').text('Nº de caracteres:' + (-num_de_caracteres+160));

		if (num_de_caracteres > 160) {
			$('#num_de_caracteres').css('color', 'red');

		} else {
			$('#num_de_caracteres').css('color', 'black');
		}

		$('#ConfiguracaoMetaDescription').keyup(function(event){
			var num_de_caracteres = $(this).val().length;

			$('#num_de_caracteres').text('Nº de caracteres:' + (-num_de_caracteres+160));

			if (num_de_caracteres > 160) {
				$('#num_de_caracteres').css('color', 'red');

			} else {
				$('#num_de_caracteres').css('color', 'black');
			}
		});

	}

	if ($('#ConfiguracaoMetaKeywords').length) {

		$("#ConfiguracaoMetaKeywords").rules("add", {
			required: true
		});

	}


	if ($('#ConfiguracaoApiPass').length) {
		
		$("#ConfiguracaoApiPass").rules("add", {
			required: true
		});

	}



	if ($('#ConfiguracaoApiUser').length) {

		$("#ConfiguracaoApiUser").rules("add", {
			required: true
		});

	}

});