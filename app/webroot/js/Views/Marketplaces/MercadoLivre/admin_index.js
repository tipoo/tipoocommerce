$(document).ready(function() {
	// <---------- Autocomplete para adicionar a coleção ------------->
	$('.typeahead').typeahead({
		name: 'produtos',
		remote: WEBROOT + 'admin/vitrines/ajax_buscar_colecoes/%QUERY',
		template: '<strong>{{descricao}}</strong> - #{{id}}',
		engine: Hogan
	}).on('typeahead:selected', function (obj, datum) {
		$("#ConfiguracaoMercadoLivreColecaoId").val( datum.id );
	});

	$('.btn-integrar-mercado-livre').on('click', function(ev) {
		$(this).button('loading');
	});
});