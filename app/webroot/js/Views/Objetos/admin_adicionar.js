$(document).ready(function() {
	$('#ObjetoAdminAdicionarForm').validate(bootstrapValidateOptions);

	$("#ObjetoController").rules("add", {
		required: true
	});

	$("#ObjetoAction").rules("add", {
		required: true
	});

});