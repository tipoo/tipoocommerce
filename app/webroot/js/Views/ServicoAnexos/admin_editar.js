$(document).ready(function() {
	$('#ServicoAnexoAdminEditarForm').validate(bootstrapValidateOptions);

	$('#ServicoAnexoNome').rules("add", {
		required: true
	});

	$('#ServicoAnexoTipo').rules("add", {
		required: true
	});

	$('#ServicoAnexoValoresPossiveis').rules("add", {
		required: true
	});;
});