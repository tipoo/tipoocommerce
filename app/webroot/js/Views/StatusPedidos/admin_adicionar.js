$(document).ready(function() {
	$('#StatusPedidoAdminAdicionarForm').validate(bootstrapValidateOptions);

	$("#StatusPedidoDescricao").rules("add", {
		required: true
	});

	$("#StatusPedidoEmailLayout").rules("add", {
		required: function() { return $('#StatusPedidoEmailNotificaCliente').prop('checked');}
	});

	$("#StatusPedidoEmailTemplate").rules("add", {
		required: function() { return $('#StatusPedidoEmailNotificaCliente').prop('checked');}
	});

});