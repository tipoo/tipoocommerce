$(document).ready(function() {

	var fixHelperModified = function(e, tr) {
		var $originals = tr.children();
		var $helper = tr.clone();
		$helper.children().each(function(index)
		{
			$(this).width($originals.eq(index).width())
		});
		return $helper;
	};

	$("#sortable").sortable({
		cursor: "move",
		helper: fixHelperModified,
		axis: "y",
		key: "sort",
		opacity: 0.6,
		tolerance: 'pointer',
		stop: function( event, ui ) {

			var url = WEBROOT + 'admin/statusPedidos/ajax_ordenar/';

			$('#sortable .ui-state-default').each(function(e) {
				var ordem = e + 1;
				var status_pedido_id = $(this).data('id');

				url += 'ordem_' + ordem + ':' + status_pedido_id +'/';
			});

			$.ajax({
				url: url,
				type: 'POST',
				success: function(data) {

					var retorno = jQuery.parseJSON(data);

					if (!retorno.sucesso) {
						alert(retorno.mensagem);
					}
				},
				error: function(data){
					alert('Ocorreu um erro ao tentar ordenar as skus. Por favor, tente novamente.');
				}
			});
		}

	}).disableSelection();

	$('.ordenacao-desce').on('click', function() {

	  	var $row = $(this).closest('tr');

		$row.next().after($row);

		var url = WEBROOT + 'admin/statusPedidos/ajax_ordenar/';

		$('#sortable .ui-state-default').each(function(e) {
			var ordem = e + 1;
			var status_pedido_id = $(this).data('id');

			url += 'ordem_' + ordem + ':' + status_pedido_id +'/';

		});

		$.ajax({
			url: url,
			type: 'POST',
			success: function(data) {

				var retorno = jQuery.parseJSON(data);

				if (!retorno.sucesso) {
					alert(retorno.mensagem);
				}
			},
			error: function(data){
				alert('Ocorreu um erro ao tentar ordenar os Skus. Por favor, tente novamente.');
			}
		});

	});

	$('.ordenacao-sobe').on('click', function() {

	  	var $row = $(this).closest('tr');

		$row.prev().before($row);

		var url = WEBROOT + 'admin/statusPedidos/ajax_ordenar/';

		$('#sortable .ui-state-default').each(function(e) {
			var ordem = e + 1;
			var status_pedido_id = $(this).data('id');

			url += 'ordem_' + ordem + ':' + status_pedido_id +'/';

		});

		$.ajax({
			url: url,
			type: 'POST',
			success: function(data) {

				var retorno = jQuery.parseJSON(data);

				if (!retorno.sucesso) {
					alert(retorno.mensagem);
				}
			},
			error: function(data){
				alert('Ocorreu um erro ao tentar ordenar os Skus. Por favor, tente novamente.');
			}
		});

	});

	if ($('.seta-ordenacao').is((':visible'))) {
			$('.alert-info').hide();

	} else {
		$('.alert-info').show();
	}

	$(window).resize(function() {

		if ($('.seta-ordenacao').is((':visible'))) {
			$('.alert-info').hide();

		} else {
			$('.alert-info').show();
		}

	});

});
