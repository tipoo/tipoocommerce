$(document).ready(function() {

	$("#btnFiltrar").click(function() {

		var categoria_id = $('#FiltroCategoriaId').val();
		var marca_id = $('#FiltroMarcaId').val();
		var nome = $('#FiltroNome').val();

		var url = WEBROOT + 'admin/produtos/index/';

		if (categoria_id != '') {
			url += 'categoria_id:' + categoria_id + '/';
		}

		if (marca_id != '') {
			url += 'marca_id:' + marca_id + '/';
		}

		if (nome != '') {
			url += 'nome:' + nome + '/';
		}

		document.location.href = url;

	});

	$("#btnLimpar").click(function() {

		var url = WEBROOT + 'admin/produtos/index/';
		document.location.href = url;

	});

	$("#FiltrarIdProduto").click(function() {

		var produto_id = $('#FiltrarIdProdutoValue').val();

		var url = WEBROOT + 'admin/produtos/index/';

		if (produto_id != '') {
			url += produto_id;
		}

		document.location.href = url;

	});

	$(".generic-modal").click(function(e) {
		e.preventDefault();
		var srcValue = $(this).attr('href');
		var descricao = $(this).attr('rel');

		bootbox.dialog({
			message: '<img class="img-responsive" src="' + srcValue + '" alt=""/>',
			title: descricao,
			buttons: {
				main: {
					label: 'Fechar X',
					className: 'btn-danger'
				}
			}
		});
	});

	$('.ver-detalhes-produto').click(function(e) {
		e.preventDefault();
		var url = $(this).attr('href');
		var descricao = 'Detalhes do Produto';
		$.ajax({
			url: url,
			type: 'GET',
			beforeSend: function() {

			},
			success: function(data) {
				bootbox.dialog({
					message:data,
					title: descricao,
					buttons: {
						main: {
							label: 'Fechar X',
							className: 'btn-danger'
						}
					}
				});
			},
			error: function(data){
				alert ('erro...');

			}
		});

	});
});