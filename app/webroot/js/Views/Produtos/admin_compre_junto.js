$(document).ready(function() {

	$('button[rel="tooltip"]').tooltip();

	var tipo_desconto = $('#CompreJuntoPromocaoTipoValor'). val();

	if(tipo_desconto == 'P') {

		 $('#CompreJuntoPromocaoValor').numeric({decimal : false, negative: false});

	} else {

		$("#CompreJuntoPromocaoValor").priceFormat({
			prefix: '',
			clearPrefix: true,
			centsSeparator: ',',
			thousandsSeparator: '.'
		});
	}



	$("#CompreJuntoPromocaoTipoValor").on('change', function() {

		$('#CompreJuntoPromocaoValor'). val('');

		 var tipo_desconto = $(this). val();

		 if(tipo_desconto == 'P') {

			$('#CompreJuntoPromocaoValor').unpriceFormat();
			$('#CompreJuntoPromocaoValor').numeric({decimal : false, negative: false});

		 } else {

		 	$('#CompreJuntoPromocaoValor').priceFormat({
				prefix: '',
				clearPrefix: true,
				centsSeparator: ',',
				thousandsSeparator: '.'
			});

			$('#CompreJuntoPromocaoValor').blur(function() {
				if ($(this).val() == '0,00') {
					$(this).val('');
				}
			});
		 }
	});

	$('.typeahead').typeahead({
		name: 'produtos',
		remote: WEBROOT + 'admin/produtos/ajax_buscar_produtos/%QUERY',
		template: '<strong>{{descricao}}</strong> - {{marca}} - #{{id}}',
		engine: Hogan
	}).on('typeahead:selected', function (obj, datum) {

		var produto_principal_id = $("#AfinidadeProdutoProdutoPrincipalId").val();

		var produto_secundario_id = datum.id;

		var url = WEBROOT + 'admin/produtos/ajax_adicionar_produto_compre_junto/' + produto_principal_id + '/' + produto_secundario_id;


			$.ajax({
				url: url,
				type: 'POST',
				async: false,
				success: function(data) {
					var retorno = jQuery.parseJSON(data);

					if (retorno.sucesso) {

						$(retorno.produto_associado).each(function(e) {

							$('.produtos-selecionados').append('<p>' + datum.value + '<button value=" '+ this.AfinidadeProduto.id + '" type="button" class="close excluir-produto" data-dismiss="alert" >&times;</button></p>');

						});


					} else	{
						alert(retorno.mensagem);
					}

				},
				error: function(data){
					alert('Ocorreu um erro ao tentar adicionar o produto ao "Compre Junto". Por favor, tente novamente.');
				}
			});


		$("#AfinidadeProdutoDescricao").val('');

	});

	$(document).on('click','.excluir-produto', function(e){
		e.preventDefault();

		var id_associado = $(this).val();

		var url = WEBROOT + 'admin/produtos/ajax_excluir_produto_compre_junto/' + id_associado;

		$.ajax({
			url: url,
			type: 'POST',
			async: false,
			success: function(data) {
				var retorno = jQuery.parseJSON(data);

				if (!retorno.sucesso) {

					alert(retorno.mensagem);

				}

			},
			error: function(data){
				alert('Ocorreu um erro ao tentar excluir esse produto do "Compre Junto". Por favor, tente novamente.');
			}
		});
		$("#AfinidadeProdutoDescricao").val('');

	});



	 $('#AdicionarValorDesconto').on('click', function(){

		var tipo_valor = $('#CompreJuntoPromocaoTipoValor').val();
		var valor = $('#CompreJuntoPromocaoValor').val();
		var produto_id = $('#AfinidadeProdutoProdutoPrincipalId').val();

		if (valor != '') {

			var url = WEBROOT + 'admin/produtos/ajax_adicionar_desconto_compre_junto/' + tipo_valor + '/' + valor + '/' + produto_id;

			$.ajax({
				url: url,
				type: 'POST',
				async: false,
				success: function(data) {
					var retorno = jQuery.parseJSON(data);

						if (retorno.sucesso) {

							$(retorno.desconto_associado).each(function(e) {

								if (this.CompreJuntoPromocao.tipo_valor == 'P') {

									$('.descontos-selecionados').append('<p> ' + valor + ' %<button value=" '+ this.CompreJuntoPromocao.id + '" type="button" class="close excluir-desconto" data-dismiss="alert">&times;</button></p>');

								} else {

									$('.descontos-selecionados').append('<p> R$ ' + valor + '<button value=" '+ this.CompreJuntoPromocao.id + '" type="button" class="close excluir-desconto" data-dismiss="alert">&times;</button></p>');

								}

							});


						} else	{
							alert(retorno.mensagem);
						}

				},
				error: function(data){
					alert('Ocorreu um erro ao tentar adicionar esse desconto ao "Compre Junto". Por favor, tente novamente.');
				}
			});

			$('#CompreJuntoPromocaoValor').val('');

		}

	 });

	$(document).on('click','.excluir-desconto', function(e){
		e.preventDefault();

		var id_associado = $(this).val();

		var url = WEBROOT + 'admin/produtos/ajax_excluir_desconto_compre_junto/' + id_associado;

		$.ajax({
			url: url,
			type: 'POST',
			async: false,
			success: function(data) {
				var retorno = jQuery.parseJSON(data);

				if (!retorno.sucesso) {

					alert(retorno.mensagem);

				}

			},
			error: function(data){
				alert('Ocorreu um erro ao tentar excluir esse desconto do "Compre Junto". Por favor, tente novamente.');
			}
		});
		$('#CompreJuntoPromocaoValor').val('');

	});

});

