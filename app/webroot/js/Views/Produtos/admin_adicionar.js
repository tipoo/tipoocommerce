function ordenarLista(a,b) {
    return $.trim($(a).text()).toLowerCase() > $.trim($(b).text()).toLowerCase() ? 1 : -1;
}

function adicionarMascara() {
	// Adiciona máscara no campos numéricos
	var numero_inteiro = $('.mascara-inteiro').length;
	if (numero_inteiro) {
		$('.mascara-inteiro').each(function() {
			$(this).numeric({
				decimal : false,
				negative: false
			});
		});
	}

	var numero_decimal = $('.mascara-decimal').length;
	if (numero_decimal) {
		$('.mascara-decimal').each(function() {
			$(this).priceFormat({
				prefix: '',
				clearPrefix: true,
				centsSeparator: ',',
				thousandsSeparator: '.'
			});
		});

		$('.mascara-decimal').blur(function() {
			if ($(this).val() == '0,00') {
				$(this).val('');
			}
		});
	}
}

$(document).ready(function() {
	$('#ProdutoAdminAdicionarForm').validate(bootstrapValidateOptions);

	$("#ProdutoCodigoDeReferencia").rules("add", {
		required: true
	});

	$("#ProdutoDescricao").rules("add", {
		required: true
	});

	$("#ProdutoCategoriaId").rules("add", {
		required: true
	});

	$("#ProdutoMarcaId").rules("add", {
		required: true
	});

	if ($("#ProdutoNcm").length) {
		$("#ProdutoNcm").rules("add", {
			required: true
		});
	}

	if ($("#ProdutoOrigem").length) {
		$("#ProdutoOrigem").rules("add", {
			required: true
		});
	}
	
	$("#Imagem0Imagem").rules("add", {
		//required: true,
		accept: 'jpg|jpeg',
		messages: {
			accept: 'A imagem deve ser jpg ou jpeg.'
		}
	});

	$('#ProdutoDesconto').numeric({decimal : ","});
	$("#ProdutoNcm").mask("9999.99.99");

	$('#ProdutoMetaDescription').keyup(function(event){
		var num_de_caracteres = $(this).val().length;

		$('#num_de_caracteres').text('Nº de caracteres:' + (-num_de_caracteres+160));

		if (num_de_caracteres > 160) {
			$('#num_de_caracteres').css('color', 'red');

		} else {
			$('#num_de_caracteres').css('color', 'black');
		}
	});

	adicionarMascara();

	$('#ProdutoPrecoDe').priceFormat({
		prefix: '',
		clearPrefix: true,
		centsSeparator: ',',
		thousandsSeparator: '.'
	});

	$('#ProdutoPrecoDe').blur(function() {
		if ($(this).val() == '0,00') {
			$(this).val('');
		}
	});

	$("#ProdutoPrecoPor").rules("add", {
		required: true
	});

	$('#ProdutoPrecoPor').priceFormat({
		prefix: '',
		clearPrefix: true,
		centsSeparator: ',',
		thousandsSeparator: '.'
	});

	$('#ProdutoCategoriaId').change(function() {

		var categoria_id = $(this).val();
		var contador = $('#contador').val();

		$.ajax({
			url: WEBROOT + 'admin/produtos/ajax_carregar_caracteristicas/' + categoria_id + '/contador:' + contador,
			type: 'POST',
			beforeSend: function() {
				$('#caracteristicasCategoria').html('<div class="controls well text-center"><img class="carregando" src="' + WEBROOT + 'img/ajax-loader.gif" /></div>');
			},
			success: function(data) {

				$('#caracteristicasCategoria').html(data);

				adicionarMascara();

			},
			error: function(data){
				alert ('Ocorreu um erro ao tentar consultar as características. Por favor, tente novamente.');
				$('.carregando').remove();
			}
		});
	});

	$('#AdicionarTag').on('click', function(){

		var nome_tag = $('#NovaTag').val();

		var url = WEBROOT + 'admin/tags/ajax_adicionar/' + nome_tag ;

		$.ajax({
			url: url,
			type: 'POST',
			success: function(data) {
				var retorno = jQuery.parseJSON(data);

				if (retorno.sucesso) {

					$(retorno.nova_tag).each(function(e) {

					    var newLi = $('<li> <input type="checkbox" name="data[TagsProduto][][tag_id]"  value="' + this.Tag.id + '"checked/> '+ this.Tag.nome  +'</li>').hide();
						$('li','.listar-tags').add(newLi.fadeIn(800)).sort(ordenarLista).appendTo('.listar-tags');

						$('.panel-body').scrollTo($('input[value="'+this.Tag.id+'"]'), 500, {over:{top:-5}});

						$('#NovaTag').val('');
						$('.input-tag').hide();

					});

				} else {
					$('#NovaTag').val('');
					alert(retorno.mensagem);
				}

			},
			error: function(data){
				alert('Ocorreu um erro ao tentar adicionar a tag. Por favor, tente novamente.');
			}
		});


	});

	$('#ProdutoDetalhes').wysihtml5({
		"stylesheets": [WEBROOT + "/css/bootstrap3-wysiwyg5-color.css"],
		locale: "pt-BR",
			"font-styles": false, //Font styling, e.g. h1, h2, etc. Default true
			"emphasis": true, //Italics, bold, etc. Default true
			"lists": true, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
			"html": false, //Button which allows you to edit the generated HTML. Default false
			"link": true, //Button to insert a link. Default true
			"image": true, //Button to insert an image. Default true,
			"color": true //Button to change color of font
	});

});