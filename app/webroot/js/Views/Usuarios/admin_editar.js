$(document).ready(function() {
	$('#UsuarioAdminEditarForm').validate(bootstrapValidateOptions);

	$('#UsuarioNome').rules("add", {
		required: true
	});

	$('#UsuarioEmail').rules("add", {
		required: true
	});

	$("#UsuarioPerfilId").rules("add", {
		required: true
	});

});