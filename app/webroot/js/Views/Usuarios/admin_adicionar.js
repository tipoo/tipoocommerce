$(document).ready(function() {
	$('#UsuarioAdminAdicionarForm').validate(bootstrapValidateOptions);

	$('#UsuarioNome').rules("add", {
		required: true
	});

	$('#UsuarioEmail').rules("add", {
		required: true
	});

	$("#UsuarioSenha").rules("add", {
		required: true,
		minlength: 4
	});

	$("#UsuarioPerfilId").rules("add", {
		required: true
	});

});