$(document).ready(function() {

	$('#UsuarioAdminAlterarSenhaForm').validate(bootstrapValidateOptions);

	$("#UsuarioSenhaAtual").rules("add", {
		required: true
	});

	$("#UsuarioSenha").rules("add", {
		required: true
	});

	$("#UsuarioSenhaConfirmacao").rules("add", {
		required: true,
		equalTo: "#UsuarioSenha"
	});


});