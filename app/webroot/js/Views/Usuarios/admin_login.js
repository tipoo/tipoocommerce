$(document).ready(function() {
	$('#UsuarioAdminLoginForm').validate(bootstrapValidateOptions);

	$("#UsuarioEmail").rules("add", {
		required: true
	});

	$("#UsuarioSenha").rules("add", {
		required: true,
		minlength: 4
	});
});