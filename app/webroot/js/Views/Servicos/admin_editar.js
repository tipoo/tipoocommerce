$(document).ready(function() {
	$('#ServicoAdminEditarForm').validate(bootstrapValidateOptions);

	$('#ServicoNome').rules("add", {
		required: true
	});

	var numero_decimal = $('.mascara-decimal').length;
	if (numero_decimal) {
		$('.mascara-decimal').each(function() {
			$(this).priceFormat({
				prefix: '',
				clearPrefix: true,
				centsSeparator: ',',
				thousandsSeparator: '.'
			});
		});

		$('.mascara-decimal').blur(function() {
			if ($(this).val() == '0,00') {
				$(this).val('');
			}
		});
	}


});