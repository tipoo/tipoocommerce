$(document).ready(function() {

	$('.lista-servicos h3').each(function(index) {

		if ($('#ServicosOpcoesSelecionado' + index + 'ServicoSelecionado').is(':checked')) {

			$('.lista-opcoes-'+index).removeClass('hide');

			$('.lista-opcoes-'+ index+' li').each(function(index2) {

				var tipo_valor_servico = 'ServicosOpcoesSelecionado'+index+'ServicoOpcao'+index2+'Tipo';
				var valor_servico = 'ServicosOpcoesSelecionado'+index+'ServicoOpcao'+index2+'Valor';

				var tipo_valor = $('#' + tipo_valor_servico). val();

				if(tipo_valor == 'P') {

					$('#' + valor_servico).unpriceFormat();
				 	$('#' + valor_servico).numeric({decimal : false, negative: false});

				} else {

					$('#' + valor_servico).priceFormat({
						prefix: '',
						clearPrefix: true,
						centsSeparator: ',',
						thousandsSeparator: '.'
					});
				}

				$('#' + tipo_valor_servico).on('change', function() {

					$('#' + valor_servico).val('');

					var tipo_valor = $('#' + tipo_valor_servico).val();

					if(tipo_valor == 'P') {

						$('#' + valor_servico).unpriceFormat();
						$('#' + valor_servico).numeric({decimal : false, negative: false});

					} else {

						$('#' + valor_servico).priceFormat({
							prefix: '',
							clearPrefix: true,
							centsSeparator: ',',
							thousandsSeparator: '.'
						});
					}

				});

				$('#ServicosOpcoesSelecionado' + index + 'ServicoOpcao' + index2 + 'Id').on('click', function(e) {

					if ($(this).is(':checked')) {
						$('.campos-valores-' + index + '-' + index2).removeClass('hide');

					} else {
						$('.campos-valores-' + index + '-' + index2).addClass('hide');
					}

				});

			});

		} else {
			$('.lista-opcoes-'+index).addClass('hide');

			if ($('#ServicosOpcoesSelecionado' + index + 'Valores').length && $('#ServicosOpcoesSelecionado' + index + 'Valores').val() > 1) {
				$('.lista-opcoes-'+index).parent().removeClass('alert alert-danger');	
				$('.alert-text').remove();	


			} 
		}

		$('#ServicosOpcoesSelecionado'+index+'ServicoSelecionado').on('click', function(e) {

			if ($('#ServicosOpcoesSelecionado'+index+'ServicoSelecionado').is(':checked')) {

				$('.lista-opcoes-'+index).removeClass('hide');

				$('.lista-opcoes-'+ index+' li').each(function(index2) {

					var tipo_valor_servico = 'ServicosOpcoesSelecionado'+index+'ServicoOpcao'+index2+'Tipo';
					var valor_servico = 'ServicosOpcoesSelecionado'+index+'ServicoOpcao'+index2+'Valor';

					var tipo_valor = $('#' + tipo_valor_servico). val();

					if(tipo_valor == 'P') {

						$('#' + valor_servico).unpriceFormat();
					 	$('#' + valor_servico).numeric({decimal : false, negative: false});

					} else {

						$('#' + valor_servico).priceFormat({
							prefix: '',
							clearPrefix: true,
							centsSeparator: ',',
							thousandsSeparator: '.'
						});
					}

					if ($('#ServicosOpcoesSelecionado' + index + 'Valores').val() < 2) {

						$(this).find('.campos-valores-0').removeClass('hide');
					}

					$('#' + tipo_valor_servico).on('change', function() {

						$('#' + valor_servico).val('');
						var tipo_valor = $('#' + tipo_valor_servico). val();

						if(tipo_valor == 'P') {

							$('#' + valor_servico).unpriceFormat();
							$('#' + valor_servico).numeric({decimal : false, negative: false});

						} else {

							$('#' + valor_servico).priceFormat({
								prefix: '',
								clearPrefix: true,
								centsSeparator: ',',
								thousandsSeparator: '.'
							});
						}

					});

					$('#ServicosOpcoesSelecionado' + index + 'ServicoOpcao' + index2 + 'Id').on('click', function(e) {

						if ($(this).is(':checked')) {
							$('.campos-valores-' + index + '-' + index2).removeClass('hide');
						} else {
							$('.campos-valores-' + index + '-' + index2).addClass('hide');
						}

					});

				});

			} else {

				$('.lista-opcoes-'+index).addClass('hide');

				if ($('#ServicosOpcoesSelecionado' + index + 'Valores').length && $('#ServicosOpcoesSelecionado' + index + 'Valores').val() > 1) {
					$('.lista-opcoes-'+index).parent().removeClass('alert alert-danger');
					$('.alert-text').remove();		


				} 
			}
		});
			
	});

	$('.lista-servicos h3').each(function(index) {

		if ($('#ServicosOpcoesSelecionado' + index + 'Valores').length && $('#ServicosOpcoesSelecionado' + index + 'Valores').val() > 1) {

			var obrigatorio = true;
			
			$('.lista-opcoes-'+ index+' li').each(function(index2) {
				$('#ServicosOpcoesSelecionado' + index + 'ServicoOpcao' + index2 + 'Id').on('click', function(){

					if ($(this).is(':checked')) {
						$('.lista-opcoes-'+index).parent().removeClass('alert alert-danger');
						$('.alert-text').remove();
					}
				});
	
			});
		} 
	});

	$('#ServicosOpcoesSelecionadoAdminServicosProdutoForm').on('submit', function(){

		var retorno = true;
		$('.alert-text').remove();

		$('.lista-servicos h3').each(function(index) {

			if ($('#ServicosOpcoesSelecionado' + index + 'ServicoSelecionado').is(':checked')) {
					
				if ($('#ServicosOpcoesSelecionado' + index + 'Valores').length) {

					var obrigatorio = true;
					
					$('.lista-opcoes-'+ index+' li').each(function(index2) {
						if ($('#ServicosOpcoesSelecionado' + index + 'ServicoOpcao' + index2 + 'Id').is(':checked')) {

							obrigatorio  = false;
						}
					});

					if (obrigatorio && $('#ServicosOpcoesSelecionado' + index + 'Valores').val() > 1) {
						$('.lista-opcoes-'+index).parent().addClass('alert alert-danger');
						$('.lista-opcoes-'+index).after('<p class="alert-text">*Selecione ao menos uma opção.</p>');
						retorno = false;
					} else {
						$('.lista-opcoes-'+index).parent().removeClass('alert alert-danger');

						var verifica_campos = true;

						$('.lista-opcoes-'+ index+' li').each(function(index2) {

							var valor_servico = 'ServicosOpcoesSelecionado'+index+'ServicoOpcao'+index2+'Valor';
					

							$('#' + valor_servico).parents('.form-group').removeClass('has-error').removeClass('text-danger');
							$('#' + valor_servico).parents('.form-group').find('span').remove();


							if ($('#ServicosOpcoesSelecionado' + index + 'ServicoOpcao' + index2 + 'Id').is(':checked')) {

								var valor = $('#' + valor_servico).val() != '' ? true : false;

								if (!valor) {

									$('#' + valor_servico).parents('.form-group').addClass('has-error').addClass('text-danger');
									$('#' + valor_servico).parents('.form-group').after().append('<span generated="true" class="help-inline">Campo obrigatório.</span>');
									verifica_campos = false;
								} 

							}
						});

						if (!verifica_campos) {
							retorno = false;
						}		
					}
				} 
			}

		});

		$('.btn-action-save-default').button('reset');
		return retorno;

	});

});