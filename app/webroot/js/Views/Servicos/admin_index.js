$(document).ready(function() {

	$('.texto-servico-valor').on('click', function(e) {
		e.preventDefault();
		var url = $(this).attr('href');
		var descricao = 'Texto';
		$.ajax({
			url: url,
			type: 'GET',
			beforeSend: function() {

			},
			success: function(data) {
				bootbox.dialog({
					message:data,
					title: descricao,
				});
			},
			error: function(data){
				alert ('erro...');

			}
		});

	});

});