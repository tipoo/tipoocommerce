$(document).ready(function() {

	var deFiltro = $('#dp1-mask').val();
	var ateFiltro = $('#dp2-mask').val();

	deFiltro = deFiltro.split('/');
	var startDate = new Date(deFiltro[2], deFiltro[1], deFiltro[0]);

	ateFiltro = ateFiltro.split('/');
	var endDate = new Date(ateFiltro[2], ateFiltro[1], ateFiltro[0]);

	$('#dp1').datepicker({
		language: "pt-BR"
	}).on('changeDate', function(ev){

		var deFiltro = $('#dp1-mask').val();
		var ateFiltro = $('#dp2-mask').val();

		deFiltro = deFiltro.split('/');
		var dataDe = deFiltro[2] + '-' + deFiltro[1] + '-' + deFiltro[0];

		ateFiltro = ateFiltro.split('/');
		var dataAte = ateFiltro[2] + '-' + ateFiltro[1] + '-' + ateFiltro[0];

		if (dataDe > dataAte) {
			$('#alert-datepicker').removeClass('hide').find('span').text('A data inicial não pode ser maior que a data final.');

		} else {

			$('#alert-datepicker').addClass('hide');
			$('#dp1').datepicker('hide');

		}
	});

	$('#dp2').datepicker({
		language: "pt-BR"
	}).on('changeDate', function(ev){

		var deFiltro = $('#dp1-mask').val();
		var ateFiltro = $('#dp2-mask').val();

		deFiltro = deFiltro.split('/');
		var dataDe = deFiltro[2] + '-' + deFiltro[1] + '-' + deFiltro[0];

		ateFiltro = ateFiltro.split('/');
		var dataAte = ateFiltro[2] + '-' + ateFiltro[1] + '-' + ateFiltro[0];

		if (dataDe > dataAte) {
			$('#alert-datepicker').removeClass('hide').find('span').text('A data final não pode ser menor que a data inicial.');

		} else {

			$('#alert-datepicker').addClass('hide');
			$('#dp2').datepicker('hide');

		}
	});

	$('#dp1-mask').mask('99/99/9999');
	$('#dp2-mask').mask('99/99/9999');


	$("#btnFiltrar").click(function() {

		var deFiltro = $('#dp1-mask').val();
		var ateFiltro = $('#dp2-mask').val();

		deFiltro = deFiltro.split('/');
		var dataDe = deFiltro[2] + '-' + deFiltro[1] + '-' + deFiltro[0];

		ateFiltro = ateFiltro.split('/');
		var dataAte = ateFiltro[2] + '-' + ateFiltro[1] + '-' + ateFiltro[0];

		if (dataDe > dataAte) {
			$('#alert-datepicker').removeClass('hide').find('span').text('A data "De" deve ser menor que a data "Até".');
			return false
		} else {
			$('#alert-datepicker').addClass('hide');
			var url = WEBROOT + 'admin/newsletters/index/de:' + dataDe +'/ate:' + dataAte;
			document.location.href = url;
		}

	});

	$(".baixar-csv").click(function(e) {
		e.preventDefault();

		var deFiltro = $('#dp1-mask').val();
		var ateFiltro = $('#dp2-mask').val();

		deFiltro = deFiltro.split('/');
		var dataDe = deFiltro[2] + '-' + deFiltro[1] + '-' + deFiltro[0];

		ateFiltro = ateFiltro.split('/');
		var dataAte = ateFiltro[2] + '-' + ateFiltro[1] + '-' + ateFiltro[0];

		if (dataDe > dataAte) {
			$('#alert-datepicker').removeClass('hide').find('span').text('A data "De" deve ser menor que a data "Até".');
			return false
		} else {
			$('#alert-datepicker').addClass('hide');
			var url = WEBROOT + 'admin/newsletters/baixar_csv/de:' + dataDe +'/ate:' + dataAte;
			document.location.href = url;
		}

	});


});