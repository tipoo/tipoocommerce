$(document).ready(function() {

	$("#FiltrarBuscaPagina").click(function() {

		var pagina = $('#FiltroPagina').val();

		var url = WEBROOT + 'admin/paginas/index/';

		if (pagina != '') {
			url += 'pagina:' + pagina;
		}

		document.location.href = url;

	});

	$("#btnLimpar").click(function(e) {
		var url = WEBROOT + 'admin/paginas/index/';
		document.location.href = url;

	});

});