$(document).ready(function() {

	$('#ColecoesLocalChave').focus();

	$('#ColecoesLocalChave').on('keyup', function() {
		if ($(this).val()) {
			$('.localizar-colecao').show();
		} else {
			$('.localizar-colecao').hide();
		}
	}).on('blur', function() {
		if ($(this).val()) {
			$('.localizar-colecao').show();
		} else {
			$('.localizar-colecao').hide();
		}
	});

	$('#ColecoesLocalAdminColecoesForm').validate(bootstrapValidateOptions);

	$("#ColecoesLocalColecaoId").rules("add", {
		required: true
	});

	// Autocomplete para adicionar novos colecoes a coleção
	var colecoesTypeahead = {};
	var colecoesDescricoesTypeahead = [];

	$(".typeahead").typeahead({
		source: function ( query, process ) {

			var descricao = $('.typeahead').val();

			//get the data to populate the typeahead (plus an id value)
			$.ajax({
				url: WEBROOT + 'admin/paginas/ajax_buscar_colecoes/' + descricao,
				cache: false,
				dataType: "json",
				success: function(data){

					//reset these containers every time the user searches
					//because we're potentially getting entirely different results from the api
					colecoesTypeahead = {};
					colecoesDescricoesTypeahead = [];

					data = data;
					//data = [{"id":4,"name":"novo"},{"id":6,"name":"novo"},{"id":7,"name":"novo"},{"id":9,"name":"novo"},{"id":11,"name":"novo"},{"id":12,"name":"novo"},{"id":13,"name":"novo img varias"},{"id":15,"name":"novo"},{"id":16,"name":"novo"},{"id":21,"name":"novo"},{"id":22,"name":"novo"}];
					//Using underscore.js for a functional approach at looping over the returned data.
					$.each( data, function(index, item){

						//for each iteration of this loop the "item" argument contains
						//1 bond object from the array in our json, such as:
						// { "id":7, "name":"Pierce Brosnan" }

						//add the label to the display array
						colecoesDescricoesTypeahead.push( item.name );

						//also store a hashmap so that when bootstrap gives us the selected
						//name we can map that back to an id value
						colecoesTypeahead[ item.name ] = item.id;
					});

					//send the array of results to bootstrap for display
					process( colecoesDescricoesTypeahead );
				}
			});
		},
		updater: function ( selectedName ) {

			//save the id value into the hidden field
			$("#ColecoesLocalColecaoId").val( colecoesTypeahead[ selectedName ] );

			$('#ColecoesLocalAdminColecoesForm').submit();

			//return the string you want to go into the textbox (the name)
			return '';
		}
	});

});