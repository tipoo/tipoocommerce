$(document).ready(function() {
	$('#PaginaAdminEditarForm').validate(bootstrapValidateOptions);

	$("#PaginaDescricao").rules("add", {
		required: true
	});

/*	$("#SlugUrl").rules("add", {
		required: true
	});*/

	$("#PaginaLayoutCtp").rules("add", {
		required: true
	});

	$("#PaginaArquivoCtp").rules("add", {
		required: true
	});

	var num_de_caracteres = $('#PaginaMetaDescription').val().length;

	$('#num_de_caracteres_pagina').text('Nº de caracteres:' + (-num_de_caracteres+160));

	if (num_de_caracteres > 160) {
		$('#num_de_caracteres_pagina').css('color', 'red');

	} else {
		$('#num_de_caracteres_pagina').css('color', 'black');
	}

	$('#PaginaMetaDescription').keyup(function(event){
		var num_de_caracteres = $(this).val().length;

		$('#num_de_caracteres_pagina').text('Nº de caracteres:' + (-num_de_caracteres+160));

		if (num_de_caracteres > 160) {
			$('#num_de_caracteres_pagina').css('color', 'red');

		} else {
			$('#num_de_caracteres_pagina').css('color', 'black');
		}
	});

});