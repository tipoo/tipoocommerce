$(document).ready(function() {

	$('#PaginasProdutoChave').focus();

	$('#PaginasProdutoChave').on('keyup', function() {
		if ($(this).val()) {
			$('.localizar-produto').show();
		} else {
			$('.localizar-produto').hide();
		}
	}).on('blur', function() {
		if ($(this).val()) {
			$('.localizar-produto').show();
		} else {
			$('.localizar-produto').hide();
		}
	});

	$('#PaginasProdutoAdminProdutosForm').validate(bootstrapValidateOptions);

	$("#PaginasProdutoProdutoId").rules("add", {
		required: true
	});

	// Autocomplete para adicionar novos produtos a coleção
	var produtosTypeahead = {};
	var produtosDescricoesTypeahead = [];

	$(".typeahead").typeahead({
		source: function ( query, process ) {

			var descricao = $('.typeahead').val();

			//get the data to populate the typeahead (plus an id value)
			$.ajax({
				url: WEBROOT + 'admin/paginas/ajax_buscar_produtos/' + descricao,
				cache: false,
				dataType: "json",
				success: function(data){

					//reset these containers every time the user searches
					//because we're potentially getting entirely different results from the api
					produtosTypeahead = {};
					produtosDescricoesTypeahead = [];

					data = data;
					//data = [{"id":4,"name":"novo"},{"id":6,"name":"novo"},{"id":7,"name":"novo"},{"id":9,"name":"novo"},{"id":11,"name":"novo"},{"id":12,"name":"novo"},{"id":13,"name":"novo img varias"},{"id":15,"name":"novo"},{"id":16,"name":"novo"},{"id":21,"name":"novo"},{"id":22,"name":"novo"}];
					//Using underscore.js for a functional approach at looping over the returned data.
					$.each( data, function(index, item){

						//for each iteration of this loop the "item" argument contains
						//1 bond object from the array in our json, such as:
						// { "id":7, "name":"Pierce Brosnan" }

						//add the label to the display array
						produtosDescricoesTypeahead.push( item.name );

						//also store a hashmap so that when bootstrap gives us the selected
						//name we can map that back to an id value
						produtosTypeahead[ item.name ] = item.id;
					});

					//send the array of results to bootstrap for display
					process( produtosDescricoesTypeahead );
				}
			});
		},
		updater: function ( selectedName ) {

			//save the id value into the hidden field
			$("#PaginasProdutoProdutoId").val( produtosTypeahead[ selectedName ] );

			$('#PaginasProdutoAdminProdutosForm').submit();

			//return the string you want to go into the textbox (the name)
			return '';
		}
	});

});