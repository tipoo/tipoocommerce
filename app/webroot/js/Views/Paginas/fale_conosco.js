$(document).ready(function() {

	$('#FormularioContatoIndexForm').validate();

	$("#ContatoNome").rules("add", {
		required: true,
		diferenteDe: 'Nome'
	});
	$("#ContatoEmail").rules("add", {
		required: true,
		email: true,
		diferenteDe: 'E-mail'
	});
	$("#ContatoAssunto").rules("add", {
		required: true,
		diferenteDe: 'Assunto'
	});
	$("#ContatoMensagem").rules("add", {
		required: true,
		diferenteDe: 'Mensagem'
	});

	$('#ContatoTelefone').mask('(99)9999-9999?9');

	if ($('.alert').length) {
		var top_distance = $('.alert').offset().top - ($(window).height() / 2);
		$('html, body').animate({scrollTop:top_distance}, 'slow');
	}

});