$(document).ready(function() {

	$('#FiltrarCepLogradouros').mask('99999-999');

	$("#FiltrarLogradouros").click(function() {

		var cep = $('#FiltrarCepLogradouros').val();

		var url = WEBROOT + 'admin/enderecos/logradouro/';

		if (cep != '') {
			url += 'cep:' + cep;
		}

		document.location.href = url;

	});

});