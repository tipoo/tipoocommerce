$(document).ready(function(){

	$('#LogradouroAdminAdicionarLogradouroForm').validate(bootstrapValidateOptions);

	$("#LogradouroEstadoId").rules("add", {
		required: true
	});
	$("#LogradouroCidadeId").rules("add", {
		required: true
	});
	$("#LogradouroBairroId").rules("add", {
		required: true
	});
	$("#LogradouroNomeclog").rules("add", {
		required: true
	});

	$('#LogradouroEstadoId').change(function() {

		var estado_id = $(this).val();

		$.ajax({
			url: WEBROOT + 'admin/enderecos/ajax_carregar_cidades/' + estado_id,
			type: 'POST',
			beforeSend: function() {
				$('#LogradouroCidadeId').empty();
				$('#LogradouroBairroId').empty();
				$('#LogradouroNomeclog').val('');
				$("#LogradouroCidadeId").append('<option>Aguarde...</option>');
				$("#LogradouroBairroId").append('<option value="">Selecione</option>');
			},
			success: function(data) {
				var retorno = jQuery.parseJSON(data);

				if (retorno.sucesso) {

					var options = '<option value="">Selecione</option>';
					$(retorno.cidades).each(function(e) {

						options += '<option value="' + this.Cidade.id + '">' + this.Cidade.nome + '</option>';

					});

					$('#LogradouroCidadeId').empty();
					$('#LogradouroBairroId').empty();
					$('#LogradouroNomeclog').val('');
					$("#LogradouroBairroId").append('<option value="">Selecione</option>');
					$("#LogradouroCidadeId").append(options);


				} else {
					$('#LogradouroCidadeId').empty();
					$('#LogradouroBairroId').empty();
					$('#LogradouroNomeclog').val('');
					$("#LogradouroCidadeId").append('<option value="">Selecione</option>');
					$("#LogradouroBairroId").append('<option value="">Selecione</option>');
				}

			},
			error: function(data){
				alert ('erro...');
				$('#LogradouroCidadeId').empty();
				$('#LogradouroBairroId').empty();
				$('#LogradouroNomeclog').val('');
				$("#LogradouroCidadeId").append('<option value="">Selecione</option>');
				$("#LogradouroBairroId").append('<option value="">Selecione</option>');
			}
		});
	});

	$('#LogradouroCidadeId').change(function() {

		var cidade_id = $(this).val();

		$.ajax({
			url: WEBROOT + 'admin/enderecos/ajax_carregar_bairros/' + cidade_id,
			type: 'POST',
			beforeSend: function() {
				$('#LogradouroBairroId').empty();
				$('#LogradouroNomeclog').val('');
				$("#LogradouroBairroId").append('<option>Aguarde...</option>');
			},
			success: function(data) {
				var retorno = jQuery.parseJSON(data);

				if (retorno.sucesso) {

					var options = '<option value="">Selecione</option>';
					$(retorno.bairros).each(function(e) {

						options += '<option value="' + this.Bairro.id + '">' + this.Bairro.nome + '</option>';

					});

					$('#LogradouroBairroId').empty();
					$('#LogradouroNomeclog').val('');
					$("#LogradouroBairroId").append(options);


				} else {
					$('#LogradouroBairroId').empty();
					$('#LogradouroNomeclog').val('');
					$("#LogradouroBairroId").append('<option value="">Selecione</option>');
				}

			},
			error: function(data){
				alert ('erro...');
				$('#LogradouroBairroId').empty();
				$('#LogradouroNomeclog').val('');
				$("#LogradouroBairroId").append('<option value="">Selecione</option>');
			}
		});
	});

	$('#LogradouroBairroId').change(function() {
		$('#LogradouroNomeclog').val('');
	});
});