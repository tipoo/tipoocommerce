$(document).ready(function() {
	$('#AdicionarAdminMovimentacaoForm').validate(bootstrapValidateOptions);
	$('#RemoverAdminMovimentacaoForm').validate(bootstrapValidateOptions);

	$("#AdicionarQtd").rules("add", {
		required: true
	});

	$("#RemoverQtd").rules("add", {
		required: true
	});

	$('#AdicionarQtd').numeric({decimal : ","});
	$('#RemoverQtd').numeric({decimal : ","});

});