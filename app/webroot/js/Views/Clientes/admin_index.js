$(document).ready(function() {

	$('#FiltrarCnpj').mask('99.999.999/9999-99');
	$('#FiltrarCpf').mask('999.999.999-99');
	$('#FiltroCep').mask('99999-999');

	$('.ver-detalhes-cliente').click(function(e){
		var url = $(this).attr('href');
		e.preventDefault();
		$.ajax({
			url: url,
			type: 'GET',
			success: function(data){
				bootbox.dialog({
					size: 'large',
					message:data,
					title: 'Detalhes do Cliente',
					buttons: {
						main: {
							label: 'Fechar X',
							className: 'btn-danger'
						}
					}
				});
			},
			error: function(data){
				alert ('Ocorreu um erro ao tentar ao tentar buscar os dados.');
			}
		});
	});

	$("#btnFiltrar").click(function() {

		var genero = $('#FiltroGenero').val();
		var estado_id = $('#FiltroEstadoId').val();
		var cpf = $('#FiltrarCpf').val();
		var cidade_id = $('#FiltroCidadeId').val();
		var email = $('#FiltroEmail').val();
		var tipo_pessoa = $('#FiltroTipoPessoa').val();
		var status_cliente = $('#FiltroStatusCliente').val();
		var nome = $('#FiltroNome').val();
		var cep = $('#FiltroCep').val();
		var receber_newsletter = $('#FiltroReceberNewsletter').val();
		var dia_aniversario = $('#FiltroDiaAniversario').val();
		var mes_aniversario = $('#FiltroMesAniversario').val();
		var ano_aniversario = $('#FiltroAnoAniversario').val();

		var cnpj = $('#FiltrarCnpj').val();
		cnpj_ = cnpj.split('/');
		cnpj_ = cnpj_ [0] + ',' + cnpj_ [1];

		if (tipo_pessoa == 'F') {
			cnpj = ''; 
		}

		if (tipo_pessoa == 'J') {
			cpf = ''; 
			genero = ''; 
		}


		var url = WEBROOT + 'admin/clientes/index/';

		if (nome != '') {
			url += 'nome:' + nome+'/';
		}

		if (tipo_pessoa != '') {
			url += 'tipo_pessoa:' + tipo_pessoa+'/';
		}

		if (status_cliente != '') {
			url += 'status_cliente:' + status_cliente+'/';
		}

		if (genero != '') {
			url += 'genero:' + genero +'/';
		}

		if (email != '') {
			url += 'email:' + email +'/';
		}

		if (estado_id != '') {
			url += 'estado_id:' + estado_id +'/';
		}

		if (cidade_id != '') {
			url += 'cidade_id:' + cidade_id+'/';
		}

		if (cnpj != '') {
			url += 'cnpj:' + cnpj_ +'/';
		}

		if (cpf != '') {
			url += 'cpf:' + cpf +'/';
		}

		if (cep != '') {
			url += 'cep:' + cep+'/';
		}

		if (receber_newsletter != '') {
			url += 'receber_newsletter:' + receber_newsletter+'/';
		}

		if (dia_aniversario != '') {
			url += 'dia_aniversario:' + dia_aniversario+'/';
		}

		if (mes_aniversario != '') {
			url += 'mes_aniversario:' + mes_aniversario+'/';
		}

		if (ano_aniversario != '') {
			url += 'ano_aniversario:' + ano_aniversario+'/';
		}

		document.location.href = url;

	});

	$('#FiltroTipoPessoa').on('change', function(){

		var tipo_pessoa = $('#FiltroTipoPessoa').val();

		if(tipo_pessoa == ''){

			$('.campo-cnpj').show();
			$('.campo-cpf').show();
			$('.campo-genero').show();
			

		} else if(tipo_pessoa == 'F'){

			$('.campo-cnpj').hide();
			$('.campo-cpf').show();
			$('.campo-genero').show();
			

		} else if(tipo_pessoa == 'J'){

			$('.campo-cpf').hide();
			$('.campo-genero').hide();
			$('.campo-cnpj').show();

		}


	});

	$("#btnLimpar").click(function() {

		var url = WEBROOT + 'admin/clientes/index/';
		document.location.href = url;

	});

	$("#baixar-csv").click(function(e) {
		e.preventDefault();

		var genero = $('#FiltroGenero').val();
		var estado_id = $('#FiltroEstadoId').val();
		var cpf = $('#FiltrarCpf').val();
		var cidade_id = $('#FiltroCidadeId').val();
		var email = $('#FiltroEmail').val();
		var tipo_pessoa = $('#FiltroTipoPessoa').val();
		var status_cliente = $('#FiltroStatusCliente').val();
		var nome = $('#FiltroNome').val();
		var cep = $('#FiltroCep').val();
		var receber_newsletter = $('#FiltroReceberNewsletter').val();
		var dia_aniversario = $('#FiltroDiaAniversario').val();
		var mes_aniversario = $('#FiltroMesAniversario').val();
		var ano_aniversario = $('#FiltroAnoAniversario').val();
		
		var cnpj = $('#FiltrarCnpj').val();
		cnpj_ = cnpj.split('/');
		cnpj_ = cnpj_ [0] + ',' + cnpj_ [1];

		if (tipo_pessoa == 'F') {
			cnpj = ''; 
		}

		if (tipo_pessoa == 'J') {
			cpf = ''; 
			genero = ''; 
		}

		var url = WEBROOT + 'admin/clientes/baixar_csv/';

		if (nome != '') {
			url += 'nome:' + nome+'/';
		}

		if (tipo_pessoa != '') {
			url += 'tipo_pessoa:' + tipo_pessoa+'/';
		}

		if (status_cliente != '') {
			url += 'status_cliente:' + status_cliente+'/';
		}

		if (genero != '') {
			url += 'genero:' + genero +'/';
		}

		if (email != '') {
			url += 'email:' + email +'/';
		}

		if (estado_id != '') {
			url += 'estado_id:' + estado_id +'/';
		}

		if (cidade_id != '') {
			url += 'cidade_id:' + cidade_id+'/';
		}

		if (cnpj != '') {
			url += 'cnpj:' + cnpj_ +'/';
		}

		if (cpf != '') {
			url += 'cpf:' + cpf +'/';
		}

		if (cep != '') {
			url += 'cep:' + cep +'/';
		}

		if (receber_newsletter != '') {
			url += 'receber_newsletter:' + receber_newsletter+'/';
		}

		if (dia_aniversario != '') {
			url += 'dia_aniversario:' + dia_aniversario+'/';
		}

		if (mes_aniversario != '') {
			url += 'mes_aniversario:' + mes_aniversario+'/';
		}

		if (ano_aniversario != '') {
			url += 'ano_aniversario:' + ano_aniversario+'/';
		}


		document.location.href = url;

	});

	$("#FiltrarBuscaCnpj").click(function() {

		var cnpj = $('#FiltrarCnpj').val();
		cnpj_ = cnpj.split('/');
		cnpj = cnpj_ [0] + ',' + cnpj_ [1];

		var url = WEBROOT + 'admin/clientes/index/';

		if (cnpj != '') {
			url += 'cnpj:' + cnpj;
		}

		document.location.href = url;

	});

	$("#FiltrarBuscaCpf").click(function() {

		var cpf = $('#FiltrarCpf').val();

		var url = WEBROOT + 'admin/clientes/index/';

		if (cpf != '') {
			url += 'cpf:' + cpf;
		}

		document.location.href = url;

	});

	$('#FiltroEstadoId').change(function() {
		var cidade_id = $(this).val();

		$.ajax({
			url: WEBROOT + 'admin/clientes/ajax_carregar_cidades/' + cidade_id,
			type: 'POST',
			beforeSend: function() {
				$('#FiltroCidadeId').empty();
				$("#FiltroCidadeId").append('<option>Aguarde...</option>');
			},
			success: function(data) {
				var retorno = jQuery.parseJSON(data);

				if (retorno.sucesso) {

					var options = '<option value="">Selecione</option>';
					$(retorno.cidades).each(function(e) {

						options += '<option value="' + this.Cidade.id + '">' + this.Cidade.nome + '</option>';

					});

					$('#FiltroCidadeId').empty();
					$("#FiltroCidadeId").append(options);


				} else {
					$('#FiltroCidadeId').empty();
					$("#FiltroCidadeId").append('<option value="">Selecione</option>');
				}

			},
			error: function(data){
				alert ('erro...');
				$('#FiltroCidadeId').empty();
				$("#FiltroCidadeId").append('<option value="">Selecione</option>');
			}
		});
	});

});