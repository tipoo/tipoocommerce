$(document).ready(function() {
	// Verificando se ja esta retornando para pagina por um erro ou similar com os dados preenchidos e exibindo os campos de endereco
	if ($("#Endereco0Cep").val() !== '') {
		$('.dados-endereco').show();
	}

	$('#ClienteCadastrarForm').validate();

	// Validações pesosa fisica
	$("#ClienteNome").rules("add", {
		required: function() {return $('#ClienteTipoPessoa').val() == 'F';}
	});
	// $("#ClienteDataNascto").rules("add", {
	// 	required: function() {return $('#ClienteTipoPessoa').val() == 'F';}
	// });
	$("#ClienteCpf").rules("add", {
		required: function() {return $('#ClienteTipoPessoa').val() == 'F';},
		cpf: function() {return $('#ClienteTipoPessoa').val() == 'F';}
	});
	$("#ClienteDataNascto").rules("add", {
		required: function() {return $('#ClienteTipoPessoa').val() == 'F';},
		dateBR: function() {return $('#ClienteTipoPessoa').val() == 'F';}
	});
	$("#ClienteTelResidencial").rules("add", {
		required: function() {return $('#ClienteTipoPessoa').val() == 'F';}
	});

	// Validacoes pessoa juridica
	$("#ClienteRazaoSocial").rules("add", {
		required: function() {return $('#ClienteTipoPessoa').val() == 'J';}
	});
	$("#ClienteNomeFantasia").rules("add", {
		required: function() {return $('#ClienteTipoPessoa').val() == 'J';}
	});
	$("#ClienteCnpj").rules("add", {
		required: function() {return $('#ClienteTipoPessoa').val() == 'J';},
		cnpj: function() {return $('#ClienteTipoPessoa').val() == 'J';}
	});
	$("#ClienteInscricaoEstadual").rules("add", {
		required: function() {return $('#ClienteTipoPessoa').val() == 'J';}
	});
	$("#ClienteTelComercialPj").rules("add", {
		required: function() {return $('#ClienteTipoPessoa').val() == 'J';}
	});

	// Validacoes endereco
	$("#Endereco0Cep").rules("add", {
		required: true
	});
	$("#Endereco0Endereco").rules("add", {
		required: true
	});
	$("#Endereco0Numero").rules("add", {
		required: true
	});
	$("#Endereco0Bairro").rules("add", {
		required: true
	});
	$("#Endereco0Cidade").rules("add", {
		required: true
	});
	$("#Endereco0Estado").rules("add", {
		required: true
	});

	// Validacao dos dados de acesso
	$("#ClienteEmail").rules("add", {
		required: true,
		email: true,
		messages: {
			email: 'Por favor, forneça um e-mail válido.'
		}
	});
	$("#ClienteEmailConfirmacao").rules("add", {
		required: true,
		email: true,
		equalTo: "#ClienteEmail"
	});
	$("#ClienteSenha").rules("add", {
		required: true
	});
	$("#ClienteSenhaConfirmacao").rules("add", {
		required: true,
		equalTo: "#ClienteSenha"
	});

	$('#ClienteDataNascto').mask('99/99/9999');
	$('#ClienteCpf').mask('999.999.999-99');
	$('#ClienteTelResidencial').mask('(99)9999-9999');
	$('#ClienteTelComercialPf').mask('(99)9999-9999');
	$('#ClienteCelular').mask('(99)9999-9999?9');

	$('#ClienteCnpj').mask('99.999.999/9999-99');
	$('#ClienteTelComercialPj').mask('(99)9999-9999');

	$('#Endereco0Cep').mask('99999-999');

	$('.bt-pessoa-fisica').on('click', function() {
		$('.dados-empresariais').hide();
		$('.dados-pessoais').show();
		$('#ClienteTipoPessoa').val('F');

		return false;
	});

	$('.bt-pessoa-juridica').on('click', function() {
		$('.dados-pessoais').hide();
		$('.dados-empresariais').show();
		$('#ClienteTipoPessoa').val('J');

		return false;
	});

	// Campos Complementares

	var numero_decimal = $('.mascara-decimal').length;
	if (numero_decimal) {
		$('.mascara-decimal').each(function() {
			$(this).priceFormat({
				prefix: '',
				clearPrefix: true,
				centsSeparator: ',',
				thousandsSeparator: '.'
			});
		});

		$('.mascara-decimal').blur(function() {
			if ($(this).val() == '0,00') {
				$(this).val('');
			}
		});
	}

	var numero_inteiro = $('.mascara-inteiro').length;
	if (numero_inteiro) {
		$('.mascara-inteiro').each(function() {
			$(this).numeric({
				decimal : false,
				negative: false
			});
		});
	}

	$('.campo-complementar.pf').each(function() {
		if ($(this).data('required')) {
			$(this).rules("add", {
				required: function() {return $('#ClienteTipoPessoa').val() == 'F';}
			});
		}
	});

	$('.campo-complementar.pj').each(function() {
		if ($(this).data('required')) {
			$(this).rules("add", {
				required: function() {return $('#ClienteTipoPessoa').val() == 'J';}
			});
		}
	});

	cep = null;
	$('#Endereco0Cep').on('blur', function() {
		if ($(this).val().length == 9 && $(this).val() != cep) {
			$('#Endereco0Endereco').val('');
			$('#Endereco0Bairro').val('');
			$('#Endereco0CidadeId').val('');
			$('#Endereco0Cidade').val('');
			$('#Endereco0Estado').val('');

			cep = $(this).val();
			$.ajax({
				url: WEBROOT + 'clientes/ajax_buscar_cep/' + cep,
				dataType: 'json',
				beforeSend: function() {
					$('.dados-endereco').hide();
					$('.cep-erro').hide();
					$('.busca-cep').hide();
					$('.busca-cep').after('<img class="loader-busca-cep" src="' + WEBROOT + 'theme/' + PROJETO + '/img/ajax-loader.gif" />');
				},
				success: function(data) {
					if (data.sucesso) {
						$('.cep-erro').hide();
						$('.loader-busca-cep').remove();

						$('#Endereco0Endereco').val(data.endereco);
						$('#Endereco0Bairro').val(data.bairro);
						$('#Endereco0CidadeId').val(data.cidade_id);
						$('#Endereco0Cidade').val(data.cidade);
						$('#Endereco0Estado').val(data.estado);

						$('.dados-endereco').show();

						if (data.endereco === '') {
							$('#Endereco0Endereco').focus();
						} else {
							$('#Endereco0Numero').focus();
						}

					} else {
						$('.dados-endereco').hide();
						$('.cep-erro p').html(data.mensagem);
						$('.cep-erro').show();
						$('.loader-busca-cep').remove();
					}

					$('.busca-cep').show();
				},
				error: function(jqXHR, textStatus, errorThrown ) {
					$('.dados-endereco').hide();
					$('.loader-busca-cep').remove();
					$('.busca-cep').show();
					$('.cep-erro p').html('Ocorreu um erro ao tentar consultar o cep. Por favor tente novamente. Caso o problema persista, entre em contato conosco e informe a mensagem: [' + textStatus + ']');
					$('.cep-erro').show();
				}
			});
		}
	});

	$('#ClienteCadastrarForm').submit(function() {
		var retorno = true;

		var nome = $("#ClienteNome").val();

		if (nome.length) {
			nome = $.trim(nome);
			nomeCompleto = nome.split(' ');

			containerNome = $("#ClienteNome").closest('div');

			if (nomeCompleto.length == 1) {
				containerNome.children('input').addClass('error');
				containerNome.append('<label for="ClienteNome" generated="true" class="error nome-completo">O nome deve ser completo.</label>');
				retorno = false;
			} else {
				containerNome.children('nome-completo').remove();
				containerNome.removeClass('error');
			}
		}

		if ($('.cep-erro').is(':visible')) {
			retorno = false;
			alert('CEP não encontrado. Forneça um CEP válido ou entre em contato.')
		} else if ($('#Endereco0CidadeId').val() == '') {
			retorno = false;
			$('.cep-erro').show();
			alert('Ocorreu um erro ao tentar cadastrar o CEP. Por favor, digite o CEP novamente.')
		}

		return retorno;
	});

});