$(document).ready(function() {
	$('#EnderecoAlterarEnderecoForm').validate();

	$("#EnderecoDescricao").rules("add", {
		required: true
	});
	$("#EnderecoEndereco").rules("add", {
		required: true
	});
	$("#EnderecoNumero").rules("add", {
		required: true
	});
	$("#EnderecoBairro").rules("add", {
		required: true
	});
});