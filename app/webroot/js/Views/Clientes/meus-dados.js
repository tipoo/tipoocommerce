$(document).ready(function() {

	$(document).on("click", ".close", function(e) {
		e.preventDefault();
		$(this).closest('.alert').fadeOut(function() {
			$(this).closest('.alert').remove();
		});
	});

	$('#ClienteDadosCadastraisForm').validate();

	if ($('#ClienteTipoPessoa').val() == 'F') {
		$("#ClienteNome").rules("add", {
			required: true
		});

		$("#ClienteEmail").rules("add", {
			required: true,
			email: true,
			messages: {
				email: 'Por favor, forneça um e-mail válido.'
			}
		});

		$("#ClienteDataNascto").rules("add", {
			required: true
		});
		$("#ClienteDataNascto").rules("add", {
			required: true,
			dateBR: true
		});
		$("#ClienteTelResidencial").rules("add", {
			required: true
		});

		$('#ClienteDataNascto').mask('99/99/9999');
		$('#ClienteTelResidencial').mask('(99)9999-9999');
		$('#ClienteTelComercialPf').mask('(99)9999-9999');
		$('#ClienteCelular').mask('(99)9999-9999?9');

	} else {
		$("#ClienteRazaoSocial").rules("add", {
			required: true
		});
		$("#ClienteNomeFantasia").rules("add", {
			required: true
		});
		$("#ClienteInscricaoEstadual").rules("add", {
			required: true
		});
		$("#ClienteTelComercialPj").rules("add", {
			required: true
		});


		$('#ClienteCnpj').mask('99.999.999/9999-99');
		$('#ClienteTelComercialPj').mask('(99)9999-9999');
	}

	// Campos Complementares

	var numero_decimal = $('.mascara-decimal').length;
	if (numero_decimal) {
		$('.mascara-decimal').each(function() {
			$(this).priceFormat({
				prefix: '',
				clearPrefix: true,
				centsSeparator: ',',
				thousandsSeparator: '.'
			});
		});

		$('.mascara-decimal').blur(function() {
			if ($(this).val() == '0,00') {
				$(this).val('');
			}
		});
	}

	var numero_inteiro = $('.mascara-inteiro').length;
	if (numero_inteiro) {
		$('.mascara-inteiro').each(function() {
			$(this).numeric({
				decimal : false,
				negative: false
			});
		});
	}

	$('.campo-complementar').each(function() {
		if ($(this).data('required')) {
			$(this).rules("add", {
				required: true
			});
		}
	});

	$('#ClienteAlterarSenhaForm').validate();

	$("#ClienteSenhaAtual").rules("add", {
		required: true
	});

	$("#ClienteSenha").rules("add", {
		required: true
	});

	$("#ClienteSenhaConfirmacao").rules("add", {
		required: true,
		equalTo: "#ClienteSenha"
	});


});