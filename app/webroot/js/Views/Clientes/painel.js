$(document).ready(function() {

	$(document).on("click", ".close", function(e) {
		e.preventDefault();
		$(this).closest('.alert').fadeOut(function() {
			$(this).closest('.alert').remove();
		});
	});

});