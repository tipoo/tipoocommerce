$(document).ready(function() {

	$(document).on("click", ".close", function(e) {
		e.preventDefault();
		$(this).closest('.alert').fadeOut(function() {
			$(this).closest('.alert').remove();
		});
	});

	// Verificando se ja esta retornando para pagina por um erro ou similar com os dados preenchidos e exibindo os campos de endereco
	if ($("#EnderecoCep").val() !== '') {
		$('.dados-endereco').show();
	}

	$('#EnderecoMeusEnderecosForm').validate();

	$("#EnderecoDescricao").rules("add", {
		required: true
	});
	$("#EnderecoCep").rules("add", {
		required: true
	});
	$("#EnderecoEndereco").rules("add", {
		required: true
	});
	$("#EnderecoNumero").rules("add", {
		required: true
	});
	$("#EnderecoBairro").rules("add", {
		required: true
	});
	$("#EnderecoCidade").rules("add", {
		required: true
	});
	$("#EnderecoEstado").rules("add", {
		required: true
	});

	$('#EnderecoCep').mask('99999-999');

	cep = null;
	$('#EnderecoCep').on('blur', function() {
		if ($(this).val().length == 9 && $(this).val() != cep) {
			$('#EnderecoEndereco').val('');
			$('#EnderecoBairro').val('');
			$('#EnderecoCidadeId').val('');
			$('#EnderecoCidade').val('');
			$('#EnderecoEstado').val('');
			
			cep = $(this).val();
			$.ajax({
				url: WEBROOT + 'clientes/ajax_buscar_cep/' + cep,
				dataType: 'json',
				beforeSend: function() {
					$('.dados-endereco').hide();
					$('.cep-erro').hide();
					$('.busca-cep').hide();
					$('.busca-cep').after('<img class="loader-busca-cep" src="' + WEBROOT + 'theme/' + PROJETO + '/img/ajax-loader.gif" />');
				},
				success: function(data) {
					if (data.sucesso) {
						$('.cep-erro').hide();
						$('.loader-busca-cep').remove();

						$('#EnderecoEndereco').val(data.endereco);
						$('#EnderecoBairro').val(data.bairro);
						$('#EnderecoCidadeId').val(data.cidade_id);
						$('#EnderecoCidade').val(data.cidade);
						$('#EnderecoEstado').val(data.estado);

						$('.dados-endereco').show();

						if (data.endereco === '') {
							$('#EnderecoEndereco').focus();
						} else {
							$('#EnderecoNumero').focus();
						}

					} else {
						$('.dados-endereco').hide();
						$('.cep-erro p').html(data.mensagem);
						$('.cep-erro').show();
						$('.loader-busca-cep').remove();
					}
				},
				error: function(jqXHR, textStatus, errorThrown ) {
					$('.dados-endereco').hide();
					$('.loader-busca-cep').remove();
					$('.busca-cep').show();
					$('.cep-erro').html('Ocorreu um erro ao tentar consultar o cep. Por favor tente novamente. Caso o problema persista, entre em contato conosco e informe a mensagem: [' + textStatus + ']');
					$('.cep-erro').show();
				}
			});
		}
	});

	$('#EnderecoMeusEnderecosForm').on('submit', function() {
		var retorno = true;
		if (!$('.dados-endereco').is(':visible')) {
			retorno = false;
			alert('CEP não encontrado. Forneça um CEP válido ou entre em contato.');
		}

		return retorno;
	});

});