calcular_frete = function(cep) {
	if (cep) {
		document.location.href = WEBROOT + 'carrinho/calcular_frete/' + cep;
	} else {
		alert('Por favor, preencha o CEP.');
	}
};

enviar_cupom = function(cupom) {
	if (cupom) {
		document.location.href = WEBROOT + 'carrinho/aplicar_cupom/' + cupom;
	} else {
		alert('Por favor, preencha o cupom de desconto.');
	}
};

$(document).ready(function() {
	$('.input-quantidade').on('blur', function() {

		var id_produto = $(this).attr('data-id');
		var quantidade = $('#quantidade-' + id_produto).val();

		if (quantidade <= 0 || quantidade === '') {
			$('#quantidade-' + id_produto).val(1);
		}

		document.location.href = WEBROOT + 'carrinho/editar/' + $(this).attr('data-id') + '/' + $(this).val();

	});

	//SERVICOS 

	$('.servicos-skus').each(function(index_sku) {

		$(this).find('.servico-sku-' + index_sku).each(function(index_servico) {

			var servico_id = $(this).find('.servico-id').val();

			$(this).find('.servico-tipo').each(function(index_opcao) {

				if ($('.texto-personalizado-' + index_sku + '-' + servico_id).length) {

					$('.field-text-' + index_sku + '-' + servico_id).hide();
					$('.btn-edit-text-' + index_sku +  '-' + servico_id).show();
					$('.btn-clean-text-' + index_sku +  '-' + servico_id).show();
					
				} else {

					$('.btn-edit-text-' + index_sku +  '-' + servico_id).hide();
					$('.btn-clean-text-' + index_sku +  '-' + servico_id).hide();
					$('.current-text-' + index_sku +  '-' + servico_id).hide();
				}

				$(this).find('.write-text').on('click', function() {

					var id_opcao_selecionada = $(this).data('opcao');
					var servico_texto = $('#servico_opcao' + id_opcao_selecionada + '_' + index_sku).val();

					if (typeof servico_texto != 'undefined' && servico_texto != '') {
						document.location.href = WEBROOT + 'carrinho/servico/' + $(this).attr('data-id') + '/' + servico_id + '/' + id_opcao_selecionada + '/?texto=' + servico_texto;
					}

				});

				$(this).find('input[type=checkbox]').on('click', function() {
					var id_opcao_selecionada = $(this).data('opcao');

					if ($(this).is(':checked')) {

						document.location.href = WEBROOT + 'carrinho/servico/' + $(this).attr('data-id') + '/' + servico_id + '/' + id_opcao_selecionada + '/?texto=';

					} else {
						document.location.href = WEBROOT + 'carrinho/limpar_servico/' + $(this).attr('data-id') + '/' + servico_id + '/' + id_opcao_selecionada;
					}

				});

				$(this).find('select').on('change', function() {

					var select_selecionado = $(this).find('option:selected');
					var id_opcao_selecionada = select_selecionado.data('opcao');

					if (select_selecionado.val() != 'Selecione') {

						document.location.href = WEBROOT + 'carrinho/servico/' + select_selecionado.attr('data-id') + '/' + servico_id + '/' + id_opcao_selecionada + '/?texto=';

					} else {

						document.location.href = WEBROOT + 'carrinho/limpar_servico/' + select_selecionado.attr('data-id') + '/' + servico_id + '/' + id_opcao_selecionada;
					}

				});

				$('.btn-edit-text-' + index_sku +  '-' + servico_id).on('click', function() {

					$(this).hide();
					$('.btn-clean-text-' + index_sku +  '-' + servico_id).hide();
					$('.current-text-' + index_sku +  '-' + servico_id).hide();

					$('.field-text-' + index_sku + '-' + servico_id).slideToggle();

				});

				$('.btn-clean-text-' + index_sku +  '-' + servico_id).on('click', function() {

					document.location.href = WEBROOT + 'carrinho/limpar_servico/' + $(this).attr('data-id') + '/' + servico_id;

				});

			});

		});

	});

	// FIM SERVICOS

	$('.remover-sku').on('click', function() {
		document.location.href = WEBROOT + 'carrinho/remover/' + $(this).attr('data-id');
	});

	$('#FreteIndexForm').validate();

	$('.input-quantidade').numeric();

	$('#FreteCalcValorFrete').mask('99999-999');

	// Calculo do Frete
	$(document).on("click", '#FreteIndexForm .submit input[type="submit"]', function(e) {
		cep = $('#FreteCalcValorFrete').val();
		calcular_frete(cep);
		return false;
	});

	// Cupom
	$(document).on("click", '#CupomIndexForm .submit input[type="submit"]', function(e) {
		cupom = $('#CupomCodigo').val();
		enviar_cupom(cupom);
		return false;
	});
});