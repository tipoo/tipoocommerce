$(document).ready(function() {
	$('#ItemMenuAdminAdicionarForm').validate(bootstrapValidateOptions);

	$("#ItemMenuNome").rules("add", {
		required: true
	});

	$("#ItemMenuLink").rules("add", {
		required: true
	});

});