$(document).ready(function() {

	$('#ColecoesProdutoDescricao').focus();

	$('#ColecoesProdutoAdminProdutosForm').validate(bootstrapValidateOptions);

	$("#ColecoesProdutoProdutoId").rules("add", {
		required: true
	});

	$('.typeahead').typeahead({
		name: 'produtos',
		remote: WEBROOT + 'admin/colecoes/ajax_buscar_produtos/%QUERY',
		template: '<strong>{{descricao}}</strong> - {{marca}} - #{{id}}',
		engine: Hogan
	}).on('typeahead:selected', function (obj, datum) {
		$("#ColecoesProdutoProdutoId").val( datum.id );
		$('#ColecoesProdutoAdminProdutosForm').submit();
	});

	var fixHelperModified = function(e, tr) {
		var $originals = tr.children();
		var $helper = tr.clone();
		$helper.children().each(function(index)
		{
			$(this).width($originals.eq(index).width());
		});
		return $helper;
	};

	$("#sortable").sortable({
		cursor: "move",
		helper: fixHelperModified,
		axis: "y",
		key: "sort",
		opacity: 0.6,
		tolerance: 'pointer',
		stop: function( event, ui ) {

			var colecao_id = $('#ColecoesProdutoColecaoId').val();

			var url = WEBROOT + 'admin/colecoes/ajax_ordenar/' + colecao_id + '/';

			$('#sortable .ui-state-default').each(function(e) {
				var ordem = e + 1;
				var id_produto = $(this).data('id');

				url += 'ordem_' + ordem + ':' + id_produto +'/';
				//console.log(url);

			});

			$.ajax({
				url: url,
				type: 'POST',
				success: function(data) {

					var retorno = jQuery.parseJSON(data);

					if (!retorno.sucesso) {
						alert(retorno.mensagem);
					}
				},
				error: function(data){
					alert('Ocorreu um erro ao tentar ordenar os Produtos. Por favor, tente novamente.');
				}
			});
		}
	}).disableSelection();

	$('.ordenacao-desce').on('click', function() {

		var colecao_id = $('#ColecoesProdutoColecaoId').val();

	  	var $row = $(this).closest('tr');

		$row.next().after($row);

		var url = WEBROOT + 'admin/colecoes/ajax_ordenar/' + colecao_id + '/';

		$('#sortable .ui-state-default').each(function(e) {
			var ordem = e + 1;
			var id_produto = $(this).data('id');

			url += 'ordem_' + ordem + ':' + id_produto +'/';

		});

		$.ajax({
			url: url,
			type: 'POST',
			success: function(data) {

				var retorno = jQuery.parseJSON(data);

				if (!retorno.sucesso) {
					alert(retorno.mensagem);
				}
			},
			error: function(data){
				alert('Ocorreu um erro ao tentar ordenar os Produtos. Por favor, tente novamente.');
			}
		});

	});

	$('.ordenacao-sobe').on('click', function() {

		var colecao_id = $('#ColecoesProdutoColecaoId').val();

	  	var $row = $(this).closest('tr');

		$row.prev().before($row);

		var url = WEBROOT + 'admin/colecoes/ajax_ordenar/' + colecao_id + '/';

		$('#sortable .ui-state-default').each(function(e) {
			var ordem = e + 1;
			var id_produto = $(this).data('id');

			url += 'ordem_' + ordem + ':' + id_produto +'/';
			console.log(url);

		});

		$.ajax({
			url: url,
			type: 'POST',
			success: function(data) {

				var retorno = jQuery.parseJSON(data);

				if (!retorno.sucesso) {
					alert(retorno.mensagem);
				}
			},
			error: function(data){
				alert('Ocorreu um erro ao tentar ordenar os Produtos. Por favor, tente novamente.');
			}
		});

	});

	if ($('.seta-ordenacao').is((':visible'))) {
			$('.alert-info').hide();

	} else {
		$('.alert-info').show();
	}

	$(window).resize(function() {

		if ($('.seta-ordenacao').is((':visible'))) {
			$('.alert-info').hide();

		} else {
			$('.alert-info').show();
		}

	});

});