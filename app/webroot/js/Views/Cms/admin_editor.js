$(document).ready(function() {
	var editor = ace.edit("editor");
	editor.setTheme("ace/theme/monokai");
	editor.getSession().setMode("ace/mode/php");
	editor.getSession().setTabSize(4);
	editor.getSession().setUseSoftTabs(false);
	editor.setShowPrintMargin(false);
	editor.getSession().on('change', function(){
		console.log('aqui');
		window.onbeforeunload = function() {
			return 'O arquivo foi alterado. Deseja realmente sair sem salva-lo?';
		};
		$('.btn-salvar').removeClass('disabled');
		$('.btn-salvar').addClass('btn-primary');
	});

	editor.commands.addCommand({
		name: 'cmdSalvar',
		bindKey: {win: 'Ctrl-S',  mac: 'Command-S'},
		exec: function(editor) {
			$('.btn-salvar').trigger('click');
		}
	});

	$('.btn-salvar').on('click', function() {
		$('#CmsAdminEditorForm').submit();
	});

	$('#CmsAdminEditorForm').on('submit', function() {
		window.onbeforeunload = null;
		$('#CmsConteudoArquivo').val(editor.getSession().getValue());
	});

	
});