var atualizarLocalAtual = function(element) {
	$('.local-atual').html(element.data('local'));
	$('.local-atual').data('local', element.data('local'));
	$('.local-atual').data('caminho', element.data('caminho'));

	$('.arvore-selecionado').removeClass('arvore-selecionado');
	element.addClass('arvore-selecionado');
};

var trocarPainelAcoes = function(painel) {
	$('.collapse-ctrl').hide();
	$('.collapse-ctrl.' + painel).show();
};

var adicionarArquivo = function(arquivo) {
	var local = $('.local-atual').data('local');
	var caminho = $('.local-atual').data('caminho');

	var html = '<li><span class="glyphicon glyphicon-file text-primary"></span> ';
	html += '<a href="#" data-local="' + local + DS + arquivo + '" data-caminho="' + caminho + '.DS.' + arquivo + '" data-tipo="' + verificarTipoArquivo(arquivo) + '" data-prioridade-arvore="1">';
	html += arquivo;
	html += '</a></li>';

	var classe = caminho;
	if (classe) {
		do {
			classe = classe.replace('.DS.', '-');
		} while(classe.indexOf('.DS.') > -1);

	} else {
		classe = 'raiz';
	}

	$(html).appendTo($('ul.' + classe));
	$('ul.' + classe + '>li').tsort('a', {data: 'prioridade-arvore'}, 'a');
	
};

var adicionarPasta = function(pasta) {
	var local = $('.local-atual').data('local');
	var caminho = $('.local-atual').data('caminho');

	var classe = caminho;
	if (classe) {
		do {
			classe = classe.replace('.DS.', '-');
		} while(classe.indexOf('.DS.') > -1);

	} else {
		classe = 'raiz';
	}

	var novoCaminho = caminho ? caminho + '.DS.' + pasta : pasta;

	var novaClasse = novoCaminho;
	do {
		novaClasse = novaClasse.replace('.DS.', '-');
	} while(novaClasse.indexOf('.DS.') > -1);
	
	var html = '<li><span class="glyphicon glyphicon-folder-open text-info"></span> &nbsp;';
	html += '<a href="#" data-local="' + local + DS + pasta + '" data-caminho="' + novoCaminho + '" data-tipo="pasta" data-prioridade-arvore="0">' + pasta + '</a>';
	html += '<ul class="' + novaClasse + '"></ul>';
	html += '</a></li>';

	$(html).appendTo($('ul.' + classe));
	$('ul.' + classe + '>li').tsort('a', {data: 'prioridade-arvore'}, 'a');

};

var excluirPasta = function(pasta) {
	var caminho = $('.local-atual').data('caminho');

	$('a[data-caminho="' + caminho + '"]').parent('li').remove();

	atualizarLocalAtual($('.arvore > a'));
	trocarPainelAcoes($('.arvore > a').data('tipo'));
};

var excluirArquivo = function(arquivo) {
	var caminho = $('.local-atual').data('caminho');

	$('a[data-caminho="' + caminho + '"]').parent('li').remove();

	atualizarLocalAtual($('.arvore > a'));
	trocarPainelAcoes($('.arvore > a').data('tipo'));
};

var verificarTipoArquivo = function(arquivo) {
	if (arquivo.indexOf('.ctp') > -1 || arquivo.indexOf('.js') > -1 || arquivo.indexOf('.css') > -1) {
		return 'arquivo_editor';
	} else if (arquivo.indexOf('.jpg') > -1 || arquivo.indexOf('.jpeg') > -1 || arquivo.indexOf('.gif') > -1 || arquivo.indexOf('.png') > -1) {
		return 'arquivo_imagem';
	} else {
		return 'arquivo';
	}
};

$(document).ready(function() {
	// Clique no diretorio raiz
	$('.arvore > a').on('click', function() {
		atualizarLocalAtual($(this));
		trocarPainelAcoes($(this).data('tipo'));
		
		$('.fileupload').fileupload(
			'option',
			'url',
			WEBROOT + 'admin/cms/ajax_upload'
		);

		return false;
	});

	// Clique em qualquer diretorio da árvore, exceto o raiz, e demais arquivos
	$('body').on('click', '.arvore li a', function() {
		atualizarLocalAtual($(this));
		trocarPainelAcoes($(this).data('tipo'));
		$(this).parent().children('ul').toggle();
		if ($(this).data('tipo') == 'pasta') {
			$('.fileupload').fileupload(
				'option',
				'url',
				WEBROOT + 'admin/cms/ajax_upload/pasta:' + $(this).data('caminho')
			);
		} else if ($(this).data('tipo') == 'arquivo_imagem') {
			var local = $('.local-atual').data('local');
			//alert(local.indexOf('webroot/img/'));
			if (local.indexOf('webroot/img/') === 0) {
				var url = WEBROOT + 'theme/' + PROJETO + '/' + local.replace('webroot/', '');
				$('.visualizador-imagem').html('<img src="' + url + '">');
				$('.visualizador-nao-pode-exibir').hide();
			} else {
				$('.visualizador-imagem').html('');
				$('.visualizador-nao-pode-exibir').show();
			}
			
		}
		
		return false;
	});

	$('.arvore > ul').toggle();
	atualizarLocalAtual($('.arvore > a'));
	trocarPainelAcoes('raiz');

	// CRIAR PASTA
	$('.btn.criar-pasta').on('click', function() {
		$.post(WEBROOT + 'admin/cms/ajax_criar_pasta/pasta:' + $('.local-atual').data('caminho'), $(this).parent('form').serialize(), function(data) {
			var result = $.parseJSON(data);

			if (result.sucesso) {
				adicionarPasta(result.pasta);
			} else {
				alert(result.mensagem);
			}
		});
	});

	// EXCLUIR PASTA
	$('.btn.excluir-pasta').on('click', function() {
		if (confirm('Ao excluir excluir esta pasta todos os arquivos e subpastas serão excluídos. Se você não tem certeza que deseja exclui-la, cancele e faça um BACKUP do tema da sua loja. Deseja realmente excluir esta pasta?')) {
			$.post(WEBROOT + 'admin/cms/ajax_excluir_pasta/pasta:' + $('.local-atual').data('caminho'), function(data) {
				var result = $.parseJSON(data);

				if (result.sucesso) {
					excluirPasta(result.pasta);
				} else {
					alert(result.mensagem);
				}
			});
		}
	});

	// CRIAR ARQUIVO
	$('.btn.criar-arquivo').on('click', function() {
		$.post(WEBROOT + 'admin/cms/ajax_criar_arquivo/pasta:' + $('.local-atual').data('caminho'), $(this).parent('form').serialize(), function(data) {
			var result = $.parseJSON(data);

			if (result.sucesso) {
				adicionarArquivo(result.arquivo);
			} else {
				alert(result.mensagem);
			}
		});
	});

	// ENVIAR ARQUIVOS
	$('.fileupload').fileupload({
		dataType: 'json',
		done: function (e, data) {
			$.each(data.result.files, function (index, file) {
				adicionarArquivo(file.name);
			});
		}
	});

	// EXCLUIR ARQUIVO
	$('.btn.excluir-arquivo').on('click', function() {
		if (confirm('Antes de excluir arquivos você deve realizar um BACKUP do tema da sua loja. Deseja realmente excluir este arquivo?')) {
			$.post(WEBROOT + 'admin/cms/ajax_excluir_arquivo/arquivo:' + $('.local-atual').data('caminho'), function(data) {
				var result = $.parseJSON(data);

				if (result.sucesso) {
					excluirArquivo(result.arquivo);
				} else {
					alert(result.mensagem);
				}
			});
		}
	});

	// ABRIR ARQUIVO EDITOR
	$('.btn.abrir-editor').on('click', function() {
		var caminho = $('.local-atual').data('caminho');
		window.open(WEBROOT + 'admin/cms/editor/arquivo:' + caminho);
	});

});