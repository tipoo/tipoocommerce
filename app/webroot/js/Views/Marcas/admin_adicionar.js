$(document).ready(function() {
	$('#MarcaAdminAdicionarForm').validate(bootstrapValidateOptions);

	$("#MarcaDescricao").rules("add", {
		required: true
	});

	$("#MarcaImagem").rules("add", {
		accept: 'jpg|jpeg|png'

	});

	$('#MarcaMetaDescription').keyup(function(event){
		var num_de_caracteres = $(this).val().length;

		$('#num_de_caracteres_marca').text('Nº de caracteres:' + (-num_de_caracteres+160));

		if (num_de_caracteres > 160) {
			$('#num_de_caracteres_marca').css('color', 'red');

		} else {
			$('#num_de_caracteres_marca').css('color', 'black');
		}
	});

});