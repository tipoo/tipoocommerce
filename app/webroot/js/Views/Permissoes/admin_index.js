$(document).ready(function() {
	$('#PermissaoPerfil').change(function(){
		$.ajax({
			url: WEBROOT + 'admin/permissoes/ajax_permitidos/'+$("#PermissaoPerfil").val(),
			type: 'POST',
			success: function(data) {
				var retorno = jQuery.parseJSON(data);
				$('.checkBoxGeral').prop('checked', false);

				$(retorno.permissoes).each(function() {
					$('.checkBoxGeral[data-objeto-id="'+this.Permissao.objeto_id+'"]').prop('checked', true);
				});
			},
			error: function(data){
				alert ('Ocorreu um erro ao tentar buscar as permissões. Por favor, tente novamente');
			}
		});
	});

	$('.checkBoxGeral').click(function(){
		console.log($(this).prop('checked'));
		if($('#PermissaoPerfil').val() !== ''){

			var checado = $(this).prop('checked') ? '1' : '0';
			var idBox = $(this).val();
			$.ajax({
				url: WEBROOT + 'admin/permissoes/ajax_permitir/'+$('#PermissaoPerfil').val()+'/'+$(this).val()+'/'+checado,
				type: 'POST',
				success: function(data) {
					var retorno = jQuery.parseJSON(data);
				},
				error: function(data){
					if(checado == '1'){
						$('.checkBoxGeral[data-objeto-id="'+idBox+'"]').prop('checked', false);
					}else if(checado == '0'){
						$('.checkBoxGeral[data-objeto-id="'+idBox+'"]').prop('checked', true);
					}

					alert ('Ocorreu um erro ao tentar salvar permissões. Por favor, tente novamente');
				}
			});
		}
	});
});