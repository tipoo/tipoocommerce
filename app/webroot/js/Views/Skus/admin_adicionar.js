$(document).ready(function() {
	$('#SkuAdminAdicionarForm').validate(bootstrapValidateOptions);

	$("#SkuDescricao").rules("add", {
		required: true
	});

	$("#SkuSku").rules("add", {
		required: true
	});

	$("#SkuQtdEstoque").rules("add", {
		required: true
	});

	$("#SkuPeso").rules("add", {
		required: true
	});

	$('#SkuPeso').numeric({decimal : false, negative: false});

	$("#SkuLargura").rules("add", {
		required: true
	});

	$('#SkuLargura').numeric({decimal : false, negative: false});

	$("#SkuAltura").rules("add", {
		required: true
	});

	$('#SkuAltura').numeric({decimal : false, negative: false});

	$("#SkuProfundidade").rules("add", {
		required: true
	});

	// Validação nas caracteristicas do Mercado Livre
	if ($('#caracteristicasMercadoLivre').length) {
		$('#caracteristicasMercadoLivre select').each(function() {
			if ($(this).data('required')) {
				$(this).rules("add", {
					required: true
				});
			}
		});
	}

	var data_inicio = $('#dp1-mask').val();

	data_inicio = data_inicio.split('/');
	var startDate = new Date(data_inicio[2], data_inicio[1], data_inicio[0]);

	$('#dp1').datepicker({
		language: "pt-BR",
		startDate   : '0'
	}).on('changeDate', function(ev){

		$('#alert-datepicker').addClass('hide');
		startDate = new Date(ev.date);
		$('#startDate').text($('#dp1').data('date'));

		$('#dp1').datepicker('hide');
	});
	$('#dp1-mask').mask('99/99/9999');

	$('#SkuProfundidade').numeric({decimal : false, negative: false});

	$('#SkuQtdEstoque').numeric({decimal : false, negative: false});

	var numero_inteiro = $('.mascara-inteiro').length;
	if (numero_inteiro) {
		$('.mascara-inteiro').each(function() {
			$(this).numeric({decimal : ","});
		});
	}

	var numero_decimal = $('.mascara-decimal').length;
	if (numero_decimal) {
		$('.mascara-decimal').each(function() {
			$(this).priceFormat({
				prefix: '',
				clearPrefix: true,
				centsSeparator: ',',
    			thousandsSeparator: '.'
			});
		});

		$('.mascara-decimal').blur(function() {
			if ($(this).val() == '0,00') {
				$(this).val('');
			}
		});
	}

	$("#Imagem0Imagem").rules("add", {
		accept: 'jpg|jpeg',
		messages: {
			accept: 'A imagem deve ser jpg ou jpeg.'
		}
	});

	$('#SkuPermitePreVenda').on('change', function(){


		if ($('#SkuPermitePreVenda').is(':checked')) {
			$('.previsao-entrega').removeClass('hide');


		} else {
			$('.previsao-entrega').addClass('hide');
			$('#dp1-mask').val('');
		}

	});

	$('#dp1-mask').on('change', function() {

		if ($('#SkuPermitePreVenda').is(':checked')) {

			$('#dp1').removeClass('has-error text-danger');
			$('.alerta-previsao-entrega').remove();

			var data_previsao = $('#dp1-mask').val();

			if (data_previsao == '' && $('#SkuCarenciaPreVenda').val() == '') {
				$('#dp1').append('<span class="alerta-previsao-entrega help-inline">Campo obrigatório.</span>');
				$('#dp1').addClass('form-group has-error text-danger');
			} else {
				$('#SkuCarenciaPreVenda').val('');
			}
		}
	});

	$('#SkuCarenciaPreVenda').numeric({decimal : false, negative: false});

	$('#SkuCarenciaPreVenda').on('change', function() {
		$('#dp1').removeClass('has-error text-danger');
		$('.alerta-previsao-entrega').remove();
		$('#dp1-mask').val('');
	});

	$('#SkuAdminAdicionarForm').on('submit', function(){

		var retorno = true;

		if ($('#SkuPermitePreVenda').is(':checked') && $('#SkuCarenciaPreVenda').val() == '') {

			$('#dp1').removeClass('has-error text-danger');
			$('.alerta-previsao-entrega').remove();

			var data_previsao = $('#dp1-mask').val();
			var data_atual = $('#SkuDataAtual').val();

			if (data_previsao == '') {

				$('#dp1').append('<span class="alerta-previsao-entrega help-inline">Campo obrigatório.</span>');
				$('#dp1').addClass('form-group has-error text-danger');
				$('.btn-action-save-default').button('reset');
				retorno = false;

			} else if(data_previsao == data_atual ){

				retorno = confirm('Você tem certeza que a data da previsão de entrega "' + data_previsao + '" está correta ?');
				$('.btn-action-save-default').button('reset');
			}

		}

		return retorno;

	});

});