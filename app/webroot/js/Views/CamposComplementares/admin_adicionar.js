$(document).ready(function() {
	$('#CamposComplementarAdminAdicionarForm').validate(bootstrapValidateOptions);

	$("#CamposComplementarDescricao").rules("add", {
		required: true
	});

	$("#CamposComplementarTipo").rules("add", {
		required: true
	});

	$("#CamposComplementarReferente").rules("add", {
		required: true
	});

	$('.campo-mascara').hide();

	$('#CamposComplementarMascara').closest('.control-group').hide();
	$('#CamposComplementarTipo').change(function() {
		if ($(this).val() == 'N') {
			$('.campo-mascara').show();
			$('#CamposComplementarMascara').closest('.control-group').show();
			$("#CamposComplementarMascara").rules("add", {
				required: true
			});
		} else {
			$('.campo-mascara').hide();
			$('#CamposComplementarMascara').val('');
			$('#CamposComplementarMascara').closest('.control-group').hide();
			$("#CamposComplementarMascara").rules("add", {
				required: false
			});
		}
	});

});