$(document).ready(function() {
	$('#CategoriaAdminEditarForm').validate(bootstrapValidateOptions);

	$("#CategoriaDescricao").rules("add", {
		required: true
	});

	var num_de_caracteres = $('#CategoriaMetaDescription').val().length;

	$('#num_de_caracteres_categoria').text('Nº de caracteres:' + (-num_de_caracteres+160));

	if (num_de_caracteres > 160) {
		$('#num_de_caracteres_categoria').css('color', 'red');

	} else {
		$('#num_de_caracteres_categoria').css('color', 'black');
	}

	$('#CategoriaMetaDescription').keyup(function(event){
		var num_de_caracteres = $(this).val().length;

		$('#num_de_caracteres_categoria').text('Nº de caracteres:' + (-num_de_caracteres+160));

		if (num_de_caracteres > 160) {
			$('#num_de_caracteres_categoria').css('color', 'red');

		} else {
			$('#num_de_caracteres_categoria').css('color', 'black');
		}
	});

	mercadoLivre.init();
});

var mercadoLivre = {
	init: function() {
		this.onClickCategory();
	},
	onClickCategory: function() {
		var self = this;

		$(document).on('click', '.mercado-livre-categorias a', function(e) {
			e.preventDefault();

			var $this = $(this);
			var ajax = $this.data('ajax');
			var has_childrens = $this.data('has-childrens');
			var categoria_id = $this.data('categoria-id');

			if (!ajax && has_childrens) {
				self.ajaxGetChildrenCategory($this, categoria_id);
			} else {
				if (has_childrens) {
					if ($this.children('span').hasClass('glyphicon-plus-sign')) {
						$this.children('span').removeClass('glyphicon-plus-sign');
						$this.children('span').addClass('glyphicon-minus-sign');
					} else if ($this.children('span').hasClass('glyphicon-minus-sign')) {
						$this.children('span').removeClass('glyphicon-minus-sign');
						$this.children('span').addClass('glyphicon-plus-sign');
					}

					$this.next('ul').slideToggle();
				} else {
					var categoria_descricao = self.buildBreadcrumb($this);
					var tipo_atributo = $this.data('tipo-atributo');

					$('#CategoriaMercadoLivreCategoriaId').val(categoria_id);
					$('#CategoriaMercadoLivreCategoriaDescricao').val(categoria_descricao.trim());
					$('#CategoriaMercadoLivreCategoriaTipoAtributo').val(tipo_atributo);


					$('.mercado-livre-categorias a').removeClass('text-success');
					$('.mercado-livre-categorias').find('.glyphicon-ok').removeClass('glyphicon-ok').addClass('glyphicon-unchecked');

					$this.children('span').removeClass('glyphicon-unchecked').addClass('glyphicon-ok');
					$this.addClass('text-success');
				}
			}
		});
	},
	ajaxGetChildrenCategory: function($el, categoria_pai_id) {
		var self = this;

		$.ajax({
			url: WEBROOT + 'admin/categorias/ajax_buscar_sub_categorias/' + categoria_pai_id,
			type: 'POST',
			beforeSend: function() {
				$el.children('span').hide();
				$el.children('.carregando').show();
			},
			success: function(data) {
				$el.children('.carregando').hide();
				$el.children('span').show();

				var retorno = jQuery.parseJSON(data);

				if (retorno.sucesso) {

					var html = self.buildHtml(retorno.sub_categorias);

					if (html == '') {
						var categoria_descricao = self.buildBreadcrumb($el);

						$el.data('has-childrens', false);
						$el.attr('data-tipo-atributo', retorno.tipo_atributo);
						$('#CategoriaMercadoLivreCategoriaId').val(categoria_pai_id);
						$('#CategoriaMercadoLivreCategoriaDescricao').val(categoria_descricao.trim());
						$('#CategoriaMercadoLivreCategoriaTipoAtributo').val(retorno.tipo_atributo);

						$('.mercado-livre-categorias a').removeClass('text-success');
						$('.mercado-livre-categorias').find('.glyphicon-ok').removeClass('glyphicon-ok').addClass('glyphicon-unchecked');

						$el.children('span').removeClass('glyphicon-plus-sign');
						$el.children('span').addClass('glyphicon-ok');
						$el.addClass('text-success');
					} else {
						$el.data('ajax', true);
						$el.after(html);
						$el.next('ul').slideDown();
						$el.children('span').removeClass('glyphicon-plus-sign');
						$el.children('span').addClass('glyphicon-minus-sign');
					}

				} else {
					alert('Ocorreu um erro ao tentar consultar as sub-categorias. Por favor, tente novamente.');
				}

			},
			error: function(data) {
				alert ('Ocorreu um erro ao tentar consultar as sub-categorias. Por favor, tente novamente.');
				$el.children('.carregando').hide();
				$el.children('span').show();
			}
		});
	},
	buildHtml: function(sub_categorias) {
		var html = '';

		if (Object.keys(sub_categorias).length) {
			html = '<ul style="display: none">';
			$.each(sub_categorias, function(index, value) {
				html += '<li><a class="" href="#" data-categoria-id="' + index + '" data-ajax="false" data-has-childrens="true"><img src="' + WEBROOT + 'img/admin-ajax-loader-min-blue.gif" class="carregando" style="display: none""><span class="glyphicon glyphicon-plus-sign"></span> ' + value + '</a></li>';
			});
			html += '</ul>';
		}

		return html;
	},
	buildBreadcrumb: function($el) {
		var parents = $el.parents('li').not('.mercado-livre-categorias-list-group-item');
		[].reverse.call(parents);

		var html = '';
		parents.each(function(e) {
			if (e == 0) {
				html += $(this).children('a').text().trim();
			} else {
				html += ' > ' + $(this).children('a').text().trim();
			}
		})

		return html;
	}
}