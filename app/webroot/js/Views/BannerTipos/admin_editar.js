$(document).ready(function() {
	$('#BannerTipoAdminEditarForm').validate(bootstrapValidateOptions);

	$("#BannerTipoDescricao").rules("add", {
		required: true
	});

	$("#BannerTipoChave").rules("add", {
		required: true
	});

	$("#BannerTipoTipo").rules("add", {
		required: true
	});
	$('#BannerTipoQtd').numeric({decimal : false, negative: false});
});