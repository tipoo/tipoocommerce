selectCategoria = function(thisCategoria, pai) {

	$(thisCategoria.children).each(function(e) {
		if (e != 0) {
			pai = ' - ';
		}
		$('#CategoriaId').append($("<option />").val(this.Categoria.id).text(pai + this.Categoria.descricao));
		if (this.children.length) {
			pai += ' - ';
			selectCategoria(this, pai);
		}
	});
}

$(document).ready(function() {
	$('#BannerTipoAdminAdicionarForm').validate(bootstrapValidateOptions);

	$("#BannerTipoDescricao").rules("add", {
		required: true
	});

	$("#BannerTipoChave").rules("add", {
		required: true
	});

	$("#BannerTipoTipo").rules("add", {
		required: true
	});

	$('#BannerTipoQtd').numeric({decimal : false, negative: false});

	$('#BannerTipoTipo').change(function() {
		var this_tipo = $(this).val();

		if (this_tipo == 'marca' || this_tipo == 'categoria') {

			$.ajax({
				url: WEBROOT + 'admin/bannerTipos/ajax_buscar_' + this_tipo + 's',
				type: 'POST',
				beforeSend: function() {
					$('#Pagina .select').empty();
					$('#Pagina .control-label').empty();
					$('#Pagina').show();
					$('#Pagina .select').html('<img class="carregando" src="' + WEBROOT + 'img/ajax-loader.gif" />');
				},
				success: function(data) {
					var retorno = jQuery.parseJSON(data);

					if (retorno.sucesso) {

						if (this_tipo == 'categoria') {

							// Categoria

							$('#Pagina .control-label').text('Categoria*');

							$('#Pagina .select').append('<select name="data[BannerTipo][categoria_id]" class="form-control row="1" id="CategoriaId" style="display:none;"><option value="">Selecione</option></select>');
							$(retorno.categorias).each(function() {
								$('#CategoriaId').append($("<option />").val(this.Categoria.id).text(this.Categoria.descricao));
								if (this.children.length) {
									var pai = ' - ';
									selectCategoria(this, pai);
								}
							});

							$('#CategoriaId').rules("add", {
								required: true
							});

							$('#CategoriaId').show();

						} else if (this_tipo == 'marca') {

							// Marca

							$('#Pagina .control-label').text('Marca*');

							$('#Pagina .select').append('<select name="data[BannerTipo][marca_id]" class="form-control " id="MarcaId" style="display:none;"><option value="">Selecione</option></select>');
							$(retorno.marcas).each(function() {
								$('#MarcaId').append($("<option />").val(this.Marca.id).text(this.Marca.descricao));
							});

							$('#MarcaId').rules("add", {
								required: true
							});

							$('#MarcaId').show();
						}

						$('.carregando').remove();

					} else {
						alert(retorno.mensagem);
						$('#Pagina .controls').empty();
						$('#Pagina').hide();
					}

				},
				error: function(data){
					alert ('Ocorreu um erro ao tentar alterar o status. Por favor, tente novamente.');
					$('.carregando').remove();
					this_pedido.append(thisChildrens[pedido_id]);
				}
			});
		} else {
			$('#Pagina .controls').empty();
			$('#Pagina').hide();
		}
	});
});