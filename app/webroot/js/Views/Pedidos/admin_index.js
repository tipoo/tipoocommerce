$(document).ready(function() {

	var deFiltro = $('#dp1-mask').val();
	var ateFiltro = $('#dp2-mask').val();

	deFiltro = deFiltro.split('/');
	var startDate = new Date(deFiltro[2], deFiltro[1], deFiltro[0]);

	ateFiltro = ateFiltro.split('/');
	var endDate = new Date(ateFiltro[2], ateFiltro[1], ateFiltro[0]);

	$('#dp1').datepicker({
		language: "pt-BR"
	}).on('changeDate', function(ev){

		var deFiltro = $('#dp1-mask').val();
		var ateFiltro = $('#dp2-mask').val();

		deFiltro = deFiltro.split('/');
		var dataDe = deFiltro[2] + '-' + deFiltro[1] + '-' + deFiltro[0];

		ateFiltro = ateFiltro.split('/');
		var dataAte = ateFiltro[2] + '-' + ateFiltro[1] + '-' + ateFiltro[0];

		if (dataDe > dataAte) {
			$('#alert-datepicker').removeClass('hide').find('span').text('A data inicial não pode ser maior que a data final.');

		} else {

			$('#alert-datepicker').addClass('hide');
			$('#dp1').datepicker('hide');

		}
	});

	$('#dp2').datepicker({
		language: "pt-BR"
	}).on('changeDate', function(ev){

		var deFiltro = $('#dp1-mask').val();
		var ateFiltro = $('#dp2-mask').val();

		deFiltro = deFiltro.split('/');
		var dataDe = deFiltro[2] + '-' + deFiltro[1] + '-' + deFiltro[0];

		ateFiltro = ateFiltro.split('/');
		var dataAte = ateFiltro[2] + '-' + ateFiltro[1] + '-' + ateFiltro[0];

		if (dataDe > dataAte) {
			$('#alert-datepicker').removeClass('hide').find('span').text('A data final não pode ser menor que a data inicial.');

		} else {

			$('#alert-datepicker').addClass('hide');
			$('#dp2').datepicker('hide');

		}
	});

	$('#dp1-mask').mask('99/99/9999');
	$('#dp2-mask').mask('99/99/9999');

	$(".generic-modal").click(function(e) {
		var url = $(this).attr('href');
		e.preventDefault();

		$.ajax({
			url: url,
			type: 'POST',
			success: function(data){
				bootbox.dialog({
					size: 'large',
					message: data,
					title: 'Pedido',
					buttons: {
						main: {
							label: 'Fechar X',
							className: 'btn-danger'
						}
					}
				}).on('shown.bs.modal', function(e) {
					// Marketplaces - Mercado Livre
					if ($('.meli-feedback').length) {
						marketplaces.mercadoLivre.feedback.init();
					}
				});
			},
			error: function(data){
				alert ('Ocorreu um erro ao tentar ao tentar buscar os dados.');
			}
		});
	});

	$("#enviarFiltros").click(function() {
		var deFiltro = $('#dp1-mask').val();
		var ateFiltro = $('#dp2-mask').val();
		var statusPedidoFiltroId = $('#statusPedidoFiltroId').val();

		deFiltro = deFiltro.split('/');
		var dataDe = deFiltro[2] + '-' + deFiltro[1] + '-' + deFiltro[0];

		ateFiltro = ateFiltro.split('/');
		var dataAte = ateFiltro[2] + '-' + ateFiltro[1] + '-' + ateFiltro[0];

		var de = new XDate(deFiltro[2], deFiltro[1] - 1, deFiltro[0]);
		var ate = new XDate(ateFiltro[2], ateFiltro[1] - 1, ateFiltro[0]);

		if (de > ate) {
			$('#alert-datepicker').removeClass('hide').find('span').text('A data "De" deve ser menor que a data "Até".');
			return false
		} else {
			$('#alert-datepicker').addClass('hide');
			var url = WEBROOT + 'admin/pedidos/index/de:' + dataDe +'/ate:' + dataAte + '/statusPedidoFiltroId:' + statusPedidoFiltroId;
			document.location.href = url;
		}

	});

	thisChildrens = new Object();
	$(document).on("click", ".mudar-status", function(e) {
		e.preventDefault();

		var pedido_id = $(this).attr('rel');
		var this_pedido = $(this).closest('td');
		var pedido_status_id = this_pedido.children('.pedido-status-id').val();
		thisChildrens[pedido_id] = this_pedido.children();

		$.ajax({
			url: WEBROOT + 'admin/statusPedidos/ajax_carregar_status/' + pedido_id,
			type: 'POST',
			beforeSend: function() {
				$('.mudar-status').addClass('disabled');
				this_pedido.html('<p class="carregando">Aguarde...</p>');
			},
			success: function(data) {
				var retorno = jQuery.parseJSON(data);

				if (retorno.sucesso) {

					this_pedido.append('<select name="data[StatusPedido][id]" class="form-control status-pedido-id"></select>');
					$(retorno.pedidosStatus).each(function() {
						if (this.StatusPedido.id == pedido_status_id) {
							this_pedido.children('.status-pedido-id').append($('<option selected="selected" />').val(this.StatusPedido.id).text(this.StatusPedido.descricao));
						} else {
							if (this.StatusPedido.requer_estoque_disponivel == '1' && retorno.pedido.estoque_disponivel == '0') {
								this_pedido.children('.status-pedido-id').append($('<option />').val(this.StatusPedido.id).text(this.StatusPedido.descricao).attr('disabled', true).addClass('disabled').attr('data-codigo-rastreamento', this.StatusPedido.codigo_rastreamento));
							} else {
								this_pedido.children('.status-pedido-id').append($('<option />').val(this.StatusPedido.id).text(this.StatusPedido.descricao).attr('data-codigo-rastreamento', this.StatusPedido.codigo_rastreamento));
							}
						}
					});

					this_pedido.append('<input type="hidden" class="pedido-id" value="' + pedido_id + '" />');
					this_pedido.append('<a class="cancelar" href="#">Cancelar</a> | <a class="mudar-staus-ok" href="#">OK</a><br />');

				} else {
					alert('Ocorreu um erro ao tentar consultar os status. Por favor, tente novamente.');
					this_pedido.append(thisChildrens[pedido_id]);
				}

				$('.carregando').remove();
			},
			error: function(data){
				alert ('Ocorreu um erro ao tentar consultar os status. Por favor, tente novamente.');
				$('.mudar-status').removeClass('disabled');
				$('.carregando').remove();
				this_pedido.append(thisChildrens[pedido_id]);
			}
		});
	});

	$(document).on("click", '.cancelar', function(e) {
		e.preventDefault();

		var pedido_id = $(this).closest('td').children('.pedido-id').val();
		$('#status_' + pedido_id).html(thisChildrens[pedido_id]);
		$('.mudar-status').removeClass('disabled');
	});

	$(document).on("change", ".status-pedido-id", function(e) {
		var status_selecionado = $(this).val();
		var requer_codigo_rastreamento = $(this).children('option[value="' + status_selecionado + '"]').data('codigo-rastreamento');

		$('.codigo-rastreamento-pedido').remove();

		if (requer_codigo_rastreamento) {
			$(this).after('<input type="text" class="form-control codigo-rastreamento-pedido" placeholder="Cód. de rastreamento" />');
		}
	});

	$(document).on("click", ".mudar-staus-ok", function(e) {
		e.preventDefault();

		var this_pedido = $(this).closest('td');
		var pedido_id = this_pedido.children('.pedido-id').val();
		var status_id = this_pedido.children('.status-pedido-id').val();

		var codigo_rastreamento = '';
		if (this_pedido.children('.codigo-rastreamento-pedido').length) {
			var codigo_rastreamento = this_pedido.children('.codigo-rastreamento-pedido').val();
		}

		$.ajax({
			url: WEBROOT + 'admin/pedidos/ajax_alterar_status/' + pedido_id + '/' + status_id + '/' + codigo_rastreamento,
			type: 'POST',
			beforeSend: function() {
				this_pedido.html('<p class="carregando">Aguarde...</p>');
			},
			success: function(data) {
				var retorno = jQuery.parseJSON(data);

				if (retorno.sucesso) {
					var status_cor = '#333';
					if (retorno.pedido.StatusPedido.status_cor != null && retorno.pedido.StatusPedido.status_cor != '') {
						status_cor = retorno.pedido.StatusPedido.status_cor;
					}

					var link_mudar_status = '';
					if (retorno.pedido.StatusPedido.status_finalizado == '0') {
						link_mudar_status = '<a class="mudar-status" href="#" rel="' + retorno.pedido.Pedido.id + '">Mudar Status</a>';
					} else {
						link_mudar_status = '<br />';
						this_pedido.closest('tr').removeClass('success');
					}

					this_pedido.append('<input type="hidden" class="pedido-status-id" value="' + retorno.pedido.StatusPedido.id + '" />' +
										'<span class="label" style="background:' + status_cor + '">' + retorno.pedido.StatusPedido.descricao + '</span>' +
										'<br />' +
										link_mudar_status);

				} else {
					alert(retorno.mensagem);
					this_pedido.append(thisChildrens[pedido_id]);
				}

				$('.carregando').remove();
				$('.mudar-status').removeClass('disabled');

			},
			error: function(data){
				alert ('Ocorreu um erro ao tentar alterar o status. Por favor, tente novamente.');
				this_pedido.append(thisChildrens[pedido_id]);
				$('.mudar-status').removeClass('disabled');
				$('.carregando').remove();
			}
		});
	});

	$('.btn-imprimir').on('click', function() {
		$('#PedidoAdminIndexForm').submit();
	});

	$('#PedidoAdminIndexForm').submit(function() {

		var retorno = true;
		var i = $('.imprimir input:checked').size();

		if (i == 0) {
			alert('É necessário selecionar ao menos um pedido para impressão.')
			retorno = false;
		}

		return retorno;
	});
});

var marketplaces = {
	mercadoLivre: {
		feedback: {
			init: function() {
				this.validationOn();
				this.changeFulfilled();
				this.countCharactersMessage();

				if ($('.modificar-feedback').length) {
					this.modify();
					this.cancel();
					this.send('put');
				} else {
					this.send('post');
				}

				this.reply.init();
			},
			modify: function() {
				var self = this;

				$(document).on('click', '.meli-feedback .btn-modificar-feedback', function(e) {
					e.preventDefault();

					$('.meli-feedback-venda .feedback').addClass('hide');
					$('.meli-feedback .modificar-feedback').removeClass('hide');
					self.validationOn();

					if ($('#MercadoLivreFeedbackSaleFulfilled').val().length && $('#MercadoLivreFeedbackSaleFulfilled').val() == false) {
						$('.meli-feedback .razoes').show();

						$('#MercadoLivreFeedbackSaleReason').rules('add', {
							required: true
						});

						$('#MercadoLivreFeedbackSaleRating option[value="POSITIVE"]').attr('disabled', true);
					}

				});
			},
			cancel: function() {
				var self = this;

				$(document).on('click', '.meli-feedback .btn-cancelar', function(e) {
					e.preventDefault();

					$('.meli-feedback-venda .feedback').removeClass('hide');
					$('.meli-feedback .modificar-feedback').addClass('hide');
					self.validationOff();
				});
			},
			validationOn: function() {
				$('#MercadoLivreFeedbackSaleAdminAjaxVisualizarForm').validate(bootstrapValidateOptions);

				$('#MercadoLivreFeedbackSaleFulfilled').rules('add', {
					required: true
				});

				$('#MercadoLivreFeedbackSaleRating').rules('add', {
					required: true
				});

				$('#MercadoLivreFeedbackSaleMessage').rules('add', {
					required: true
				});
			},
			validationOff: function() {
				$('#MercadoLivreFeedbackSaleFulfilled').rules('add', {
					required: false
				});

				$('#MercadoLivreFeedbackSaleRating').rules('add', {
					required: false
				});

				$('#MercadoLivreFeedbackSaleMessage').rules('add', {
					required: false
				});
			},
			changeFulfilled: function() {
				$('#MercadoLivreFeedbackSaleFulfilled').on('change', function() {
					if ($(this).val().length && $(this).val() == false) {
						$('.meli-feedback .razoes').show();

						$('#MercadoLivreFeedbackSaleReason').rules('add', {
							required: true
						});

						$('#MercadoLivreFeedbackSaleRating').val('');
						$('#MercadoLivreFeedbackSaleRating option[value="POSITIVE"]').attr('disabled', true);
					} else {
						$('.meli-feedback .razoes').hide();
						$('#MercadoLivreFeedbackSaleReason').val('');

						$('#MercadoLivreFeedbackSaleReason').rules('add', {
							required: false
						});

						$('#MercadoLivreFeedbackSaleRating option[value="POSITIVE"]').attr('disabled', false);
					}
				});
			},
			countCharactersMessage: function() {
				$('.meli-feedback .num-caracteres').text('Nº de caracteres:' + (-$('#MercadoLivreFeedbackSaleMessage').val().length+160));

				$('#MercadoLivreFeedbackSaleMessage').keyup(function(event){
					var count = $(this).val().length;

					$('.meli-feedback .num-caracteres').text('Nº de caracteres:' + (-count+160));

					if (count > 160) {
						$('.meli-feedback .num-caracteres').css('color', 'red');
					} else {
						$('.meli-feedback .num-caracteres').css('color', 'black');
					}
				});
			},
			send: function(type) {
				$('#MercadoLivreFeedbackSaleAdminAjaxVisualizarForm').on('submit', function() {
					if ($(this).closest('form').valid()) {

						$.ajax({
							url: WEBROOT + 'admin/marketplaces/ajax_enviar_feedback/' + type,
							type: 'POST',
							data: $('#MercadoLivreFeedbackSaleAdminAjaxVisualizarForm').serialize(),
							beforeSend: function() {
								$('#MercadoLivreFeedbackSaleAdminAjaxVisualizarForm input[type="submit"]').button('loading');
								$('#MercadoLivreFeedbackSaleAdminAjaxVisualizarForm .btn-cancelar').button('loading');
							},
							success: function(data) {
								var retorno = jQuery.parseJSON(data);

								if (retorno.sucesso) {
									$('.meli-feedback-venda .enviar-feedback').hide();
									$('.meli-feedback-venda .modificar-feedback').hide();
									$('.meli-feedback-venda h4').after('<div class="alert alert-success text-center">Feedback enviado com sucesso!</div>');
								} else {
									alert('O Mercado Livre está retornando um erro ao tentar enviar o feedback. Por favor, informe o suporte técnico');
								}

								$('#MercadoLivreFeedbackSaleAdminAjaxVisualizarForm input[type="submit"]').button('reset');
								$('#MercadoLivreFeedbackSaleAdminAjaxVisualizarForm .btn-cancelar').button('reset');
							},
							error: function(data){
								alert('Ocorreu um erro ao tentar ao tentar enviar o feedback.');
								$('#MercadoLivreFeedbackSaleAdminAjaxVisualizarForm input[type="submit"]').button('reset');
								$('#MercadoLivreFeedbackSaleAdminAjaxVisualizarForm .btn-cancelar').button('reset');
							}
						});

					}
					return false;
				});
			},
			reply: {
				init: function() {
					if ($('.meli-feedback-compra .reply-feedback').length) {
						this.show();
						this.cancel();
						this.countCharactersMessageReply();
						this.send();
					}
				},
				show: function() {
					var self = this;

					$(document).on('click', '.meli-feedback .btn-reply-feedback', function(e) {
						e.preventDefault();

						$('.meli-feedback-compra .reply-feedback').removeClass('hide');
						$(this).addClass('hide');
						self.validationOn();
					});
				},
				cancel: function() {
					var self = this;

					$(document).on('click', '.meli-feedback .btn-cancelar-replica', function(e) {
						e.preventDefault();

						$('.meli-feedback-compra .reply-feedback').addClass('hide');
						$('.meli-feedback .btn-reply-feedback').removeClass('hide');
						self.validationOff();
					});
				},
				validationOn: function() {
					$('#MercadoLivreFeedbackReplyAdminAjaxVisualizarForm').validate(bootstrapValidateOptions);

					$('#MercadoLivreFeedbackReplyMessage').rules('add', {
						required: true
					});
				},
				validationOff: function() {
					$('#MercadoLivreFeedbackReplyMessage').rules('add', {
						required: false
					});
				},
				countCharactersMessageReply: function() {
					$('.meli-feedback-compra .reply-feedback .num-caracteres-reply').text('Nº de caracteres:' + (-$('#MercadoLivreFeedbackReplyMessage').val().length+250));

					$('#MercadoLivreFeedbackReplyMessage').keyup(function(event){
						var count = $(this).val().length;

						$('.meli-feedback-compra .reply-feedback .num-caracteres-reply').text('Nº de caracteres:' + (-count+250));

						if (count > 250) {
							$('.meli-feedback-compra .reply-feedback .num-caracteres-reply').css('color', 'red');
						} else {
							$('.meli-feedback-compra .reply-feedback .num-caracteres-reply').css('color', 'black');
						}
					});
				},
				send: function() {
					$('#MercadoLivreFeedbackReplyAdminAjaxVisualizarForm').on('submit', function() {
						if ($(this).closest('form').valid()) {

							$.ajax({
								url: WEBROOT + 'admin/marketplaces/ajax_reply_feedback',
								type: 'POST',
								data: $('#MercadoLivreFeedbackReplyAdminAjaxVisualizarForm').serialize(),
								beforeSend: function() {
									$('#MercadoLivreFeedbackReplyAdminAjaxVisualizarForm input[type="submit"]').button('loading');
									$('#MercadoLivreFeedbackReplyAdminAjaxVisualizarForm .btn-cancelar-replica').button('loading');
								},
								success: function(data) {
									var retorno = jQuery.parseJSON(data);

									if (retorno.sucesso) {
										$('.meli-feedback-compra .reply-feedback').addClass('hide');
										$('.meli-feedback .btn-reply-feedback').after('<div class="alert alert-success text-center">Réplica enviada com sucesso!</div>');
									} else {
										alert('O Mercado Livre está retornando um erro ao tentar enviar a réplica. Por favor, informe o suporte técnico');
									}

									$('#MercadoLivreFeedbackReplyAdminAjaxVisualizarForm input[type="submit"]').button('reset');
									$('#MercadoLivreFeedbackReplyAdminAjaxVisualizarForm .btn-cancelar-replica').button('reset');
								},
								error: function(data){
									alert('Ocorreu um erro ao tentar ao tentar enviar a réplica.');
									$('#MercadoLivreFeedbackReplyAdminAjaxVisualizarForm input[type="submit"]').button('reset');
									$('#MercadoLivreFeedbackReplyAdminAjaxVisualizarForm .btn-cancelar-replica').button('reset');
								}
							});

						}
						return false;
					});
				}
			}
		}
	}
}