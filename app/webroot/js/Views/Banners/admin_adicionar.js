$(document).ready(function() {
	$('#BannerAdminAdicionarForm').validate(bootstrapValidateOptions);

	$('#BannerDescricao').rules("add", {
		required: true
	});

	$('#BannerDataInicio').rules("add", {
		required: true
	});

	$('#BannerImagem').rules("add", {
		required: true,
		accept: 'jpg|jpeg|png|gif'
	});

	$('#BannerLink').rules("add", {
		//url: true
	});

	$('#BannerDataInicio').mask('99/99/9999 99:99:99');
	$('#BannerDataFim').mask('99/99/9999 99:99:99');

	var nowTemp = new Date();
	var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
});