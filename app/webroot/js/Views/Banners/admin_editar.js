$(document).ready(function() {
	$('#BannerAdminEditarForm').validate(bootstrapValidateOptions);

	$('#BannerDescricao').rules("add", {
		required: true
	});

	$('#BannerDataInicio').rules("add", {
		required: true
	});

	$('#BannerLink').rules("add", {
		//url: true
	});

	$('#BannerImagem').rules("add", {
		accept: 'jpg|jpeg|png|gif'
	});

	$('#BannerDataInicio').mask('99/99/9999 99:99:99');
	$('#BannerDataFim').mask('99/99/9999 99:99:99');
});