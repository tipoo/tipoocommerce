$(document).ready(function() {

	$('#LojaAdminAdicionarForm').validate(bootstrapValidateOptions);

	$('#LojaCep').mask('99999-999');

	$("#LojaDescricao").rules("add", {
		required: true
	});
	$("#LojaEndereco").rules("add", {
		required: true
	});
	$("#LojaNumero").rules("add", {
		required: true
	});
	$("#LojaBairro").rules("add", {
		required: true
	});
	$("#LojaCidade").rules("add", {
		required: true
	});
	$("#LojaEstado").rules("add", {
		required: true
	});

	$('#LojasBuscarCep').on('click', function() {

		$('.input-group').removeClass('has-error text-danger');
		$('.cep-obrigatorio').remove();


		$('.dados-endereco').addClass('hide');
		$('.add-logradouro').addClass('hide');
		$('.salvar-loja').addClass('hide');

		$('#LojaEndereco').val('');
		$('#LojaDescricao').val('');
		$('#LojaNumero').val('');
		$('#LojaBairro').val('');
		$('#LojaCidadeId').val('');
		$('#LojaCidade').val('');
		$('#LojaEstado').val('');

		var cep = $('#LojaCep').val();

		if (cep != '') {

			var url =  WEBROOT + 'admin/lojas/ajax_buscar_cep/' + cep;

			$.ajax({
				url: url,
				dataType: 'json',
				beforeSend: function() {
					$('#nao-sei-meu-cep').after('<div class="carregando-endereco"><img class="ajax-loader" src="' + WEBROOT + 'img/ajax-loader.gif"></div>');

				},
				success: function(data) {

					$('.carregando-endereco').remove();

					if (data.sucesso) {
						$('.dados-endereco').removeClass('hide');
						$('#LojaEndereco').val(data.endereco);
						$('#LojaBairro').val(data.bairro);
						$('#LojaCidadeId').val(data.cidade_id);
						$('#LojaCidade').val(data.cidade);
						$('#LojaEstado').val(data.estado);

						if (data.endereco === '') {
							$('#LojaEndereco').focus();
						} else {
							$('#LojaNumero').focus();
						}

						$('.salvar-loja').removeClass('hide');

					} else {
						var url = $('.add-logradouro a').attr('href');
						url += '/' + cep + '/lj';
						$('.add-logradouro a').attr('href', url);
						$('.add-logradouro').removeClass('hide');
					}
				},
				error: function(jqXHR, textStatus, errorThrown ) {
					$('.carregando-endereco').remove();
					alert('Erro...');
				}
			});

		}


	});

	$('#LojaCep').blur(function() {

		$('.dados-endereco').addClass('hide');
		$('.add-logradouro').addClass('hide');
		$('.salvar-loja').addClass('hide');
		$('#LojaEndereco').val('');
		$('#LojaDescricao').val('');
		$('#LojaNumero').val('');
		$('#LojaBairro').val('');
		$('#LojaCidadeId').val('');
		$('#LojaCidade').val('');
		$('#LojaEstado').val('');
	});

});