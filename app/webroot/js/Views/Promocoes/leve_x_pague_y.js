$(document).ready(function() {

	$('#PromocaoAdminPromocaoForm').validate(bootstrapValidateOptions);

	$.validator.addMethod(
		"validacaoHora",
		function (value,element){
			return this.optional(element) || /^(([0-1]?[0-9])|([2][0-3])):([0-5]?[0-9])(:([0-5]?[0-9]))?$/i.test(value);
		},
		"Por Favor, entre com uma hora válida."
	);

	$("#PromocaoDescricao").rules("add", {
		required: true
	});

	$('#dp1-mask').rules("add", {
		required: true
	});
	$('#dp2-mask').rules("add", {
		required: true
	});

	$("#PromocaoHoraInicio").rules("add", {
		required: true,
		validacaoHora: "Campo obrigatório"
	});

	$("#PromocaoHoraFim").rules("add", {
		required: true,
		validacaoHora: "Campo obrigatório"
	});

	$("#PromocaoEfeitoValor").rules("add", {
		required: true
	});
	$("#PromocaoCausaQtdPecas").rules("add", {
		required: true
	});

	$('#PromocaoDataInicio').mask('99/99/9999');

	$('#PromocaoDataFim').mask('99/99/9999');

	$('#PromocaoHoraInicio').mask('99:99:99');

	$('#PromocaoHoraFim').mask('99:99:99');

	$('#PromocaoCausaCepInicio').mask('99999-999');

	$('#PromocaoCausaCepFim').mask('99999-999');

	$('#PromocaoCausaQtdPecas').numeric({decimal : false, negative: false});

 	$('#PromocaoCausaQtdPecas').blur(function() {
		if ($(this).val() == '0,00' || $(this).val() == '0') {
			$(this).val('');
		}
	});

	var deFiltro = $('#dp1-mask').val();
	var ateFiltro = $('#dp2-mask').val();

	deFiltro = deFiltro.split('/');
	var startDate = new Date(deFiltro[2], deFiltro[1], deFiltro[0]);

	ateFiltro = ateFiltro.split('/');
	var endDate = new Date(ateFiltro[2], ateFiltro[1], ateFiltro[0]);

	$('#dp1').datepicker({
		language: "pt-BR",
		startDate   : '0'
	}).on('changeDate', function(ev){

		var deFiltro = $('#dp1-mask').val();
		var ateFiltro = $('#dp2-mask').val();

		deFiltro = deFiltro.split('/');
		var dataDe = deFiltro[2] + '-' + deFiltro[1] + '-' + deFiltro[0];

		ateFiltro = ateFiltro.split('/');
		var dataAte = ateFiltro[2] + '-' + ateFiltro[1] + '-' + ateFiltro[0];

		if (dataDe > dataAte) {
			$('#alert-datepicker').removeClass('hide').find('span').text('A data inicial não pode ser maior que a data final.');

		} else {

			$('#alert-datepicker').addClass('hide');
			$('#dp1').datepicker('hide');

		}
	});

	$('#dp2').datepicker({
		language: "pt-BR",
		startDate   : '0'
	}).on('changeDate', function(ev){

		var deFiltro = $('#dp1-mask').val();
		var ateFiltro = $('#dp2-mask').val();

		deFiltro = deFiltro.split('/');
		var dataDe = deFiltro[2] + '-' + deFiltro[1] + '-' + deFiltro[0];

		ateFiltro = ateFiltro.split('/');
		var dataAte = ateFiltro[2] + '-' + ateFiltro[1] + '-' + ateFiltro[0];

		if (dataDe > dataAte) {
			$('#alert-datepicker').removeClass('hide').find('span').text('A data final não pode ser menor que a data inicial.');

		} else {

			$('#alert-datepicker').addClass('hide');
			$('#dp2').datepicker('hide');

		}
	});

	$('#dp1-mask').mask('99/99/9999');
	$('#dp2-mask').mask('99/99/9999');


	$('#PromocaoEfeitoValor').numeric({decimal : false, negative: false});

 	$('#PromocaoEfeitoValor').blur(function() {
		if ($(this).val() == '0,00' || $(this).val() == '0') {
			$(this).val('');
		}
	});


	$('#opcao a').click(function (e) {
	  e.preventDefault()
	  $(this).tab('show');
	});

	$('.produto').typeahead({
		name: 'produtos',
		remote: WEBROOT + 'admin/promocoes/ajax_buscar_produtos/%QUERY',
		template: '<strong>{{descricao}}</strong> - #{{id}}',
		engine: Hogan
	}).on('typeahead:selected', function (obj, datum) {
		$("#PromocaoCausaProdutoId").val( datum.id );
		$(".colecao").val('');
		$("#PromocaoCausaColecaoId").val('');
		$(".categoria").val('');
		$("#PromocaoCausaCategoriaId").val('');
		$(".marca").val('');
		$("#PromocaoCausaMarcaId").val('');

		var colecao = $("#PromocaoCausaColecaoId").data('colecao');
		var categoria = $("#PromocaoCausaCategoriaId").data('categoria');
		var marca = $("#PromocaoCausaMarcaId").data('marca');

/*		if (colecao == '') {
			$(".colecao").val('');
			$("#PromocaoCausaColecaoId").val('');
		}
		if (categoria == '') {
			$(".categoria").val('');
			$("#PromocaoCausaCategoriaId").val('');
		}
		if (marca == '') {
			$(".marca").val('');
			$("#PromocaoCausaMarcaId").val('');
		}*/



	});

	$('.colecao').typeahead({
		name: 'colecoes',
		remote: WEBROOT + 'admin/promocoes/ajax_buscar_colecoes/%QUERY',
		template: '<strong>{{descricao}}</strong> - #{{id}}',
		engine: Hogan
	}).on('typeahead:selected', function (obj, datum) {
		$("#PromocaoCausaColecaoId").val( datum.id );
		$(".produto").val('');
		$("#PromocaoCausaProdutoId").val('');
		$(".categoria").val('');
		$("#PromocaoCausaCategoriaId").val('');
		$(".marca").val('');
		$("#PromocaoCausaMarcaId").val('');

		var produto = $("#PromocaoCausaProdutoId").data('produto');
		var categoria = $("#PromocaoCausaCategoriaId").data('categoria');
		var marca = $("#PromocaoCausaMarcaId").data('marca');

/*		if (produto == '') {
			$(".produto").val('');
			$("#PromocaoCausaProdutoId").val('');
		}
		if (categoria == '') {
			$(".categoria").val('');
			$("#PromocaoCausaCategoriaId").val('');
		}
		if (marca == '') {
			$(".marca").val('');
			$("#PromocaoCausaMarcaId").val('');
		}*/

	});

	$('.categoria').typeahead({
		name: 'categorias',
		remote: WEBROOT + 'admin/promocoes/ajax_buscar_categorias/%QUERY',
		template: '<strong>{{descricao}}</strong> - #{{id}}',
		engine: Hogan
	}).on('typeahead:selected', function (obj, datum) {
		$("#PromocaoCausaCategoriaId").val( datum.id );

		$(".colecao").val('');
		$("#PromocaoCausaColecaoId").val('');
		$(".produto").val('');
		$("#PromocaoCausaProdutoId").val('');
		$(".marca").val('');
		$("#PromocaoCausaMarcaId").val('');

		var produto = $("#PromocaoCausaProdutoId").data('produto');
		var colecao = $("#PromocaoCausaColecaoId").data('colecao');
		var marca = $("#PromocaoCausaMarcaId").data('marca');

/*		if (produto == '') {
			$(".produto").val('');
			$("#PromocaoCausaProdutoId").val('');
		}
		if (colecao == '') {
			$(".colecao").val('');
			$("#PromocaoCausaColecaoId").val('');
		}
		if (marca == '') {
			$(".marca").val('');
			$("#PromocaoCausaMarcaId").val('');
		}*/

	});

	$('.marca').typeahead({
		name: 'marca',
		remote: WEBROOT + 'admin/promocoes/ajax_buscar_marcas/%QUERY',
		template: '<strong>{{descricao}}</strong> - #{{id}}',
		engine: Hogan
	}).on('typeahead:selected', function (obj, datum) {

		$("#PromocaoCausaMarcaId").val( datum.id );

		$(".colecao").val('');
		$("#PromocaoCausaColecaoId").val('');
		$(".categoria").val('');
		$("#PromocaoCausaCategoriaId").val('');
		$(".produto").val('');
		$("#PromocaoCausaProdutoId").val('');

		var produto = $("#PromocaoCausaProdutoId").data('produto');
		var colecao = $("#PromocaoCausaColecaoId").data('colecao');
		var categoria = $("#PromocaoCausaCategoriaId").data('categoria');


/*		if (produto == '') {
			$(".produto").val('');
			$("#PromocaoCausaProdutoId").val('');
		}
		if (colecao == '') {
			$(".colecao").val('');
			$("#PromocaoCausaColecaoId").val('');
		}
		if (categoria == '') {
			$(".categoria").val('');
			$("#PromocaoCausaCategoriaId").val('');
		}*/

	});

/*	$('#PromocaoMarca').blur(function(){

		var valor = $(this).val();

		if(valor == ''){
			$(this).text('');
		}

	});*/

	$('#PromocaoCausaValorAcimaDe').priceFormat({
		prefix: '',
		clearPrefix: true,
		centsSeparator: ',',
		thousandsSeparator: '.'
	});

	$('#PromocaoCausaValorAcimaDe').blur(function() {
		if ($(this).val() == '0,00') {
			$(this).val('');
		}
	});

	$('#PromocaoAdminPromocaoForm').on('submit', function(){

		var retorno = true;

		var leve = $('#PromocaoCausaQtdPecas').val();
		var pague = $('#PromocaoEfeitoValor').val();

		if (leve < pague) {
			alert('Por favor, o "leve" deve ser maior que o "pague".');
			$('.btn-action-save-default').button('reset');
			retorno = false;

		}

		var deFiltro = $('#dp1-mask').val();
		var ateFiltro = $('#dp2-mask').val();

		deFiltro = deFiltro.split('/');
		var startDate = new Date(deFiltro[2], deFiltro[1], deFiltro[0]);

		ateFiltro = ateFiltro.split('/');
		var endDate = new Date(ateFiltro[2], ateFiltro[1], ateFiltro[0]);

		if (startDate > endDate) {
			alert('Intervalo entre datas incorreto.');
			$('.btn-action-save-default').button('reset');
			$('html,body').animate({scrollTop: 0},'slow');
			retorno = false;
		}

		var produto = $('#PromocaoProduto').val();
		var colecao = $('#PromocaoColecao').val();
		var categoria = $('#PromocaoCategoria').val();
		var marca = $('#PromocaoMarca').val();

		if (produto == '') {
			$("#PromocaoCausaProdutoId").val('');
		}

		if (colecao == '') {
			$("#PromocaoCausaColecaoId").val('');
		}

		if (categoria == '') {
			$("#PromocaoCausaCategoriaId").val('');
		}

		if (marca == '') {
			$("#PromocaoCausaMarcaId").val('');
		}

		 return retorno;

	});

});
