$(document).ready(function() {

	$('.ver-detalhes-promocao').click(function(e){
		var url = $(this).attr('href');
		e.preventDefault();
		$.ajax({
			url: url,
			type: 'GET',
			success: function(data){
				bootbox.dialog({
					size: 'large',
					message:data,
					title: 'Detalhes da Promoção',
					buttons: {
						main: {
							label: 'Fechar X',
							className: 'btn-danger'
						}
					}
				});
			},
			error: function(data){
				alert ('Ocorreu um erro ao tentar ao tentar buscar os dados.');
			}
		});
	});

});