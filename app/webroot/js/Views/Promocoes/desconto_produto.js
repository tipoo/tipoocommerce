$(document).ready(function() {

	$('#PromocaoAdminPromocaoForm').validate(bootstrapValidateOptions);

	$.validator.addMethod(
		"validacaoHora",
		function (value,element){  
			return this.optional(element) || /^(([0-1]?[0-9])|([2][0-3])):([0-5]?[0-9])(:([0-5]?[0-9]))?$/i.test(value);  
		}, 
		"Por Favor, entre com uma hora válida."
	);

	$.validator.addMethod(
		"validacaoValorAcimeDe",
		function (value,element){  

			var valor_acima = $('#PromocaoCausaValorAcimaDe').val();
			var carriho_valor_acima = $('#PromocaoCausaCarrinhoValorAcimaDe'). val();

			if(valor_acima == '0,00') {
				 $('#PromocaoCausaValorAcimaDe').val('');
			}
			if (carriho_valor_acima == '0,00') {
				$('#PromocaoCausaCarrinhoValorAcimaDe'). val('');

			}

			return (valor_acima != '' && carriho_valor_acima != '') ? false : true;  
		}, 
		"Apenas um desses campos deve ser preenchido."
	);

	$("#PromocaoCausaValorAcimaDe").rules("add", {
		validacaoValorAcimeDe: true
	});

	$("#PromocaoCausaCarrinhoValorAcimaDe").rules("add", {
		validacaoValorAcimeDe: true
	});

	$("#PromocaoDescricao").rules("add", {
		required: true
	});

	$('#dp1-mask').rules("add", {
		required: true
	});
	$('#dp2-mask').rules("add", {
		required: true
	});

	$("#PromocaoHoraInicio").rules("add", {
		required: true,
		validacaoHora: "Campo obrigatório"
	});

	$("#PromocaoHoraFim").rules("add", {
		required: true,
		validacaoHora: "Campo obrigatório"
	});

	$("#PromocaoEfeitoTipoValor").rules("add", {
		required: true
	});

	$("#PromocaoEfeitoValor").rules("add", {
		required: true
	});

	$('#PromocaoDataInicio').mask('99/99/9999');

	$('#PromocaoDataFim').mask('99/99/9999');

	$('#PromocaoHoraInicio').mask('99:99:99');

	$('#PromocaoHoraFim').mask('99:99:99');

	$('#PromocaoCausaCepInicio').mask('99999-999');

	$('#PromocaoCausaCepFim').mask('99999-999');

	$('#PromocaoCausaQtdPecas').numeric({decimal : false, negative: false});

	$('#PromocaoCausaCarrinhoQtdPecas').numeric({decimal : false, negative: false});

 	$('#PromocaoCausaQtdPecas').blur(function() {
		if ($(this).val() == '0,00' || $(this).val() == '0') {
			$(this).val('');
		}
	});

 	$('#PromocaoCausaCarrinhoQtdPecas').blur(function() {
		if ($(this).val() == '0,00' || $(this).val() == '0') {
			$(this).val('');
		}
	});

	var deFiltro = $('#dp1-mask').val();
	var ateFiltro = $('#dp2-mask').val();

	deFiltro = deFiltro.split('/');
	var startDate = new Date(deFiltro[2], deFiltro[1], deFiltro[0]);

	ateFiltro = ateFiltro.split('/');
	var endDate = new Date(ateFiltro[2], ateFiltro[1], ateFiltro[0]);

	$('#dp1').datepicker({
		language: "pt-BR",
		startDate   : '0'
	}).on('changeDate', function(ev){

		var deFiltro = $('#dp1-mask').val();
		var ateFiltro = $('#dp2-mask').val();

		deFiltro = deFiltro.split('/');
		var dataDe = deFiltro[2] + '-' + deFiltro[1] + '-' + deFiltro[0];

		ateFiltro = ateFiltro.split('/');
		var dataAte = ateFiltro[2] + '-' + ateFiltro[1] + '-' + ateFiltro[0];

		if (dataDe > dataAte) {
			$('#alert-datepicker').removeClass('hide').find('span').text('A data inicial não pode ser maior que a data final.');

		} else {

			$('#alert-datepicker').addClass('hide');
			$('#dp1').datepicker('hide');

		}
	});

	$('#dp2').datepicker({
		language: "pt-BR",
		startDate   : '0'
	}).on('changeDate', function(ev){

		var deFiltro = $('#dp1-mask').val();
		var ateFiltro = $('#dp2-mask').val();

		deFiltro = deFiltro.split('/');
		var dataDe = deFiltro[2] + '-' + deFiltro[1] + '-' + deFiltro[0];

		ateFiltro = ateFiltro.split('/');
		var dataAte = ateFiltro[2] + '-' + ateFiltro[1] + '-' + ateFiltro[0];

		if (dataDe > dataAte) {
			$('#alert-datepicker').removeClass('hide').find('span').text('A data final não pode ser menor que a data inicial.');

		} else {

			$('#alert-datepicker').addClass('hide');
			$('#dp2').datepicker('hide');

		}
	});

	$('#dp1-mask').mask('99/99/9999');
	$('#dp2-mask').mask('99/99/9999');

	 var tipo_desconto = $('#PromocaoEfeitoTipoValor'). val();

	 if(tipo_desconto == 'P') {

	 	  $('#PromocaoEfeitoValor').numeric({decimal : false, negative: false});

	 } else {

		$("#PromocaoEfeitoValor").priceFormat({
			prefix: '',
			clearPrefix: true,
			centsSeparator: ',',
			thousandsSeparator: '.'
		});
	 }



	 $("#PromocaoEfeitoTipoValor").on('change', function() {

	 	$('#PromocaoEfeitoValor'). val('');

		 var tipo_desconto = $(this). val();

		 if(tipo_desconto == 'P') {
		 	
			$('#PromocaoEfeitoValor').unpriceFormat();
			$('#PromocaoEfeitoValor').numeric({decimal : false, negative: false});

		 } else {

		 	$('#PromocaoEfeitoValor').priceFormat({
				prefix: '',
				clearPrefix: true,
				centsSeparator: ',',
				thousandsSeparator: '.'
			});

			$('#PromocaoEfeitoValor').blur(function() {
				if ($(this).val() == '0,00') {
					$(this).val('');
				}
			});
		 }
	 });

 	$('#PromocaoEfeitoValor').blur(function() {
		if ($(this).val() == '0,00' || $(this).val() == '0') {
			$(this).val('');
		}
	});

	$('#opcao a').click(function (e) {
	  e.preventDefault()
	  $(this).tab('show');
	});

	$('.produto').typeahead({
		name: 'produtos',
		remote: WEBROOT + 'admin/promocoes/ajax_buscar_produtos/%QUERY',
		template: '<strong>{{descricao}}</strong> - #{{id}}',
		engine: Hogan
	}).on('typeahead:selected', function (obj, datum) {
		$("#PromocaoCausaProdutoId").val( datum.id );
		$(".colecao").val('');
		$("#PromocaoCausaColecaoId").val('');
		$(".categoria").val('');
		$("#PromocaoCausaCategoriaId").val('');
		$(".marca").val('');
		$("#PromocaoCausaMarcaId").val('');

		var colecao = $("#PromocaoCausaColecaoId").data('colecao');
		var categoria = $("#PromocaoCausaCategoriaId").data('categoria');
		var marca = $("#PromocaoCausaMarcaId").data('marca');

	});

	$('.colecao').typeahead({
		name: 'colecoes',
		remote: WEBROOT + 'admin/promocoes/ajax_buscar_colecoes/%QUERY',
		template: '<strong>{{descricao}}</strong> - #{{id}}',
		engine: Hogan
	}).on('typeahead:selected', function (obj, datum) {
		$("#PromocaoCausaColecaoId").val( datum.id );
		$(".produto").val('');
		$("#PromocaoCausaProdutoId").val('');
		$(".categoria").val('');
		$("#PromocaoCausaCategoriaId").val('');
		$(".marca").val('');
		$("#PromocaoCausaMarcaId").val('');

		var produto = $("#PromocaoCausaProdutoId").data('produto');
		var categoria = $("#PromocaoCausaCategoriaId").data('categoria');
		var marca = $("#PromocaoCausaMarcaId").data('marca');


	});

	$('.categoria').typeahead({
		name: 'categorias',
		remote: WEBROOT + 'admin/promocoes/ajax_buscar_categorias/%QUERY',
		template: '<strong>{{descricao}}</strong> - #{{id}}',
		engine: Hogan
	}).on('typeahead:selected', function (obj, datum) {
		$("#PromocaoCausaCategoriaId").val( datum.id );

		$(".colecao").val('');
		$("#PromocaoCausaColecaoId").val('');
		$(".produto").val('');
		$("#PromocaoCausaProdutoId").val('');
		$(".marca").val('');
		$("#PromocaoCausaMarcaId").val('');

		var produto = $("#PromocaoCausaProdutoId").data('produto');
		var colecao = $("#PromocaoCausaColecaoId").data('colecao');
		var marca = $("#PromocaoCausaMarcaId").data('marca');

	});

	$('.marca').typeahead({
		name: 'marca',
		remote: WEBROOT + 'admin/promocoes/ajax_buscar_marcas/%QUERY',
		template: '<strong>{{descricao}}</strong> - #{{id}}',
		engine: Hogan
	}).on('typeahead:selected', function (obj, datum) {
		$("#PromocaoCausaMarcaId").val( datum.id );
		$(".colecao").val('');
		$("#PromocaoCausaColecaoId").val('');
		$(".categoria").val('');
		$("#PromocaoCausaCategoriaId").val('');
		$(".produto").val('');
		$("#PromocaoCausaProdutoId").val('');

		var produto = $("#PromocaoCausaProdutoId").data('produto');
		var colecao = $("#PromocaoCausaColecaoId").data('colecao');
		var categoria = $("#PromocaoCausaCategoriaId").data('categoria');

	});

	$('#PromocaoCausaValorAcimaDe').priceFormat({
		prefix: '',
		clearPrefix: true,
		centsSeparator: ',',
		thousandsSeparator: '.'
	});

	$('#PromocaoCausaCarrinhoValorAcimaDe').priceFormat({
		prefix: '',
		clearPrefix: true,
		centsSeparator: ',',
		thousandsSeparator: '.'
	});

	$('#PromocaoAdminPromocaoForm').on('submit', function(){

		 var produto = $('#PromocaoProduto').val();
		 var colecao = $('#PromocaoColecao').val();
		 var categoria = $('#PromocaoCategoria').val();
		 var marca = $('#PromocaoMarca').val();

		 if (produto == '') {
			$("#PromocaoCausaProdutoId").val('');
		 }

 		if (colecao == '') {
			$("#PromocaoCausaColecaoId").val('');
		 }

 		if (categoria == '') {
			$("#PromocaoCausaCategoriaId").val('');
		 }

 		if (marca == '') {
			$("#PromocaoCausaMarcaId").val('');
		 }

	});

	$('#PromocaoAdminPromocaoForm').submit(function(){

		var retorno = true;

		var deFiltro = $('#dp1-mask').val();
		var ateFiltro = $('#dp2-mask').val();

		deFiltro = deFiltro.split('/');
		var startDate = new Date(deFiltro[2], deFiltro[1], deFiltro[0]);

		ateFiltro = ateFiltro.split('/');
		var endDate = new Date(ateFiltro[2], ateFiltro[1], ateFiltro[0]);

		if (startDate > endDate) {
			alert('Intervalo entre datas incorreto.');
			$('.btn-action-save-default').button('reset');
			$('html,body').animate({scrollTop: 0},'slow');
			retorno = false;
		} 

		return retorno;

	});

});
