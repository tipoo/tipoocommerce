$(document).ready(function() {
	shipping.init();
});

var shipping = {
	init: function() {
		this.mask();
		this.simulate();
		this.calculate();
		this.calculating = false;
	},
	simulate: function() {
		var self = this;

		$('.t-calcular-frete-form').on('submit', function(e) {
			self.calculate();
			return false;
		});
	},
	calculate: function() {
		var self = this;

		var cep = $('.t-calcular-frete-form .t-cep').val();
		var produto_id = tipoo.data.produto.id;
		var sku_id = $('.t-sku-id').val();
		var qtd = tipoo.data.produto.quantidade;
		var simulacao_valor_carrinho = qtd * tipoo.data.produto.preco_por;

		if (cep == '_____-___') {
			cep = null;
		}

		if (cep) {

			$.ajax({
				url: WEBROOT + 'catalogo/ajax_simular_frete/' + cep + '/' +  simulacao_valor_carrinho + '/' +  qtd + '/' + produto_id + '/' + sku_id,
				type: 'POST',
				data: $('.t-calcular-frete-form').serialize(),
				beforeSend: function() {
					$('.t-frete-transportadoras').html('');
					$('.t-calcular-frete-form input[type="submit"]').hide();
					if (!self.calculating) {
						self.calculating = true;
						$('.t-calcular-frete-form .submit').before('<img class="loader-produto-frete" src="' + WEBROOT + 'theme/' + PROJETO + '/img/loader-produto-frete.gif" />');
					}
				},
				success: function(data) {
					$('.t-frete-transportadoras').html(data);

					$('.loader-produto-frete').remove();
					$('.t-calcular-frete-form input[type="submit"]').show();
					self.calculating = false;
				},
				error: function(data) {
					$('.loader-produto-frete').remove();
					$('.t-calcular-frete-form input[type="submit"]').show();
					self.calculating = false;
					alert('Falha de comunicação ao tentar fazer a simulação. Por favor, tente novamente.');
				}
			});
		}
	},
	mask: function() {
		$('.t-calcular-frete-form .t-cep').mask('99999-999');
	}
}