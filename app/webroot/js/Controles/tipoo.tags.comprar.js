$(document).ready(function() {
	tipooBuyButton.init();

	// Verifica se a forma de selecionar o sku é por caracteristica ou direto por sku
	if (!$('.t-skus-caracteristicas').length) {
		tipooSku.selection.init();
	} else {
		tipooSku.selectionSpecification.init();
	}

	tipooSku.quantity.init();
	tipooBuyTogether.init();
});

var tipooBuyButton = {
	init: function () {
		this.ajaxAviseMe();
		this.buyOnClick();
	},
	hideCallToActionsAndClearValues: function () {
		$('.t-btn-comprar').hide();
		$('.t-pre-venda').hide();
		$('.t-avise-me').hide();

		$('.t-mensagem-avise-me p').remove();
		$('.t-previsao-pre-venda').empty();

		$('#AviseMeSkuId').val('');
		$('.t-btn-comprar').attr('href', '#');
		$('.t-btn-pre-venda').attr('href', '#');
	},
	showCallToAction: function (stockSituationVal, skuSelected) {
		var self = this;
		var $buyButton = $('.t-btn-comprar');

		switch(stockSituationVal) {
			case 'Disponivel':
				self.callBuyButton(skuSelected);
				break;
			case 'PreVenda':
				self.callPreVenda(skuSelected);
				break;
			case 'AviseMe':
				self.callAviseMe(skuSelected);
				break;
			default:
				self.hideCallToActionsAndClearValues();
				$buyButton.show();
		}
	},
	callBuyButton: function (skuSelected) {
		var self = this;
		var $buyButton = $('.t-btn-comprar');

		self.hideCallToActionsAndClearValues();
		self.changeUrlButton($buyButton, skuSelected);
		$('[data-situacao-estoque="Disponivel"]').show();
	},
	callPreVenda: function (skuSelected) {
		var self = this;
		var $buyButton = $('.t-btn-pre-venda');
		var previsaoPreVenda = $('[value="' + skuSelected + '"]').data('previsao-pre-venda');

		self.hideCallToActionsAndClearValues();
		self.changeUrlButton($buyButton, skuSelected);

		$('.t-previsao-pre-venda').text(previsaoPreVenda);
		$('[data-situacao-estoque="PreVenda"]').show();
	},
	callAviseMe: function (skuSelected) {
		var self = this;

		self.hideCallToActionsAndClearValues();

		$('#AviseMeSkuId').val(skuSelected);
		$('.t-avise-me').show(); // Mostra também a mensagem do produto esgotado
		$('[data-situacao-estoque="AviseMe"]').show();
	},
	ajaxAviseMe: function () {
		$('#AviseMeIndexForm').submit(function() {
			$.ajax({
				url: WEBROOT + 'avise_me/enviar',
				type: 'POST',
				data: $('#AviseMeIndexForm').serialize(),
				beforeSend: function() {
					$('#AviseMeIndexForm input[type="submit"]').hide();
					$('#AviseMeIndexForm .submit').append('<img class="t-loader-avise-me" src="' + WEBROOT + 'theme/' + PROJETO + '/img/ajax-avise-me.gif" />');
				},
				success: function(data) {
					$('#AviseMeIndexForm').fadeOut(function() {
						$('.t-mensagem-avise-me').append('<p style="display:none;">Dados enviados com sucesso. Você será notificado quando o produto entrar em estoque.</p>');
						$('.t-mensagem-avise-me p').fadeIn();
						$('.t-loader-avise-me').remove();
						$('#AviseMeIndexForm input[type="submit"]').show();
					});
				},
				error: function(data) {
					$('.t-loader-avise-me').remove();
					$('#AviseMeIndexForm input[type="submit"]').show();
					alert('Falha de comunicação ao tentar enviar seu e-mail. Por favor, tente novamente.');
				}
			});

			return false;
		});
	},
	changeUrlButton: function ($buyButton, skuSelected) {
		var self = this;
		var url  = WEBROOT + 'carrinho/adicionar/';
		var skuQuantity = tipooSku.quantity.value();

		if (skuSelected == '') {
			url = '#';
		} else {
			url += skuSelected + '/' + skuQuantity;
		}

		$buyButton.attr('href', url);
	},
	buyOnClick: function () {
		var self = this;
		var $buyButton = $('.t-btn-comprar');
		var alertMessage = $('.t-mensagem-sku span').text();

		$buyButton.on('click', function(e) {
			if ($buyButton.attr('href') == '#') {
				e.preventDefault();
				self.buyOnClickAlert(alertMessage);
			}
		});
	},
	buyOnClickAlert: function(alertMessage) {
		alert(alertMessage);
	}
};

var tipooSku = {
	selection: {
		init: function () {
			this.tipo = $('.t-skus .t-skus-selecao').data('tipo');
			this.skusSelection = $('.t-skus .t-skus-selecao');
			this.skus = $('.t-skus .t-skus-selecao .t-sku');

			this.countSkus();
			this.skuOnChange();
		},
		countSkus: function () {
			var self = this;
			var count = self.skus.length;

			if (count == 1) {
				self.skuActivate();
			}
		},
		skuOnChange: function () {
			var self = this;
			self.skusSelection.on('change', this.skuSelected);
		},
		skuSelected: function () {
			tipooSku.selection.skuActivate();
		},
		skuActivate: function () {
			var self = this;
			var $sku = self.skusSelection;

			if (self.tipo == 'lista') {
				var skuSelected = $sku.find('input:checked').val();
				self.activateSkuList($sku);
			} else if (self.tipo == 'select') {
				var skuSelected = $sku.val();
				self.activateSkuSelect($sku, skuSelected);
			}

			$('.t-sku-id').val(skuSelected);

			self.stockSituation(skuSelected);
		},
		activateSkuList: function ($sku) {
			$sku.children('li').removeClass('active');
			$sku.find('input:checked').closest('li').addClass('active');
		},
		activateSkuSelect: function ($sku, skuSelected) {
			$sku.children('.t-sku').removeClass('active');
			$sku.children('.t-sku[value="' + skuSelected + '"]').addClass('active');
		},
		stockSituation: function (skuSelected) {
			var self = this;
			var stockSituationVal = $('.t-sku[value="' + skuSelected + '"]').data('situacao-estoque');

			tipooBuyButton.showCallToAction(stockSituationVal, skuSelected);
		}
	},
	selectionSpecification: {
		init: function () {
			this.skusSpecificationsSelection = $('.t-skus .t-skus-caracteristicas-selecao');
			// Serve para inativar a caracteristicano selecionada no primeiro clique caso todas as opções de skus estejam indiponíveis
			this.specificationInactivateBool = true;
			this.skus = tipoo.data.skus;

			this.countSkus();
			this.specificationOnChange();
		},
		countSkus: function () {
			var self = this;
			var count = Object.keys(self.skus).length;

			if (count == 1) {
				self.specificationActivate();
			}
		},
		specificationOnChange: function () {
			var self = this;
			self.skusSpecificationsSelection.on('change', this.specificationSelected);
		},
		specificationSelected: function () {
			tipooSku.selectionSpecification.specificationActivate();
		},
		specificationActivate: function () {
			var self = this;
			var $skuSpecification = self.skusSpecificationsSelection;

			var $specificationSelected = $skuSpecification.find('input:checked');

			$skuSpecification.children('li').removeClass('sku-indisponivel');
			$skuSpecification.children('li').removeClass('active');

			$skuSpecification.find('input:checked').closest('li').addClass('active');

			self.eachOthersSpecifications($specificationSelected);
		},
		eachOthersSpecifications: function ($specificationSelected) {
			var self = this;
			var $skuSpecification = self.skusSpecificationsSelection;
			self.specificationInactivateBool = true;

			$.each(tipoo.data.skus, function(i, value) {
				$.each(this.caracteristicas.valores.ids, function(j, value) {
					$specificationSelected.each(function(index, el) {
						if (j == $(el).val()) {
							// Quando selecionada a caracteristica, muda o valor da caracteristica para true
							tipoo.data.skus[i].caracteristicas.valores.ids[j] = true;
							self.checkSpecificationsAvailability(tipoo.data.skus[i]);
						}
					});

				});

				var showCallToAction = true;
				$.each(this.caracteristicas.valores.ids, function(j, value) {
					// Verifica se todas as caracterisicas foram selecionadas para chamar o call to action
					if (!tipoo.data.skus[i].caracteristicas.valores.ids[j]) {
						showCallToAction = false;
					}

					// Garante que ao final da verificação dos valores das caracteristicas selecionados, voltem para o status de false (inicial),
					// para não haver conclito caso o cliente selecione outra caracteristica
					tipoo.data.skus[i].caracteristicas.valores.ids[j] = false;
				});

				if (showCallToAction) {
					$('.t-sku-id').val(i);
					tipooBuyButton.showCallToAction(tipoo.data.skus[i].situacao_estoque, i);
				}
			});

			if (self.specificationInactivateBool) {
				$skuSpecification.find('input:checked').closest('li').addClass('sku-indisponivel');
			}
		},
		checkSpecificationsAvailability: function(sku) {
			var self = this;

			if (sku.situacao_estoque == 'AviseMe') {
				self.specificationInactivate(sku);
			} else {
				self.specificationInactivateBool = false;
			}

		},
		specificationInactivate: function(sku) {
			var self = this;
			var $skuSpecification = self.skusSpecificationsSelection;

			var i = 0;
			$.each(sku.caracteristicas.valores.ids, function(index, value) {
				if (value) {
					i++;
				}
			});

			$.each(sku.caracteristicas.valores.ids, function(index, value) {

				if (value == false && (Object.keys(sku.caracteristicas.valores.ids).length - 1) == i) {
					$skuSpecification.find('input[value="' + index + '"]').closest('li').addClass('sku-indisponivel');
				}
			});

			// Todas as caracterisricas selecionadas ou ao selecionar uma caracteristica todas as outras restantes não estão disponíveis
			if (i == Object.keys(sku.caracteristicas.valores.ids).length) {
				$skuSpecification.find('input:checked').closest('li').addClass('sku-indisponivel');
			}
		}
	},
	quantity: {
		init: function () {
			this.onClick();
			this.onChange();
		},
		onClick: function () {
			var self = this;
			var $skuQuantity = $('.t-sku-quantidade');
			var skuQuantity = $skuQuantity.val();

			$('.t-quantidade-botoes span').on('click', function() {
				var operador = $(this).data('operador'); // pode ser + ou -
				if (operador == '+') {
					skuQuantity++;
				} else if (operador == '-') {
					skuQuantity = (skuQuantity > 1) ? skuQuantity - 1 : 1;
				}

				$skuQuantity.val(skuQuantity);
				$('.t-sku-quantidade').trigger('keyup');
			});
		},
		onChange: function () {
			var self = this;

			$('.t-sku-quantidade').on('keyup', function() {
				var skuQuantity = parseInt($(this).val());
				if (skuQuantity === 0) {
					skuQuantity = 1;
					$(this).val(skuQuantity);
				}

				tipoo.data.produto.quantidade = skuQuantity;

				// Recaulcula a simulação do frete
				if (typeof shipping !== "undefined") {
		 			shipping.calculate();
		 		}

				// Verifica se a forma de selecionar o sku é por caracteristica ou direto por sku
				if (!$('.t-skus-caracteristicas').length) {
					tipooSku.selection.skuActivate();
				} else {
					tipooSku.selectionSpecification.specificationActivate();
				}
			});
		},
		value: function() {
			var skuQuantity = 1;
			var $skuQuantity = $('.t-sku-quantidade');

			if ($skuQuantity.length) {
				$('.t-sku-quantidade').numeric({
					negative : false,
					decimal : false
				});
				skuQuantity = $skuQuantity.val();
			}

			return skuQuantity;
		}
	}
}

var tipooBuyTogether = {
	init: function () {
		this.containersBuyTogether = $('.t-produtos-compre-junto');

		if (this.containersBuyTogether.length) {
			this.eachCompreJunto();
			this.skuOnChange();
			this.buyOnClick();
		}
	},
	eachCompreJunto: function () {
		var self = this;

		self.containersBuyTogether.each(function(index, el) {
			self.changeUrlButton($(el));
		});
	},
	skuOnChange: function () {
		$('.t-skus-selecao').on('change', this.skuSelected);
	},
	skuSelected: function () {
		var $sku = $(this);
		var $compreJunto = $sku.closest('.t-produtos-compre-junto');

		tipooBuyTogether.changeUrlButton($compreJunto);
	},
	changeUrlButton: function ($compreJunto) {
		var url  = WEBROOT + 'carrinho/adicionar/';
		var $skus = $compreJunto.find('.t-skus-selecao');
		var $buyButton = $compreJunto.find('.t-compre-junto-btn-comprar');

		var urlSkuSelected = '';
		var urlSkuQuantity = '';

		$skus.each(function(e) {
			var skuSelected = $(this).val();
			var skuQuantity = 1;

			if (skuSelected == '') {
				url = '#';
				return false;
			} else {
				if (e == 0) {
					urlSkuSelected += skuSelected;
					urlSkuQuantity += skuQuantity;
				} else {
					urlSkuSelected +=  ',' + skuSelected;
					urlSkuQuantity += ',' + skuQuantity;
				}
			}
		});

		if (url != '#') {
			url += urlSkuSelected + '/' + urlSkuQuantity;
		}

		$buyButton.attr('href', url);
	},
	buyOnClick: function () {
		var self = this;
		var $buyButtons = $('.t-compre-junto-btn-comprar');

		$buyButtons.on('click', function(e) {
			var $buyButton = $(this);

			if ($buyButton.attr('href') == '#') {
				e.preventDefault();
				self.buyOnClickAlert();
			}
		});
	},
	buyOnClickAlert: function() {
		alert('Selecione os modelos antes de comprar.');
	}
};