$(document).ready(function() {

	var pontuacao = $('#produto_pontos').val();

	$('.star').raty({
		path: WEBROOT + 'theme/' + PROJETO + '/img/raty',
 		click: function(score, evt) {

		}
	});

	$('.t-avaliacao-produto-form').on('submit', function() {
		var $form = $(this);

		$form.closest('.avaliacao').find('.avaliacao-error').remove();
		$form.closest('.avaliacao').find('.alert-campos-obrigatorios').remove();

		var nome = $form.closest('.avaliacao').find('.avaliacao-nome').val('');
		var email = $form.closest('.avaliacao').find('.avaliacao-email').val(''); 

		if (nome != '' && email != '') {

		 	var url = WEBROOT + 'produtos/ajax_avaliar';

			$.ajax({
				url: url,
				type: 'POST',
				data: $form.serialize(),
				beforeSend: function() {
				},
				success: function(data) {

					var retorno = jQuery.parseJSON(data);

					if (retorno.sucesso) {

						var media = retorno.media_avaliacao;
						media = media.toFixed(2);
						var decimais = media.toString().split('.'); 
						var media_produto = 0;

						if (decimais[1] >= 50) {
							 media_produto = ( parseInt(decimais[0]) +  parseInt(1)) * 10;

						} else {
							 media_produto = decimais[0] * 10;

						}

						var produto_id = retorno.produto_id;

						if ($("span[data-produto='" + produto_id +"']").length) {

							var classe = $("span[data-produto='" + produto_id +"']").attr('class').split(' ')[1];
							$("span[data-produto='" + produto_id +"']").removeClass(classe);

							$("span[data-produto='" + produto_id +"']").addClass('media-' + media_produto);

						}

						$form.find('.avaliacao-nome').val('');
						$form.find('.avaliacao-email').val(''); 

						$form.after('<p>Obrigada pela sua avaliação.</p>');

						$form.remove();

					} else {

						$form.find('.avaliacao-nome').val('');
						$form.find('.avaliacao-email').val(''); 

						$form.find('.star').raty('click', 0);

						$form.after('<span class="avaliacao-error">' + retorno.mensagem + '</span>');

					}

				},
				error: function(data) {
					alert('Falha de comunicação ao tentar avaliar esse produto. Por favor, tente novamente.');
				}
			});
		} else {

			$form.after('<p class="alert-campos-obrigatorios">*Forneça seu nome e e-mail para avaliar esse produto.</p>');
		}

		return false;
	});

});