enviar_cupom = function(cupom) {
	if (cupom) {
		document.location.href = WEBROOT + 'carrinho/aplicar_cupom/' + cupom;
	} else {
		alert('Por favor, preencha o cupom de desconto.');
	}
};

adicionar_servico = function(sku_id, servico_id) {
	document.location.href = WEBROOT + 'carrinho/adicionar_servico/' + sku_id + '/' + servico_id;
};

remover_servico = function(sku_id, servico_id) {
	document.location.href = WEBROOT + 'carrinho/remover_servico/' + sku_id + '/' + servico_id;
};

adicionar_anexo = function(sku_id, anexo_id, valor_anexo) {
	document.location.href = WEBROOT + 'carrinho/adicionar_anexo/' + sku_id + '/' + anexo_id + '/' + valor_anexo;
};

remover_anexo = function(sku_id, servico_id, anexo_id) {
	document.location.href = WEBROOT + 'carrinho/remover_anexo/' + sku_id + '/' + servico_id + '/' + anexo_id;
};

$(document).ready(function() {
	$('.input-quantidade').on('blur', function() {

		var id_produto = $(this).attr('data-id');
		var quantidade = $('#quantidade-' + id_produto).val();

		if (quantidade <= 0 || quantidade === '') {
			$('#quantidade-' + id_produto).val(1);
		}

		document.location.href = WEBROOT + 'carrinho/editar/' + $(this).attr('data-id') + '/' + $(this).val();

	});

	$('.remover-sku').on('click', function() {
		document.location.href = WEBROOT + 'carrinho/remover/' + $(this).attr('data-id');
	});

	// $('#FreteIndexForm').validate();

	$('.input-quantidade').numeric();

	// Cupom
	$(document).on("click", '#CupomIndexForm .submit input[type="submit"]', function(e) {
		cupom = $('#CupomCodigo').val();
		enviar_cupom(cupom);
		return false;
	});

	// Servicos
	$('.t-adicionar-servico').on('click', function() {
		var servico_id = $(this).data('servico-id');
		var sku_id = $(this).data('sku-id');
		adicionar_servico(sku_id, servico_id);
	});

	$('.t-remover-servico').on('click', function() {
		var servico_id = $(this).data('servico-id');
		var sku_id = $(this).data('sku-id');
		remover_servico(sku_id, servico_id);
	});

	// Anexos (Serviços)
	$('.t-adicionar-anexo').on('click', function() {
		var anexo_id = $(this).data('anexo-id');
		var sku_id = $(this).data('sku-id');
		var valor_anexo = $(this).closest('div').find('.input-anexo').val();
		adicionar_anexo(sku_id, anexo_id, valor_anexo);
	});

	$('.t-remover-anexo').on('click', function() {
		var anexo_id = $(this).data('anexo-id');
		var servico_id = $(this).data('servico-id');
		var sku_id = $(this).data('sku-id');
		remover_anexo(sku_id, servico_id, anexo_id);
	});

	shipping.init();
});

var shipping = {
	init: function() {
		this.mask();
		this.simulate();
		this.calculate();
		this.calculating = false;
	},
	simulate: function() {
		var self = this;

		$('#FreteIndexForm').on('submit', function(e) {
			self.calculate();
			return false;
		});
	},
	calculate: function() {
		var self = this;

		var cep = $('#FreteCalcValorFrete').val();
		var valor_total_carrinho = $('.col-total span').data('valor-total');

		if (cep == '_____-___') {
			cep = null;
		}

		if (cep) {

			$.ajax({
				url: WEBROOT + 'carrinho/ajax_simular_frete/' + cep + '/' + valor_total_carrinho,
				type: 'POST',
				beforeSend: function() {
					$('.t-frete-transportadoras').html('');
					$('#FreteIndexForm input[type="submit"]').hide();
					if (!self.calculating) {
						self.calculating = true;
						$('#FreteIndexForm .submit').before('<img class="loader-carrinho-frete" src="' + WEBROOT + 'theme/' + PROJETO + '/img/loader-carrinho-frete.gif" />');
					}
				},
				success: function(data) {
					$('.t-frete-transportadoras').html(data);

					$('.loader-carrinho-frete').remove();
					$('#FreteIndexForm input[type="submit"]').show();
					self.calculating = false;
				},
				error: function(data) {
					$('.loader-carrinho-frete').remove();
					$('#FreteIndexForm input[type="submit"]').show();
					self.calculating = false;
					alert('Falha de comunicação ao tentar fazer a simulação. Por favor, tente novamente.');
				}
			});
		}
	},
	mask: function() {
		$('#FreteCalcValorFrete').mask('99999-999');
	}
}