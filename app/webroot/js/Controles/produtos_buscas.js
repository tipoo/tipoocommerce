$(document).ready(function() {
	/*$('.produtos-buscas-mais-produtos a').on('click', function() {
		var buscaId = $('.produtos-buscas').data('busca-id');
		var totalProdutos = parseInt($('.produtos-buscas').data('total-produtos'), 10);
		var pagina = parseInt($('.produtos-buscas').data('pagina'), 10) + 1;

		$.ajax({
			url: WEBROOT + 'catalogo/ajax_produtos',
			data: {
				busca_id: buscaId,
				pagina: pagina
			},
			dataType: 'html',
			success: function(data) {
				$('.produtos-buscas-mais-produtos').before(data);
				$('.produtos-buscas').data('pagina', pagina);

				if ($('.produto-busca').length >= totalProdutos) {
					$('.produtos-buscas-mais-produtos').hide();
				}
			},
			error: function() {
				alert('Erro ao buscar mais produtos.');
			}
		});

		return false;
	});*/
	$('.produtos-buscas').waypoint(function() {
		var buscaId = $('.produtos-buscas').data('busca-id');
		var totalProdutos = parseInt($('.produtos-buscas').data('total-produtos'), 10);
		var pagina = parseInt($('.produtos-buscas').data('pagina'), 10) + 1;
		var qtdMaisProdutos = parseInt($('.produtos-buscas').data('qtd-mais-produtos'), 10) + 1;
		var qtdInicialProdutos = parseInt($('.produtos-buscas').data('qtd-inicial-produtos'), 10) + 1;
		var tamanhoImagem = $('.produtos-buscas').data('tamanho-imagem');

		if (((pagina - 1) * qtdMaisProdutos) <= totalProdutos) {
			console.log('chamou pagina ' + pagina);
			$('.produtos-buscas').data('pagina', pagina);
			$.ajax({
				url: WEBROOT + 'catalogo/ajax_produtos',
				data: {
					busca_id: buscaId,
					pagina: pagina
				},
				dataType: 'html',
				success: function(data) {
					$('.produtos-buscas').append(data);
					$.waypoints('refresh');
				},
				error: function() {
					$('.produtos-buscas').data('pagina', pagina - 1);
					$.waypoints('refresh');
					alert('Erro ao buscar mais produtos.');
				}
			});
		}
	}, {
		offset: function() {
			return $.waypoints('viewportHeight') - $(this).height() + waypoints_offset_rodape;
		}
	});

	$('.produtos-buscas-ordenacao a').on('click', function() {
		var buscaId = $('.produtos-buscas').data('busca-id');
		var totalProdutos = parseInt($('.produtos-buscas').data('total-produtos'), 10);
		var ordem = $(this).data('ordem');
		var ordemDir = $(this).data('ordem-dir');
		var pagina = 0;

		$.ajax({
			url: WEBROOT + 'catalogo/ajax_produtos',
			data: {
				busca_id: buscaId,
				ordem: ordem,
				ordem_dir: ordemDir
			},
			dataType: 'html',
			success: function(data) {
				$('.produto-busca').remove();
				$('.produtos-buscas').append(data);
				$('.produtos-buscas').data('pagina', pagina);
				$.waypoints('refresh');
			},
			error: function() {
				$.waypoints('refresh');
				alert('Erro ao ordenar produtos.');
			}
		});

		return false;
	});
});