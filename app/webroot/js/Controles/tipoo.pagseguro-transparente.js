$(document).ready(function() {
	tipooPagSeguro.init();
});

var tipooPagSeguro = {
	init: function() {
		this.valorTotal = $('.t-valor-total').val();

		this.checkBinOnChange();
		this.changeBrand();
		this.getInstallments();
		this.installmentsOnChange();
		this.buyWithCreditCardOnsubmit();
		this.buyBoleto();
		this.buyEFT();
		this.mask();
		this.checkTypeOfPerson();
		this.editCreditCardHolder();
	},
	checkBinOnChange: function () {
		var self = this;
		// Usa os 6 primeiros dígitos para identificar a bandeira
		$('.t-pagseguro-transparente-cc-numero').on('keyup', function() {
			var bin = $(this).val();

			if (bin.length >= 6) {
				self.getBrand(bin);
			} else {
				$('.t-pagseguro-transparente-cc-codigo-seguranca').closest('.input').show();
				$('.t-pagseguro-transparente-cc-validade-mes').closest('.input').show();
				$('.t-pagseguro-transparente-cc-validade-ano').closest('.input').show();
				$('.t-pagseguro-transparente-cc-senha').closest('.input').hide();
			}
		});
	},
	getBrand: function(bin) {
		var self = this;

		PagSeguroDirectPayment.getBrand({
			cardBin: bin,
			success: function(response) {
				//bandeira encontrada
				console.log('sucesso');
				$('.t-pagseguro-transparente-cc-bandeira[data-name="' + response.brand.name + '"]').attr('checked', true);

				if (response.brand.config.hasCvv) {
					$('.t-pagseguro-transparente-cc-codigo-seguranca').closest('.input').show();
				} else {
					$('.t-pagseguro-transparente-cc-codigo-seguranca').closest('.input').hide();
					$('.t-pagseguro-transparente-cc-codigo-seguranca').val('');
				}

				if (response.brand.config.hasDueDate) {
					$('.t-pagseguro-transparente-cc-validade-mes').closest('.input').show();
					$('.t-pagseguro-transparente-cc-validade-ano').closest('.input').show();
				} else {
					$('.t-pagseguro-transparente-cc-validade-mes').closest('.input').hide();
					$('.t-pagseguro-transparente-cc-validade-mes').val('');

					$('.t-pagseguro-transparente-cc-validade-ano').closest('.input').hide();
					$('.t-pagseguro-transparente-cc-validade-ano').val('');
				}

				if (response.brand.config.hasPassword) {
					$('.t-pagseguro-transparente-cc-senha').closest('.input').show();
				} else {
					$('.t-pagseguro-transparente-cc-senha').closest('.input').hide();
					$('.t-pagseguro-transparente-cc-senha').val('');
				}

				self.getInstallments();
			},
			error: function(response) {
				//tratamento do erro
				console.log('Cartão inválido.');
			},
			complete: function(response) {
				//tratamento comum para todas chamadas
				console.log('Complete');
			}
		});
	},
	changeBrand: function() {
		var self = this;
		$('.t-pagseguro-transparente-cc-bandeira').on('click', function() {
			self.getInstallments();
		});
	},
	getInstallments: function() {
		var self = this;
		bandeira = $('.t-pagseguro-transparente-cc-bandeira:checked').val();

		PagSeguroDirectPayment.getInstallments({
			amount: self.valorTotal,
			brand: bandeira,
			success: function(response) {
				var options = '';
				$.each(response.installments[bandeira], function(index, value) {
					if (value.interestFree) {
						var juros = 'sem juros';
					} else {
						var juros = 'com juros';
					}
					options += '<option data-installment-amount="' + self.number_format(value.installmentAmount, 2, '.', '.') + '" value="' + value.quantity + '">' + value.quantity + 'x R$ ' + self.number_format(value.installmentAmount, 2, ',', '.') + ' ' + juros + '</option>';
				});

				$('.t-pagseguro-transparente-cc-parcelas').html(options);

				self.changeInstallmentAmount();
			},
			error: function(response) {
				console.log('Erro');
			},
			complete: function(response) {
				console.log('Complete');
			}
		});
	},
	installmentsOnChange: function() {
		var self = this;

		$('.t-pagseguro-transparente-cc-parcelas').on('change', function() {
			self.changeInstallmentAmount();
		});
	},
	changeInstallmentAmount: function() {
		var installmentAmount = $('option[value="' + $('.t-pagseguro-transparente-cc-parcelas').val() + '"]').data('installment-amount');
		$('.t-pagseguro-transparente-cc-valor-parcelas').val(installmentAmount);
	},
	buyBoleto: function() {
		var self = this;

		$('.t-pagseguro-transparente-pagamento-boleto-form').on('submit', function() {
			var senderHash = PagSeguroDirectPayment.getSenderHash();

			if ($('.t-sender-hash').val(senderHash)) {
				$.ajax({
					url: WEBROOT + 'checkout/ajaxPagamentoPagSeguroTransparenteEnviarTransacao',
					type: 'POST',
					data: $('.t-pagseguro-transparente-pagamento-boleto-form').serialize(),
					beforeSend: function() {
						$('.t-pagar-boleto').attr('disabled', true);
						$('.t-pagar-boleto').before('<img class="loader-boleto" src="' + WEBROOT + 'theme/' + PROJETO + '/img/loader-pagamento.gif" />');
						console.log('Enviando...');
					},
					success: function(data) {
						var retorno = jQuery.parseJSON(data);
						self.cleanErrors();

						if (typeof retorno.errors === 'object') {
							self.showErrors(retorno.errors);

							$('.t-pagar-boleto').removeAttr('disabled');
							$('.loader-boleto').remove();
						} else {
							document.location.href = WEBROOT + 'checkout/compra_finalizada?meio=PagSeguroTransparente&pedido=' + retorno.transaction.reference;
							console.log('sucesso!');
							console.log(retorno);
						}
					},
					error: function(data) {
						$('.t-pagar-boleto').removeAttr('disabled');
						$('.loader-boleto').remove();
						alert('Falha de comunicação ao tentar enviar os dados. Por favor, tente novamente.');
					}
				});
			}

			return false;
		});
	},
	buyWithCreditCardOnsubmit: function() {
		var self = this;

		$('.t-pagseguro-transparente-pagamento-cartao-credito-form').on('submit', function() {
			var senderHash = PagSeguroDirectPayment.getSenderHash();
			$('.t-sender-hash').val(senderHash);

			var param = {
				cardNumber: $(".t-pagseguro-transparente-cc-numero").val(),
				cvv: $(".t-pagseguro-transparente-cc-codigo-seguranca").val(),
				expirationMonth: $(".t-pagseguro-transparente-cc-validade-mes").val(),
				expirationYear: $(".t-pagseguro-transparente-cc-validade-ano").val(),
				success: function(response) {
					//token gerado, esse deve ser usado na chamada da API do Checkout Transparente
					$('.t-pagseguro-transparente-cc-token').val(response.card.token);

					$.ajax({
						url: WEBROOT + 'checkout/ajaxPagamentoPagSeguroTransparenteEnviarTransacao',
						type: 'POST',
						data: $('.t-pagseguro-transparente-pagamento-cartao-credito-form').serialize(),
						beforeSend: function() {
							$('.t-pagar-cartao-credito').attr('disabled', true);
							$('.t-pagar-cartao-credito').before('<img class="loader-credito" src="' + WEBROOT + 'theme/' + PROJETO + '/img/loader-pagamento.gif" />');
							console.log('Enviando...');
						},
						success: function(data) {
							var retorno = jQuery.parseJSON(data);

							self.cleanErrors();

							if (typeof retorno.errors === 'object') {
								self.showErrors(retorno.errors);

								$('.t-pagar-cartao-credito').removeAttr('disabled');
								$('.loader-credito').remove();
							} else {
								document.location.href = WEBROOT + 'checkout/compra_finalizada?meio=PagSeguroTransparente&pedido=' + retorno.transaction.reference;
								console.log('sucesso!');
								console.log(retorno);
							}
						},
						error: function(data) {
							$('.t-pagar-cartao-credito').removeAttr('disabled');
							$('.loader-credito').remove();
							alert('Falha de comunicação ao tentar enviar os dados. Por favor, tente novamente.');
						}
					});
				},
				error: function(response) {
					var errors = new Object();
					errors.error = new Object();

					$.each(response.errors, function(index, value) {
						errors.error.code = index;
						errors.error.message = value;
					});

					self.showErrors(errors);
				},
				complete: function(response) {
					//tratamento comum para todas chamadas
				}
			}

			//parâmetro opcional para qualquer chamada
			var bandeira = $('.t-pagseguro-transparente-cc-bandeira:checked').val();
			if(bandeira != '') {
				param.brand = bandeira;
			}

			PagSeguroDirectPayment.createCardToken(param);

			return false;
		});
	},
	buyEFT: function() {
		var self = this;

		$('.t-pagseguro-transparente-pagamento-debito-form').on('submit', function() {
			var senderHash = PagSeguroDirectPayment.getSenderHash();

			if ($('.t-sender-hash').val(senderHash)) {
				$.ajax({
					url: WEBROOT + 'checkout/ajaxPagamentoPagSeguroTransparenteEnviarTransacao',
					type: 'POST',
					data: $('.t-pagseguro-transparente-pagamento-debito-form').serialize(),
					beforeSend: function() {
						$('.t-pagar-debito').attr('disabled', true);
						$('.t-pagar-debito').before('<img class="loader-debito" src="' + WEBROOT + 'theme/' + PROJETO + '/img/loader-pagamento.gif" />');
						console.log('Enviando...');
					},
					success: function(data) {
						var retorno = jQuery.parseJSON(data);
						self.cleanErrors();

						if (typeof retorno.errors === 'object') {
							self.showErrors(retorno.errors);

							$('.t-pagar-debito').removeAttr('disabled');
							$('.loader-debito').remove();
						} else {
							document.location.href = WEBROOT + 'checkout/compra_finalizada?meio=PagSeguroTransparente&pedido=' + retorno.transaction.reference;
							console.log('sucesso!');
							console.log(retorno);
						}
					},
					error: function(data) {
						$('.t-pagar-debito').removeAttr('disabled');
						$('.loader-debito').remove();
						alert('Falha de comunicação ao tentar enviar os dados. Por favor, tente novamente.');
					}
				});
			}

			return false;
		});
	},
	mask: function() {
		$('#DonoCartaoTelefone').mask('(99)9999-9999?9');
		$('#DonoCartaoDataNascto').mask('99/99/9999');
		$('#DonoCartaoCpf').mask('999.999.999-99');

		$('#PagSeguroTransparenteCcValidadeMes').mask('99');
		$('#PagSeguroTransparenteCcValidadeAno').mask('9999');
	},
	checkTypeOfPerson: function() {
		var self = this;

		if ($('#DonoCartaoNome').val() == '') {
			$('.t-pagseguro-transparente-dados-dono-cartao .t-editar').hide();
			$('.t-pagseguro-transparente-dados-dono-cartao .t-dados-dono-cartao.t-text').hide();
			$('.t-pagseguro-transparente-dados-dono-cartao .t-dados-dono-cartao.t-input').show();
		}
	},
	editCreditCardHolder: function() {
		$('.t-pagseguro-transparente-dados-dono-cartao .t-editar').on('click', function(e) {
			e.preventDefault();
			$('.t-pagseguro-transparente-dados-dono-cartao .t-dados-dono-cartao').toggle();
			$(this).hide();
		});
	},
	showErrors: function(errors) {
		var html = '';

		if ($.isArray(errors.error)) {
			$.each(errors.error, function(index, value) {
				value.message = messages.translate(value.code, value.message);
				html += '<li>' + value.message + ' <span>(' + value.code + ')</span></li>';
			});
		} else {
			errors.error.message = messages.translate(errors.error.code, errors.error.message);
			html += '<li>' + errors.error.message + ' <span>(' + errors.error.code + ')</span></li>';
		}

		$('.t-pagseguro-transparente-erro ul').html(html);
		$('.t-pagseguro-transparente-erro').show();
		$('html, body').animate({scrollTop:$('.t-pagseguro-transparente-erro').offset().top}, 'slow');
	},
	cleanErrors: function() {
		$('.t-pagseguro-transparente-erro ul').empty();
		$('.t-pagseguro-transparente-erro').hide();
	},
	number_format: function(number, decimals, dec_point, thousands_sep) {
		number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
		var n = !isFinite(+number) ? 0 : +number,
		prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
		sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
		dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
		s = '',
		toFixedFix = function (n, prec) {
			var k = Math.pow(10, prec);
			return '' + Math.round(n * k) / k;
		};
		// Fix for IE parseFloat(0.55).toFixed(0) = 0;
		s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
		if (s[0].length > 3) {
		s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
		}
		if ((s[1] || '').length < prec) {
		s[1] = s[1] || '';
		s[1] += new Array(prec - s[1].length + 1).join('0');
		}
		return s.join(dec);
	}
};

var messages = {
	translate: function(codeError, messageError) {

		var msg_original = null;
		var msg_ptbr = null;

		var msg_ptbr_amigavel = null;

		switch (codeError) {
			case '30405':
				msg_original = 'invalid date format.';
				msg_ptbr = 'Formato de data inválido.';

				msg_ptbr_amigavel = 'Formato de data inválido. MÊS XX e ANO XXXX';

			break;

			case '10001':

				msg_original = 'creditcard number with invalid length.';
				msg_ptbr = 'Número de caracteres do cartão é inválido.';

				msg_ptbr_amigavel = 'Número de caracteres do cartão é inválido.';

			break;

			case '10002':

				msg_original = 'invalid date format.';
				msg_ptbr = 'Formato de data inválido.';

				msg_ptbr_amigavel = 'Formato de data inválido.';

			break;

			case '10004':

				msg_original = 'cvv is mandatory .';
				msg_ptbr = 'Informe o código de segurança do cartão.';

				msg_ptbr_amigavel = 'Informe o código de segurança do cartão.';

			break;

			case '10006':

				msg_original = 'security field with invalid length (10006).';
				msg_ptbr = 'Código de segurança inválido.';

				msg_ptbr_amigavel = 'Código de segurança inválido.';

			break;

			case '53004':

				msg_original = 'items invalid quantity.';
				msg_ptbr = 'Quantidade inválida de itens.';

				msg_ptbr_amigavel = 'Quantidade de itens inválida.';

				break;

			case '53005':

				msg_original = 'currency is required.';
				msg_ptbr = 'A moeda é obrigatória.';

				msg_ptbr_amigavel = 'Informe a moeda corrente. (Real)';

				break;

			case '53006':

				msg_original = 'currency invalid value: {0}';
				msg_ptbr = 'Valor de moeda inválido: {0}';

				msg_ptbr_amigavel = 'Moeda corrente inválida: {0}';

				break;

			case '53007':

				msg_original = 'reference invalid length: {0}';
				msg_ptbr = 'Tamanho de referência inválido: {0}';

				msg_ptbr_amigavel = 'Formato de moeda inválida: {0}';

				break;

			case '53008':

				msg_original = 'notificationURL invalid length: {0}';
				msg_ptbr = 'Tamanho da URL de notificação inválido: {0}';

				msg_ptbr_amigavel = 'Tamanho da URL de notificação inválido: {0}';

				break;

			case '53009':

				msg_original = 'notificationURL invalid value: {0}';
				msg_ptbr = 'Formato da URL de notificação inválido: {0}';

				msg_ptbr_amigavel = 'URL de notificação inválida: {0}';

				break;

			case '53010':

				msg_original = 'sender email is required.';
				msg_ptbr = 'O email do remetente é obrigatório.';

				msg_ptbr_amigavel = 'Informe o email do comprador.';

				break;

			case '53011':

				msg_original = 'sender email invalid length: {0}';
				msg_ptbr = 'O tamanho de email do remetente é invalido: {0}';

				msg_ptbr_amigavel = 'Tamanho do email do comprador inválido: {0}';

				break;

			case '53012':

				msg_original = 'sender email invalid value: {0}';
				msg_ptbr = 'O valor de email do remetente é invalido:{0}';

				msg_ptbr_amigavel = 'Email do comprador inválido: {0}';

				break;

			case '53013':

				msg_original = 'sender name is required.';
				msg_ptbr = 'O nome do remetente é obrigatório';

				msg_ptbr_amigavel = 'Informe o nome do comprador';

				break;

			case '53014':

				msg_original = 'sender name invalid length: {0}';
				msg_ptbr = 'O tamanho da descrição do nome do remetente é invalido: {0}';

				msg_ptbr_amigavel = 'Tamanho do nome do comprador inválido: {0}';

				break;

			case '53015':

				msg_original = 'sender name invalid value: {0}';
				msg_ptbr = 'O valor do nome do remetente é invalido: {0}';

				msg_ptbr_amigavel = 'Nome do comprador inválido: {0}';

				break;

			case '53017':

				msg_original = 'sender cpf invalid value: {0}';
				msg_ptbr = 'O valor do cpf do remetente é invalido: {0}';

				msg_ptbr_amigavel = 'CPF do comprador inválido: {0}';

				break;

			case '53018':

				msg_original = 'sender area code is required.';
				msg_ptbr = 'O código de área do remetente é obrigatório';

				msg_ptbr_amigavel = 'Informe o código de área do comprador.';

				break;

			case '53019':

				msg_original = 'sender area code invalid value: {0}';
				msg_ptbr = 'O valor do código de área do remetente é invalido: {0}';

				msg_ptbr_amigavel = 'Código de área do comprador inválido: {0}';

				break;

			case '53020':

				msg_original = 'sender phone is required.';
				msg_ptbr = 'O telefone do remetente é obrigatório';

				msg_ptbr_amigavel = 'Informe o email do comprador.'

				break;

			case '53021':

				msg_original = 'sender phone invalid value: {0}';
				msg_ptbr = 'O valor do telefone do remetente é invalido: {0}';

				msg_ptbr_amigavel = 'Telefone do comprador inválido: {0}';

				break;

			case '53022':

				msg_original = 'shipping address postal code is required.';
				msg_ptbr = 'O código postal do endereço de entrega é obrigatório.';

				msg_ptbr_amigavel = 'Informe o código postal do endereço de entrega';

				break;

			case '53023':

				msg_original = 'shipping address postal code invalid value: {0}';
				msg_ptbr = 'O valor do código postal do endereço de entrega é inválido: {0}';

				msg_ptbr_amigavel = 'Código postal do endereço de entrega inválido: {0}';

				break;

			case '53024':

				msg_original = 'shipping address street is required.';
				msg_ptbr = 'A rua do endereço de entrega é obrigatório.';

				msg_ptbr_amigavel = 'Informe a rua do endereço de entrega.'

				break;

			case '53025':

				msg_original = 'shipping address street invalid length: {0}';
				msg_ptbr = 'O tamanho da descrição da rua do endereço de entrega é invalido : {0}';

				msg_ptbr_amigavel = 'O tamanho da descrição da rua do endereço de entrega do comprador é inválido: {0}';

				break;

			case '53026':

				msg_original = 'shipping address number is required.';
				msg_ptbr = 'O número do endereço de entrega é obrigatório.';

				msg_ptbr_amigavel = 'Informe o número do endereço de entrega.';

				break;

			case '53027':

				msg_original = 'shipping address number invalid length: {0}';
				msg_ptbr = 'O tamanho da descrição do número do endereço de entrega é invalido : {0}';

				msg_ptbr_amigavel = 'O tamanho da descrição do número do endereço de entrega do comprador é inválido: {0}';

				break;

			case '53028':

				msg_original = 'shipping address complement invalid length: {0}';
				msg_ptbr = 'O tamanho da descrição do complemento do endereço de entrega é inválido: {0}';

				msg_ptbr_amigavel = 'O tamanho da descrição do complemento do endereço de entrega é inválido: {0}';

				break;

			case '53029':

				msg_original = 'shipping address district is required.';
				msg_ptbr = 'O bairro de entrega é obrigatório.';

				msg_ptbr_amigavel = 'Informe o bairro de entrega.';

				break;

			case '53030':

				msg_original = 'shipping address district invalid length: {0}';
				msg_ptbr = 'O tamanho do nome do Bairro do endereço de entrega é inválido: {0}';

				msg_ptbr_amigavel = 'O tamanho do nome do bairro do endereço de entrega é inválido: {0}';

				break;

			case '53031':

				msg_original = 'shipping address city is required.';
				msg_ptbr = 'A cidade do endereço de entrega é obrigatório.';

				msg_ptbr_amigavel = 'Informe a cidade do endereço de entrega.';

				break;

			case '53032':

				msg_original = 'shipping address city invalid length: {0}';
				msg_ptbr = 'O tamanho do nome da cidade do endereço de entrega é inválido: {0}';

				msg_ptbr_amigavel = 'O tamanho do nome da cidade do endereço de entrega é inválido: {0}';

				break;

			case '53033':

				msg_original = 'shipping address state is required.';
				msg_ptbr = 'O estado do endereço de entrega é obrigatório.';

				msg_ptbr_amigavel = 'Informe o estado do endereço de entrega.';

				break;

			case '53034':

				msg_original = 'shipping address state invalid value: {0}';
				msg_ptbr = 'O tamanho do nome do estado do endereço de entrega é inválido: {0}';

				msg_ptbr_amigavel = 'O tamanho do nome do estado do endereço de entrega é inválido: {0}';

				break;

			case '53035':

				msg_original = 'shipping address country is required.';
				msg_ptbr = 'O país do endereço de entrega é obrigatório.';

				msg_ptbr_amigavel = 'Informe o país do endereço de entrega.';

				break;

			case '53036':

				msg_original = 'shipping address country invalid length: {0}';
				msg_ptbr = 'O tamanho do nome do país do endereço de entrega é inválido: {0}';

				msg_ptbr_amigavel = 'O tamanho do nome do país do endereço de entrega é inválido: {0}';

				break;

			case '53037':

				msg_original = 'credit card token is required.';
				msg_ptbr = 'O token do cartão de crédito é obrigatório.';

				msg_ptbr_amigavel = 'Informe o token de segurança do cartão de crédito.';

				break;

			case '53038':

				msg_original = 'installment quantity is required.';
				msg_ptbr = 'A quantidade de parcelas é obrigatória.';

				msg_ptbr_amigavel = 'Informe a quantidade de parcelas.';

				break;

			case '53039':

				msg_original = 'installment quantity invalid value: {0}';
				msg_ptbr = 'O valor da quantidade de parcelas é inválido: {0}';

				msg_ptbr_amigavel = 'Quantidade de parcelas inválida. {0}';

				break;

			case '53040':

				msg_original = 'installment value is required.';
				msg_ptbr = 'O valor da parcela é obrigatório.';

				msg_ptbr_amigavel = 'Informe o valor da parcela.';

				break;

			case '53041':

				msg_original = 'installment value invalid value: {0}';
				msg_ptbr = 'O valor de parcela é inválido: {0}';

				msg_ptbr_amigavel = 'Valor da parcela inválido. {0}';

				break;

			case '53042':

				msg_original = 'credit card holder name is required.';
				msg_ptbr = 'O nome do titular do cartão é obrigatório';

				msg_ptbr_amigavel = 'Informe o nome do titular do cartão.';

				break;

			case '53043':

				msg_original = 'credit card holder name invalid length: {0}';
				msg_ptbr = 'O tamanho da descrição do nome do titular do cartão é inválido: {0}';

				msg_ptbr_amigavel = 'O tamanho da descrição do nome do titular do cartão é inválido: {0}';

				break;

			case '53044':

				msg_original = 'credit card holder name invalid value: {0}';
				msg_ptbr = 'O valor do nome do titular do cartão é inválido:{0}';

				msg_ptbr_amigavel = 'Nome do titular do cartão inválido: {0}';

				break;

			case '53045':

				msg_original = 'credit card holder cpf is required.';
				msg_ptbr = 'O cpf do titular do cartão é obrigatório.';

				msg_ptbr_amigavel = 'Informe o CPF do titular do cartão.';

				break;

			case '53046':

				msg_original = 'credit card holder cpf invalid value: {0}';
				msg_ptbr = 'O valor do cpf do titular do cartão é inválido:{0}';

				msg_ptbr_amigavel = 'CPF do titular inválido: {0}';

				break;

			case '53047':

				msg_original = 'credit card holder birthdate is required.';
				msg_ptbr = 'A data de nascimento do titular do cartão é obrigatório.';

				msg_ptbr_amigavel = 'Informe a data de nascimento do titular do cartão';

				break;

			case '53048':

				msg_original = 'credit card holder birthdate invalid value: {0}';
				msg_ptbr = 'O valor da data de nascimento do titular do cartão é inválido:{0}';

				msg_ptbr_amigavel = 'Data de nascimento inválida: {0}';

				break;

			case '53049':

				msg_original = 'credit card holder area code is required.';
				msg_ptbr = 'O código de área do titular do cartão é obrigatório.';

				msg_ptbr_amigavel = 'Informe o código de área do titular do cartão.';

				break;

			case '53050':

				msg_original = 'credit card holder area code invalid value: {0}';
				msg_ptbr = 'O valor do código de área do titular do cartão é inválido:{0}';

				msg_ptbr_amigavel = 'O código de área do titular do cartão é inválido; {0}';

				break;

			case '53051':

				msg_original = 'credit card holder phone is required.';
				msg_ptbr = 'O telefone do titular do cartão é obrigatório.';

				msg_ptbr_amigavel = 'Informe o telefone do titular do cartão.';

				break;

			case '53052':

				msg_original = 'credit card holder phone invalid value: {0}';
				msg_ptbr = 'O valor do telefone do titular do cartão é inválido:{0}';

				msg_ptbr_amigavel = 'O telefone do titular do cartão é inválido:{0}';

				break;

			case '53053':

				msg_original = 'billing address postal code is required.';
				msg_ptbr = 'O código postal do endereço de cobrança é obrigatório.';

				msg_ptbr_amigavel = 'Informe o código postal do endereço de cobrança.';

				break;

			case '53054':

				msg_original = 'billing address postal code invalid value: {0}';
				msg_ptbr = 'O valor do código postal do endereço de cobrança é inválido:{0}';

				msg_ptbr_amigavel = 'O código postal do endereço de cobrança é inválido:{0}';

				break;

			case '53055':

				msg_original = 'billing address street is required.';
				msg_ptbr = 'A rua do endereço de cobrança é obrigatória.';

				msg_ptbr_amigavel = 'Informe a rua do endereço de cobrança.';

				break;

			case '53056':

				msg_original = 'billing address street invalid length: {0}';
				msg_ptbr =  'O valor da rua do endereço de cobrança é inválido:{0}';

				msg_ptbr_amigavel = 'A rua do endereço de cobrança é inválida:{0}';

				break;

			case '53057':

				msg_original = 'billing address number is required.';
				msg_ptbr = 'O número do endereço de cobrança é obrigatório.';

				msg_ptbr_amigavel = 'Informe o número do endereço de cobrança.';

				break;

			case '53058':

				msg_original = 'billing address number invalid length: {0}';
				msg_ptbr = 'O valor do número do endereço de cobrança é inválido:{0}';

				msg_ptbr_amigavel = 'O número do endereço de cobrança é inválido:{0}';

				break;

			case '53059':

				msg_original = 'billing address complement invalid length: {0}';
				msg_ptbr = 'O tamanho da descrição do complemento do endereço de cobrança é inválido: {0}';

				msg_ptbr_amigavel = 'O tamanho da descrição do complemento do endereço de cobrança é inválido: {0}';

				break;

			case '53060':

				msg_original = 'billing address district is required.';
				msg_ptbr = 'O bairro do endereço de cobrança é obrigatório.';

				msg_ptbr_amigavel = 'Informe o bairro do endereço de cobrança.';

				break;

			case '53061':

				msg_original = 'billing address district invalid length: {0}';
				msg_ptbr = 'O tamanho da descrição do bairro do endereço de cobrança é inválido: {0}';

				msg_ptbr_amigavel = 'O tamanho da descrição do bairro do endereço de cobrança é inválido: {0}';

				break;

			case '53062':

				msg_original = 'billing address city is required.';
				msg_ptbr = 'A cidade do endereço de cobrança é obrigatório.';

				msg_ptbr_amigavel = 'Informe a cidade do endereço de cobrança.';

				break;

			case '53063':

				msg_original = 'billing address city invalid length: {0}';
				msg_ptbr = 'O tamanho da descrição da cidade do endereço de cobrança é inválido: {0}';

				msg_ptbr_amigavel = 'O tamanho da descrição da cidade do endereço de cobrança é inválido: {0}';

				break;

			case '53064':

				msg_original = 'billing address state is required.';
				msg_ptbr = 'O estado do endereço de cobrança é obrigatório.';

				msg_ptbr_amigavel = 'Informe o estado do endereço de cobrança';

				break;

			case '53065':

				msg_original = 'billing address state invalid value: {0}';
				msg_ptbr = 'O tamanho da descrição do estado do endereço de cobrança é inválido: {0}';

				msg_ptbr_amigavel = 'O tamanho da descrição do estado do endereço de cobrança é inválido: {0}';

				break;

			case '53066':

				msg_original = 'billing address country is required.';
				msg_ptbr = 'O país do endereço de cobrança é obrigatório.';

				msg_ptbr_amigavel = 'Informe o país do endereço de cobrança.';

				break;

			case '53067':

				msg_original = 'billing address country invalid length: {0}';
				msg_ptbr = 'O tamanho da descrição do país do endereço de cobrança é inválido:{0}';

				msg_ptbr_amigavel = 'O tamanho da descrição do país do endereço de cobrança é inválido:{0}';

				break;

			case '53068':

				msg_original = 'receiver email invalid length: {0}';
				msg_ptbr = 'O tamanho do email do receptor é inválido: {0}';

				msg_ptbr_amigavel = 'O tamanho do email do vendedor é inválido: {0}';

				break;

			case '53069':

				msg_original = 'receiver email invalid value: {0}';
				msg_ptbr = 'O valor do email do receptor é inválido:{0}';

				msg_ptbr_amigavel = 'Email do vendedor inválido:{0}';

				break;

			case '53070':

				msg_original = 'item id is required.';
				msg_ptbr = 'O identificador do item é obrigatório.';

				msg_ptbr_amigavel = 'Informe o identificador do item.';

				break;

			case '53071':

				msg_original = 'item id invalid length: {0}';
				msg_ptbr = 'O tamanho da descrição do id do item é inválido:{0}';

				msg_ptbr_amigavel = 'O tamanho do identificador do item é inválido:{0}';

				break;

			case '53072':

				msg_original = 'item description is required.';
				msg_ptbr = 'A descrição do item é obrigatória.';

				msg_ptbr_amigavel = 'Informe a descrição do item.';

				break;

			case '53073':

				msg_original = 'item description invalid length: {0}';
				msg_ptbr = 'O tamanho da descrição da descrição do item é inválida:{0}';

				msg_ptbr_amigavel = 'O tamanho da descrição do item é inválida:{0}';

				break;

			case '53074':

				msg_original = 'item quantity is required.';
				msg_ptbr = 'A quantidade do item é obrigatória.';

				msg_ptbr_amigavel = 'Informe a quantidade do item.';

				break;

			case '53075':

				msg_original = 'item quantity out of range: {0}';
				msg_ptbr = 'Quantidade de item fora da faixa.';

				msg_ptbr_amigavel = 'Quantidade fora da faixa: {0}';

				break;

			case '53076':

				msg_original = 'item quantity invalid value: {0}';
				msg_ptbr = 'O valor da quantidade do item é inválido:{0}';

				msg_ptbr_amigavel = 'Quantidade de intem inválida: {0}';

				break;

			case '53077':

				msg_original = 'item amount is required.';
				msg_ptbr = 'O valor do item é obrigatório.';

				msg_ptbr_amigavel = 'Informe o valor unitário do item.';

				break;

			case '53078':

				msg_original = 'item amount invalid pattern: {0}. Must fit the patern: \\d+.\\d\{2\}';
				msg_ptbr = 'O valor unitário do item é inválido: {0}. Deve seguir o padrão: \\d+.\\d\{2\}';

				msg_ptbr_amigavel = 'O valor unitário do item é inválido: {0}. Deve seguir o padrão: \\d+.\\d\{2\}';

				break;

			case '53079':

				msg_original = 'item amount out of range: {0}';
				msg_ptbr = 'O valor unitário do item está fora do intervalo: {0}';

				msg_ptbr_amigavel = 'Valor unitário fora da faixa: {0}';

				break;

			case '53081':

				msg_original = 'sender is related to receiver.';
				msg_ptbr = 'O remetente está relacionado com o receptor.';

				msg_ptbr_amigavel = 'Comprador semelhante ao vendedor.';

				break;

			case '53084':

				msg_original = 'invalid receiver: {0}, verify receiver`s account status and if it is a seller`s account.';
				msg_ptbr = 'Receptor inválido: {0}, verificar o status da conta do destinatário e, se for uma conta do vendedor.';

				msg_ptbr_amigavel = 'Vendedor inválido: {0}, verificar o status da conta do destinatário e, se for uma conta do vendedor.';

				break;

			case '53085':

				msg_original = 'payment method unavailable.';
				msg_ptbr = 'Método de pagamento indisponível';

				msg_ptbr_amigavel = 'Método de pagamento indisponível';

				break;

			case '53086':

				msg_original = 'cart total amount out of range: {0}';
				msg_ptbr = 'A valor total do carriho está fora do intervalo: {0}';

				msg_ptbr_amigavel = 'O valor do pedido está fora da faixa: {0}';

				break;

			case '53087':

				msg_original = 'invalid credit card data.';
				msg_ptbr = 'Dados do cartão de crédito inválido.';

				msg_ptbr_amigavel = 'Dados do cartão de crédito inválido.';

				break;

			case '53091':

				msg_original = 'sender hash invalid.';
				msg_ptbr = 'O hash do remetente é inválido.';

				msg_ptbr_amigavel = 'Hash do comprador é inválido';

				break;

			case '53092':

				msg_original = 'credit card brand is not accepted.';
				msg_ptbr = 'A bandeira do cartão não é aceita.';

				msg_ptbr_amigavel = 'Essa bandeira de cartão não é aceita.';

				break;

			case '53095':

				msg_original = 'shipping type invalid pattern: {0}';
				msg_ptbr = 'Tipo de transporte padrão inválido: {0}';

				msg_ptbr_amigavel = 'Tipo de transporte inválido.';

				break;

			case '53096':

				msg_original = 'shipping cost invalid pattern: {0}';
				msg_ptbr = 'Custo de transporte padrão inválido: {0}';

				msg_ptbr_amigavel = 'Custo de transporte inválido.';

				break;

			case '53097':

				msg_original = 'shipping cost out of range: {0}';
				msg_ptbr = 'Custo de transporte fora de faixa: {0}';

				msg_ptbr_amigavel = 'Custo de transporte fora de faixa: {0}';

				break;

			case '53098':

				msg_original = 'cart total value is negative: {0}';
				msg_ptbr = 'O valor total do carrinho é negativo: {0}';

				msg_ptbr_amigavel = 'O valor total do carrinho não pode ser negativo: {0}';

				break;

			case '53099':

				msg_original = 'extra amount invalid pattern: {0}. Must fit the patern: -?\\d+.\\d\{2\}';
				msg_ptbr = 'O valor extra é inválido: {0}. Deve caber no modelo: -?\\d+.\\d\{2\}';

				msg_ptbr_amigavel = 'O valor extra é inválido: {0} . Ele deve seguir o modelo: -?\\d+.\\d\{2\}';

				break;

			case '53101':

				msg_original = 'payment mode invalid value, valid values are default and gateway.';
				msg_ptbr = 'O valor do modo de pagamento é inválido, os valores válidos são padrão e gateway.';

				msg_ptbr_amigavel = 'Modo de pagamento inválido, os valores válidos são : default e gateway.';

				break;

			case '53102':

				msg_original = 'payment method invalid value, valid values are creditCard, boleto e eft.';
				msg_ptbr = 'Método de pagamento é inválido, os valores válidos são : cartão de crédito, boleto e débito em conta.';

				msg_ptbr_amigavel = 'Método de pagamento é inválido, os valores válidos são : cartão de crédito, boleto e débito em conta.';

				break;

			case '53104':

				msg_original = 'shipping cost was provided, shipping address must be complete.';
				msg_ptbr = 'O custo de transporte foi fornecido, o endereço de entrega deve ser completo.';

				msg_ptbr_amigavel = 'O custo de transporte foi fornecido, o endereço de entrega deve ser completo.';

				break;

			case '53105':

				msg_original = 'sender information was provided, email must be provided too.';
				msg_ptbr = 'As informações do remetente foram fornecidas, o email deve ser fornecido também.';

				msg_ptbr_amigavel = 'As informações do remetente foram fornecidas, o email deve ser fornecido também.';

				break;

			case '53106':

				msg_original = 'credit card holder is incomplete.';
				msg_ptbr = 'O titular do cartão de crédito está incompleto.';

				msg_ptbr_amigavel = 'Complete os dados do titular do cartão.';

				break;

			case '53109':

				msg_original = 'shipping address information was provided, sender email must be provided too.';
				msg_ptbr = 'As informações sobre o endereço do transporte foram fornecidas, o email do remetente deve ser fornecido também.';

				msg_ptbr_amigavel = 'As informações sobre o endereço do transporte foram fornecidas, o email do remetente deve ser fornecido também.';

				break;

			case '53110':

				msg_original = 'eft bank is required.';
				msg_ptbr =  'O banco do débito em conta é obrigatótio';

				msg_ptbr_amigavel = 'Informe o banco do débito em conta.'

				break;

			case '53111':

				msg_original = 'eft bank is not accepted.';
				msg_ptbr = 'O banco do débito em conta não é aceito.';

				msg_ptbr_amigavel = 'O banco do débito em conta não é aceito.';

				break;

			case '53115':

				msg_original = 'sender born date invalid value: {0}';
				msg_ptbr = 'A data de nascimento do remetente é inválida: {0}';

				msg_ptbr_amigavel = 'A data de nascimento do comprador é inválida: {0}';

				break;

			case '53122':

				msg_original = 'sender email invalid domain: {0}. You must use an email @sandbox.pagseguro.com.br';
				msg_ptbr = 'O domínio de email do remetente é inválido: {0}. Você deve usar email @sandbox.pagseguro.com.br';

				msg_ptbr_amigavel = 'Email do comprador inválido: {0}. Use no formato @sandbox.pagseguro.com.br';

				break;

			case '53140':

				msg_original = 'installment quantity out of range: {0}. The value must be greater than zero.';
				msg_ptbr = 'A quantidade de parcela  está fora do intervalo: {0}. O valor deve ser maior que zero. ';

				msg_ptbr_amigavel = 'Quantidade de parcela fora da faixa. O valor tem que ser maior que zero.'

				break;

			case '53141':

				msg_original = 'sender is blocked.';
				msg_ptbr = 'Remetente bloqueado.';

				msg_ptbr_amigavel = 'Comprador bloqueado.';

				break;

			case '53142':

				msg_original = 'credit card token invalid.';
				msg_ptbr = 'O token do cartão de crédito é inválido';

				msg_ptbr_amigavel = 'Token de segurança do cartão de crédito é inválido.';

				break;

			default:

				msg_ptbr = messageError;

				break;
		}

		return msg_ptbr;
	}
}