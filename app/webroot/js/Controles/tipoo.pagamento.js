$(document).ready(function() {
	if ($("#CieloPagamentoForm").length > 0) {

		if ($('#CieloCcBandeiraAtiva').length) {
			var bandeira = $('#CieloCcBandeiraAtiva').val();
			var parcela_id = $('#CieloParcelaIdAtiva').val();
			$('#' + bandeira).attr('checked', true);
			$('#CieloParcelaId').val(parcela_id);
			$('.cc_bandeira').trigger('change');
		} else {
			$('.cc_bandeira:first').prop("checked", true);
		}

		$('#CieloParcelaId').html($('.parcelas.' + $('.cc_bandeira:checked').val()).html());

		if ($('#CieloParcelaIdAtiva').length) {
			var parcela_id = $('#CieloParcelaIdAtiva').val();
			$('#CieloParcelaId').val(parcela_id);
		}

		$('#CieloCcCodigoSeguranca').mask("" + $('.cc_bandeira:checked').data('mascara-cc-codigo-seguranca'));
		$('#CieloCcNumero').mask("" + $('.cc_bandeira:checked').data('mascara-cc-numero'));

		$('#CieloCcValidadeMes').mask('99');
		$('#CieloCcValidadeAno').mask('9999');

		$('#CieloPagamentoForm').validate();
		$("#CieloCcValidadeMes").rules("add", {required: true});
		$("#CieloCcValidadeAno").rules("add", {required: true});
		$("#CieloCcNumero").rules("add", {required: true});
		$("#CieloCcCodigoSeguranca").rules("add", {required: true});

		$('.cc_bandeira').on('change', function() {
			$('#CieloParcelaId').html($('.parcelas.' + $('.cc_bandeira:checked').val()).html());
			$('#CieloCcValidadeMes, #CieloCcValidadeAno, #CieloCcNumero, #CieloCcCodigoSeguranca').val('');
			$('#CieloCcNumero').unmask().mask("" + $('.cc_bandeira:checked').data('mascara-cc-numero'));
			$('#CieloCcCodigoSeguranca').unmask().mask("" + $('.cc_bandeira:checked').data('mascara-cc-codigo-seguranca'));
		});
	}
});