$(document).ready(function() {
	$('.t-newsletter').submit(function() {
		$this = $(this);
		$.ajax({
			url: WEBROOT + 'newsletters/enviar',
			type: 'POST',
			data: $this.closest('.t-newsletter').serialize(),
			beforeSend: function() {
				$this.find('input[type="submit"]').hide();
				$this.find('.submit').append('<img class="loader-newsletter" src="' + WEBROOT + 'theme/' + PROJETO + '/img/loader-newsletter.gif" />');
			},
			success: function(data) {
				$this.fadeOut(function() {
					$this.after('<p class="mensagem-newsletter" style="display:none;">E-mail enviado com sucesso.</p>');
					$('.mensagem-newsletter').fadeIn();
				});
			},
			error: function(data) {
				$this.find('.submit .loader-newsletter').remove();
				$this.find('input[type="submit"]').show();
				alert('Falha de comunicação ao tentar enviar seu e-mail. Por favor, tente novamente.');
			}
		});

		return false;
	});
});