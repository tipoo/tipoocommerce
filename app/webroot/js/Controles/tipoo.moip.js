$(document).ready(function() {
	tipooMoip.init();
});

var tipooMoip = {
	init: function() {
		var self = this;

		this.mask();

		$('.forma-pagamento .moip-boleto .pagar-boleto').on('click', self.pagarBoleto);
		$('.forma-pagamento .moip-cartao-credito .pagar-cartao-credito').on('click', self.pagarCartaoCredito);

		$('#MoipPagamentoForm').on('submit', function() {return false;});
	},

	pagarBoleto: function() {
		tipooMoip.forma = $(this).data('forma');
		tipooMoip.limparErros();

		var settings = {
			"Forma": "BoletoBancario"
		};

		MoipWidget(settings);
	},

	pagarCartaoCredito: function() {
		tipooMoip.forma = $(this).data('forma');
		tipooMoip.limparErros();

		var settings = {
			"Forma": "CartaoCredito",
			"Instituicao": $('#MoipCcInstituicao:checked').val(),
			"Parcelas": $('#MoipParcelas').val(),
			"CartaoCredito": {
				"Numero": $('#MoipCcNumero').val(),
				"Expiracao": $('#MoipCcValidadeMes').val() + '/' + $('#MoipCcValidadeAno').val(),
				"CodigoSeguranca": $('#MoipCcCodigoSeguranca').val(),
				"Portador": {
					"Nome": $('#MoipCcDonoNome').val(),
					"DataNascimento": $('#MoipCcDonoDataNascto').val(),
					"Telefone": $('#MoipCcDonoTelefone').val(),
					"Identidade": $('#MoipCcDonoCpf').val()
				}
			}
		};

		MoipWidget(settings);
	},

	exibirErros: function(data) {
		if ($.isArray(data)) {
			for (var i = 0; i < data.length; i++) {
				var erro = data[i];
				$('.moip-erro ul').append('<li>' + erro.Mensagem + ' <span>(' + erro.Codigo + ')</span>');
			}
		} else {
			$('.moip-erro ul').append('<li>' + data.Mensagem + ' <span>(' + data.Codigo + ')</span>');
		}

		$('.moip-erro').show();
	},

	limparErros: function() {
		$('.moip-erro ul').empty();
		$('.moip-erro').hide();
	},

	sucesso: function(data) {
		$('#MoipForma').val(tipooMoip.forma);
		$('#MoipRetorno').val(JSON.stringify(data));
		$('#MoipFinalizar').submit();
	},

	falha: function(data) {
		console.log(data);
		tipooMoip.exibirErros(data);
	},

	mask: function() {
		$('#MoipCcDonoDataNascto').mask('99/99/9999');
		$('#MoipCcDonoCpf').mask('999.999.999-99');
		$('#MoipCcDonoTelefone').mask('(99)9999-9999?9');
	}

};