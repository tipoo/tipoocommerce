$(document).ready(function() {
	$('a[rel="tooltip"]').tooltip();
	$('span[data-html="true"]').tooltip();

	$('.btn-action-save-default').on('click', function(ev) {

		if ($(this).closest('form').valid()) {
			$(this).button('loading');
		}

	});

	$('.btn-action-save-default').closest('form').on('submit', function() {
		$(this).keypress(function (e) {
			var code = null;
			code = (e.keyCode ? e.keyCode : e.which);
			return (code == 13) ? false : true;
		});

		if ($(this).valid()) {
			$(this).find('.btn-action-save-default').button('loading');
		}
	});

/*
	$(this).bind("contextmenu",function(e){
		return false;
	});

	$(this).mousedown(function(e){
		if( e.button == 2 ) {
			var id_vitrine = $('.modal_').data('id');
			//alert(id_vitrine);
			$('.editar-vitrine-colecao').trigger("click");

		  return false;

		}
		return true;
	});*/


});