<?php 
App::uses('SmtpTransport', 'Network/Email');

class SendgridTransport extends SmtpTransport {

	/**
	  * Regras:
	  *
	  * $from pode ser uma string apenas com o e-mail, um array chave/valor com o e-mail na chave e nome no valor ou um array apenas com o e-mail
	  * $to pode ser uma string apenas com o e-mail ou um array com uma lista de e-mails (sem os nomes)
	  * $replyTo deve ser uma string apenas com o e-mail
	  * $cc e $bcc são ignorados por esse transport
	  */
	public function send(CakeEmail $email) {
		$this->_cakeEmail = $email;
		$url = $this->_config['host'];
		$user = $this->_config['username'];
		$pass = $this->_config['password'];

		$json_string = array(
			'to' => $this->getTo()
		);

		$params = array(
			'api_user'  => $user,
			'api_key'   => $pass,
			'x-smtpapi' => wordwrap(json_encode($json_string), 76, "\n "),
			'to'        => 'contato@tipoo.com.br', // usando x-smtpapi este destinatrio não irá receber e-mail, mas ele é necessário
			'replyto'   => $this->getReplyTo(),
			'subject'   => $this->_cakeEmail->subject(),
			'html'      => $this->getContent(),
			'text'      => $this->getContent(),
			'from'      => $this->getFromEmail(),
			'fromname'      => $this->getFromName(),
		);

		$request = $url.'api/mail.send.json';
		$session = curl_init($request);
		curl_setopt ($session, CURLOPT_POST, true);
		curl_setopt ($session, CURLOPT_POSTFIELDS, $params);
		curl_setopt($session, CURLOPT_HEADER, false);
		curl_setopt($session, CURLOPT_RETURNTRANSFER, true);

		$response = curl_exec($session);
		curl_close($session);

		CakeLog::write(LOG_ERR, var_export($response, true));
		
	}

	private function getContent() {
		$lines = $this->_cakeEmail->message();
		$messages = array();
		foreach ($lines as $line) {
			if ((!empty($line)) && ($line[0] === '.')) {
				$messages[] = '.' . $line;
			} else {
				$messages[] = $line;
			}
		}
		
		$message = implode("\r\n", $messages);
		
		return $message;
	}

	private function getTo() {
		$to = $this->_cakeEmail->to();
		
		if (is_string($to)) {
			return array($to);
		}

		return array_values($to);
	}

	private function getFromEmail() {
		$from = $this->_cakeEmail->from();

		if (is_array($from)) {
			// Retorna apenas o primeiro item do array
			foreach ($from as $key => $value) {
				if (is_numeric($key)) {
					return $value;
				} else {
					return $key;
				}
			}
		}
		return $from;
	}

	private function getFromName() {
		$from = $this->_cakeEmail->from();

		if (is_array($from)) {
			// Retorna apenas o primeiro item do array
			foreach ($from as $value) {
				return $value;
			}
		}

		return $from; // retorna o próprio e-mail
	}

	private function getReplyTo() {
		$replyTo = $this->_cakeEmail->replyTo();

		if (!$replyTo) {
			return $this->getFromEmail();
		}

		if (is_array($replyTo)) {
			// Retorna apenas o primeiro item do array
			foreach ($replyTo as $key => $value) {
				return $value;
			}
		}

		return $replyTo;
	}

}

?>