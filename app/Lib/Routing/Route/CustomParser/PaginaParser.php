<?php
App::uses('Pagina', 'Model');

class PaginaParser extends AbstractParser {
	private $Pagina;

	public function __construct() {
		$this->Pagina = new Pagina();
	}

	public function parse($params, $slug) {
		if ($slug['Slug']['controller'] == 'paginas' && $slug['Slug']['action'] == 'ver') {
			$pagina = $this->Pagina->find('first', array(
				'contain' => false,
				'conditions' => array(
					'Pagina.slug_id' => $slug['Slug']['id']
				)
			));

			if ($pagina) {
				$params['id'] = $pagina['Pagina']['id'];	
			}
			
		}

		$params['controller'] = $slug['Slug']['controller'];
		$params['action'] = $slug['Slug']['action'];

		return $params;
	}
}
?>