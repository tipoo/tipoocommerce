<?php
abstract class AbstractParser {

	abstract public function parse($params, $slug);

}
?>