<?php
App::uses('Tag', 'Model');

class TagParser extends AbstractParser {
	private $Tag;

	public function __construct() {
		$this->Tag = new Tag();
	}

	public function parse($params, $slug) {
		if ($slug['Slug']['controller'] == 'catalogo' && $slug['Slug']['action'] == 'tag') {
			$tag = $this->Tag->find('first', array(
				'contain' => false,
				'conditions' => array(
					'Tag.slug_id' => $slug['Slug']['id']
				)
			));

			if ($tag) {
				$params['id'] = $tag['Tag']['id'];	
			}
			
		}

		$params['controller'] = $slug['Slug']['controller'];
		$params['action'] = $slug['Slug']['action'];

		return $params;
	}
}
?>