<?php
App::uses('Marca', 'Model');

class MarcaParser extends AbstractParser {
	private $Marca;

	public function __construct() {
		$this->Marca = new Marca();
	}

	public function parse($params, $slug) {
		if ($slug['Slug']['controller'] == 'catalogo' && $slug['Slug']['action'] == 'marca') {
			$marca = $this->Marca->find('first', array(
				'contain' => false,
				'conditions' => array(
					'Marca.slug_id' => $slug['Slug']['id']
				)
			));

			if ($marca) {
				$params['id'] = $marca['Marca']['id'];	
			}
			
		}

		$params['controller'] = $slug['Slug']['controller'];
		$params['action'] = $slug['Slug']['action'];

		return $params;
	}
}
?>