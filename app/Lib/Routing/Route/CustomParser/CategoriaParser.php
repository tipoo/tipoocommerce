<?php
App::uses('Categoria', 'Model');

class CategoriaParser extends AbstractParser {
	private $Categoria;

	public function __construct() {
		$this->Categoria = new Categoria();
	}

	public function parse($params, $slug) {
		if ($slug['Slug']['controller'] == 'catalogo' && $slug['Slug']['action'] == 'categoria') {
			$categoria = $this->Categoria->find('first', array(
				'contain' => false,
				'conditions' => array(
					'Categoria.slug_id' => $slug['Slug']['id']
				)
			));

			if ($categoria) {
				$params['id'] = $categoria['Categoria']['id'];	
			}
			
		}

		$params['controller'] = $slug['Slug']['controller'];
		$params['action'] = $slug['Slug']['action'];

		return $params;
	}
}
?>