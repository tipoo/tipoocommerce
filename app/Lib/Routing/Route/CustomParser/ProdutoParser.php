<?php
App::uses('Produto', 'Model');

class ProdutoParser extends AbstractParser {
	private $Produto;

	public function __construct() {
		$this->Produto = new Produto();
	}

	public function parse($params, $slug) {
		if ($slug['Slug']['controller'] == 'catalogo' && $slug['Slug']['action'] == 'produto') {
			$produto = $this->Produto->find('first', array(
				'contain' => false,
				'conditions' => array(
					'Produto.slug_id' => $slug['Slug']['id']
				)
			));

			if ($produto) {
				$params['id'] = $produto['Produto']['id'];	
			}
			
		}

		$params['controller'] = $slug['Slug']['controller'];
		$params['action'] = $slug['Slug']['action'];

		return $params;
	}
}
?>