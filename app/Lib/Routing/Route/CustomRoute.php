<?php 
App::uses('CakeRoute', 'Routing/Route');

class CustomRoute extends CakeRoute {

	public function parse($url) {
		$params = parent::parse($url);
		if (empty($params)) {
			return false;
		}

		// API
		if (strpos($url, '/api') !== false) {
			$params_retorno['controller'] = $params['pass'][1];
			$params_retorno['prefix'] = 'api';

			if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
				$params_retorno['action'] = 'edit';
				$params_retorno['pass'][1] = $params['pass'][2];

			} else if ($_SERVER['REQUEST_METHOD'] == 'DELETE') {
				$params_retorno['action'] = 'delete';
				debug($params['pass'][1]);
				$params_retorno['pass'][1] = $params['pass'][2];

			} else if ($_SERVER['REQUEST_METHOD'] == 'POST') {
				if (count($params['pass']) > 2) {
					$params_retorno['action'] = 'edit';
					$params_retorno['pass'][1] = $params['pass'][2];

				} else {
					$params_retorno['action'] = 'add';
				}

			} else if ($_SERVER['REQUEST_METHOD'] == 'GET') {
				if (count($params['pass']) > 2) {
					$params_retorno['action'] = 'view';
					$params_retorno['pass'][1] = $params['pass'][2];

				} else {
					$params_retorno['action'] = 'index';
				}
			}

			return $params_retorno;
		}

		// Para o ambiente administrativo não há url amigável
		if ($params['pass'][0] == 'admin') {
			return false;
		}

		$url =  '';
		foreach ($params['pass'] as $param) {
			$url .= '/'.$param;
		}

		App::uses('Slug', 'Model');
		$Slug = new Slug();
		$slug = $Slug->find('first', array(
			'conditions' => array(
				'Slug.url' => $url,
				'Slug.ativo'
			)
		));
		if ($slug) {
			if ($slug['Slug']['custom_parser_class']) {
				App::uses('AbstractParser', 'Routing/Route/CustomParser');
				App::uses($slug['Slug']['custom_parser_class'], 'Routing/Route/CustomParser');
				$parser =  new $slug['Slug']['custom_parser_class']();

				return $parser->parse($params, $slug);

			} else {
				$params['controller'] = $slug['Slug']['controller'];
				$params['action'] = $slug['Slug']['action'];
			}
			return $params;
		}
		
		return false;
	}

}
?>