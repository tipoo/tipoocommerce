<?php
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
App::uses('AppController', 'Controller');
class BannersController extends AppController {

	public $uses = array('Banner', 'BannerTipo');

	public function admin_index($id = null) {

		$conditions = array(
			'Banner.ativo' => true,
			'Banner.banner_tipo_id' => $id
		);

		$this->paginate = array(
			'conditions' => $conditions,
			'limit' => Configure::read('Sistema.Paginacao.limit'),
			'order' => array(
				'Banner.ordem' => 'ASC'
			)
		);

		$bannerTipo = $this->BannerTipo->find('first', array(
			'contain' => array(
				'Marca.descricao',
				'Categoria.descricao'
			),
			'conditions' => array(
				'BannerTipo.id' => $id
			)
		));

		$this->set('banners', $this->paginate());
		$this->set('banner_tipo_id', $id);
		$this->set('tipo', $this->getTipoBanner());
		$this->set('bannerTipo', $bannerTipo);
	}

	public function admin_adicionar($id = null) {

		$tipo = $this->BannerTipo->find('first', array(
			'contain' => false,
			'conditions' => array(
				'BannerTipo.id' => $id
			),
			'fields' => array('qtd')
		));

		$bannerTipo = $this->BannerTipo->find('first', array(
			'contain' => array(
				'Marca.descricao',
				'Categoria.descricao'
			),
			'conditions' => array(
				'BannerTipo.id' => $id
			)
		));

		$banners = $this->Banner->find('count', array(
			'conditions' => array(
				'Banner.ativo' => true,
				'Banner.banner_tipo_id' => $id
			)
		));

		if ($this->request->is('post')) {

			$this->request->data['Banner']['banner_tipo_id'] = $id;

			$diretorio = $this->getDiretorioBanners();
			$this->FileUpload->permitirSomenteImagens();

			if ($this->request->data['Banner']['imagem']['name'] == '') {
				unset($this->request->data['Banner']['imagem']);
			} else {
				$path_parts = pathinfo($this->request->data['Banner']['imagem']['name']);

				$nomeImagem = strtolower($this->request->data['Banner']['descricao']);
				$this->request->data['Banner']['imagem']['name'] = Inflector::slug($nomeImagem, '_') . '.' . $path_parts['extension'];
				$this->request->data['Banner']['imagem'] = $this->FileUpload->upload($diretorio, array($this->request->data['Banner']['imagem']), true);
			}

			$this->request->data['Banner']['ordem'] = $banners + 1;

			if ($this->request->data['Banner']['data_inicio'] == '') {
				unset($this->request->data['Banner']['data_inicio']);
			}

			if ($this->request->data['Banner']['data_fim'] == '') {
				unset($this->request->data['Banner']['data_fim']);
			}

			$this->Banner->create();
			if ($this->Banner->save($this->request->data)) {
				$this->Session->setFlash('Banner salvo com sucesso.', FLASH_SUCCESS);
				$this->backToPaginatorIndex();
			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar salvar o banner. Por favor, tente novamente.', FLASH_ERROR);
			}
		}

		$this->set('banner_tipo_id', $id);
		$this->set('bannerTipo', $bannerTipo);
		$this->set('tipo', $this->getTipoBanner());

	}

	public function admin_editar($id = null , $banner_tipo_id = null) {
		$this->Banner->id = $id;

		$bannerTipo = $this->BannerTipo->find('first', array(
			'contain' => array(
				'Marca.descricao',
				'Categoria.descricao'
			),
			'conditions' => array(
				'BannerTipo.id' => $banner_tipo_id
			)
		));

		if (!$this->Banner->exists()) {
			throw new NotFoundException('Banner inexistente.');
		}

		if ($this->request->is('post') || $this->request->is('put')) {

			$diretorio = $this->getDiretorioBanners();
			$this->FileUpload->permitirSomenteImagens();

			if ($this->request->data['Banner']['imagem']['name'] == '') {
				unset($this->request->data['Banner']['imagem']);
			} else {
				$path_parts = pathinfo($this->request->data['Banner']['imagem']['name']);

				$nomeImagem = strtolower($this->request->data['Banner']['descricao']);
				$this->request->data['Banner']['imagem']['name'] = Inflector::slug($nomeImagem, '_') . '.' . $path_parts['extension'];
				$this->request->data['Banner']['imagem'] = $this->FileUpload->upload($diretorio, array($this->request->data['Banner']['imagem']), true);
			}

			if ($this->request->data['Banner']['data_inicio'] == '') {
				unset($this->request->data['Banner']['data_inicio']);
			}

			if ($this->request->data['Banner']['data_fim'] == '') {
				unset($this->request->data['Banner']['data_fim']);
			}


			if ($this->Banner->save($this->request->data)) {
				$this->Session->setFlash('Banner salvo com sucesso.', FLASH_SUCCESS);
				$this->backToPaginatorIndex();
			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar editar o banner. Por favor, tente novamente.', FLASH_ERROR);
			}
		}

		$this->request->data = $this->Banner->read(null, $id);

		$this->set('bannerTipo', $bannerTipo);
		$this->set('tipo', $this->getTipoBanner());
	}

	public function admin_excluir($id = null, $banner_tipo_id = null) {
		$this->Banner->id = $id;

		if (!$this->Banner->exists()) {
			throw new NotFoundException('Banner inexistente.');
		}

		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Banner->saveField('ativo', false, false)) {

				$banners = $this->Banner->find('all', array(
					'conditions' => array(
						'Banner.ativo' => true,
						'Banner.banner_tipo_id' => $banner_tipo_id,
					),
					'order' => array(
						'Banner.ordem' => 'ASC'
					)
				));

				$count_banners = 1;
				foreach ($banners as $key => $banner_) {
					$this->request->data[$key]['Banner']['id'] = $banner_['Banner']['id'];
					$this->request->data[$key]['Banner']['ordem'] = $count_banners;

					$count_banners++;
				}

				$this->Banner->saveMany($this->request->data, array('deep' => true));

				$this->Session->setFlash('Banner excluído com sucesso.', FLASH_SUCCESS);

			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar excluir o banner. Por favor, tente novamente.', FLASH_ERROR);
			}
		}
		$this->backToPaginatorIndex();
	}

	private function getDiretorioBanners() {
		$diretorio = $this->getDiretorioProjeto('banners');
		new Folder($diretorio, true);

		return $diretorio;
	}

	private function getTipoBanner() {
		return array(
			'home' => 'Home',
			'marca' => 'Marca',
			'categoria' => 'Categoria',
			'generico' => 'Genérico'
		);
	}

	public function admin_ajax_ordenar($banner_tipo_id = null) {

		$qtd_banners = $this->Banner->find('count', array(
			'conditions' => array(
				'Banner.ativo' => true,
				'Banner.banner_tipo_id'  => $banner_tipo_id
			),
		));

		$count_banners = 1;

		while ($count_banners <= $qtd_banners) {

			$this->request->data[$count_banners]['Banner']['id'] = $this->params['named']['ordem_' . $count_banners];
			$this->request->data[$count_banners]['Banner']['ordem'] = $count_banners;

			if ($count_banners == 1) {
				$this->request->data[$count_banners]['Banner']['destaque'] = true;
			} else {
				$this->request->data[$count_banners]['Banner']['destaque'] = false;
			}

			$count_banners++;
		}

		if ($this->Banner->saveMany($this->request->data, array('deep' => true))) {
			$json = array('sucesso' => true);
		} else {
			$json = array('sucesso' => false, 'mensagem' => 'Ocorreu um erro ao tentar ordenar os banners. Por favor, tente novamente.');
		}

		$this->renderJson($json);

	}

}

?>