<?php
App::uses('AppController', 'Controller');

class ColecoesController extends AppController {

	public $uses = array('Colecao', 'ColecoesProduto', 'Produto', 'ColecoesLocal', 'Marca');

	public function admin_index() {

			$conditions = array(
				'Colecao.ativo' => true
			);

			$this->paginate = array(
				'conditions' => $conditions,
				'order' => array(
					'Colecao.descricao'
				),
				'limit' => 50,
				'order' => array(
					'Colecao.id' => 'DESC'
				)
			);

			$colecoes_local = $this->ColecoesLocal->find('all', array(
				'conditions' => array(
					'ColecoesLocal.ativo' => true
				)
			));

			$this->set('colecoes_local', $colecoes_local);
			$this->set('colecoes', $this->paginate());

	}

	public function admin_adicionar($id = null) {

		if ($this->request->is('post')) {
			$this->Colecao->create();

			if ($this->Colecao->save($this->request->data)) {
				$this->Session->setFlash('Coleção salva com sucesso.', FLASH_SUCCESS);
				$this->backToPaginatorIndex();
			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar salvar a Coleção. Por favor, tente novamente.', FLASH_ERROR);
			}
		}
	}

	public function admin_editar($id = null) {
		$this->Colecao->id = $id;
		$colecaoEditando = $this->Colecao->findById($id);

		if (!$this->Colecao->exists()) {
			throw new NotFoundException('Coleção inexistente.');
		}
		if ($this->request->is('post') || $this->request->is('put')) {

			if ($this->Colecao->save($this->request->data)) {

				$this->Session->setFlash('Coleção salva com sucesso.', FLASH_SUCCESS);
				$this->backToPaginatorIndex();
			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar salvar a Coleção. Por favor, tente novamente.', FLASH_ERROR);
			}
		} else {
			$this->request->data = $this->Colecao->read(null, $id);
			$this->set('colecao', $colecaoEditando);
		}
	}

	public function admin_excluir($id = null) {
		$this->Colecao->id = $id;

		if (!$this->Colecao->exists()) {
			throw new NotFoundException('Coleção inexistente.');
		}

		if ($this->request->is('post')) {

			/* Verifica se a coleção já esta sendo utilizada por alguma página para poder ser excluída */
			$colecao = $this->Colecao->find('first', array(
				'contain' => array(
					'ColecoesLocal' => array(
						'conditions' => array(
							'ColecoesLocal.ativo' => true
						)
					)
				),
				'conditions' => array(
					'Colecao.id' => $id,
					'Colecao.ativo' => true,
				)
			));

			$colecoes_produtos = $this->ColecoesProduto->find('all', array(
				'contain' => false,
				'conditions' => array(
					'ColecoesProduto.colecao_id' => $id
				),
			));

			foreach ($colecoes_produtos as $colecoes_produtos) {
				$this->ColecoesProduto->id = $colecoes_produtos['ColecoesProduto']['id'];
				$this->ColecoesProduto->saveField('ativo', false, false);
			}

			if (count($colecao['ColecoesLocal']) == 0) {

				if ($this->Colecao->saveField('ativo', false, false)) {
					$this->Session->setFlash('Coleção excluída com sucesso.', FLASH_SUCCESS);
					$this->backToPaginatorIndex();
				} else {
					$this->Session->setFlash('Ocorreu um erro ao tentar excluir a Coleção. Por favor, tente novamente.', FLASH_ERROR);
				}
			} else {
				$this->Session->setFlash('Não é possível exluir esta coleção, pois ela já se encontra em uso.', FLASH_ERROR);
				$this->redirect($this->referer());
			}
		}
	}

	public function admin_produtos($id = null) {
		if ($this->request->is('post') || $this->request->is('put')) {
			$produtoExiste = $this->ColecoesProduto->find('count', array(
				'conditions' => array(
					'ColecoesProduto.produto_id' => $this->request->data('ColecoesProduto.produto_id'),
					'ColecoesProduto.colecao_id' => $id
				)
			));

			$qtd_produtos = $this->ColecoesProduto->find('count', array(
				'conditions' => array(
					'ColecoesProduto.colecao_id' => $id
				),
			));

			$this->request->data['ColecoesProduto']['ordem'] = $qtd_produtos + 1;

			if (!$produtoExiste) {
				$this->ColecoesProduto->create();
				if ($this->ColecoesProduto->save($this->request->data)) {
					$this->Session->setFlash('Produto adicionado a coleção com sucesso.', FLASH_SUCCESS);
				} else {
					$this->Session->setFlash('Ocorreu um erro ao tentar adicionar produto a coleção. Por favor, tente novamente.', FLASH_ERROR);
				}
			} else {
				$this->Session->setFlash('O produto que você tentou adicionar já estava na coleção.', FLASH_WARNING);
			}

			$this->request->data('ColecoesProduto.produto_id', null);
			$this->request->data('ColecoesProduto.descricao', null);
		}

		$order = array(
			'ColecoesProduto.ordem' => 'ASC',
		);

		$colecao = $this->Colecao->find('first', array(
			'contain' => array(
				'ColecoesProduto.Produto'
			),
			'conditions' => array(
				'Colecao.ativo',
				'Colecao.id' => $id
			),
		));

		$produtos = $this->ColecoesProduto->find('all', array(
			'contain' => array(
				'Produto.Marca',
				'Colecao'
			),
			'conditions' => array(
				'ColecoesProduto.colecao_id' => $id,
			),
			'order' => $order
		));

		$this->request->data('ColecoesProduto.colecao_id', $id);
		$this->set('colecao', $colecao);
		$this->set('produtos', $produtos);
	}

	public function admin_ajax_buscar_produtos($descricao = null) {
		$produtos = $this->Produto->find('all', array(
			'contain' => array(
				'Marca.descricao'
			),
			'conditions' => array(
				'Produto.ativo',
				'Produto.descricao LIKE "%'.$descricao.'%"'
			)
		));

		$produtosArray = array();
		foreach ($produtos as $produto) {
			$produtosArray[] = array(
				'id' => $produto['Produto']['id'],
				'descricao' => $produto['Produto']['descricao'],
				'marca' => $produto['Marca']['descricao'],
				'value' => $produto['Produto']['descricao'] . ' - ' . $produto['Marca']['descricao'] . ' - #' . $produto['Produto']['id']
			);
		}

		$this->renderJson($produtosArray);
	}

	public function admin_excluir_produto($id = null, $colecoesProduto_id = null) {

		$this->ColecoesProduto->id = $colecoesProduto_id;
		if ($this->ColecoesProduto->delete()) {

			$produtos = $this->ColecoesProduto->find('all', array(
				'conditions' => array(
					'ColecoesProduto.colecao_id' => $id,
				),
				'order' => array(
					'ColecoesProduto.ordem' => 'ASC'
				)
			));

			$count_produtos = 1;
			foreach ($produtos as $key => $produto) {
				$this->request->data[$key]['ColecoesProduto']['id'] = $produto['ColecoesProduto']['id'];
				$this->request->data[$key]['ColecoesProduto']['ordem'] = $count_produtos;
				$count_produtos++;
			}

			$this->ColecoesProduto->saveMany($this->request->data, array('deep' => true));

			$this->Session->setFlash('Produto removido da coleção com sucesso.', FLASH_SUCCESS);
		} else {
			$this->Session->setFlash('Ocorreu um erro ao tentar remover o produto da coleção. Por favor, tente novamente.', FLASH_ERROR);
		}

		$this->redirect(array('controller' => 'colecoes', 'action' => 'produtos', 'admin' => true, $id));
	}

	public function admin_ajax_ordenar($colecao_id = null) {

		$qtd_produtos = $this->ColecoesProduto->find('count', array(
				'conditions' => array(
				'ColecoesProduto.colecao_id' => $colecao_id,
			),
		));

		$count_produtos = 1;

		while ($count_produtos <= $qtd_produtos) {

			$this->request->data[$count_produtos]['ColecoesProduto']['id'] = $this->params['named']['ordem_' . $count_produtos];
			$this->request->data[$count_produtos]['ColecoesProduto']['ordem'] = $count_produtos;

			$count_produtos++;

		}

		if ($this->ColecoesProduto->saveMany($this->request->data, array('deep' => true))) {
			$json = array('sucesso' => true);
		} else {
			$json = array('sucesso' => false, 'mensagem' => 'Ocorreu um erro ao tentar ordenar os Produtos. Por favor, tente novamente.');
		}

		$this->renderJson($json);

	}

}
?>