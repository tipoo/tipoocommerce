<?php
App::uses('AppController', 'Controller');

class ObjetosController extends AppController {

	public function admin_index() {

			$conditions = array(
				'Objeto.ativo' => true
			);

			$this->paginate = array(
				'conditions' => $conditions,
				'order' => array(
					'Objeto.plugin' => 'ASC',
					'Objeto.controller' => 'ASC',
					'Objeto.action' => 'ASC'
				),
				//'limit' => Configure::read('Sistema.Paginacao.limit')
				'limit' => 50
			);
			$this->set('objetos', $this->paginate());

	}

	public function admin_adicionar($id = null) {

		if ($this->request->is('post')) {
			$this->Objeto->create();
			if($this->request->data['Objeto']['plugin'] == ''){
				unset($this->request->data['Objeto']['plugin']);
			}
			if ($this->Objeto->save($this->request->data)) {
				$this->Session->setFlash('Objeto salvo com sucesso.', FLASH_SUCCESS);
				$this->backToPaginatorIndex();
			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar salvar o Objeto. Por favor, tente novamente.', FLASH_ERROR);
			}
		}
	}

	public function admin_editar($id = null) {
		$this->Objeto->id = $id;
		$objetoEditando = $this->Objeto->findById($id);

		if (!$this->Objeto->exists()) {
			throw new NotFoundException('Objeto inexistente.');
		}
		if ($this->request->is('post') || $this->request->is('put')) {

			if ($this->Objeto->save($this->request->data)) {

				if($this->request->data['Objeto']['plugin'] == '') {
					$this->Objeto->saveField('plugin', null, false);
				}

				$this->Session->setFlash('Objeto salvo com sucesso.', FLASH_SUCCESS);
				$this->backToPaginatorIndex();
			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar salvar o Objeto. Por favor, tente novamente.', FLASH_ERROR);
			}
		} else {
			$this->request->data = $this->Objeto->read(null, $id);
			$this->set('objeto', $objetoEditando);
		}
	}

	public function admin_excluir($id = null) {
		$this->Objeto->id = $id;

		if (!$this->Objeto->exists()) {
			throw new NotFoundException('Objeto inexistente.');
		}

		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Objeto->saveField('ativo', false, false)) {
				$this->Session->setFlash('Objeto excluído com sucesso.', FLASH_SUCCESS);
				$this->backToPaginatorIndex();
			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar excluir o Objeto. Por favor, tente novamente.', FLASH_ERROR);
			}
		}
	}

}
?>