<?php
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
App::uses('AppController', 'Controller');
App::uses('CropImage', 'Vendor');

class ConfiguracoesController extends AppController {

	public $uses = array(
		'Configuracao',
		'Slug',
		'Produto'
	);

	public $components = array('RequestHandler');
	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow(array(
			'robots',
			'sitemap',
			'sitemap_categorias',
			'sitemap_marcas',
			'sitemap_produtos',
			'sitemap_tags',
			'sitemap_paginas'
		));
	}

	public function admin_index() {

		$configuracao = $this->Configuracao->find();

		$permissao_configuracao_analytics = $this->checkPermissaoConfiguracao('@configuracao_analytics');
		$permissao_configuracao_api_pass = $this->checkPermissaoConfiguracao('@configuracao_api_pass');
		$permissao_configuracao_api_user = $this->checkPermissaoConfiguracao('@configuracao_api_user');
		$permissao_configuracao_frete = $this->checkPermissaoConfiguracao('@configuracao_frete');
		$permissao_configuracao_imagens =  $this->checkPermissaoConfiguracao('@configuracao_imagens');
		$permissao_configuracao_meta_description = $this->checkPermissaoConfiguracao('@configuracao_meta_description');
		$permissao_configuracao_meta_keywords = $this->checkPermissaoConfiguracao('@configuracao_meta_keywords');
		$permissao_configuracao_parcelamento = $this->checkPermissaoConfiguracao('@configuracao_parcelamento');
		$permissao_configuracao_robots = $this->checkPermissaoConfiguracao('@configuracao_robots');

		$this->set('configuracao', $configuracao);
		$this->set('tipos', $this->getFreteTipos());
		$this->set('tamanho', $this->getTamanhoRecortes());

		$this->set('permissao_configuracao_analytics', $permissao_configuracao_analytics);
		$this->set('permissao_configuracao_api_pass', $permissao_configuracao_api_pass);
		$this->set('permissao_configuracao_api_user', $permissao_configuracao_api_user);
		$this->set('permissao_configuracao_frete', $permissao_configuracao_frete);
		$this->set('permissao_configuracao_imagens', $permissao_configuracao_imagens);
		$this->set('permissao_configuracao_meta_description', $permissao_configuracao_meta_description);
		$this->set('permissao_configuracao_meta_keywords', $permissao_configuracao_meta_keywords);
		$this->set('permissao_configuracao_parcelamento', $permissao_configuracao_parcelamento);
		$this->set('permissao_configuracao_robots', $permissao_configuracao_robots);
	}


	public function sitemap() {
		$this->layout= 'ajax';

		// Verrifica se existem tagas. Nem todos os sites possuem tags
		$slugs_tags = $this->Slug->find('count', array(
			'conditions' => array(
				'Slug.ativo' => true,
				'Slug.action' => 'tag'
			)
		));

		$this->set('slugs_tags', $slugs_tags);

		$this->RequestHandler->respondAs('xml');
	}

	public function sitemap_categorias() {
		$this->layout= 'ajax';

		$slugs_categorias = $this->Slug->find('all', array(
			'contain' => false,
			'conditions' => array(
				'Slug.ativo' => true,
				'Slug.action' => 'categoria'
			)
		));

		$this->set('slugs_categorias', $slugs_categorias);
		$this->RequestHandler->respondAs('xml');
	}

	public function sitemap_marcas() {
		$this->layout= 'ajax';

		$slugs_marcas = $this->Slug->find('all', array(
			'contain' => false,
			'conditions' => array(
				'Slug.ativo' => true,
				'Slug.action' => 'marca'
			)
		));

		$this->set('slugs_marcas', $slugs_marcas);
		$this->RequestHandler->respondAs('xml');
	}

	public function sitemap_produtos() {
		$this->layout= 'ajax';

		$produtos = $this->Produto->find('all', array(
			'contain' => array(
				'Slug',
				'Imagem' => array(
					'conditions' => array(
						'Imagem.destaque' => true
					)
				)
			),
			'conditions' => array(
				'Produto.ativo' => true,
			)
		));

		$this->set('produtos', $produtos);
		$this->RequestHandler->respondAs('xml');
	}

	public function sitemap_tags() {
		$this->layout= 'ajax';

		$slugs_tags = $this->Slug->find('all', array(
			'contain' => false,
			'conditions' => array(
				'Slug.ativo' => true,
				'Slug.action' => 'tag'
			)
		));

		$this->set('slugs_tags', $slugs_tags);
		$this->RequestHandler->respondAs('xml');
	}

	public function sitemap_paginas() {
		$this->layout= 'ajax';

		$slugs_paginas = $this->Slug->find('all', array(
			'contain' => false,
			'conditions' => array(
				'Slug.ativo' => true,
				'Slug.action' => 'ver'
			)
		));

		$this->set('slugs_paginas', $slugs_paginas);
		$this->RequestHandler->respondAs('xml');
	}

	public function robots(){
		$this->layout='ajax';
		$this->RequestHandler->respondAs('text');
	}

	public function admin_editar($id = 1) {
		$this->Configuracao->id = $id;

		$configuracao = $this->Configuracao->find('first',  array(
			'conditions' => array(
				'Configuracao.id' => $id
			)
		));

		$permissao_configuracao_analytics = $this->checkPermissaoConfiguracao('@configuracao_analytics');
		$permissao_configuracao_api_pass = $this->checkPermissaoConfiguracao('@configuracao_api_pass');
		$permissao_configuracao_api_user = $this->checkPermissaoConfiguracao('@configuracao_api_user');
		$permissao_configuracao_frete = $this->checkPermissaoConfiguracao('@configuracao_frete');
		$permissao_configuracao_imagens =  $this->checkPermissaoConfiguracao('@configuracao_imagens');
		$permissao_configuracao_meta_description = $this->checkPermissaoConfiguracao('@configuracao_meta_description');
		$permissao_configuracao_meta_keywords = $this->checkPermissaoConfiguracao('@configuracao_meta_keywords');
		$permissao_configuracao_parcelamento = $this->checkPermissaoConfiguracao('@configuracao_parcelamento');
		$permissao_configuracao_robots = $this->checkPermissaoConfiguracao('@configuracao_robots');

		if (!$this->Configuracao->exists()) {
			throw new NotFoundException('Configuracao inexistente.');
		}

		if(($this->request->is('post') || $this->request->is('put')) && !empty($this->request->data)) {

			if ($this->Configuracao->save($this->request->data)) {
				$this->Session->setFlash('Configuração salva com sucesso.', FLASH_SUCCESS);
				$this->backToPaginatorIndex();
			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar salvar a configuração. Por favor, tente novamente.', FLASH_ERROR);
			}
		}

		$this->request->data = $this->Configuracao->read(null, $id);
		$this->set('tipos', $this->getFreteTipos());
		$this->set('tamanho', $this->getTamanhoRecortes());

		$this->set('permissao_configuracao_analytics', $permissao_configuracao_analytics);
		$this->set('permissao_configuracao_api_pass', $permissao_configuracao_api_pass);
		$this->set('permissao_configuracao_api_user', $permissao_configuracao_api_user);
		$this->set('permissao_configuracao_frete', $permissao_configuracao_frete);
		$this->set('permissao_configuracao_imagens', $permissao_configuracao_imagens);
		$this->set('permissao_configuracao_meta_description', $permissao_configuracao_meta_description);
		$this->set('permissao_configuracao_meta_keywords', $permissao_configuracao_meta_keywords);
		$this->set('permissao_configuracao_parcelamento', $permissao_configuracao_parcelamento);
		$this->set('permissao_configuracao_robots', $permissao_configuracao_robots);
	}

	private function checkPermissaoConfiguracao($funcionalidade = null) {

		if ($this->Permissoes->check($this->usuarioAutenticado['perfil_id'], 'configuracoes', $funcionalidade)) {
			return true;

		} else {
			return false;
		}

	}

	private function getFreteTipos() {
		return array(
			'C' => 'Correio',
			'T' => 'Transportadora'
		);
	}

	private function getTamanhoRecortes() {
		return array(
			'thumb' => 'Thumb',
			'small' => 'Small',
			'normal' => 'Normal',
			'medium' => 'Medium',
			'big' => 'Big',
		);
	}


}