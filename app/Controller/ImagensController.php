<?php
App::uses('AppController', 'Controller');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
App::uses('CropImage', 'Vendor');

class ImagensController extends AppController {

	public $uses = array('Imagem', 'Sku', 'Produto', 'Configuracao');

	public function admin_index($referente_id = null) {

		$referente = $this->params['named']['referente'];

		$conditions = array(
			'Imagem.ativo' => true,
			'Imagem.' . $referente => $referente_id
		);

		$order = array(
			'Imagem.ordem' => 'ASC',
		);

		$imagens = $this->Imagem->find('all', array(
			'contain' => false,
			'conditions' => $conditions,
			'order' => $order,
		));

		if ($referente == 'produto_id') {
			$produto = $this->Produto->find('first', array(
				'contain' => false,
				'conditions' => array(
					'Produto.id' => $referente_id
				)
			));
			$this->set('produto', $produto);

		} else if ($referente == 'sku_id') {
			$sku = $this->Sku->find('first', array(
				'contain' => array(
					'Produto'
				),
				'conditions' => array(
					'Sku.id' => $referente_id
				)
			));
			$this->set('sku', $sku);
		}

		$this->set('imagens', $imagens);
		$this->set('referente_id', $referente_id);
		$this->set('referente', $referente);
	}

	public function admin_adicionar($referente_id = null) {

		$configuracao = $this->Configuracao->find('first');

		$referente = $this->params['named']['referente'];

		if ($referente == 'produto_id') {

			$produto = $this->Produto->find('first', array(
				'contain' => false,
				'conditions' => array(
					'Produto.id' => $referente_id
				),
				'fields' => array('id', 'descricao')
			));

			$this->set('produto', $produto);

			$nomeImagem = strtolower($produto['Produto']['descricao']);

		} else if ($referente == 'sku_id') {

			$sku = $this->Sku->find('first', array(
				'contain' => array(
					'Produto.descricao'
				),
				'conditions' => array(
					'Sku.id' => $referente_id
				),
				'fields' => array('id', 'produto_id', 'descricao')
			));

			$this->set('sku', $sku);

			$nomeImagem = strtolower($sku['Produto']['descricao'] . '_' . $sku['Sku']['descricao']);

		}

		if ($this->request->is('post')) {

			/* Imagem */

			$diretorio = $this->getDiretorioProdutos();
			$this->FileUpload->permitirSomenteImagens();

			if ($this->request->data['Imagem']['imagem']['name'] == '') {
				unset($this->request->data['Imagem']['imagem']);
			} else {
				$path_parts = pathinfo($this->request->data['Imagem']['imagem']['name']);

				$this->request->data['Imagem']['imagem']['name'] = Inflector::slug($nomeImagem, '_') . '.' . $path_parts['extension'];
				$this->request->data['Imagem']['imagem'] = $this->FileUpload->upload($diretorio, array($this->request->data['Imagem']['imagem']), true);
			}

			/* Fim Imagem */

			/* Recorte e Resize */

			$image = new CropImage();
			$image->setImage($diretorio . DS . $this->request->data['Imagem']['imagem']);
			$image->createThumb($diretorio . DS . 'big_' . $this->request->data['Imagem']['imagem'], $configuracao['Configuracao']['img_big_w'] , $configuracao['Configuracao']['img_big_h']);
			$image->createThumb($diretorio . DS . 'medium_' . $this->request->data['Imagem']['imagem'], $configuracao['Configuracao']['img_medium_w'] , $configuracao['Configuracao']['img_medium_h']);
			$image->createThumb($diretorio . DS . 'normal_' . $this->request->data['Imagem']['imagem'], $configuracao['Configuracao']['img_normal_w'] , $configuracao['Configuracao']['img_normal_h']);
			$image->createThumb($diretorio . DS . 'small_' . $this->request->data['Imagem']['imagem'], $configuracao['Configuracao']['img_small_w'] , $configuracao['Configuracao']['img_small_h']);
			$image->createThumb($diretorio . DS . 'thumb_' . $this->request->data['Imagem']['imagem'], $configuracao['Configuracao']['img_thumb_w'] , $configuracao['Configuracao']['img_thumb_h']);

			/* Fim Recorte e Resize */

			$this->request->data['Imagem'][$referente] = $referente_id;

			$qtd_imagens = $this->Imagem->find('count', array(
				'conditions' => array(
					'Imagem.ativo' => true,
					'Imagem.' . $referente => $referente_id
				),
			));

			if ($qtd_imagens == 0) {
				$this->request->data['Imagem']['destaque'] = true;
			}

			$this->request->data['Imagem']['ordem'] = $qtd_imagens + 1;

			$this->Imagem->create();
			if ($this->Imagem->save($this->request->data)) {
				$this->Session->setFlash('Imagem salva com sucesso.', FLASH_SUCCESS);

				// Mercado Livre
				if ($this->Configuracao->get('mercado_livre_ativo') && $referente == 'produto_id') {
					$this->MercadoLivreProdutos->editar_produto($referente_id);
				}
				// Fim - Mercado Livre

				$this->backToPaginatorIndex();
			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar salvar a imagem. Por favor, tente novamente.', FLASH_ERROR);
			}
		}

		$this->set('referente', $referente);

	}

	public function admin_editar($id = null) {

		$this->Imagem->id = $id;
		$this->request->data['Imagem']['id'] = $id;

		$configuracao = $this->Configuracao->find('first');

		$imagem = $this->Imagem->find('first', array(
			'contain' => array(
				'Produto',
				'Sku.Produto'
			),
			'conditions' => array(
				'Imagem.id' => $id
			)
		));

		if ($this->request->is('post') || $this->request->is('put')) {

			$nomeImagem = $imagem['Imagem']['imagem'];

			/* Imagem */

			$diretorio = $this->getDiretorioProdutos();
			$this->FileUpload->permitirSomenteImagens();

			if ($this->request->data['Imagem']['imagem']['name'] == '') {
				unset($this->request->data['Imagem']['imagem']);
			} else {
				$this->request->data['Imagem']['imagem']['name'] = $nomeImagem;
				$this->request->data['Imagem']['imagem'] = $this->FileUpload->upload($diretorio, array($this->request->data['Imagem']['imagem']), true);

				/* Recorte e Resize */

				$image = new CropImage();
				$image->setImage($diretorio . DS . $this->request->data['Imagem']['imagem']);
				$image->createThumb($diretorio . DS . 'big_' . $this->request->data['Imagem']['imagem'], $configuracao['Configuracao']['img_big_w'] , $configuracao['Configuracao']['img_big_h']);
				$image->createThumb($diretorio . DS . 'medium_' . $this->request->data['Imagem']['imagem'], $configuracao['Configuracao']['img_medium_w'] , $configuracao['Configuracao']['img_medium_h']);
				$image->createThumb($diretorio . DS . 'normal_' . $this->request->data['Imagem']['imagem'], $configuracao['Configuracao']['img_normal_w'] , $configuracao['Configuracao']['img_normal_h']);
				$image->createThumb($diretorio . DS . 'small_' . $this->request->data['Imagem']['imagem'], $configuracao['Configuracao']['img_small_w'] , $configuracao['Configuracao']['img_small_h']);
				$image->createThumb($diretorio . DS . 'thumb_' . $this->request->data['Imagem']['imagem'], $configuracao['Configuracao']['img_thumb_w'] , $configuracao['Configuracao']['img_thumb_h']);

				/* Fim Recorte e Resize */
			}

			/* Fim Imagem */

			if ($this->Imagem->save($this->request->data)) {
				$this->Session->setFlash('Imagem editada com sucesso.', FLASH_SUCCESS);

				// Mercado Livre
				if ($this->Configuracao->get('mercado_livre_ativo') && $imagem['Imagem']['produto_id'] != '') {
					$this->MercadoLivreProdutos->editar_produto($imagem['Imagem']['produto_id']);
				}
				// Fim - Mercado Livre

				$this->backToPaginatorIndex();
			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar salvar a Imagem. Por favor, tente novamente.', FLASH_ERROR);
			}
		} else {
			$this->request->data = $this->Imagem->read(null, $id);
		}

		$this->set('imagem', $imagem);

	}

	public function admin_excluir($id = null, $referente = null, $referente_id = null) {
		$this->Imagem->id = $id;

		if (!$this->Imagem->exists()) {
			throw new NotFoundException('Imagem inexistente.');
		}

		$imagem = $this->Imagem->find('first', array(
			'contain' => false,
			'conditions' => array(
				'Imagem.ativo' => true,
				'Imagem.id' => $id,

			)
		));

		if (!$imagem['Imagem']['destaque']) {
			if ($this->request->is('post')) {
				if ($this->Imagem->saveField('ativo', false, false)) {

					$imagens = $this->Imagem->find('all', array(
						'conditions' => array(
							'Imagem.ativo' => true,
							'Imagem.'. $referente => $referente_id
						),
						'order' => array(
							'Imagem.ordem' => 'ASC'
						)
					));

					$count_img = 1;
					foreach ($imagens as $key => $img_) {
						$this->request->data[$count_img]['Imagem']['id'] = $img_['Imagem']['id'];
						$this->request->data[$count_img]['Imagem']['ordem'] = $count_img;

						if ($count_img == 1) {
							$this->request->data[$count_img]['Imagem']['destaque'] = true;
						} else {
							$this->request->data[$count_img]['Imagem']['destaque'] = false;
						}

						$count_img++;

					}

					$this->Imagem->saveMany($this->request->data, array('deep' => true));

					$this->Session->setFlash('Imagem excluída com sucesso.', FLASH_SUCCESS);
					$this->backToPaginatorIndex();
				} else {
					$this->Session->setFlash('Ocorreu um erro ao tentar excluir a Imagem. Por favor, tente novamente.', FLASH_ERROR);
				}
			}
		} else {
			$this->Session->setFlash('Esta imagem não pode ser excluída pois é a imagem principal. Inverta a ordem para poder excluí-la.', FLASH_ERROR);
			$this->backToPaginatorIndex();
		}

	}

	private function getDiretorioProdutos() {
		$diretorio = $this->getDiretorioProjeto('produtos');
		new Folder($diretorio, true);

		return $diretorio;
	}

	public function admin_ajax_ordenar($referente_id = null) {

		$referente = $this->params['named']['referente'];

		$qtd_imagens = $this->Imagem->find('count', array(
			'conditions' => array(
				'Imagem.ativo' => true,
				'Imagem.' . $referente => $referente_id
			),
		));

		$count_img = 1;
		while ($count_img <= $qtd_imagens) {

			$this->request->data[$count_img]['Imagem']['id'] = $this->params['named']['ordem_' . $count_img];
			$this->request->data[$count_img]['Imagem']['ordem'] = $count_img;

			if ($count_img == 1) {
				$this->request->data[$count_img]['Imagem']['destaque'] = true;
			} else {
				$this->request->data[$count_img]['Imagem']['destaque'] = false;
			}

			$count_img++;
		}

		if ($this->Imagem->saveMany($this->request->data, array('deep' => true))) {
			$json = array('sucesso' => true);
		} else {
			$json = array('sucesso' => false, 'mensagem' => 'Ocorreu um erro ao tentar ordenar as imagens. Por favor, tente novamente.');
		}

		$this->renderJson($json);

	}
}
?>