<?php
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
App::uses('UploadHandler', 'Vendor/JqueryFileUpload');
App::uses('HZip', 'Vendor');
App::uses('AppController', 'Controller');

class CmsController extends AppController {

	public function admin_index() {
		$arvore = $this->varrerDiretorio(APP . 'View' . DS . 'Themed'. DS . ucfirst(PROJETO));
		$this->set('arvore', $arvore);
	}

	private function varrerDiretorio($diretorio) {
		$arvore = array('pastas' => array(), 'arquivos' => array());
		$folder = new Folder($diretorio);
		$dir = $folder->read();

		foreach ($dir[0] as $nome_dir) {
			$arvore['pastas'][$nome_dir] = $this->varrerDiretorio($diretorio . DS . $nome_dir);
		}

		foreach ($dir[1] as $nome_arq) {
			$arvore['arquivos'][$nome_arq] = $diretorio . DS . $nome_arq;
		}

		return $arvore;
	}

	public function admin_editor() {
		$this->layout = 'editor';

		if ($this->request->is('post') || $this->request->is('put')) {
			$nome_arquivo = $this->request->data('Cms.nome_arquivo');
			$caminho_arquivo = APP . 'View' . DS . 'Themed'. DS . ucfirst(PROJETO) . DS . $nome_arquivo;
			$arquivo = new File($caminho_arquivo);
			if (!$arquivo->exists()) {
				$this->Session->setFlash('Arquivo '.$caminho_arquivo.' não encontrado.', FLASH_ERROR);
			} else {
				$arquivo->write($this->request->data('Cms.conteudo_arquivo'));
			}
		} else {
			$nome_arquivo = $this->request->params['named']['arquivo'];
			$nome_arquivo = str_replace('.DS.', DS, $nome_arquivo);
			$caminho_arquivo = APP . 'View' . DS . 'Themed'. DS . ucfirst(PROJETO) . DS . $nome_arquivo;
			$arquivo = new File($caminho_arquivo);
		}


		if (!$arquivo->exists()) {
			$this->Session->setFlash('Arquivo '.$caminho_arquivo.' não encontrado.', FLASH_ERROR);
		} else {
			$this->set('title_for_layout', $nome_arquivo);
			$this->set('nome_arquivo', $nome_arquivo);
			$this->set('conteudo_arquivo', $arquivo->read());
			$this->request->data('Cms.nome_arquivo', $nome_arquivo);
		}
	}

	public function admin_ajax_upload() {
		$this->autoRender = false;

		$dir = DS;
		if (isset($this->request->params['named']['pasta']) && !empty($this->request->params['named']['pasta'])) {
			$dir .= str_replace('.DS.', DS, $this->request->params['named']['pasta']) . DS;
		}

		$options = array(
			'upload_dir' => APP . 'View' . DS . 'Themed'. DS . ucfirst(PROJETO) . $dir,
			'image_versions' => array(),
			'mkdir_mode' => '0777',
			'min_file_size' => 0
		);

		new UploadHandler($options);
	}

	public function admin_ajax_criar_arquivo() {
		$dir = DS;
		if (isset($this->request->params['named']['pasta']) && !empty($this->request->params['named']['pasta'])) {
			$dir .= str_replace('.DS.', DS, $this->request->params['named']['pasta']) . DS;
		}

		$novo_arquivo = APP . 'View' . DS . 'Themed'. DS . ucfirst(PROJETO) . $dir . $this->request->data('Cms.novo_arquivo');

		if (is_dir($novo_arquivo) || file_exists($novo_arquivo)) {
			$this->renderJson(array('sucesso' => false, 'mensagem' => 'O arquivo ou diretório '.$novo_arquivo.' já existe.'));
		} else {
			$file = new File($novo_arquivo, true, 0777);
			$this->renderJson(array('sucesso' => true, 'arquivo' => $this->request->data('Cms.novo_arquivo')));
		}
	}

	public function admin_ajax_criar_pasta() {
		$dir = DS;
		if (isset($this->request->params['named']['pasta']) && !empty($this->request->params['named']['pasta'])) {
			$dir .= str_replace('.DS.', DS, $this->request->params['named']['pasta']) . DS;
		}

		$nova_pasta = APP . 'View' . DS . 'Themed'. DS . ucfirst(PROJETO) . $dir . $this->request->data('Cms.nova_pasta');

		if (is_dir($nova_pasta) || file_exists($nova_pasta)) {
			$this->renderJson(array('sucesso' => false, 'mensagem' => 'O arquivo ou diretório '.$nova_pasta.' já existe.'));
		} else {
			$folder = new Folder();
			if ($folder->create($nova_pasta, 0777)) {
				$this->renderJson(array('sucesso' => true, 'pasta' => $this->request->data('Cms.nova_pasta')));
			} else {
				$this->renderJson(array('sucesso' => false, 'mensagem' => 'Ocorreu um erro ao criar a nova pasta.'));
			}
		}
	}

	public function admin_ajax_excluir_pasta() {
		$dir = DS;
		if (isset($this->request->params['named']['pasta']) && !empty($this->request->params['named']['pasta'])) {
			$dir .= str_replace('.DS.', DS, $this->request->params['named']['pasta']) . DS;
			$pasta = APP . 'View' . DS . 'Themed'. DS . ucfirst(PROJETO) . $dir;
			$folder = new Folder($pasta);

			if ($folder->delete()) {
				$this->renderJson(array('sucesso' => true, 'pasta' => $this->request->data('Cms.pasta')));
			} else {
				$this->renderJson(array('sucesso' => false, 'mensagem' => 'Ocorreu um erro ao a pasta.'));
			}
		} else {
			$this->renderJson(array('sucesso' => false, 'mensagem' => 'Você deve selecionar uma pasta para excluir.'));
		}

	}

	public function admin_ajax_excluir_arquivo() {
		if (isset($this->request->params['named']['arquivo']) && !empty($this->request->params['named']['arquivo'])) {
			$arquivo = str_replace('.DS.', DS, $this->request->params['named']['arquivo']);
			$arquivo = APP . 'View' . DS . 'Themed'. DS . ucfirst(PROJETO) . DS . $arquivo;
			$file = new File($arquivo, false);

			if ($file->delete()) {
				$this->renderJson(array('sucesso' => true, 'arquivo' => $arquivo));
			} else {
				$this->renderJson(array('sucesso' => false, 'mensagem' => 'Ocorreu um erro ao o arquivo.'));
			}
		} else {
			$this->renderJson(array('sucesso' => false, 'mensagem' => 'Você deve selecionar um arquivo para excluir.'));
		}

	}


	public function admin_backup() {
		$folder = new Folder();
		$folder->delete(WWW_ROOT . DS . 'files' . DS . PROJETO . DS . 'tmp' . DS . 'bkp');
		$folder->create(WWW_ROOT . DS . 'files' . DS . PROJETO . DS . 'tmp' . DS . 'bkp', 0777);

		$origem = APP . 'View' . DS . 'Themed'. DS . ucfirst(PROJETO);
		$arquivo_sem_extensao = PROJETO . '_' . date('YmdHis');
		$arquivo = $arquivo_sem_extensao . '.zip';
		$caminho_destino = WWW_ROOT . DS . 'files' . DS . PROJETO . DS . 'tmp' . DS . 'bkp' . DS;
		$destino = $caminho_destino . $arquivo;

		HZip::zipDir($origem, $destino);

		$this->viewClass = 'Media';
		
		$params = array(
			'id' => $arquivo,
			'name' => $arquivo_sem_extensao,
			'download' => true,
			'extension' => 'zip',
			'path' => $caminho_destino
		);
		$this->set($params);
	}

}
?>