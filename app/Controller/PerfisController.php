<?php
App::uses('AppController', 'Controller');

class PerfisController extends AppController {

	public function admin_index() {
		$this->paginate = array(
			'contain' => false,
			'conditions' => array(
				'Perfil.ativo' => true
			),
			'order' => 'descricao',
			'limit' => Configure::read('Sistema.Paginacao.limit')
		);
		$this->set('perfis', $this->paginate());
	}

	public function admin_adicionar() {
		if(($this->request->is('post') || $this->request->is('put')) && !empty($this->request->data)) {
			$this->Perfil->create();
			if ($this->Perfil->save($this->request->data)) {
				$this->Session->setFlash('Perfil salvo com sucesso.', FLASH_SUCCESS);
				$this->backToPaginatorIndex();
			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar salvar o perfil. Por favor, tente novamente.', FLASH_ERROR);
			}
		}
	}

	public function admin_editar($id = null) {
		$this->Perfil->id = $id;
	
		if (!$this->Perfil->exists()) {
			throw new NotFoundException('Perfil inexistente.');
		}

		if(($this->request->is('post') || $this->request->is('put')) && !empty($this->request->data)) {
			if ($this->Perfil->save($this->request->data)) {
				$this->Session->setFlash('Perfil salvo com sucesso.', FLASH_SUCCESS);
				$this->backToPaginatorIndex();
			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar salvar o perfil. Por favor, tente novamente.', FLASH_ERROR);
			}
		}

		$this->request->data = $this->Perfil->read(null, $id);
	}

	public function admin_excluir($id = null) {
		$this->Perfil->id = $id;
	
		if (!$this->Perfil->exists()) {
			throw new NotFoundException('Perfil inexistente.');
		}

		if($this->request->is('post') || $this->request->is('put')) {
			if ($this->Perfil->saveField('ativo', false, false)) {
				$this->Session->setFlash('Perfil excluído com sucesso.', FLASH_SUCCESS);
				$this->backToPaginatorIndex();
			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar excluir o perfil. Por favor, tente novamente.', FLASH_ERROR);
			}
		}
	}

	
}