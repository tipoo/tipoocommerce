<?php
App::uses('AppController', 'Controller');

class AviseMeController extends AppController {

	public $uses = array('AviseMe');

	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('enviar');
	}

	public function enviar($id = null) {

		if ($this->request->is('post')) {

			$this->AviseMe->create();
			if ($this->AviseMe->save($this->request->data)) {
				$this->Session->setFlash('Dados enviados com sucesso. Você será notificado quando o produto entrar em estoque.', FLASH_SUCCESS, array(), 'avise_me');
				$this->redirect($this->referer());
			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar enviar seus dados. Por favor, tente novamente.', FLASH_ERROR, array(), 'avise_me');
			}

		}
	}

}

?>