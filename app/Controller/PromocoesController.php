<?php
App::uses('AppController', 'Controller');

class PromocoesController extends AppController {

	public $uses = array('Promocao', 'Produto', 'Colecao','Categoria', 'Marca', 'FreteTransportadora', 'PromocoesFreteTransportadora', 'PromocaoCausa');

	private $elements_set = array('leve_x_pague_y', 'desconto_produto', 'desconto_pedido', 'desconto_frete', 'cupom_desconto');

	public function admin_index() {
		$promocoes = array();

		foreach ($this->elements_set as $element) {
			$promocoes[$element] = $this->Promocao->find('all', array(
				'conditions' => array(
					'Promocao.ativo',
					'Promocao.element' => $element,
					'Promocao.pedido_id' => '',
					'Promocao.pedido_sku_id' => ''
				),
				'order' => array(
					'Promocao.created' => 'DESC'
				)
			));
		}

		$this->set(compact('promocoes'));
	}

	public function admin_promocao($id = null) {
		if (!isset($this->request->params['named']['element']) && $id == '') {
			$this->Session->setFlash('Para acessar a página anterior é necessário escolher um tipo de promoção.', FLASH_ERROR);
			$this->backToPaginatorIndex();
		} else {

			$frete_transportadoras = $this->FreteTransportadora->find('all', array(
				'conditions' => array(
					'FreteTransportadora.loja' => true,
					'FreteTransportadora.ativo' => true
				),
				'fields' => array(
					'id',
					'descricao',
					'frete_tipo'
				),
			));

			$element = $this->request->params['named']['element'];
			$this->set(compact('element'));

			if($this->request->is('post') || $this->request->is('put')) {

				if (isset($this->request->data['PromocaoCausa']['cupom'])) {
					$verifica_cupom = $this->PromocaoCausa->find('first', array(
						'conditions' => array(
							'PromocaoCausa.cupom' => $this->request->data['PromocaoCausa']['cupom'],
						)
					));

					if (count($verifica_cupom)) {
						$this->Session->setFlash('O código de cupom "<strong>'.$this->request->data['PromocaoCausa']['cupom'].'</strong>" já se encontra em uso. Por favor cadastre um código diferente.', FLASH_ERROR);
						$this->redirect($this->referer());
					}
				}

				$data_inicio = explode('/', $this->request->data['Promocao']['data_inicio']);
				$data_inicio = $data_inicio[2] . '-' . $data_inicio[1] . '-' . $data_inicio[0] . " " . $this->request->data['Promocao']['hora_inicio'] ;

				$data_fim = explode('/', $this->request->data['Promocao']['data_fim']);
				$data_fim = $data_fim[2] . '-' . $data_fim[1] . '-' . $data_fim[0] . " " . $this->request->data['Promocao']['hora_fim'] ;

				$this->request->data['Promocao']['data_inicio'] =  date($data_inicio);
				$this->request->data['Promocao']['data_fim'] =  date($data_fim);

				if (isset($this->request->data['PromocaoCausa']['cep_inicio'])) {
					$cep_inicio = str_replace('-','',$this->request->data['PromocaoCausa']['cep_inicio']);
					$this->request->data['PromocaoCausa']['cep_inicio'] = $cep_inicio;
				} else {
					$this->request->data['PromocaoCausa']['cep_inicio'] = '';
				}

				if (isset($this->request->data['PromocaoCausa']['cep_fim'])) {
					$cep_fim = str_replace('-','',$this->request->data['PromocaoCausa']['cep_fim']);
					$this->request->data['PromocaoCausa']['cep_fim'] = $cep_fim;
				} else {
					$this->request->data['PromocaoCausa']['cep_fim'] = '';
				}

				if (isset($this->request->data['PromocaoCausa']['valor_acima_de'])) {

					if ($this->request->data['PromocaoEfeito']['valor'] == '0,00') {
						$this->request->data['PromocaoEfeito']['valor'] = '';
					}

					if ($this->request->data['PromocaoCausa']['valor_acima_de'] == '0,00') {
						$this->request->data['PromocaoCausa']['valor_acima_de'] = '';
					}

					$this->request->data['PromocaoCausa']['valor_acima_de'] = str_replace('.', '', $this->request->data['PromocaoCausa']['valor_acima_de']);
					$this->request->data['PromocaoCausa']['valor_acima_de'] = str_replace(',', '.', $this->request->data['PromocaoCausa']['valor_acima_de']);

				}

				if ($this->request->data['PromocaoCausa']['carrinho_valor_acima_de'] == '0,00') {
					$this->request->data['PromocaoCausa']['carrinho_valor_acima_de'] = '';
				}

				$this->request->data['PromocaoEfeito']['valor'] = str_replace('.', '', $this->request->data['PromocaoEfeito']['valor']);
				$this->request->data['PromocaoEfeito']['valor'] = str_replace(',', '.', $this->request->data['PromocaoEfeito']['valor']);

				$this->request->data['PromocaoCausa']['carrinho_valor_acima_de'] = str_replace('.', '', $this->request->data['PromocaoCausa']['carrinho_valor_acima_de']);
				$this->request->data['PromocaoCausa']['carrinho_valor_acima_de'] = str_replace(',', '.', $this->request->data['PromocaoCausa']['carrinho_valor_acima_de']);

				if ($id != '') {
					/* Editar */
					$this->request->data['Promocao']['id'] = $id;

					$promocao = $this->Promocao->find('first', array(
						'contain' => array(
							'PromocaoEfeito',
							'PromocaoCausa',
						),
						'conditions' => array(
							'Promocao.id' => $id
						)
					));

					$this->request->data['PromocaoEfeito']['id'] = $promocao['PromocaoEfeito']['id'];
					$this->request->data['PromocaoCausa']['id'] = $promocao['PromocaoCausa']['id'];
				} else {
					/* Adicionar */
					$this->Promocao->create();
				}

				if ($this->Promocao->saveAll($this->request->data)) {

					$this->PromocoesFreteTransportadora->deleteAll(array('PromocoesFreteTransportadora.promocao_id' => $this->Promocao->id));

					if (isset($this->request->data['PromocoesFreteTransportadora']['frete_transportadora_id'])) {

						foreach ($this->request->data['PromocoesFreteTransportadora']['frete_transportadora_id'] as $frete_transportadora_id) {

							$this->PromocoesFreteTransportadora->create();

							$this->request->data['PromocoesFreteTransportadora']['promocao_id'] = $this->Promocao->id;
							$this->request->data['PromocoesFreteTransportadora']['frete_transportadora_id'] = $frete_transportadora_id;

							$this->PromocoesFreteTransportadora->save($this->request->data['PromocoesFreteTransportadora']);

						}
					}

					$this->Session->setFlash('Promoção salva com sucesso.', FLASH_SUCCESS);
					$this->backToPaginatorIndex();
				} else {
					$this->Session->setFlash('Ocorreu um erro ao tentar salvar a Promoção. Por favor, tente novamente.', FLASH_ERROR);
				}


			}
			$this->set('frete_transportadoras', $frete_transportadoras);
			$this->set('id', $id);

		}

		if ($id != '') {

			$frete_transportadoras = $this->FreteTransportadora->find('all', array(
				'conditions' => array(
					'FreteTransportadora.loja' => true,
					'FreteTransportadora.ativo' => true
				),
				'fields' => array(
					'id',
					'descricao',
					'frete_tipo',
				),
			));
			/* Editar */

			$promocao_selecionada = $this->Promocao->find('first', array(
				'conditions' => array(
					'Promocao.ativo' => true,
					'Promocao.id' => $id,
				),

			));

			if($promocao_selecionada['PromocaoCausa']['produto_id'] != '' ) {

				$produto = $this->Produto->find('first', array(
					'contain' => false,
					'conditions' => array(
						'Produto.id' => $promocao_selecionada['PromocaoCausa']['produto_id']
					),
					'fields' => array(
						'descricao',
						'id'
					)
				));

				$this->set('produto_descricao', $produto);

			} else if($promocao_selecionada['PromocaoCausa']['colecao_id'] != '' ) {

					$colecao = $this->Colecao->find('first', array(
					'contain' => false,
					'conditions' => array(
						'Colecao.id' => $promocao_selecionada['PromocaoCausa']['colecao_id']
					),
					'fields' => array(
						'descricao',
						'id'
					)
				));

				$this->set('colecao_descricao', $colecao);

			} else if($promocao_selecionada['PromocaoCausa']['categoria_id'] != '' ) {

					$categoria = $this->Categoria->find('first', array(
						'contain' => false,
						'conditions' => array(
							'Categoria.id' => $promocao_selecionada['PromocaoCausa']['categoria_id']
						),
						'fields' => array(
							'descricao',
							'id'
						)
					));

					$this->set('categoria_descricao', $categoria);

			} else if($promocao_selecionada['PromocaoCausa']['marca_id'] != '' ) {

				$marca = $this->Marca->find('first', array(
					'contain' => false,
					'conditions' => array(
						'Marca.id' => $promocao_selecionada['PromocaoCausa']['marca_id']
					),
					'fields' => array(
						'descricao',
						'id'
					)
				));

				$this->set('marca_descricao', $marca);

			}

			$this->request->data = $this->Promocao->read(null, $id);
			$this->set('frete_transportadoras', $frete_transportadoras);
		}
	}

	public function admin_excluir($id = null) {
		$this->Promocao->id = $id;

		if (!$this->Promocao->exists()) {
			throw new NotFoundException('Promoção inexistente.');
		}

		$promocao = $this->Promocao->find('first', array(
			'contain' => array(
				'PromocaoEfeito',
				'PromocaoCausa'
			),
			'conditions' => array(
				'Promocao.id' => $id
			)
		));

		$data = array(
			'Promocao' => array('ativo' => false, 'id' => $id),
			'PromocaoEfeito' => array('ativo' => false , 'id' => $promocao['PromocaoEfeito']['id']),
			'PromocaoCausa' => array('ativo' => false, 'id' => $promocao['PromocaoCausa']['id'])
		);

		if($this->request->is('post')) {

			if ($this->Promocao->saveAll($data)) {
				$this->Session->setFlash('Promoção excluída com sucesso.', FLASH_SUCCESS);
				$this->backToPaginatorIndex();
			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar excluir a Promoção. Por favor, tente novamente.', FLASH_ERROR);
			}
		}
	}

	public function admin_ativar($id = null) {
		$this->Promocao->id = $id;

		if (!$this->Promocao->exists()) {
			throw new NotFoundException('Promoção inexistente.');
		}

		if ($this->request->is('post')) {
			if ($this->Promocao->saveField('situacao', 'A', false)) {
				$this->Session->setFlash('Promoção ativada com sucesso.', FLASH_SUCCESS);
				$this->backToPaginatorIndex();
			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar ativar a Promoção. Por favor, tente novamente.', FLASH_ERROR);
			}
		}
	}

	public function admin_desativar($id = null) {
		$this->Promocao->id = $id;

		if (!$this->Promocao->exists()) {
			throw new NotFoundException('Promoção inexistente.');
		}

		if ($this->request->is('post')) {
			if ($this->Promocao->saveField('situacao', 'I', false)) {
				$this->Session->setFlash('Promoção desativada com sucesso.', FLASH_SUCCESS);
				$this->backToPaginatorIndex();
			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar desativar a Promoção. Por favor, tente novamente.', FLASH_ERROR);
			}
		}
	}

	public function admin_ajax_visualizar( $id = null) {

		$promocao = $this->Promocao->find('first', array(
			'conditions' => array(
				'Promocao.ativo' => true,
				'Promocao.id' => $id,
			),

		));

		if($promocao['PromocaoCausa']['produto_id'] != '' ) {

			$produto = $this->Produto->find('first', array(
				'contain' => false,
				'conditions' => array(
					'Produto.id' => $promocao['PromocaoCausa']['produto_id']
				),
				'fields' => array(
					'descricao',
					'id'
				)
			));

			$this->set('produto', $produto);

		} else if($promocao['PromocaoCausa']['colecao_id'] != '' ) {

				$colecao = $this->Colecao->find('first', array(
				'contain' => false,
				'conditions' => array(
					'Colecao.id' => $promocao['PromocaoCausa']['colecao_id']
				),
				'fields' => array(
					'descricao',
					'id'
				)
			));

			$this->set('colecao', $colecao);

		} else if($promocao['PromocaoCausa']['categoria_id'] != '' ) {

				$categoria = $this->Categoria->find('first', array(
					'contain' => false,
					'conditions' => array(
						'Categoria.id' => $promocao['PromocaoCausa']['categoria_id']
					),
					'fields' => array(
						'descricao',
						'id'
					)
				));

				$this->set('categoria', $categoria);

		} else if($promocao['PromocaoCausa']['marca_id'] != '' ) {

			$marca = $this->Marca->find('first', array(
				'contain' => false,
				'conditions' => array(
					'Marca.id' => $promocao['PromocaoCausa']['marca_id']
				),
				'fields' => array(
					'descricao',
					'id'
				)
			));

			$this->set('marca', $marca);

		}

		$this->set('promocao', $promocao);

	}

	public function admin_ajax_buscar_produtos($descricao = null) {

		$produtos = $this->Produto->find('all', array(
			'contain' => false,
			'conditions' => array(
				'Produto.ativo',
				'Produto.descricao LIKE "%' . $descricao . '%"'
			)
		));

		$resultado = array();
		foreach ($produtos as $produto) {
			$resultado[] = array(
				'id' => $produto['Produto']['id'],
				'descricao' => $produto['Produto']['descricao'],
				'value' => $produto['Produto']['descricao'] . ' - #' . $produto['Produto']['id']
			);
		}

		$this->renderJson($resultado);
	}

	public function admin_ajax_buscar_colecoes($descricao = null) {

		$colecoes = $this->Colecao->find('all', array(
			'contain' => false,
			'conditions' => array(
				'Colecao.ativo',
				'Colecao.descricao LIKE "%' . $descricao . '%"'
			)
		));

		$resultado = array();
		foreach ($colecoes as $colecao) {
			$resultado[] = array(
				'id' => $colecao['Colecao']['id'],
				'descricao' => $colecao['Colecao']['descricao'],
				'value' => $colecao['Colecao']['descricao'] . ' - #' . $colecao['Colecao']['id']
			);
		}

		$this->renderJson($resultado);
	}

	public function admin_ajax_buscar_categorias($descricao = null) {

		$categorias = $this->Categoria->find('all', array(
			'contain' => false,
			'conditions' => array(
				'Categoria.ativo',
				'Categoria.descricao LIKE "%' . $descricao . '%"'
			)
		));

		$resultado = array();
		foreach ($categorias as $categoria) {
			$resultado[] = array(
				'id' => $categoria['Categoria']['id'],
				'descricao' => $categoria['Categoria']['descricao'],
				'value' => $categoria['Categoria']['descricao'] . ' - #' . $categoria['Categoria']['id']
			);
		}

		$this->renderJson($resultado);
	}

	public function admin_ajax_buscar_marcas($descricao = null) {

		$marcas = $this->Marca->find('all', array(
			'contain' => false,
			'conditions' => array(
				'Marca.ativo',
				'Marca.descricao LIKE "%' . $descricao . '%"'
			)
		));

		$resultado = array();
		foreach ($marcas as $marca) {
			$resultado[] = array(
				'id' => $marca['Marca']['id'],
				'descricao' => $marca['Marca']['descricao'],
				'value' => $marca['Marca']['descricao'] . ' - #' . $marca['Marca']['id']
			);
		}

		$this->renderJson($resultado);
	}

	public function admin_ajax_gerar_cupom() {

		$cupom_unico = false;

		do {

			$novo_cupom = $this->Random->gerarString();

			$verifica_cupom = $this->PromocaoCausa->find('first', array(
				'conditions' => array(
					'PromocaoCausa.cupom' => $novo_cupom
				)
			));

			if (!count($verifica_cupom)) {
				$cupom_unico = true;
			}

		} while(!$cupom_unico);

		if (isset($novo_cupom)) {

			$json = array('sucesso' => true, 'novo_cupom' => $novo_cupom);

		} else {
			$json = array('sucesso' => false, 'mensagem' => 'Ocorreu um erro ao gerar um cupom.');
		}

		$this->renderJson($json);

	}

}
?>