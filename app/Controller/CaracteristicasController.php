<?php
App::uses('AppController', 'Controller');

class CaracteristicasController extends AppController {

	public $uses = array('Caracteristica', 'CaracteristicasValoresSelecionado');

	public function admin_index($id = null) {

		$conditions = array(
			'Caracteristica.ativo' => true,
			'Caracteristica.categoria_id' => $id
		);

		$this->paginate = array(
			'conditions' => $conditions,
			'limit' => Configure::read('Sistema.Paginacao.limit')
		);

		$this->set('caracteristicas', $this->paginate());
		$this->set('tipo', $this->getTipo());
		$this->set('referente', $this->getReferente());
		$this->set('categoria_id', $id);

	}

	public function admin_adicionar($id = null) {

		if ($this->request->is('post')) {

			$this->request->data['Caracteristica']['categoria_id'] = $id;

			$this->Caracteristica->create();
			if ($this->Caracteristica->save($this->request->data)) {
				$this->Session->setFlash('Característica salva com sucesso.', FLASH_SUCCESS);
				$this->backToPaginatorIndex();
			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar salvar a Caracteristica. Por favor, tente novamente.', FLASH_ERROR);
			}
		}

		$this->set('referente', $this->getReferente());
		$this->set('tipo', $this->getTipo());
		$this->set('mascara', $this->getMascara());

	}

	public function admin_editar($id = null) {

		$caracteristica = $this->Caracteristica->find('first', array(
			'contain' => false,
			'conditions' => array(
				'Caracteristica.ativo' => true,
				'Caracteristica.id' => $id
			)
		));

		if ($caracteristica && $id) {
			if ($this->request->is('post') || $this->request->is('put')) {

				$this->Caracteristica->id = $id;
				if ($this->Caracteristica->save($this->request->data)) {
					$this->Session->setFlash('Característica salva com sucesso.', FLASH_SUCCESS);
					$this->backToPaginatorIndex();
				} else {
					$this->Session->setFlash('Ocorreu um erro ao tentar salvar a Caracteristica. Por favor, tente novamente.', FLASH_ERROR);
				}
			}

			$this->request->data = $this->Caracteristica->read(null, $id);

			$this->set('referente', $this->getReferente());
			$this->set('tipo', $this->getTipo());
			$this->set('mascara', $this->getMascara());
			$this->set('caracteristica', $caracteristica);

		} else {
			$this->Session->setFlash('Característica inexistente.', FLASH_ERROR);
			$this->backToPaginatorIndex();
		}

	}


	public function admin_excluir($id = null) {
		$this->Caracteristica->id = $id;

		if (!$this->Caracteristica->exists()) {
			throw new NotFoundException('Característica inexistente.');
		}

		if ($this->request->is('post')) {

			/* Verifica se o valor já foi utilizado para poder excluir ou não a categorias */
			$caracteristicasValoresSelecionados = $this->CaracteristicasValoresSelecionado->find('count', array(
				'conditions' => array(
					'CaracteristicasValoresSelecionado.ativo' => true,
					'CaracteristicasValoresSelecionado.caracteristica_id' => $id
				)
			));

			if ($caracteristicasValoresSelecionados == 0) {
				if ($this->Caracteristica->saveField('ativo', false, false)) {
					$this->Session->setFlash('Característica excluída com sucesso.', FLASH_SUCCESS);
					$this->backToPaginatorIndex();
				} else {
					$this->Session->setFlash('Ocorreu um erro ao tentar excluir a Característica. Por favor, tente novamente.', FLASH_ERROR);
				}
			} else {
				$this->Session->setFlash('Não é possível exluir esta característica, pois ela já se encontra em uso.', FLASH_ERROR);
				$this->backToPaginatorIndex();
			}
		}
	}


	private function getReferente() {
		return array(
			'P' => 'Produto',
			'S' => 'SKU'
		);
	}

	private function getTipo() {
		return array(
			'T' => 'Texto',
			'TG' => 'Texto Grande',
			'L' => 'Lista',
			'N' => 'Número',
		);
	}

	private function getMascara() {
		return array(
			'I' => 'Inteiro',
			'D' => 'Decimal',
		);
	}

}
?>