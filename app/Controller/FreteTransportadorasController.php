<?php
App::uses('AppController', 'Controller');

class FreteTransportadorasController  extends AppController {

	public function admin_index() {

		$conditions = array(
			'FreteTransportadora.ativo' => true,
		);

		$order = array(
			'FreteTransportadora.descricao' => 'ASC'
		);

		$this->paginate = array(
			'conditions' => $conditions,
			'order' => $order,
			'limit' => Configure::read('Sistema.Paginacao.limit')
		);
		$this->set('frete_transportadoras', $this->paginate());
		$this->set('tipos', $this->getFreteTipos());
		$this->set('lojas', $this->getFreteLojas());

	}

	public function admin_adicionar($id = null) {

		if ($this->request->is('post')) {
			if ($this->request->data['FreteTransportadora']['frete_tipo'] == 'C') {
				$this->request->data['FreteTransportadora']['loja'] = false;
				$this->request->data['FreteTransportadora']['mercado_livre'] = false;
			}

			$this->FreteTransportadora->create();

			if ($this->FreteTransportadora->save($this->request->data)) {
				$this->Session->setFlash('Frete de transportadora salvo com sucesso.', FLASH_SUCCESS);
				$this->backToPaginatorIndex();
			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar salvar o Frete de transportadora. Por favor, tente novamente.', FLASH_ERROR);
			}
		}
		$this->set('tipos', $this->getFreteTipos());

	}

	public function admin_editar($id = null) {
		$this->FreteTransportadora->id = $id;

		if (!$this->FreteTransportadora->exists()) {
			throw new NotFoundException('Frete de transportadora inexistente.');
		}
		if ($this->request->is('post') || $this->request->is('put')) {

			if ($this->request->data['FreteTransportadora']['frete_tipo'] == 'C') {
				$this->request->data['FreteTransportadora']['loja'] = false;
				$this->request->data['FreteTransportadora']['mercado_livre'] = false;
			}

			if ($this->FreteTransportadora->save($this->request->data)) {
				$this->Session->setFlash('Frete de transportadora editado com sucesso.', FLASH_SUCCESS);

				$transportadora = $this->FreteTransportadora->find('first', array(
					'contain' => array(
						'FreteValor' => array(
							'conditions' => array(
								'FreteValor.ativo' => true
							)
						)
					),
					'conditions'=> array(
						'FreteTransportadora.id' => $this->FreteTransportadora->id
					)
				));

				if ($this->Configuracao->get('mercado_livre_ativo') && $transportadora['FreteTransportadora']['mercado_livre'] && count($transportadora['FreteValor']) > 0) {
					$this->MercadoLivreFrete->atualizar_frete();
				}

				$this->backToPaginatorIndex();
			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar editar o Frete de transportadora. Por favor, tente novamente.', FLASH_ERROR);
			}
		} else {
			$this->request->data = $this->FreteTransportadora->read(null, $id);
		}
		$this->set('tipos', $this->getFreteTipos());
	}

	public function admin_excluir($id = null) {
		$this->FreteTransportadora->id = $id;

		$transportadora = $this->FreteTransportadora->find('first', array(
			'conditions'=> array(
				'FreteTransportadora.id' => $id
			)
		));

		if (!$this->FreteTransportadora->exists()) {
			throw new NotFoundException('Frete de transportadora inexistente.');
		}

		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->FreteTransportadora->saveField('ativo', false, false)) {
				$this->Session->setFlash('Frete de transportadora excluído com sucesso.', FLASH_SUCCESS);

				if ($this->Configuracao->get('mercado_livre_ativo') && $transportadora['FreteTransportadora']['mercado_livre']) {
					$this->MercadoLivreFrete->atualizar_frete();
				}

				$this->backToPaginatorIndex();
			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar excluir o Frete de transportadora. Por favor, tente novamente.', FLASH_ERROR);
			}
		}
	}

	private function getFreteTipos() {
		return array(
			'T' => 'Transportadora',
			'C' => 'Correio'
		);
	}

	private function getFreteLojas() {
		return array(
			'loja' => 'Loja Principal',
			'mercado_livre' => 'Mercado Livre'
		);
	}

}
?>