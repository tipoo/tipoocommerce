<?php
App::uses('AppController', 'Controller');

class ServicoAnexosController extends AppController {

	public $uses = array('ServicoAnexo', 'Servico');

	public function admin_index($servico_id = null) {
		$this->Servico->id = $servico_id;

		if (!$this->Servico->exists()) {
			throw new NotFoundException('Anexo inexistente.');
		}

		$conditions = array(
			'ServicoAnexo.ativo' => true,
			'ServicoAnexo.servico_id' => $servico_id,
		);

		$this->paginate = array(
			'conditions' => $conditions,
			'limit' => 50
		);

		$this->set('servico_anexos', $this->paginate());
		$this->set('tipos', $this->get_tipos());
		$this->set('servico_id', $servico_id);
	}

	public function admin_adicionar($servico_id = null) {
		if ($this->request->is('post')) {

			$this->request->data['ServicoAnexo']['servico_id'] = $servico_id;

			$this->ServicoAnexo->create();
			if ($this->ServicoAnexo->save($this->request->data)) {
				$this->Session->setFlash('Anexo salvo com sucesso.', FLASH_SUCCESS);
				$this->backToPaginatorIndex();
			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar salvar o anexo. Por favor, tente novamente.', FLASH_ERROR);
			}
		}

		$this->set('tipos', $this->get_tipos());
	}

	public function admin_editar($id = null) {
		$this->ServicoAnexo->id = $id;

		if (!$this->ServicoAnexo->exists()) {
			throw new NotFoundException('Anexo inexistente.');
		}

		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->ServicoAnexo->save($this->request->data)) {
				$this->Session->setFlash('Anxeo editado com sucesso.', FLASH_SUCCESS);
				$this->backToPaginatorIndex();
			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar editar o anexo. Por favor, tente novamente.', FLASH_ERROR);
			}
		} else {
			$this->request->data = $this->ServicoAnexo->read(null, $id);
			$this->set('tipos', $this->get_tipos());
		}
	}


	public function admin_excluir($id = null) {
		$this->ServicoAnexo->id = $id;

		if (!$this->ServicoAnexo->exists()) {
			throw new NotFoundException('Anexo inexistente.');
		}

		if ($this->request->is('post') || $this->request->is('put')) {

			if ($this->ServicoAnexo->saveField('ativo', false, false)) {
				$this->Session->setFlash('Anexo excluído com sucesso.', FLASH_SUCCESS);
				$this->backToPaginatorIndex();
			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar excluir o anexo. Por favor, tente novamente.', FLASH_ERROR);
			}

		}

	}

	private function get_tipos() {
		return array(
			'T' => 'Texto simples',
			// 'A' => 'Área de texto',
			'S' => 'Select (seleção única)',
			// 'C' => 'Checkbox (múltipla escolha)',
		);
	}
}
?>