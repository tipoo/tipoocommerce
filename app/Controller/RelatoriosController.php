<?php
App::uses('AppController', 'Controller');
class RelatoriosController extends AppController {

	public $uses = array('Pedido', 'StatusPedido', 'Sku', 'Produto', 'PedidoSku', 'AviseMe', 'TermosBuscado', 'Avaliacao');

	public function admin_relatorio_vendas($id = null) {

		$de = date('Y-m-d', strtotime('-31 day'));
		if (isset($this->request->params['named']['de'])) {
			$de = $this->request->params['named']['de'];
		}

		$ate = date('Y-m-d');
		if (isset($this->request->params['named']['ate'])) {
			$ate = $this->request->params['named']['ate'];
		}

		$conditions = array(
			'Pedido.data_hora >=' => $de . ' 00:00:00',
			'Pedido.data_hora <=' => $ate . ' 23:59:59',
			'StatusPedido.venda_concluida' => true
		);

		if (!empty($this->request->params['named']['statusPedidoFiltroId'])) {
			$conditions['Pedido.status_id'] = $this->request->params['named']['statusPedidoFiltroId'];
			$this->request->data('statusPedidoFiltroId', $this->request->params['named']['statusPedidoFiltroId']);
		}

		$vendas = $this->Pedido->find('all', array(
			'contain' => array(
				'Cliente.nome',
				'Endereco' => array(
					'Cidade' => array('nome', 'uf'),
					'fields' => array('id')
				),
				'StatusPedido',
				'PedidoSku'
			),
			'conditions' => $conditions,
			'order' => array(
				'Pedido.data_hora' => 'DESC'
			)
		));

		$qtd_vendas = 0;
		//$qtd_produtos_vendidos = 0;
		$qtd_skus_vendidos = 0;

		$receita_bruta = 0; //com frete
		$valor_total_vendas_liquido = 0;

		$qtd_frete = 0;
		$total_valor_frete = 0;

		$ticket_medio = 0;

		foreach ($vendas as $key => $venda) {
			$receita_bruta += $venda['Pedido']['valor_total'];
			$valor_total_vendas_liquido += $venda['Pedido']['valor_produtos'];
			$qtd_skus_vendidos += count($venda['PedidoSku']);
			if ($venda['Pedido']['valor_frete'] != '0.00') {
				$total_valor_frete += $venda['Pedido']['valor_frete'];
				$qtd_frete++;
			}
		}

		$qtd_vendas = count($vendas);

		if ($qtd_vendas > 0) {
			$ticket_medio = ($receita_bruta / $qtd_vendas);
		}

		$this->set('qtd_vendas', $qtd_vendas);
		$this->set('qtd_skus_vendidos', $qtd_skus_vendidos);
		$this->set('receita_bruta', $receita_bruta);
		$this->set('valor_total_vendas_liquido', $valor_total_vendas_liquido);
		$this->set('qtd_frete', $qtd_frete);
		$this->set('total_valor_frete', $total_valor_frete);
		$this->set('ticket_medio', $ticket_medio);

		$this->set('vendas', $vendas);
		$this->set('de', $de);
		$this->set('ate', $ate);
	}

	public function admin_estoque() {

		$sku = null;
		if (isset($this->request->params['named']['sku'])) {
			$sku = $this->request->params['named']['sku'];
			$this->request->data['Filtro']['sku'] = $sku;
		}

		$produto = null;
		if (isset($this->request->params['named']['produto'])) {
			$produto = $this->request->params['named']['produto'];
			$this->request->data['Filtro']['produto'] = $produto;
		}

		$order = array(
			'Produto.descricao' => 'ASC',
			'Sku.descricao' => 'ASC',
			'Sku.created' => 'DESC'
		);

		$conditions = array(
			'Sku.ativo' => true,
		);

		if($sku != '') {
			$conditions[] = 'Sku.sku LIKE "%' . $sku . '%"';
		}

		if($produto != '') {
			$conditions[] = 'Produto.descricao LIKE "%' . $produto . '%"';
		}

		$skus = $this->Sku->find('all', array(
			'contain' => array('Produto'),
			'conditions' => $conditions,
			'order' => $order,
		));

		$pedidos_skus = $this->PedidoSku->find('all');


		$this->set('skus', $skus);
		$this->set('pedidos_skus', $pedidos_skus);

	}

	public function admin_baixar_relatorio_estoque() {
		$this->layout = 'csv';

		$filename = 'estoque.csv';

		header('Content-type: application/csv');
		header('Content-Disposition: attachment; filename="'.$filename.'"');

		$csv_file = fopen('php://output', 'w');

		$sku = null;
		if (isset($this->request->params['named']['sku'])) {
			$sku = $this->request->params['named']['sku'];
			$this->request->data['Filtro']['sku'] = $sku;
		}

		$produto = null;
		if (isset($this->request->params['named']['produto'])) {
			$produto = $this->request->params['named']['produto'];
			$this->request->data['Filtro']['produto'] = $produto;
		}

		$conditions = array(
			'Sku.ativo' => true,
		);

		if($sku != '') {
			$conditions[] = 'Sku.sku LIKE "%' . $sku . '%"';
		}

		if($produto != '') {
			$conditions[] = 'Produto.descricao LIKE "%' . $produto . '%"';
		}

		$order = array(
			'Produto.descricao' => 'ASC',
			'Sku.descricao' => 'ASC',
			'Sku.created' => 'DESC'
		);

		$skus = $this->Sku->find('all', array(
			'contain' => array('Produto'),
			'conditions' => $conditions,
			'order' => $order,
		));

		$pedidos_skus = $this->PedidoSku->find('all');

		$header_row = array("#" , "sku", "descricao sku", "produto", "disponiveis", "reservados", "em pre-venda", "entregues");
	    fputcsv($csv_file,$header_row,';','"');

		foreach($skus as $sku) {

			$nome_produto = null;
			$qtd_pedido = 0;
			$qtd_pre_venda = 0;
			$qtd_reservas = 0 ;

			foreach ($pedidos_skus as $pedido) {
				if ($pedido['PedidoSku']['sku_id'] == $sku['Sku']['id']) {

					if ($pedido['PedidoSku']['situacao_estoque'] == 'Pedido') {
						$qtd_pedido += $pedido['PedidoSku']['qtd'];
					}

					if ($pedido['PedidoSku']['situacao_estoque'] == 'PreVenda') {
						$qtd_pre_venda += $pedido['PedidoSku']['qtd'];
					}

					if ($pedido['PedidoSku']['situacao_estoque'] == 'Reserva') {
						$qtd_reservas += $pedido['PedidoSku']['qtd'];
					}
				}
			}
			$row = array(
				$sku['Sku']['id'],
				$sku['Sku']['sku'],
				$sku['Sku']['descricao'],
				$sku['Produto']['descricao'],
				$sku['Sku']['qtd_estoque'],
				$qtd_reservas,
				$qtd_pre_venda,
				$qtd_pedido,
			);

			fputcsv($csv_file,$row,';', '"');
		}

		fclose($csv_file);
	}

	public function admin_relatorio_avise_me() {

		$de = date('Y-m-d', strtotime('-31 day'));
		if (isset($this->request->params['named']['de'])) {
			$de = $this->request->params['named']['de'];
		}

		$ate = date('Y-m-d');
		if (isset($this->request->params['named']['ate'])) {
			$ate = $this->request->params['named']['ate'];
		}

		$conditions = array(
			'AviseMe.ativo' => true,
			'AviseMe.created >=' => $de . ' 00:00:00',
			'AviseMe.created <=' => $ate . ' 23:59:59',
		);

		$pedidos_grupo = $this->AviseMe->find('all', array(
		 	'contain' => array(
		 		'Sku.Produto'
	 		),
		 	'conditions' => $conditions,
 			'group' => array(
				'Sku.id',
			),
			'order' => array(
				'COUNT(AviseMe.sku_id)' => 'DESC'
			)
 		));

		$pedidos_aviso = $this->AviseMe->find('all', array(
		 	'contain' => array(
		 		'Sku.Produto'
	 		),
		 	'conditions' => $conditions,
		 	'order' => 'nome'
 		));

		$this->set('pedidos_aviso', $pedidos_aviso);
		$this->set('pedidos_grupo', $pedidos_grupo);
		$this->set('de', $de);
		$this->set('ate', $ate);
	}

	public function admin_relatorio_termos_buscados() {

		$de = date('Y-m-d', strtotime('-31 day'));
		if (isset($this->request->params['named']['de'])) {
			$de = $this->request->params['named']['de'];
		}

		$ate = date('Y-m-d');
		if (isset($this->request->params['named']['ate'])) {
			$ate = $this->request->params['named']['ate'];
		}

		$conditions = array(
			'TermosBuscado.created >=' => $de . ' 00:00:00',
			'TermosBuscado.created <=' => $ate . ' 23:59:59',
		);

		$termos_buscados = $this->TermosBuscado->find('all', array(
		 	'conditions' => $conditions,
		 	 'group' => 'TermosBuscado.termo',
		 	 'fields' => array('id', 'termo', 'COUNT(TermosBuscado.termo) as qtd_termos'),
		 	 'order' => array(
		 	 	'qtd_termos' => 'DESC'
	 	 	)
 		));

		$this->set('termos_buscados', $termos_buscados);
		$this->set('de', $de);
		$this->set('ate', $ate);
	}

	public function admin_visualizar_avise_me($id = null) {

		$this->layout = 'clean';

		$de = $this->request->params['named']['de'];
		$ate = $this->request->params['named']['ate'];
	
		$conditions = array(
			'AviseMe.ativo' => true,
			'AviseMe.sku_id' => $id,
			'AviseMe.created >=' => $de . ' 00:00:00',
			'AviseMe.created <=' => $ate . ' 23:59:59',
		);

		$pedidos_avise_me = $this->AviseMe->find('all', array(
		 	'contain' => array(
		 		'Sku.Produto'
	 		),
		 	'conditions' => $conditions,
		 	'order' => array(
		 		'AviseMe.created' => 'DESC'
	 		)
 		));

		$this->set('pedidos_avise_me', $pedidos_avise_me);

	}

	public function admin_baixar_avise_me() {

		$this->layout = 'csv';

		$filename = 'relatorio_avise_me.csv';

		header('Content-type: application/csv');
		header('Content-Disposition: attachment; filename="'.$filename.'"');

		$csv_file = fopen('php://output', 'w');

		$conditions = array(
			'AviseMe.ativo' => true,
		);

		$de = '';
		if (isset($this->params['named']['de'])) {
			$de = $this->params['named']['de'];
			$conditions['AviseMe.created >='] = $de;
		}

		$ate = '';
		if (isset($this->params['named']['ate'])) {
			$ate = $this->params['named']['ate'];
			$conditions['AviseMe.created <='] = $ate;
		}

		$pedidos_avise_me = $this->AviseMe->find('all', array(
		 	'contain' => array(
		 		'Sku.Produto'
	 		),
		 	'conditions' => $conditions,
		 	'order' => array(
		 		'AviseMe.created' => 'DESC'
	 		)
 		));

		$header_row = array("produto", "sku", "email", "data de cadastro");
	    fputcsv($csv_file,$header_row,';','"');

		foreach($pedidos_avise_me as $pedido_avise_me) {

			$row = array(
				$pedido_avise_me['Sku']['Produto']['descricao'],
				$pedido_avise_me['Sku']['sku'],
				$pedido_avise_me['AviseMe']['email'],
				$pedido_avise_me['AviseMe']['created']
			);

			fputcsv($csv_file, $row, ';', '"');
		}

		fclose($csv_file);

	}

	public function admin_relatorio_avaliacoes() {

		$de = date('Y-m-d', strtotime('-31 day'));
		if (isset($this->request->params['named']['de'])) {
			$de = $this->request->params['named']['de'];
		}

		$ate = date('Y-m-d');
		if (isset($this->request->params['named']['ate'])) {
			$ate = $this->request->params['named']['ate'];
		}

		$conditions = array(
			'Avaliacao.ativo' => true,
			'Avaliacao.created >=' => $de . ' 00:00:00',
			'Avaliacao.created <=' => $ate . ' 23:59:59',
		);

		$avaliacoes = $this->Avaliacao->find('all', array(
		 	'conditions' => $conditions,
 			'group' => array(
				'Avaliacao.produto_id',
			),
			'order' => array(
				'COUNT(Avaliacao.produto_id)' => 'DESC'
			)
 		));

		$total_avaliacoes = $this->Avaliacao->find('all', array(
		 	'conditions' => $conditions,
		 	'order' => 'nome'
 		));

		$this->set('total_avaliacoes', $total_avaliacoes);
		$this->set('avaliacoes', $avaliacoes);
		$this->set('de', $de);
		$this->set('ate', $ate);
	}

	public function admin_visualizar_avaliacao($id = null) {

		$this->layout = 'clean';

		$de = $this->request->params['named']['de'];
		$ate = $this->request->params['named']['ate'];
	
		$conditions = array(
			'Avaliacao.ativo' => true,
			'Avaliacao.produto_id' => $id,
			'Avaliacao.created >=' => $de . ' 00:00:00',
			'Avaliacao.created <=' => $ate . ' 23:59:59',
		);

		$avaliacoes = $this->Avaliacao->find('all', array(
		 	'conditions' => $conditions,
		 	'order' => array(
		 		'Avaliacao.created' => 'DESC'
	 		)
 		));

		$this->set('avaliacoes', $avaliacoes);

	}


	private function getEstoque() {
		return array(
			'sku' => 'Sku',
			'disponiveis' => 'Disponiveis',
			'pedidos' => 'Pedidos',
			'pre_venda' => 'Pré-vendas',
			'reservas' => 'Reservas'
		);
	}

}

?>