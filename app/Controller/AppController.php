<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

	public $components = array(
		'Auth',
		'Session',
		'Email',
		'Permissoes',
		'MenuCategorias',
		'Redirect',
		'Banners',
		'FileUpload',
		'CustomTagsReader',
		'Slugger',
		'Colecoes',
		'Random',
		'RegrasProdutos',
		'RegrasStatus',
		'Bling',
		'FreteTransportadoras',
		'FreteCorreios',
		'Promocoes',
		'ItemMenus', //trocamos o nome de "Menu" para "ItemMenu" pois apresentava erro em classe interna do cakephp
		'Lojas',
		'ClearSale',
		'MercadoLivreAuth',
		'MercadoLivreCategorias',
		'MercadoLivreProdutos',
		'MercadoLivreFrete',
		'MercadoLivrePedidos',
		'MercadoLivreStatus',
		'MercadoLivreClientes',
		'MercadoLivreEnderecos',
		'MercadoLivreFeedback',
		'MercadoLivreLogs'
	);

	public $helpersAdmin = array(
		'Form' => array('className' => 'TipooTBForm'),
		'Paginator' => array('className' => 'TipooPaginator'),
		'Html',
		'Text',
		'Session',
		'Permissoes',
		'CakePtbr.Formatacao',
		'Cms',
		'CustomTime'
	);

	public $helpersSite = array(
		'Form',
		'Paginator' => array('className' => 'TipooPaginator'),
		'Html',
		'Text',
		'Session',
		'Permissoes',
		'CakePtbr.Formatacao',
		'Controles',
		'Parcelas',
		'Configuracoes',
		'Carrinho',
		'CustomTime',
		'VitrineProdutos',
		'Banners'
	);

	public $uses = array('Configuracao');

	public function beforeFilter() {
		parent::beforeFilter();

		// Recuperando as utm's
		$utm_keys = array('utm_source', 'utm_medium', 'utm_campaign');
		foreach ($utm_keys as $utm) {
			if (!empty($this->request->query[$utm])) {
				$this->Session->write('Promocao.'.$utm, $this->request->query[$utm]);
			}
		}

		// Difinindo quais helpers serão disponibilizados para as views
		if ($this->params['prefix'] == 'admin') {
			$this->helpers = $this->helpersAdmin;
		} else {
			$this->helpers = $this->helpersSite;
		}

		// Salvando os dados de paginação da index para voltar e dando permissão para o método
		$this->Auth->allow('backToPaginatorIndex', 'admin_backToPaginatorIndex');
		if ($this->request->params['action'] == 'index' || $this->request->params['action'] == 'admin_index') {
			$this->setPaginatorUrlIndex();
		}

		// Carregando arquivos de configuração
		Configure::load('sistema');
		Configure::load('loja');

		// Verificando a requisição é para uma url com prefixo e definindo o layout e o tema
		if ($this->params['prefix'] == 'admin') {
			$this->layout = 'admin';
			if (AMBIENTE == AMBIENTE_DEV) {
				$this->set('title_for_layout', Configure::read('Loja.nome').' - Admin - '.AMBIENTE);
			} else {
				$this->set('title_for_layout', Configure::read('Loja.nome').' - Admin');
			}

		} else if ($this->params['prefix'] == 'sistema') {
			$this->layout = 'sistema';

		} else {
			$this->theme = PROJETO;
			$this->layout = 'catalogo';
			$this->set('title_for_layout', Configure::read('Loja.nome'));
		}

		// Caso seja uma requisição ajax, altera o layout novamente
		if ($this->request->is('ajax')) {
			$this->layout = 'ajax';
		}

		// Definindo as configurações de autenticação para o ambiente adminstrativo e para o site
		if ($this->params['prefix'] == 'admin') {
			AuthComponent::$sessionKey = 'Auth.User'; // default
			$this->Auth->loginAction = array('controller' => 'usuarios', 'action' => 'admin_login');
			$this->Auth->loginRedirect = array('controller' => 'paginas', 'action' => 'admin_dashboard');
			$this->Auth->logoutRedirect = array('controller' => 'usuarios', 'action' => 'admin_login');
			$this->Auth->authenticate = array(
				'Form' => array(
					'userModel' => 'Usuario',
					'fields' => array(
						'username' => 'email',
						'password' => 'senha'
					),
					'scope' => array(
						'Usuario.ativo' => 1
					)
				)
			);
			$this->Auth->authorize = array('Controller');
			$this->Auth->flash = array('element' => FLASH_ERROR, 'key' => 'flash', 'params' => array());

		} else if ($this->params['prefix'] == 'sistema') {
			//AuthComponent::$sessionKey = 'Auth.sistema'; // modificada
			$this->Auth->loginAction = array('controller' => 'usuarios', 'action' => 'sistema_login');
			$this->Auth->loginRedirect = array('controller' => 'agendador', 'action' => 'sistema_index');
			$this->Auth->logoutRedirect = array('controller' => 'usuarios', 'action' => 'sistema_login');
			$this->Auth->authenticate = array(
				'Basic' => array(
					'userModel' => 'Usuario',
					'fields' => array(
						'username' => 'email',
						'password' => 'senha'
					),
					'scope' => array(
						'Usuario.ativo' => 1
					)
				)
			);

		} else if ($this->params['prefix'] == 'api') {
			AuthComponent::$sessionKey = 'Auth.api'; // modificada
			$this->Auth->authenticate = array(
				'Api'
			);

		} else {
			AuthComponent::$sessionKey = 'Auth.Client'; // modificada
			$this->Auth->loginAction = array('action' => 'login');
			$this->Auth->loginRedirect = array('controller' => 'clientes', 'action' => 'painel');
			$this->Auth->logoutRedirect = array('controller' => 'catalogo', 'action' => 'index');

			$this->Auth->authenticate = array(
				'Form' => array(
					'userModel' => 'Cliente',
					'fields' => array(
						'username' => 'email',
						'password' => 'senha'
					),
					'scope' => array(
						'Cliente.ativo' => 1
					)
				)
			);
			$this->Auth->authorize = array('Controller');
			$this->Auth->flash = array('element' => FLASH_ERROR, 'key' => 'flash', 'params' => array());

			// Disponibilizando as metatags padrão para as views.
			// Não deve ser usado o helper de configuração pois estes valores podem ser sobrepostos por outros controllers.
			$meta_description = $this->Configuracao->get('meta_description');
			$meta_keywords = $this->Configuracao->get('meta_keywords');
			$this->set(compact('meta_description', 'meta_keywords'));
		}

		// Disponibilizando as configurações mantida no banco de dados para as views
		$this->set('db_config', $this->Configuracao->find('first'));

		// Armazenando informacoes de redirecionamento
		if (!$this->params['prefix']) {
			$this->Redirect->captureRedirectUrl();
		}

		// Configurações de autenticação
		$this->setUsuarioAutenticado();
		$this->setPermissoesSistema();

	}

	public function beforeRender() {
		if ($this->name == 'CakeError') {
			$this->layout = 'default';
		}
	}

	public function isAuthorized($usuario) {
		$autorizado = $this->Permissoes->check($usuario['perfil_id'], $this->request->params['controller'], $this->request->params['action'], $this->request->params['plugin']);
		if (!$autorizado) {
			$this->Auth->logout();
		}
		return $autorizado;
	}

	private function setPermissoesSistema() {
		if (isset($this->usuarioAutenticado) && isset($this->usuarioAutenticado['perfil_id'])) {
			if (!$this->Session->check('Permissoes.'.$this->usuarioAutenticado['perfil_id'])) {
				$dados = $this->Permissao->find('all', array(
					'conditions' => array('perfil_id' => $this->usuarioAutenticado['perfil_id']),
					'recursive' => '0'
				));

				$permissoes = array();
				foreach ($dados as $permissao) {
					$o = $permissao['Objeto'];
					$permissoes[$o['plugin']][$o['controller']][$o['action']] = true;
				}

				$this->Session->write('Permissoes.'.$this->usuarioAutenticado['perfil_id'], $permissoes);
			}
			$this->set('permissoesSistema', $this->Session->read('Permissoes.'.$this->usuarioAutenticado['perfil_id']));
		}
	}

	private function setUsuarioAutenticado() {
		$this->usuarioAutenticado = $this->Auth->user();
		$this->set('usuarioAutenticado', $this->usuarioAutenticado);
	}

	// Métodos usados para manter o estado da paginação nas actions index
	protected function setPaginatorUrlIndex() {
		$sessionKey = Inflector::camelize($this->request->params['plugin']).Inflector::camelize($this->request->params['controller']).'PaginatorUrlIndex';
		$this->Session->write($sessionKey, $this->request->params);
	}

	protected function getPaginatorUrlIndex($controller = null, $plugin = null) {
		if (!$controller) {
			$controller = $this->request->params['controller'];
		}

		if (!$plugin) {
			$plugin = $this->request->params['plugin'];
		}

		$sessionKey = Inflector::camelize($plugin).Inflector::camelize($controller).'PaginatorUrlIndex';
		if ($this->Session->check($sessionKey)) {
			$params = $this->Session->read($sessionKey);

			$url = array(
				'plugin' => $params['plugin'],
				'controller' => $params['controller'],

			);

			if ($params['admin']) {
				$url['action'] = str_replace('admin_', '', $params['action']);
				$url['admin'] = true;
			} else {
				$url['action'] = $params['action'];
				$url['admin'] = false;
			}

			foreach ($params['named'] as $key => $value) {
				$url[$key] = $value;
			}

			foreach ($params['pass'] as $value) {
				$url[] = $value;
			}

			return $url;
		}

		return array('action' => 'index');
	}

	protected function getDiretorioProjeto($subdiretorio) {
		$diretorio = APP . 'webroot' . DS . 'files' . DS . PROJETO . DS . $subdiretorio;
		return $diretorio;
	}

	public function backToPaginatorIndex($controller = null, $plugin = null) {
		$this->redirect($this->getPaginatorUrlIndex($controller, $plugin));
	}

	public function admin_backToPaginatorIndex($controller = null, $plugin = null) {
		$this->redirect($this->getPaginatorUrlIndex($controller, $plugin));
	}

	protected function renderJson($json) {
		$this->autoRender = false;
		$output = fopen('php://output', 'w');
		fputs($output, json_encode($json));
		fclose($output);
	}

	protected function isPrefix($prefix) {
		return isset($this->request->params['prefix']) && $this->request->params['prefix'] == $prefix;
	}

}
