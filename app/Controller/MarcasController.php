<?php
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
App::uses('AppController', 'Controller');

class MarcasController extends AppController {

	public $uses = array('Marca', 'Produto', 'Slug');

	public function admin_index() {

		$conditions = array(
			//'Marca.ativo' => true,
		);

		$this->paginate = array(
			'conditions' => $conditions,
			'order' => 'descricao',
			'limit' => 30
		);
		$this->set('marcas', $this->paginate());

	}

	public function admin_adicionar() {

		if ($this->request->is('post')) {

			/* Imagem */

			$diretorio = $this->getDiretorioMarcas();
			$this->FileUpload->permitirSomenteImagens();

			if ($this->request->data['Marca']['imagem']['name'] == '') {
				unset($this->request->data['Marca']['imagem']);
			} else {
				$path_parts = pathinfo($this->request->data['Marca']['imagem']['name']);

				$nomeImagem = strtolower($this->request->data['Marca']['descricao']);
				$this->request->data['Marca']['imagem']['name'] = Inflector::slug($nomeImagem, '_') . '.' . $path_parts['extension'];
				$this->request->data['Marca']['imagem'] = $this->FileUpload->upload($diretorio, array($this->request->data['Marca']['imagem']), true);
			}

			/* Fim Imagem */

			/* Slug */

			$count_slug = 0;
			do {
				if ($count_slug == 0) {
					$slug = '/' . Inflector::slug($this->request->data['Marca']['descricao'], '-');
				} else {
					$slug = '/' . Inflector::slug($this->request->data['Marca']['descricao'], '-') . '-' . $count_slug;
				}
				$slug_find = $this->Slug->find('first', array(
					'contain' => false,
					'conditions' => array(
						'Slug.url' => $slug
					)
				));

				$count_slug++;

			} while ($slug_find);

			$this->request->data['Slug']['url'] = strtolower($slug);

			/* Slug - Fim */

			$this->request->data['Slug']['controller'] = 'catalogo';
			$this->request->data['Slug']['action'] = 'marca';
			$this->request->data['Slug']['custom_parser_class'] = 'MarcaParser';

			$this->Marca->create();
			if ($this->Marca->saveAll($this->request->data)) {
				$this->Session->setFlash('Marca salva com sucesso.', FLASH_SUCCESS);
				$this->backToPaginatorIndex();
			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar salvar a Marca. Por favor, tente novamente.', FLASH_ERROR);
			}
		}
	}

	public function admin_editar($id = null) {
		$this->Marca->id = $id;

		$marca = $this->Marca->find('first', array(
			'contain' => array(
				'Slug'
			),
			'conditions' => array(
				'Marca.id' => $id
			)
		));

		if (!$this->Marca->exists()) {
			throw new NotFoundException('Marca inexistente.');
		}
		if ($this->request->is('post') || $this->request->is('put')) {

			/* Imagem */

			$diretorio = $this->getDiretorioMarcas();
			$this->FileUpload->permitirSomenteImagens();

			if ($this->request->data['Marca']['imagem']['name'] == '') {
				unset($this->request->data['Marca']['imagem']);
			} else {
				$path_parts = pathinfo($this->request->data['Marca']['imagem']['name']);

				$nomeImagem = strtolower($this->request->data['Marca']['descricao']);
				$this->request->data['Marca']['imagem']['name'] = Inflector::slug($nomeImagem, '_') . '.' . $path_parts['extension'];
				$this->request->data['Marca']['imagem'] = $this->FileUpload->upload($diretorio, array($this->request->data['Marca']['imagem']), true);
			}

			/* Fim Imagem */

/*			$this->request->data['Slug']['url'] = '/' . Inflector::slug($this->request->data['Slug']['url'], '-');
			$this->request->data['Slug']['url'] = strtolower($this->request->data['Slug']['url']);

			$verificaSlug = $this->Slug->find('first', array(
				'conditions' => array(
					'Slug.url' => $this->request->data['Slug']['url'] 
				)
			));

			if(count($verificaSlug) == 0 || $verificaSlug['Slug']['id'] == $marca['Marca']['slug_id']){

				$this->Slug->id = $marca['Marca']['slug_id'];
				$this->Slug->saveField('url', $this->request->data['Slug']['url'], false);*/

				if ($this->Marca->save($this->request->data)) {
					$this->Session->setFlash('Marca editada com sucesso.', FLASH_SUCCESS);
					$this->backToPaginatorIndex();
				} else {
					$this->Session->setFlash('Ocorreu um erro ao tentar editar a Marca. Por favor, tente novamente.', FLASH_ERROR);
				}

/*			} else {
				$this->Session->setFlash('Já existe uma url  "<strong>' .  $this->request->data['Slug']['url']  . '</strong>". Por favor, cadastre uma url diferente.', FLASH_ERROR);
				$this->request->data['Slug']['url'] = $marca['Slug']['url'];
			}*/


		}  else {
			$this->request->data = $this->Marca->read(null, $id);

		}

		
	}

	public function admin_excluir($id = null) {
		$this->Marca->id = $id;

		if (!$this->Marca->exists()) {
			throw new NotFoundException('Marca inexistente.');
		}

		if ($this->request->is('post')) {

			/* Verifica se a marca está sendo utilizada por algum produto para poder ser excluída */
			$produtos = $this->Produto->find('count', array(
				'conditions' => array(
					'Produto.ativo' => true,
					'Produto.marca_id' => $id
				)
			));

			if ($produtos == 0) {

				if ($this->Marca->saveField('ativo', false, false)) {
					$marca = $this->Marca->find('first', array(
						'contain' => array(
							'Slug'
						),
						'conditions' => array(
							'Marca.ativo' => false,
							'Marca.id' => $id
						)
					));

					$this->Slug->id = $marca['Slug']['id'];
					if ($this->Slug->saveField('ativo', false, false)) {
						$this->Session->setFlash('Marca desativada com sucesso.', FLASH_SUCCESS);
						$this->backToPaginatorIndex();
					} else {
						$this->Session->setFlash('Ocorreu um erro ao tentar desativar a url da Marca. Por favor, informe nossa equipe sobre o ocorrido.', FLASH_ERROR);
					}
				} else {
					$this->Session->setFlash('Ocorreu um erro ao tentar excluir a Marca. Por favor, tente novamente.', FLASH_ERROR);
				}

			} else {
				$this->Session->setFlash('Não é possível exluir esta marca, pois a mesma possui produto ativo no site.', FLASH_ERROR);
				$this->backToPaginatorIndex();
			}

		}

	}

	public function admin_ativar($id = null) {
		$this->Marca->id = $id;

		if (!$this->Marca->exists()) {
			throw new NotFoundException('Marca inexistente.');
		}

		if ($this->request->is('post')) {

			if ($this->Marca->saveField('ativo', true, false)) {


				$marca = $this->Marca->find('first', array(
					'contain' => array(
						'Slug'
					),
					'conditions' => array(
						//'Categoria.ativo' => false,
						'Marca.id' => $id
					)
				));

				$this->Slug->id = $marca['Slug']['id'];
				$this->Slug->saveField('ativo', true, false);

				$this->Session->setFlash('Marca ativada com sucesso.', FLASH_SUCCESS);
				$this->backToPaginatorIndex();
			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar ativar a Marca. Por favor, tente novamente.', FLASH_ERROR);
			}
		}

	}

	private function getDiretorioMarcas() {
		$diretorio = $this->getDiretorioProjeto('marcas');
		new Folder($diretorio, true);

		return $diretorio;
	}

}
?>