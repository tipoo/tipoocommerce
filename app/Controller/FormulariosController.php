<?php
App::uses('AppController', 'Controller');

class FormulariosController extends AppController {

	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('contato');
	}

	public function contato() {

		if(($this->request->is('post') || $this->request->is('put')) && !empty($this->request->data)) {

			$email = new CakeEmail('contato');
			$email->theme(ucfirst(PROJETO));
			$email->replyTo($this->request->data('Contato.email'));
			$email->to(Configure::read('Sistema.Contato.email'));
			$email->viewVars(array(
				'nome' => $this->request->data('Contato.nome'),
				'sobrenome' => $this->request->data('Contato.sobrenome'),
				'telefone' => $this->request->data('Contato.telefone'),
				'email' => $this->request->data('Contato.email'),
				'assunto' => $this->request->data('Contato.assunto'),
				'mensagem' => $this->request->data('Contato.mensagem')
			));

			try {
				$email->send();
				unset($this->request->data['Contato']);
				$this->Session->setFlash('Mensagem enviada com sucesso.', FLASH_SUCCESS, array(), 'contato');
				$this->redirect($this->referer());
			} catch (Exception $e) {
				$this->Session->setFlash('Ocorreu um erro ao tentar enviar sua mensagem. Por favor, tente novamente.', FLASH_ERROR);
			}
		}
	}

}

?>