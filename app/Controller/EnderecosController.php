<?php
App::uses('AppController', 'Controller');

class EnderecosController extends AppController {

	public $uses = array('Endereco', 'Logradouro', 'Estado', 'Cidade', 'Bairro');

	public function admin_logradouro() {

		$cep = null;
		if (isset($this->request->params['named']['cep'])) {
			$cep = $this->request->params['named']['cep'];
			$this->request->data['Filtro']['cep'] = $cep;

		}

		$this->paginate = array(
			'contain' => array(
				'Cidade',
				'Bairro'
			),
			'conditions' => array(
				'Logradouro.cep' => $cep
			),
			'order' => array(
				'Logradouro.nomeclog' => 'ASC'
			),
		);


		$this->set('logradouros', $this->paginate('Logradouro'));
		$this->set('cep', $cep);

	}

	public function admin_adicionar_logradouro($cep = null, $loja_fisica = null) {

		$estados = $this->Estado->find('list', array(
			'fields' => array(
				'Estado.id',
				'Estado.nome'
			)
		));
		$this->set('estados', $estados);
		$this->set('cep', $cep);

		if($this->request->is('post')) {

			$uf = $this->Estado->find('first', array(
				'contain'=> false,
				'conditions'=> array(
					'Estado.id' => $this->request->data['Logradouro']['estado_id']
				),
			));

			$this->request->data['Logradouro']['cep'] = $cep;
			$this->request->data['Logradouro']['uf'] = $uf['Estado']['uf'] ;

			$this->Logradouro->create();
			if ($this->Logradouro->save($this->request->data)) {
				$this->Session->setFlash('Logradouro salvo com sucesso.', FLASH_SUCCESS);

				if ($loja_fisica != '') {
					$this->redirect(array('controller' => 'lojasFisicas', 'action' => 'index'));
				} else {
					$this->redirect(array('controller' => 'enderecos', 'action' => 'logradouro'));
				}

			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar salvar o Logradouro. Por favor, tente novamente.', FLASH_ERROR);
			}
		}

	}

    public function admin_ajax_carregar_cidades($estado_id = null) {

		$cidades = $this->Cidade->find('all', array(
			'contain' => false,
			'conditions' => array(
				'Cidade.estado_id' => $estado_id
			),
			'fields' => array(
				'Cidade.id',
				'Cidade.nome'
			),
		));

		if (count($cidades)) {
			$json = array('sucesso' => true, 'cidades' => $cidades);
		} else {
			$json = array('sucesso' => false, 'mensagem' => 'Ocorreu um erro...');
		}

		$this->autoRender = false;
		$output = fopen('php://output', 'w');
		fputs($output, json_encode($json));
		fclose($output);
	}

    public function admin_ajax_carregar_bairros($cidade_id = null) {

		$bairros = $this->Bairro->find('all', array(
			'conditions' => array(
				'Bairro.cidade_id' => $cidade_id
			),
			'fields' => array(
				'Bairro.id',
				'Bairro.nome'
			),
		));

		if (count($bairros)) {
			$json = array('sucesso' => true, 'bairros' => $bairros);
		} else {
			$json = array('sucesso' => false, 'mensagem' => 'Nenhum bairro cadastrado para esta cidade.');
		}

		$this->autoRender = false;
		$output = fopen('php://output', 'w');
		fputs($output, json_encode($json));
		fclose($output);
	}

}
?>