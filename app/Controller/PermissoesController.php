<?php
App::uses('AppController', 'Controller');

class PermissoesController extends AppController{

	public $uses = array('Permissao', 'Objeto', 'Perfil');

	public function beforeFilter() {
		parent::beforeFilter();

		//$this->Auth->allow('admin_index', 'admin_ajax_permitidos', 'admin_ajax_permitir');
	}

	public function admin_index(){
		if ($this->Permissoes->check($this->usuarioAutenticado['perfil_id'], 'permissoes', '@listar_todos_perfis')) {
			$perfis = $this->Perfil->find('list', array('order' => 'descricao'));
		} else {
			$perfis = $this->Perfil->find('list', array('conditions' => array('Perfil.ativo' => true), 'order' => 'descricao'));
		}
		
		$objetos = $this->Objeto->find('all', array('conditions' => array('Objeto.ativo' => true), 'order' => array('Objeto.plugin', 'Objeto.controller', 'Objeto.action')));
		$this->set('objetos', $objetos);
		$this->set('perfis', $perfis);
	}

	public function admin_ajax_permitidos($perfil_id = null){
		$permissoes = $this->Permissao->find('all', array('conditions' => array('Permissao.perfil_id' => $perfil_id)));
		$json = array('sucesso' => true, 'permissoes' => $permissoes);
		$this->renderJson($json);
	}

	public function admin_ajax_permitir($perfil_id, $objeto_id, $permitido){
		if ($permitido == '0') {
			$this->Permissao->deleteAll(array('Permissao.perfil_id' => $perfil_id, 'Permissao.objeto_id' => $objeto_id));
		} else if ($permitido == '1') {
			$this->Permissao->create();
			$this->Permissao->save(array('Permissao'=> array('perfil_id' => $perfil_id, 'objeto_id' => $objeto_id)));
		}
		$json = array('sucesso' => true);
		$this->renderJson($json);
	}

}

?>
