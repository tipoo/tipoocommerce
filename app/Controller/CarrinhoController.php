<?php
App::uses('AppController', 'Controller');

class CarrinhoController extends AppController {

	private $skus = array();
	private $pedido = array();
	private $cep;
	private $cupom;

	public $uses = array('Sku', 'Promocao', 'Configuracao', 'Servico', 'ServicoAnexo');

	public $components = array(
		'CustomTagsParser' => array('actions' => array('index')),
		'RegrasPromocoesProduto',
		'RegrasPromocoesPedidos',
		'RegrasPromocoesFrete',
		'RegrasPromocoesLeveXPagueY',
		'RegrasPromocoesPrecos',
		'RegrasPromocoesCompreJunto',
		'RegrasPromocoesCupom'
	);

	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow(
			'index',
			'adicionar',
			'editar',
			'remover',
			'calcular_frete',
			'aplicar_cupom',
			'adicionar_servico',
			'remover_servico',
			'adicionar_anexo',
			'remover_anexo',
			'ajax_simular_frete'
		);

		$this->layout = 'carrinho';

		// Verificando se já existe uma estrutura para armazenar os SKUS na sessão e se se não há cria
		if ($this->Session->check('Carrinho.Skus')) {
			$this->skus = $this->Session->read('Carrinho.Skus');
		}

		if ($this->Session->check('Carrinho.Pedido')) {
			$this->pedido = $this->Session->read('Carrinho.Pedido');
		}

		if ($this->Session->check('Carrinho.CEP')) {
			$this->cep = $this->Session->read('Carrinho.CEP');
		}

		if ($this->Session->check('Carrinho.Cupom')) {
			$this->cupom = $this->Session->read('Carrinho.Cupom');
		}
	}

	public function index() {
		$cupom = $this->cupom;
		// Normalizando o formato do cep
		$cep = null;
		if (!empty($this->cep)) {
			$cep = str_replace('-', '', $this->cep);
		}

		// Revalidando o estoque cada vez que o usuário tenta acessar o carrinho.
		$this->set('msg_skus', $this->revalidar_estoque());

		// Injetar promoções nos SKUS
		$this->skus = $this->aplicar_promocoes($this->skus, $cep, $cupom);

		// Calculando desconto baseado em promoções dos produtos (desconto)
		$this->skus = $this->RegrasPromocoesProduto->definir_promocoes_skus($this->skus);

		// Atualizando o preço dos produtos com base nas promoções de desconto no produto e compre junto
		$this->skus = $this->RegrasPromocoesPrecos->recalcular($this->skus);

		// Calculando desconto baseado em promoções de compre junto
		$this->skus = $this->RegrasPromocoesCompreJunto->definir_promocoes_skus($this->skus);

		// Calculando desconto baseado em promoções Leve X, Pague Y
		$this->skus = $this->RegrasPromocoesLeveXPagueY->definir_promocoes_skus($this->skus);

		// Recalcular valores totais dos produtos (preco e peso)
		$valor_sub_total = $this->calcular_sub_total();

		// Recalcular o valor subtotal se o cupom for preenchido
		$valor_sub_total = $this->aplicar_promocoes_cupom($valor_sub_total, $this->skus);

		$this->set('skus', $this->skus);
		$this->set('valor_sub_total', $valor_sub_total);
		$this->set('cep', $this->cep);

		if (!$this->usuarioAutenticado) {
			$cupom = null;
		}

		$this->set('cupom', $cupom);
		$this->persistir_redirecionar(false);
	}

	/**
	  * Adiciona um ou mais skus no carrinho, com uma quantidade variável para cada sku.
	  * Para adicionar mais de um sku diferente, os skus e as suas respectivas quantidades
	  * devem ser passados separados por vírgula. Caso tenha mais de um sku diferente,
	  * obrigatória a passagem do parâmetro de quantidade
	  */
	public function adicionar($ids, $qtds_adicionar = 1) {
		$ids_separados = explode(',', $ids);
		$qtds_separadas = explode(',', $qtds_adicionar);

		$count = count($ids_separados);
		for ($i = 0; $i < $count; $i++) {
			$id = $ids_separados[$i];
			$qtd_adicionar = $qtds_separadas[$i];

			$sku = $this->Sku->find('first', array(
				'contain' => array(
					'Produto' => array(
						'Imagem' => array(
							'conditions' => array(
								'Imagem.ativo' => true,
								'Imagem.destaque' => true
							)
						),
						'Marca.descricao',
						'CaracteristicasValoresSelecionado' => array(
							'CaracteristicasValor.descricao',
							'Caracteristica' => array('id', 'descricao', 'tipo'),
							'fields' => array('id', 'caracteristica_id', 'caracteristicas_valor_id', 'descricao', 'descricao_grande')
						),
						'Servico' => array(
							'ServicoAnexo' => array(
								'conditions' => array(
									'ServicoAnexo.ativo' => true
								)
							),
							'conditions' => array(
								'Servico.ativo' => true
							)
						)
					),
					'CaracteristicasValoresSelecionado' => array(
						'CaracteristicasValor.descricao',
						'Caracteristica' => array('id', 'descricao', 'tipo'),
						'fields' => array('id', 'caracteristica_id', 'caracteristicas_valor_id', 'descricao_grande')
					),
					'Imagem' => array(
						'conditions' => array(
							'Imagem.ativo' => true,
							'Imagem.destaque' => true
						)
					)
				),
				'conditions' => array(
					'Sku.ativo',
					'Sku.id' => $id
				)
			));

			if (array_key_exists($id, $this->skus)) {

				$this->skus[$id]['qtd'] = $this->skus[$id]['qtd'] + $qtd_adicionar;

				// Verificando se o produto deve ser adicionado como pre venda ou nao
				$sku['pre_venda'] = $this->RegrasProdutos->situacao_estoque_sku($sku, $qtd_adicionar) == 'PreVenda' ? true : false;

			} else {

				if (!$sku) {
					$this->Session->setFlash('Não foi possível adicionar o produto ao carrinho. Ele não encontra-se em nossa base de dados.', FLASH_ERROR);
				} else {
					$sku['qtd'] = $qtd_adicionar;

					$sku['preco_unitario'] = $sku['Produto']['preco_por']; // Buscar o preco aqui
					$sku['peso_unitario'] = $sku['Sku']['peso'];

					// Verificando se o produto deve ser adicionado como pre venda ou nao
					$sku['pre_venda'] = $this->RegrasProdutos->situacao_estoque_sku($sku, $qtd_adicionar) == 'PreVenda' ? true : false;

					$this->skus[$id] = $sku;
				}
			}
		}

		$this->persistir_redirecionar();
	}

	public function editar($id, $qtd) {

		if (array_key_exists($id, $this->skus)) {
			$sku = $this->Sku->find('first', array(
				'contain' => false,
				'conditions' => array(
					'Sku.ativo',
					'Sku.id' => $id
				)
			));

			// Verificando se o produto deve ser editado como pre venda ou nao
			$this->skus[$id]['pre_venda'] = $this->RegrasProdutos->situacao_estoque_sku($sku, $qtd) == 'PreVenda' ? true : false;

			$this->skus[$id]['qtd'] = $qtd;

		}

		$this->persistir_redirecionar();
	}

	public function remover($id) {
		if (array_key_exists($id, $this->skus)) {
			unset($this->skus[$id]);
		}

		$this->persistir_redirecionar();
	}

	public function ajax_simular_frete($cep = null, $valor_sub_total = null) {
		$this->layout = 'ajax';

		if ($cep) {
			$cupom = $this->cupom;
			$this->cep = $cep;

			// Injetar promoções nos SKUS
			$this->skus = $this->aplicar_promocoes($this->skus, $cep, $cupom);

			// Calculando desconto baseado em promoções dos produtos (desconto)
			$this->skus = $this->RegrasPromocoesProduto->definir_promocoes_skus($this->skus);

			$frete_transportadoras_correios = $this->FreteCorreios->buscar($this->skus, $cep);
			$frete_transportadoras_transp = $this->FreteTransportadoras->buscar($this->skus, $cep);
			$frete_transportadoras = array_merge($frete_transportadoras_correios, $frete_transportadoras_transp);

			foreach ($frete_transportadoras as $key => $frete_transportadora) {
				$frete_transportadoras[$key] = $this->RegrasPromocoesFrete->calcular_valor_frete($frete_transportadora, $this->skus, $cep);
			}

		} else {
			$this->cep = null;
		}

		// Adiciona o novo valor do cep na sessão
		$this->Session->write('Carrinho.CEP', $this->cep);

		$this->set(compact('frete_transportadoras', 'valor_sub_total'));
	}

	/**
	 * Este método agrupa os Skus em Produtos para poder aplicar promoções
	 * onde a quantidade de peças do produto é levada em consideração.
	 * Após verificar as promoções por produtos, aplica as mesmas nos skus
	 * de cada produto, ou seja, nos skus do carrinho.
	 */
	private function aplicar_promocoes($skus = array(), $cep = null, $cupom = null) {
		$produtos_skus = array();

		$qtd_pecas_carrinho = 0;
		// Juntar os skus do mesmo produto e somando a quantidade de pecas no carrinho
		foreach ($skus as $id => $sku) {
			if (!array_key_exists($sku['Produto']['id'], $produtos_skus)) {
				$produtos_skus[$sku['Produto']['id']] = array();
			}
			$produtos_skus[$sku['Produto']['id']][] = $id;
			$qtd_pecas_carrinho += $sku['qtd'];
		}

		$sub_total_carrinho = $this->calcular_sub_total();

		// Verificar as promocoes para cada produto e aplicar a todos os skus dele
		foreach ($produtos_skus as $id => $produto_skus) {
			// Somando a quantidade de todos os skus do produto atual
			$qtd = 0;
			foreach ($produto_skus as $sku_id) {
				$qtd += $skus[$sku_id]['qtd'];
			}

			// Armazenando os valores originais antes de aplicar as promoções
			if (!isset($skus[$sku_id]['Produto']['preco_de_original']) && !isset($skus[$sku_id]['Produto']['preco_por_original'])) {
				$skus[$sku_id]['Produto']['preco_de_original'] = $skus[$sku_id]['Produto']['preco_de'];
				$skus[$sku_id]['Produto']['preco_por_original'] = $skus[$sku_id]['Produto']['preco_por'];
			}

			$produto = $this->Promocoes->aplicar_promocoes(array('Produto' => $skus[$sku_id]['Produto']), $qtd, $cep, $sub_total_carrinho['valor_produtos'], $qtd_pecas_carrinho, $cupom);

			foreach ($produto_skus as $sku_id) {
				unset($skus[$sku_id]['Promocao']);
				$skus[$sku_id] = array_merge($skus[$sku_id], $produto);
			}
		}

		return $skus;
	}

	private function aplicar_promocoes_cupom($valor_sub_total, $skus) {
		$valor_sub_total = $this->RegrasPromocoesCupom->aplicar_promocoes_cupom($valor_sub_total, $skus, $this->usuarioAutenticado);
		$this->pedido = $valor_sub_total;

		return $valor_sub_total;
	}

	private function calcular_sub_total() {
		$valor_sub_total = 0;
		$valor_servicos = 0;
		$this->pedido = array();

		foreach ($this->skus as $id => $sku) {
			$preco_unitario = (isset($this->skus[$id]['preco_unitario_desconto']) && $this->skus[$id]['preco_unitario_desconto'] > 0) ? $this->skus[$id]['preco_unitario_desconto'] : $this->skus[$id]['preco_unitario'];
			$valor_unitario = $preco_unitario;
			$this->skus[$id]['preco_total'] = $preco_unitario * $this->skus[$id]['qtd'];
			$this->skus[$id]['peso_total'] = $this->skus[$id]['peso_unitario'] * $this->skus[$id]['qtd'];
			$valor_sub_total += $this->skus[$id]['preco_total'];

			if (isset($this->skus[$id]['Servico']) && count($this->skus[$id]['Servico']) > 0) {
				foreach ($this->skus[$id]['Servico'] as $servico) {
					$valor_servicos += $servico['valor'] * $this->skus[$id]['qtd'];
				}
			}
		}

		// Calculando desconto baseado em promoções do pedido
		$valor_sub_total_com_desconto = $this->RegrasPromocoesPedidos->definir_promocoes_pedido($valor_sub_total, $this->skus);
		if (isset($valor_sub_total_com_desconto['Promocao']['melhor_desconto_pedido']['promocao'])) {
			$this->pedido['Promocao'] = $valor_sub_total_com_desconto['Promocao'];
			$valor_sub_total_com_desconto = $valor_sub_total_com_desconto['Promocao']['melhor_desconto_pedido']['valor'];
		}

		$this->pedido['valor_produtos'] = $valor_sub_total;
		$this->pedido['valor_servicos'] = $valor_servicos;
		$this->pedido['valor_desconto'] = $valor_sub_total - $valor_sub_total_com_desconto;
		$this->pedido['valor_total'] = $valor_sub_total_com_desconto + $valor_servicos;

		return $this->pedido;
	}

	private function revalidar_estoque() {
		$msg_skus = array();
		$skus_remover = array();

		foreach ($this->skus as $id => $value) {
			$sku = $this->Sku->find('first', array(
				'contain' => array(
					'Produto.descricao'
				),
				'conditions' => array(
					'Sku.ativo',
					'Sku.id' => $id
				)
			));

			// Verificando se o produto pode ser vendido como pré-venda
			$this->skus[$id]['pre_venda'] = $this->RegrasProdutos->situacao_estoque_sku($sku, $this->skus[$id]['qtd']) == 'PreVenda' ? true : false;

			if (!$this->skus[$id]['pre_venda']) {
				if ($sku['Sku']['qtd_estoque'] == 0) {
					$skus_remover[] = $sku['Sku']['id'];
					$msg_skus[] = 'O produto "' . $sku['Produto']['descricao'] . ' - ' . $sku['Sku']['descricao'] . '" não consta mais em nosso estoque. Ele foi REMOVIDO do seu carrinho de compras.';
				} else if ($this->skus[$id]['qtd'] > $sku['Sku']['qtd_estoque']) {
					$this->skus[$id]['qtd'] = $sku['Sku']['qtd_estoque'];
					$msg_skus[] = 'Consta(m) apenas ' . $sku['Sku']['qtd_estoque'] . ' unidade(s) disponíveis do produto "' . $sku['Produto']['descricao'] . ' - ' . $sku['Sku']['descricao'] . '" no nosso estoque. Você está tentando comprar uma quantidade maior que a disponível, por isso a quantidade no seu carrinho foi REDUZIDA para o máximo disponível.';
				}
			}

			$this->skus[$id]['preco_total'] = $this->skus[$id]['preco_unitario'] * $this->skus[$id]['qtd'];
			$this->skus[$id]['peso_total'] = $this->skus[$id]['peso_unitario'] * $this->skus[$id]['qtd'];
		}

		foreach ($skus_remover as $id) {
			$this->remover($id);
		}

		return $msg_skus;
	}

	public function calcular_frete($cep = null) {
		if ($cep) {
			if ($cep != $this->cep) {
				$this->cep = $cep;
			}

		} else {
			$this->cep = null;
		}

		$this->persistir_redirecionar();
	}

	public function aplicar_cupom($cupom = null) {
		if ($cupom) {
			if ($cupom != $this->cupom) {
				$this->cupom = $cupom;
			}

		} else {
			$this->cupom = null;
		}

		if ($this->usuarioAutenticado) {
			$this->persistir_redirecionar();
		} else {
			$this->persistir_redirecionar($url = '/clientes/login?redirect=%2Fcarrinho', $mensagem_cupom = true);
		}
	}

	public function adicionar_servico($sku_id, $servico_id) {
		$servico = $this->Servico->find('first', array(
			'contain' => false,
			'conditions' => array(
				'Servico.id' => $servico_id
			)
		));

		$this->skus[$sku_id]['Servico'][$servico_id] = $servico['Servico'];

		$this->persistir_redirecionar();
	}

	public function remover_servico($sku_id, $servico_id) {
		if (array_key_exists($sku_id, $this->skus) && array_key_exists($servico_id, $this->skus[$sku_id]['Servico'])) {
			unset($this->skus[$sku_id]['Servico'][$servico_id]);
		}

		$this->persistir_redirecionar();
	}

	public function adicionar_anexo($sku_id, $anexo_id, $valor_anexo = null) {
		$anexo = $this->ServicoAnexo->find('first', array(
			'contain' => array(
				'Servico'
			),
			'conditions' => array(
				'ServicoAnexo.id' => $anexo_id
			)
		));

		if (!isset($this->skus[$sku_id]['Servico'][$anexo['Servico']['id']])) {
			$this->skus[$sku_id]['Servico'][$anexo['Servico']['id']] = $anexo['Servico'];
		}

		$this->skus[$sku_id]['Servico'][$anexo['Servico']['id']]['ServicoAnexo'][$anexo['ServicoAnexo']['id']] = $anexo['ServicoAnexo'];
		$this->skus[$sku_id]['Servico'][$anexo['Servico']['id']]['ServicoAnexo'][$anexo['ServicoAnexo']['id']]['AnexoValor']['valor'] = $valor_anexo;

		$this->persistir_redirecionar();
	}

	public function remover_anexo($sku_id, $servico_id, $anexo_id) {
		if (isset($this->skus[$sku_id]['Servico'][$servico_id]['ServicoAnexo'][$anexo_id])) {
			if (count($this->skus[$sku_id]['Servico'][$servico_id]['ServicoAnexo']) > 1) {
				unset($this->skus[$sku_id]['Servico'][$servico_id]['ServicoAnexo'][$anexo_id]);
			} else {
				unset($this->skus[$sku_id]['Servico'][$servico_id]);
			}
		}

		$this->persistir_redirecionar();
	}


	private function persistir_redirecionar($url = array('controller' => 'carrinho', 'action' => 'index'), $mensagem_cupom = false) {
		$this->Session->write('Carrinho.Skus', $this->skus);
		$this->Session->write('Carrinho.Pedido', $this->pedido);
		$this->Session->write('Carrinho.CEP', $this->cep);
		$this->Session->write('Carrinho.Cupom', $this->cupom);

		if ($mensagem_cupom) {
			$this->Session->setFlash('Para utilizar o cupom você precisa estar logado. <a href="' . Router::url('/carrinho', true) . '">Voltar para o carrinho</a>.', FLASH_INFO, array(), 'cupom_desconto');
		}

		if ($url !== false) {
			$this->redirect($url);
		}
	}

}
?>