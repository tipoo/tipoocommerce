<?php
App::uses('AppController', 'Controller');

class SlugsController extends AppController {

	public function admin_index() {

			$conditions = array(
				'Slug.ativo' => true
			);

			$this->paginate = array(
				'contain' => array(
					'Pagina'
				),
				'conditions' => array(
					'Slug.controller <>' => 'paginas'
				),
				'order' => array(
					'Slug.ativo' => 'DESC',
					'Slug.action' => 'ASC',
					'Slug.url' => 'ASC'
				),
				//'limit' => Configure::read('Sistema.Paginacao.limit')
				'limit' => 50
			);
			$this->set('slugs', $this->paginate());

	}

	public function admin_editar($id = null) {
		$this->Slug->id = $id;

		$slug = $this->Slug->find('first', array(
			'conditions' => array(
				'Slug.id' => $id
			)
		));

		if (!$this->Slug->exists()) {
			throw new NotFoundException('Url inexistente.');
		}

		if ($this->request->is('post') || $this->request->is('put')) {

			$urlSlug = $this->Slugger->regularizarSlugComposto($this->request->data('Slug.url'));
			$this->request->data('Slug.url', $urlSlug);

			$this->request->data['Slug']['url'] = strtolower($this->request->data['Slug']['url']);

			$verificaSlug = $this->Slug->find('first', array(
				'conditions' => array(
					'Slug.url' => $urlSlug
				)
			));

			if (count($verificaSlug) == 0 || ($verificaSlug['Slug']['id'] == $slug['Slug']['id'])) {

				if ($this->Slug->save($this->request->data)) {

					$this->Session->setFlash('Url salva com sucesso.', FLASH_SUCCESS);
					$this->backToPaginatorIndex();
				} else {
					$this->Session->setFlash('Ocorreu um erro ao tentar salvar a url. Por favor, tente novamente.', FLASH_ERROR);
				}
			} else {
				$this->Session->setFlash('Já existe uma url  "<strong>' .  $this->request->data['Slug']['url']  . '</strong>". Por favor, cadastre uma url diferente.', FLASH_ERROR);
				$this->request->data['Slug']['url'] = $slug['Slug']['url'];

			}

		} else {
			$this->request->data = $this->Slug->read(null, $id);

		}
	}
}
?>