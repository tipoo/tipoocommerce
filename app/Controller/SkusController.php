<?php
App::uses('AppController', 'Controller');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
App::uses('CropImage', 'Vendor');

class SkusController extends AppController {

	public $uses = array('Sku', 'Caracteristica', 'Produto', 'Categoria', 'CaracteristicasValoresSelecionado', 'Configuracao');

	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('ajax_buscar_skus');
	}

	public function admin_index($produto_id = null) {


	$conditions = array(
			//'Sku.ativo' => true,
			'Sku.produto_id' => $produto_id
		);

		$order = array(
			'Sku.ordem' => 'ASC',
			'Sku.created' => 'DESC'
		);

		$skus = $this->Sku->find('all', array(
			'contain' => false,
			'conditions' => $conditions,
			'order' => $order,
		));

		$produto = $this->Produto->find('first', array(
			'conditions' => array(
				'Produto.id' => $produto_id
			)
		));

		$this->set('skus', $skus);
		$this->set('produto', $produto);
		$this->set('produto_id', $produto_id);
	}

	public function admin_adicionar($id = null) {

		$configuracao = $this->Configuracao->find('first');

		$produto = $this->Produto->find('first', array(
			'contain' => false,
			'conditions' => array(
				'Produto.id' => $id
			),
			'fields' => array('descricao', 'categoria_id')
		));

		if ($this->request->is('post')) {

			$verificaSkuExistente = $this->Sku->find('count', array(
				'conditions' => array(
					'Sku.ativo' => true,
					'Sku.sku' => $this->request->data['Sku']['sku']
				)
			));

			$verifica_codigo_referencia = $this->Sku->find('count', array(
				'contain' => false,
				'conditions' => array(
					'AND' => array(
						'Sku.codigo_de_referencia' => $this->request->data['Sku']['codigo_de_referencia'],
						'Sku.codigo_de_referencia <>' => '',
					)
				)
			));

			$verifica_ean = $this->Sku->find('count', array(
				'contain' => false,
				'conditions' => array(
					'AND' => array(
						'Sku.ean' => $this->request->data['Sku']['ean'],
						'Sku.ean <>' => '',
					)
				)
			));

			if ($verificaSkuExistente == 0 && $verifica_codigo_referencia == 0 && $verifica_ean == 0) {

				/* Imagem */

				$diretorio = $this->getDiretorioProdutos();
				$this->FileUpload->permitirSomenteImagens();

				if ($this->request->data['Imagem'][0]['imagem']['name'] == '') {
					unset($this->request->data['Imagem']);
				} else {
					$path_parts = pathinfo($this->request->data['Imagem'][0]['imagem']['name']);
					$nomeImagem = strtolower($produto['Produto']['descricao']);

					$this->request->data['Imagem'][0]['imagem']['name'] = Inflector::slug($nomeImagem, '_') . '.' . $path_parts['extension'];
					$this->request->data['Imagem'][0]['imagem'] = $this->FileUpload->upload($diretorio, array($this->request->data['Imagem'][0]['imagem']), true);

					/* Recorte e Resize */

					if (isset($this->request->data['Imagem'])) {

						$image = new CropImage();
						$image->setImage($diretorio . DS . $this->request->data['Imagem'][0]['imagem']);
						$image->createThumb($diretorio . DS . 'big_' . $this->request->data['Imagem'][0]['imagem'], $configuracao['Configuracao']['img_big_w'] , $configuracao['Configuracao']['img_big_h']);
						$image->createThumb($diretorio . DS . 'medium_' . $this->request->data['Imagem'][0]['imagem'], $configuracao['Configuracao']['img_medium_w'] , $configuracao['Configuracao']['img_medium_h']);
						$image->createThumb($diretorio . DS . 'normal_' . $this->request->data['Imagem'][0]['imagem'], $configuracao['Configuracao']['img_normal_w'] , $configuracao['Configuracao']['img_normal_h']);
						$image->createThumb($diretorio . DS . 'small_' . $this->request->data['Imagem'][0]['imagem'], $configuracao['Configuracao']['img_small_w'] , $configuracao['Configuracao']['img_small_h']);
						$image->createThumb($diretorio . DS . 'thumb_' . $this->request->data['Imagem'][0]['imagem'], $configuracao['Configuracao']['img_thumb_w'] , $configuracao['Configuracao']['img_thumb_h']);
					}

					/* Fim Recorte e Resize */
				}

				/* Fim Imagem */

				$this->request->data['Sku']['produto_id'] = $id;

				//Verifica qual o sku destaque atual
				$skuDestaque = $this->Sku->find('first', array(
					'contain' => false,
					'conditions' => array(
						'Sku.ativo' => true,
						'Sku.destaque' => true,
						'Sku.produto_id' => $id
					)
				));

				if ($this->request->data['Sku']['destaque'] == true) {
					if (count($skuDestaque) != 0) {
						$this->Sku->id = $skuDestaque['Sku']['id'];
						if (!$this->Sku->saveField('destaque', 0, false)) {
							$this->request->data['Sku']['destaque'] = false;
						}
					}
				} else {
					if (count($skuDestaque) == 0) {
						$this->request->data['Sku']['destaque'] = true;
					}
				}

				$qtd_skus = $this->Sku->find('count', array(
					'conditions' => array(
						'Sku.ativo' => true,
						'Sku.produto_id' => $id
					),
				));

				if ($qtd_skus == 0) {
					$this->request->data['Sku']['destaque'] = true;
				}

				$this->request->data['Sku']['ordem'] = $qtd_skus + 1;

				/* Limpa do array os valores vazios */

				if (isset($this->request->data['CaracteristicasValoresSelecionado'])) {
					foreach ($this->request->data['CaracteristicasValoresSelecionado'] as $key => $caracteristicas_valor_selecionado) {
						if ($caracteristicas_valor_selecionado['caracteristica_tipo'] == 'L') {
							if ($caracteristicas_valor_selecionado['caracteristicas_valor_id'] == '') {
								unset($this->request->data['CaracteristicasValoresSelecionado'][$key]);
							}
						} else if ($caracteristicas_valor_selecionado['caracteristica_tipo'] == 'TG') {
							if ($caracteristicas_valor_selecionado['descricao_grande'] == '') {
								unset($this->request->data['CaracteristicasValoresSelecionado'][$key]);
							}
						} else {
							if ($caracteristicas_valor_selecionado['descricao'] == '') {
								unset($this->request->data['CaracteristicasValoresSelecionado'][$key]);
							}
						}
					}
				}

				/* Fim - Limpa do array os valores vazios */

				$this->Sku->create();
				if ($this->Sku->saveAll($this->request->data)) {

					$novo_sku = $this->Sku->findById($this->Sku->id);

					if ($novo_sku['Sku']['destaque'] ==  true) {
						$this->Sku->id = $novo_sku['Sku']['id'];
						$this->Sku->saveField('ordem', 1, false);

						$skus_ordem = $this->Sku->find('all', array(
							'conditions' => array(
								'Sku.produto_id'  =>  $id,
								'Sku.id <>'  =>$novo_sku['Sku']['id'],
							),
						));

						$count = 2;
						foreach ($skus_ordem as $sku_ordem) {
							$this->Sku->id  = $sku_ordem['Sku']['id'];
							$this->Sku->saveField('ordem', $count, false);
							$count++;
						}
					}

					$this->Session->setFlash('Sku salvo com sucesso.', FLASH_SUCCESS);
					$this->backToPaginatorIndex();

				} else {
					$this->Session->setFlash('Ocorreu um erro ao tentar salvar o Sku. Por favor, tente novamente.', FLASH_ERROR);
				}
			} else {

				if ($verificaSkuExistente != 0) {
					$this->Session->setFlash('Este SKU já existe. Escolha outro SKU para salvar.', FLASH_ERROR);
				} else if ($verifica_codigo_referencia != 0) {
					$this->Session->setFlash('Este Código de Referência já cadastrado em outro SKU.', FLASH_ERROR);
				} else if ($verifica_ean != 0) {
					$this->Session->setFlash('Este EAN já cadastrado em outro SKU.', FLASH_ERROR);
				}

			}
		}

		$caracteristicas_gerais = $this->getCaracteristicasGerais();
		$caracteristicas_categoria = $this->getCaracteristicasCategoria($produto['Produto']['categoria_id']);
		$mercado_livre_caracteristicas_categoria = $this->getCaracteristicasCategoriaMercadoLivre($produto['Produto']['categoria_id']);

		$this->set('caracteristicas_gerais', $caracteristicas_gerais);
		$this->set('caracteristicas_categoria', $caracteristicas_categoria);
		$this->set('mercado_livre_caracteristicas_categoria', $mercado_livre_caracteristicas_categoria);

	}

	public function admin_editar($id = null, $produto_id = null) {

		$this->Sku->id = $id;
		$this->request->data['Sku']['id'] = $id;

		if (!$this->Sku->exists()) {
			throw new NotFoundException('Sku inexistente.');
		}

		$sku = $this->Sku->find('first', array(
			'contain' => array(
				'Produto' => array('id', 'categoria_id')
			),
			'conditions' => array(
				'Sku.id' => $id
			),
			'fields' => array('id', 'sku', 'destaque','previsao_entrega_pre_venda')
		));

		if ($this->request->is('post') || $this->request->is('put')) {

			$verifica_codigo_referencia = $this->Sku->find('first', array(
				'contain' => false,
				'conditions' => array(
					'AND' => array(
						'Sku.codigo_de_referencia' => $this->request->data['Sku']['codigo_de_referencia'],
						'Sku.codigo_de_referencia <>' => '',
					),
					'Sku.id <>' => $id
				)
			));

			$verifica_ean = $this->Sku->find('first', array(
				'contain' => false,
				'conditions' => array(
					'AND' => array(
						'Sku.ean' => $this->request->data['Sku']['ean'],
						'Sku.ean <>' => '',
					),
					'Sku.id <>' => $id
				)
			));

			if (!count($verifica_codigo_referencia) && !count($verifica_ean) ) {


				if ($this->request->data['Sku']['destaque'] == true) {

					$this->request->data['Sku']['ordem'] = 1;

					$skus_ordem = $this->Sku->find('all', array(
						'conditions' => array(
							'Sku.produto_id'  => $produto_id,
							'Sku.id <>' => $id
						),
					));

					$count = 2;
					foreach ($skus_ordem as $sku_ordem) {
						$this->Sku->id  = $sku_ordem['Sku']['id'];
						$this->Sku->saveField('ordem', $count, false);
						$count++;
					}

					//Verifica qual o sku destaque atual
					$skuDestaque = $this->Sku->find('first', array(
						'contain' => false,
						'conditions' => array(
							'Sku.ativo' => true,
							'Sku.destaque' => true,
							'Sku.produto_id' => $sku['Produto']['id']
						)
					));

					$this->Sku->id = $skuDestaque['Sku']['id'];
					if (!$this->Sku->saveField('destaque', 0, false)) {
						$sucesso = false;
						$this->request->data['Sku']['destaque'] = false;
					}
				}

				$caracteristicas_sucesso = $this->CaracteristicasValoresSelecionado->deleteAll(array('CaracteristicasValoresSelecionado.sku_id' => $id));

				/* Limpa do array os valores vazios */

				if (isset($this->request->data['CaracteristicasValoresSelecionado'])) {
					foreach ($this->request->data['CaracteristicasValoresSelecionado'] as $key => $caracteristicas_valor_selecionado) {
						if ($caracteristicas_valor_selecionado['caracteristica_tipo'] == 'L') {
							if ($caracteristicas_valor_selecionado['caracteristicas_valor_id'] == '') {
								unset($this->request->data['CaracteristicasValoresSelecionado'][$key]);
							}
						} else if ($caracteristicas_valor_selecionado['caracteristica_tipo'] == 'TG') {
							if ($caracteristicas_valor_selecionado['descricao_grande'] == '') {
								unset($this->request->data['CaracteristicasValoresSelecionado'][$key]);
							}
						} else {
							if ($caracteristicas_valor_selecionado['descricao'] == '') {
								unset($this->request->data['CaracteristicasValoresSelecionado'][$key]);
							}
						}
					}
				}

				/* Fim - Limpa do array os valores vazios */

				$this->Sku->id = $id;
				if ($this->Sku->saveAll($this->request->data)) {

					if ($caracteristicas_sucesso) {
						$this->Session->setFlash('Sku editado com sucesso.', FLASH_SUCCESS);

						// Mercado Livre
						if ($this->Configuracao->get('mercado_livre_ativo')) {
							$this->MercadoLivreProdutos->editar_produto($produto_id);
						}
						// Fim - Mercado Livre

						$this->backToPaginatorIndex();
					} else {
						$this->Session->setFlash('Ocorreu um erro ao tentar salvar as características. Por favor, tente novamente.', FLASH_ERROR);
					}

				} else {
					$this->Session->setFlash('Ocorreu um erro ao tentar editar o Sku. Por favor, tente novamente.', FLASH_ERROR);
				}

			} else {

				if (count($verifica_codigo_referencia)) {
					$this->Session->setFlash('O Código de Referência "'.$verifica_codigo_referencia['Sku']['codigo_de_referencia'].'" já cadastrado em outro SKU.', FLASH_ERROR);
				} else if (count($verifica_ean)) {
					$this->Session->setFlash('O EAN "'.$verifica_ean['Sku']['ean'].'" já cadastrado em outro SKU.', FLASH_ERROR);

				}

			}

		} else {

			$this->request->data = $this->Sku->read(null, $id);

			$caracteristicas_valores_selecionados = array();
			foreach ($this->request->data['CaracteristicasValoresSelecionado'] as $key => $caracteristicas_valores_selecionado) {
				$caracteristicas_valores_selecionados[$caracteristicas_valores_selecionado['caracteristica_id']] = $caracteristicas_valores_selecionado;
			}

			$this->request->data['CaracteristicasValoresSelecionado'] = $caracteristicas_valores_selecionados;
		}

		$caracteristicas_gerais = $this->getCaracteristicasGerais();
		$caracteristicas_categoria = $this->getCaracteristicasCategoria($sku['Produto']['categoria_id']);
		$mercado_livre_caracteristicas_categoria = $this->getCaracteristicasCategoriaMercadoLivre($sku['Produto']['categoria_id']);

		$this->set('caracteristicas_gerais', $caracteristicas_gerais);
		$this->set('caracteristicas_categoria', $caracteristicas_categoria);
		$this->set('mercado_livre_caracteristicas_categoria', $mercado_livre_caracteristicas_categoria);
		$this->set('sku', $sku);
		$this->set('produto_id', $produto_id);
	}

	public function ajax_buscar_skus($produto_id = null) {

		$skus = $this->Sku->find('all', array(
			'contain' => array(
				'Produto' => array(
					'Imagem' => array(
						'conditions' => array(
							'Imagem.ativo' => true,
							'Imagem.destaque' => true
						)
					)
				)
			),
			'conditions' => array(
				'Sku.ativo' => true,
				'Sku.produto_id' => $produto_id
			)
		));

		if (count($skus)) {
			$json = array('sucesso' => true, 'skus' => $skus);
		} else {
			$json = array('sucesso' => false, 'mensagem' => 'Nenhum sku encontrado para este produto.');
		}

		$this->autoRender = false;
		$output = fopen('php://output', 'w');
		fputs($output, json_encode($json));
		fclose($output);
	}

	private function getCaracteristicasGerais() {
		$caracteristicas = $this->Caracteristica->find('all', array(
			'contain' => array(
				'CaracteristicasValor' => array(
					'conditions' => array(
						'CaracteristicasValor.ativo' => true
					)
				)
			),
			'conditions' => array(
				'Caracteristica.ativo',
				'Caracteristica.categoria_id' => '',
				'Caracteristica.referente' => 'S'
			)
		));

		return $caracteristicas;
	}

	private function getCaracteristicasCategoria($categoria_id, $caracteristicas = array()) {
		$categoria = $this->Categoria->find('first', array(
			'contain' => array(
				'CategoriaPai' => array(
					'conditions' => array(
						'Categoria.ativo'
					)
				),
				'Caracteristica' => array(
					'CaracteristicasValor' => array(
						'conditions' => array(
							'CaracteristicasValor.ativo' => true
						)
					),
					'conditions' => array(
						'Caracteristica.ativo',
						'Caracteristica.referente' => 'S',
						'Caracteristica.mercado_livre_caracteristica_id is null'
					)
				)
			),
			'conditions' => array(
				'Categoria.id' => $categoria_id,
				'Categoria.ativo'
			)
		));

		if ($categoria) {

			if (count($categoria['Caracteristica'])) {
				$caracteristicas = array_merge($caracteristicas, $categoria['Caracteristica']);
			}

			if ($categoria['Categoria']['categoria_pai_id']) {
				$caracteristicas = $this->getCaracteristicasCategoria($categoria['Categoria']['categoria_pai_id'], $caracteristicas);
			}

			return $caracteristicas;

		} else {
			return $caracteristicas;
		}

	}

	private function getCaracteristicasCategoriaMercadoLivre($categoria_id, $caracteristicas = array()) {
		$categoria = $this->Categoria->find('first', array(
			'contain' => array(
				'CategoriaPai' => array(
					'conditions' => array(
						'Categoria.ativo'
					)
				),
				'Caracteristica' => array(
					'CaracteristicasValor' => array(
						'conditions' => array(
							'CaracteristicasValor.ativo' => true
						)
					),
					'conditions' => array(
						'Caracteristica.ativo',
						'Caracteristica.referente' => 'S',
						'Caracteristica.mercado_livre_caracteristica_id !=' =>  ''
					)
				)
			),
			'conditions' => array(
				'Categoria.id' => $categoria_id,
				'Categoria.ativo'
			)
		));

		if ($categoria) {

			if (count($categoria['Caracteristica'])) {
				$caracteristicas = array_merge($caracteristicas, $categoria['Caracteristica']);
			}

			if ($categoria['Categoria']['categoria_pai_id']) {
				$caracteristicas = $this->getCaracteristicasCategoriaMercadoLivre($categoria['Categoria']['categoria_pai_id'], $caracteristicas);
			}

			return $caracteristicas;

		} else {
			return $caracteristicas;
		}

	}

	public function admin_excluir($id = null) {
		$this->Sku->id = $id;

		if (!$this->Sku->exists()) {
			throw new NotFoundException('Sku inexistente.');
		}

		$sku = $this->Sku->find('first', array(
			'contain' => false,
			'conditions' => array(
				'Sku.ativo' => true,
				'Sku.id' => $id
			)
		));

		if (!$sku['Sku']['destaque']) {
			if ($this->request->is('post')) {
				if ($this->Sku->saveField('ativo', false, false)) {
					$this->Session->setFlash('Sku desativada com sucesso.', FLASH_SUCCESS);
					$this->backToPaginatorIndex();
				} else {
					$this->Session->setFlash('Ocorreu um erro ao tentar desativar o sku. Por favor, tente novamente.', FLASH_ERROR);
				}
			}
		} else {
			$this->Session->setFlash('Este sku não pode ser desativado pois é um sku destaque.', FLASH_ERROR);
			$this->backToPaginatorIndex();
		}

	}

	public function admin_ativar($id = null) {
		$this->Sku->id = $id;

		if (!$this->Sku->exists()) {
			throw new NotFoundException('Sku inexistente.');
		}

		if ($this->request->is('post')) {

			if ($this->Sku->saveField('ativo', true, false)) {
				$this->Session->setFlash('Sku ativada com sucesso.', FLASH_SUCCESS);
				$this->backToPaginatorIndex();
			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar ativar o Sku. Por favor, tente novamente.', FLASH_ERROR);
			}
		}


	}

	private function getDiretorioProdutos() {
		$diretorio = $this->getDiretorioProjeto('produtos');
		new Folder($diretorio, true);

		return $diretorio;
	}

	public function admin_ajax_ordenar($produto_id = null) {

		$qtd_skus = $this->Sku->find('count', array(
			'conditions' => array(
				'Sku.produto_id'  => $produto_id
			),
		));

		$count_sku = 1;

		while ($count_sku <= $qtd_skus) {

			$this->request->data[$count_sku]['Sku']['id'] = $this->params['named']['ordem_' . $count_sku];
			$this->request->data[$count_sku]['Sku']['ordem'] = $count_sku;

			if ($count_sku == 1) {
				$this->request->data[$count_sku]['Sku']['destaque'] = true;
			} else {
				$this->request->data[$count_sku]['Sku']['destaque'] = false;
			}

			$count_sku++;
		}

		if ($this->Sku->saveMany($this->request->data, array('deep' => true))) {
			$json = array('sucesso' => true);
		} else {
			$json = array('sucesso' => false, 'mensagem' => 'Ocorreu um erro ao tentar ordenar os skus. Por favor, tente novamente.');
		}

		$this->renderJson($json);

	}

}
?>