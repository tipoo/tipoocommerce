<?php
App::uses('AppController', 'Controller');

class CamposComplementaresController extends AppController {

	public $uses = array('CamposComplementar', 'CamposComplementaresValoresSelecionado');

	public function admin_index() {

		$conditions = array(
			'CamposComplementar.ativo' => true
		);

		$this->paginate = array(
			'conditions' => $conditions,
			'limit' => Configure::read('Sistema.Paginacao.limit')
		);

		$this->set('campos_complementares', $this->paginate());
		$this->set('referencia', $this->getReferencia());
		$this->set('tipo', $this->getTipo());
	}

	public function admin_adicionar($id = null) {

		if ($this->request->is('post')) {

			$this->CamposComplementar->create();
			if ($this->CamposComplementar->save($this->request->data)) {
				$this->Session->setFlash('Característica salva com sucesso.', FLASH_SUCCESS);
				$this->backToPaginatorIndex();
			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar salvar a campo complementar. Por favor, tente novamente.', FLASH_ERROR);
			}
		}

		$this->set('referencia', $this->getReferencia());
		$this->set('tipo', $this->getTipo());
		$this->set('mascara', $this->getMascara());

	}

	public function admin_editar($id = null) {

		$campoComplementar = $this->CamposComplementar->find('first', array(
			'contain' => false,
			'conditions' => array(
				'CamposComplementar.ativo' => true,
				'CamposComplementar.id' => $id
			)
		));

		if ($campoComplementar && $id) {
			if ($this->request->is('post') || $this->request->is('put')) {

				$this->CamposComplementar->id = $id;
				if ($this->CamposComplementar->save($this->request->data)) {
					$this->Session->setFlash('Campo complementar salvo com sucesso.', FLASH_SUCCESS);
					$this->backToPaginatorIndex();
				} else {
					$this->Session->setFlash('Ocorreu um erro ao tentar salvar o campo complementar. Por favor, tente novamente.', FLASH_ERROR);
				}
			}

			$this->request->data = $this->CamposComplementar->read(null, $id);

			$this->set('referencia', $this->getReferencia());
			$this->set('tipo', $this->getTipo());
			$this->set('mascara', $this->getMascara());
			$this->set('campoComplementar', $campoComplementar);

		} else {
			$this->Session->setFlash('Campo Complementar inexistente.', FLASH_ERROR);
			$this->backToPaginatorIndex();
		}

	}


	public function admin_excluir($id = null) {
		$this->CamposComplementar->id = $id;

		if (!$this->CamposComplementar->exists()) {
			throw new NotFoundException('Campo Complementar inexistente.');
		}

		if ($this->request->is('post')) {

			/* Verifica se o valor já foi utilizado para poder excluir ou não a categorias */
			$campos_complementares_valores_selecionados = $this->CamposComplementaresValoresSelecionado->find('count', array(
				'conditions' => array(
					'CamposComplementaresValoresSelecionado.ativo' => true,
					'CamposComplementaresValoresSelecionado.campos_complementar_id' => $id
				)
			));

			if ($campos_complementares_valores_selecionados == 0) {
				if ($this->CamposComplementar->saveField('ativo', false, false)) {
					$this->Session->setFlash('Campo complementar excluída com sucesso.', FLASH_SUCCESS);
					$this->backToPaginatorIndex();
				} else {
					$this->Session->setFlash('Ocorreu um erro ao tentar excluir o campo complementar. Por favor, tente novamente.', FLASH_ERROR);
				}
			} else {
				$this->Session->setFlash('Não é possível exluir este campo complementar, pois ela já se encontra em uso.', FLASH_ERROR);
				$this->backToPaginatorIndex();
			}
		}
	}

	private function getReferencia() {
		return array(
			'PF' => 'Pessoa Física',
			'PJ' => 'Pessoa Jurídica',
			// 'P' => 'Pedido'
		);
	}


	private function getTipo() {
		return array(
			'T' => 'Texto',
			'TG' => 'Texto Grande',
			'L' => 'Lista',
			'N' => 'Número'
		);
	}

	private function getMascara() {
		return array(
			'I' => 'Inteiro',
			'D' => 'Decimal',
		);
	}

}
?>