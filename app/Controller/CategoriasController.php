<?php
App::uses('AppController', 'Controller');

class CategoriasController extends AppController {

	public $uses = array('Categoria', 'Colecao', 'ColecoesLocal', 'Slug', 'Produto');
	public $components = array('CustomTagsReader');

	public function admin_index($id = null) {

		$categoria = $this->Categoria->find('count', array(
			'conditions' => array(
				'Categoria.id' => $id
			)
		));

		if ($id && $categoria == 0) {
			$this->Session->setFlash('Não é possível cadastrar sub-categorias para este id.', FLASH_ERROR);
			$this->redirect(array('controller' => 'categorias'));
		}

		$contain = array(
			'Slug',
			'CategoriaFilha'
		);

		$conditions = array(
			//'Categoria.ativo' => true,
			'Categoria.categoria_pai_id' => $id
		);

/*		$controlesMenuCategorias = $this->Categoria->find('threaded', array(
			'contain' => $contain,
			'parent' => 'categoria_pai_id',
			'conditions' => $conditions,
			'order' => 'descricao',
			'limit' => 3,
		));
*/
		$this->paginate = array(
			'contain' => $contain,
			'conditions' => $conditions,
			'limit' => 50,
			'order' => 'descricao'
		);

		if ($id) {
			$this->set('voltar', true);
		} else {
			$this->set('voltar', false);
		}

		$this->set('id', $id);
		$this->set('categorias', $this->paginate());
	}

	public function admin_adicionar($id = null) {

		if ($this->request->is('post')) {

			if ($id) {
				$this->request->data['Categoria']['categoria_pai_id'] = $id;

				$categoriaPai = $this->Categoria->find('first', array(
					'contain' => array(
						'Slug'
					),
					'conditions' => array(
						//'Categoria.ativo' => true,
						'Categoria.id' => $id
					)
				));
			}

			/* Slug */

			$count_slug = 0;
			do {
				if ($count_slug == 0) {
					if ($id) {
						$slug = $categoriaPai['Slug']['url'] . '/' . Inflector::slug($this->request->data['Categoria']['descricao'], '-');
					} else {
						$slug = '/' . Inflector::slug($this->request->data['Categoria']['descricao'], '-');
					}
				} else {
					if ($id) {
						$slug = $categoriaPai['Slug']['url'] . '/' . Inflector::slug($this->request->data['Categoria']['descricao'], '-') . '-' . $count_slug;
					} else {
						$slug = '/' . Inflector::slug($this->request->data['Categoria']['descricao'], '-') . '-' . $count_slug;
					}
				}
				$slug_find = $this->Slug->find('first', array(
					'contain' => false,
					'conditions' => array(
						'Slug.url' => $slug
					)
				));

				$count_slug++;

			} while ($slug_find);

			$this->request->data['Slug']['url'] = strtolower($slug);

			$this->request->data['Slug']['controller'] = 'catalogo';
			$this->request->data['Slug']['action'] = 'categoria';
			$this->request->data['Slug']['custom_parser_class'] = 'CategoriaParser';

			/* Slug - Fim */

			/* Mercado Livre - Buscar atributos */
			if ($this->Configuracao->get('mercado_livre_ativo') && isset($this->request->data['Categoria']['mercado_livre_categoria_tipo_atributo']) && $this->request->data['Categoria']['mercado_livre_categoria_tipo_atributo'] == 'variations') {
				$mercado_livre_variacoes = $this->MercadoLivreCategorias->get_variacoes($this->request->data['Categoria']['mercado_livre_categoria_id']);

				foreach ($mercado_livre_variacoes as $key_variacao => $mercado_livre_variacao) {
					$this->request->data['Caracteristica'][$key_variacao]['tipo'] = 'L';
					$this->request->data['Caracteristica'][$key_variacao]['referente'] = 'S';
					$this->request->data['Caracteristica'][$key_variacao]['mercado_livre_caracteristica_id'] = $mercado_livre_variacao->id;
					$this->request->data['Caracteristica'][$key_variacao]['mercado_livre_caracteristica_descricao'] = $mercado_livre_variacao->name;
					if (isset($mercado_livre_variacao->tags->required)) {
						$this->request->data['Caracteristica'][$key_variacao]['mercado_livre_caracteristica_campo_obrigatorio'] = $mercado_livre_variacao->tags->required;
					}

					foreach ($mercado_livre_variacao->values as $key_variacao_valor => $mercado_livre_variacao_valor) {
						$this->request->data['Caracteristica'][$key_variacao]['CaracteristicasValor'][$key_variacao_valor]['mercado_livre_caracteristicas_valor_id'] = $mercado_livre_variacao_valor->id;
						$this->request->data['Caracteristica'][$key_variacao]['CaracteristicasValor'][$key_variacao_valor]['mercado_livre_caracteristicas_valor_descricao'] = $mercado_livre_variacao_valor->name;
					}
				}
			}
			/* Mercado Livre - Fim */

			$this->Categoria->create();

			if ($this->Categoria->saveAll($this->request->data, array('deep' => true))) {

				if ($id) {
					$this->Categoria->id = $id;
					if ($this->Categoria->saveField('descendente', 'C', false)) {
						$this->Session->setFlash('Categoria salva com sucesso.', FLASH_SUCCESS);
						$this->backToPaginatorIndex();
					} else {
						$this->Session->setFlash('Ocorreu um erro ao tentar salvar a categoria. Por favor, tente novamente.', FLASH_ERROR);
					}
				} else {
					$this->Session->setFlash('Categoria salva com sucesso.', FLASH_SUCCESS);
					$this->backToPaginatorIndex();
				}

			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar salvar a categoria. Por favor, tente novamente.', FLASH_ERROR);
			}
		}

		if ($this->Configuracao->get('mercado_livre_ativo')) {
			$this->MercadoLivreCategorias->set_categorias();
		}

	}

	public function admin_editar($id = null) {

		$this->Categoria->id = $id;

		$categoria = $this->Categoria->find('first', array(
			'contain' => array(
				'Slug'
			),
			'conditions' => array(
				'Categoria.id' => $id
			)
		));

/*		$slug_completo = explode('/', $categoria['Slug']['url']);
		$slug_editavel = explode('/', $categoria['Slug']['url'], (count($slug_completo)));
		$slug_nao_editavel = explode('/', $categoria['Slug']['url'], -1);


		$url_nao_editavel = null;
		foreach ($slug_nao_editavel as $nao_editavel) {

			if ($nao_editavel != '') {
				$url_nao_editavel .= '/' . $nao_editavel;
			}

		}
		$url_editavel = $slug_editavel[count($slug_completo)-1];

		$this->set('url_nao_editavel', $url_nao_editavel);
		$this->set('url_editavel', $url_editavel);
		$this->set('slug_completo' ,$slug_completo);*/


		if (!$this->Categoria->exists()) {
			throw new NotFoundException('Categoria inexistente.');
		}

		if ($this->request->is('post') || $this->request->is('put')) {

/*			$this->request->data['Slug']['url'] = '/' . Inflector::slug($this->request->data['Slug']['url'], '-');
			$this->request->data['Slug']['url'] = strtolower($this->request->data['Slug']['url']);

			$this->request->data['Slug']['url'] = $url_nao_editavel . $this->request->data['Slug']['url'];

			$verificaSlug = $this->Slug->find('first', array(
				'conditions' => array(
					'Slug.url' => $this->request->data['Slug']['url']
				)
			));

			if(count($verificaSlug) == 0 || $verificaSlug['Slug']['id'] == $categoria['Categoria']['slug_id']){

				$this->Slug->id = $categoria['Categoria']['slug_id'];
				$this->Slug->saveField('url', $this->request->data['Slug']['url'], false);*/

				/* Mercado Livre - Buscar atributos */
				if ($this->Configuracao->get('mercado_livre_ativo') && isset($this->request->data['Categoria']['mercado_livre_categoria_tipo_atributo']) && $this->request->data['Categoria']['mercado_livre_categoria_tipo_atributo'] == 'variations') {
					$mercado_livre_variacoes = $this->MercadoLivreCategorias->get_variacoes($this->request->data['Categoria']['mercado_livre_categoria_id']);

					foreach ($mercado_livre_variacoes as $key_variacao => $mercado_livre_variacao) {
						$this->request->data['Caracteristica'][$key_variacao]['tipo'] = 'L';
						$this->request->data['Caracteristica'][$key_variacao]['referente'] = 'S';
						$this->request->data['Caracteristica'][$key_variacao]['mercado_livre_caracteristica_id'] = $mercado_livre_variacao->id;
						$this->request->data['Caracteristica'][$key_variacao]['mercado_livre_caracteristica_descricao'] = $mercado_livre_variacao->name;
						if (isset($mercado_livre_variacao->tags->required)) {
							$this->request->data['Caracteristica'][$key_variacao]['mercado_livre_caracteristica_campo_obrigatorio'] = $mercado_livre_variacao->tags->required;
						}

						foreach ($mercado_livre_variacao->values as $key_variacao_valor => $mercado_livre_variacao_valor) {
							$this->request->data['Caracteristica'][$key_variacao]['CaracteristicasValor'][$key_variacao_valor]['mercado_livre_caracteristicas_valor_id'] = $mercado_livre_variacao_valor->id;
							$this->request->data['Caracteristica'][$key_variacao]['CaracteristicasValor'][$key_variacao_valor]['mercado_livre_caracteristicas_valor_descricao'] = $mercado_livre_variacao_valor->name;
						}
					}
				}
				/* Mercado Livre - Fim */

				if ($this->Categoria->saveAll($this->request->data, array('deep' => true))) {
					$this->Session->setFlash('Categoria editada com sucesso.', FLASH_SUCCESS);
					$this->backToPaginatorIndex();
				} else {
					$this->Session->setFlash('Ocorreu um erro ao tentar editar a categoria. Por favor, tente novamente.', FLASH_ERROR);
				}

/*			} else {
				$this->Session->setFlash('Já existe uma url  "<strong>' .  $this->request->data['Slug']['url']  . '</strong>". Por favor, cadastre uma url diferente.', FLASH_ERROR);
				$this->request->data['Slug']['url'] = $categoria['Slug']['url'];
			}*/
		} else {
			$this->request->data = $this->Categoria->read(null, $id);

			if ($this->Configuracao->get('mercado_livre_ativo')) {
				$this->MercadoLivreCategorias->set_categorias();
			}
		}
	}

	public function admin_excluir($id = null) {

		$this->Categoria->id = $id;

		if (!$this->Categoria->exists()) {
			throw new NotFoundException('Categoria inexistente.');
		}

		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Categoria->saveField('ativo', false, false)) {

				$categoria = $this->Categoria->find('first', array(
					'contain' => array(
						'Slug'
					),
					'conditions' => array(
						//'Categoria.ativo' => false,
						'Categoria.id' => $id
					)
				));

				$produtos_categoria = $this->Produto->find('all', array(
					'contain' => array(
						'Slug'
					),
					'conditions' => array(
						'Produto.ativo' => true,
						'Produto.categoria_id' => $id
					)

				));

				foreach ($produtos_categoria as $produto_categoria) {
					$this->Produto->id = $produto_categoria['Produto']['id'];
					$this->Produto->saveField('ativo', false, false);

					$this->Slug->id = $produto_categoria['Slug']['id'];
					$this->Slug->saveField('ativo', false, false);
				}

				$this->Slug->id = $categoria['Slug']['id'];
				if ($this->Slug->saveField('ativo', false, false)) {
					$this->Session->setFlash('Categoria desativada com sucesso.', FLASH_SUCCESS);

/*					if ($categoria['Categoria']['descendente'] == 'P') {
						$this->Session->setFlash('Os produtos que pertencem a essa categoria também foram desativados.', FLASH_WARNING);
					}*/

					$this->redirect($this->referer());
					//$this->backToPaginatorIndex();
				} else {
					$this->Session->setFlash('Ocorreu um erro ao tentar desativar a url da Categoria. Por favor, informe nossa equipe sobre o ocorrido.', FLASH_ERROR);
				}
			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar desativar a Categoria. Por favor, tente novamente.', FLASH_ERROR);
			}
		}
	}

	public function admin_ativar($id = null) {
		$this->Categoria->id = $id;

		if (!$this->Categoria->exists()) {
			throw new NotFoundException('Categoria inexistente.');
		}

		if ($this->request->is('post')) {

			if ($this->Categoria->saveField('ativo', true, false)) {

				$categoria = $this->Categoria->find('first', array(
					'contain' => array(
						'Slug'
					),
					'conditions' => array(
						//'Categoria.ativo' => false,
						'Categoria.id' => $id
					)
				));

				$this->Slug->id = $categoria['Slug']['id'];
				$this->Slug->saveField('ativo', true, false);

				$this->Session->setFlash('Categoria ativada com sucesso.', FLASH_SUCCESS);
				$this->backToPaginatorIndex();
			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar ativar a Categoria. Por favor, tente novamente.', FLASH_ERROR);
			}
		}

	}

	public function admin_ajax_buscar_sub_categorias($categoria_pai_id) {
		$sub_categorias = $this->MercadoLivreCategorias->get_sub_categorias($categoria_pai_id);

		$json = array('sucesso' => true, 'sub_categorias' => $sub_categorias['children_categories'], 'tipo_atributo' => $sub_categorias['attribute_types']);

		$this->renderJson($json);
	}

/**
 * Overridden paginate method - group by week, away_team_id and home_team_id
 */

/*	private function paginateThreaded($contain = array(), $conditions = array(), $fields = array(), $order = array(), $limit = array(), $page = 1, $recursive = null, $extra = array()) {
	    $recursive = -1;
	   // $group = null;
	    return $this->Categoria->find(
	        'threaded',
	        compact('contain', 'conditions', 'fields', 'order', 'limit', 'page', 'recursive')
	    );
	}*/
}
?>