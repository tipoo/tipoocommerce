<?php
App::uses('AppController', 'Controller');

class NewslettersController extends AppController {

	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('enviar');
	}

	public function enviar($id = null) {

		if ($this->request->is('post')) {

			//if ($this->Newsletter->validates()) {
				$verificaNewsletter = $this->Newsletter->find('first', array(
					'conditions' => array(
						'Newsletter.email' => $this->request->data['Newsletter']['email']
					)
				));

				if (count($verificaNewsletter) == 0) {

					$this->Newsletter->create();
					if ($this->Newsletter->save($this->request->data)) {
						//$this->Session->setFlash('Newsletter enviada com sucesso.', FLASH_SUCCESS, array(), 'newsletter');
						$this->redirect($this->referer());
					} else {
						$this->Session->setFlash('Ocorreu um erro ao tentar salvar a newsletter. Por favor, tente novamente.', FLASH_ERROR, array(), 'newsletter');
					}
				} else {
					$this->Newsletter->id = $verificaNewsletter['Newsletter']['id'];
					$nome = $this->request->data['Newsletter']['nome'];
					if ($this->Newsletter->saveField('nome', $nome, false)) {
						$this->redirect($this->referer());
					} else {
						$this->Session->setFlash('Ocorreu um erro ao tentar salvar a newsletter. Por favor, tente novamente.', FLASH_ERROR, array(), 'newsletter');
					}
				}
			//}
		}
	}

	public function admin_index() {

		$de = date('Y-m-d', strtotime('-31 day'));
		if (isset($this->request->params['named']['de'])) {
			$de = $this->request->params['named']['de'];
		}

		$ate = date('Y-m-d');
		if (isset($this->request->params['named']['ate'])) {
			$ate = $this->request->params['named']['ate'];
		}

		$conditions = array(
			'Newsletter.ativo' =>true
	    );

		$data_inicio = null;
		if (isset($this->params['named']['data_inicio'])) {
			$data_inicio = $this->params['named']['data_inicio'];
		}

		$data_fim = null;
		if (isset($this->params['named']['data_fim'])) {
			$data_fim = $this->params['named']['data_fim'];
		}

		$conditions = array(
			'Newsletter.created >=' => $de . ' 00:00:00',
			'Newsletter.created <=' => $ate . ' 23:59:59',
		);

		$this->paginate = array(
			'conditions' => $conditions,
			'limit' => 50,
			'order' => array(
				'Newsletter.created' => 'DESC'
			)
		);

		$total_cadastrados = $this->Newsletter->find('count',array(
			'conditions' => $conditions,
			'order' => array(
				'Newsletter.created' => 'DESC'
			)
		));

		$this->set('de', $de);
		$this->set('ate', $ate);

		$this->set('newsletters', $this->paginate());
		$this->set('total_cadastrados', $total_cadastrados);
	}

	public function admin_baixar_csv() {

		$this->layout = 'csv';

		$filename = 'newsletter.csv';

		header('Content-type: application/csv');
		header('Content-Disposition: attachment; filename="'.$filename.'"');

		$csv_file = fopen('php://output', 'w');

		$de = date('Y') . '-' . date('m') . '-01';
		if (isset($this->request->params['named']['de'])) {
			$de = $this->request->params['named']['de'];
		}

		$ate = date('Y-m-t');
		if (isset($this->request->params['named']['ate'])) {
			$ate = $this->request->params['named']['ate'];
		}

		$conditions = array(
			'Newsletter.ativo' =>true
	    );


		$conditions = array(
			'Newsletter.created >=' => $de . ' 00:00:00',
			'Newsletter.created <=' => $ate . ' 23:59:59',
		);

		$newsletters = $this->Newsletter->find('all', array(
			'contain' => false,
			'order' => array(
				'Newsletter.nome' => 'ASC'
			),
			'fields' => array('id', 'nome', 'email', 'created'),
			'conditions' => $conditions,
		));

		$header_row = array("id", "nome", "email", "data");
	    fputcsv($csv_file,$header_row,';','"');


		foreach($newsletters as $newsletter) {

			$row = array(
				$newsletter['Newsletter']['id'],
				$newsletter['Newsletter']['nome'],
				$newsletter['Newsletter']['email'],
				$newsletter['Newsletter']['created']
			);

			fputcsv($csv_file,$row,';', '"');
		}

		fclose($csv_file);

	}
}

?>