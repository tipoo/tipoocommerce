<?php
App::uses('AppController', 'Controller');

class ApiController extends AppController {
	public function beforeFilter() {
		parent::beforeFilter();
	}

	public function api_index() {
		$this->load_component();
		$this->{$this->get_component_name()}->index();
	}

	public function api_add() {
		$this->load_component();
		$this->{$this->get_component_name()}->add();
	}

	public function api_view($id) {
		$this->load_component();
		$this->{$this->get_component_name()}->view($id);
	}

	public function api_edit($id) {
		$this->load_component();
		$this->{$this->get_component_name()}->edit($id);
	}

	public function api_delete($id) {
		$this->load_component();
		$this->{$this->get_component_name()}->delete($id);
	}

	private function load_component() {
		$component_name = $this->get_component_name();
		$this->{$component_name} = $this->Components->load($component_name);
		$this->{$component_name}->initialize($this);
	}

	private function get_component_name() {
		return 'Api' . $this->normalize_controller_name();
	}

	private function normalize_controller_name() {
		return ucfirst($this->params['controller']);
	}

}
?>