<?php
App::uses('AppController', 'Controller');

class ClientesController extends AppController {

	public $uses = array('Cliente', 'Logradouro', 'Cidade', 'Endereco', 'Pedido', 'Estado', 'CamposComplementar', 'CamposComplementaresValoresSelecionado');

	public $components = array(
		'CustomTagsParser' => array('actions' => array('cadastrar', 'painel', 'meus_dados', 'meus_enderecos', 'adicionar_endereco', 'alterar_endereco', 'meus_pedidos', 'pedido', 'login', 'esqueci_minha_senha'))
	);

	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('cadastrar', 'ajax_buscar_cep', 'login', 'logout', 'esqueci_minha_senha');

		if (!$this->isPrefix('admin')) {
			$this->layout = 'cadastro';
		}

	}

	public function admin_index() {

		$estados = $this->Estado->find('list', array(
				'fields' => array(
					'Estado.id',
					'Estado.nome'
				)
		));

		$tipo_pessoa = null;
		if (isset($this->params['named']['tipo_pessoa'])) {
			$tipo_pessoa = $this->params['named']['tipo_pessoa'];
			$this->request->data['Filtro']['tipo_pessoa'] = $tipo_pessoa;
		}

		$nome = null;
		if (isset($this->params['named']['nome'])) {
			$nome = $this->params['named']['nome'];
			$this->request->data['Filtro']['nome'] = $nome;
		}

		$status_cliente = null;
		if (isset($this->params['named']['status_cliente'])) {
			$status_cliente = $this->params['named']['status_cliente'];
			$this->request->data['Filtro']['status_cliente'] = $status_cliente;
		}

		$email = null;
		if (isset($this->params['named']['email'])) {
			$email = $this->params['named']['email'];
			$this->request->data['Filtro']['email'] = $email;
		}

		$genero = null;
		if (isset($this->params['named']['genero'])) {
			$genero = $this->params['named']['genero'];
			$this->request->data['Filtro']['genero'] = $genero;
		}

		$estado_id = null;
		if (isset($this->params['named']['estado_id'])) {
			$estado_id = $this->params['named']['estado_id'];
			$this->request->data['Filtro']['estado_id'] = $estado_id;
		}

		$cpf = null;
		if (isset($this->request->params['named']['cpf'])) {
			$cpf = $this->request->params['named']['cpf'];
			$this->request->data['Filtro']['cpf'] = $cpf;
		}

		$cnpj = null;
		if (isset($this->request->params['named']['cnpj'])) {
			$cnpj = $this->request->params['named']['cnpj'];
			$cnpj = explode(',', $cnpj);
			$cnpj = $cnpj[0] . '/' . $cnpj[1];
			$this->request->data['Filtro']['cnpj'] = $cnpj;

		}

		$cidade_id = null;
		if (isset($this->request->params['named']['cidade_id'])) {
			$cidade_id = $this->request->params['named']['cidade_id'];
			$this->request->data['Filtro']['cidade_id'] = $cidade_id;
		}

		$cep = null;
		if (isset($this->params['named']['cep'])) {
			$cep = $this->params['named']['cep'];
			$this->request->data['Filtro']['cep'] = $cep;
		}

		$receber_newsletter = null;
		if (isset($this->params['named']['receber_newsletter'])) {
			$receber_newsletter = $this->params['named']['receber_newsletter'];
			$this->request->data['Filtro']['receber_newsletter'] = $receber_newsletter;
		}

		$dia_aniversario = null;
		if (isset($this->params['named']['dia_aniversario'])) {
			$dia_aniversario = $this->params['named']['dia_aniversario'];
			$this->request->data['Filtro']['dia_aniversario'] = $dia_aniversario;
		}

		$mes_aniversario = null;
		if (isset($this->params['named']['mes_aniversario'])) {
			$mes_aniversario = $this->params['named']['mes_aniversario'];
			$this->request->data['Filtro']['mes_aniversario'] = $mes_aniversario;
		}

		$ano_aniversario = null;
		if (isset($this->params['named']['ano_aniversario'])) {
			$ano_aniversario = $this->params['named']['ano_aniversario'];
			$this->request->data['Filtro']['ano_aniversario'] = $ano_aniversario;
		}

		$conditions = $this->filtro($nome, $tipo_pessoa, $status_cliente, $email, $genero, $estado_id, $cidade_id, $cnpj, $cpf, $cep, $receber_newsletter, $dia_aniversario, $mes_aniversario, $ano_aniversario);


		$this->paginate = array(
			'contain' => 'Endereco.Cidade',
			'conditions' => $conditions,
			'limit' => 50,
			'order' => array(
				'Cliente.nome' => 'ASC',
			)
		);

		$this->set('clientes', $this->paginate());
		$this->set('sexos', $this->getSexos());
		$this->set('tipo_pessoa', $this->getTipoPessoa());
		$this->set('status_cliente', $this->getStatusCliente());
		$this->set('receber_newsletter', $this->getEmailOfertas());
		$this->set('mes_aniversario', $this->getMesAniversario());
		$this->set('dia_aniversario', $this->getDiaAniversario());
		$this->set('ano_aniversario', $this->getAnoAniversario());
		$this->set('estados', $estados);
	}

	public function admin_desativar($id = null){

		$this->Cliente->id = $id;

		if (!$this->Cliente->exists()) {
			throw new NotFoundException('Cliente inexistente.');
		}

		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Cliente->saveField('ativo', false, false)) {
				$this->Session->setFlash('Cliente desativado com sucesso.', FLASH_SUCCESS);
				$this->backToPaginatorIndex();
			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar desativar o Cliente. Por favor, tente novamente.', FLASH_ERROR);
			}
		}

	}

	public function admin_ativar($id = null){

		$this->Cliente->id = $id;

		if (!$this->Cliente->exists()) {
			throw new NotFoundException('Cliente inexistente.');
		}

		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Cliente->saveField('ativo', true, false)) {
				$this->Session->setFlash('Cliente ativado com sucesso.', FLASH_SUCCESS);
				$this->backToPaginatorIndex();
			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar ativar o Cliente. Por favor, tente novamente.', FLASH_ERROR);
			}
		}
	}

	public function admin_ajax_visualizar($id = null) {

		$cliente = $this->Cliente->find('first', array(
			'contain' => array(
				'CamposComplementaresValoresSelecionado' => array(
					'CamposComplementar',
					'CamposComplementaresValor'
				),
				'Endereco.Cidade',
				'Pedido' => array(
					'PedidoSku.PedidoCaracteristica',
					'PedidoSku.CompreJuntoPromocao',
					'Promocao.PromocaoCausa',
					'Promocao.PromocaoEfeito',
					'PedidoSku.Sku.Produto',
					'PedidoSku.Promocao',
					'PedidoSku.PedidoSkuServico',
					'StatusPedido',
					'order' => array(
						'Pedido.created' => 'DESC'
					)
				)

			),
			'conditions' => array(
				//'Cliente.ativo' => true,
				'Cliente.id' => $id,
			),
			'limit' => 50,
			'order' => array(
				'Cliente.created' => 'DESC'
			)
		));

		$this->set('cliente', $cliente);
		$this->set('sexos', $this->getSexos());
	}

	public function cadastrar() {
		if ($this->request->is('post')) {

			// Verificando se o cliente já existe

			if ($this->request->data('Cliente.tipo_pessoa') == 'F') {
				$or = array(
					'Cliente.cpf' => $this->request->data('Cliente.cpf'),
					'Cliente.email' => $this->request->data('Cliente.email')
				);

				// Tira os espaços em branco e for igual a dois
				$this->request->data['Cliente']['nome'] = preg_replace('/\s\s+/', ' ', $this->request->data['Cliente']['nome']);
			} else {
				$or = array(
					'Cliente.cnpj' => $this->request->data('Cliente.cnpj'),
					'Cliente.email' => $this->request->data('Cliente.email')
				);

				// Tira os espaços em branco e for igual a dois
				$this->request->data['Cliente']['nome_fantasia'] = preg_replace('/\s\s+/', ' ', $this->request->data['Cliente']['nome_fantasia']);
			}

			$clienteExistente = $this->Cliente->find('first', array(
				'contain' => false,
				'conditions' => array(
					'OR' => $or,
					'Cliente.ativo'
				)
			));

			if (!$clienteExistente) {
				// Adicionando o perfil de cliente aos dados enviados pelo formulario
				$this->request->data('Cliente.perfil_id', 1);

				// Limpa do array os valores vazios
				if (isset($this->request->data['CamposComplementaresValoresSelecionado'])) {
					foreach ($this->request->data['CamposComplementaresValoresSelecionado'] as $key => $campo_complementar_valor_selecionado) {
						if ($campo_complementar_valor_selecionado['campos_complementar_tipo'] == 'L') {
							if ($campo_complementar_valor_selecionado['campos_complementares_valor_id'] == '') {
								unset($this->request->data['CamposComplementaresValoresSelecionado'][$key]);
							}
						} else if ($campo_complementar_valor_selecionado['campos_complementar_tipo'] == 'TG') {
							if ($campo_complementar_valor_selecionado['descricao_grande'] == '') {
								unset($this->request->data['CamposComplementaresValoresSelecionado'][$key]);
							}
						} else {
							if ($campo_complementar_valor_selecionado['descricao'] == '') {
								unset($this->request->data['CamposComplementaresValoresSelecionado'][$key]);
							}
						}
					}
				}

				// Fim - Limpa do array os valores vazios

				$this->Cliente->create();
				if ($this->Cliente->saveAssociated($this->request->data)) {
					$clienteAutenticado = $this->Cliente->find('first', array(
						'contain' => false,
						'conditions' => array(
							'Cliente.id' => $this->Cliente->id
						)
					));

					$this->Auth->login($clienteAutenticado['Cliente']);

					//$this->Session->setFlash('Cadastro efetuado com sucesso.', FLASH_SUCCESS);
					// Força a entrada na entrega se vier do checkout
					if ($this->params->named['return'] == 'checkout') {
						$this->redirect(array('controller' => 'checkout', 'action' => 'entrega'));
					} else {
						// Verificando se foi agendada alguma URL para ser redirecionada
						if (!$this->Redirect->redirect()) {
							$this->redirect(array('controller' => $this->params->named['return'], 'action' => 'login'));
						}
					}

				}
			} else {

				if ($clienteExistente['Cliente']['mercado_livre_cliente_id'] != '') {

					$this->request->data['Cliente']['id'] = $clienteExistente['Cliente']['id'];
					$this->Cliente->id = $clienteExistente['Cliente']['id'];

					if ($this->Cliente->saveAssociated($this->request->data)) {
						$clienteAutenticado = $this->Cliente->find('first', array(
							'contain' => false,
							'conditions' => array(
								'Cliente.id' => $this->Cliente->id
							)
						));

						$this->Auth->login($clienteAutenticado['Cliente']);

						// Força a entrada na entrega se vier do checkout
						if ($this->params->named['return'] == 'checkout') {
							$this->redirect(array('controller' => 'checkout', 'action' => 'entrega'));
						} else {
							// Verificando se foi agendada alguma URL para ser redirecionada
							if (!$this->Redirect->redirect()) {
								$this->redirect(array('controller' => $this->params->named['return'], 'action' => 'login'));
							}
						}

					}
				} else {
					$this->Session->setFlash('Você já é um cliente cadastrado em nossa loja. Efetue login através do formulário abaixo ou utilize o link "Esqueci minha senha" para recupera-la.', FLASH_ERROR, array(), 'cliente_ja_cadastrado');
					$this->redirect(array('controller' => $this->params->named['return'], 'action' => 'login'));
				}
			}

		}

		$campos_complementares = $this->CamposComplementar->find('all', array(
			'contain' => array(
				'CamposComplementaresValor' => array(
					'conditions' => array(
						'CamposComplementaresValor.ativo' => true
					)
				)
			),
			'conditions' => array(
				'CamposComplementar.ativo' => true,
				'OR' => array(
					'CamposComplementar.ativo' => 'PF',
					'CamposComplementar.ativo' => 'Pj'
				)
			)
		));

		$this->set('campos_complementares', $campos_complementares);
		$this->set('sexos', $this->getSexos());
	}

	public function ajax_buscar_cep($cep) {
		if ($cep) {

			// O cep pode ser por logradouro ou cidade
			$logradouro = $this->Logradouro->find('first', array(
				'contain' => array(
					'Bairro' => array('nome'),
					'Cidade' => array('nome'),
					'Estado' => array('nome'),
				),
				'conditions' => array('Logradouro.cep' => $cep)
			));

			if ($logradouro) {
				$json = array(
					'sucesso' => true,
					'cidade_id' => $logradouro['Logradouro']['cidade_id'],
					'estado_id' => $logradouro['Logradouro']['estado_id'],
					'cidade' => $logradouro['Cidade']['nome'],
					'estado' => $logradouro['Estado']['nome'],
					'bairro' => $logradouro['Bairro']['nome'],
					'endereco' => $logradouro['Logradouro']['nomeclog']
				);
			} else {
				$cidade = $this->Cidade->find('first', array(
					'contain' => array(
						'Estado' => array('nome')
					),
					'conditions' => array('Cidade.cep' => $cep)
				));

				if ($cidade) {
					$json = array(
						'sucesso' => true,
						'cidade_id' => $cidade['Cidade']['id'],
						'estado_id' => $cidade['Cidade']['estado_id'],
						'cidade' => $cidade['Cidade']['nome'],
						'estado' => $cidade['Estado']['nome'],
						'bairro' => '',
						'endereco' => ''
					);
				} else {
					$json = array('sucesso' => false, 'mensagem' => 'CEP não encontrado.');
				}
			}
		} else {
			$json = array('sucesso' => false, 'mensagem' => 'CEP inválido.');
		}

		$this->renderJson($json);

	}

	public function painel() {
		$this->layout = 'painel';
	}

	public function notafiscal($pedido_id = null) {
		$this->layout = 'clean';

		$pedido = $this->Pedido->find('first', array(
			'contain' => false,
			'conditions' => array(
				'Pedido.id' => $pedido_id,
				'Pedido.cliente_id' => $this->usuarioAutenticado['id']
			)
		));

		if (count($pedido) == 0) {
			$this->redirect(array('controller' => 'clientes', 'action' => 'meus_pedidos'));
		} else {
			if ($pedido['Pedido']['bling_danfe'] != '') {

				$nota_fiscal = json_decode($pedido['Pedido']['bling_danfe']);

				if (isset($nota_fiscal->retorno->notasfiscais[0]->notafiscal->linkDanfe) && $nota_fiscal->retorno->notasfiscais[0]->notafiscal->linkDanfe) {
					$this->set('url', $nota_fiscal->retorno->notasfiscais[0]->notafiscal->linkDanfe);
				} else {
					$this->redirect(array('controller' => 'clientes', 'action' => 'pedido', $pedido_id));
				}
			} else {
				$this->redirect(array('controller' => 'clientes', 'action' => 'pedido', $pedido_id));
			}

		}
	}

	public function meus_dados() {
		$this->layout = 'painel';

		$cliente = $this->Cliente->find('first', array(
			// 'contain' => array(

			// )
			'conditions' => array(
				'Cliente.id' => $this->usuarioAutenticado['id']
			)
		));

		if ($this->request->is('post') || $this->request->is('put')) {
			$this->Cliente->id = $this->usuarioAutenticado['id'];

			$this->request->data['Cliente']['nome'] = preg_replace('/\s\s+/', ' ', $this->request->data['Cliente']['nome']);

			$senhaValida = true;

			if (isset($this->request->data['Cliente']['senha_atual'])) {
				$senhaAtual = AuthComponent::password($this->request->data['Cliente']['senha_atual']);

				if ($cliente['Cliente']['senha'] == $senhaAtual) {
					$senhaValida = true;
				} else {
					$senhaValida = false;
				}
			}

			if ($senhaValida) {

				$this->CamposComplementaresValoresSelecionado->deleteAll(array('CamposComplementaresValoresSelecionado.cliente_id' => $this->usuarioAutenticado['id']));

				// Limpa do array os valores vazios
				if (isset($this->request->data['CamposComplementaresValoresSelecionado'])) {
					foreach ($this->request->data['CamposComplementaresValoresSelecionado'] as $key => $campo_complementar_valor_selecionado) {
						if ($campo_complementar_valor_selecionado['campos_complementar_tipo'] == 'L') {
							if ($campo_complementar_valor_selecionado['campos_complementares_valor_id'] == '') {
								unset($this->request->data['CamposComplementaresValoresSelecionado'][$key]);
							}
						} else if ($campo_complementar_valor_selecionado['campos_complementar_tipo'] == 'TG') {
							if ($campo_complementar_valor_selecionado['descricao_grande'] == '') {
								unset($this->request->data['CamposComplementaresValoresSelecionado'][$key]);
							}
						} else {
							if ($campo_complementar_valor_selecionado['descricao'] == '') {
								unset($this->request->data['CamposComplementaresValoresSelecionado'][$key]);
							}
						}
					}
				}

				// Fim - Limpa do array os valores vazios

            	if ($this->Cliente->saveAll($this->request->data)) {
					$this->Session->setFlash('Dados alterados com sucesso.', FLASH_SUCCESS, array(), 'meus_dados');
					$this->redirect($this->referer());
				} else {
					$this->Session->setFlash('Ocorreu um erro ao tentar alterar seus dados. Por favor, tente novamente.', FLASH_ERROR, array(), 'meus_dados');
				}

            } else {
            	$this->Session->setFlash('Senha atual incorreta.', FLASH_ERROR, array(), 'meus_dados');
            }
		}

		$campos_complementares_valores_selecionados = array();
		foreach ($cliente['CamposComplementaresValoresSelecionado'] as $key => $caracteristicas_valores_selecionado) {
			$campos_complementares_valores_selecionados[$caracteristicas_valores_selecionado['campos_complementar_id']] = $caracteristicas_valores_selecionado;
		}

		$cliente['CamposComplementaresValoresSelecionado'] = $campos_complementares_valores_selecionados;

		unset($cliente['Cliente']['senha']);

		$campos_complementares = $this->CamposComplementar->find('all', array(
			'contain' => array(
				'CamposComplementaresValor' => array(
					'conditions' => array(
						'CamposComplementaresValor.ativo' => true
					)
				)
			),
			'conditions' => array(
				'CamposComplementar.ativo' => true,
				'OR' => array(
					'CamposComplementar.ativo' => 'PF',
					'CamposComplementar.ativo' => 'Pj'
				)
			)
		));

		$this->set('campos_complementares', $campos_complementares);

		$this->set('sexos', $this->getSexos());
		$this->request->data = $cliente;
	}

	public function meus_enderecos() {
		$this->layout = 'painel';

		$enderecos = $this->Endereco->find('all', array(
			'contain' => array(
				'Cidade' => array('nome', 'uf'),
				'Cidade.Estado.nome'
			),
			'conditions' => array(
				'Endereco.ativo',
				'Endereco.cliente_id' => $this->usuarioAutenticado['id']
			),
			'order' => array(
				'Endereco.created' => 'DESC'
			)
		));

		$this->set(compact('enderecos'));
	}

	public function adicionar_endereco() {
		if ($this->request->is('post') || $this->request->is('put')) {
			$this->request->data('Endereco.cliente_id', $this->usuarioAutenticado['id']);
			$this->Endereco->create();
			if ($this->Endereco->save($this->request->data)) {
				$this->Session->setFlash('Endereço adicionado com sucesso.', FLASH_SUCCESS, array(), 'endereco');
				// Verificando se foi agendada alguma URL para ser redirecionada
				if (!$this->Redirect->redirect()) {
					$this->redirect($this->referer());
				}

			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar alterar o endereço. Por favor, tente novamente.', FLASH_ERROR, array(), 'endereco');
			}
		}
	}

	public function alterar_endereco($id = null) {
		$this->layout = 'painel';

		if (!$id) {
			$this->Session->setFlash('Endereço não encontrado.', FLASH_ERROR);
			$this->redirect(array('controller' => 'clientes', 'action' => 'meus_enderecos'));
		}

		$endereco = $this->Endereco->find('first', array(
			'contain' => array(
				'Cidade.nome',
				'Cidade.Estado.nome'
			),
			'conditions' => array(
				'Endereco.ativo',
				'Endereco.id' => $id,
				'Endereco.cliente_id' => $this->usuarioAutenticado['id']
			)
		));

		if (!$endereco) {
			$this->Session->setFlash('Endereço não encontrado.', FLASH_ERROR, array(), 'endereco');
			// Verificando se foi agendada alguma URL para ser redirecionada
			if (!$this->Redirect->redirect()) {
				$this->redirect(array('controller' => 'clientes', 'action' => 'meus_enderecos'));
			}
		}

		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Endereco->save($this->request->data)) {
				$this->Session->setFlash('Endereço alterado com sucesso.', FLASH_SUCCESS, array(), 'endereco');
				$this->redirect(array('controller' => 'clientes', 'action' => 'meus_enderecos'));
			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar alterar o endereço. Por favor, tente novamente.', FLASH_ERROR, array(), 'endereco');
			}
		}

		$this->request->data = $endereco;

	}

	public function excluir_endereco($id = null) {
		if (!$id) {
			$this->Session->setFlash('Endereço não encontrado.', FLASH_ERROR);
			$this->redirect(array('controller' => 'clientes', 'action' => 'meus_enderecos'));
		}

		$endereco = $this->Endereco->find('first', array(
			'contain' => array(
				'Cidade.nome',
				'Cidade.Estado.nome'
			),
			'conditions' => array(
				'Endereco.ativo',
				'Endereco.id' => $id,
				'Endereco.cliente_id' => $this->usuarioAutenticado['id']
			)
		));

		if (!$endereco) {
			$this->Session->setFlash('Endereço não encontrado.', FLASH_ERROR);
			$this->redirect(array('controller' => 'clientes', 'action' => 'meus_enderecos'));
		}

		if ($this->request->is('post') || $this->request->is('put')) {
			$this->Endereco->id = $id;
			if ($this->Endereco->saveField('ativo', false, false)) {
				$this->Session->setFlash('Endereço excluído com sucesso.', FLASH_SUCCESS, array(), 'endereco');
				$this->redirect(array('controller' => 'clientes', 'action' => 'meus_enderecos'));
			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar excluir o Endereço. Por favor, tente novamente.', FLASH_ERROR, array(), 'endereco');
			}
		}
	}

	public function login() {
		$this->layout = 'cadastro';

		if ($this->request->is('post')) {

			if ($this->Auth->login()) {
				$this->usuarioAutenticado = $this->Auth->user();

				// Limpando as permissões do perfil do usuário caso elas já existam de um login anterior para que elas sejam atualizadas
				if ($this->Session->check('Permissoes.'.$this->usuarioAutenticado['perfil_id'])) {
					$this->Session->delete('Permissoes.'.$this->usuarioAutenticado['perfil_id']);
				}

				// Força a entrada na entrega se vier do checkout
				if ($this->params->named['return'] == 'checkout') {
					$this->redirect(array('controller' => 'checkout', 'action' => 'entrega'));
				} else {
					// Verificando se foi agendada alguma URL para ser redirecionada
					if (!$this->Redirect->redirect()) {
						return $this->redirect($this->Auth->redirectUrl());
					}
				}

			} else {
				$this->Session->setFlash('E-mail e/ou senha inválidos.', FLASH_ERROR, array(), 'login');
				$this->redirect(array('controller' => $this->params->named['return'], 'action' => 'login'));
			}
		}
	}

	public function logout() {
		$urlAuthLogout = $this->Auth->logout();

		// Verificando se foi agendada alguma URL para ser redirecionada
		if (!$this->Redirect->redirect()) {
			return $this->redirect($urlAuthLogout);
		}
	}

	public function esqueci_minha_senha() {

		if ($this->request->is('post') || $this->request->is('put')) {
			$cliente = $this->Cliente->find('first', array(
				'conditions' => array(
					'email' => $this->request->data('Esqueci.email')
				)
			));

			if (!$cliente) {
				$this->Session->setFlash('Não foi encontrado nenhum usuário com o e-mail '.$this->request->data('Esqueci.email').'.', FLASH_ERROR, array(), 'esqueci_minha_senha');
				$this->redirect(array('controller' => $this->params->named['return'], 'action' => 'esqueci_minha_senha'));
			} else {
				$cliente['Cliente']['senha'] = $this->Random->gerarString();

				if ($this->Cliente->save($cliente)) {
					$email = new CakeEmail('esqueciMinhaSenha');
					$email->theme(ucfirst(PROJETO));
					$email->to($cliente['Cliente']['email']);
					$email->viewVars(array('cliente' => $cliente));
					$email->send();
					$this->Session->setFlash('Nova senha enviada com sucesso para o e-mail '.$cliente['Cliente']['email'].'.', FLASH_SUCCESS, array(), 'esqueci_minha_senha');
					$this->redirect(array('controller' => $this->params->named['return'], 'action' => 'login'));
				} else {
					$this->Session->setFlash('Ocorreu um erro ao tentar enviar uma nova senha. Por favor, tente novamente.', FLASH_ERROR, array(), 'esqueci_minha_senha');
				}
			}
		}
	}

	public function meus_pedidos() {
		$this->layout = 'painel';

		$cliente_id = $this->usuarioAutenticado['id'];

		/* Lista todos os pedidos do cliente */
		$this->paginate = array(
			'contain' => array(
				'StatusPedido' => array('descricao', 'status_inicial'),
				'PedidoSku' => array('preco_total')
			),
			'conditions' => array(
				'Pedido.cliente_id' => $cliente_id
			),
			'order' => array(
				'Pedido.data_hora' => 'DESC'
			),
			'limit' => 20
		);

		$this->set('pedidos', $this->paginate('Pedido'));

	}

	public function pedido($id = null) {
		$this->layout = 'painel';

		$cliente_id = $this->usuarioAutenticado['id'];

		$pedido = $this->Pedido->find('first', array(
			'contain' => array(
				'Cliente.nome',
				'StatusPedido.descricao',
				'Endereco' => array(
					'Cidade' => array('nome', 'uf')
				),
				'PedidoSku.PedidoCaracteristica' => array('caracteristica', 'valor', 'referente'),
				'PedidoSku.Promocao',
				'PedidoSku.CompreJuntoPromocao',
				'PedidoSku.Sku.Produto',
				'PedidoSku.PedidoSkuServico',
				'Promocao.PromocaoCausa',
				'Promocao.PromocaoEfeito',
				'FreteTransportadora'
			),
			'conditions' => array(
				'Pedido.id' => $id,
				'Pedido.cliente_id' => $cliente_id
			)
		));

		if ($pedido['Pedido']['bling_danfe'] != '') {
			$nota_fiscal = json_decode($pedido['Pedido']['bling_danfe']);
			$this->set('nota_fiscal', $nota_fiscal);
		}

		if (count($pedido) == 0) {
			/* Se não encontrou o pedido, volta para a listagem */
			$this->redirect(array('controller' => 'clientes', 'action' => 'meus_pedidos'));
		} else {
			$this->set('pedido', $pedido);
		}
	}

	public function admin_baixar_csv() {

		$this->layout = 'csv';

		$filename = 'clientes.csv';

		header('Content-type: application/csv');
		header('Content-Disposition: attachment; filename="'.$filename.'"');

		$csv_file = fopen('php://output', 'w');

		$nome = null;
		if (isset($this->params['named']['nome'])) {
			$nome = $this->params['named']['nome'];
		}

		$tipo_pessoa = null;
		if (isset($this->params['named']['tipo_pessoa'])) {
			$tipo_pessoa = $this->params['named']['tipo_pessoa'];
		}

		$status_cliente = null;
		if (isset($this->params['named']['status_cliente'])) {
			$status_cliente = $this->params['named']['status_cliente'];
		}

		$email = null;
		if (isset($this->params['named']['email'])) {
			$email = $this->params['named']['email'];
		}

		$genero = null;
		if (isset($this->params['named']['genero'])) {
			$genero = $this->params['named']['genero'];
		}

		$estado_id = null;
		if (isset($this->params['named']['estado_id'])) {
			$estado_id = $this->params['named']['estado_id'];
		}

		$cpf = null;
		if (isset($this->request->params['named']['cpf'])) {
			$cpf = $this->request->params['named']['cpf'];
		}

		$cnpj = null;
		if (isset($this->request->params['named']['cnpj'])) {
			$cnpj = $this->request->params['named']['cnpj'];
			$cnpj = explode(',', $cnpj);
			$cnpj = $cnpj[0] . '/' . $cnpj[1];

		}

		$cep = null;
		if (isset($this->params['named']['cep'])) {
			$cep = $this->params['named']['cep'];
		}

		$cidade_id = null;
		if (isset($this->request->params['named']['cidade_id'])) {
			$cidade_id = $this->request->params['named']['cidade_id'];
		}

		$receber_newsletter = null;
		if (isset($this->params['named']['receber_newsletter'])) {
			$receber_newsletter = $this->params['named']['receber_newsletter'];
		}

		$dia_aniversario = null;
		if (isset($this->params['named']['dia_aniversario'])) {
			$dia_aniversario = $this->params['named']['dia_aniversario'];
		}

		$mes_aniversario = null;
		if (isset($this->params['named']['mes_aniversario'])) {
			$mes_aniversario = $this->params['named']['mes_aniversario'];
		}

		$ano_aniversario = null;
		if (isset($this->params['named']['ano_aniversario'])) {
			$mes_aniversario = $this->params['named']['ano_aniversario'];
		}

		$conditions = $this->filtro($nome, $tipo_pessoa, $status_cliente, $email, $genero, $estado_id, $cidade_id, $cnpj, $cpf, $cep, $receber_newsletter, $dia_aniversario, $mes_aniversario, $ano_aniversario);

		$clientes = $this->Cliente->find('all', array(
			'order' => array(
				'Cliente.nome' => 'ASC'
			),
			'conditions' => $conditions,
		));

		$header_row = array("id", "tipo de pessoa", "nome", "razao social", "nome fantasia",  "data de nascimento", "genero", "cpf", "cnpj", "email", "tel residencial", "tel comercial pf",  "tel comercial pj", "celular", "data do cadastro", "status", "receber email oferta");
	    fputcsv($csv_file,$header_row,';','"');

		foreach($clientes as $cliente) {

			$tipo_pessoa = ($cliente['Cliente']['tipo_pessoa'] === 'F' ? 'PF':'PJ');
			$dados = ($cliente['Cliente']['tipo_pessoa'] === 'F' ? $cliente['Cliente']['cpf']:$cliente['Cliente']['cnpj']);
			$status_cliente = $cliente['Cliente']['ativo'] ? 'Ativo' : 'Inativo';
			$receber_newsletter = $cliente['Cliente']['receber_newsletter'] ? 'Sim' : 'Nao';

			$row = array(
				$cliente['Cliente']['id'],
				$tipo_pessoa,
				$cliente['Cliente']['nome'],
				$cliente['Cliente']['razao_social'],
				$cliente['Cliente']['nome_fantasia'],
				$cliente['Cliente']['data_nascto'],
				$cliente['Cliente']['sexo'],
				$cliente['Cliente']['cpf'],
				$cliente['Cliente']['cnpj'],
				$cliente['Cliente']['email'],
				$cliente['Cliente']['tel_residencial'],
				$cliente['Cliente']['tel_comercial_pf'],
				$cliente['Cliente']['tel_comercial_pj'],
				$cliente['Cliente']['celular'],
				$cliente['Cliente']['created'],
				$status_cliente,
				$receber_newsletter 
			);

			fputcsv($csv_file,$row,';', '"');
		}

		fclose($csv_file);

	}

    public function admin_ajax_carregar_cidades($estado_id = null) {

		$cidades = $this->Cidade->find('all', array(
			'contain' => false,
			'conditions' => array(
				'Cidade.estado_id' => $estado_id
			),
			'fields' => array(
				'Cidade.id',
				'Cidade.nome'
			),
		));

		if (count($cidades)) {
			$json = array('sucesso' => true, 'cidades' => $cidades);
		} else {
			$json = array('sucesso' => false, 'mensagem' => 'Ocorreu um erro...');
		}

		$this->autoRender = false;
		$output = fopen('php://output', 'w');
		fputs($output, json_encode($json));
		fclose($output);
	}

	private function filtro($nome = null, $tipo_pessoa = null ,$status_cliente = null, $email = null, $genero = null, $estado_id = null, $cidade_id = null, $cnpj = null, $cpf = null, $cep = null, $receber_newsletter = null, $dia_aniversario = null, $mes_aniversario = null, $ano_aniversario = null) {

		$conditions = array(
			//'Cliente.ativo' => true,
		);


		if($nome != ''){
			$conditions = array(
				'OR' => array(
					'Cliente.nome like' => '%'.$nome.'%',
					'Cliente.nome_fantasia like' => '%'.$nome.'%',
				)
			);
		}

		if($tipo_pessoa != ''){
			$conditions['Cliente.tipo_pessoa'] = $tipo_pessoa;
		}

		if($receber_newsletter != ''){
			$conditions['Cliente.receber_newsletter'] = $receber_newsletter;
		}

		if($dia_aniversario != ''){

			$conditions['DAY(Cliente.data_nascto) ='] = $dia_aniversario;
		}

		if($mes_aniversario != ''){

			$conditions['MONTH(Cliente.data_nascto) ='] = $mes_aniversario;
		}

		if($ano_aniversario != ''){

			$conditions['YEAR(Cliente.data_nascto) ='] = $ano_aniversario;
		}

		if($status_cliente != ''){
			$conditions['Cliente.ativo'] = $status_cliente;
		}

		if($email != ''){
			$conditions['Cliente.email'] = $email;

		}

		if($genero != ''){
			$conditions['Cliente.sexo'] = $genero;

		}

		if($cpf != ''){
			$conditions['Cliente.cpf'] = $cpf;
		}

		if($cnpj != ''){
			$conditions['Cliente.cnpj'] = $cnpj;
		}

		if($cep != ''){

			$enderecos =  $this->Endereco->find('all', array(
				'contain' => 'Cliente',
				'conditions' => array(
					'Endereco.descricao' => 'Principal',
					'Endereco.cep' => $cep ,
					'Cliente.id <>' => '',
				)
			));

			$clientes_id = array();
			foreach ($enderecos as $cliente) {
				$clientes_id[] = $cliente['Cliente']['id'];
			}

			$conditions['Cliente.id'] = $clientes_id;
		}

		if ($estado_id != '') {

			$cidade =  $this->Cidade->find('list', array(
				'contais' => false,
				'conditions' => array(
					'Cidade.estado_id' => $estado_id
				),
				'fields' => array(
					'Cidade.nome'
				)
			));

			$this->set('cidade', $cidade);

			$id_cidades =  $this->Cidade->find('list', array(
				'conditions' => array(
					'Cidade.estado_id' => $estado_id
				),
				'fields' => array(
					'Cidade.id'
				)
			));

			if ($cidade_id != '') {
				$enderecos =  $this->Endereco->find('all', array(
					'contain' => 'Cliente',
					'conditions' => array(
						'Endereco.cidade_id' => $cidade_id,
						'Endereco.descricao' => 'Principal',
						'Cliente.id <>' => '',
					)
				));

			} else {
				$enderecos =  $this->Endereco->find('all', array(
					'contain' => 'Cliente',
					'conditions' => array(
						'Endereco.cidade_id' => $id_cidades,
						'Endereco.descricao' => 'Principal',
						'Cliente.id <>' => '',
					)
				));
			}

			$clientes_id = array();
			foreach ($enderecos as $cliente) {
				$clientes_id[] = $cliente['Cliente']['id'];
			}

			$conditions['Cliente.id'] = $clientes_id;

		} else {
			$cidade = null;
			$this->set('cidade', $cidade);
		}

		return $conditions;

	}

	private function getSexos() {
		return array(
			'M' => 'Masculino',
			'F' => 'Feminino',
		);
	}

	private function getTipoPessoa() {
		return array(
			'F' => 'Pessoa Física',
			'J' => 'Pessoa Jurídica',
		);
	}

	private function getStatusCliente() {
		return array(
			'1' => 'Ativo',
			'0' => 'Inativo',
		);
	}

	private function getEmailOfertas() {
		return array(
			'1' => 'Quer receber',
			'0' => 'Não quer receber',
		);
	}

	private function getMesAniversario() {
		return array(
			'01' => 'Janeiro',
			'02' => 'Fevereiro',
			'03' => 'Março',
			'04' => 'Abril',
			'05' => 'Maio',
			'06' => 'Junho',
			'07' => 'Julho',
			'08' => 'Agosto',
			'09' => 'Setembro',
			'10' => 'Outubro',
			'11' => 'Novembro',
			'12' => 'Dezembro',
		);
	}

	private function getDiaAniversario() {
		return array(
			'01' => '01',
			'02' => '02',
			'03' => '03',
			'04' => '04',
			'05' => '05',
			'06' => '06',
			'07' => '07',
			'08' => '08',
			'09' => '09',
			'10' => '10',
			'11' => '11',
			'12' => '12',
			'13' => '13',
			'14' => '14',
			'15' => '15',
			'16' => '16',
			'17' => '17',
			'18' => '18',
			'19' => '19',
			'20' => '20',
			'21' => '21',
			'22' => '22',
			'23' => '23',
			'24' => '24',
			'25' => '25',
			'26' => '26',
			'27' => '27',
			'28' => '28',
			'29' => '29',
			'30' => '30',
			'31' => '31',

		);
	}

	private function getAnoAniversario() {

		$ano = array();
		$ano_atual = date('o');
		$ano_limite = date('o', strtotime('-100 year'));

		for ($i = $ano_atual ; $i > $ano_limite; $i--) { 

			$ano[$i] = $i;
		}

		return $ano;
	}

}
?>