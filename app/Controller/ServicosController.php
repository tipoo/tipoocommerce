<?php
App::uses('AppController', 'Controller');

class ServicosController extends AppController {

	public $uses = array('Servico', 'Produto', 'ServicoAnexo');

	public function admin_index() {
		$this->paginate = array(
			'limit' => 50
		);

		$this->set('servicos', $this->paginate());

	}

	public function admin_adicionar($id = null) {
		if ($this->request->is('post')) {
			$this->Servico->create();
			if ($this->Servico->save($this->request->data)) {
				$this->Session->setFlash('Serviço salvo com sucesso.', FLASH_SUCCESS);
				$this->backToPaginatorIndex();
			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar salvar o serviço. Por favor, tente novamente.', FLASH_ERROR);
			}
		}
	}

	public function admin_editar($id = null) {
		$this->Servico->id = $id;

		if (!$this->Servico->exists()) {
			throw new NotFoundException('Serviço inexistente.');
		}

		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Servico->save($this->request->data)) {
				$this->Session->setFlash('Serviço  editado com sucesso.', FLASH_SUCCESS);
				$this->backToPaginatorIndex();
			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar editar o serviço . Por favor, tente novamente.', FLASH_ERROR);
			}
		} else {
			$this->request->data = $this->Servico->read(null, $id);
		}
	}


	public function admin_excluir($id = null) {
		$this->Servico->id = $id;

		if (!$this->Servico->exists()) {
			throw new NotFoundException('Serviço inexistente.');
		}

		if ($this->request->is('post')) {

			if ($this->Servico->saveField('ativo', false, false)) {
				$this->Session->setFlash('Serviço desativado com sucesso.', FLASH_SUCCESS);
				$this->backToPaginatorIndex();
			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar desativar o serviço. Por favor, tente novamente.', FLASH_ERROR);
			}

		}

	}

	public function admin_ativar($id = null) {
		$this->Servico->id = $id;

		// $anexos = $this->ServicoAnexo->find('count', array(
		// 	'conditions' => array(
		// 		'ServicoAnexo.ativo' => true,
		// 		'ServicoAnexo.servico_id' => $id
		// 	)
		// ));

		// if (!$anexos) {
		// 	$this->Session->setFlash('Para ativar um serviço é necessário que você cadastre no mínimo um anexo.', FLASH_ERROR);
		// 	$this->redirect($this->referer());
		// }

		if (!$this->Servico->exists()) {
			throw new NotFoundException('Serviço inexistente.');
		}

		if ($this->request->is('post')) {

			if ($this->Servico->saveField('ativo', true, false)) {
				$this->Session->setFlash('Serviço ativado com sucesso.', FLASH_SUCCESS);
				$this->backToPaginatorIndex();
			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar ativar o serviço. Por favor, tente novamente.', FLASH_ERROR);
			}

		}

	}
}
?>