<?php
App::uses('AppController', 'Controller');

class TagsController extends AppController {

	public $uses = array('Tag', 'Slug');

	public function admin_index() {

		$conditions = array(
			'Tag.ativo' => true,
		);

		$tag = null;
		if (isset($this->params['named']['tag'])) {
			$tag = $this->params['named']['tag'];
			$this->request->data['Filtro']['tag'] = $tag;
		}

		if($tag != ''){
			$conditions['Tag.nome like'] = '%'.$tag.'%';
		}

		$this->paginate = array(
			'conditions' => $conditions,
			'order' => array(
				'Tag.nome' => 'ASC'
			),
			'limit' => 50
		);

		$this->set('tags', $this->paginate());
	}

	public function admin_adicionar() {
		if(($this->request->is('post') || $this->request->is('put')) && !empty($this->request->data)) {

			$verificaTag = $this->Tag->find('first', array(
				'conditions' => array(
					'Tag.nome' => $this->request->data['Tag']['nome'],
				)
			));

			if (count($verificaTag) == 0) {


				/* Slug */

				$count_slug = 0;
				do {
					if ($count_slug == 0) {
						$slug = '/' . Inflector::slug($this->request->data['Tag']['nome'], '-');
					} else {
						$slug = '/' . Inflector::slug($this->request->data['Tag']['nome'], '-') . '-' . $count_slug;
					}
					$slug_find = $this->Slug->find('first', array(
						'contain' => false,
						'conditions' => array(
							'Slug.url' => $slug
						)
					));

					$count_slug++;

				} while ($slug_find);

				$this->request->data['Slug']['url'] = strtolower($slug);

				$this->request->data['Slug']['controller'] = 'catalogo';
				$this->request->data['Slug']['action'] = 'tag';
				$this->request->data['Slug']['custom_parser_class'] = 'TagParser';

				$this->Tag->create();
				if ($this->Tag->saveAll($this->request->data)) {
					$this->Session->setFlash('Tag salva com sucesso.', FLASH_SUCCESS);
					$this->backToPaginatorIndex();
				} else {
					$this->Session->setFlash('Ocorreu um erro ao tentar salvar a Tag. Por favor, tente novamente.', FLASH_ERROR);
				}
			} else {
				$this->Session->setFlash('Já existe uma tag cadastrada com o nome <i>"' . $verificaTag['Tag']['nome'] . '"</i>', FLASH_ERROR);
			}
		}
	}

	public function admin_editar($id = null) {
		$this->Tag->id = $id;

		$tag = $this->Tag->find('first', array(
			'contain' => array(
				'Slug'
			),
			'conditions' => array(
				'Tag.id' => $id
			)
		));

		if (!$this->Tag->exists()) {
			throw new NotFoundException('Tag inexistente.');
		}

		if(($this->request->is('post') || $this->request->is('put')) && !empty($this->request->data)) {

			$verificaTag = $this->Tag->find('first', array(
				'conditions' => array(
					'Tag.nome' => $this->request->data['Tag']['nome'],
				)
			));

			if (count($verificaTag) == 0 || $verificaTag['Tag']['id'] == $id ) {

				$this->request->data['Slug']['url'] = '/' . Inflector::slug($this->request->data['Slug']['url'], '-');
				$this->request->data['Slug']['url'] = strtolower($this->request->data['Slug']['url']);

				$verificaSlug = $this->Slug->find('first', array(
					'conditions' => array(
						'Slug.url' => $this->request->data['Slug']['url'] 
					)
				));

				if(count($verificaSlug) == 0 || $verificaSlug['Slug']['id'] == $tag['Tag']['slug_id']){

					if ($this->Tag->saveAll($this->request->data)) {
						$this->Session->setFlash('Tag salva com sucesso.', FLASH_SUCCESS);
						$this->backToPaginatorIndex();
					} else {
						$this->Session->setFlash('Ocorreu um erro ao tentar salvar a Tag. Por favor, tente novamente.', FLASH_ERROR);
					}

				} else {
					$this->Session->setFlash('Já existe uma url  "<strong>' .  $this->request->data['Slug']['url']  . '</strong>". Por favor, cadastre uma url diferente.', FLASH_ERROR);
					$this->request->data['Slug']['url'] = $tag['Slug']['url'];
				}

			} else {
				$this->Session->setFlash('Já existe uma tag cadastrada com o nome <i>"' . $verificaTag['Tag']['nome'] . '"</i>', FLASH_ERROR);
			}
		} else {

			$this->request->data = $this->Tag->read(null, $id);

		}


	}

	public function admin_excluir($id = null) {
		$this->Tag->id = $id;

		if (!$this->Tag->exists()) {
			throw new NotFoundException('Tag inexistente.');
		}

		if($this->request->is('post') || $this->request->is('put')) {

			$tag = $this->Tag->find('first', array(
				'conditions' => array(
					'Tag.id' => $id
				)
			));

			$this->Slug->id = $tag['Tag']['slug_id'];
			$this->Slug->saveField('ativo', false, false);

			if ($this->Tag->saveField('ativo', false, false)) {
				$this->Session->setFlash('Tag excluída com sucesso.', FLASH_SUCCESS);
				$this->backToPaginatorIndex();
			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar excluir a Tag. Por favor, tente novamente.', FLASH_ERROR);
			}
		}
	}

	public function admin_ajax_buscar_tags($nome = null) {

		$tags = $this->Tag->find('all', array(
			'contain' => false,
			'conditions' => array(
				'Tag.ativo',
				'Tag.nome LIKE "%' . $nome . '%"'
			)
		));

		$resultado = array();
		foreach ($tags as $tag) {
			$resultado[] = array(
				'value' => $tag['Tag']['id'],
				'nome' => $tag['Tag']['nome']
			);
		}

		$this->renderJson($resultado);
	}

	public function admin_ajax_adicionar($nome = null) {

		$verificaTag = $this->Tag->find('first', array(
			'conditions' => array(
				'Tag.nome' => $nome,
			)
		));

		if (count($verificaTag) == 0) {

			/* Slug */

			$count_slug = 0;
			do {
				if ($count_slug == 0) {
					$slug = '/' . Inflector::slug($nome, '-');
				} else {
					$slug = '/' . Inflector::slug($nome, '-') . '-' . $count_slug;
				}
				$slug_find = $this->Slug->find('first', array(
					'contain' => false,
					'conditions' => array(
						'Slug.url' => $slug
					)
				));

				$count_slug++;

			} while ($slug_find);

			$this->Slug->create();

			$this->request->data['Slug']['url'] = strtolower($slug);

			$this->request->data['Slug']['controller'] = 'catalogo';
			$this->request->data['Slug']['action'] = 'tag';
			$this->request->data['Slug']['custom_parser_class'] = 'TagParser';
		
			$this->Slug->save($this->request->data['Slug']);

			$this->Tag->create();

			$data = array(
				'nome' =>  $nome,
				'slug_id' => $this->Slug->id
			);

			if ($this->Tag->save($data)) {

				$nova_tag = $this->Tag->find('all', array(
					'conditions' => array(
						'Tag.ativo' => true,
						'Tag.id' => $this->Tag->id,
					),
				));

			$json = array('sucesso' => true, 'nova_tag' => $nova_tag);

			} 
		} else {

			$json = array('sucesso' => false,'mensagem' => 'Essa tag já está cadastrada.');
		}

		$this->renderJson($json);
	}

}