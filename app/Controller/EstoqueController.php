<?php
App::uses('AppController', 'Controller');

class EstoqueController extends AppController {

	public $uses = array('Sku', 'PedidoSku', 'Pedido');

	public function admin_index() {

	}

	public function admin_movimentacao($id = null) {

		$sku = $this->Sku->find('first', array(
			'contain' => array(
				'Produto.Marca'
			),
			'conditions' => array(
				'Sku.id' => $id
			)
		));

		$qtd_reservas = $this->PedidoSku->query("select COALESCE(SUM(`qtd`), 0) as qtd_reservas from `pedido_skus` where sku_id = " . $id . " and `situacao_estoque` = 'Reserva'");
		$qtd_pre_vendas = $this->PedidoSku->query("select COALESCE(SUM(`qtd`), 0) as qtd_pre_vendas from `pedido_skus` where sku_id = " . $id . " and `situacao_estoque` = 'PreVenda'");

		$this->set('qtd_reservas', $qtd_reservas[0][0]);
		$this->set('qtd_pre_vendas', $qtd_pre_vendas[0][0]);
		$this->set('sku', $sku);
	}

	public function admin_adicionar($id = null) {
		$this->Sku->id = $id;

		$sku = $this->Sku->find('first', array(
			'contain' => array(
				'Produto'
			),
			'conditions' => array(
				'Sku.id' => $id
			)
		));

		$qtd_total = $qtd_total_inicial = $this->request->data['Adicionar']['qtd'] + $sku['Sku']['qtd_estoque'];

		// Busca os pedido_skus em pré-venda que tem quantidade menor ou igual ao qtd_total disponível
		$pedido_skus = $this->PedidoSku->find('all', array(
			'contain' => array(
				'Pedido.StatusPedido'
			),
			'conditions' => array(
				'PedidoSku.sku_id' => $id,
				'PedidoSku.situacao_estoque' => 'PreVenda',
				'PedidoSku.qtd <= ' => $qtd_total
			),
			'order' => array('previsao_entrega_pre_venda ASC')
		));

		// Verifica se é possível passar os pedido_sku para situacao 'Pedido'
		foreach ($pedido_skus as $pedido_sku) {
			if ($pedido_sku['PedidoSku']['qtd'] <= $qtd_total) {
				$qtd_total -= $pedido_sku['PedidoSku']['qtd'];

				$this->PedidoSku->id = $pedido_sku['PedidoSku']['id'];

				if ($pedido_sku['Pedido']['StatusPedido']['status_inicial']) {
					$this->PedidoSku->saveField('situacao_estoque', 'Reserva');
				} else {
					$this->PedidoSku->saveField('situacao_estoque', 'Pedido');
				}

				// Verificando se o pedido não tem mais nenhum produto em pré-venda e alterando colocando o pedido com o estoque disponível
				$tem_prevenda = $this->PedidoSku->find('count', array(
					'contain' => false,
					'conditions' => array(
						'PedidoSku.pedido_id' => $pedido_sku['PedidoSku']['pedido_id'],
						'PedidoSku.situacao_estoque' => 'PreVenda'
					)
				));

				if (!$tem_prevenda) {
					$this->Pedido->id = $pedido_sku['PedidoSku']['pedido_id'];
					$this->Pedido->saveField('estoque_disponivel', true);
				}
			}
		}

		if ($this->Sku->saveField('qtd_estoque', $qtd_total)) {
			if ($qtd_total != $qtd_total_inicial) {
				$this->Session->setFlash('Seu estoque ficou com '.$qtd_total.' item(s). Os demais atenderam a demanda de Pré-Venda', FLASH_SUCCESS);
			} else {
				$this->Session->setFlash('Seu estoque ficou com '.$qtd_total.' item(s).', FLASH_SUCCESS);
			}

			// Mercado Livre
			if ($this->Configuracao->get('mercado_livre_ativo')) {
				$this->MercadoLivreProdutos->editar_produto($sku['Produto']['id']);
			}
			// Fim - Mercado Livre
		} else {
			$this->Session->setFlash('Ocorreu um erro ao tentar atualizar o estoque. Por favor, tente novamente.', FLASH_ERROR);
		}
		$this->redirect($this->referer());
	}

	public function admin_remover($id = null) {
		$this->Sku->id = $id;

		$sku = $this->Sku->find('first', array(
			'contain' => array(
				'Produto'
			),
			'conditions' => array(
				'Sku.id' => $id
			)
		));

		$qtd_estoque = $sku['Sku']['qtd_estoque'] - $this->request->data['Remover']['qtd'];

		if ($qtd_estoque < 0) {
			$this->Session->setFlash('Não foi possível remover ' . $this->request->data['Remover']['qtd'] . ' skus, pois apenas ' . $sku['Sku']['qtd_estoque'] . ' se encontram em estoque.', FLASH_ERROR);
		} else {
			if ($this->request->is('post')) {
				if ($this->Sku->saveField('qtd_estoque', $qtd_estoque, false)) {
					$this->Session->setFlash('Estoque atualizado com sucesso.', FLASH_SUCCESS);

					// Mercado Livre
					if ($this->Configuracao->get('mercado_livre_ativo')) {
						$this->MercadoLivreProdutos->editar_produto($sku['Produto']['id']);
					}
					// Fim - Mercado Livre
				} else {
					$this->Session->setFlash('Ocorreu um erro ao tentar atualizar o estoque. Por favor, tente novamente.', FLASH_ERROR);
				}
			}
		}

		$this->redirect($this->referer());
	}

}
?>