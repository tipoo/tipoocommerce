<?php
App::uses('AppController', 'Controller');

App::uses('Moip', 'Vendor/Moip');
App::uses('MoipClient', 'Vendor/Moip');

App::uses('HttpSocket', 'Network/Http');

class CheckoutController extends AppController {

	public $uses = array(
		'Endereco',
		'Pedido',
		'StatusPedido',
		'CieloBandeira',
		'Parcela',
		'Promocao',
		'Configuracao',
		'Sku',
		'PromocoesFreteTransportadora',
		'MoipInstituicao',
		'PagseguroTransparenteBandeira',
		'CamposComplementar'
	);

	public $components = array(
		'PagSeguro.Carrinho',
		'PagSeguro.Notificacao',
		'Cielo.Cielo',
		'RegrasPromocoesProduto',
		'RegrasPromocoesPedidos',
		'RegrasPromocoesFrete',
		'RegrasPromocoesLeveXPagueY',
		'RegrasPromocoesPrecos',
		'RegrasPromocoesCompreJunto',
		'RegrasPromocoesCupom',
		'RegrasProdutos',
		'CustomTagsParser' => array('actions' => array('entrega', 'pagamento', 'login', 'cadastrar', 'esqueci_minha_senha', 'compra_finalizada'))
	);

	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('login', 'cadastrar', 'esqueci_minha_senha', 'notificacao_pagseguro', 'notificacaoMoip');

		$this->layout = 'checkout';

	}

	public function entrega($id = null, $frete_transportadora_id = null) {
		//$skus = $this->Session->read('Carrinho.Skus');
		//$pedido_valores = $this->Session->read('Carrinho.Pedido');
		//$cupom = $this->Session->read('Carrinho.Cupom');

		if ($id && $frete_transportadora_id) {
			// Recuperando os endereços da sessão para poder persistir o valor, o prazo e a transportadora
			$enderecos_sessao = $this->Session->read('Checkout.Enderecos');
			if ($enderecos_sessao) {
				foreach ($enderecos_sessao as $endereco_sessao) {
					if ($endereco_sessao['Endereco']['id'] == $id) {
						$endereco = $endereco_sessao;
						foreach ($endereco_sessao['FreteTransportadora'] as $frete_transportadora_sessao) {
							if ($frete_transportadora_sessao['FreteTransportadora']['id'] == $frete_transportadora_id) {
								$frete_transportadora = $frete_transportadora_sessao;
							}
						}
					}
				}
			}

			if ($endereco) {
				$skus = $enderecos_sessao[$id]['Skus'];
				$pedido_valores = $enderecos_sessao[$id]['Pedido'];

				// Verificando se não há produtos no carrinho.
				if (!count($skus)) {
					$this->Session->setFlash('Seu carrinho está vazio. Caso você tenha realizado um pedido e deseje pagá-lo agora, vá até Meus Pedidos, encontre-o e selecione a opção de pagar.');
					$this->redirect(array('controller' => 'carrinho'));
				}

				// Salva os dados do pedido e redireciona para a página de pagamento
				$status_id = $this->StatusPedido->field('StatusPedido.id', array('StatusPedido.status_inicial'));

				unset($endereco['Endereco']['id']);
				unset($endereco['Endereco']['cliente_id']);
				unset($endereco['Cidade']);
				unset($endereco['FreteTransportadora']);

				$pedido = array(
					'Pedido' => array(
						'cliente_id' => $this->usuarioAutenticado['id'],
						'status_id' => $status_id,
						'data_hora' => date('d/m/Y H:i:s'),
						'frete_transportadora_id' => $frete_transportadora['FreteTransportadora']['id'],
						'prazo_frete' => $frete_transportadora['FreteTransportadora']['prazo_entrega_frete'],
						'valor_frete' => $frete_transportadora['FreteTransportadora']['valor_frete']
					),
					'PedidoSku' => array(),
				);

				if (Configure::read('Sistema.Checkout.PagSeguro.ativo')) {
					$pedido['Pedido']['forma_pagamento'] = 'PagSeguro';
				}

				if ($this->Configuracao->get('moip_ativo')) {
					$pedido['Pedido']['forma_pagamento'] = 'Moip';
				}

				// Adiciona um novo endereço de entrega na tabela que não poderá ser alterado
				$pedido += $endereco;

				// Adicionando a promoção de frete pedido, caso exista
				$pedido['Promocao'] = array();
				if (isset($frete_transportadora['FreteTransportadora']['promocao'])) {
					$promocao = $frete_transportadora['FreteTransportadora']['promocao'];

					// Prepara promoção no array para salvar junto com o pedido
					$pedido['Promocao'][] = $this->pedido_promocao($promocao);
				}

				// Adicionando a promoção de pedido, caso exista
				if (isset($pedido_valores['Promocao']['melhor_desconto_pedido'])) {
					$promocao = $pedido_valores['Promocao']['melhor_desconto_pedido']['promocao'];

					// Prepara promoção no array para salvar junto com o pedido
					$pedido['Promocao'][] = $this->pedido_promocao($promocao);
				}

				// Adicionando a promoção de cupom, caso exista e o cupom ainda não foi utilizado por aquele cliente
				if (isset($pedido_valores['Promocao']['cupom_desconto']) && (!$pedido_valores['Promocao']['cupom_desconto']['cupom_utilizado'] || $pedido_valores['Promocao']['cupom_desconto']['cupom_reutilizavel'])) {
					$promocao = $pedido_valores['Promocao']['cupom_desconto'];

					// Prepara promoção no array para salvar junto com o pedido
					$pedido['Promocao'][] = $this->pedido_promocao($promocao);
				}

				// Adicionando os produtos do carrinho ao pedido
				$i = 0;
				$tem_produto_prevenda = false;

				foreach ($skus as $sku) {
					$preco_unitario_calculo = (isset($sku['preco_unitario_desconto']) && $sku['preco_unitario_desconto'] > 0) ? $sku['preco_unitario_desconto'] : $sku['preco_unitario'];
					$preco_unitario_desconto = (isset($sku['preco_unitario_desconto']) && $sku['preco_unitario_desconto'] > 0) ? $sku['preco_unitario_desconto'] : '';

					$pedido['PedidoSku'][$i] = array(
						'sku_id' => $sku['Sku']['id'],
						'sku' => $sku['Sku']['sku'],
						'descricao_marca' => $sku['Produto']['Marca']['descricao'],
						'descricao_produto' => $sku['Produto']['descricao'],
						'ncm' => $sku['Produto']['ncm'],
						'origem' => $sku['Produto']['origem'],
						'imagem_produto' => isset($sku['Imagem'][0]['imagem']) ? $sku['Imagem'][0]['imagem'] : $sku['Produto']['Imagem'][0]['imagem'],
						'descricao_sku' => $sku['Sku']['descricao'],
						'preco_unitario' => $preco_unitario_desconto != '' ? $sku['Produto']['preco_de'] : $sku['preco_unitario'],
						'preco_unitario_desconto' => $preco_unitario_desconto,
						'preco_total' => $preco_unitario_calculo * $sku['qtd'],
						'peso_unitario' => $sku['peso_unitario'],
						'peso_total' => $sku['Sku']['peso'] * $sku['qtd'],
						'largura_unitario' => $sku['Sku']['largura'],
						'altura_unitario' => $sku['Sku']['altura'],
						'profundidade_unitario' => $sku['Sku']['profundidade'],
						'qtd' => $sku['qtd']
					);

					// Verificando se este sku se encontra em pré-venda
					if ($sku['pre_venda']) {
						$tem_produto_prevenda = true;
						$pedido['PedidoSku'][$i]['situacao_estoque'] = 'PreVenda';
						$pedido['PedidoSku'][$i]['previsao_entrega_pre_venda'] = $sku['Sku']['previsao_entrega_pre_venda'];
					} else {
						$pedido['PedidoSku'][$i]['situacao_estoque'] = 'Reserva';

						// Removendo do estoque os itens que estão entrando em reserva
						$nova_qtd = $sku['Sku']['qtd_estoque'] - $sku['qtd'];
						$this->Sku->id = $sku['Sku']['id'];
						$this->Sku->saveField('qtd_estoque', $nova_qtd);
					}

					// Verificando se existe alguma promoção de desconto no sku e adicionando a mesma no ProdutoSku
					if (isset($sku['Promocao']['melhor_desconto_produto']['promocao'])) {
						if (!isset($pedido['PedidoSku'][$i]['Promocao'])) {
							$pedido['PedidoSku'][$i]['Promocao'] = array();
						}

						$promocao = $sku['Promocao']['melhor_desconto_produto']['promocao'];

						// Prepara promoção no array para salvar junto com o pedido
						$pedido['PedidoSku'][$i]['Promocao'][] = $this->pedido_promocao($promocao);
					}

					// Verificando se existe alguma promoção leve x e pague y e adicionando a mesma no ProdutoSku
					if (isset($sku['Promocao']['melhor_leve_x_pague_y']['promocao'])) {
						if (!isset($pedido['PedidoSku'][$i]['Promocao'])) {
							$pedido['PedidoSku'][$i]['Promocao'] = array();
						}

						$promocao = $sku['Promocao']['melhor_leve_x_pague_y']['promocao'];

						// Prepara promoção no array para salvar junto com o pedido
						$pedido['PedidoSku'][$i]['Promocao'][] = $this->pedido_promocao($promocao);
					}

					// Verificando se existe alguma promocao compre junto e adicionado-as no PedidoSku
					if (isset($sku['Promocao']['melhor_compre_junto']['promocao'])) {
						$promocao = $sku['Promocao']['melhor_compre_junto']['promocao'];

						// Adicionando as afinidades ao PedidoSku
						if (!isset($pedido['PedidoSku'][$i]['AfinidadeProduto'])) {
							$pedido['PedidoSku'][$i]['AfinidadeProduto'] = array();
						}

						$afinidadeProdutos = array('AfinidadeProduto' => array());
						foreach ($promocao['AfinidadeProduto'] as $key => $afinidadeProduto) {
							unset($afinidadeProduto['id']);

							$pedido['PedidoSku'][$i]['AfinidadeProduto'][] = $afinidadeProduto;
						}

						// Adicionando os compre junto ao PedidoSku
						if (!isset($pedido['PedidoSku'][$i]['CompreJuntoPromocao'])) {
							$pedido['PedidoSku'][$i]['CompreJuntoPromocao'] = array();
						}
						foreach ($promocao['CompreJuntoPromocao'] as $key => $compre_junto_completo) {
							$compre_junto_promocao = $compre_junto_completo['compre_junto_promocao']['CompreJuntoPromocao'];
							$compre_junto_promocao['pedido_sku_valor'] = $compre_junto_completo['valor'];
							$compre_junto_promocao['pedido_sku_qtd'] = $compre_junto_completo['qtd_compre_junto'];
							unset($compre_junto_promocao['id']);

							$pedido['PedidoSku'][$i]['CompreJuntoPromocao'][] = $compre_junto_promocao;
						}
					}

					// Características do produto
					if (count($sku['Produto']['CaracteristicasValoresSelecionado'])) {
						foreach ($sku['Produto']['CaracteristicasValoresSelecionado'] as $caracteristica) {
							$valor = '';
							if ($caracteristica['Caracteristica']['tipo'] == 'T') {
								if (isset($caracteristica['descricao'])) {
									$valor = $caracteristica['descricao'];
								}
							} else if ($caracteristica['Caracteristica']['tipo'] == 'TG') {
								if (isset($caracteristica['descricao_grande'])) {
									$valor = $caracteristica['descricao_grande'];
								}
							} else {
								if (isset($caracteristica['CaracteristicasValor']['descricao'])) {
									$valor = $caracteristica['CaracteristicasValor']['descricao'];
								}
							}
							$pedido['PedidoSku'][$i]['PedidoCaracteristica'][] = array(
								'referente' => 'P',
								'caracteristica' => $caracteristica['Caracteristica']['descricao'],
								'valor' => $valor
							);
						}
					}

					// Características do sku
					if (count($sku['CaracteristicasValoresSelecionado'])) {
						foreach ($sku['CaracteristicasValoresSelecionado'] as $caracteristica) {
							$valor = '';
							if ($caracteristica['Caracteristica']['tipo'] == 'T') {
								if (isset($caracteristica['descricao'])) {
									$valor = $caracteristica['descricao'];
								}
							} else if ($caracteristica['Caracteristica']['tipo'] == 'TG') {
								if (isset($caracteristica['descricao_grande'])) {
									$valor = $caracteristica['descricao_grande'];
								}
							} else {
								if (isset($caracteristica['CaracteristicasValor']['descricao'])) {
									$valor = $caracteristica['CaracteristicasValor']['descricao'];
								}
							}
							$pedido['PedidoSku'][$i]['PedidoCaracteristica'][] = array(
								'referente' => 'S',
								'caracteristica' => $caracteristica['Caracteristica']['descricao'],
								'valor' => $valor
							);
						}
					}

					// Serviços
					if (isset($sku['Servico']) && count($sku['Servico']) > 0) {
						foreach ($sku['Servico'] as $servico) {

							if (isset($servico['ServicoAnexo'])) {
								foreach ($servico['ServicoAnexo'] as $anexo) {
									$pedido['PedidoSku'][$i]['PedidoSkuServico'][] = array(
										'servico' => $servico['nome'],
										'valor' => $servico['valor'] == '' ? 0 : $servico['valor'],
										'anexo' => $anexo['nome'],
										'tipo_anexo' => $anexo['tipo'],
										'valor_anexo' => $anexo['AnexoValor']['valor']
									);
								}
							} else {
								$pedido['PedidoSku'][$i]['PedidoSkuServico'][] = array(
									'servico' => $servico['nome'],
									'valor' => $servico['valor'] == '' ? 0 : $servico['valor'],
									'anexo' => '',
									'tipo_anexo' => '',
									'valor_anexo' => ''
								);
							}
						}
					}

					$i++;
				}

				$pedido['Pedido']['valor_produtos'] = $pedido_valores['valor_produtos'];
				$pedido['Pedido']['valor_servicos'] = $pedido_valores['valor_servicos'];
				$pedido['Pedido']['valor_desconto'] = $pedido_valores['valor_desconto'];
				$pedido['Pedido']['valor_total'] = $pedido_valores['valor_total'];

				// Caso tenha produtos em pré-venda, o pedido fica com a flag de estoque disponível marcada como false;
				$pedido['Pedido']['estoque_disponivel'] = !$tem_produto_prevenda;

				// Calculando o valor total do pedido baseado no frete transportadora
				$pedido['Pedido']['valor_total'] = $frete_transportadora['FreteTransportadora']['valor_frete'] + $pedido['Pedido']['valor_total'];

				$this->Pedido->create();
				if ($this->Pedido->saveAssociated($pedido, array('deep' => true))) {
					$this->Session->write('Pedido.id', $this->Pedido->id);
					$this->Session->delete('Carrinho');

					if ($this->Configuracao->get('moip_ativo') || $this->Configuracao->get('moip_boleto_ativo')) {
						$this->pagamentoMoipCriarTransacao($this->Pedido->id);
					}

					$this->redirect(array('controller' => 'checkout', 'action' => 'pagamento'));
				} else {
					$this->Session->setFlash('Ocorreu um erro ao tentar salvar o pedido. Por favor, tente novamente. Caso o problema persita, por favor entre em contato.', FLASH_ERROR);
				}

			} else {
				$this->Session->setFlash('O endereço selecionado não existe. Por favor selecione um endereço.', FLASH_ERROR);
			}
		}

		$enderecos = $this->Endereco->find('all', array(
			'contain' => array(
				'Cidade' => array('nome', 'uf'),
				'Cidade.Estado.nome'
			),
			'conditions' => array(
				'Endereco.ativo',
				'Endereco.cliente_id' => $this->usuarioAutenticado['id']
			),
			'order' => array(
				'Endereco.created' => 'DESC'
			)
		));

		// Calculando o frete para cada um dos endereços
		$enderecos_entrega = array(); // variavel apenas para montar o array com o id do endereço na chave
		foreach ($enderecos as $key => $endereco) {

			$cep = str_replace('-', '', $endereco['Endereco']['cep']);

			/* Promoções */
			$skus = $this->Session->read('Carrinho.Skus');
			$cupom = $this->Session->read('Carrinho.Cupom');

			// Injetar promoções nos SKUS
			$skus = $this->aplicar_promocoes($skus, $cep, $cupom);

			// Calculando desconto baseado em promoções dos produtos (desconto)
			$skus = $this->RegrasPromocoesProduto->definir_promocoes_skus($skus);

			// Atualizando o preço dos produtos com base nas promoções de desconto no produto e compre junto
			$skus = $this->RegrasPromocoesPrecos->recalcular($skus);

			// Calculando desconto baseado em promoções de compre junto
			$skus = $this->RegrasPromocoesCompreJunto->definir_promocoes_skus($skus);

			// Calculando desconto baseado em promoções Leve X, Pague Y
			$skus = $this->RegrasPromocoesLeveXPagueY->definir_promocoes_skus($skus);

			// Recalcular valores totais dos produtos (preco e peso)
			$valor_sub_total = $this->calcular_sub_total($skus);

			// Recalcular o valor subtotal se o cupom for preenchido
			$valor_sub_total = $this->aplicar_promocoes_cupom($valor_sub_total, $skus);
			$pedido_valores = $valor_sub_total;

			/* Fim - Promoções */

			//Buscando as transportadoras e servicos dos correios que atendem o contexto atual do carrinho
			$frete_transportadoras_correios = $this->FreteCorreios->buscar($skus, $cep);
			$frete_transportadoras_transp = $this->FreteTransportadoras->buscar($skus, $cep);
			$frete_transportadoras = array_merge($frete_transportadoras_correios, $frete_transportadoras_transp);

			foreach ($frete_transportadoras as $key_transportadora => $frete_transportadora) {
				$frete_transportadoras[$key_transportadora] = $this->RegrasPromocoesFrete->calcular_valor_frete($frete_transportadora, $skus, $cep);
			}

			$enderecos_entrega[$endereco['Endereco']['id']] = $endereco;
			$enderecos_entrega[$endereco['Endereco']['id']]['FreteTransportadora'] = $frete_transportadoras;
			$enderecos_entrega[$endereco['Endereco']['id']]['Skus'] = $skus;
			$enderecos_entrega[$endereco['Endereco']['id']]['Pedido'] = $pedido_valores;
		}

		$enderecos = $enderecos_entrega;

		// Mantendo os endereços e transportadoras na sessão
		$this->Session->write('Checkout.Enderecos', $enderecos);

		// Calculando valor sub total do pedido
		$this->set(compact('pedido_valores', 'enderecos'));
	}

	public function pagamento($pedido_id = null) {

		if (empty($pedido_id)) {
			$pedido_id = $this->Session->read('Pedido.id');
		}

		$pedido = $this->Pedido->find('first', array(
			'contain' => array(
				'PedidoSku.Sku' => array(
					'Imagem',
					'Produto.Imagem'
				),
				'Endereco' => array(
					'Cidade' => array('nome', 'uf')
				),
				'Cliente',
				'FreteTransportadora'
			),
			'conditions' => array(
				'Pedido.id' => $pedido_id
			)
		));

		$total_pedido = $pedido['Pedido']['valor_total'];

		$this->set(compact('pedido', 'total_pedido'));

		// Caso pagamento com Cielo esteja habilitado fornece os dados das bandeiras
		if ($this->Configuracao->get('cielo_ativo')) {
			$cielo_bandeiras = $this->CieloBandeira->find('all', array(
				'contain' => array(
					'Parcela' => array(
						'conditions' => array(
							'Parcela.ativo'
						)
					)
				),
				'conditions' => array(
					'CieloBandeira.ativo'
				)
			));

			// TODO - Se não for post não muda o status
			$status_pedido = $this->StatusPedido->find('first', array(
				'conditions' => array(
					'StatusPedido.ativo',
					'StatusPedido.status_inicial'
				)
			));

			if ($status_pedido) {
				$this->Pedido->id = $pedido_id;
				$this->Pedido->saveField('status_id', $status_pedido['StatusPedido']['id']);
				$this->RegrasStatus->status_alterado($this->Pedido->id);
			}

			$this->set(compact('cielo_bandeiras'));

			if ($this->Configuracao->get('moip_boleto_ativo')) {
				$this->pagamentoCieloBoletoMoip($pedido);
			}
		}

		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->request->data('Cielo.tipo') == 'cielo') {
				$this->pagamentoCielo($pedido_id);
			}
		}

		if (Configure::read('Sistema.Checkout.PagSeguro.ativo')) {
			$this->pagamentoPagSeguro($pedido_id);
		}

		if ($this->Configuracao->get('moip_ativo')) {
			$this->pagamentoMoip($pedido);
		}

		if ($this->Configuracao->get('pagseguro_transparente_ativo')) {
			$this->pagamentoPagSeguroTransparente($pedido);
		}
	}

	private function pagamentoCielo($pedido_id) {

		$pedido = $this->Pedido->find('first', array(
			'contain' => array(
				'PedidoSku',
				'Endereco' => array(
					'Cidade' => array('nome', 'uf')
				),
				'Cliente.tel_residencial'
			),
			'conditions' => array(
				'Pedido.id' => $pedido_id
			)
		));

		$total_pedido = $pedido['Pedido']['valor_total'];

		$parcela = $this->Parcela->find('first', array(
			'contain' => false,
			'conditions' => array(
				'Parcela.id' => $this->request->data('Cielo.parcela_id')
			)
		));

		$this->Cielo->buy_page_cielo = false;
		$this->Cielo->autorizar = 3;
		$this->Cielo->capturar = false;

		$this->Cielo->xml_id = uniqid();

		$this->Cielo->cc_numero = $this->request->data('Cielo.cc_numero');
		$this->Cielo->cc_validade = $this->request->data('Cielo.cc_validade_ano').$this->request->data('Cielo.cc_validade_mes');
		$this->Cielo->cc_codigo_seguranca = $this->request->data('Cielo.cc_codigo_seguranca');
		$this->Cielo->cc_bandeira = $this->request->data('Cielo.cc_bandeira');
		$this->Cielo->cc_produto = $parcela['Parcela']['cielo_produto'];

		$this->Cielo->pedido_id = $pedido['Pedido']['id'];
		$this->Cielo->pedido_valor = $this->Cielo->converterValor($total_pedido);
		$this->Cielo->pedido_data_hora = $this->Cielo->converterData($pedido['Pedido']['data_hora']);
		$this->Cielo->pagamento_qtd_parcelas = $parcela['Parcela']['parcelas'];

		$this->Cielo->finalizar();

		$this->Pedido->id = $pedido_id;
		$this->Pedido->saveField('forma_pagamento', 'Cielo');

		if ($this->Cielo->erro) {
			$erro = $this->Cielo->erro;

			$this->Pedido->saveField('retorno_cielo', serialize($erro));
			$this->Session->setFlash($this->Cielo->erro['mensagem'], FLASH_ERROR, array(), 'cielo');
		} else {
			$retorno = $this->Cielo->retornoTransacao();

			$this->Pedido->saveField('retorno_cielo', serialize($retorno));

			if ($retorno['transacao']['status'] == '4') {
				$status_pedido = $this->StatusPedido->find('first', array(
					'conditions' => array(
						'StatusPedido.ativo',
						'StatusPedido.status_cielo_trans_aut'
					)
				));

				if ($status_pedido) {
					$this->Pedido->saveField('status_id', $status_pedido['StatusPedido']['id']);
					$pedido = $this->RegrasStatus->status_alterado($this->Pedido->id);
				}

				// $this->Session->setFlash('Compra finalizada com sucesso. Em breve enviaremos o seu pedido.', FLASH_SUCCESS, array(), 'cielo');
				$this->Session->setFlash('Sua compra foi finalizada com sucesso e seu pagamento está em análise. Após aprovado enviaremos o seu pedido.', FLASH_SUCCESS, array(), 'cielo');
				$this->redirect(array('action' => 'compra_finalizada', '?' => array('meio' => 'Cielo', 'pedido' => $this->Pedido->id)));
			} else {
				if (in_array($retorno['transacao']['autorizacao']['lr'], array('06', '76', '91', '96', 'AA'))) {
					$this->Session->setFlash('Por solicitação da administradora, tente novamente. Se o problema persistir, entre em contato com a sua administradora de cartão.', FLASH_ERROR, array(), 'cielo');
				} else if (in_array($retorno['transacao']['autorizacao']['lr'], array('78'))) {
					$this->Session->setFlash('Desbloqueie seu cartão junto ao emissor.', FLASH_ERROR, array(), 'cielo');
				} else {
					$status_pedido = $this->StatusPedido->find('first', array(
						'conditions' => array(
							'StatusPedido.ativo',
							'StatusPedido.status_cielo_trans_nao_aut'
						)
					));

					if ($status_pedido) {
						$this->Pedido->saveField('status_id', $status_pedido['StatusPedido']['id']);
						$pedido = $this->RegrasStatus->status_alterado($this->Pedido->id);
					}
					$this->Session->setFlash('Autorização de pagamento negada. Entre em contato com a administradora do seu cartão.', FLASH_ERROR, array(), 'cielo');
				}
			}
		}
	}

	private function pagamentoPagSeguro($pedido_id) {
		$this->Pedido->id = $pedido_id;
		$this->Pedido->saveField('forma_pagamento', 'PagSeguro');

		$status_pedido = $this->StatusPedido->find('first', array(
			'conditions' => array(
				'StatusPedido.ativo',
				'StatusPedido.status_inicial'
			)
		));

		if ($status_pedido) {
			$this->Pedido->saveField('status_id', $status_pedido['StatusPedido']['id']);
			$this->RegrasStatus->status_alterado($this->Pedido->id);
		}

		$pedido = $this->Pedido->find('first', array(
			'contain' => array(
				'PedidoSku',
				'Endereco' => array(
					'Cidade' => array('nome', 'uf')
				),
				'Cliente.tel_residencial'
			),
			'conditions' => array(
				'Pedido.id' => $pedido_id
			)
		));

		if (strlen($pedido['Cliente']['tel_residencial']) == 10 || strlen($pedido['Cliente']['tel_residencial']) == 11) {
			$ddd = substr($pedido['Cliente']['tel_residencial'], 0, 2);
			$telefone = substr($pedido['Cliente']['tel_residencial'], 2, strlen($pedido['Cliente']['tel_residencial']));
		} else {
			$ddd = substr($pedido['Cliente']['tel_residencial'], 1, 2);
			$telefone = substr($pedido['Cliente']['tel_residencial'], 4, strlen($pedido['Cliente']['tel_residencial']));
			$telefone = str_replace('-', '', $telefone);
		}

		$pedido['Cliente']['ddd'] = $ddd;
		$pedido['Cliente']['telefone'] = $telefone;

		/* API PagSeguro */

		// definindo suas credenciais
		$this->Carrinho->setCredenciais(Configure::read('Sistema.Checkout.PagSeguro.email'), Configure::read('Sistema.Checkout.PagSeguro.token'));

		// definindo a URL de retorno ao realizar o pagamento (opcional)
		$this->Carrinho->setUrlRetorno(Configure::read('Sistema.Checkout.PagSeguro.url_retorno')); // Voltar para confirmação de pagamento

		// definindo a referência da compra (opcional)
		$this->Carrinho->setReferencia($pedido['Pedido']['id']);

		/**
		*   adicionarItem method
		*   @param int id  OBRIGATÓRIO
		*   @param string descricao OBRIGATÓRIO
		*   @param string valorUnitario (formato 0.00 -> ponto como separador
		*       de centavos, e nada separando milhares) OBRIGATÓRIO
		*   @param string peso (formato em gramas ex: 1KG = 1000) OBRIGATÓRIO
		*   @param int quantidade OBRIGATÓRIO (padrão 1 unidade)
		*   @param string frete (valor do frete formato 0.00 -> ponto como separador
		*       de centavos e nada separando milhares) OPCIONAL
		*/

		$item = 1;
		foreach($pedido['PedidoSku'] as $sku) {
			$preco_unitario = $sku['preco_unitario_desconto'] > 0 ? $sku['preco_unitario_desconto'] : $sku['preco_unitario'];

			$this->Carrinho->adicionarItem(
				utf8_decode($sku['sku']),
				utf8_decode($sku['descricao_produto'] . ' - ' . $sku['descricao_sku']),
				utf8_decode($preco_unitario),
				utf8_decode($sku['peso_total']),
				utf8_decode($sku['qtd'])
			);
			$item++;
		}

		// + Serviços - Desconto
		if ($pedido['Pedido']['valor_desconto'] != '') {
			$this->Carrinho->setValorExtra($pedido['Pedido']['valor_servicos']-$pedido['Pedido']['valor_desconto']);
		}

		// definindo o contato do comprador
		$this->Carrinho->setContatosComprador(
			utf8_decode($this->usuarioAutenticado['nome']),
			utf8_decode($this->usuarioAutenticado['email']),
			utf8_decode($pedido['Cliente']['ddd']),
			utf8_decode($pedido['Cliente']['telefone'])
		);

		// definindo o endereço do comprador
		$this->Carrinho->setEnderecoComprador(
			utf8_decode(str_replace('-', '', $pedido['Endereco']['cep'])),
			utf8_decode($pedido['Endereco']['endereco']),
			utf8_decode($pedido['Endereco']['numero']),
			utf8_decode($pedido['Endereco']['complemento']),
			utf8_decode($pedido['Endereco']['bairro']),
			utf8_decode($pedido['Endereco']['Cidade']['nome']),
			utf8_decode($pedido['Endereco']['Cidade']['uf'])
		);

		/**
		*   setValorFrete method OPCIONAL
		*
		*   @param string valorTotalFrete (formato 0.00 -> ponto como separador de
		*       centavos e nada separando milhares)
		*
		*/
		//$this->Carrinho->setValorTotalFrete('32.00');
		$this->Carrinho->setValorTotalFrete($pedido['Pedido']['valor_frete']);

		// e finalmente se os dados estivere corretos, redirecionando ao Pagseguro
		if ($result = $this->Carrinho->finalizaCompra()) {
			$this->redirect($result);
		}

		$this->set('pedido', $pedido);
	}

	private function pagamentoPagSeguroTransparente($pedido) {
		$this->Pedido->id = $pedido['Pedido']['id'];
		$this->Pedido->saveField('forma_pagamento', 'PagSeguroTransparente');

		$status_pedido = $this->StatusPedido->find('first', array(
			'conditions' => array(
				'StatusPedido.ativo',
				'StatusPedido.status_inicial'
			)
		));

		if ($status_pedido) {
			$this->Pedido->saveField('status_id', $status_pedido['StatusPedido']['id']);
			$this->RegrasStatus->status_alterado($this->Pedido->id);
		}

		$HttpSocket = new HttpSocket();

		$credenciais = array(
			'email' => $this->Configuracao->get('pagseguro_transparente_email'),
			'token' => $this->Configuracao->get('pagseguro_transparente_token')
		);

		$xml = $HttpSocket->post('https://ws.' . $this->Configuracao->get('pagseguro_transparente_url') . '/v2/sessions', $credenciais);
		$session = Xml::toArray(Xml::build($xml->body));
		$session_id = $session['session']['id'];
		$bandeiras_pagseguro_cc = $this->PagseguroTransparenteBandeira->find('all', array(
			'conditions' => array(
				'PagseguroTransparenteBandeira.ativo',
				'PagseguroTransparenteBandeira.tipo' => 'CartaoCredito'
			)
		));

		$bandeiras_pagseguro_debito = $this->PagseguroTransparenteBandeira->find('all', array(
			'conditions' => array(
				'PagseguroTransparenteBandeira.ativo',
				'PagseguroTransparenteBandeira.tipo' => 'DebitoBancario'
			)
		));

		$this->set(compact('session_id', 'bandeiras_pagseguro_cc', 'bandeiras_pagseguro_debito', 'pedido'));
	}

	public function ajaxPagamentoPagSeguroTransparenteEnviarTransacao() {
		$HttpSocket = new HttpSocket();

		if ($this->request->is('post') || $this->request->is('put')) {

			$pedido = $this->Pedido->find('first', array(
				'contain' => array(
					'PedidoSku',
					'Endereco' => array(
						'Cidade' => array('nome', 'uf')
					),
					'Cliente'
				),
				'conditions' => array(
					'Pedido.id' => $this->request->data['PagSeguroTransparente']['pedido_id']
				)
			));

			// Dados do pagamento via pagSeguro
			$params = array(
				'email' => $this->Configuracao->get('pagseguro_transparente_email'),
				'token' => $this->Configuracao->get('pagseguro_transparente_token'),
				'paymentMode' => 'default',
				'paymentMethod' => $this->request->data['PagSeguroTransparente']['meio_pagamento'], //creditCard, boleto ou eft.
				'notificationURL' => $this->Configuracao->get('pagseguro_transparente_url_notificacao'),
				'extraAmount' =>  number_format($pedido['Pedido']['valor_servicos'] - $pedido['Pedido']['valor_desconto'], 2), // - Desconto e + Serviço
				//'receiverEmail' => '',
				//'shippingType' => '', // Frete (Tratado pela plataforma)
				'shippingCost' => $pedido['Pedido']['valor_frete'], // Frete
			);

			// Dados do Pedido
			$params = $params + array(
				'reference' => $pedido['Pedido']['id'],
				'currency' => 'BRL'
			);

			$item = 1;
			foreach($pedido['PedidoSku'] as $sku) {
				$params = $params + array(
					'itemId' . $item => $sku['sku'],
					'itemDescription' . $item => substr($sku['descricao_produto'] . ' - ' . $sku['descricao_sku'], 0, 100),
					'itemAmount' . $item => (isset($sku['preco_unitario_desconto']) && $sku['preco_unitario_desconto'] > 0) ? $sku['preco_unitario_desconto'] : $sku['preco_unitario'],
					'itemQuantity' . $item => $sku['qtd']
				);

				$item++;
			}

			if ($pedido['Cliente']['tipo_pessoa'] == 'F') {

				$nome = $pedido['Cliente']['nome'];

				// Ajustar Telefone
				if (strlen($pedido['Cliente']['tel_residencial']) == 10 || strlen($pedido['Cliente']['tel_residencial']) == 11) {
					$ddd = substr($pedido['Cliente']['tel_residencial'], 0, 2);
					$telefone = substr($pedido['Cliente']['tel_residencial'], 2, strlen($pedido['Cliente']['tel_residencial']));
				} else {
					$ddd = substr($pedido['Cliente']['tel_residencial'], 1, 2);
					$telefone = substr($pedido['Cliente']['tel_residencial'], 4, strlen($pedido['Cliente']['tel_residencial']));
					$telefone = str_replace('-', '', $telefone);
				}

				// Ajustar data de aniversário
				$data_nascto = explode('-', $pedido['Cliente']['data_nascto']);
				$pedido['Cliente']['data_nascto'] = $data_nascto[2] . '/' . $data_nascto[1] . '/' . $data_nascto[0];

				// Ajustar CPF
				$pedido['Cliente']['cpf'] = str_replace('-', '', $pedido['Cliente']['cpf']);
				$pedido['Cliente']['cpf'] = str_replace('.', '', $pedido['Cliente']['cpf']);

				$params = $params + array(
					'senderCPF' => $pedido['Cliente']['cpf']
				);
			} else {

				$nome = $pedido['Cliente']['nome_fantasia'];

				// Ajustar Telefone
				if (strlen($pedido['Cliente']['tel_comercial_pj']) == 10 || strlen($pedido['Cliente']['tel_comercial_pj']) == 11) {
					$ddd = substr($pedido['Cliente']['tel_comercial_pj'], 0, 2);
					$telefone = substr($pedido['Cliente']['tel_comercial_pj'], 2, strlen($pedido['Cliente']['tel_comercial_pj']));
				} else {
					$ddd = substr($pedido['Cliente']['tel_comercial_pj'], 1, 2);
					$telefone = substr($pedido['Cliente']['tel_comercial_pj'], 4, strlen($pedido['Cliente']['tel_comercial_pj']));
					$telefone = str_replace('-', '', $telefone);
				}

				// Ajustar CNPJ
				$pedido['Cliente']['cnpj'] = str_replace('-', '', $pedido['Cliente']['cnpj']);
				$pedido['Cliente']['cnpj'] = str_replace('.', '', $pedido['Cliente']['cnpj']);
				$pedido['Cliente']['cnpj'] = str_replace('/', '', $pedido['Cliente']['cnpj']);

				$params = $params + array(
					'senderCNPJ' => $pedido['Cliente']['cnpj']
				);
			}

			$pedido['Cliente']['ddd'] = $ddd;
			$pedido['Cliente']['telefone'] = $telefone;
			$pedido['Cliente']['nome'] = $nome;

			// Dados do comprador
			$params = $params + array(
				'senderName' => $pedido['Cliente']['nome'],
				'senderAreaCode' => $pedido['Cliente']['ddd'],
				'senderPhone' => $pedido['Cliente']['telefone'],
				'senderEmail' =>  $pedido['Cliente']['email'],
				'senderHash' => $this->request->data['PagSeguroTransparente']['sender_hash'],
			);

			// Endereço de Entrega
			$params = $params + array(
				'shippingAddressStreet' => $pedido['Endereco']['endereco'],
				'shippingAddressNumber' => $pedido['Endereco']['numero'],
				'shippingAddressComplement' => $pedido['Endereco']['complemento'],
				'shippingAddressDistrict' => $pedido['Endereco']['bairro'],
				'shippingAddressPostalCode' => str_replace('-', '', $pedido['Endereco']['cep']),
				'shippingAddressCity' => $pedido['Endereco']['Cidade']['nome'],
				'shippingAddressState' => $pedido['Endereco']['Cidade']['uf'],
				'shippingAddressCountry' => 'BRA'
			);

			if ($this->request->data['PagSeguroTransparente']['meio_pagamento'] == 'eft') {
				$params = $params + array(
					'bankName' => $this->request->data['PagSeguroTransparente']['etf_bank']
				);
			}

			// CARTÃO DE CRÉDITO

			if ($this->request->data['PagSeguroTransparente']['meio_pagamento'] == 'creditCard') {

				// Parcelas, Vaolres e Token
				$params = $params + array(
					'installmentQuantity' => $this->request->data['PagSeguroTransparente']['cc_parcelas'],
					'installmentValue' => $this->request->data['PagSeguroTransparente']['cc_valor_parcelas'],
					'creditCardToken' => $this->request->data['PagSeguroTransparente']['cc_token']
				);

				// Ajustar Telefone
				if (strlen($this->request->data['DonoCartao']['telefone']) == 10 || strlen($this->request->data['DonoCartao']['telefone']) == 11) {
					$cc_ddd = substr($this->request->data['DonoCartao']['telefone'], 0, 2);
					$cc_telefone = substr($this->request->data['DonoCartao']['telefone'], 2, strlen($this->request->data['DonoCartao']['telefone']));
				} else {
					$cc_ddd = substr($this->request->data['DonoCartao']['telefone'], 1, 2);
					$cc_telefone = substr($this->request->data['DonoCartao']['telefone'], 4, strlen($this->request->data['DonoCartao']['telefone']));
					$cc_telefone = str_replace('-', '', $cc_telefone);
				}

				$this->request->data['DonoCartao']['ddd'] = $cc_ddd;
				$this->request->data['DonoCartao']['telefone'] = $cc_telefone;

				// Ajustar CPF
				$this->request->data['DonoCartao']['cpf'] = str_replace('-', '', $this->request->data['DonoCartao']['cpf']);
				$this->request->data['DonoCartao']['cpf'] = str_replace('.', '', $this->request->data['DonoCartao']['cpf']);

				$params = $params + array(
					'creditCardHolderName' => $this->request->data['DonoCartao']['nome'],
					'creditCardHolderCPF' => $this->request->data['DonoCartao']['cpf'],
					'creditCardHolderBirthDate' => $this->request->data['DonoCartao']['data_nascto'],
					'creditCardHolderAreaCode' => $this->request->data['DonoCartao']['ddd'],
					'creditCardHolderPhone' => $this->request->data['DonoCartao']['telefone']
				);

				// Endereço de cobrança
				$params = $params + array(
					'billingAddressStreet' => $pedido['Endereco']['endereco'],
					'billingAddressNumber' => $pedido['Endereco']['numero'],
					'billingAddressComplement' => $pedido['Endereco']['complemento'],
					'billingAddressDistrict' => $pedido['Endereco']['bairro'],
					'billingAddressPostalCode' => str_replace('-', '', $pedido['Endereco']['cep']),
					'billingAddressCity' => $pedido['Endereco']['Cidade']['nome'],
					'billingAddressState' => $pedido['Endereco']['Cidade']['uf'],
					'billingAddressCountry' => 'BRA'
				);
			}

			$xml = $HttpSocket->post('https://ws.' . $this->Configuracao->get('pagseguro_transparente_url') . '/v2/transactions', array_map('utf8_decode', $params));
			$json = json_encode(Xml::build($xml->body));

			$meio_pagamento = $this->getMeioPagamento();

			$data = array(
				'dados_enviados_pagseguro' => json_encode($params),
				'retorno_pagseguro' => $json,
				'forma_pagseguro' => $meio_pagamento[$this->request->data['PagSeguroTransparente']['meio_pagamento']]
			);

			$this->Pedido->id = $pedido['Pedido']['id'];
			$this->Pedido->save($data);

			$result = Xml::toArray(Xml::build($xml->body));
			$this->renderJson($result);
		}
	}

	public function notificacao_pagseguro() {
		$this->autoRender = false;

		if ($this->Configuracao->get('pagseguro_transparente_ativo')) {
			$email = $this->Configuracao->get('pagseguro_transparente_email');
			$token = $this->Configuracao->get('pagseguro_transparente_token');
		} else {
			$email = Configure::read('Sistema.Checkout.PagSeguro.email');
			$token = Configure::read('Sistema.Checkout.PagSeguro.token');
		}

		$this->Notificacao->setCredenciais($email, $token);

		$tipo = $this->request->data['notificationType'];
		$codigo = $this->request->data['notificationCode'];

		if ($this->Notificacao->obterDadosTransacao($tipo, $codigo)) {
			$dadosUsuario = $this->Notificacao->obterDadosUsuario(); // retorna somente os dados do comprador
			$statusTransacao = $this->Notificacao->obterStatusTransacao(); // retorna o status da transação
			$dadosPagamento = $this->Notificacao->obterDadosPagamento(); // retorna os dados de pagamento (tipo de pagamento e forma de pagamento)
			$dataTransacao = $this->Notificacao->obterDataTransacao(); // retorna a data que a compra foi realizada e última notificação
			$produtos = $this->Notificacao->obterValores(); // retorna os dados de produtos comprados
			$codigoTransacao = $this->Notificacao->obterCodigoTransacao(); // retorna o id da transação para consulta e/ou atualização

			// agora exibindo todos os resultados (para efeito de testes)
			debug($dadosUsuario); $this->log($codigo, 'notificacao_pagseguro');
			debug($statusTransacao); $this->log($statusTransacao, 'notificacao_pagseguro');
			debug($dadosPagamento); $this->log($dadosPagamento, 'notificacao_pagseguro');
			debug($dataTransacao); $this->log($dataTransacao, 'notificacao_pagseguro');
			debug($produtos); $this->log($produtos, 'notificacao_pagseguro');
			debug($codigoTransacao); $this->log($codigoTransacao, 'notificacao_pagseguro');


			$pedido_id = $produtos['referencia'];

			$pedido = $this->Pedido->find('first', array(
				'contain' => false,
				'conditions' => array(
					'Pedido.id' => $pedido_id
				)
			));

			$this->Pedido->id = $pedido_id;
			//$this->Pedido->saveField('forma_pagamento', 'PagSeguro');

			$retorno_pagseguro = array(
				'dados_usuario' => $dadosUsuario,
				'status_transacao' => $statusTransacao,
				'dados_pagamento' => $dadosPagamento,
				'data_transacao' => $dataTransacao,
				'produtos' => $produtos,
				'codigo_transacao' => $codigoTransacao,
			);

			if ($pedido['Pedido']['retorno_pagseguro']) {
				$retornos = unserialize($pedido['Pedido']['retorno_pagseguro']);
			} else {
				$retornos = array();
			}
			$retornos[] = $retorno_pagseguro;
			$this->Pedido->saveField('retorno_pagseguro', serialize($retornos));

			if ($statusTransacao == 'Aguardando pagamento' || $statusTransacao == 'Em análise') {
				$status = 'StatusPedido.status_inicial';
			} else if ($statusTransacao == 'Paga') {
				$status = 'StatusPedido.status_pagseg_trans_aut';
			} else if ($statusTransacao == 'Disponível') {
				$status = 'StatusPedido.status_pagseg_disponivel';
			} else if ($statusTransacao == 'Em disputa') {
				$status = 'StatusPedido.status_pagseg_em_disputa';
			} else if ($statusTransacao == 'Devolvida') {
				$status = 'StatusPedido.status_pagseg_devolvida';
			} else if ($statusTransacao == 'Cancelada') {
				$status = 'StatusPedido.status_cancelado';
			}

			$status_pedido = $this->StatusPedido->find('first', array(
				'conditions' => array(
					'StatusPedido.ativo',
					$status
				)
			));

			if ($status_pedido) {
				$this->Pedido->saveField('status_id', $status_pedido['StatusPedido']['id']);
				$pedido = $this->RegrasStatus->status_alterado($this->Pedido->id);
			}
		}
	}

	/**
	  * Cria uma transação no moip para ser paga na sequência através do checkout transparent (javascript)
	  *
	  */
	private function pagamentoMoipCriarTransacao($pedido_id) {
		$pedido = $this->Pedido->find('first', array(
			'contain' => array(
				'Cliente',
				'Endereco' => array(
					'Cidade' => array('nome', 'uf')
				),
			),
			'conditions' => array(
				'Pedido.id' => $pedido_id
			)
		));

		$moip = new Moip();
		$moip->setEnvironment($this->Configuracao->get('moip_ambiente_producao'));
		$moip->setCredential(array(
			'key' => $this->Configuracao->get('moip_chave'),
			'token' => $this->Configuracao->get('moip_token')
		));

		$moip->setUniqueID($pedido['Pedido']['id']);

		// Calculando o valor máximo de parcelas
		$parcelas = $this->RegrasProdutos->calcular_parcelamento_maximo($pedido['Pedido']['valor_total'], $this->Configuracao->get('parc_parcelas'), $this->Configuracao->get('parc_valor_min_parcela'), $this->Configuracao->get('parc_juros'));

		// Verificando se há possibilidade de parcelar e se os juros serão cobrados do cliente
		$num_parcelas = array(1 => '1x' . number_format($pedido['Pedido']['valor_total'], 2, ',', '.'));
		if ($parcelas['parcelas'] > 1) {
			// o mínimo de parcelas sempre será 2, pois em uma vez não é considerado parcelamento
			$parc_min = 2;
			$parc_max = $parcelas['parcelas'];

			// Verificando se o juros será cobrado do cliente ou se a loja irá absorver esses juros
			if ($this->Configuracao->get('parc_juros_cliente')) {
				$moip->addParcel($parc_min, $this->Configuracao->get('parc_juros_min_parcela') - 1);
				$moip->addParcel($this->Configuracao->get('parc_juros_min_parcela'), $parc_max, $this->Configuracao->get('parc_juros'), true);
				for ($i = $parc_min; $i <= ($this->Configuracao->get('parc_juros_min_parcela') - 1); $i++) {
					$valor_parcela = $pedido['Pedido']['valor_total'] / $i;
					$num_parcelas[$i] = $i . 'x' . number_format($valor_parcela, 2, ',', '.');
				}

				for ($i = $this->Configuracao->get('parc_juros_min_parcela'); $i <= $parc_max; $i++) {
					$juros = $this->Configuracao->get('parc_juros') / 100;
					$valor_parcela = ($pedido['Pedido']['valor_total'] * $juros) / (1 - (1 / pow(1 + $juros, $i)));

					$num_parcelas[$i] = $i . 'x' . number_format($valor_parcela, 2, ',', '.') . ' com juros';
				}

			} else {
				$moip->addParcel($parc_min, $parc_max);
				for ($i = $parc_min; $i <= $parc_max; $i++) {
					$valor_parcela = $pedido['Pedido']['valor_total'] / $i;
					$num_parcelas[$i] = $i . 'x' . number_format($valor_parcela, 2, ',', '.');
				}
			}
		}

		$moip->setValue($pedido['Pedido']['valor_total']);
		$moip->setReason('Compra na loja ' . Configure::read('Loja.nome'));
		$moip->setPayer(array(
			'name' => $pedido['Cliente']['tipo_pessoa'] == 'F' ? $pedido['Cliente']['nome'] : $pedido['Cliente']['nome_fantasia'],
			'email' => $pedido['Cliente']['email'],
			'payerId' => $pedido['Cliente']['id'],
			'billingAddress' => array(
				'address' => $pedido['Endereco']['endereco'],
				'number' => $pedido['Endereco']['numero'],
				'complement' => $pedido['Endereco']['complemento'],
				'city' => $pedido['Endereco']['Cidade']['nome'],
				'neighborhood' => $pedido['Endereco']['bairro'],
				'state' => $pedido['Endereco']['Cidade']['uf'],
				'country' => 'BRA',
				'zipCode' => $pedido['Endereco']['cep'],
				'phone' => $pedido['Cliente']['tipo_pessoa'] == 'F' ? $pedido['Cliente']['tel_residencial'] : $pedido['Cliente']['tel_comercial_pj']
			)
		));

		$moip->validate('Identification');
		$moip->send();

		$xml = new SimpleXmlElement($moip->getAnswer(true));

        $dados_moip = array(
			'response' => $xml->Resposta->Status == 'Sucesso' ? true : false,
			'error' => $xml->Resposta->Status == 'Falha' ? (string)$xml->Resposta->Erro : false,
			'token' => (string) $xml->Resposta->Token,
			'parcelas' => $num_parcelas
		);

		$this->Pedido->id = $pedido_id;
		$this->Pedido->saveField('transacao_moip', serialize($dados_moip));

		return $dados_moip;

	}

	/**
	  * Pagamento com boleto Moip para Cielo
	  *
	  */
	private function pagamentoCieloBoletoMoip($pedido) {

		$this->Pedido->id = $pedido['Pedido']['id'];
		$this->Pedido->saveField('forma_pagamento', 'Moip');

		$dados_boleto_moip = unserialize($pedido['Pedido']['transacao_moip']);

		$data_nascto_dia = substr($pedido['Cliente']['data_nascto'], 8, 2);
		$data_nascto_mes = substr($pedido['Cliente']['data_nascto'], 5, 2);
		$data_nascto_ano = substr($pedido['Cliente']['data_nascto'], 0, 4);
		$cliente_boleto_moip = array(
			'Nome' => $pedido['Cliente']['nome'],
			'DataNascimento' => $data_nascto_dia . '/' . $data_nascto_mes . '/' . $data_nascto_ano,
			'Telefone' => $pedido['Cliente']['tel_residencial'],
			'Identidade' => $pedido['Cliente']['cpf'] ? $pedido['Cliente']['cpf'] : $pedido['Cliente']['cnpj']
		);

		$cliente_boleto_moip = json_encode($cliente_boleto_moip);

		$this->set(compact('dados_boleto_moip', 'cliente_boleto_moip'));
	}

	private function pagamentoMoip($pedido) {
		$status_pedido = $this->StatusPedido->find('first', array(
			'conditions' => array(
				'StatusPedido.ativo',
				'StatusPedido.status_inicial'
			)
		));

		if ($status_pedido) {
			$this->Pedido->id = $pedido['Pedido']['id'];
			$this->Pedido->saveField('status_id', $status_pedido['StatusPedido']['id']);
			$this->RegrasStatus->status_alterado($this->Pedido->id);
		}

		$dados_moip = unserialize($pedido['Pedido']['transacao_moip']);
		$instituicoes_moip_cc = $this->MoipInstituicao->find('all', array(
			'conditions' => array(
				'MoipInstituicao.ativo',
				'MoipInstituicao.tipo' => 'CartaoCredito'
			)
		));

		$data_nascto_dia = substr($pedido['Cliente']['data_nascto'], 8, 2);
		$data_nascto_mes = substr($pedido['Cliente']['data_nascto'], 5, 2);
		$data_nascto_ano = substr($pedido['Cliente']['data_nascto'], 0, 4);
		$cliente_moip = array(
			'Nome' => $pedido['Cliente']['nome'],
			'DataNascimento' => $data_nascto_dia . '/' . $data_nascto_mes . '/' . $data_nascto_ano,
			'Telefone' => $pedido['Cliente']['tel_residencial'],
			'Identidade' => $pedido['Cliente']['cpf'] ? $pedido['Cliente']['cpf'] : $pedido['Cliente']['cnpj']
		);

		$this->set(compact('dados_moip', 'instituicoes_moip_cc', 'cliente_moip'));
	}

	public function notificacaoMoip() {
		$this->autoRender = false;

		$statusTransacao = $this->request->data('status_pagamento');

		if ($statusTransacao == 1) {
			$status = 'StatusPedido.status_moip_pag_autorizado';
		} else if ($statusTransacao == 3) {
			$status = 'StatusPedido.status_moip_boleto_impresso';
		} else if ($statusTransacao == 4) {
			$status = 'StatusPedido.status_moip_pag_creditado';
		} else if ($statusTransacao == 5) {
			$status = 'StatusPedido.status_moip_pag_nao_autorizado';
		} else if ($statusTransacao == 7) {
			$status = 'StatusPedido.status_moip_pag_estornado';
		} else if ($statusTransacao == 9) {
			$status = 'StatusPedido.status_moip_pag_reembolsado';
		} else {
			$status = '';
		}

		if ($status) {
			$status_pedido = $this->StatusPedido->find('first', array(
				'contain' => false,
				'conditions' => array(
					'StatusPedido.ativo',
					$status
				)
			));
		} else {
			$status_pedido = array();
		}

		if ($status_pedido) {
			$pedido = array('Pedido' => array(
				'id' => $this->request->data('id_transacao'),
				'status_id' => $status_pedido['StatusPedido']['id'],
				'notificacao_moip' => json_encode($this->request->data)
			));

			$this->Pedido->save($pedido);

			$this->RegrasStatus->status_alterado($this->Pedido->id);
		}
	}

	public function login() {
		$this->CustomTagsParser->viewRender = '/Clientes/login';
	}

	public function cadastrar() {

		$campos_complementares = $this->CamposComplementar->find('all', array(
			'contain' => array(
				'CamposComplementaresValor' => array(
					'conditions' => array(
						'CamposComplementaresValor.ativo' => true
					)
				)
			),
			'conditions' => array(
				'CamposComplementar.ativo' => true,
				'OR' => array(
					'CamposComplementar.ativo' => 'PF',
					'CamposComplementar.ativo' => 'Pj'
				)
			)
		));

		$this->set('campos_complementares', $campos_complementares);

		$this->set('sexos', $this->getSexos());
		$this->CustomTagsParser->viewRender = '/Clientes/cadastrar';
	}

	public function esqueci_minha_senha() {
		$this->CustomTagsParser->viewRender = '/Clientes/esqueci_minha_senha';
	}

	public function compra_finalizada() {
		if ($this->request->is('post')) {
			// Moip
			if ($this->request->data('Moip')) {
				$pedido = array(
					'Pedido' => array(
						'id' => $this->request->data('Moip.pedido_id'),
						'forma_moip' => $this->request->data('Moip.forma'),
						'retorno_moip' => $this->request->data('Moip.retorno')
					)
				);

				$this->Pedido->save($pedido);

				$dados_moip = array(
					'pedido_id' => $this->request->data('Moip.pedido_id'),
					'forma' => $this->request->data('Moip.forma'),
					'retorno' => json_decode($this->request->data('Moip.retorno'))
				);

				$pedido = $this->Pedido->find('first', array(
					'contain' => array(
						'StatusPedido',
						'PedidoSku.Sku.Produto.Categoria'
					),
					'conditions' => array(
						'Pedido.id' => $this->request->data('Moip.pedido_id'),
						'Pedido.cliente_id' => $this->usuarioAutenticado['id'],
						'StatusPedido.status_finalizado' => false
					)
				));

				$this->set(compact('dados_moip', 'pedido'));

			}
		}

		// PagSeguro
		if (isset($this->request->query['pedido']) && (isset($this->request->query['meio']) && $this->request->query['meio'] == 'PagSeguroTransparente')) {
			$numero_pedido = $this->request->query['pedido'];

			$pedido = $this->Pedido->find('first', array(
				'contain' => array(
					'StatusPedido',
					'PedidoSku.Sku.Produto.Categoria'
				),
				'conditions' => array(
					'Pedido.id' => $numero_pedido,
					'Pedido.cliente_id' => $this->usuarioAutenticado['id'],
					'StatusPedido.status_finalizado' => false
				)
			));

			if (isset($pedido['Pedido']['retorno_pagseguro'])) {
				$dados_pagseguro = json_decode($pedido['Pedido']['retorno_pagseguro']);
			}

			$this->set(compact('pedido', 'dados_pagseguro'));
		}

		// Cielo
		if (isset($this->request->query['pedido']) && (isset($this->request->query['meio']) && $this->request->query['meio'] == 'Cielo')) {
			$numero_pedido = $this->request->query['pedido'];

			$pedido = $this->Pedido->find('first', array(
				'contain' => array(
					'StatusPedido',
					'PedidoSku.Sku.Produto.Categoria'
				),
				'conditions' => array(
					'Pedido.id' => $numero_pedido,
					'Pedido.cliente_id' => $this->usuarioAutenticado['id'],
					'StatusPedido.status_finalizado' => false
				)
			));

			$this->set(compact('pedido'));
		}
	}

	private function pedido_promocao($promocao) {
		unset($promocao['Promocao']['id']);
		unset($promocao['PromocaoCausa']['id']);
		unset($promocao['PromocaoCausa']['promocao_id']);
		unset($promocao['PromocaoEfeito']['id']);
		unset($promocao['PromocaoEfeito']['promocao_id']);

		$promocao['Promocao']['PromocaoCausa'] = $promocao['PromocaoCausa'];
		$promocao['Promocao']['PromocaoEfeito'] = $promocao['PromocaoEfeito'];

		unset($promocao['PromocaoCausa']);
		unset($promocao['PromocaoEfeito']);

		return $promocao['Promocao'];
	}

	private function getSexos() {
		return array(
			'M' => 'Masculino',
			'F' => 'Feminino'
		);
	}

	/**
	 * PROMOÇÕES
	 */

	/**
	 * Este método agrupa os Skus em Produtos para poder aplicar promoções
	 * onde a quantidade de peças do produto é levada em consideração.
	 * Após verificar as promoções por produtos, aplica as mesmas nos skus
	 * de cada produto, ou seja, nos skus do carrinho.
	 */
	private function aplicar_promocoes($skus = array(), $cep = null, $cupom = null) {
		$produtos_skus = array();

		$qtd_pecas_carrinho = 0;

		// Retornando para o carrinho caso dê erro e caia a sessão

		if (count($skus) == 0) {
			$this->redirect(array('controller' => 'carrinho', 'action' => 'index'));
		}

		// Juntar os skus do mesmo produto e somando a quantidade de pecas no carrinho
		foreach ($skus as $id => $sku) {
			if (!array_key_exists($sku['Produto']['id'], $produtos_skus)) {
				$produtos_skus[$sku['Produto']['id']] = array();
			}
			$produtos_skus[$sku['Produto']['id']][] = $id;
			$qtd_pecas_carrinho += $sku['qtd'];
		}

		$sub_total_pedido = $this->calcular_sub_total($skus);

		// Verificar as promocoes para cada produto e aplicar a todos os skus dele
		foreach ($produtos_skus as $id => $produto_skus) {
			// Somando a quantidade de todos os skus do produto atual
			$qtd = 0;
			foreach ($produto_skus as $sku_id) {
				$qtd += $skus[$sku_id]['qtd'];
			}

			// Armazenando os valores originais antes de aplicar as promoções
			if (!isset($skus[$sku_id]['Produto']['preco_de_original']) && !isset($skus[$sku_id]['Produto']['preco_por_original'])) {
				$skus[$sku_id]['Produto']['preco_de_original'] = $skus[$sku_id]['Produto']['preco_de'];
				$skus[$sku_id]['Produto']['preco_por_original'] = $skus[$sku_id]['Produto']['preco_por'];
			}

			$produto = $this->Promocoes->aplicar_promocoes(array('Produto' => $skus[$sku_id]['Produto']), $qtd, $cep, $sub_total_pedido['valor_produtos'], $qtd_pecas_carrinho, $cupom);

			foreach ($produto_skus as $sku_id) {
				unset($skus[$sku_id]['Promocao']);
				$skus[$sku_id] = array_merge($skus[$sku_id], $produto);
			}
		}

		return $skus;
	}

	private function calcular_sub_total($skus) {
		$valor_sub_total = 0;
		$valor_servicos = 0;
		$pedido = array();

		foreach ($skus as $id => $sku) {
			$preco_unitario = (isset($skus[$id]['preco_unitario_desconto']) && $skus[$id]['preco_unitario_desconto'] > 0) ? $skus[$id]['preco_unitario_desconto'] : $skus[$id]['preco_unitario'];
			$skus[$id]['preco_total'] = $preco_unitario * $skus[$id]['qtd'];
			$skus[$id]['peso_total'] = $skus[$id]['peso_unitario'] * $skus[$id]['qtd'];
			$valor_sub_total += $skus[$id]['preco_total'];

			if (isset($skus[$id]['Servico']) && count($skus[$id]['Servico']) > 0) {
				foreach ($skus[$id]['Servico'] as $servico) {
					$valor_servicos += $servico['valor'] * $skus[$id]['qtd'];
				}
			}

		}

		// Calculando desconto baseado em promoções do pedido
		$valor_sub_total_com_desconto = $this->RegrasPromocoesPedidos->definir_promocoes_pedido($valor_sub_total, $skus);
		if (isset($valor_sub_total_com_desconto['Promocao']['melhor_desconto_pedido']['promocao'])) {
			$pedido['Promocao'] = $valor_sub_total_com_desconto['Promocao'];
			$valor_sub_total_com_desconto = $valor_sub_total_com_desconto['Promocao']['melhor_desconto_pedido']['valor'];
		}

		$pedido['valor_produtos'] = $valor_sub_total;
		$pedido['valor_servicos'] = $valor_servicos;
		$pedido['valor_desconto'] = $valor_sub_total - $valor_sub_total_com_desconto;
		$pedido['valor_total'] = $valor_sub_total_com_desconto + $valor_servicos;

		return $pedido;
	}

	private function aplicar_promocoes_cupom($valor_sub_total, $skus) {
		$valor_sub_total = $this->RegrasPromocoesCupom->aplicar_promocoes_cupom($valor_sub_total, $skus, $this->usuarioAutenticado);

		return $valor_sub_total;
	}

	private function getMeioPagamento() {
		return array(
			'boleto' => 'BoletoBancario',
			'creditCard' => 'CartaoCredito',
			'eft' => 'DebitoBancario',
		);
	}

}

?>