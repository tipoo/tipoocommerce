<?php
App::uses('AppController', 'Controller');
App::uses('Parcelamento', 'Vendor');

class CatalogoController extends AppController {

	private $cep;

	public $uses = array(
		'Categoria',
		'Produto',
		'Imagem',
		'Marca',
		'Banner',
		'Colecao',
		'TermosBuscado',
		'TagsProduto',
		'ItemMenu',
		'Tag',
		'Caracteristica',
		'CaracteristicasValor',
		'HtmlsLocal',
		'PagseguroTransparenteBandeira',
		'MoipInstituicao',
		'Pagina'
	);

	public $components = array(
		'CustomTagsParser' => array('actions' => array('index', 'categoria', 'marca', 'busca', 'produto','tag')),
		'BreadCrumb',
		'RegrasPromocoesCompreJunto',
		'CompreJunto',
		'RegrasPromocoesFrete'
	);

	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('index', 'categoria', 'marca', 'busca', 'produto','tag', 'menu', 'ajax_simular_frete');

		$this->layout = 'catalogo';

		// Disponibilizando para a view os dados necessarios para monta o menu de categorias
		$this->MenuCategorias->setMenuCategorias();
		$this->ItemMenus->setMenu();
		$this->Banners->setTipos('generico');

		// Disponibilizando todas as marcas para as views do catálogo
		$marcas = $this->Marca->find('all', array(
			'contain' => array('Slug'),
			'conditions' => array('Marca.ativo' => true),
			'order' => 'descricao'
		));

		if ($this->Session->check('Carrinho.CEP')) {
			$this->cep = $this->Session->read('Carrinho.CEP');
		}

		$this->set('controlesMenuMarcas', $marcas);

		$this->set('skus', $this->Session->read('Carrinho.Skus'));
		$this->set('mensagens_skus', $this->Session->read('Carrinho.Mensagens'));
	}

	public function produto() {
		$id = $this->params['id'];

		if (!$id) {
			throw new Exception("Produto não encontrado");
		}

		// Normalizando o formato do cep
		$cep = null;
		if (!empty($this->cep)) {
			$cep = str_replace('-', '', $this->cep);
		}

		$controleProduto = $this->Produto->find('first', array(
			'contain' => array(
				'CaracteristicasValoresSelecionado' => array(
					'CaracteristicasValor',
					'Caracteristica'
				),
				'Slug',
				'Marca',
				'Categoria',
				'Sku' => array(
					'Imagem' => array(
						'conditions' => array(
							'Imagem.ativo' => true,
							'Imagem.destaque' => true
						)
					),
					'CaracteristicasValoresSelecionado',
					'order' => array(
						'ordem' => 'ASC',
						'descricao' => 'ASC'
					),
					'conditions' => array(
						'Sku.ativo' => true
					)
				),
				'Imagem' => array(
					'conditions' => array(
						'Imagem.ativo' => true,
						'Imagem.destaque' => true,
						'Imagem.produto_id is not null'
					),
					'order' => array(
						'Imagem.ordem' => 'ASC',
					)
				),
				'TagsProduto.Tag.Slug',
				'CompreJuntoPromocao' => array(
					'conditions' => array(
						'CompreJuntoPromocao.pedido_sku_id is null'
					)
				)
			),
			'conditions' => array(
				'Produto.id' => $id,
				'Produto.ativo'
			)
		));

		$controleProduto = $this->RegrasProdutos->aplicar_regras($controleProduto, $qtd_pecas = 1, $cep, $controleProduto['Produto']['preco_por'], $qtd_pecas_carrinho = 1);

		$controleProdutoImagens = $this->Imagem->find('all', array(
			'conditions' => array(
				'Imagem.ativo' => true,
				'Imagem.produto_id' => $id
			),
			'order' => array(
				'Imagem.ordem' => 'ASC',
			)
		));

		if ($this->Configuracao->get('pagseguro_transparente_ativo')) {
			$controleParcelamento = $this->PagseguroTransparenteBandeira->find('first', array(
				'contain' => array(
					'Parcela' => array(
						'conditions' => array(
							'Parcela.ativo' => true
						),
						'order' => array(
							'Parcela.parcelas' => 'ASC'
						)
					)
				),
				'conditions' => array(
					'PagseguroTransparenteBandeira.ativo' => true,
					'PagseguroTransparenteBandeira.tabela_parcelamento' => true,
				)
			));
		} else if ($this->Configuracao->get('moip_ativo')) {
			$controleParcelamento = $this->MoipInstituicao->find('first', array(
				'contain' => array(
					'Parcela' => array(
						'conditions' => array(
							'Parcela.ativo' => true
						),
						'order' => array(
							'Parcela.parcelas' => 'ASC'
						)
					)
				),
				'conditions' => array(
					'MoipInstituicao.ativo' => true,
					'MoipInstituicao.tabela_parcelamento' => true,
				)
			));
		}

		if (!$controleProduto) {
			throw new Exception("Produto não encontrado");
		}

		$controleCaracteristicasSkus = $this->getCaracteristicasSkus($controleProduto['Sku'], $controleProduto['Produto']['categoria_id']);

		$title_for_layout = $controleProduto['Produto']['title'] ? $controleProduto['Produto']['title'] : $controleProduto['Produto']['descricao'] . ' - ' . Configure::read('Loja.nome');
		$meta_title = $controleProduto['Produto']['meta_title'] ? $controleProduto['Produto']['meta_title'] : $controleProduto['Produto']['descricao'] . ' - ' . Configure::read('Loja.nome');

		$this->set(compact('controleProduto', 'controleProdutoImagens', 'controleCaracteristicasSkus', 'controleParcelamento', 'title_for_layout', 'meta_title'));

		// Verificando se tem metatags customizada  e sobreescrevendo as padrões
		if (!empty($controleProduto['Produto']['meta_description'])) {
			$this->set('meta_description', $controleProduto['Produto']['meta_description']);
		}
		if (!empty($controleProduto['Produto']['meta_keywords'])) {
			$this->set('meta_keywords', $controleProduto['Produto']['meta_keywords']);
		}

		$this->Colecoes->setColecoesLocais($opts = array('produto_id' => $id));

		$this->BreadCrumb->gerarPorCategoria($controleProduto['Categoria']['id']);

		$compre_junto = $this->CompreJunto->get($controleProduto);

		$controleHtml = $this->HtmlsLocal->get($opts = array('produto_id' => $id));
		$this->set(compact('controleHtml'));

		$this->set('controleCompreJunto', $compre_junto);
		$this->set('cep', $this->cep);
	}

	public function index() {
		$pagina = $this->Pagina->find('first', array(
			'conditions' => array(
				'Pagina.id' => Configure::read('Sistema.Colecoes.Home.id'),
				'Pagina.ativo'
			)
		));

		$this->layout = $pagina['Pagina']['layout_ctp'];
		$this->CustomTagsParser->viewRender = $pagina['Pagina']['arquivo_ctp'];

		$this->Banners->setTipos('home');
		$this->Colecoes->setColecoesLocais($opts = array('pagina_id' => Configure::read('Sistema.Colecoes.Home.id')));

		$controleHtml = $this->HtmlsLocal->get($opts = array('pagina_id' => Configure::read('Sistema.Paginas.Home.id')));
		$this->set(compact('controleHtml'));
	}

	public function categoria() {
		$id = $this->params['id'];

		$categoria = $this->Categoria->find('first', array(
			'contain' => false,
			'conditions' => array(
				'Categoria.id' => $id
			)
		));

		if ($this->temBuscaPrevia()) {
			$busca_id = $this->request->query['busca_id'];

			if (isset($this->request->query['pagina'])) {
				$this->paginacao($busca_id);

			} else if (isset($this->request->query['ordem']) || isset($this->request->query['ordem_dir'])) {
				$this->ordenacao($busca_id);

			} else {
				$this->setProdutosBuscas($busca_id, 1);
			}
		} else {
			$categoriasIn = $this->MenuCategorias->getCategoriasIn($id);

			$produtos = $this->Produto->find('all', array(
				'contain' => array(
					'Marca' => array(
						'id',
						'descricao'
					),
					'Categoria' => array(
						'id',
						'descricao'
					),
					'Sku' => array(
						'CaracteristicasValoresSelecionado' => array(
							'CaracteristicasValor',
							'Caracteristica'
						),
						'conditions' => array(
							'Sku.ativo' => true
						),
						'order' => array(
							'ordem' => 'ASC',
						)
					),
					'Slug',
					'Imagem' => array(
						'conditions' => array(
							'Imagem.ativo' => true,
							'Imagem.produto_id !=' => ''
						),
						'order' => array(
							'Imagem.ordem' => 'ASC',
						)
					),
					'CaracteristicasValoresSelecionado' => array(
						'CaracteristicasValor',
						'Caracteristica'
					),
					'TagsProduto.Tag.Slug'
				),
				'conditions' => array(
					'Produto.ativo' => true,
					'Produto.categoria_id' => $categoriasIn
				)
			));

			$busca_id = $this->salvarBusca($produtos);

			if (isset($this->request->query['pagina'])) {
				$this->paginacao($busca_id);

			} else if (isset($this->request->query['ordem']) || isset($this->request->query['ordem_dir'])) {
				$this->ordenacao($busca_id);

			} else {
				$this->setProdutosBuscas($busca_id, 1);
			}

		}

		// Verificando se tem metatags customizada  e sobreescrevendo as padrões
		if (!empty($categoria['Categoria']['meta_description'])) {
			$this->set('meta_description', $categoria['Categoria']['meta_description']);
		}
		if (!empty($categoria['Categoria']['meta_keywords'])) {
			$this->set('meta_keywords', $categoria['Categoria']['meta_keywords']);
		}

		$this->Colecoes->setColecoesLocais($opts = array('categoria_id' => $id));
		$this->Banners->setTipos('categoria', array('categoria_id' => $id));

		if (!$categoria['Categoria']['categoria_pai_id']) {
			$this->CustomTagsParser->viewRender = 'departamento';
		}

		// Enviando o menu dessa categoria para a tela
		$this->MenuCategorias->setMenuCategoria($id);
		$this->BreadCrumb->gerarPorCategoria($id);

		$controleHtml = $this->HtmlsLocal->get($opts = array('categoria_id' => $id));

		$title_for_layout = $categoria['Categoria']['title'] ? $categoria['Categoria']['title'] :  $categoria['Categoria']['descricao'] . ' - ' . Configure::read('Loja.nome');
		$meta_title = $categoria['Categoria']['meta_title'] ? $categoria['Categoria']['meta_title'] :  $categoria['Categoria']['descricao'] . ' - ' . Configure::read('Loja.nome');

		$this->set(compact('controleHtml', 'title_for_layout', 'meta_title'));

		$this->set('categoria', $categoria);
	}

	public function busca() {

		if ($this->temBuscaPrevia()) {
			$busca_id = $this->request->query['busca_id'];

			if (isset($this->request->query['pagina'])) {
				$this->paginacao($busca_id);

			} else if (isset($this->request->query['ordem']) || isset($this->request->query['ordem_dir'])) {
				$this->ordenacao($busca_id);

			} else {
				$this->setProdutosBuscas($busca_id, 1);
			}
		} else {
			$palavraChave = $this->request->query['q'];

 			$termo = trim($palavraChave);


			if ($termo  != '') {

				$this->TermosBuscado->create();
				$this->request->data['TermosBuscado']['termo'] = $termo;
				$this->TermosBuscado->save($this->request->data);
			}

			$tag = $this->Tag->find('first', array(
				'contain' => array(
					'TagsProduto'
				),
				'conditions' => array(
					'Tag.ativo' => true,
					'Tag.nome like' => '%' . $palavraChave . '%',
				)
			));

			$tag_produtos_id = array();
			if (isset($tag['TagsProduto']) && count($tag['TagsProduto']) > 0) {
				foreach ($tag['TagsProduto'] as $tag_produto) {
					$tag_produtos_id[] = $tag_produto['produto_id'];
				}
			}

			$produtos = $this->Produto->find('all', array(
				'contain' => array(
					'Marca' => array(
						'id',
						'descricao'
					),
					'Categoria' => array(
						'id',
						'descricao'
					),
					'Sku' => array(
						'CaracteristicasValoresSelecionado' => array(
							'CaracteristicasValor',
							'Caracteristica'
						),
						'conditions' => array(
							'Sku.ativo' => true
						),
						'order' => array(
							'ordem' => 'ASC',
						)
					),
					'Slug.url',
					'Imagem' => array(
						'conditions' => array(
							'Imagem.ativo' => true,
							'Imagem.produto_id !=' => ''
						),
						'order' => array(
							'Imagem.ordem' => 'ASC',
						)
					),
					'CaracteristicasValoresSelecionado' => array(
						'CaracteristicasValor',
						'Caracteristica'
					),
					'TagsProduto.Tag.Slug'
				),
				'conditions' => array(
					'Produto.ativo' => true,
					'OR' => array(
						'Produto.descricao like' => '%' . $palavraChave . '%',
						'Marca.descricao like' => '%' . $palavraChave . '%',
						'Produto.id' => $tag_produtos_id
					)
				)
			));

			$busca_id = $this->salvarBusca($produtos);

			if (isset($this->request->query['pagina'])) {
				$this->paginacao($busca_id);

			} else if (isset($this->request->query['ordem']) || isset($this->request->query['ordem_dir'])) {
				$this->ordenacao($busca_id);

			} else {
				$this->setProdutosBuscas($busca_id, 1);
			}

		}
	}

	public function marca() {
		$id = $this->params['id'];

		$marca = $this->Marca->find('first', array(
			'contain' => false,
			'conditions' => array(
				'Marca.id' => $id
			)
		));

		if ($this->temBuscaPrevia()) {
			$busca_id = $this->request->query['busca_id'];

			if (isset($this->request->query['pagina'])) {
				$this->paginacao($busca_id);

			} else if (isset($this->request->query['ordem']) || isset($this->request->query['ordem_dir'])) {
				$this->ordenacao($busca_id);

			} else {
				$this->setProdutosBuscas($busca_id, 1);
			}
		} else {
			$produtos = $this->Produto->find('all', array(
				'contain' => array(
					'Marca' => array(
						'id',
						'descricao'
					),
					'Categoria' => array(
						'id',
						'descricao'
					),
					'Sku' => array(
						'CaracteristicasValoresSelecionado' => array(
							'CaracteristicasValor',
							'Caracteristica'
						),
						'conditions' => array(
							'Sku.ativo' => true
						),
						'order' => array(
							'ordem' => 'ASC',
						)
					),
					'Slug.url',
					'Imagem' => array(
						'conditions' => array(
							'Imagem.ativo' => true,
							'Imagem.produto_id !=' => ''
						),
						'order' => array(
							'Imagem.ordem' => 'ASC',
						)
					),
					'CaracteristicasValoresSelecionado' => array(
						'CaracteristicasValor',
						'Caracteristica'
					),
					'TagsProduto.Tag.Slug'
				),
				'conditions' => array(
					'Produto.ativo',
					'Produto.marca_id' => $id
				)
			));

			$busca_id = $this->salvarBusca($produtos);

			if (isset($this->request->query['pagina'])) {
				$this->paginacao($busca_id);

			} else if (isset($this->request->query['ordem']) || isset($this->request->query['ordem_dir'])) {
				$this->ordenacao($busca_id);

			} else {
				$this->setProdutosBuscas($busca_id, 1);
			}

		}

		// Verificando se tem metatags customizada  e sobreescrevendo as padrões
		if (!empty($marca['Marca']['meta_description'])) {
			$this->set('meta_description', $marca['Marca']['meta_description']);
		}
		if (!empty($marca['Marca']['meta_keywords'])) {
			$this->set('meta_keywords', $marca['Marca']['meta_keywords']);
		}

		$this->Colecoes->setColecoesLocais($opts = array('marca_id' => $id));

		$this->BreadCrumb->gerarPorMarca($id);

		$controleHtml = $this->HtmlsLocal->get($opts = array('marca_id' => $id));
		$this->set(compact('controleHtml'));
	}

	public function tag() {
		$id = $this->params['id'];

		$produtos_tags = $this->TagsProduto->find('list', array(
			'contain' => false,
			'conditions' => array(
				'TagsProduto.tag_id' => $id
			),
			'fields' => 'produto_id'
		));

		if ($this->temBuscaPrevia()) {
			$busca_id = $this->request->query['busca_id'];

			if (isset($this->request->query['pagina'])) {
				$this->paginacao($busca_id);

			} else if (isset($this->request->query['ordem']) || isset($this->request->query['ordem_dir'])) {
				$this->ordenacao($busca_id);

			} else {
				$this->setProdutosBuscas($busca_id, 1);
			}
		} else {
			$produtos = $this->Produto->find('all', array(
				'contain' => array(
					'Marca' => array(
						'id',
						'descricao'
					),
					'Categoria' => array(
						'id',
						'descricao'
					),
					'Sku' => array(
						'CaracteristicasValoresSelecionado' => array(
							'CaracteristicasValor',
							'Caracteristica'
						),
						'conditions' => array(
							'Sku.ativo' => true
						),
						'order' => array(
							'ordem' => 'ASC',
						)
					),
					'Slug.url',
					'Imagem' => array(
						'conditions' => array(
							'Imagem.ativo' => true,
							'Imagem.produto_id !=' => ''
						),
						'order' => array(
							'Imagem.ordem' => 'ASC',
						)
					),
					'CaracteristicasValoresSelecionado' => array(
						'CaracteristicasValor',
						'Caracteristica'
					),
					'TagsProduto.Tag.Slug'
				),
				'conditions' => array(
					'Produto.ativo',
					'Produto.id' => $produtos_tags
				)
			));

			$busca_id = $this->salvarBusca($produtos);

			if (isset($this->request->query['pagina'])) {
				$this->paginacao($busca_id);

			} else if (isset($this->request->query['ordem']) || isset($this->request->query['ordem_dir'])) {
				$this->ordenacao($busca_id);

			} else {
				$this->setProdutosBuscas($busca_id, 1);
			}

		}
	}

	public function ajax_simular_frete($cep = null, $simulacao_valor_carrinho = null, $qtd = 1, $produto_id = null, $sku_id = null) {
		$this->layout = 'ajax';

		if ($cep) {

			$this->cep = $cep;

			$sku_conditions = array(
				'Sku.ativo' => true
			);

			if ($sku_id) {
				$sku_conditions += array(
					'Sku.id' => $sku_id
				);
			}

			$produto = $this->Produto->find('first', array(
				'contain' => array(
					'Sku' => array(
						'order' => array(
							'ordem' => 'ASC'
						),
						'conditions' => $sku_conditions
					)
				),
				'conditions' => array(
					'Produto.id' => $produto_id,
					'Produto.ativo'
				)
			));

			$qtd_pecas = $qtd;
			$qtd_pecas_carrinho = $qtd;
			// Vai aplicar as regras e buscar as promoções se existir
			$produto = $this->RegrasProdutos->aplicar_regras($produto, $qtd_pecas, $cep, $simulacao_valor_carrinho, $qtd_pecas_carrinho);

			// Verifica sempre pelo sku primeiro sku, pois se vier mais de um ele vai pegar o sku destaque (primeiro sku na ordem de skus)
			$sku = array(
				'Sku' => $produto['Sku'][0],
				'qtd' => $qtd,
				'peso_total' => ($produto['Sku'][0]['peso'] * $qtd),
				'Promocao' => array(
					'desconto_frete' => $produto['Promocao']['desconto_frete']
				)
			);

			$frete_transportadoras = $this->getFreteTransportadoras($cep, $sku, $qtd);

		} else {
			$this->cep = null;
		}

		// Adiciona o novo valor do cep na sessão
		$this->Session->write('Carrinho.CEP', $this->cep);

		$this->set(compact('frete_transportadoras'));
	}

	private function getFreteTransportadoras($cep = null, $sku = null, $qtd_sku = 1) {
		//Buscando as transportadoras e servicos dos correios
		$frete_transportadoras_correios = $this->FreteCorreios->buscar(array($sku), $cep);
		$frete_transportadoras_transp = $this->FreteTransportadoras->buscar(array($sku), $cep);
		$frete_transportadoras = array_merge($frete_transportadoras_correios, $frete_transportadoras_transp);

		foreach ($frete_transportadoras as $key => $frete_transportadora) {
			$frete_transportadoras[$key] = $this->RegrasPromocoesFrete->calcular_valor_frete($frete_transportadora, array($sku), $cep);
		}

		return $frete_transportadoras;
	}

	private function salvarBusca($produtos) {
		$produtos = $this->aplicar_regras($produtos);
		$produtos = $this->reorganizar_busca($produtos);

		$busca_id = md5(uniqid(rand(), true));
		$this->Session->write('Buscas.'.$busca_id, $produtos);

		return $busca_id;
	}

	private function setProdutosBuscas($busca_id, $pagina = 0) {
		$produtos = $this->gerarFiltros($this->Session->read('Buscas.'.$busca_id));

		if ($pagina) {
			$inicio = ($pagina - 1) * $this->Configuracao->get('cat_qtd_produtos_pagina');
		} else {
			$inicio = 0;
		}
		if (count($produtos) > $this->Configuracao->get('cat_qtd_produtos_pagina')) {
			$this->set('controleProdutosBuscas', array_slice($produtos, $inicio, $this->Configuracao->get('cat_qtd_produtos_pagina')));
		} else {
			$this->set('controleProdutosBuscas', $produtos);
		}

		$this->set('controleProdutosBuscaId', $busca_id);
		$this->set('controleProdutosTotal', count($produtos));
	}

	private function temBuscaPrevia() {
		return isset($this->request->query['busca_id']) && $this->Session->check('Buscas.'.$this->request->query['busca_id']);
	}

	private function paginacao($busca_id) {
		$pagina = $this->request->query['pagina'];
		$produtos = $this->Session->read('Buscas.'.$busca_id);

		$this->setProdutosBuscas($busca_id, $pagina);
	}

	private function ordenacao($busca_id) {
		$produtos = $this->Session->read('Buscas.'.$busca_id);

		$produtos = $this->reorganizar_busca($produtos, $this->request->query['ordem'], $this->request->query['ordem_dir']);
		$this->Session->write('Buscas.'.$busca_id, $produtos);

		$this->setProdutosBuscas($busca_id, 1);
	}

	private function aplicar_regras($produtos) {
			// Aqui aplicar regras de preços, promoções, estoque etc...
			// Criar component para ser reutilizado em varios locais

			$cep = null;
			if (!empty($this->cep)) {
				$cep = str_replace('-', '', $this->cep);
			}

			foreach ($produtos as $key => $produto) {
				$produto = $this->RegrasProdutos->aplicar_regras($produto, $qtd_pecas = 1, $cep, $produto['Produto']['preco_por'], $qtd_pecas_carrinho = 1);
				$produtos[$key] = $produto;
			}

			return $produtos;
		}

	private function reorganizar_busca($produtos, $ordem = 'descricao', $ordem_dir = 'ASC') {
		if (strtolower($ordem) == 'descricao') {
			if (strtolower($ordem_dir) == 'asc') {
				usort($produtos, function($a, $b) {
					return strcasecmp($a['Produto']['descricao'], $b['Produto']['descricao']);
				});
			} else {
				usort($produtos, function($a, $b) {
					return strcasecmp($b['Produto']['descricao'], $a['Produto']['descricao']);
				});
			}
		} else if (strtolower($ordem) == 'preco') {
			if (strtolower($ordem_dir) == 'asc') {
				usort($produtos, function($a, $b) {
					return $a['Produto']['preco_por'] - $b['Produto']['preco_por'];
				});
			} else {
				usort($produtos, function($a, $b) {
					return $b['Produto']['preco_por'] - $a['Produto']['preco_por'];
				});
			}
		} else if (strtolower($ordem) == 'economia') {
			if (strtolower($ordem_dir) == 'asc') {
				usort($produtos, function($a, $b) {
					return $b['Produto']['economize'] - $a['Produto']['economize'];
				});
			} else {
				usort($produtos, function($a, $b) {
					return $a['Produto']['economize'] - $b['Produto']['economize'];
				});
			}
		}

		return $produtos;
	}

	private function gerarFiltros($produtos) {
		$filtros_url = $this->getRequestFiltros();

		// Filtrando os produtos
		$produtos_filtrados = array();
		foreach ($produtos as $produto) {
			$pertence_aos_filtros_produto = $this->verificarCaracteristicasValoresSelecionado($produto['CaracteristicasValoresSelecionado'], 'produto', $filtros_url);

			$pertence_aos_filtros_sku = true;
			foreach ($produto['Sku'] as $sku) {
				if ($this->verificarCaracteristicasValoresSelecionado($sku['CaracteristicasValoresSelecionado'], 'sku', $filtros_url) && ($sku['situacao_estoque'] != 'AviseMe' || $produto['Produto']['mostrar_indisponivel'])) {
					$pertence_aos_filtros_sku = true;
					break;
				} else {
					$pertence_aos_filtros_sku = false;
				}
			}

			$pertence_aos_filtros_marca = $this->verificarMarcaSelecionada($produto['Marca'], $filtros_url);

			$pertence_aos_filtros_tag = $this->verificarTagsSelecionadas($produto['TagsProduto'], $filtros_url);

			$pertence_aos_filtros_preco = $this->verificarFaixaPreco($produto['Produto']['preco_por'], $filtros_url);

			if ($pertence_aos_filtros_produto
					&& $pertence_aos_filtros_sku
					&& $pertence_aos_filtros_marca
					&& $pertence_aos_filtros_tag
					&& $pertence_aos_filtros_preco) {
				$produtos_filtrados[] = $produto;
			}
		}

		// Gerando os filtros
		$filtros_caracteristicas = array();
		$filtros_marcas = array();
		$filtros_precos = array(
			'min' => 0,
			'max' => 0,
			'faixas' => array()
		);
		foreach ($produtos_filtrados as $produto) {
			$filtros_caracteristicas = $this->gerarFiltrosCaracteristicasValoresSelecionado($produto['CaracteristicasValoresSelecionado'], $filtros_caracteristicas, $produto['Produto']['id']);

			foreach ($produto['Sku'] as $sku) {
				if (!$produto['Produto']['esgotado'] || $produto['Produto']['mostrar_indisponivel']) {
					$filtros_caracteristicas = $this->gerarFiltrosCaracteristicasValoresSelecionado($sku['CaracteristicasValoresSelecionado'], $filtros_caracteristicas, $sku['produto_id'], $filtros_url);
				}
			}

			$filtros_marcas = $this->gerarFiltrosMarcas($produto['Marca'], $filtros_marcas);

			$filtros_precos = $this->gerarFiltrosPrecoMinMax($produto['Produto']['preco_por'], $filtros_precos);
		}

		$filtros_precos = $this->gerarFiltrosPrecoFaixas($filtros_precos, $filtros_url);
		$filtros_precos = $this->calcularQtdFaixaPreco($produtos_filtrados, $filtros_precos);

		$this->set('controlesFiltrosCaracteristicas', $filtros_caracteristicas);
		$this->set('controlesFiltrosMarcas', $filtros_marcas);
		$this->set('controlesFiltrosPrecos', $filtros_precos);

		return $produtos_filtrados;
	}

	private function calcularQtdFaixaPreco($produtos, $filtros) {
		foreach ($produtos as $produto) {
			foreach ($filtros['faixas'] as $key => $faixa) {
				if ($produto['Produto']['preco_por'] >= $faixa['de'] && $produto['Produto']['preco_por'] <= $faixa['ate']) {
					$filtros['faixas'][$key]['qtd']++;
				}
			}
		}

		return $filtros;
	}

	private function calcularPotenciaDez($preco) {
		$precos = explode('.', $preco);
		$tam = strlen($precos[0]);
		return $tam - 1;
	}

	/**
	 * Gera as faixas de preços para filtrar os produtos. Caso já exista uma faixa selecionada apenas a mesma é retornada,
	 * pois como as faixas de precos são calculadas a cada requisição são baseadas nos produtos já filtrados,
	 * o filtro mudaria após um filtro de preço já realizado
	 */

	private function gerarFiltrosPrecoFaixas($filtros, $filtros_url) {
		if ($filtros_url && isset($filtros_url['preco'])) {
			$filtros['faixas'][] = array(
				'de' => $filtros_url['preco']['inicial'],
				'ate' => $filtros_url['preco']['final'],
				'min_original' => $filtros_url['preco']['min_original'],
				'max_original' => $filtros_url['preco']['max_original'],
				'qtd' => 0
			);

		} else {
			$pow = pow(10, $this->calcularPotenciaDez($filtros['min']));
			$ate = $filtros['min'] / (pow(10, $this->calcularPotenciaDez($filtros['min'])));
			$ate = floor($ate + 1) * $pow; // Não foi usada a funcao ceil porque quando o valor é redondo ele não iria para o próximo valor
			$ate = $ate;
			$faixa_inicial = array(
				'de' => 0,
				'ate' => $ate,
				'min_original' => $filtros['min'],
				'max_original' => $filtros['max'],
				'qtd' => 0
			);

			$pow = pow(10, $this->calcularPotenciaDez($filtros['max']));
			$de = $filtros['max'] / $pow;
			$de = floor($de - 1) * $pow;
			$faixa_final = array(
				'de' => $de,
				'ate' => $filtros['max'],
				'min_original' => $filtros['min'],
				'max_original' => $filtros['max'],
				'qtd' => 0
			);


			$filtros['faixas'][] = $faixa_inicial;
			if ($faixa_final['de'] > $faixa_inicial['ate']) {
				// Calcula o incremento para criar os intervalos de faixas
				$incremento = round(($faixa_final['de'] - $faixa_inicial['ate']) / 5);

				if ($incremento > 5) {
					for ($i = 0; $i < 5; $i++) {
						if ($i == 0) {
							$filtros['faixas'][] = array(
								'de' => $faixa_inicial['ate'],
								'ate' => $faixa_inicial['ate'] + $incremento,
								'min_original' => $filtros['min'],
								'max_original' => $filtros['max'],
								'qtd' => 0
							);
						} else if ($i == 4) {
							$filtros['faixas'][] = array(
								'de' => $filtros['faixas'][$i]['ate'],
								'ate' => $faixa_final['de'],
								'min_original' => $filtros['min'],
								'max_original' => $filtros['max'],
								'qtd' => 0
							);
						} else {
							$filtros['faixas'][] = array(
								'de' => $filtros['faixas'][$i]['ate'],
								'ate' =>  $filtros['faixas'][$i]['ate'] + $incremento,
								'min_original' => $filtros['min'],
								'max_original' => $filtros['max'],
								'qtd' => 0
							);
						}
					}
				}

				foreach ($filtros['faixas'] as $key => $faixa) {
					if ($key < count($filtros['faixas'])) {
						$filtros['faixas'][$key]['ate'] = $filtros['faixas'][$key]['ate'] - 0.01;
					}
				}

				$filtros['faixas'][] = $faixa_final;
			}
		}

		return $filtros;
	}

	// Verifica se o preco do produto é pode ser o minimo ou maximo e atualiza essa informacao no array de filtros de precos
	private function gerarFiltrosPrecoMinMax($preco, $filtros) {
		if ($preco < $filtros['min'] || $filtros['min'] == 0) {
			$filtros['min'] = $preco;
		}

		if ($preco > $filtros['max']) {
			$filtros['max'] = $preco;
		}

		return $filtros;
	}

	// Verifica se o preco_por corresponde ao filtro faixa de preco selecionado pelo usuário, caso ele tenha escolhido um.
	private function verificarFaixaPreco($preco_por, $filtros) {
		if ($filtros && isset($filtros['preco'])) {
			return $preco_por >= $filtros['preco']['inicial'] && $preco_por <= $filtros['preco']['final'];
		}

		return true;
	}

	// Gera as marcas para o filtro
	private function gerarFiltrosMarcas($marca, $filtros_marcas) {
		if (!array_key_exists($marca['id'], $filtros_marcas)) {
			$filtros_marcas[$marca['id']] = array(
				'id' => $marca['id'],
				'descricao' => $marca['descricao'],
				'qtd' => 1
			);
		} else {
			$filtros_marcas[$marca['id']]['qtd'] = $filtros_marcas[$marca['id']]['qtd'] + 1;
		}

		return $filtros_marcas;
	}

	// Verifica se uma determinada marca corresponde ao filtro de marca selecionado pelo usuário, caso ele tenha escolhido um.
	private function verificarMarcaSelecionada($marca, $filtros) {
		if ($filtros && isset($filtros['marca'])) {
			return $marca['id'] == $filtros['marca'];
		}

		return true;
	}

	// Verifica se determinadas tags correspondem ao filtro de tags selecionado pelo usuário, caso ele tenha escolhido um ou mais de um.
	private function verificarTagsSelecionadas($tags, $filtros) {
		if ($filtros && isset($filtros['tag'])) {
			if (in_array('or', $filtros['tag'])) {
				// Lista todoas os produtos que tem qualquer uma das tags escolhidas no filtro
				foreach ($tags as $tag) {
					if (in_array($tag['tag_id'], $filtros['tag'])) {
						return true;
					}
				}
			} else {
				// Lista apenas os produtos que tem todas as tags escolhidas no filtro
				$count = 0;
				foreach ($tags as $tag) {
					if (in_array($tag['tag_id'], $filtros['tag'])) {
						$count++;
					}
				}

				if ($count == count($filtros['tag'])) {
					$retorno = true;
				} else {
					$retorno = false;
				}
				return $retorno;
			}
		} else {
			return true;
		}

		return false;
	}

	// Gera os filtros por característica de produto ou sku
	private function gerarFiltrosCaracteristicasValoresSelecionado($caracteristicas_valores_selecionado, $filtros, $produto_id, $filtros_url = null) {
		foreach ($caracteristicas_valores_selecionado as $cvs) {
			// Verifica se é uma característica do tipo lista
			if ($cvs['Caracteristica']['tipo'] == 'L') {
				// Verifica se caracteristica já esta nos filtros. Se não estiver adiciona
				if (!array_key_exists($cvs['caracteristica_id'], $filtros)) {
					$mostra_filtro_cvs = false;
					if ($cvs['Caracteristica']['referente'] == 'P') { // Caracteristica referente a P, mostra todos os filtros de produto
						$mostra_filtro_cvs = true;
					} else if (!isset($filtros_url['caracteristica']['sku'])) { // Não tem caracteristica selecionada de sku no filtro, então mostra todos os filtros de sku
						$mostra_filtro_cvs = true;
					} else if ($cvs['Caracteristica']['referente'] == 'S' && isset($filtros_url['caracteristica']['sku'])) { // Se tiver caracteristica de sku no filtro, verifica qual está selecionada e não mostra as outras
						$caracteristicas_selecionadas_id = array();
						$caracteristicas_valores_selecionados_id = array();
						foreach ($filtros_url['caracteristica']['sku'] as $filtro_url) {
							$caracteristicas_selecionadas_id[] = $filtro_url['caracteristica_id'];
							$caracteristicas_valores_selecionados_id[] = $filtro_url['caracteristicas_valor_id'];
						}

						if (in_array($cvs['caracteristica_id'], $caracteristicas_selecionadas_id) && in_array($cvs['CaracteristicasValor']['id'], $caracteristicas_valores_selecionados_id)) { // Existe o id da caracteristica e o id do valor selecionado na url de filtro do sku em questão
							$mostra_filtro_cvs = true;
						} else if (!in_array($cvs['caracteristica_id'], $caracteristicas_selecionadas_id) && !in_array($cvs['CaracteristicasValor']['id'], $caracteristicas_valores_selecionados_id)) { // Não existe nenhuma caracteristica na url dos skus de um determinado produto, então mostra todos as opções para serem selecionadas
						 	$mostra_filtro_cvs = true;
						}
					}

					if ($mostra_filtro_cvs) {
						$filtros[$cvs['caracteristica_id']] = array(
							'id' => $cvs['Caracteristica']['id'],
							'referente' => $cvs['Caracteristica']['referente'],
							'produto_id' => $produto_id,
							'descricao' => $cvs['Caracteristica']['descricao'],
							'caracteristicas_valores' => array(
								$cvs['caracteristicas_valor_id'] => array(
									'id' => $cvs['CaracteristicasValor']['id'],
									'descricao' => $cvs['CaracteristicasValor']['descricao'],
									'qtd' => 1
								)
							)
						);
					}

				} else {
					// Verifica se caracteristica valor já existe. Se não existir, cria.
					if (!array_key_exists($cvs['caracteristicas_valor_id'], $filtros[$cvs['caracteristica_id']]['caracteristicas_valores'])) {
						$mostra_filtro_cvs = false;
						if ($cvs['Caracteristica']['referente'] == 'P') { // Caracteristica referente a P, mostra todos os filtros de produto
							$mostra_filtro_cvs = true;
						} else if (!isset($filtros_url['caracteristica']['sku'])) { // Não tem caracteristica selecionada de sku no filtro, então mostra todos os filtros de sku
							$mostra_filtro_cvs = true;
						} else if ($cvs['Caracteristica']['referente'] == 'S' && isset($filtros_url['caracteristica']['sku'])) { // Se tiver caracteristica de sku no filtro, verifica qual está selecionada e não mostra as outras
							$caracteristicas_selecionadas_id = array();
							$caracteristicas_valores_selecionados_id = array();
							foreach ($filtros_url['caracteristica']['sku'] as $filtro_url) {
								$caracteristicas_selecionadas_id[] = $filtro_url['caracteristica_id'];
								$caracteristicas_valores_selecionados_id[] = $filtro_url['caracteristicas_valor_id'];
							}

							if (in_array($cvs['caracteristica_id'], $caracteristicas_selecionadas_id) && in_array($cvs['CaracteristicasValor']['id'], $caracteristicas_valores_selecionados_id)) { // Existe o id da caracteristica e o id do valor selecionado na url de filtro do sku em questão
								$mostra_filtro_cvs = true;
							} else if (!in_array($cvs['caracteristica_id'], $caracteristicas_selecionadas_id) && !in_array($cvs['CaracteristicasValor']['id'], $caracteristicas_valores_selecionados_id) && $produto_id == $filtros[$cvs['caracteristica_id']]['produto_id']) { // Não existe caracteristica na url dos skus de um determinado produto, então mostra todos as opções para serem selecionadas
								$mostra_filtro_cvs = true;
							}
						}

						if ($mostra_filtro_cvs) {
							$filtros[$cvs['caracteristica_id']]['caracteristicas_valores'][$cvs['caracteristicas_valor_id']] = array(
								'id' => $cvs['CaracteristicasValor']['id'],
								'descricao' => $cvs['CaracteristicasValor']['descricao'],
								'qtd' => 1
							);
						}
					} else if ($produto_id != $filtros[$cvs['caracteristica_id']]['produto_id']) {
						$filtros[$cvs['caracteristica_id']]['caracteristicas_valores'][$cvs['caracteristicas_valor_id']]['qtd'] = 1 + $filtros[$cvs['caracteristica_id']]['caracteristicas_valores'][$cvs['caracteristicas_valor_id']]['qtd'];
					}
				}
			}
		}

		// ordena array pela chave, garantindo que o filtro fique sempre na mesma ordem
		ksort($filtros);

		return $filtros;
	}

	// Verifica se um determinado produto ou sku possui valores de características correspondentes aos dos filtros selecionados pelo usuário
	private function verificarCaracteristicasValoresSelecionado($caracteristicas_valores_selecionado, $referente = 'produto', $filtros) { // $referente = 'produto' || 'sku'
		if ($filtros && isset($filtros['caracteristica'][$referente])) {
			foreach ($filtros['caracteristica'][$referente] as $filtro) {
				// Verificando se o produto tem valores cadastrados para todos filtros.
				$tem_cvs = false;
				foreach ($caracteristicas_valores_selecionado as $cvs) {
					if ($cvs['caracteristica_id'] == $filtro['caracteristica_id']) {
						$tem_cvs = true;
					}
				}

				if (!$tem_cvs) {
					return false;
				}

				// Verificando se os valores correspondem aos valores dos filtros
				foreach ($caracteristicas_valores_selecionado as $cvs) {
					if ($cvs['caracteristica_id'] == $filtro['caracteristica_id'] &&
							$cvs['caracteristicas_valor_id'] != $filtro['caracteristicas_valor_id']) {
						return false;
					}
				}
			}
		}

		return true;

	}

	// Retorna os filtros enviados na requisicao atual em formato de array
	private function getRequestFiltros() {
		if (isset($this->request->query['filtro'])) {
			$retorno = array();

			$filtros = $this->request->query['filtro'];
			$filtros = explode(';', $filtros);

			foreach ($filtros as $filtro) {
				$filtro = explode(',', $filtro);
				if ($filtro[0] == 'c') {
					if ($filtro[1] == 'p') {
						$retorno['caracteristica']['produto'][] = array('caracteristica_id' => $filtro[2], 'caracteristicas_valor_id' => $filtro[3]);
					} else if ($filtro[1] == 's') {
						$retorno['caracteristica']['sku'][] = array('caracteristica_id' => $filtro[2], 'caracteristicas_valor_id' => $filtro[3]);
					}

				} else if ($filtro[0] == 'p') {
					$retorno['preco']['inicial'] = $filtro[1];
					$retorno['preco']['final'] = $filtro[2];
					$retorno['preco']['min_original'] = $filtro[3];
					$retorno['preco']['max_original'] = $filtro[4];

				} else if ($filtro[0] == 'm') {
					$retorno['marca'] = $filtro[1];
				} else if ($filtro[0] == 't') {
					$retorno['tag'][] = $filtro[1];
				}
			}

			return $retorno;
		}

		return false;

	}

	private function getCaracteristicasSkus($skus) {

		$caracteristicas_skus = array();

		if (isset($skus[0]['CaracteristicasValoresSelecionado']) && count($skus[0]['CaracteristicasValoresSelecionado']) > 0) {

			// Monta o json a ser usado para verificar o estoque pela seleção da caracteristica
			$json_caracteristicas_skus = array();
			$sku_selecao = false;

			foreach ($skus as $sku) {
				$json_caracteristicas_skus[$sku['id']] = array(
					'situacao_estoque' => $sku['situacao_estoque'],
					'caracteristicas' => array(
						'valores' => array(
							'ids' => array()
						)
					)
				);

				foreach ($sku['CaracteristicasValoresSelecionado'] as $caracteristicas_valor_selecionado) {

					if ($caracteristicas_valor_selecionado['Caracteristica']['selecao']) {
						$sku_selecao = true;

						// Monta array das caracteristicas dos skus do produto em questão
						if (!array_key_exists ($caracteristicas_valor_selecionado['Caracteristica']['id'], $caracteristicas_skus)) {
							$caracteristicas_skus[$caracteristicas_valor_selecionado['Caracteristica']['id']] = array(
								'Caracteristica' => $caracteristicas_valor_selecionado['Caracteristica'],
								'CaracteristicasValor' => array(
									$caracteristicas_valor_selecionado['CaracteristicasValor']['id'] => $caracteristicas_valor_selecionado['CaracteristicasValor']
								)
							);
						} else {
							$caracteristicas_skus[$caracteristicas_valor_selecionado['Caracteristica']['id']]['CaracteristicasValor'][$caracteristicas_valor_selecionado['CaracteristicasValor']['id']] = $caracteristicas_valor_selecionado['CaracteristicasValor'];
						}

						// Valor inicial igual a false. Esse valor vai ser modificado de acordo com as caracteristicas que o cliente for selecionando.
						$json_caracteristicas_skus[$sku['id']]['caracteristicas']['valores']['ids'][$caracteristicas_valor_selecionado['caracteristicas_valor_id']] = false;
					}
				}
			}

			if ($sku_selecao) {
				$caracteristicas_skus['json'] = $json_caracteristicas_skus;
			}
		}

		return $caracteristicas_skus;
	}

}
?>