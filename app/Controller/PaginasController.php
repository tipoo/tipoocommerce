<?php
App::uses('AppController', 'Controller');

class PaginasController extends AppController {

	public $uses = array(
		'Pagina',
		'Categoria',
		'Produto',
		'PaginasProduto',
		'ColecoesLocal',
		'Banner',
		'Slug',
		'Colecao',
		'Pedido',
		'Sku',
		'Marca',
		'Loja',
		'HtmlsLocal',
		'TermosBuscado'
	);

	public $components = array(
		'CustomTagsParser' => array('actions' => array('ver'))
	);

	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('admin_index', 'ver');
	}

	public function ver() {
		$this->MenuCategorias->setMenuCategorias();
		$this->Banners->setTipos('generico');
		$this->ItemMenus->setMenu();
		$this->autoRender = false;
		$id = $this->params['id'];

		$pagina = $this->Pagina->find('first', array(
			'conditions' => array(
				'Pagina.id' => $id,
				'Pagina.ativo'
			)
		));

		$this->Colecoes->setColecoesLocais(array('pagina_id' => $id));

		$controleHtml = $this->HtmlsLocal->get($opts = array('pagina_id' => $id));

		$this->Lojas->setLojas();

		$title_for_layout = $pagina['Pagina']['title'] ? $pagina['Pagina']['title'] : $pagina['Pagina']['descricao'] . ' - ' . Configure::read('Loja.nome');
		$meta_title = $pagina['Pagina']['meta_title'] ? $pagina['Pagina']['meta_title'] : $pagina['Pagina']['descricao'] . ' - ' . Configure::read('Loja.nome');

		$this->set(compact('title_for_layout', 'meta_title'));

		// Verificando se tem metatags customizada  e sobreescrevendo as padrões
		if (!empty($pagina['Pagina']['meta_description'])) {
			$this->set('meta_description', $pagina['Pagina']['meta_description']);
		}

		$this->layout = $pagina['Pagina']['layout_ctp'];
		$this->CustomTagsParser->viewRender = $pagina['Pagina']['arquivo_ctp'];

		$this->set(compact('controleHtml'));
		$this->set('skus', $this->Session->read('Carrinho.Skus'));
		$this->set('mensagens_skus', $this->Session->read('Carrinho.Mensagens'));
	}

	public function admin_dashboard() {
		// Relatório de vendas
		$vendas = $this->Pedido->find('all', array(
			'contain' => array(
				'Cliente.nome',
				'Endereco' => array(
					'Cidade' => array('nome', 'uf'),
					'fields' => array('id')
				),
				'StatusPedido',
				'PedidoSku'
			),
			'conditions' => array(
				'StatusPedido.venda_concluida' => true
			),
			'order' => array(
				'Pedido.data_hora' => 'DESC'
			)
		));

		$qtd_vendas = 0;
		//$qtd_produtos_vendidos = 0;
		$qtd_skus_vendidos = 0;

		$receita_bruta = 0; //com frete
		$valor_total_vendas_liquido = 0;

		$qtd_frete = 0;
		$total_valor_frete = 0;

		$ticket_medio = 0;

		foreach ($vendas as $key => $venda) {
			$receita_bruta += $venda['Pedido']['valor_total'];
			$valor_total_vendas_liquido += $venda['Pedido']['valor_produtos'];
			$qtd_skus_vendidos += count($venda['PedidoSku']);
			if ($venda['Pedido']['valor_frete'] != '0.00') {
				$total_valor_frete += $venda['Pedido']['valor_frete'];
				$qtd_frete++;
			}
		}

		$qtd_vendas = count($vendas);

		if ($qtd_vendas > 0) {
			$ticket_medio = ($receita_bruta / $qtd_vendas);
		}

		$this->set('qtd_vendas', $qtd_vendas);
		$this->set('qtd_skus_vendidos', $qtd_skus_vendidos);
		$this->set('receita_bruta', $receita_bruta);
		$this->set('valor_total_vendas_liquido', $valor_total_vendas_liquido);
		$this->set('qtd_frete', $qtd_frete);
		$this->set('total_valor_frete', $total_valor_frete);
		$this->set('ticket_medio', $ticket_medio);

		$this->set('vendas', $vendas);

		// Gráfico vendas por estado

		// Gráfico termos mais buscados
		$de = date('Y-m-d', strtotime('-1 year day'));
		if (isset($this->request->params['named']['de'])) {
			$de = $this->request->params['named']['de'];
		}

		$ate = date('Y-m-d');
		if (isset($this->request->params['named']['ate'])) {
			$ate = $this->request->params['named']['ate'];
		}

		$conditions = array(
			'TermosBuscado.created >=' => $de . ' 00:00:00',
			'TermosBuscado.created <=' => $ate . ' 23:59:59',
		);

		$termos_buscados = $this->TermosBuscado->find('all', array(
		 	'conditions' => $conditions,
		 	'group' => 'TermosBuscado.termo',
		 	'fields' => array('id', 'termo', 'COUNT(TermosBuscado.termo) as qtd_termos'),
		 	'order' => array(
		 	 	'qtd_termos' => 'DESC'
	 	 	),
	 	 	'limit' => 5
 		));

		$termos_buscados_json = array();

		$total = 0;

		foreach ($termos_buscados as $termo_buscado) {
			$total += $termo_buscado[0]['qtd_termos'];
		}

		foreach ($termos_buscados as $termo_buscado) {
			$shortName = strlen($termo_buscado['TermosBuscado']['termo']) < 8 ? $termo_buscado['TermosBuscado']['termo'] : substr($termo_buscado['TermosBuscado']['termo'], 0, 5) . '...';

			$termos_buscados_json[] = array(
				'name' => $termo_buscado['TermosBuscado']['termo'], 
				'shortName' => $shortName,
				'y' => $termo_buscado[0]['qtd_termos'] / $total * 100
			);
		}

		$this->set('termos_buscados_json', json_encode($termos_buscados_json));

		// Relatório do Catálogo
		$qtd_produtos = $this->Produto->find('count', array(
			'contain' => false,
			'conditions' => array(
				'Produto.ativo'
			)
		));

		$qtd_skus = $this->Sku->find('count', array(
			'contain' => array(
				'Produto'
			),
			'conditions' => array(
				'Produto.ativo',
				'Sku.ativo'
			)
		));

		$qtd_categorias = $this->Categoria->find('count', array(
			'contain' => false,
			'conditions' => array(
				'Categoria.ativo'
			)
		));

		$qtd_marcas = $this->Marca->find('count', array(
			'contain' => false,
			'conditions' => array(
				'Marca.ativo'
			)
		));

		$this->set(compact('qtd_produtos', 'qtd_skus', 'qtd_categorias', 'qtd_marcas'));
	}

	public function admin_index() {

		$pagina = null;
		if (isset($this->request->params['named']['pagina'])) {
			$pagina = $this->request->params['named']['pagina'];
			$this->request->data['Filtro']['pagina'] = $pagina;
		}

		$conditions = array(
			'Pagina.ativo' => true
		);

		if($pagina != '') {
			$conditions[] = 'Pagina.descricao LIKE "%' . $pagina . '%"';
		}

		$this->paginate = array(
			'conditions' => $conditions,
			'order' => array(
				'Pagina.sistema' => 'DESC',
				'Pagina.descricao' => 'ASC'
			),
			'limit' => Configure::read('Sistema.Paginacao.limit')
		);
		$this->set('paginas', $this->paginate());

	}

	public function admin_adicionar($id = null) {
		if ($this->request->is('post')) {
			// Verificando se o slug já existe
			$urlSlug = $this->Slugger->regularizarSlugComposto($this->request->data('Slug.url'));
			$this->request->data('Slug.url', $urlSlug);
			$slugExiste = $this->Slug->find('first', array(
				'conditions' => array(
					'Slug.ativo',
					'Slug.url' => $urlSlug
				)
			));

			if (!$slugExiste) {
				$this->request->data('Slug.controller', 'paginas');
				$this->request->data('Slug.action', 'ver');
				$this->request->data('Slug.custom_parser_class', 'PaginaParser');

				$this->Pagina->create();
				if ($this->Pagina->saveAll($this->request->data)) {
					$this->Session->setFlash('Página salva com sucesso.', FLASH_SUCCESS);
					$this->backToPaginatorIndex();
				} else {
					$this->Session->setFlash('Ocorreu um erro ao tentar salvar a Página. Por favor, tente novamente.', FLASH_ERROR);
				}
			} else {
				$this->Session->setFlash('O slug selecionado já está em uso. Por favor, informe outro slug tente novamente.', FLASH_ERROR);
			}
		}
	}

	public function admin_editar($id = null) {
		$this->Pagina->id = $id;
		$paginaEditando = $this->Pagina->findById($id);

		if (!$this->Pagina->exists()) {
			throw new NotFoundException('Página inexistente.');
		}

		if ($this->request->is('post') || $this->request->is('put')) {
			// Verificando se o slug já existe
			$urlSlug = $this->Slugger->regularizarSlugComposto($this->request->data('Slug.url'));
			$this->request->data('Slug.url', $urlSlug);
			$slugExiste = $this->Slug->find('first', array(
				'conditions' => array(
					'Slug.ativo',
					'Slug.id <>' => $this->request->data('Slug.id'),
					'Slug.url' => $urlSlug
				)
			));

			if (!$slugExiste) {
				if ($this->Pagina->saveAll($this->request->data)) {
					$this->Session->setFlash('Página salva com sucesso.', FLASH_SUCCESS);
					$this->backToPaginatorIndex();
				} else {
					$this->Session->setFlash('Ocorreu um erro ao tentar salvar a Página. Por favor, tente novamente.', FLASH_ERROR);
				}
			} else {
				$this->Session->setFlash('Já existe uma url  "<strong>' .  $this->request->data['Slug']['url']  . '</strong>". Por favor, cadastre uma url diferente.', FLASH_ERROR);
				$this->request->data['Slug']['url'] = $paginaEditando ['Slug']['url'];
			}
		} else {
			$this->request->data = $this->Pagina->read(null, $id);
			$this->set('pagina', $paginaEditando);
		}
	}

	public function admin_excluir($id = null, $slug_id = null) {
		$this->Pagina->id = $id;
		$this->Slug->id = $slug_id;

		if (!$this->Pagina->exists() || !$this->Slug->exists()) {
			throw new NotFoundException('Página ou Slug inexistente.');
		}

		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Pagina->saveField('ativo', false, false) && $this->Slug->saveField('ativo', false, false)) {
				$this->Session->setFlash('Página excluída com sucesso.', FLASH_SUCCESS);
				$this->backToPaginatorIndex();
			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar excluir a Página. Por favor, tente novamente.', FLASH_ERROR);
			}
		}
	}

	// Produtos
	public function admin_produtos($id = null) {
		if ($this->request->is('post') || $this->request->is('put')) {
			$produtoExiste = $this->PaginasProduto->find('count', array(
				'conditions' => array(
					'OR' => array(
						'PaginasProduto.produto_id' => $this->request->data('PaginasProduto.produto_id'),
						'PaginasProduto.chave' => $this->request->data('PaginasProduto.chave'),
					),
					'PaginasProduto.pagina_id' => $id
				)
			));

			if (!$produtoExiste) {
				$this->PaginasProduto->create();
				if ($this->PaginasProduto->save($this->request->data)) {
					$this->Session->setFlash('Produto adicionado a página com sucesso.', FLASH_SUCCESS);
				} else {
					$this->Session->setFlash('Ocorreu um erro ao tentar adicionar produto a página. Por favor, tente novamente.', FLASH_ERROR);
				}
			} else {
				$this->Session->setFlash('O produto que você tentou adicionar já estava na página ou chave "'.$this->request->data('PaginasProduto.chave').'" já está sendo utilizada.', FLASH_WARNING);
			}

			$this->request->data('PaginasProduto.produto_id', null);
			$this->request->data('PaginasProduto.descricao', null);
			$this->request->data('PaginasProduto.chave', null);
		}

		$pagina = $this->Pagina->find('first', array(
			'contain' => array(
				'PaginasProduto',
				'PaginasProduto.Produto'
			),
			'conditions' => array(
				'Pagina.ativo',
				'Pagina.id' => $id
			)
		));
		$this->request->data('PaginasProduto.pagina_id', $id);
		$this->set('pagina', $pagina);
	}

	public function admin_ajax_buscar_produtos($descricao = null) {
		$produtos = $this->Produto->find('all', array(
			'contain' => array(
				'Marca.descricao'
			),
			'conditions' => array(
				'Produto.ativo',
				'Produto.descricao LIKE "%'.$descricao.'%"'
			)
		));

		$produtosArray = array();
		foreach ($produtos as $produto) {
			$produtosArray[] = array(
				'id' => $produto['Produto']['id'],
				'name' => $produto['Produto']['descricao'].' - '.$produto['Marca']['descricao'].' - #'.$produto['Produto']['id']
			);
		}

		$this->renderJson($produtosArray);
	}

	public function admin_excluir_produto($id = null, $paginasProduto_id = null) {
		$this->PaginasProduto->id = $paginasProduto_id;
		if ($this->PaginasProduto->delete()) {
			$this->Session->setFlash('Produto removido da página com sucesso.', FLASH_SUCCESS);
		} else {
			$this->Session->setFlash('Ocorreu um erro ao tentar remover o produto da página. Por favor, tente novamente.', FLASH_ERROR);
		}

		$this->redirect(array('controller' => 'paginas', 'action' => 'produtos', 'admin' => true, $id));
	}

	/*
	** Coleções
	*/
	public function admin_colecoes($id = null) {
		if ($this->request->is('post') || $this->request->is('put')) {
			$colecaoExiste = $this->ColecoesLocal->find('count', array(
				'conditions' => array(
					'OR' => array(
						'ColecoesLocal.colecao_id' => $this->request->data('ColecoesLocal.colecao_id'),
						'ColecoesLocal.chave' => $this->request->data('ColecoesLocal.chave'),
					),
					'ColecoesLocal.pagina_id' => $id,
					'ColecoesLocal.ativo' => true
				)
			));

			if (!$colecaoExiste) {
				$this->ColecoesLocal->create();
				if ($this->ColecoesLocal->save($this->request->data)) {
					$this->Session->setFlash('Coleção adicionada a página com sucesso.', FLASH_SUCCESS);
				} else {
					$this->Session->setFlash('Ocorreu um erro ao tentar adicionar coleção a página. Por favor, tente novamente.', FLASH_ERROR);
				}
			} else {
				$this->Session->setFlash('O coleção que você tentou adicionar já estava na página ou chave "' . $this->request->data('ColecoesLocal.chave') . '" já está sendo utilizada.', FLASH_WARNING);
			}

			$this->request->data('ColecoesLocal.colecao_id', null);
			$this->request->data('ColecoesLocal.descricao', null);
			$this->request->data('ColecoesLocal.chave', null);
		}

		$pagina = $this->Pagina->find('first', array(
			'contain' => array(
				'ColecoesLocal' => array(
					'Colecao',
					'conditions' => array(
						'ColecoesLocal.ativo' => true
					)
				)
			),
			'conditions' => array(
				'Pagina.ativo' => true,
				'Pagina.id' => $id
			)
		));

		$this->request->data('ColecoesLocal.pagina_id', $id);
		$this->set('pagina', $pagina);
	}

	public function admin_ajax_buscar_colecoes($descricao = null) {
		$colecoes = $this->Colecao->find('all', array(
			'contain' => false,
			'conditions' => array(
				'Colecao.ativo',
				'Colecao.descricao LIKE "%'.$descricao.'%"'
			)
		));

		$colecoesArray = array();
		foreach ($colecoes as $colecao) {
			$colecoesArray[] = array(
				'id' => $colecao['Colecao']['id'],
				'name' => $colecao['Colecao']['descricao'].' - #'.$colecao['Colecao']['id']
			);
		}

		$this->renderJson($colecoesArray);
	}

	public function admin_excluir_colecao($id = null, $paginasColecao_id = null) {
		$this->ColecoesLocal->id = $paginasColecao_id;
		if ($this->ColecoesLocal->delete()) {
			$this->Session->setFlash('Coleção removido da página com sucesso.', FLASH_SUCCESS);
		} else {
			$this->Session->setFlash('Ocorreu um erro ao tentar remover a coleção da página. Por favor, tente novamente.', FLASH_ERROR);
		}

		$this->redirect(array('controller' => 'paginas', 'action' => 'colecoes', 'admin' => true, $id));
	}

}
?>