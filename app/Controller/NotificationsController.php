<?php
App::uses('AppController', 'Controller');

class NotificationsController extends AppController {

	public $components = array('PagSeguro.Notificacao');

	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('index');
	}

	public function index() {

		if ($this->request->is('post')) {

			$tipo = $this->request->data['notificationType'];
	        $codigo = $this->request->data['notificationCode'];

	        if ( $this->Notificacao->obterDadosTransacao($tipo, $codigo) ) {
	            // retorna somente os dados do comprador
	            $dadosUsuario = $this->Notificacao->obterDadosUsuario();

	            // retorna o status da transação
	            $statusTransacao = $this->Notificacao->obterStatusTransacao();

	            // retorna os dados de pagamento (tipo de pagamento e forma de pagamento)
	            $dadosPagamento = $this->Notificacao->obterDadosPagamento();

	            // retorna a data que a compra foi realizada e última notificação
	            $dataTransacao = $this->Notificacao->obterDataTransacao();

	            // retorna os dados de produtos comprados
	            $produtos = $this->Notificacao->obterValores()


	            // agora exibindo todos os resultados

	            $this->log($dadosUsuario, LOG_DEBUG);
	            /*
	            array(
	                'nome' => 'Andre Cardoso',
	                'email' => 'andrecardosodev@gmail.com',
	                'telefoneCompleto' => '41 00000000',
	                'codigoArea' => '41',
	                'numeroTelefone' => '00000000',
	                'endereco' => 'Rua Teste',
	                'numero' => '1234',
	                'complemento' => 'Complemento',
	                'bairro' => 'Centro',
	                'cidade' => 'Curitiba',
	                'cep' => '80000000',
	                'uf' => 'PR',
	                'pais' => 'BRA'
	            )
	            */

	            $this->log($statusTransacao, LOG_DEBUG);
	            /*
	            array(
	                'id' => (int) 1,
	                'descricao' => 'Aguardando pagamento'
	            )
	            */

	            $this->log($dadosPagamento, LOG_DEBUG);
	            /*
	            array(
	                'tipo' => 'Boleto',
	                'metodo' => 'Boleto Santander'
	            )
	            */

	            $this->log($dataTransacao, LOG_DEBUG);
	            /*
	            array(
	                'iso' => '2013-02-16T19:35:53.000-02:00',
	                'ptBr' => '16/02/2013 19:35:53',
	                'ultimaTentativaIso' => '2013-02-16T19:36:00.000-02:00',
	                'ultimaTentativaPtBr' => '16/02/2013 19:36:00'
	            )
	            */

	            $this->log($produtos, LOG_DEBUG);
	            /*
	            array(
	                'valorTotal' => '0.01',
	                'descontoAplicado' => '0.00',
	                'valorExtra' => '0.00',
	                'produtos' => array(
	                        (int) 0 => array(
	                                'id' => '1',
	                                'descricao' => 'Produto Teste',
	                                'quantidade' => '1',
	                                'valorUnitario' => '0.01',
	                                'peso' => '1000',
	                                'frete' => null
	                        )
	                )
	            )
	            */


	        }
		}

	}
}

?>


