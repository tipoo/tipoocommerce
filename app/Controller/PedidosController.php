<?php
App::uses('ApiController', 'Controller');

class PedidosController extends ApiController {

	public $uses = array('Pedido', 'StatusPedido', 'PagseguroTransparenteBandeira');

	public $components = array(
		'Cielo.Cielo'
	);

	public function admin_index() {

		$de = date('Y-m-d', strtotime('-31 day'));
		if (isset($this->request->params['named']['de'])) {
			$de = $this->request->params['named']['de'];
		}

		$ate = date('Y-m-d');
		if (isset($this->request->params['named']['ate'])) {
			$ate = $this->request->params['named']['ate'];
		}

		$statusPedidoFiltro = $this->StatusPedido->find('list', array(
			'contain' => false,
			'conditions' => array(
				'StatusPedido.ativo'
			),
			'order' => array(
				'StatusPedido.ordem' => 'ASC'
			)
		));

		$conditions = array(
			'Pedido.data_hora >=' => $de . ' 00:00:00',
			'Pedido.data_hora <=' => $ate . ' 23:59:59'
		);

		if (!empty($this->request->params['named']['statusPedidoFiltroId'])) {
			$conditions['Pedido.status_id'] = $this->request->params['named']['statusPedidoFiltroId'];
			$this->request->data('statusPedidoFiltroId', $this->request->params['named']['statusPedidoFiltroId']);
		}

		$this->paginate = array(
			'contain' => array(
				'Cliente',
				'StatusPedido',
				'Endereco' => array(
					'Cidade' => array('nome', 'uf'),
					'fields' => array('id')
				),
				//'PedidoSku' => array('id', 'qtd', 'preco_total')
			),
			'conditions' => $conditions,
			'order' => array(
				'Pedido.data_hora' => 'DESC'
			),
			'limit' => 40
		);

		$this->set('pedidos', $this->paginate());
		$this->set('de', $de);
		$this->set('ate', $ate);
		$this->set('statusPedidoFiltro', $statusPedidoFiltro);

	}

	public function admin_ajax_visualizar($id = null) {
		//$this->layout = 'clean';

		$pedido = $this->Pedido->find('first', array(
			'contain' => array(
				'Cliente' => array(
					'CamposComplementaresValoresSelecionado' => array(
						'CamposComplementar',
						'CamposComplementaresValor'
					)
				),
				'StatusPedido',
				'Endereco' => array(
					'Cidade' => array('nome', 'uf')
				),
				'PedidoSku.PedidoCaracteristica' => array('caracteristica', 'valor', 'referente'),
				'PedidoSku.CompreJuntoPromocao',
				'Promocao',
				'Promocao.PromocaoCausa',
				'Promocao.PromocaoEfeito',
				'FreteTransportadora',
				'PedidoSku.Sku.Produto',
				'PedidoSku.Promocao',
				'PedidoSku.PedidoSkuServico'
			),
			'conditions' => array(
				'Pedido.id' => $id
			)
		));

		if ($pedido['Pedido']['forma_pagamento'] == 'PagSeguroTransparente') {
			$bandeiras_pagseguro = $this->PagseguroTransparenteBandeira->find('list', array(
				'fields' => array(
					'codigo',
					'nome'
				)
			));

			$this->set('bandeiras_pagseguro', $bandeiras_pagseguro);
		}

		if ($this->Configuracao->get('mercado_livre_ativo')) {
			$this->MercadoLivreFrete->set_url_etiqueta($pedido);
			$this->MercadoLivreFeedback->set($pedido);
		}

		$this->set('pedido', $pedido);
	}

	public function admin_imprimir() {
		//$this->layout = 'clean';

		if ($this->request->is('post')) {
			$pedidos = $this->Pedido->find('all', array(
				'contain' => array(
					'Cliente',
					'StatusPedido.descricao',
					'Endereco' => array(
						'Cidade' => array('nome', 'uf')
					),
					'PedidoSku.PedidoCaracteristica' => array('caracteristica', 'valor', 'referente'),
					'PedidoSku.CompreJuntoPromocao',
					'Promocao',
					'Promocao.PromocaoCausa',
					'Promocao.PromocaoEfeito',
					'FreteTransportadora',
					'PedidoSku.Sku.Produto',
					'PedidoSku.Promocao',
					'PedidoSku.PedidoSkuServico'
				),
				'conditions' => array(
					'Pedido.id' => $this->request->data['Pedido']
				),
				'order' => array(
					'Pedido.data_hora' => 'DESC'
				),
			));

			$this->set('pedidos', $pedidos);
		} else {
			$this->redirect(array('controller' => 'pedidos', 'action' => 'index'));
		}
	}

	public function admin_imprimir_pdf($id) {
		$this->layout = 'pdf';

		$pedido = $this->Pedido->get($id);

		if ($this->Configuracao->get('mercado_livre_ativo')) {
			$this->MercadoLivreFrete->set_url_etiqueta($pedido);
		}
	}

	public function admin_ajax_alterar_status($id = null, $status_id = null, $codigo_rastreamento = null) {
		$pedido['Pedido'] = array();

		$status = $this->StatusPedido->find('first', array(
			'contain' => false,
			'conditions' => array(
				'StatusPedido.id' => $status_id
			)
		));

		if ($this->Configuracao->get('cielo_ativo') && $status['StatusPedido']['status_cielo_requisicao_captura']) {
			// (provisório)TODO - Mudar para component da Cielo quando for fazer OnePageCheckout
			$pedido = $this->capturarTransacaoCielo($id);
		}

		// Verificando se o pedido gera e envia nota fiscal através do Bling
		if ($status['StatusPedido']['status_bling_gera_nf']) {
			$pedido = $this->Bling->gerar_nf($id);
		}

		if (!isset($pedido['error'])) {
			$this->Pedido->id = $id;

			$pedido['Pedido']['status_id'] = $status_id;
			$pedido['Pedido']['codigo_rastreamento'] = $codigo_rastreamento;

			if ($this->Pedido->save($pedido)) {
				$pedido = $this->RegrasStatus->status_alterado($id);
				$json = array('sucesso' => true, 'pedido' => $pedido);
			} else {
				$json = array('sucesso' => false, 'mensagem' => 'Ocorreu um erro ao tentar alterar o status. Por favor, tente novamente.');
			}
		} else {
			$json = array('sucesso' => false, 'mensagem' => $pedido['error']['mensagem']);
		}

		$this->renderJson($json);
	}

	// (provisório)TODO - Mudar para component da Cielo quando for fazer OnePageCheckout
	private function capturarTransacaoCielo($pedido_id) {
		$pedido = $this->Pedido->find('first', array(
			'contain' => false,
			'conditions' => array(
				'Pedido.id' => $pedido_id
			)
		));

		$cielo_transacao = unserialize($pedido['Pedido']['retorno_cielo']);
		$cielo_transacao_id = $cielo_transacao['transacao']['tid'];
		$captura = $this->Cielo->capturarTransacao($cielo_transacao_id);

		$pedido['Pedido'] = array();
		if($captura) {
			$pedido['Pedido']['retorno_cielo'] = serialize($captura);
		} else {
			$erro = '(' . $this->Cielo->erro['codigo'] . ') ' . $this->Cielo->erro['mensagem'];
			$pedido['error']['mensagem'] = $erro;
		}

		return $pedido;
	}

}
?>