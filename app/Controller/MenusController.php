<?php
App::uses('AppController', 'Controller');

class MenusController extends AppController {

	public function admin_index() {

		$conditions = array(
			//'Menu.ativo' => true
		);

		$this->paginate = array(
			'conditions' => $conditions,
			'order' => array(
				'Menu.descricao' => 'ASC',

			),
			'limit' => Configure::read('Sistema.Paginacao.limit')
		);
		$this->set('menus', $this->paginate());

	}

	public function admin_adicionar($id = null) {

		if ($this->request->is('post')) {
			$this->Menu->create();
	
			if ($this->Menu->save($this->request->data)) {
				$this->Session->setFlash('Menu salvo com sucesso.', FLASH_SUCCESS);
				$this->backToPaginatorIndex();
			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar salvar o menu. Por favor, tente novamente.', FLASH_ERROR);
			}
		}
	}

	public function admin_editar($id = null) {
		$this->Menu->id = $id;

		if (!$this->Menu->exists()) {
			throw new NotFoundException('Menu inexistente.');
		}
		if ($this->request->is('post') || $this->request->is('put')) {

			if ($this->Menu->save($this->request->data)) {

				$this->Session->setFlash('Menu salvo com sucesso.', FLASH_SUCCESS);
				$this->backToPaginatorIndex();
			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar salvar o menu. Por favor, tente novamente.', FLASH_ERROR);
			}
		} else {
			$this->request->data = $this->Menu->read(null, $id);

		}
	}

	public function admin_excluir($id = null) {
		$this->Menu->id = $id;

		if (!$this->Menu->exists()) {
			throw new NotFoundException('Menu inexistente.');
		}

		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Menu->saveField('ativo', false, false)) {
				$this->Session->setFlash('Menu excluído com sucesso.', FLASH_SUCCESS);
				$this->backToPaginatorIndex();
			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar excluir o menu. Por favor, tente novamente.', FLASH_ERROR);
			}
		}
	}

	public function admin_ativar($id = null) {
		$this->Menu->id = $id;

		if (!$this->Menu->exists()) {
			throw new NotFoundException('Menu inexistente.');
		}

		if ($this->request->is('post')) {

			if ($this->Menu->saveField('ativo', true, false)) {
				
				$this->Session->setFlash('Menu ativado com sucesso.', FLASH_SUCCESS);
				$this->redirect($this->referer());
			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar ativar o menu . Por favor, tente novamente.', FLASH_ERROR);
			}
		}

	}

}
?>