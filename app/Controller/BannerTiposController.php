<?php

class BannerTiposController extends AppController {

	public $uses = array('BannerTipo', 'Categoria', 'Marca', 'Banner');

	public function admin_index() {

		$conditions = array(
			'BannerTipo.ativo' => true
		);

		$this->paginate = array(
			'conditions' => $conditions,
			'limit' => 50
		);

		$this->set('banners', $this->paginate());
		$this->set('tipo', $this->getTipoBanner());
	}

	public function admin_adicionar($id = null) {

		if ($this->request->is('post')) {

			$this->BannerTipo->create();
			if ($this->BannerTipo->save($this->request->data)) {
				$this->Session->setFlash('Tipo de Banner salvo com sucesso.', FLASH_SUCCESS);
				$this->backToPaginatorIndex();
			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar salvar o tipo de banner. Por favor, tente novamente.', FLASH_ERROR);
			}
		}

		$this->set('tipo', $this->getTipoBanner());
	}

	public function admin_editar($id = null) {
		$this->BannerTipo->id = $id;

		if (!$this->BannerTipo->exists()) {
			throw new NotFoundException('Tipo de Banner inexistente.');
		}

		if ($this->request->is('post') || $this->request->is('put')) {

			$this->request->data['BannerTipo']['id'] = $id;

			if ($this->BannerTipo->save($this->request->data)) {
				$this->Session->setFlash('Tipo de Banner editado com sucesso.', FLASH_SUCCESS);
				$this->backToPaginatorIndex();
			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar editar o tipo de banner. Por favor, tente novamente.', FLASH_ERROR);
			}
		} else {
			$this->request->data = $this->BannerTipo->read(null, $id);
			$this->set('tipo', $this->getTipoBanner());
		}
	}

	public function admin_excluir($id = null) {
		$this->BannerTipo->id = $id;

		if (!$this->BannerTipo->exists()) {
			throw new NotFoundException('Tipo de Banner inexistente.');
		}

		if ($this->request->is('post') || $this->request->is('put')) {

			$banners = $this->Banner->find('count', array(
				'conditions' => array(
					'Banner.ativo' => true,
					'Banner.banner_tipo_id' => $id
				)
			));

			if ($banners == 0) {

				if ($this->BannerTipo->saveField('ativo', false, false)) {
					$this->Session->setFlash('Tipo de Banner excluído com sucesso.', FLASH_SUCCESS);
					$this->backToPaginatorIndex();
				} else {
					$this->Session->setFlash('Ocorreu um erro ao tentar excluir o tipo de banner. Por favor, tente novamente.', FLASH_ERROR);
				}

			} else {
				$this->Session->setFlash('Não é possível exluir este tipo de banner, pois ela já se encontra em uso.', FLASH_ERROR);
				$this->backToPaginatorIndex();
			}
		}
	}

	public function admin_ajax_buscar_categorias() {
		$categorias = $this->Categoria->find('threaded', array(
			'contain' => false,
			'parent' => 'categoria_pai_id',
			'conditions' => array(
				'Categoria.ativo'
			),
			'fields' => array('id', 'descricao', 'categoria_pai_id')
		));

		if (count($categorias) > 0) {
			$json = array('sucesso' => true, 'categorias' => $categorias);
		} else {
			$json = array('sucesso' => false, 'mensagem' => 'Nenhuma categoria encontrada.');
		}

		$this->autoRender = false;
		$output = fopen('php://output', 'w');
		fputs($output, json_encode($json));
		fclose($output);
	}

	public function admin_ajax_buscar_marcas() {

		$marcas = $this->Marca->find('all', array(
			'contain' => false,
			'conditions' => array(
				'Marca.ativo'
			),
			'fields' => array('id', 'descricao')
		));

		if (count($marcas) > 0) {
			$json = array('sucesso' => true, 'marcas' => $marcas);
		} else {
			$json = array('sucesso' => false, 'mensagem' => 'Nenhuma marca encontrada.');
		}

		$this->autoRender = false;
		$output = fopen('php://output', 'w');
		fputs($output, json_encode($json));
		fclose($output);
	}


	private function getTipoBanner() {
		return array(
			'home' => 'Home',
			'marca' => 'Marca',
			'categoria' => 'Categoria',
			'generico' => 'Genérico'
		);
	}

}

?>