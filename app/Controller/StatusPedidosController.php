<?php
App::uses('AppController', 'Controller');

class StatusPedidosController extends AppController {

	public $uses = array('StatusPedido', 'Pedido');

	public function admin_index() {

			$conditions = array(
				'StatusPedido.ativo' => true
			);

			$this->paginate = array(
				'conditions' => $conditions,
				'order' => array(
					'StatusPedido.ordem' => 'ASC'
				),
				'limit' => Configure::read('Sistema.Paginacao.limit')
			);
			$this->set('statusPedidos', $this->paginate());

	}

	public function admin_adicionar($id = null) {

		if ($this->request->is('post')) {
			$this->StatusPedido->create();
			if ($this->StatusPedido->save($this->request->data)) {
				$this->Session->setFlash('Status de Pedido salvo com sucesso.', FLASH_SUCCESS);
				$this->backToPaginatorIndex();
			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar salvar o Status de Pedido. Por favor, tente novamente.', FLASH_ERROR);
			}
		}
	}

	public function admin_editar($id = null) {
		$this->StatusPedido->id = $id;
		$statusPedidoEditando = $this->StatusPedido->findById($id);

		if (!$this->StatusPedido->exists()) {
			throw new NotFoundException('Status de Pedido inexistente.');
		}
		if ($this->request->is('post') || $this->request->is('put')) {

			if ($this->StatusPedido->save($this->request->data)) {
				$this->Session->setFlash('Status de Pedido salvo com sucesso.', FLASH_SUCCESS);
				$this->backToPaginatorIndex();
			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar salvar o Status de Pedido. Por favor, tente novamente.', FLASH_ERROR);
			}
		} else {
			$this->request->data = $this->StatusPedido->read(null, $id);
			$this->set('statusPedido', $statusPedidoEditando);
		}
	}

	public function admin_excluir($id = null) {
		$this->StatusPedido->id = $id;

		if (!$this->StatusPedido->exists()) {
			throw new NotFoundException('Status de Pedido inexistente.');
		}

		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->StatusPedido->saveField('ativo', false, false)) {
				$this->Session->setFlash('StatusPedido excluído com sucesso.', FLASH_SUCCESS);
				$this->backToPaginatorIndex();
			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar excluir o Status de Pedido. Por favor, tente novamente.', FLASH_ERROR);
			}
		}
	}

	public function admin_ajax_carregar_status($id = null) {

		$status = $this->StatusPedido->find('all', array(
			'contain' => false,
			'conditions' => array(
				'StatusPedido.ativo' => true
			),
			'order' => array(
				'StatusPedido.ordem' => 'ASC'
			)
		));

		$pedido = $this->Pedido->find('first', array(
			'contain' => false,
			'conditions' => array(
				'Pedido.id' => $id
			)
		));

		$json = array('sucesso' => true, 'pedidosStatus' => $status, 'pedido' => $pedido['Pedido']);

		$this->autoRender = false;
		$output = fopen('php://output', 'w');
		fputs($output, json_encode($json));
		fclose($output);
	}

	public function admin_ajax_ordenar() {

		$status = $this->StatusPedido->find('count');

		$count_status_pedido = 1;

		while ($count_status_pedido <= $status) {

			$this->request->data[$count_status_pedido]['StatusPedido']['id'] = $this->params['named']['ordem_' . $count_status_pedido];
			$this->request->data[$count_status_pedido]['StatusPedido']['ordem'] = $count_status_pedido;

			$count_status_pedido++;
		}

		if ($this->StatusPedido->saveMany($this->request->data, array('deep' => true))) {
			$json = array('sucesso' => true);
		} else {
			$json = array('sucesso' => false, 'mensagem' => 'Ocorreu um erro ao tentar ordenar os skus. Por favor, tente novamente.');
		}

		$this->renderJson($json);

	}

}
?>