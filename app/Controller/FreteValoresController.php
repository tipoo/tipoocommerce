<?php
App::uses('AppController', 'Controller');

class FreteValoresController  extends AppController {

	public $uses = array('FreteValor', 'FreteTransportadora');

	public function admin_index($id = null) {

		$conditions = array(
			'FreteValor.ativo' => true,
			'FreteValor.frete_transportadora_id' => $id
		);

		$order = array(
			'FreteValor.descricao' => 'ASC'
		);

		$this->paginate = array(
			'conditions' => $conditions,
			'fields' => array('id', 'frete_transportadora_id', 'cep_inicio', 'cep_fim', 'peso_inicio', 'peso_fim', 'valor', 'prazo', 'ativo', '(FreteValor.prazo + FreteTransportadora.prazo_carencia) as FreteValor__prazo_carencia'),
			'order' => $order,
			'limit' => Configure::read('Sistema.Paginacao.limit')
		);

		$transportadora = $this->FreteTransportadora->find('first', array(
			'conditions'=> array(
				'FreteTransportadora.ativo' => true,
				'FreteTransportadora.id' => $id
			)
		));

		$this->set('frete_valores', $this->paginate());
		$this->set('transportadora', $transportadora);
		$this->set('tipos', $this->getFreteTipos());

	}

	public function admin_adicionar($id = null) {

		$transportadora = $this->FreteTransportadora->find('first', array(
			'conditions'=> array(
				'FreteTransportadora.ativo' => true,
				'FreteTransportadora.id' => $id
			)
		));

		if ($this->request->is('post')) {

			if ($this->request->data['FreteValor']['valor'] == '0,00') {
				$this->request->data['FreteValor']['valor'] = '';
			}

			$this->request->data['FreteValor']['valor'] = str_replace('.', '', $this->request->data['FreteValor']['valor']);
			$this->request->data['FreteValor']['valor'] = str_replace(',', '.', $this->request->data['FreteValor']['valor']);

			$this->request->data['FreteValor']['frete_transportadora_id'] = $id;

			$cep_inicio = str_replace('-','',$this->request->data['FreteValor']['cep_inicio']);
			$this->request->data['FreteValor']['cep_inicio'] = $cep_inicio;

			$cep_fim = str_replace('-','',$this->request->data['FreteValor']['cep_fim']);
			$this->request->data['FreteValor']['cep_fim'] = $cep_fim;

			$this->FreteValor->create();
			if ($this->FreteValor->save($this->request->data)) {
				$this->Session->setFlash('Valor de Frete salvo com sucesso.', FLASH_SUCCESS);

				if ($this->Configuracao->get('mercado_livre_ativo') && $transportadora['FreteTransportadora']['mercado_livre']) {
					$this->MercadoLivreFrete->atualizar_frete();
				}

				$this->backToPaginatorIndex();
			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar salvar o Valor de Frete. Por favor, tente novamente.', FLASH_ERROR);
			}
		}

		$this->set('transportadora', $transportadora);
		$this->set('tipos', $this->getFreteTipos());

	}

	public function admin_editar($id = null) {
		$this->FreteValor->id = $id;

		$transportadora = $this->FreteValor->find('first', array(
			'contain' => array(
				'FreteTransportadora'
			),
			'conditions'=> array(
				'FreteValor.ativo' => true,
				'FreteValor.id' => $id
			)
		));

		if (!$this->FreteValor->exists()) {
			throw new NotFoundException('Valor inexistente.');
		}

		if ($this->request->is('post') || $this->request->is('put')) {

			$cep_inicio = str_replace('-','',$this->request->data['FreteValor']['cep_inicio']);
			$this->request->data['FreteValor']['cep_inicio'] = $cep_inicio;

			$cep_fim = str_replace('-','',$this->request->data['FreteValor']['cep_fim']);
			$this->request->data['FreteValor']['cep_fim'] = $cep_fim;

			$this->request->data['FreteValor']['valor'] = str_replace('.', '', $this->request->data['FreteValor']['valor']);
			$this->request->data['FreteValor']['valor'] = str_replace(',', '.', $this->request->data['FreteValor']['valor']);

			if ($this->FreteValor->save($this->request->data)) {
				$this->Session->setFlash('Valor de Frete editado com sucesso.', FLASH_SUCCESS);

				if ($this->Configuracao->get('mercado_livre_ativo') && $transportadora['FreteTransportadora']['mercado_livre']) {
					$this->MercadoLivreFrete->atualizar_frete();
				}

				$this->backToPaginatorIndex();
			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar editar o Valor de Frete. Por favor, tente novamente.', FLASH_ERROR);
			}
		} else {
			$this->request->data = $this->FreteValor->read(null, $id);
		}

		$this->set('transportadora', $transportadora);
		$this->set('tipos', $this->getFreteTipos());

	}

	public function admin_excluir($id = null) {
		$this->FreteValor->id = $id;

		if (!$this->FreteValor->exists()) {
			throw new NotFoundException('Valor de Frete inexistente.');
		}

		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->FreteValor->saveField('ativo', false, false)) {
				$this->Session->setFlash('Valor de Frete excluído com sucesso.', FLASH_SUCCESS);
				$this->backToPaginatorIndex();
			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar excluir o Valor de Frete. Por favor, tente novamente.', FLASH_ERROR);
			}
		}
	}

	public function admin_importar($transportadora_id = null) {

		if($this->request->is('post') || $this->request->is('put')){
			$diretorio = APP . 'webroot/files/' . PROJETO . '/csv';

			$arquivoTmp = $this->FileUpload->upload($diretorio, array($this->request->data['FreteValor']['arquivo_csv']), true);

			if ($arquivoTmp !== false) {
				$lines = file ($diretorio.DS.$arquivoTmp);
				$indice = 0;

				$this->FreteValor->deleteAll(array('FreteValor.ativo' => true, 'FreteValor.frete_transportadora_id' => $transportadora_id), false);

				foreach ($lines as $line) {

					$dado = explode(';', $line);

					if($indice > 0) {

						$this->request->data['FreteValor']['frete_transportadora_id'] = $transportadora_id;
						$this->request->data['FreteValor']['cep_inicio'] = $dado[0];
						$this->request->data['FreteValor']['cep_fim'] = $dado[1];
						$this->request->data['FreteValor']['peso_inicio'] = $dado[2];
						$this->request->data['FreteValor']['peso_fim'] = $dado[3];
						$this->request->data['FreteValor']['valor'] = str_replace(',', '.', $dado[4]);
						$this->request->data['FreteValor']['prazo'] = $dado[5];


						if ($this->FreteValor->saveAll($this->request->data)) {
							$this->Session->setFlash('Valores carregados com sucesso.', FLASH_SUCCESS);
						} else {
							$this->Session->setFlash('#1 - Ocorreu um erro ao tentar importar os Valores. Por favor tente novamente. ', FLASH_ERROR);
						}
					}
					$indice++;
				}
				$this->redirect(array('action' => 'index',$transportadora_id));

			} else {
				$this->Session->setFlash('#2 - Ocorreu um erro ao tentar importar os Valores. Por favor tente novamente. '. $this->FileUpload->mensagemErro, FLASH_ERROR);
			}
			unlink($diretorio.DS.$arquivoTmp);
		}

	}

	public function admin_baixar_csv_padrao() {
		$this->layout = 'csv';

		$filename = 'modelo_csv_padrao.csv';

		header('Content-type: application/csv');
		header('Content-Disposition: attachment; filename="'.$filename.'"');

		$csv_file = fopen('php://output', 'w');

		$header_row = array("cep inicio" , "cep fim", "peso inicio", "peso fim", "valor", "prazo");
	    fputcsv($csv_file,$header_row,';','"');

		fclose($csv_file);
	}

	private function getFreteTipos() {
		return array(
			'T' => 'Transportadora',
			'C' => 'Correio'
		);
	}

}
?>