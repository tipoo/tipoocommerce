<?php
App::uses('AppController', 'Controller');

class LojasController extends AppController {

	public $uses = array('Loja', 'Logradouro', 'Cidade', 'Estado');

	public function admin_index() {

			$conditions = array(
				'Loja.ativo' => true
			);

			$this->paginate = array(
				'contain' => array(
					'Cidade.Estado'
				),
				'conditions' => $conditions,
				'order' => array(
					'Loja.descricao' => 'ASC',
				),
				'limit' => 50
			);
			$this->set('lojas_fisicas', $this->paginate());

	}

	public function admin_adicionar($id = null) {

		if ($this->request->is('post')) {
			$this->Loja->create();

			if ($this->Loja->save($this->request->data)) {
				$this->Session->setFlash('Loja salva com sucesso.', FLASH_SUCCESS);
				$this->backToPaginatorIndex();
			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar salvar a loja. Por favor, tente novamente.', FLASH_ERROR);
			}
		}
	}

	public function admin_editar($id = null) {
		$this->Loja->id = $id;

		$loja = $this->Loja->find('first', array(
			'contain' => array(
				'Cidade.Estado'
			),
			'conditions' => array(
				'Loja.ativo' => true,
				'Loja.id' => $id
			)
		));

		if (!$this->Loja->exists()) {
			throw new NotFoundException('Loja inexistente.');
		}
		if ($this->request->is('post') || $this->request->is('put')) {

			if ($this->Loja->save($this->request->data)) {
				$this->Session->setFlash('Loja salva com sucesso.', FLASH_SUCCESS);
				$this->backToPaginatorIndex();
			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar salvar a loja. Por favor, tente novamente.', FLASH_ERROR);
			}
		} else {
			$this->request->data = $this->Loja->read(null, $id);
			$this->set('loja', $loja);
		}
	}

	public function admin_excluir($id = null) {
		$this->Loja->id = $id;

		if (!$this->Loja->exists()) {
			throw new NotFoundException('Loja inexistente.');
		}

		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Loja->saveField('ativo', false, false)) {
				$this->Session->setFlash('Loja excluída com sucesso.', FLASH_SUCCESS);
				$this->backToPaginatorIndex();
			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar excluir a loja. Por favor, tente novamente.', FLASH_ERROR);
			}
		}
	}

	public function admin_ajax_buscar_cep($cep) {

		if ($cep) {

			// O cep pode ser por logradouro ou cidade
			$logradouro = $this->Logradouro->find('first', array(
				'contain' => array(
					'Bairro' => array('nome'),
					'Cidade' => array('nome'),
					'Estado' => array('nome'),
				),
				'conditions' => array('Logradouro.cep' => $cep)
			));

			if ($logradouro) {

				$json = array(
					'sucesso' => true,
					'cidade_id' => $logradouro['Logradouro']['cidade_id'],
					'estado_id' => $logradouro['Logradouro']['estado_id'],
					'cidade' => $logradouro['Cidade']['nome'],
					'estado' => $logradouro['Estado']['nome'],
					'bairro' => $logradouro['Bairro']['nome'],
					'endereco' => $logradouro['Logradouro']['nomeclog']
				);
			} else {
				$cidade = $this->Cidade->find('first', array(
					'contain' => array(
						'Estado' => array('nome')
					),
					'conditions' => array('Cidade.cep' => $cep)
				));

				if ($cidade) {
					$json = array(
						'sucesso' => true,
						'cidade_id' => $cidade['Cidade']['id'],
						'estado_id' => $cidade['Cidade']['estado_id'],
						'cidade' => $cidade['Cidade']['nome'],
						'estado' => $cidade['Estado']['nome'],
						'bairro' => '',
						'endereco' => ''
					);
				} else {
					$json = array('sucesso' => false, 'mensagem' => 'CEP não encontrado.');
				}
			}
		} else {
			$json = array('sucesso' => false, 'mensagem' => 'CEP inválido.');
		}

		$this->renderJson($json);

	}

}
?>