<?php
App::uses('AppController', 'Controller');

class CamposComplementaresValoresController  extends AppController {

	public function admin_index($id = null) {

		$conditions = array(
			'CamposComplementaresValor.ativo' => true,
			'CamposComplementaresValor.campos_complementar_id' => $id
		);

		$order = array(
			'CamposComplementaresValor.descricao' => 'ASC'
		);

		$this->paginate = array(
			'conditions' => $conditions,
			'order' => $order,
			'limit' => Configure::read('Sistema.Paginacao.limit')
		);
		$this->set('campos_complementares_valores', $this->paginate());
		$this->set('campos_complementar_id', $id);

	}

	public function admin_adicionar($id = null) {

		if ($this->request->is('post')) {

			$this->request->data['CamposComplementaresValor']['campos_complementar_id'] = $id;

			$this->CamposComplementaresValor->create();
			if ($this->CamposComplementaresValor->save($this->request->data)) {
				$this->Session->setFlash('Valor salvo com sucesso.', FLASH_SUCCESS);
				$this->backToPaginatorIndex();
			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar salvar o valor. Por favor, tente novamente.', FLASH_ERROR);
			}
		}

	}

	public function admin_editar($id = null) {
		$this->CamposComplementaresValor->id = $id;

		if (!$this->CamposComplementaresValor->exists()) {
			throw new NotFoundException('Valor inexistente.');
		}
		if ($this->request->is('post') || $this->request->is('put')) {

			if ($this->CamposComplementaresValor->save($this->request->data)) {
				$this->Session->setFlash('Valor editado com sucesso.', FLASH_SUCCESS);
				$this->backToPaginatorIndex();
			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar editar o valor. Por favor, tente novamente.', FLASH_ERROR);
			}
		} else {
			$this->request->data = $this->CamposComplementaresValor->read(null, $id);
		}
	}

	public function admin_excluir($id = null) {
		$this->CamposComplementaresValor->id = $id;

		if (!$this->CamposComplementaresValor->exists()) {
			throw new NotFoundException('Valor inexistente.');
		}

		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->CamposComplementaresValor->saveField('ativo', false, false)) {
				$this->Session->setFlash('Valor excluído com sucesso.', FLASH_SUCCESS);
				$this->backToPaginatorIndex();
			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar excluir o valor. Por favor, tente novamente.', FLASH_ERROR);
			}
		}
	}

}
?>