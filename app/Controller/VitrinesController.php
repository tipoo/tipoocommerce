<?php
App::uses('AppController', 'Controller');

class VitrinesController extends AppController {

	public $uses = array('ColecoesLocal', 'HtmlsLocal', 'Categoria', 'Pagina', 'Marca', 'Colecao', 'Produto');
	public $components = array('CustomTagsReader');

	/*
	** Coleções
	*/
	public function admin_colecoes() {

		if (isset($this->params['named']['departamento_id'])) {
			$this->Model = $this->Categoria;
			$model = 'Categoria';
			$breadcrumb = 'Categorias';
			$conditions = array(
				'Categoria.id' => $this->params['named']['departamento_id']
			);
			$custom_tags_ctp = 'Catalogo'.DS.'departamento.ctp';
			$find_model_id = 'categoria_id';
			$vitrinesBackToPaginatorIndex = 'categorias';
		}

		if (isset($this->params['named']['categoria_id'])) {
			$this->Model = $this->Categoria;
			$model = 'Categoria';
			$breadcrumb = 'Categorias';
			$conditions = array(
				'Categoria.id' => $this->params['named']['categoria_id']
			);
			$custom_tags_ctp = 'Catalogo'.DS.'categoria.ctp';
			$find_model_id = 'categoria_id';
			$vitrinesBackToPaginatorIndex = 'categorias';
		}

		if (isset($this->params['named']['pagina_id'])) {
			$this->Model = $this->Pagina;
			$model = 'Pagina';
			$breadcrumb = 'Páginas';
			$conditions = array(
				'Pagina.id' => $this->params['named']['pagina_id']
			);

			if (Configure::read('Sistema.Colecoes.Home.id') == $this->params['named']['pagina_id']) {
				$custom_tags_ctp = 'Catalogo'.DS.'index.ctp';
			} else {
				$pagina = $this->Pagina->find('first', array(
					'contain' => false,
					'conditions'=> array(
						'Pagina.id' => $this->params['named']['pagina_id']
					),
					'fields' => array('id', 'arquivo_ctp')
				));
				$custom_tags_ctp = 'Paginas'.DS.$pagina['Pagina']['arquivo_ctp'].'.ctp';
			}
			$find_model_id = 'pagina_id';
			$vitrinesBackToPaginatorIndex = 'paginas';
		}

		if (isset($this->params['named']['marca_id'])) {
			$this->Model = $this->Marca;
			$model = 'Marca';
			$breadcrumb = 'Marcas';
			$conditions = array(
				'Marca.id' => $this->params['named']['marca_id']
			);
			$custom_tags_ctp = 'Catalogo'.DS.'marca.ctp';
			$find_model_id = 'marca_id';
			$vitrinesBackToPaginatorIndex = 'marcas';
		}

		if (isset($this->params['named']['produto_id'])) {
			$this->Model = $this->Produto;
			$model = 'Produto';
			$breadcrumb = 'Produtos';
			$conditions = array(
				'Produto.id' => $this->params['named']['produto_id']
			);
			$custom_tags_ctp = 'Catalogo'.DS.'produto.ctp';
			$find_model_id = 'produto_id';
			$vitrinesBackToPaginatorIndex = 'produtos';
		}

		$colecoesLocais = $this->Model->find('first', array(
			'contain' => array(
				'ColecoesLocal' => array(
					'Colecao',
					'conditions' => array(
						'ColecoesLocal.ativo' => true
					),
					'order' => array(
						'ColecoesLocal.chave' => 'ASC'
					)
				)
			),
			'conditions' => $conditions
		));

		$tags = $this->CustomTagsReader->readFromFile(APP.'View'.DS.'Themed'.DS.ucfirst(PROJETO).DS.$custom_tags_ctp);
		$chaves = array();
		foreach ($tags as $tag) {
			if ($tag['name'] == 'vitrineProdutosColecao') {
				$chaves[$tag['attributes']->chave] = $tag['attributes']->chave;
				$chaves[$tag['attributes']->chave] = array();
			}
		}
		$chaves = array_reverse($chaves);

		foreach ($colecoesLocais['ColecoesLocal'] as $colecoesLocal) {
			$chaves[$colecoesLocal['chave']][] = $colecoesLocal;
		}

		$this->set('chaves', $chaves);
		$this->set('model', $model);
		$this->set('breadcrumb', $breadcrumb);
		$this->set('find_model_id', $find_model_id);
		$this->set('colecoesLocais', $colecoesLocais);
		$this->set('vitrinesBackToPaginatorIndex', $vitrinesBackToPaginatorIndex);

	}

	public function admin_ajax_adicionar_colecoes($model_id = null, $chave = null) {
		$this->layout = 'ajax';

		if ($this->request->is('post') || $this->request->is('put')) {

			$this->request->data['ColecoesLocal'][$this->params['named']['find_model_id']] = $model_id;
			$this->request->data['ColecoesLocal']['chave'] = $chave;

			if ($this->request->data['ColecoesLocal']['data_fim'] == '') {
				unset($this->request->data['ColecoesLocal']['data_fim']);
			}

			$this->ColecoesLocal->create();
			if ($this->ColecoesLocal->save($this->request->data)) {
				$this->Session->setFlash('Vitrine de Coleção adicionada com sucesso.', FLASH_SUCCESS);
				$this->redirect($this->referer());
			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar adicionar coleção. Por favor, tente novamente.', FLASH_ERROR);
			}
		}
	}

	public function admin_ajax_buscar_colecoes($descricao = null) {
		$colecoes = $this->Colecao->find('all', array(
			'contain' => false,
			'conditions' => array(
				'Colecao.ativo',
				'Colecao.descricao LIKE "%' . $descricao . '%"'
			)
		));

		$colecoesArray = array();
		foreach ($colecoes as $colecao) {
			$colecoesArray[] = array(
				'id' => $colecao['Colecao']['id'],
				'descricao' => $colecao['Colecao']['descricao'],
				'value' => $colecao['Colecao']['descricao'] . ' - #' . $colecao['Colecao']['id']
			);
		}

		$this->renderJson($colecoesArray);
	}

	public function admin_ajax_editar_colecoes($id = null, $colecao_id = null) {
		$this->layout = 'ajax';

		$this->ColecoesLocal->id = $id;

		$colecao = $this->Colecao->find('first', array(
			'contain' => false,
			'conditions' => array(
				'Colecao.id' => $colecao_id
			),
		));

		$this->set('colecao', $colecao);

		if (!$this->ColecoesLocal->exists()) {
			throw new NotFoundException('Coleção inexistente.');
		}
		if ($this->request->is('post') || $this->request->is('put')) {

			if ($this->request->data['ColecoesLocal']['data_fim'] == '') {
				unset($this->request->data['ColecoesLocal']['data_fim']);
			}

			if ($this->ColecoesLocal->save($this->request->data)) {
				$this->Session->setFlash('Vitrine de coleção editada com sucesso.', FLASH_SUCCESS);
				$this->redirect($this->referer());
			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar editar a vitrine de coleção. Por favor, tente novamente.', FLASH_ERROR);
			}
		} else {
			$this->request->data = $this->ColecoesLocal->read(null, $id);
		}
	}

	public function admin_excluir_colecoes($id = null) {
		$this->ColecoesLocal->id = $id;
		if ($this->ColecoesLocal->delete()) {
			$this->Session->setFlash('Vitrine de coleção removida com sucesso.', FLASH_SUCCESS);
			$this->redirect($this->referer());
		} else {
			$this->Session->setFlash('Ocorreu um erro ao tentar remover a vitrine de coleção. Por favor, tente novamente.', FLASH_ERROR);
		}
	}

	/*
	** Html
	*/
	public function admin_htmls() {

		if (isset($this->params['named']['departamento_id'])) {
			$this->Model = $this->Categoria;
			$model = 'Categoria';
			$breadcrumb = 'Categorias';
			$conditions = array(
				'Categoria.id' => $this->params['named']['departamento_id']
			);
			$custom_tags_ctp = 'Catalogo' . DS . 'departamento.ctp';
			$find_model_id = 'categoria_id';
			$htmlsBackToPaginatorIndex = 'categorias';
		}

		if (isset($this->params['named']['categoria_id'])) {
			$this->Model = $this->Categoria;
			$model = 'Categoria';
			$breadcrumb = 'Categorias';
			$conditions = array(
				'Categoria.id' => $this->params['named']['categoria_id']
			);
			$custom_tags_ctp = 'Catalogo' . DS . 'categoria.ctp';
			$find_model_id = 'categoria_id';
			$htmlsBackToPaginatorIndex = 'categorias';
		}

		if (isset($this->params['named']['pagina_id'])) {
			$this->Model = $this->Pagina;
			$model = 'Pagina';
			$breadcrumb = 'Páginas';
			$conditions = array(
				'Pagina.id' => $this->params['named']['pagina_id']
			);

			if (Configure::read('Sistema.Paginas.Home.id') == $this->params['named']['pagina_id']) {
				$custom_tags_ctp = 'Catalogo' . DS . 'index.ctp';
			} else {
				$pagina = $this->Pagina->find('first', array(
					'contain' => false,
					'conditions'=> array(
						'Pagina.id' => $this->params['named']['pagina_id']
					),
					'fields' => array('id', 'arquivo_ctp')
				));
				$custom_tags_ctp = 'Paginas' . DS . $pagina['Pagina']['arquivo_ctp'].'.ctp';
			}
			$find_model_id = 'pagina_id';
			$htmlsBackToPaginatorIndex = 'paginas';
		}

		if (isset($this->params['named']['marca_id'])) {
			$this->Model = $this->Marca;
			$model = 'Marca';
			$breadcrumb = 'Marcas';
			$conditions = array(
				'Marca.id' => $this->params['named']['marca_id']
			);
			$custom_tags_ctp = 'Catalogo' . DS . 'marca.ctp';
			$find_model_id = 'marca_id';
			$htmlsBackToPaginatorIndex = 'marcas';
		}

		if (isset($this->params['named']['produto_id'])) {
			$this->Model = $this->Produto;
			$model = 'Produto';
			$breadcrumb = 'Produtos';
			$conditions = array(
				'Produto.id' => $this->params['named']['produto_id']
			);
			$custom_tags_ctp = 'Catalogo' . DS . 'produto.ctp';
			$find_model_id = 'produto_id';
			$htmlsBackToPaginatorIndex = 'produtos';
		}

		$htmlsLocais = $this->Model->find('first', array(
			'contain' => array(
				'HtmlsLocal' => array(
					'order' => array(
						'HtmlsLocal.chave' => 'ASC'
					)
				)
			),
			'conditions' => $conditions
		));

		$tags = $this->CustomTagsReader->readFromFile(APP . 'View' . DS . 'Themed' . DS . ucfirst(PROJETO) . DS . $custom_tags_ctp);
		$chaves = array();
		foreach ($tags as $tag) {
			if ($tag['name'] == 'conteudoHtml') {
				$chaves[$tag['attributes']->chave] = $tag['attributes']->chave;
				$chaves[$tag['attributes']->chave] = array();
			}
		}
		$chaves = array_reverse($chaves);

		foreach ($htmlsLocais['HtmlsLocal'] as $colecoesLocal) {
			$chaves[$colecoesLocal['chave']][] = $colecoesLocal;
		}

		$this->set('chaves', $chaves);
		$this->set('model', $model);
		$this->set('breadcrumb', $breadcrumb);
		$this->set('find_model_id', $find_model_id);
		$this->set('htmlsLocais', $htmlsLocais);
		$this->set('htmlsBackToPaginatorIndex', $htmlsBackToPaginatorIndex);
	}

	public function admin_adicionar_htlm($model_id = null, $chave = null) {
		if ($this->request->is('post') || $this->request->is('put')) {

			$this->request->data['HtmlsLocal'][$this->params['named']['find_model_id']] = $model_id;
			$this->request->data['HtmlsLocal']['chave'] = $chave;

			if (isset($this->request->data['HtmlsLocal']['id'])) {
				$this->HtmlsLocal->id = $this->request->data['HtmlsLocal']['id'];
			} else {
				$this->HtmlsLocal->create();
			}

			if ($this->HtmlsLocal->save($this->request->data)) {
				$this->Session->setFlash('Conteúdo salvo com sucesso.', FLASH_SUCCESS);
				$this->redirect($this->referer());
			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar salvar o conteúdo. Por favor, tente novamente.', FLASH_ERROR);
			}
		}
	}
}
?>