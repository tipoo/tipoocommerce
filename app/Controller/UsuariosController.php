<?php
App::uses('AppController', 'Controller');

class UsuariosController extends AppController {

	public $uses = array('Usuario', 'Perfil');

	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('admin_login', 'admin_logout', 'admin_esqueci_minha_senha');
	}

	public function admin_index() {

		if ($this->Permissoes->check($this->usuarioAutenticado['perfil_id'], 'permissoes', '@listar_todos_perfis')) {
			$perfis = $this->Perfil->find('list', array(
				'order' => 'descricao'
			));

			$conditions = array(
				'Usuario.ativo' => true
			);
		} else {
			$perfis = $this->Perfil->find('list', array(
				'conditions' => array(
					'Perfil.ativo' => true
				),
				'order' => 'descricao'
			));

			$conditions = array(
				'Usuario.ativo' => true,
				'Perfil.ativo' => true
			);
		}

		if ($this->request->is('post')) {
			if ($this->request->data('Usuario')) {
				$this->Session->write('Filtros.Usuarios.index', $this->request->data('Usuario'));
			}
		} else {
			$this->Session->write('Filtros.Usuarios.index', null);
		}

		if ($this->Session->check('Filtros.Usuarios.index')) {
			if ($this->Session->read('Filtros.Usuarios.index.perfil_id')) {
				$conditions['Usuario.perfil_id'] =  $this->Session->read('Filtros.Usuarios.index.perfil_id');
			}
		}

		$this->request->data['Usuario'] = $this->Session->read('Filtros.Usuarios.index');

		$this->paginate = array(
			'contain' => array(
				'Perfil'
			),
			'conditions' => $conditions,
			'limit' => Configure::read('Sistema.Paginacao.limit')
		);

		$this->set('perfis', $perfis);

		$this->set('usuarios', $this->paginate());
		$this->set('perfis', $perfis);
	}

	public function admin_adicionar() {
		if ($this->request->is('post')) {

			$verificaEmail = $this->Usuario->find('first', array(
				'conditions' => array(
					'Usuario.email' => $this->request->data['Usuario']['email']
				)
			));

			if (count($verificaEmail) == 0) {
				$this->Usuario->create();

				if ($this->Usuario->save($this->request->data)) {
					$this->Session->setFlash('Usuário salvo com sucesso.', FLASH_SUCCESS);
					$this->backToPaginatorIndex();
				} else {
					$this->Session->setFlash('Ocorreu um erro ao tentar salvar o usuário. Por favor, tente novamente.', FLASH_ERROR);
				}
			} else {
				$this->Session->setFlash('Este e-mail já esta cadastrado no sistema', FLASH_ERROR);
			}
		}

		if ($this->Permissoes->check($this->usuarioAutenticado['perfil_id'], 'permissoes', '@listar_todos_perfis')) {
			$perfis = $this->Perfil->find('list', array(
				'order' => 'descricao',
			));
		} else {
			$perfis = $this->Perfil->find('list', array(
				'conditions' => array(
					'Perfil.ativo' => true
				),
				'order' => 'descricao'
			));
		}

		$this->set('perfis', $perfis);
	}

	public function admin_editar($id = null) {

		$this->Usuario->id = $id;

		if (!$this->Usuario->exists()) {
			throw new NotFoundException('Usuário inexistente.');
		}

		if ($this->request->is('post') || $this->request->is('put')) {

			$verificaEmail = $this->Usuario->find('first', array(
			'conditions' => array(
				'Usuario.email' => $this->request->data['Usuario']['email'],
				'Usuario.id  <>' => $id
			)));

			if (count($verificaEmail) == 0) {
				$this->Usuario->create();

				if ($this->Usuario->save($this->request->data)) {
					$this->Session->setFlash('Usuário editado com sucesso.', FLASH_SUCCESS);
					$this->backToPaginatorIndex();
				} else {
					$this->Session->setFlash('Ocorreu um erro ao tentar editar o usuário. Por favor, tente novamente.', FLASH_ERROR);
				}
			} else {
				$this->Session->setFlash('Este e-mail já esta cadastrado no sistema', FLASH_ERROR);
			}
		} else {
			$this->request->data = $this->Usuario->read(null, $id);
		}

		if ($this->Permissoes->check($this->usuarioAutenticado['perfil_id'], 'permissoes', '@listar_todos_perfis')) {
			$perfis = $this->Perfil->find('list', array(
				'order' => 'descricao'
			));
		} else {
			$perfis = $this->Perfil->find('list', array(
				'conditions' => array(
					'Perfil.ativo' => true
				),
				'order' => 'descricao'
			));
		}

		$this->set('perfis', $perfis);
	}

	public function admin_excluir($id = null) {
		$this->Usuario->id = $id;
		if (!$this->Usuario->exists()) {
			throw new NotFoundException('Usuário inexistente.');
		}

		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Usuario->saveField('ativo', false, false)) {
				$this->Session->setFlash('Usuário excluído com sucesso.', FLASH_SUCCESS);
				$this->backToPaginatorIndex();
			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar salvar o usuário. Por favor, tente novamente.', FLASH_ERROR);
			}
		}
	}


	public function admin_login() {

		$this->layout = 'admin_login';

		if ($this->request->is('post')) {

			if ($this->Auth->login()) {
				$this->usuarioAutenticado = $this->Auth->user();

				// Limpando as permissões do perfil do usuário caso elas já existam de um login anterior para que elas sejam atualizadas
				if ($this->Session->check('Permissoes.'.$this->usuarioAutenticado['perfil_id'])) {
					$this->Session->delete('Permissoes.'.$this->usuarioAutenticado['perfil_id']);
				}

				return $this->redirect($this->Auth->redirectUrl());
			} else {
				$this->Session->setFlash('E-mail e/ou senha inválidos.', FLASH_ERROR);
			}
		}

		if ($this->usuarioAutenticado) {
			$this->redirect(array('controller' => 'paginas', 'action' => 'admin_dashboard'));
		}
	}

	public function admin_logout() {
		$this->redirect($this->Auth->logout());
	}

	public function admin_alterar_senha() {

		$usuario = $this->Usuario->find('first', array(
			'conditions' => array(
				'Usuario.id' => $this->usuarioAutenticado['id']
			)
		));

		if ($this->request->is('post') || $this->request->is('put')) {
			$this->Usuario->id = $this->usuarioAutenticado['id'];

			$senhaValida = true;

			if (isset($this->request->data['Usuario']['senha_atual'])) {
				$senhaAtual = AuthComponent::password($this->request->data['Usuario']['senha_atual']);

				if ($usuario['Usuario']['senha'] == $senhaAtual) {
					$senhaValida = true;
				} else {
					$senhaValida = false;
				}
			}

			if ($senhaValida) {

            	if ($this->Usuario->save($this->request->data)) {
					$this->Session->setFlash('Senha alterada com sucesso.', FLASH_SUCCESS);
					 $this->redirect(array('controller' => 'paginas', 'action' => 'dashboard'));
				} else {
					$this->Session->setFlash('Ocorreu um erro ao tentar alterar sua senha. Por favor, tente novamente.', FLASH_ERROR);
				}

            } else {
            	$this->Session->setFlash('Senha atual incorreta.', FLASH_ERROR);
            }
		}
	}

	public function admin_esqueci_minha_senha() {

		$this->layout = 'admin_login';

		if ($this->request->is('post') || $this->request->is('put')) {

			$usuario = $this->Usuario->find('first', array(
				'conditions' => array(
					'email' => $this->request->data('EsqueciMinhaSenha.email')
				)
			));

			if (!$usuario) {
				$this->Session->setFlash('Não foi encontrado nenhum usuário com o e-mail ' . $this->request->data('EsqueciMinhaSenha.email').'.', FLASH_ERROR);
				$this->redirect($this->referer());
			} else {
				$usuario['Usuario']['senha'] = $this->Random->gerarString();

				if ($this->Usuario->save($usuario)) {
					$email = new CakeEmail('adminEsqueciMinhaSenha');
					$email->to($usuario['Usuario']['email']);
					$email->viewVars(array('usuario' => $usuario));
					$email->send();
					$this->Session->setFlash('Nova senha enviada com sucesso para o e-mail ' . $usuario['Usuario']['email'].'.', FLASH_SUCCESS);
					$this->redirect('/admin');
				} else {
					$this->Session->setFlash('Ocorreu um erro ao tentar enviar uma nova senha. Por favor, tente novamente.');
				}
			}
		}
	}

	public function sistema_login() {
		if ($this->Auth->login()) {
			$this->usuarioAutenticado = $this->Auth->user();

			// Limpando as permissões do perfil do usuário caso elas já existam de um login anterior para que elas sejam atualizadas
			if ($this->Session->check('Permissoes.'.$this->usuarioAutenticado['perfil_id'])) {
				$this->Session->delete('Permissoes.'.$this->usuarioAutenticado['perfil_id']);
			}

			return $this->redirect($this->Auth->redirect());
		} else {
			$this->Session->setFlash('E-mail e/ou senha inválidos.', FLASH_ERROR);
		}
	}

	public function sistema_logout() {
		$this->redirect($this->Auth->logout());
	}

	public function api_login() {
		$this->layout = 'clean';
		if ($this->Auth->login()) {
			$this->usuarioAutenticado = $this->Auth->user();

			// Limpando as permissões do perfil do usuário caso elas já existam de um login anterior para que elas sejam atualizadas
			if ($this->Session->check('Permissoes.'.$this->usuarioAutenticado['perfil_id'])) {
				$this->Session->delete('Permissoes.'.$this->usuarioAutenticado['perfil_id']);
			}

			return $this->redirect($this->Auth->redirect());
		} else {
			$this->Session->setFlash('E-mail e/ou senha inválidos.', FLASH_ERROR);
		}
	}

	public function api_logout() {
		$this->layout = 'clean';
		$this->redirect($this->Auth->logout());
	}

}
?>