<?php
App::uses('AppController', 'Controller');

class ItemMenusController extends AppController {

	public function admin_index($menu_id = null, $id = null) {

		$item_menu = $this->ItemMenu->find('count', array(
			'conditions' => array(
				'ItemMenu.id' => $id,
			),

		));

		if ($id && $item_menu == 0) {
			$this->Session->setFlash('Não é possível cadastrar sub-menu para este id.', FLASH_ERROR);
			$this->redirect(array('controller' => 'itemMenus'));
		}


		$conditions = array(
			'ItemMenu.item_menu_pai_id' => $id,
			'ItemMenu.menu_id' => $menu_id
		);

		$this->paginate = array(
			'conditions' => $conditions,
			'limit' => 50,
			'order' => array(
				'ItemMenu.ordem' => 'ASC'
			)
		);

		if ($id) {
			$this->set('voltar', true);
		} else {
			$this->set('voltar', false);
		}

		$this->set('id', $id);
		$this->set('menu_id', $menu_id);
		$this->set('item_menus', $this->paginate());
	}

	public function admin_adicionar($menu_id = null, $id = null) {

		$qtd_item_menu = $this->ItemMenu->find('count', array(
			'conditions' => array(
				'ItemMenu.menu_id' => $menu_id,
				'ItemMenu.item_menu_pai_id' => $id
			)
		));

		if ($this->request->is('post')) {

			$this->request->data['ItemMenu']['menu_id'] = $menu_id;

			$this->request->data['ItemMenu']['ordem'] = $qtd_item_menu + 1;

			if ($id) {
				$this->request->data['ItemMenu']['item_menu_pai_id'] = $id;
			}

			$this->ItemMenu->create();

			if ($this->ItemMenu->save($this->request->data)) {
				$this->Session->setFlash('Item de menu salvo com sucesso.', FLASH_SUCCESS);
				$this->backToPaginatorIndex();

			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar salvar o item de menu. Por favor, tente novamente.', FLASH_ERROR);
			}
		}

	}

	public function admin_editar($id = null) {

		$this->ItemMenu->id = $id;

		if (!$this->ItemMenu->exists()) {
			throw new NotFoundException('Item de menu inexistente.');
		}

		if ($this->request->is('post') || $this->request->is('put')) {

			if ($this->ItemMenu->save($this->request->data)) {
				$this->Session->setFlash('Item de menu salvo com sucesso.', FLASH_SUCCESS);
				$this->backToPaginatorIndex();
			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar salvar o item de menu. Por favor, tente novamente.', FLASH_ERROR);
			}


		} else {
			$this->request->data = $this->ItemMenu->read(null, $id);

		}

	}

	public function admin_excluir($id = null) {

		$this->ItemMenu->id = $id;

		if (!$this->ItemMenu->exists()) {
			throw new NotFoundException('Item de menu  inexistente.');
		}

		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->ItemMenu->saveField('ativo', false, false)) {
				$this->Session->setFlash('Item de menu desativado com sucesso.', FLASH_SUCCESS);
				$this->redirect($this->referer());

			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar desativar o item de menu . Por favor, tente novamente.', FLASH_ERROR);
			}
		}
	}

	public function admin_ativar($id = null) {
		$this->ItemMenu->id = $id;

		if (!$this->ItemMenu->exists()) {
			throw new NotFoundException('Item de menu inexistente.');
		}

		if ($this->request->is('post')) {

			if ($this->ItemMenu->saveField('ativo', true, false)) {
				
				$this->Session->setFlash('Item de menu ativado com sucesso.', FLASH_SUCCESS);
				$this->redirect($this->referer());
			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar ativar o item de menu . Por favor, tente novamente.', FLASH_ERROR);
			}
		}

	}

	public function admin_ajax_ordenar($menu_id = null, $item_pai_id = null) {

		$conditions = array(
			'ItemMenu.menu_id' => $menu_id
		);

		if ($item_pai_id == '') {
			$conditions['ItemMenu.item_menu_pai_id'] = '';
		} else {
			$conditions['ItemMenu.item_menu_pai_id'] = $item_pai_id;
		}

		$qtd_itens = $this->ItemMenu->find('count', array(
			'conditions' => $conditions
		));

		$count_menu = 1;
		while ($count_menu <= $qtd_itens) {

			$this->request->data[$count_menu]['ItemMenu']['id'] = $this->params['named']['ordem_' . $count_menu];
			$this->request->data[$count_menu]['ItemMenu']['ordem'] = $count_menu;

			$count_menu++;
		}

		if ($this->ItemMenu->saveMany($this->request->data, array('deep' => true))) {
			$json = array('sucesso' => true);
		} else {
			$json = array('sucesso' => false, 'mensagem' => 'Ocorreu um erro ao tentar ordenar os itens de menu. Por favor, tente novamente.');
		}

		$this->renderJson($json);

	}
}
?>