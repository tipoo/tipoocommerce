<?php
App::uses('ApiComponent', 'Controller/Component');

class ApiPedidosComponent extends ApiComponent {
	public function index() {
		$pedidos = $this->Controller->Pedido->find('all');
		$this->normalize_and_set($pedidos, 'Pedido');
	}

	public function view($id) {
		$pedido = $this->Controller->Pedido->find('first');
		$this->normalize_and_set($pedido, 'Pedido');
	}
}
?>