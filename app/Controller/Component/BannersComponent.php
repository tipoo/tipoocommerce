<?php
App::uses('Component', 'Controller');
class BannersComponent extends Component {

	private $BannerTipo;
	private $Controller;

	public function initialize(Controller $controller) {
		parent::initialize($controller);

		$this->Controller = $controller;

		App::uses('BannerTipo', 'Model');
		$this->BannerTipo = new BannerTipo();
	}

	public function setTipos($tipo = null, $opts = array()) {

		$padrao = array(
			'categoria_id' => null
		);

		$opts = array_merge($padrao, $opts);

		$controleBanners = $this->BannerTipo->find('all', array(
			'contain' => array(
				'Banner' => array(
					'conditions' => array(
						'Banner.ativo' => true,
						'Banner.data_inicio <=' => date('Y-m-d H:i:s'),
						'OR' => array(
							'Banner.data_fim >=' => date('Y-m-d H:i:s'),
							'Banner.data_fim' => ''
						)
					),
					'order' => array(
						'Banner.ordem' => 'ASC'
					)
				)
			),
			'conditions' => array(
				'BannerTipo.ativo' => true,
				'BannerTipo.tipo' => $tipo,
				'BannerTipo.categoria_id' => $opts['categoria_id']
			)
		));

		if ($tipo == 'generico') {
			$controleBannersGenerico = $controleBanners;
			$this->Controller->set(compact('controleBannersGenerico'));
		} else {
			$this->Controller->set(compact('controleBanners'));
		}
	}

}
?>