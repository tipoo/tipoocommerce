<?php
App::uses('Configuracao', 'Model');
App::uses('Pedido', 'Model');
App::uses('PedidoSku', 'Model');
App::uses('Sku', 'Model');
App::uses('Component', 'Controller');

class RegrasStatusComponent extends Component {

	public $Controller;
	public $Configuracao;
	public $Pedido;
	public $PedidoSku;
	public $Sku;

	public function initialize(Controller $controller) {
		parent::initialize($controller);

		$this->Controller = $controller;

		$this->Configuracao = new Configuracao();
		$this->Pedido = new Pedido();
		$this->PedidoSku = new PedidoSku();
		$this->Sku = new Sku();
	}

	// Este método deve ser chamado após o status do pedido ser alterado
	public function status_alterado($pedido_id, $viewVars = array()) {

		$pedido = $this->Pedido->get($pedido_id);

		// Verificando se o status requer movimentacao de estoque
		if ($pedido['StatusPedido']['confirma_sku_pedido']) {
			$this->PedidoSku->updateAll(
				array('PedidoSku.situacao_estoque' => '"Pedido"'),
				array(
					'PedidoSku.pedido_id' => $pedido_id,
					'PedidoSku.situacao_estoque' => 'Reserva'
				)
			);

			// Verificando se ainda tem sku em pré-venda. Se não tiver, altera a flag estoque disponível do pedido
			$tem_prevenda = $this->PedidoSku->find('count', array(
				'contain' => false,
				'conditions' => array(
					'PedidoSku.pedido_id' => $pedido_id,
					'PedidoSku.situacao_estoque' => 'PreVenda'
				)
			));

			if (!$tem_prevenda) {
				$this->Pedido->id = $pedido_id;
				$this->Pedido->saveField('estoque_disponivel', true);
			}

		} else if ($pedido['StatusPedido']['retorna_estoque']) {
			// Devolvendo os skus em reserva ou no pedido para o estoque
			$pedido_skus = $this->PedidoSku->find('all', array(
				'contain' => array('Sku'),
				'conditions' => array(
					'PedidoSku.pedido_id' => $pedido_id,
					'OR' => array(
						'PedidoSku.situacao_estoque' => array('Pedido', 'Reserva')
					)
				)
			));

			foreach ($pedido_skus as $pedido_sku) {
				$nova_qtd = $pedido_sku['PedidoSku']['qtd'] + $pedido_sku['Sku']['qtd_estoque'];
				$this->Sku->id = $pedido_sku['PedidoSku']['sku_id'];
				$this->Sku->saveField('qtd_estoque', $nova_qtd);
			}

			$this->PedidoSku->updateAll(
				array('PedidoSku.situacao_estoque' => '"Retorno"'), //'PreVenda','Reserva','Pedido','Retorno'
				array(
					'PedidoSku.pedido_id' => $pedido_id,
					'OR' => array(
						'PedidoSku.situacao_estoque' => array('Pedido', 'Reserva', 'PreVenda')
					)
				)
			);

			// Verifica se é o status foi alterado pela plataforma e atualiza o estoque no Mercado Livre
			if ($this->Configuracao->get('mercado_livre_ativo') && !$pedido['StatusPedido']['status_mercado_livre_frete_cancelled']) {
				foreach ($pedido['PedidoSku'] as $sku) {
					$this->Controller->MercadoLivreProdutos->editar_produto($sku['Sku']['Produto']['id']);
				}
			}
		}

		// Verificando se o pedido vai passar pelo anti-fraude
		if ($this->Configuracao->get('clear_sale_ativo') && $pedido['StatusPedido']['status_clear_sale_start']) {
			$this->Controller->ClearSale->start($pedido_id);
		}

		if ($this->Configuracao->get('mercado_livre_ativo')) {
			// Verifica se é o status inicial da plataforma e atualiza o estoque no Mercado Livre
			if ($pedido['StatusPedido']['status_inicial'] && !$pedido['StatusPedido']['status_mercado_livre_confirmed']) {
				foreach ($pedido['PedidoSku'] as $sku) {
					$this->Controller->MercadoLivreProdutos->editar_produto($sku['Sku']['Produto']['id']);
				}
			}

			// (Marketplace - Mercado Livre) Verificando se o pedido vai alterar o status de frete do mercado livre
			$this->Controller->MercadoLivreStatus->alterar_meli_status($pedido);
		}

		// Fazendo as notificações do pedido
		$viewVars['pedido'] = $pedido;

		// Notificando o cliente da alteração de status caso necessário
		if ($pedido['StatusPedido']['email_notifica_cliente']) {
			$email = new CakeEmail('notificacao_status_alterado');
			$email->theme(ucfirst(PROJETO));
			$email->subject($pedido['StatusPedido']['email_assunto']);
			$email->template($pedido['StatusPedido']['email_template'], $pedido['StatusPedido']['email_layout']);
			$email->to($pedido['Cliente']['email']);
			$email->viewVars($viewVars);
			$email->send();
		}

		// Notificando o loja da alteração de status caso necessário
		if ($pedido['StatusPedido']['email_notifica_loja']) {
			$email = new CakeEmail('notificacao_status_alterado');
			$email->theme(ucfirst(PROJETO));
			$email->subject($pedido['StatusPedido']['email_assunto'] . ' - Cópia da notificação');
			$email->template($pedido['StatusPedido']['email_template'], $pedido['StatusPedido']['email_layout']);
			$email->to(Configure::read('Sistema.StatusPedidos.notificacaoStatus'));
			$email->viewVars($viewVars);
			$email->send();
		}

		return $pedido;
	}
}
?>