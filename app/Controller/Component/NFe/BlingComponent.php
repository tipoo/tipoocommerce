<?php
App::uses('Component', 'Controller');
App::uses('Xml', 'Utility');
App::uses('HttpSocket', 'Network/Http');

class BlingComponent extends Component {

	private $Pedido;
	private $Configuracao;
	private $Controller;

	private $nfe = array('pedido' => array());

	private $bling_base_url = 'https://bling.com.br/Api/v2/';

	public function initialize(Controller $controller) {
		parent::initialize($controller);

		$this->Controller = $controller;

		App::uses('Pedido', 'Model');
		$this->Pedido = new Pedido();

		App::uses('Configuracao', 'Model');
		$this->Configuracao = new Configuracao();
	}

	public function gerar_nf($pedido_id) {
		$pedido = $this->Pedido->get($pedido_id);

		// Gera a NF
		$this->set_cliente($pedido['Cliente'], $pedido['EnderecoEntrega']);
		$this->set_dados_etiqueta($pedido['Cliente'], $pedido['EnderecoEntrega']);

		foreach ($pedido['PedidoSku'] as $pedido_sku) {
			$this->add_item($pedido_sku);
		}

		$xml_nfe = Xml::fromArray($this->nfe);
		$xml = $xml_nfe->asXML();

		// Salvando o xml enviado para o bling para a criação da notafiscal
		$this->Pedido->id = $pedido['Pedido']['id'];
		$this->Pedido->saveField('bling_xml', $xml);

		// Criando a nota fiscal dentro do bling e salvando o retorno
		$retorno = $this->http_post_nfe($xml);
		$this->Pedido->saveField('bling_nfe', $retorno);

		$retorno = json_decode($retorno, true);
		$verificar_erros = $this->verificar_erros($retorno);

		$pedido_retorno['Pedido'] = array();
		if (!$verificar_erros['erro']) {
			// Envia para o cliente e gera a DANFE com link para impressão
			$retorno = $this->http_post_nfe_send($retorno['retorno']['notasfiscais'][0]['notaFiscal']['numero']);
			$this->Pedido->saveField('bling_danfe', $retorno);

			$retorno = json_decode($retorno, true);
			$verificar_erros = $this->verificar_erros($retorno);

			if ($verificar_erros['erro']) {
				$pedido_retorno['error']['mensagem'] = $verificar_erros['mensagem'];
			}

		} else {
			$pedido_retorno['error']['mensagem'] = $verificar_erros['mensagem'];
		}

		return $pedido_retorno;
	}

	private function set_cliente($cliente, $endereco) {
		$cliente = array(
			'nome' => $cliente['nome'],
			'tipoPessoa' => $cliente['tipo_pessoa'],
			'cpf_cnpj' => $this->remover_mascara($cliente['tipo_pessoa'] == 'F' ? $cliente['cpf'] : $cliente['cnpj']),
			'ie_rg' => $this->remover_mascara($cliente['tipo_pessoa'] == 'F' ? '' : $cliente['inscricao_estadual']),
			'endereco' => $endereco['endereco'],
			'numero' => $endereco['numero'],
			'complemento' => $endereco['complemento'],
			'bairro' => $endereco['bairro'],
			'cep' => $this->converter_cep($endereco['cep']),
			'cidade' => $endereco['Cidade']['nome'],
			'uf' => $endereco['Cidade']['uf'],
			'fone' => $cliente['tipo_pessoa'] == 'F' ? $cliente['tel_residencial'] : $cliente['tel_comercial_pj'],
			'email' => $cliente['email']
		);

		$this->nfe['pedido']['cliente'] = $cliente;
	}

	private function set_dados_etiqueta($cliente, $endereco) {
		$dados_etiqueta = array(
			'nome' => $cliente['nome'],
			'endereco' => $endereco['endereco'],
			'numero' => $endereco['numero'],
			'complemento' => $endereco['complemento'],
			'municipio' => $endereco['Cidade']['nome'],
			'uf' => $endereco['Cidade']['uf'],
			'cep' => $this->converter_cep($endereco['cep']),
			'bairro' => $endereco['bairro']
		);

		$this->nfe['pedido']['dados_etiqueta'] = $dados_etiqueta;
	}

	private function add_item($pedido_sku) {
		if (!isset($this->nfe['pedido']['itens'])) {
			$this->nfe['pedido']['itens'] = array();
		}

		if (!isset($this->nfe['pedido']['itens']['item'])) {
			$this->nfe['pedido']['itens']['item'] = array();
		}

		$item = array(
			'codigo' => $pedido_sku['sku'],
			'descricao' => $pedido_sku['descricao_produto'] . ' ' . $pedido_sku['descricao_sku'],
			'un' => 'un',
			'qtde' => $pedido_sku['qtd'],
			'vlr_unit' => $pedido_sku['preco_unitario_desconto'] ? $pedido_sku['preco_unitario_desconto'] : $pedido_sku['preco_unitario'],
			'tipo' => 'P',
			'class_fiscal' => $pedido_sku['ncm'],
			'origem' => $pedido_sku['origem']
		);

		$this->nfe['pedido']['itens']['item'][] = $item;
	}

	private function http_post_nfe($xml) {
		$url = $this->bling_base_url . 'notafiscal/json/';
		$data = array (
			'apikey' => $this->Configuracao->get('bling_api_key'),
			'xml' => rawurlencode($xml)
		);

		$curl_handle = curl_init();
		curl_setopt($curl_handle, CURLOPT_URL, $url);
		curl_setopt($curl_handle, CURLOPT_POST, count($data));
		curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $data);
		curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, FALSE);

		$response = curl_exec($curl_handle);

		curl_close($curl_handle);

		return $response;
	}

	private function http_post_nfe_send($numero_nfe) {
		$url = $this->bling_base_url . 'notafiscal/json/';
		$data = array (
			'apikey' => $this->Configuracao->get('bling_api_key'),
			'number' => $numero_nfe,
			'serie' => '1',
			'sendEmail' => true
		);

		$curl_handle = curl_init();
		curl_setopt($curl_handle, CURLOPT_URL, $url);
		curl_setopt($curl_handle, CURLOPT_POST, count($data));
		curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $data);
		curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, FALSE);

		$response = curl_exec($curl_handle);

		curl_close($curl_handle);

		return $response;
	}

	private function remover_mascara($valor) {
		$valor = str_replace('-', '', $valor);
		$valor = str_replace('.', '', $valor);

		return $valor;
	}

	private function converter_cep($cep) {
		$cep = substr($cep , 0, 2) . '.' . substr($cep , 2, 7);

		return $cep;
	}

	private function verificar_erros($retorno) {
		$erro = array(
			'erro' => false,
			'mensagem' => ''
		);

		if (isset($retorno['retorno']['erros'])) {
			$erro_msg = '';

			foreach ($retorno['retorno']['erros'] as $erro) {
				if (isset($erro['notafiscal']['erro'])) {
					$erro_msg .= '(' . $erro['notafiscal']['erro'] . ') ' . $erro['notafiscal']['mensagem'];
				} else {
					$erro_msg .= '(' . $erro['erro']['cod'] . ') ' . $erro['erro']['msg'];
				}

			}

			$erro = array(
				'erro' => true,
				'mensagem' => $erro_msg
			);
		}

		return $erro;
	}
}
?>