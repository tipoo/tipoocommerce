<?php
class RegrasPromocoesComponent extends Component {

	protected $Controller;

	public function initialize(Controller $Controller) {
		parent::initialize($Controller);
		$this->Controller = $Controller;

	}

	public function recalcular_valores_produto($produto, $promocao = null) {
		if ($promocao) {
			if (empty($promocao['promocao']['PromocaoCausa']['qtd_pecas']) || $promocao['promocao']['PromocaoCausa']['qtd_pecas'] == 1) {
				$produto['preco_de'] = $produto['preco_por'];
				$produto['preco_por'] = $promocao['valor'];
				$produto['economize'] = $produto['preco_de'] - $produto['preco_por'];
			}
		}

		return $produto;
	}

	public function recalcular_valores_sku($sku, $promocao = null) {
		if ($promocao) {
			$sku['Produto']['preco_de'] = $sku['Produto']['preco_por'];
			$sku['preco_unitario_desconto'] = $promocao['valor'];
		} else {
			$sku['Produto']['preco_de'] = $sku['Produto']['preco_de_original'];
			unset($sku['preco_unitario_desconto']);
		}

		return $sku;
	}

	protected function calcular_valor_total_monetario_com_desconto($valor, $desconto, $tipo_valor = 'M') {
		if ($tipo_valor == 'M') {
			return $valor - $desconto;
		} else {
			return $valor - round(($valor * $desconto / 100), 2);
		}
	}

	protected function calcular_valor_monetario_desconto($valor, $desconto, $tipo_valor = 'M') {
		if ($tipo_valor == 'M') {
			return $desconto;
		} else {
			return round(($valor * $desconto / 100), 2);
		}
	}

}
?>