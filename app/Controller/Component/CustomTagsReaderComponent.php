<?php
App::uses('Component', 'Controller');
class CustomTagsReaderComponent extends Component {

	public $ct;

	public function __construct() {
		require_once APP.'Vendor'.DS.'CustomTags'.DS.'CustomTags.php';

		$this->ct = new CustomTags(
			array(
				'parse_on_shutdown' => false,
				'tag_directory' => APP.'Vendor'.DS.'CustomTags'.DS.'tags'.DS,
				'sniff_for_buried_tags' => true,
				'tag_name' => 'tipoo',
				'tag_callback_prefix' => 'tipoo_',
				//'view_context' => $view
			)
		);
	}

	public function readFromFile($path) {
		$file = new File($path);
		$source = $file->read();

		return $this->readFromString($source);
	}

	public function readFromString($source) {
		return $this->ct->collectTags($source);
	}

}
?>