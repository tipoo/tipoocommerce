<?php
class ApiComponent extends Component {

	protected $Controller;

	public function initialize(Controller $Controller) {
		parent::initialize($Controller);
		$this->Controller = $Controller;
	}

	public function index() {
		$this->set('Unimplemented method index', 'Message', 405);
	}

	public function add() {
		$this->set('Unimplemented method add', 'Message', 405);
	}

	public function view($id) {
		$this->set('Unimplemented method view', 'Message', 405);
	}

	public function edit($id) {
		$this->set('Unimplemented method edit', 'Message', 405);
	}

	public function delete($id) {
		$this->set('Unimplemented method delete', 'Message', 405);
	}

	protected function normalize_and_set($data, $model_name = null, $statusCode = 200) {
		if ($this->Controller->params['action'] == 'api_index') {
			foreach ($data as $key => $value) {
				$data[$key] = array_merge($data[$key][$model_name], $data[$key]);
				unset($data[$key][$model_name]);
			}

		} else if ($this->Controller->params['action'] == 'api_view') {
			$data = array_merge($data[$model_name], $data);
			unset($data[$model_name]);
		}

		$this->set($data, $model_name, $statusCode);
	}

	protected function set($data, $model_name, $statusCode = 200) {
		debug($model_name);


		$this->Controller->set(array(
			$model_name => $data,
			'_model_name' => $model_name,
			'_serialize' => $model_name,
			'_rootNode' => Inflector::pluralize($model_name)
		));

		$this->Controller->response->statusCode($statusCode);

		$this->render();
	}

	public function render() {
		$this->Controller->layout = 'clean';
		$this->Controller->render('/Api/json/api_generic');
	}
}
?>