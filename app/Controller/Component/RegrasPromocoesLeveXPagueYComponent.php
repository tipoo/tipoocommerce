<?php
App::uses('RegrasPromocoesComponent', 'Controller/Component');

class RegrasPromocoesLeveXPagueYComponent extends RegrasPromocoesComponent {

	public function definir_promocoes_skus($skus) {
		$produtos_skus = $this->agrupar_produtos($skus);

		foreach ($produtos_skus as $produto_id => $produto_skus_ids) {
			$tem_promocao = false;
			$qtd_comprada = 0;
			$preco_unitario_original = 0;
			$promocoes = array();

			// Verificando se existe promoção para o produto atual
			foreach ($produto_skus_ids as $sku_id) {
				$sku = $skus[$sku_id];
				if (isset($sku['Promocao']['leve_x_pague_y']) && count($sku['Promocao']['leve_x_pague_y'])) {
					$tem_promocao = true;
					$qtd_comprada += $sku['qtd'];
					$promocoes = $sku['Promocao']['leve_x_pague_y'];
				}
			}

			// Caso a tenha promoção leve x, pague y, define qual a melhor. Caso não tenha, volta o valor original do sku.
			if ($tem_promocao) {
				// Identificando a melhor.
				$melhor_promocao = $this->definir_melhor_promocao($promocoes, $qtd_comprada);

				if ($melhor_promocao) {
					$preco_unitario = (isset($sku['preco_unitario_desconto']) && $sku['preco_unitario_desconto'] > 0) ? $sku['preco_unitario_desconto'] : $sku['preco_unitario'];
					$total_com_desconto = ($qtd_comprada - $melhor_promocao['qtd_gratis']) * $preco_unitario;
					$preco_unitario_desconto = $total_com_desconto / $qtd_comprada;

					foreach ($produto_skus_ids as $sku_id) {
						$skus[$sku_id]['preco_unitario_desconto'] = $preco_unitario_desconto;
						$skus[$sku_id]['Promocao']['melhor_leve_x_pague_y']['valor'] = $preco_unitario_desconto;
						$skus[$sku_id]['Promocao']['melhor_leve_x_pague_y']['promocao'] = $melhor_promocao;
					}
				}
			} else {
				foreach ($produto_skus_ids as $sku_id) {
					if (!isset($skus[$sku_id]['Promocao']['melhor_desconto_produto']) && !isset($skus[$sku_id]['Promocao']['melhor_compre_junto'])) {
						unset($skus[$sku_id]['preco_unitario_desconto']);
					}

					unset($skus[$sku_id]['Promocao']['melhor_leve_x_pague_y']);
				}
			}

		}

		return $this->ordenar_skus_produtos($skus);
	}

	private function definir_melhor_promocao($promocoes, $qtd_comprada) {
		// Verificando se a promoção é única, se não for, identifica a melhor
		if (count($promocoes) == 1) {
			return $this->vezes_aplicadas($promocoes[0], $qtd_comprada);

		} else if (count($promocoes) > 1) {
			$melhor_promocao = null;

			foreach ($promocoes as $key => $promocao) {
				$promocao = $this->vezes_aplicadas($promocao, $qtd_comprada);

				if (!$melhor_promocao) {
					$melhor_promocao = $promocao;
				} else if ($promocao['qtd_gratis'] >= $melhor_promocao['qtd_gratis']) {
					$melhor_promocao = $promocao;
				}
			}

			return $melhor_promocao;

		} else {
			return null;
		}
	}

	private function vezes_aplicadas($promocao, $qtd_comprada) {
		$leve_x = $promocao['PromocaoCausa']['qtd_pecas'];
		$pague_y = $promocao['PromocaoEfeito']['valor'];

		$promocao['vezes_aplicadas'] = floor($qtd_comprada / $leve_x); // Quantidade de vezes que uma promoção leve x, pague y pode ser aplicada a um produto.
		$promocao['qtd_gratis'] = ($promocao['vezes_aplicadas'] * $leve_x) - ($promocao['vezes_aplicadas'] * $pague_y);

		return $promocao;
	}

	private function agrupar_produtos($skus) {
		$produtos_skus = array();

		// Juntar os skus do mesmo produto
		foreach ($skus as $id => $sku) {
			if (!array_key_exists($sku['Produto']['id'], $produtos_skus)) {
				$produtos_skus[$sku['Produto']['id']] = array();
			}
			$produtos_skus[$sku['Produto']['id']][] = $id;
		}

		return $produtos_skus;
	}

	private function ordenar_skus_produtos($skus) {
		uasort($skus, function($a, $b) {
			return strcasecmp($a['Produto']['descricao'], $b['Produto']['descricao']);
		});

		return $skus;
	}



}
?>