<?php
App::uses('Component', 'Controller');
class LojasComponent extends Component {

	private $Loja;
	private $Controller;

	public function initialize(Controller $controller) {
		parent::initialize($controller);

		$this->Controller = $controller;

		App::uses('Loja', 'Model');
		$this->Loja = new Loja();
	}

	public function setLojas() {

		$lojas = $this->Loja->find('all', array(
			'contain' => array(
				'Cidade.Estado'
			),
			'conditions' => array(
				'Loja.ativo' => true
			),
			'order' => array(
				'Cidade.estado_id' => 'ASC',
				'Cidade.nome' => 'ASC',
			)
		));

		// $lojasPorEstado = array();
		// foreach ($lojas as $loja) {
		// 	$lojasPorEstado[$loja['Cidade']['estado_id']][] = $loja;
		// }

		$this->Controller->set(compact('lojas'));

	}

}
?>