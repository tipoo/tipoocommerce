<?php
App::uses('Component', 'Controller');
App::uses('CustomTagsReader', 'Controller/Component');
class CustomTagsParserComponent extends Component {

	public $settings;
	public $viewRender = null;

	public function __construct(ComponentCollection $collection, $settings = array('actions' => array())) {
		$this->settings = $settings;
	}

	public function initialize(Controller $controller) {
		if (in_array($controller->request->params['action'], $this->settings['actions'])) {
			$controller->autoRender = false;
		}
	}

	public function shutdown(Controller $controller) {
		if (in_array($controller->request->params['action'], $this->settings['actions'])) {
			$view = new View($controller, false);
			$view_content = new View($controller, false);
			$view_render = $this->viewRender ? $this->viewRender : $controller->request->params['action'];

			// Verificando se o view render especificado pertence ao controller passado na url ou se é de outro controller. 
			// Se for de outro controller, assume-se que o proprio view render tem o caminho completo, sem a necessidade de adicionar o controller provindo da url no caminho para o arquivo .ctp
			if (strpos($view_render, "/") === FALSE) {
				if (file_exists(APP.'View'.DS.'Themed'.DS.ucfirst(PROJETO).DS.ucfirst($controller->request->params['controller']).DS.$view_render.'.ctp')) {
					$file_ctp = APP.'View'.DS.'Themed'.DS.ucfirst(PROJETO).DS.ucfirst($controller->request->params['controller']).DS.$view_render.'.ctp';
				} else {
					$file_ctp = APP.'View'.DS.ucfirst($controller->request->params['controller']).DS.$view_render.'.ctp';
				}	
			} else {
				if (file_exists(APP.'View'.DS.'Themed'.DS.ucfirst(PROJETO).$view_render.'.ctp')) {
					$file_ctp = APP.'View'.DS.'Themed'.DS.ucfirst(PROJETO).$view_render.'.ctp';
				} else {
					$file_ctp = APP.'View'.$view_render.'.ctp';
				}
			}

			// Injetando scripts, css, etc. adicionados na página.
			$tags = $controller->CustomTagsReader->readFromFile($file_ctp);
			$tags = array_reverse($tags);
			foreach ($tags as $tag) {
				$inline = isset($tag['attributes']->inline) && $tag['attributes']->inline != 'false' ? true : false;
				$tipo = isset($tag['attributes']->tipo) ? $tag['attributes']->tipo : false;

				if (($tag['name'] == 'script' || $tag['name'] == 'css') && !$inline) {
					if (isset($tag['attributes']->src)) {
						if ($tag['name'] == 'script') {
							$view->Html->script(array($tag['attributes']->src), array('inline' => false));
						} else {
							$view->Html->css(array($tag['attributes']->src), 'stylesheet', array('inline' => false));
						}
					} else {
						if ($tag['name'] == 'script') {
							$view->Html->scriptBlock($tag['content'], array('inline' => false));
						} else {
							echo 'ERRO: A tag css não aceita blocos de código. O atributo  "src" deve ser especificado';
						}
					}
				}
			}

			// Fazendo parser do layout
			$html = $view->render($this->viewRender);

			// Fazendo parser do content separado do layout
			$view_content->layout = '__tipoo_tags__';
			$html_content = $view_content->render($this->viewRender);
			
			// Substituindo a tag fetch content pelo conteúdo
			$tags = $controller->CustomTagsReader->readFromString($html);
			$tags = array_reverse($tags);
			foreach ($tags as $tag) {
				if ($tag['name'] == 'fetch') {
					if (!isset($tag['attributes']->tipo)) {
						echo 'ERRO: É necessário especificar o parâmetro "tipo" para a tag fetch.';
					} else {
						if ($tag['attributes']->tipo == 'content') {
							$html = str_replace($tag['block'], $html_content, $html);
						}
					}
				}
			}

			require_once APP.'Vendor'.DS.'CustomTags'.DS.'CustomTags.php';
			$ct = new CustomTags(
				array(
					'parse_on_shutdown' => true,
					'tag_directory' => APP.'Vendor'.DS.'CustomTags'.DS.'tags'.DS,
					'sniff_for_buried_tags' => true,
					'tag_name' => 'tipoo',
					'tag_callback_prefix' => 'tipoo_',
					'view_context' => $view
				)
			);

			$ct->parse($html);
		}
	}
}
?>