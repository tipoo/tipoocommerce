<?php
App::uses('Component', 'Controller');

class RandomComponent extends Component {
	
	private $minusculas = 'abcdefghijklmnopqrstuvwxyz';
	private $maiusculas = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	private $numerais = '0123456789';
	
	public function gerarString($tam = 8, $incluirMinusculas = true, $incluirMaiusculas = true, $incluirNumerais = true) {
		$caracteres = $incluirMinusculas ? $this->minusculas : '';
		$caracteres .= $incluirMaiusculas ? $this->maiusculas : '';
		$caracteres .= $incluirNumerais ? $this->numerais : '';
		
		$chave = "";
		for ($i = 0; $i < $tam; $i++) {
			$chave .= substr($caracteres, rand(0, strlen($caracteres)), 1);
		}
		return $chave;
	}
	
}
?>