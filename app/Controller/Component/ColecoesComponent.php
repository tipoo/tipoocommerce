<?php
App::uses('Component', 'Controller');
class ColecoesComponent extends Component {

	private $ColecoesLocal;
	private $Controller;
	private $cep;

	public function initialize(Controller $controller) {
		parent::initialize($controller);

		$this->Controller = $controller;

		App::uses('ColecoesLocal', 'Model');
		$this->ColecoesLocal = new ColecoesLocal();

		if ($this->Controller->Session->check('Carrinho.CEP')) {
			$this->cep = $this->Controller->Session->read('Carrinho.CEP');
		}
	}

	public function setColecoesLocais($opts = array()) {

		$padrao = array(
			'pagina_id' => null,
			'categoria_id' => null,
			'produto_id' => null,
			'marca_id' => null
		);

		$opts = array_merge($padrao, $opts);

		$controleColecoes = $this->ColecoesLocal->find('all', array(
			'contain' => array(
				'Colecao.ColecoesProduto' => array(
					'Produto' => array(
						'Sku' => array(
							'conditions' => array(
								'Sku.ativo' => true
							),
							'order' => array(
								'ordem' => 'ASC',
							),
						),
						'TagsProduto.Tag.Slug',
						'CaracteristicasValoresSelecionado' => array(
							'CaracteristicasValor',
							'Caracteristica'
						),
						'Marca',
						'Categoria',
						'Slug',
						'Imagem' => array(
							'conditions' => array(
								'Imagem.ativo' => true,
								'Imagem.produto_id !=' => ''
							),
							'order' => array(
								'Imagem.ordem' => 'ASC'
							)
						),
						'conditions' => array(
							'Produto.ativo' => true
						)
					),
					'order' => array(
						'ColecoesProduto.ordem'
					)

				)
			),
			'conditions' => array(
				'ColecoesLocal.categoria_id' => $opts['categoria_id'],
				'ColecoesLocal.pagina_id' => $opts['pagina_id'],
				'ColecoesLocal.produto_id' => $opts['produto_id'],
				'ColecoesLocal.marca_id' => $opts['marca_id'],
				'ColecoesLocal.ativo' => true,
				'ColecoesLocal.data_inicio <=' => date('Y-m-d H:i:s'),
					'OR' => array(
						'ColecoesLocal.data_fim >=' => date('Y-m-d H:i:s'),
						'ColecoesLocal.data_fim' => ''
					)
			),
			'order' => array(
				'ColecoesLocal.id' => 'DESC'
			)
		));

		foreach ($controleColecoes as $key_controle_colecoes => $controleColecao) {
			foreach ($controleColecao['Colecao']['ColecoesProduto'] as $key => $produto) {
				if (count($produto['Produto']) == 0) {
					unset($controleColecoes[$key_controle_colecoes]['Colecao']['ColecoesProduto'][$key]);
				}
			}
		}

		$controleColecoes = $this->aplicar_regras($controleColecoes);

		$this->Controller->set(compact('controleColecoes'));
	}

	private function aplicar_regras($controleColecoes) {
		// Aqui aplicar regras de preços, promoções, estoque etc...
		// Criar component para ser reutilizad em varios locais

		// Normalizando o formato do cep
		$cep = null;
		if (!empty($this->cep)) {
			$cep = str_replace('-', '', $this->cep);
		}

		foreach ($controleColecoes as $key_controle_colecoes => $controleColecao) {
			foreach ($controleColecao['Colecao']['ColecoesProduto'] as $key => $produto) {

				$produto['Slug'] = $produto['Produto']['Slug'];
				unset($produto['Produto']['Slug']);

				$produto['Marca'] = $produto['Produto']['Marca'];
				unset($produto['Produto']['Marca']);

				$produto['Categoria'] = $produto['Produto']['Categoria'];
				unset($produto['Produto']['Categoria']);

				$produto['Imagem'] = $produto['Produto']['Imagem'];
				unset($produto['Produto']['Imagem']);

				$produto['Sku'] = $produto['Produto']['Sku'];
				unset($produto['Produto']['Sku']);

				$produto['TagsProduto'] = $produto['Produto']['TagsProduto'];
				unset($produto['Produto']['TagsProduto']);

				$produto['CaracteristicasValoresSelecionado'] = $produto['Produto']['CaracteristicasValoresSelecionado'];
				unset($produto['Produto']['CaracteristicasValoresSelecionado']);

				$controleColecoes[$key_controle_colecoes]['Colecao']['ColecoesProduto'][$key] = $this->Controller->RegrasProdutos->aplicar_regras($produto, $qtd_pecas = 1, $cep, $produto['Produto']['preco_por'], $qtd_pecas_carrinho = 1);
			}
		}

		return $controleColecoes;


		/*$retorno = array();

		foreach ($produtos as $key => $produto) {
			$produto = $this->Controller->RegrasProdutos->aplicar_regras($produto);
			$retorno[$key] = $produto;
		}

		return $retorno;*/
	}

}
?>