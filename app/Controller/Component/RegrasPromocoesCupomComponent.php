<?php
App::uses('RegrasPromocoesComponent', 'Controller/Component');

class RegrasPromocoesCupomComponent extends RegrasPromocoesComponent {

	protected $Pedido;
	protected $Controller;

	public function initialize(Controller $controller) {
		parent::initialize($controller);

		$this->Controller = $controller;

		App::uses('Pedido', 'Model');
		$this->Pedido = new Pedido();
	}

	public function aplicar_promocoes_cupom($valor_sub_total, $skus, $usuarioAutenticado) {

		// Se o cliente não estiver logado, retorna o subtotal e não passa pela verificação do cupom
		if (!$usuarioAutenticado) {
			return $valor_sub_total;
		}

		$total_desconto = 0;
		$promocao = null;
		$cupom_utilizado = null;
		$cupom_reutilizavel = true;

		foreach ($skus as $key => $sku) {
			if (isset($sku['Promocao']['cupom_desconto']['Promocao'])) {
				$promocao = $sku['Promocao']['cupom_desconto'];
				$cupom_utilizado = $this->verificar_cupom_utilizado($promocao, $usuarioAutenticado);
				$cupom_reutilizavel = $promocao['PromocaoCausa']['cupom_reutilizavel'];

				if (!$cupom_utilizado || $cupom_reutilizavel) {
					if ($promocao['PromocaoEfeito']['tipo_valor'] == 'P') {
						$total_desconto += $this->calcular_valor_monetario_desconto($sku['preco_total'], $promocao['PromocaoEfeito']['valor'], $promocao['PromocaoEfeito']['tipo_valor']);
					} else if ($promocao['PromocaoEfeito']['tipo_valor'] == 'M') {
						$total_desconto = $this->calcular_valor_monetario_desconto($sku['preco_total'], $promocao['PromocaoEfeito']['valor'], $promocao['PromocaoEfeito']['tipo_valor']);
						break;
					}
				}
			}
		}

		if ($promocao) {
			$valor_sub_total['Promocao']['cupom_desconto'] = $promocao;
			$valor_sub_total['Promocao']['cupom_desconto']['valor_desconto'] = $total_desconto;
			$valor_sub_total['Promocao']['cupom_desconto']['cupom_utilizado'] = $cupom_utilizado;
			$valor_sub_total['Promocao']['cupom_desconto']['cupom_reutilizavel'] = $cupom_reutilizavel;

			$valor_sub_total['valor_total'] = $valor_sub_total['valor_total'] - $valor_sub_total['Promocao']['cupom_desconto']['valor_desconto'];
			$valor_sub_total['valor_desconto'] = $valor_sub_total['Promocao']['cupom_desconto']['valor_desconto'];
		}

		return $valor_sub_total;
	}

	// Verifica se o cupom já foi utilizado pelo cliente, caso sim, não deixa utilizar novamente
	private function verificar_cupom_utilizado($promocao, $usuarioAutenticado) {
		if ($usuarioAutenticado) {
			$pedidos = $this->Pedido->find('all', array(
				'contain' => array(
					'Promocao.PromocaoCausa'
				),
				'conditions' => array(
					'Pedido.cliente_id' => $usuarioAutenticado['id'],
				)
			));

			$cupom_utilizado = false;
			foreach ($pedidos as $pedido) {
				if (isset($pedido['Promocao'])) {
					foreach ($pedido['Promocao'] as $promocao_pedido) {
						if (isset($promocao_pedido['PromocaoCausa']['cupom']) && $promocao_pedido['PromocaoCausa']['cupom'] == $promocao['PromocaoCausa']['cupom']) {
							$cupom_utilizado = true;
							break;
						}
					}
				}

				if ($cupom_utilizado) {
					break;
				}
			}

			return $cupom_utilizado;
		} else {
			return false;
		}
	}
}
?>