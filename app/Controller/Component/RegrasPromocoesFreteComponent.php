<?php
App::uses('RegrasPromocoesComponent', 'Controller/Component');

class RegrasPromocoesFreteComponent extends RegrasPromocoesComponent {

	protected $Controller;
	protected $PromocoesFreteTransportadora;

	public function initialize(Controller $Controller) {
		parent::initialize($Controller);
		$this->Controller = $Controller;

		App::uses('PromocoesFreteTransportadora', 'Model');
		$this->PromocoesFreteTransportadora = new PromocoesFreteTransportadora();
	}

	// Calculando o valor do frete considerando as promoções para a transportadora passada como parâmetro
	public function calcular_valor_frete($frete_transportadora = null, $skus = array(), $cep = null) {
		if (!count($skus) || !$frete_transportadora || !$cep) {
			return false;
		}

		// Calcula melhor promoção de produto para todos os skus
		$skus = $this->definir_melhor_promocao($frete_transportadora, $skus, $cep);

		// Verificando se todos os produtos tem promoção de frete e a pior promocão dentre eles.
		$todos_produtos_com_promocao_frete = true;
		$pior_promocao = array(
			'valor' => false,
			'promocao' => false
		);

		foreach ($skus as $sku) {

			if (!isset($sku['Promocao']['melhor_desconto_frete'])) {
				$todos_produtos_com_promocao_frete = false;
				break;
			}

			if ($pior_promocao['valor'] === false || $pior_promocao['valor'] < $sku['Promocao']['melhor_desconto_frete']['valor']) {
				$pior_promocao['valor'] = $sku['Promocao']['melhor_desconto_frete']['valor'];
				$pior_promocao['promocao'] = $sku['Promocao']['melhor_desconto_frete']['promocao'];
			}
		}

		if ($todos_produtos_com_promocao_frete) {
			$frete_transportadora['FreteTransportadora']['valor_frete'] = $pior_promocao['valor'];
			$frete_transportadora['FreteTransportadora']['promocao'] = $pior_promocao['promocao'];
		}

		return $frete_transportadora;
	}

	public function definir_melhor_promocao($frete_transportadora = null, $skus = array(), $cep = null) {
		if (!count($skus) || !$frete_transportadora || !$cep) {
			return false;
		}

		// Para cada sku vamos verificar a melhor promocao
		// $sku['Promocao']['melhor_desconto_frete'];
		foreach ($skus as $key => $sku) {
			if (isset($sku['Promocao']) && isset($sku['Promocao']) && count($sku['Promocao'])) {
				$melhor_promocao = array(
					'valor' => 0,
					'promocao' => false
				);

				foreach ($sku['Promocao']['desconto_frete'] as $promocao) {
					// Gambiarra para verificar as promoções de frete que vem do sku que está na sessão
					if ($promocao['PromocaoCausa']['cep_inicio'] != '' && $promocao['PromocaoCausa']['cep_fim'] != '') {
						$cep = str_replace('-', '', $cep);

						if ($promocao['PromocaoCausa']['cep_inicio'] <= $cep && $promocao['PromocaoCausa']['cep_fim'] >= $cep) {
							if ($this->transportadora_valida($frete_transportadora, $promocao)) {
								$valor = $this->calcular_valor_total_monetario_com_desconto($frete_transportadora['FreteTransportadora']['valor_frete'], $promocao['PromocaoEfeito']['valor'], $promocao['PromocaoEfeito']['tipo_valor']);

								if (!$melhor_promocao['promocao'] || $valor < $melhor_promocao['valor']) {
									$melhor_promocao['valor'] = $valor > 0 ? $valor : 0;
									$melhor_promocao['promocao'] = $promocao;
								}
							}
						}
					} else {
						if ($this->transportadora_valida($frete_transportadora, $promocao)) {
							$valor = $this->calcular_valor_total_monetario_com_desconto($frete_transportadora['FreteTransportadora']['valor_frete'], $promocao['PromocaoEfeito']['valor'], $promocao['PromocaoEfeito']['tipo_valor']);

							if (!$melhor_promocao['promocao'] || $valor < $melhor_promocao['valor']) {
								$melhor_promocao['valor'] = $valor > 0 ? $valor : 0;
								$melhor_promocao['promocao'] = $promocao;
							}
						}
					}
				}

				if ($melhor_promocao['promocao']) {
					$skus[$key]['Promocao']['melhor_desconto_frete'] = $melhor_promocao;
				}
			}
		}

		return $skus;
	}

	private function transportadora_valida($frete_transportadora = null, $promocao = null) {
		if (!$frete_transportadora || !$promocao) {
			return false;
		}

		return (bool) $this->PromocoesFreteTransportadora->find('count', array(
			'conditions' => array(
				'PromocoesFreteTransportadora.frete_transportadora_id' => $frete_transportadora['FreteTransportadora']['id'],
				'PromocoesFreteTransportadora.promocao_id' => $promocao['Promocao']['id']
			)
		));
	}
}
?>