<?php
App::uses('RegrasPromocoesComponent', 'Controller/Component');

class RegrasPromocoesPedidosComponent extends RegrasPromocoesComponent {

	public function definir_promocoes_pedido($valor_sub_total, $skus) {
		// Pega o primeiro indice do array de skus
		$first_sku = current($skus);
		if ($first_sku != '' && isset($first_sku['Promocao']['desconto_pedido'])) {
			$melhor_desconto_pedido = $this->definir_melhor_promocao($valor_sub_total, $first_sku['Promocao']['desconto_pedido']);

			if ($melhor_desconto_pedido['promocao']) {
				$valor_sub_total_promocao['Promocao']['melhor_desconto_pedido'] = $melhor_desconto_pedido;
				$valor_sub_total_promocao['Promocao']['melhor_desconto_pedido']['valor_original'] = $valor_sub_total;
				$valor_sub_total_promocao['Promocao']['melhor_desconto_pedido']['valor_desconto'] = $valor_sub_total - $melhor_desconto_pedido['valor'];
				$valor_sub_total = $valor_sub_total_promocao;
			}
		}

		return $valor_sub_total;
	}

	private function definir_melhor_promocao($valor_sub_total, $promocoes = array()) {
		// Definindo a melhor promoção de desconto no produto, pois só uma delas é apresentada ao cliente
		$melhor_desconto_pedido = array(
			'valor' => 0,
			'promocao' => false
		);

		foreach ($promocoes as $promocao) {
			$valor = $this->calcular_valor_total_monetario_com_desconto($valor_sub_total, $promocao['PromocaoEfeito']['valor'], $promocao['PromocaoEfeito']['tipo_valor']);
			if (($melhor_desconto_pedido['valor'] == 0 || $melhor_desconto_pedido['valor'] > $valor) && $valor > 0) {
				$melhor_desconto_pedido['valor'] = $valor;
				$melhor_desconto_pedido['promocao'] = $promocao;
			}
		}

		return $melhor_desconto_pedido;
	}

}
?>