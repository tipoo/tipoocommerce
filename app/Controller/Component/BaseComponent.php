<?php
App::uses('Component', 'Controller/Component');

class BaseComponent extends Component {

	public $Controller;
	public $CompreJuntoPromocao;
	public $AfinidadeProduto;
	public $Produto;

	public function initialize(Controller $controller) {
		parent::initialize($controller);
		$this->Controller = $controller;
	}

	protected function load_models() {
		$models = func_get_args();

		foreach ($models as $model) {
			App::uses($model, 'Model');
			$this->{$model} = new $model();	
		}
	}

}
?>
