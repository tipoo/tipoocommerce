<?php
App::uses('Component', 'Controller');

App::uses('Categoria', 'Model');
App::uses('Marca', 'Model');

class BreadCrumbComponent extends Component {

	private $Categoria;
	private $Marca;
	private $Controller;

	public function initialize(Controller $controller) {
		parent::initialize($controller);

		$this->Controller = $controller;

		$this->Categoria = new Categoria();
		$this->Marca = new Marca();
	}

	public function gerarPorCategoria($categoria_id) {
		$breadCrumb = array();

		do {
			$categoria = $this->Categoria->find('first', array(
				'contain' => array('Slug'),
				'conditions' => array('Categoria.id' => $categoria_id)
			));

			$breadCrumb[$categoria['Slug']['url']] = $categoria['Categoria']['descricao'];

			if (!$categoria['Categoria']['categoria_pai_id']) {
				$continuar = false;
			} else {
				$categoria_id = $categoria['Categoria']['categoria_pai_id'];
				$continuar = true;
			}

		} while ($continuar);

		$breadCrumb = array_reverse($breadCrumb);

		$this->Controller->set('controlesBreadCrumbCategoria', $breadCrumb);

	}

	public function gerarPorMarca($marca_id) {
		$marca = $this->Marca->find('first', array(
			'contain' => array('Slug'),
			'conditions' => array('Marca.id' => $marca_id)
		));

		$breadCrumb = array($marca['Slug']['url'] => $marca['Marca']['descricao']);

		$this->Controller->set('controlesBreadCrumbMarca', $breadCrumb);
	}
}
?>