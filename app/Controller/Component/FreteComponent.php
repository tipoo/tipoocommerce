<?php
App::uses('Component', 'Controller');

class FreteComponent extends Component {

	public $Controller;

	private $volume_total = 0;
	private $peso_total = 0;
	private $valor_total = 0;

	public function initialize(Controller $controller) {
		parent::initialize($controller);

		$this->Controller = $controller;
	}

	protected function calcular_peso_total($skus = array()) {
		$peso_total = 0;
		foreach ($skus as $sku) {
			$peso_total += $sku['peso_total'];
		}

		return $peso_total;
	}

	protected function calcular_volume_total($skus = array()) {
		$volume_total = 0;
		foreach ($skus as $sku) {
			$volume_total += $sku['qtd'] * ($sku['Sku']['largura'] * $sku['Sku']['altura'] * $sku['Sku']['profundidade']);
		}

		return $volume_total;
	}

	protected function calcular_valor_total($skus = array()) {
		$valor_total = 0;
		foreach ($skus as $sku) {
			$valor_total += $sku['preco_total'];
		}

		return $valor_total;
	}
}
?>