<?php
App::uses('Component', 'Controller');
class RedirectComponent extends Component {

	private $Controller;

	public function initialize(Controller $controller) {
		parent::initialize($controller);
		$this->Controller = $controller;
	}

	public function captureRedirectUrl() {
		if (isset($this->Controller->request->query['redirect'])) {
			$redirect = $this->Controller->request->query['redirect'];
			CakeSession::write('Redirect.url', $redirect);
		}
	}

	public function redirect($clearRedirect = true) {
		if ($this->Controller->Session->check('Redirect.url')) {
			$redirect = CakeSession::read('Redirect.url');

			if ($clearRedirect) {
				CakeSession::delete('Redirect.url');
			}

			$this->Controller->redirect($redirect);

			return true;
		}

		return false;
	}

}
?>