<?php
App::uses('Configuracao', 'Model');
App::uses('FreteTransportadora', 'Model');
App::uses('FreteComponent', 'Controller/Component');

class FreteCorreiosComponent extends FreteComponent {

	public $Controller;
	public $Configuracao;

	private $maiorLargura;
	private $maiorAltura;
	private $maiorProfundidade;

	public function initialize(Controller $controller) {
		parent::initialize($controller);

		$this->Controller = $controller;

		$this->Configuracao = new Configuracao();
		$this->FreteTransportadora = new FreteTransportadora();
	}

	// retorna um array com sucesso, valor e prazo_entrega em caso de sucesso e sucesso=false e msg em caso de erro
	public function buscar($skus, $cep_destino) {
		$largura = 0;
		$altura = 0;
		$profundidade = 0;

		$frete_transportadoras = $this->get_transportadoras_disponiveis();

		// Calculando LxAxP do pacote total
		$largura = $altura = $profundidade = pow($this->calcular_volume_total($skus), 1/3);

		$largura = round($largura);
		$altura = round($altura);
		$profundidade = round($profundidade);

		/* Definindo os valores mínimos e máximos para largura */
		if ($largura < 11) {
			$largura = 11;
		} else if ($largura > 105) {
			$largura = 105;
		}

		/* Definindo os valores mínimos e máximos para altura */
		if ($altura < 2) {
			$altura = 2;
		} else if ($altura > 105) {
			$altura = 105;
		}

		/* Definindo os valores mínimos e máximos para profundidade */
		if ($profundidade < 16) {
			$profundidade = 16;
		} else if ($profundidade > 105) {
			$profundidade = 105;
		}

		// peso em kg
		$peso = $this->calcular_peso_total($skus) / 1000;

		$data['nCdEmpresa'] = $this->Configuracao->get('frete_correios_codigo_empresa');
		$data['sDsSenha'] = $this->Configuracao->get('frete_correios_senha');
		$data['sCepOrigem'] = $this->Configuracao->get('frete_cep_origem');
		$data['sCepDestino'] = $cep_destino;
		$data['nVlPeso'] = $peso;
		$data['nCdFormato'] = '1';
		$data['nVlComprimento'] = $profundidade;
		$data['nVlAltura'] = $altura;
		$data['nVlLargura'] = $largura;
		$data['nVlDiametro'] = '0';
		$data['sCdMaoPropria'] = 'n';
		$data['nVlValorDeclarado'] = 0; // number_format($valor_total, 2, ",", "");
		$data['sCdAvisoRecebimento'] = 'n';
		$data['StrRetorno'] = 'xml';
		$data['nCdServico'] = $this->get_servicos($frete_transportadoras);
		$data = http_build_query($data);

		$url = 'http://ws.correios.com.br/calculador/CalcPrecoPrazo.aspx';

		$curl = curl_init($url . '?' . $data);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		$result = curl_exec($curl);
		$result = simplexml_load_string($result);
		$result = $result[0];

		$cServicos = array();
		if (isset($result->cServico) && $result->cServico) {
			foreach ($result->cServico as $cServico) {
				if ($cServico->Erro == 0) {
					$cServicos[(string) $cServico->Codigo] = array(
						'sucesso' => true,
						'valor_frete' => str_replace(',', '.', (string) $cServico->Valor),
						'prazo_entrega_frete' => (string) $cServico->PrazoEntrega
					);
				}
			}
		}

		$frete_transportadoras_disponiveis = array();
		foreach ($frete_transportadoras as $frete_transportadora) {
			$key = $frete_transportadora['FreteTransportadora']['correios_codigo_servico'];
			if (array_key_exists($key, $cServicos)) {
				$frete_transportadora['FreteTransportadora']['valor_frete'] = $cServicos[$key]['valor_frete'];
				$frete_transportadora['FreteTransportadora']['prazo_entrega_frete'] = $cServicos[$key]['prazo_entrega_frete'] + $frete_transportadora['FreteTransportadora']['prazo_carencia'];
				$frete_transportadoras_disponiveis[] = $frete_transportadora;
			}
		}

		return $frete_transportadoras_disponiveis;
	}

	private function get_transportadoras_disponiveis() {
		return $this->FreteTransportadora->find('all', array(
			'contain' => false,
			'conditions' => array(
				'FreteTransportadora.ativo',
				'FreteTransportadora.frete_tipo' => 'C'
			)
		));
	}

	private function get_servicos($frete_transportadoras) {
		$codigos = '';
		foreach ($frete_transportadoras as $frete_transportadora) {
			if (!empty($codigos)) {
				$codigos .= ', ';
			}
			$codigos .= $frete_transportadora['FreteTransportadora']['correios_codigo_servico'];
		}
		return $codigos;
	}
}
?>