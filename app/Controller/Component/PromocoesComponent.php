<?php
class PromocoesComponent extends Component {

	protected $Controller;

	public function initialize(Controller $Controller) {
		parent::initialize($Controller);
		$this->Controller = $Controller;
		$this->Controller->loadModel('Promocao');
		$this->Controller->loadModel('ColecoesProduto');
		$this->Controller->loadModel('Categoria');
	}

	protected function verificar_causas($promocao, $cep = null, $sub_total = null, $marca_id = null, $produto_id = null,  $categorias_id = array(), $colecoes_ids = array(), $utm_source = null, $utm_medium = null, $utm_campaign = null, $qtd_pecas = 0, $ignorar = array(), $sub_total_carrinho = null, $carrinho_qtd_pecas, $cupom = null) {
		$promocao_valida = true;
		$promocao_verificada = false;

		if ($promocao['PromocaoCausa']['cep_inicio'] != '' && $promocao['PromocaoCausa']['cep_fim'] != '') {
			$promocao_verificada = true;
			$cep = str_replace('-', '', $cep);

			if (!in_array('cep', $ignorar) && !($promocao['PromocaoCausa']['cep_inicio'] <= $cep && $promocao['PromocaoCausa']['cep_fim'] >= $cep)) {
				$promocao_valida = false;
			} else {
				$promocao = $this->registrar_causa_valida($promocao, '[#' . $promocao['PromocaoCausa']['id'] . '] - CEP ' . $cep . ' está entre ' . $promocao['PromocaoCausa']['cep_inicio'] . ' e ' . $promocao['PromocaoCausa']['cep_fim']);
			}
		}

		if ($promocao_valida && !empty($promocao['PromocaoCausa']['valor_acima_de'])) {
			$promocao_verificada = true;
			if ($promocao['PromocaoCausa']['valor_acima_de'] > $sub_total && !in_array('valor_acima_de', $ignorar)) {
				$promocao_valida = false;
			} else {
				$promocao = $this->registrar_causa_valida($promocao, '[#' . $promocao['PromocaoCausa']['id'] . '] - Subtotal ' . $sub_total . ' está igual ou acima de ' . $promocao['PromocaoCausa']['valor_acima_de']);
			}
		}

		if ($promocao_valida && !empty($promocao['PromocaoCausa']['carrinho_valor_acima_de'])) {
			$promocao_verificada = true;
			if ($promocao['PromocaoCausa']['carrinho_valor_acima_de'] > $sub_total_carrinho && !in_array('carrinho_valor_acima_de', $ignorar)) {
				$promocao_valida = false;
			} else {
				$promocao = $this->registrar_causa_valida($promocao, '[#' . $promocao['PromocaoCausa']['id'] . '] - Subtotal Carrinho ' . $sub_total_carrinho . ' está igual ou acima de ' . $promocao['PromocaoCausa']['carrinho_valor_acima_de']);
			}
		}

		if ($promocao_valida && !empty($promocao['PromocaoCausa']['marca_id'])) {
			$promocao_verificada = true;
			if ($promocao['PromocaoCausa']['marca_id'] != $marca_id && !in_array('marca_id', $ignorar)) {
				$promocao_valida = false;
			} else {
				$promocao = $this->registrar_causa_valida($promocao, '[#' . $promocao['PromocaoCausa']['id'] . '] - marca_id igual a ' . $marca_id);
			}
		}

		if ($promocao_valida && !empty($promocao['PromocaoCausa']['produto_id'])) {
			$promocao_verificada = true;
			if ($promocao['PromocaoCausa']['produto_id'] != $produto_id && !in_array('produto_id', $ignorar)) {
				$promocao_valida = false;
			} else {
				$promocao = $this->registrar_causa_valida($promocao, '[#' . $promocao['PromocaoCausa']['id'] . '] - produto_id igual a ' . $produto_id);
			}
		}

		if ($promocao_valida && !empty($promocao['PromocaoCausa']['colecao_id'])) {
			$promocao_verificada = true;
			if (!in_array($promocao['PromocaoCausa']['colecao_id'], $colecoes_ids) && !in_array('colecao_id', $ignorar)) {
				$promocao_valida = false;
			} else {
				$promocao = $this->registrar_causa_valida($promocao, '[#' . $promocao['PromocaoCausa']['id'] . '] - colecao_id está contido em colecoes_ids (' . implode(', ', $colecoes_ids) . ')');
			}
		}

		if ($promocao_valida && !empty($promocao['PromocaoCausa']['categoria_id'])) {
			$promocao_verificada = true;
			if (!in_array($promocao['PromocaoCausa']['categoria_id'], $categorias_id) && !in_array('categoria_id', $ignorar)) {
				$promocao_valida = false;
			} else {
				$promocao = $this->registrar_causa_valida($promocao, '[#' . $promocao['PromocaoCausa']['id'] . '] - categoria_id está contido em categorias_ids (' . implode(', ', $categorias_id) . ')');
			}
		}

		if ($promocao_valida && !empty($promocao['PromocaoCausa']['utm_source'])) {
			$promocao_verificada = true;
			if ($promocao['PromocaoCausa']['utm_source'] != $utm_source && !in_array('utm_source', $ignorar)) {
				$promocao_valida = false;
			} else {
				$promocao = $this->registrar_causa_valida($promocao, '[#' . $promocao['PromocaoCausa']['id'] . '] - utm_source igual a ' . $utm_source);
			}
		}

		if ($promocao_valida && !empty($promocao['PromocaoCausa']['utm_medium'])) {
			$promocao_verificada = true;
			if ($promocao['PromocaoCausa']['utm_medium'] != $utm_medium && !in_array('utm_medium', $ignorar)) {
				$promocao_valida = false;
			} else {
				$promocao = $this->registrar_causa_valida($promocao, '[#' . $promocao['PromocaoCausa']['id'] . '] - utm_medium igual a ' . $utm_medium);
			}
		}

		if ($promocao_valida && !empty($promocao['PromocaoCausa']['utm_campaign'])) {
			$promocao_verificada = true;
			if ($promocao['PromocaoCausa']['utm_campaign'] != $utm_campaign && !in_array('utm_campaign', $ignorar)) {
				$promocao_valida = false;
			} else {
				$promocao = $this->registrar_causa_valida($promocao, '[#' . $promocao['PromocaoCausa']['id'] . '] - utm_campaign igual a ' . $utm_campaign);
			}
		}

		if ($promocao_valida && !empty($promocao['PromocaoCausa']['qtd_pecas'])) {
			$promocao_verificada = true;
			if ($promocao['PromocaoCausa']['qtd_pecas'] > $qtd_pecas && !in_array('qtd_pecas', $ignorar)) {
				$promocao_valida = false;
			} else {
				$promocao = $this->registrar_causa_valida($promocao, '[#' . $promocao['PromocaoCausa']['id'] . '] - Quandidade de peças ' . $qtd_pecas . ' igual ou acima de ' . $promocao['PromocaoCausa']['qtd_pecas']);
			}
		}

		if ($promocao_valida && !empty($promocao['PromocaoCausa']['carrinho_qtd_pecas'])) {
			$promocao_verificada = true;
			if ($promocao['PromocaoCausa']['carrinho_qtd_pecas'] > $carrinho_qtd_pecas && !in_array('carrinho_qtd_pecas', $ignorar)) {
				$promocao_valida = false;
			} else {
				$promocao = $this->registrar_causa_valida($promocao, '[#' . $promocao['PromocaoCausa']['id'] . '] - Quandidade de peças carrinho ' . $carrinho_qtd_pecas . ' igual ou acima de ' . $promocao['PromocaoCausa']['carrinho_qtd_pecas']);
			}
		}

		if ($promocao_valida && !empty($promocao['PromocaoCausa']['cupom'])) {
			$promocao_verificada = true;
			if ($promocao['PromocaoCausa']['cupom'] != $cupom && !in_array('cupom', $ignorar)) {
				$promocao_valida = false;
			} else {
				$promocao = $this->registrar_causa_valida($promocao, '[#' . $promocao['PromocaoCausa']['id'] . '] - Cupom de desconto ' . $cupom . ' igual a ' . $promocao['PromocaoCausa']['cupom']);
			}
		}

		if ($promocao_valida && $promocao_verificada) {
			return $promocao;
		}

		return false;
	}

	private function registrar_causa_valida($promocao, $texto) {
		if (!isset($promocao['CausasValidas'])) {
			$promocao['CausasValidas'] = array();
		}

		$promocao['CausasValidas'][] = $texto;

		return $promocao;
	}

	protected function get_utm_session($utm = 'utm_source') {
		return $this->Controller->Session->check('Promocao.' . $utm) ? $this->Controller->Session->read('Promocao.' . $utm) : null;
	}

	// Retorna todas as promoções que se encaixam no contexto atual da compra e do produto
	public function validas($produto, $qtd_pecas, $cep, $ignorar = array(), $sub_total_carrinho = null, $qtd_pecas_carrinho = null, $cupom = null) {
		$promocoes = $this->Controller->Promocao->find('all', array(
			'contain' => array(
				'PromocaoCausa',
				'PromocaoEfeito'
			),
			'conditions' => array(
				'Promocao.ativo',
				'Promocao.data_inicio <= ' => date('Y-m-d H:i:s'),
				'Promocao.data_fim >= ' => date('Y-m-d H:i:s'),
				'Promocao.situacao' => 'A',
				'Promocao.pedido_id' => '',
				'Promocao.pedido_sku_id' => ''
			)
		));

		$sub_total = $produto['preco_por'] * $qtd_pecas;

		$colecoes_produtos = $this->Controller->ColecoesProduto->find('all', array(
			'contain' => array('Colecao.ativo'),
			'conditions' => array(
				'ColecoesProduto.produto_id' => $produto['id'],
				'Colecao.ativo' => true
			)
		));

		/*
		 * Monta um arrar com os ids das coleções em quem o produto está adiconado
		 */
		$colecoes_ids = array();
		foreach ($colecoes_produtos as $colecoes_produto) {
			$colecoes_ids[] = $colecoes_produto['ColecoesProduto']['colecao_id'];
		}

		/*
		 * Monta um arrar com os ids das categorias pai
		 */
		$categoria_id = $produto['categoria_id'];
		$categorias_ids = array();
		do {
			$categoria = $this->Controller->Categoria->find('first', array(
				'contain' => false,
				'conditions' => array('Categoria.id' => $categoria_id)
			));

			$categorias_ids[] = $categoria['Categoria']['id'];

			if (!$categoria['Categoria']['categoria_pai_id']) {
				$continuar = false;
			} else {
				$categoria_id = $categoria['Categoria']['categoria_pai_id'];
				$continuar = true;
			}

		} while ($continuar);

		$promocoes_validas = array();
		foreach ($promocoes as $promocao) {
			$promocao = $this->verificar_causas(
				$promocao,
				$cep,
				$sub_total,
				$produto['marca_id'],
				$produto['id'],
				$categorias_ids,
				$colecoes_ids,
				$this->get_utm_session('utm_source'),
				$this->get_utm_session('utm_medium'),
				$this->get_utm_session('utm_campaign'),
				$qtd_pecas,
				$ignorar,
				$sub_total_carrinho,
				$qtd_pecas_carrinho,
				$cupom
			);

			if ($promocao) {
				$promocoes_validas[] = $promocao;
			}
		}

		return $promocoes_validas;
	}

	public function aplicar_promocoes($produto, $qtd_pecas = null, $cep = null, $sub_total_carrinho = null, $qtd_pecas_carrinho = null, $cupom = null) {
		$ignorar = array();
		if (!$qtd_pecas) {
			$ignorar[] = 'qtd_pecas';
			$qtd_pecas = 1;
		}
		if (!$qtd_pecas_carrinho) {
			$ignorar[] = 'carrinho_qtd_pecas';
			$qtd_pecas_carrinho = 1;
		}
		if (!$cep) {
			$ignorar[] = 'cep';
		}

		$promocoes_validas = $this->validas($produto['Produto'], $qtd_pecas, $cep, $ignorar, $sub_total_carrinho, $qtd_pecas_carrinho, $cupom);

		$produto['Promocao']['desconto_produto'] = array();
		$produto['Promocao']['desconto_pedido'] = array();
		$produto['Promocao']['desconto_frete'] = array();
		$produto['Promocao']['leve_x_pague_y'] = array();
		$produto['Promocao']['cupom_desconto'] = null;

		foreach ($promocoes_validas as $promocao) {

			switch ($promocao['Promocao']['element']) {
				case 'desconto_produto':
					$produto['Promocao']['desconto_produto'][] = $promocao;
					break;
				case 'desconto_pedido':
					$produto['Promocao']['desconto_pedido'][] = $promocao;
					break;
				case 'desconto_frete':
					$produto['Promocao']['desconto_frete'][] = $promocao;
					break;
				case 'leve_x_pague_y':
					$produto['Promocao']['leve_x_pague_y'][] = $promocao;
					break;
				case 'cupom_desconto':
					$produto['Promocao']['cupom_desconto'] = $promocao;
					break;
			}
		}

		return $produto;
	}
}
?>