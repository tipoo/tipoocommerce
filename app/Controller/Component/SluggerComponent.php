<?php
App::uses('Component', 'Controller');
class SluggerComponent extends Component {

	public function regularizarSlugComposto($url) {
		$urlPartes = explode('/', $url);
		$urlSlugs = array();
		foreach ($urlPartes as $key => $value) {
			if ($value) {
				$urlSlugs[] = Inflector::slug($value, '-');
			}
		}
		return '/'.implode('/', $urlSlugs);
	}

}

?>