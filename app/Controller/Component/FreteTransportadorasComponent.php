<?php
App::uses('Configuracao', 'Model');
App::uses('FreteValor', 'Model');
App::uses('FreteComponent', 'Controller/Component');

class FreteTransportadorasComponent extends FreteComponent {

	public $Controller;
	public $Configuracao;

	public function initialize(Controller $controller) {
		parent::initialize($controller);

		$this->Controller = $controller;

		$this->Configuracao = new Configuracao();
		$this->FreteValor = new FreteValor();
	}

	public function buscar($skus, $cep_destino) {
		$peso = $this->calcular_peso_total($skus);

		$frete_valores = $this->FreteValor->find('all', array(
			'contain' => array(
				'FreteTransportadora' => array(
					'fields' => array('id', 'descricao', 'frete_tipo', 'correios_codigo_servico', 'prazo_carencia', 'loja', 'mercado_livre', 'ativo')
				),
			),
			'conditions'=> array(
				'FreteValor.cep_inicio <=' => $cep_destino,
				'FreteValor.cep_fim >=' => $cep_destino,
				'FreteValor.peso_inicio <=' => $peso,
				'FreteValor.peso_fim >=' => $peso,
				'FreteTransportadora.frete_tipo' => 'T',
				'FreteTransportadora.loja' => true,
				'FreteTransportadora.ativo'
			),
			'fields' => array('id', 'frete_transportadora_id', 'cep_inicio', 'cep_fim', 'peso_inicio', 'peso_fim', 'valor', 'prazo', 'ativo', '(FreteValor.prazo + FreteTransportadora.prazo_carencia) as FreteValor__prazo_carencia')
		));

		foreach ($frete_valores as $key => $frete_valor) {
			$frete_valores[$key]['FreteTransportadora']['valor_frete'] = $frete_valor['FreteValor']['valor'];
			$frete_valores[$key]['FreteTransportadora']['prazo_entrega_frete'] = $frete_valor['FreteValor']['prazo_carencia'];
			unset($frete_valores[$key]['FreteValor']);
		}

		return $frete_valores;
	}

}
