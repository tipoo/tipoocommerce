<?php
class RegrasPromocoesPrecosComponent extends RegrasPromocoesComponent {

	public function recalcular($skus) {
		foreach ($skus as $key => $sku) {
			$tem_melhor_desconto_produto = !empty($sku['Promocao']['melhor_desconto_produto']['valor']) && $sku['Promocao']['melhor_desconto_produto']['valor'] > 0;
			//$tem_melhor_compre_junto = !empty($sku['Promocao']['melhor_compre_junto']['valor']) && $sku['Promocao']['melhor_compre_junto']['valor'] > 0);
			$tem_melhor_compre_junto = false; // TODO Alterar pela linha acima quando tiver compre junto

			// Verifica se tem melhor_desconto_produto ou melhor_compre_junto
			$melhor_promocao = null;
			if ($tem_melhor_desconto_produto || $tem_melhor_compre_junto) {
				// Verifica melhor desconto entre melhor_compre_junto e melhor_desconto_produto
				// if () {
					$melhor_promocao = $sku['Promocao']['melhor_desconto_produto']; // TODO Trocar para melhor promoção entre melhor_compre_junto e melhor_desconto_produto

				// }
			}

			$skus[$key] = $this->recalcular_valores_sku($skus[$key], $melhor_promocao);

		}

		return $skus;
	}
}
?>