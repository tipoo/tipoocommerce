<?php
App::uses('Component', 'Controller');

class FileUploadComponent extends Component {

	public $mensagemErro = null;
	public $tiposPermitidos = array(
		'image/jpeg', // images
		'image/pjpeg',
		'image/png',
		'image/gif',
		'image/tiff',
		'image/x-tiff',
		'application/pdf', // pdf
		'application/x-pdf',
		'application/acrobat',
		'text/pdf',
		'text/x-pdf',
		'text/plain',
		'text/csv', // text
		'application/msword', // word
		'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
		'application/mspowerpoint', // powerpoint
		'application/powerpoint',
		'application/vnd.ms-powerpoint',
		'application/x-mspowerpoint',
		'application/vnd.openxmlformats-officedocument.presentationml.presentation',
		'application/x-msexcel', // excel
		'application/excel',
		'application/x-excel',
		'application/vnd.ms-excel',
		'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
		'application/x-compressed', // compressed files
		'application/x-zip-compressed',
		'application/zip',
		'multipart/x-zip',
		'application/x-tar',
		'application/x-compressed',
		'application/x-gzip',
		'application/x-gzip',
		'multipart/x-gzip',
		'application/octet-stream',
		'application/x-shockwave-flash'
	);

	public function setTiposPermitidos($tiposPermitidos = array()) {
		$this->tiposPermitidos = $tiposPermitidos;
	}

	public function permitirSomenteImagens() {
		$this->setTiposPermitidos(array(
			'image/jpeg', // images
			'image/pjpeg',
			'image/png',
			'image/gif'
		));
	}

	public function upload($dir, $formdata, $criarDir = true) {
		$this->mensagemErro = null;

		if (!is_dir($dir) && $criarDir) {
			mkdir($dir);
		}

		foreach($formdata as $file) {
			$filename = $file['name'];

			$typeOK = false;

			foreach($this->tiposPermitidos as $type) {
				if($type == $file['type']) {
					$typeOK = true;
					break;
				}
			}

			if ($typeOK) {
				switch ($file['error']) {
					case UPLOAD_ERR_OK:

						if (!file_exists($dir . DS . $filename)) {
							move_uploaded_file($file['tmp_name'], $dir . DS . $filename);
							return $filename;
						} else {
							$arquivo = $dir . DS . $filename;

							$cont = 0;
							do {
								$cont++;
								$pathParts = pathinfo($arquivo);
								$nomeNovoArquivo = $pathParts['filename'] . '_' . $cont . '.' . $pathParts['extension'];
								$novoArquivo = $dir . DS . $nomeNovoArquivo;

							} while (file_exists($novoArquivo));

							move_uploaded_file($file['tmp_name'], $novoArquivo);
							return $nomeNovoArquivo;
						}

					case UPLOAD_ERR_INI_SIZE:
						$this->mensagemErro = 'O tamanho do arquivo excede o limite configurado.';
						return false;

					case UPLOAD_ERR_FORM_SIZE:
						$this->mensagemErro = 'O tamanho do arquivo excede o limite estabelecido.';
						return false;

					case UPLOAD_ERR_PARTIAL:
						$this->mensagemErro = 'O envio do arquivo não foi totalmente concluído. Por favor, tente novamente.';
						return false;

					case UPLOAD_ERR_NO_FILE:
						$this->mensagemErro = 'Nenhum arquivo foi enviado. Por favor, tente novamente.';
						return false;

					case UPLOAD_ERR_NO_TMP_DIR:
						$this->mensagemErro = 'Ocorreu um erro ao tentar enviar arquivo. O diretório temporário não foi encontrado.';
						return false;

					case UPLOAD_ERR_CANT_WRITE:
						$this->mensagemErro = 'Ocorreu um erro ao tentar enviar arquivo. Permissão negada.';
						return false;

					case UPLOAD_ERR_EXTENSION:
						$this->mensagemErro = 'Ocorreu um erro ao tentar enviar o arquivo. Um componente do servidor pode ter causado este erro.';
						return false;

					default:
						$this->mensagemErro = 'Ocorreu um erro desconhecido ao tentar enviar arquivo. Por favor, tente novamente.';
						return false;
				}
			} else {
				$this->mensagemErro =  'Tipo de arquivo não suportado por este formulário.';
				return false;
			}
		}
	}

}
?>