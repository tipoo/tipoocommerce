<?php
App::uses('Component', 'Controller');
class MenuCategoriasComponent extends Component {

	private $Categoria;
	private $Controller;

	public function initialize(Controller $controller) {
		parent::initialize($controller);

		$this->Controller = $controller;

		App::uses('Categoria', 'Model');
		$this->Categoria = new Categoria();
	}

	public function setMenuCategorias() {
		$controlesMenuCategorias = $this->Categoria->find('threaded', array(
			'contain' => array('Slug.url'),
			'parent' => 'categoria_pai_id',
			'conditions' => array('Categoria.menu', 'Categoria.ativo')
		));

		$this->Controller->set(compact('controlesMenuCategorias'));
	}

	public function setMenuCategoria($id) {
		if (!isset($this->Controller->viewVars['controlesMenuCategorias'])) {
			$this->setMenuCategorias();
		}
		$categorias = $this->Controller->viewVars['controlesMenuCategorias'];
		$controlesMenuCategoria = $this->buscarCategoria($id, $categorias);

		$this->Controller->set(compact('controlesMenuCategoria'));
	}

	private function buscarCategoria($id, $categorias) {
		foreach ($categorias as $categoria) {
			if ($categoria['Categoria']['id'] == $id) {
				return $categoria;
				break;
			}

			if (count($categoria['children'])) {
				$categoria = $this->buscarCategoria($id, $categoria['children']);
				if ($categoria) {
					return $categoria;
					break;
				}
			}
		}
	}

	public function getArvoreCategorias() {
		return $this->Categoria->find('threaded', array(
			'contain' => false,
			'parent' => 'categoria_pai_id',
			'conditions' => array('Categoria.ativo')
		));
	}

	public function getCategoriasIn($categoria_id, $categoriasIn = array()) {
		$categoria = $this->Controller->Categoria->find('first', array(
			'contain' => false,
			'fields' => array('id'),
			'conditions' => array(
				'Categoria.id' => $categoria_id
			)
		));

		$categoriasIn[] = $categoria['Categoria']['id'];

		$categoriasFilhas = $categoria = $this->Controller->Categoria->find('all', array(
			'contain' => false,
			'fields' => array('id'),
			'conditions' => array(
				'Categoria.categoria_pai_id' => $categoria['Categoria']['id']
			)
		));

		if (count($categoriasFilhas)) {
			foreach ($categoriasFilhas as $categoria) {
				$categoriasIn = $this->getCategoriasIn($categoria['Categoria']['id'], $categoriasIn);
			}
		}

		return $categoriasIn;
	}

}
?>