<?php
App::uses('HttpSocket', 'Network/Http');

class ClearSaleComponent extends Component {

	public $Controller;
	public $Configuracao;
	public $Pedido;

	public function initialize(Controller $controller) {
		parent::initialize($controller);
		$this->Controller = $controller;

		App::uses('Configuracao', 'Model');
		$this->Configuracao = new Configuracao();

		App::uses('Pedido', 'Model');
		$this->Pedido = new Pedido();
	}

	public function start($pedido_id) {

		$pedido = $this->Pedido->get($pedido_id);

		if ($this->Configuracao->get('clear_sale_ambiente_sandbox')) {
			$clear_sale_url = $this->Configuracao->get('clear_sale_url_sandbox');
		} else if ($this->Configuracao->get('clear_sale_ambiente_producao')) {
			$clear_sale_url = $this->Configuracao->get('clear_sale_url_producao');
		}

		$forma_pagamento = $this->getCodigoFormaPagamento();

		// Dados do Pedido
		$params = array(
			'CodigoIntegracao' => $this->Configuracao->get('clear_sale_codigo'), // Obrigatório
			'PedidoID' => $pedido['Pedido']['id'], // Obrigatório
			'Data' => date('d/m/Y H:i:s', strtotime($pedido['Pedido']['data_hora'])), // Obrigatório
			'Total' => $pedido['Pedido']['valor_total'], // Obrigatório
			'TipoPagamento' => $forma_pagamento['CartaoCredito'], // Obrigatório //TODO $pedido['Pedido']['forma_pagamento']
			// 'IP' => '',
			// 'TipoCartao' => '',
			// 'Parcelas' => '',
			// 'Cartao_Bin' => '',
			// 'Cartao_Fim' => '',
			// 'Cartao_Numero_Mascarado' => ''
		);

		$telefone = $this->converterTelefone($pedido['Cliente']['tel_residencial']);

		// Dados de Cobrança
		$params = $params + array(
			'Cobranca_Nome' => $pedido['Cliente']['nome'],
			'Cobranca_Nascimento' => date('d/m/Y H:i:s', strtotime($pedido['Cliente']['data_nascto'])),
			'Cobranca_Email' => $pedido['Cliente']['email'], // Obrigatório
			'Cobranca_Documento' => $this->converterCpf($pedido['Cliente']['cpf']), // Obrigatório
			'Cobranca_Logradouro' => $pedido['EnderecoCobranca']['endereco'],
			'Cobranca_Logradouro_Numero' => $pedido['EnderecoCobranca']['numero'],
			'Cobranca_Bairro' => $pedido['EnderecoCobranca']['bairro'],
			'Cobranca_Cidade' => $pedido['EnderecoCobranca']['Cidade']['nome'],
			'Cobranca_Estado' => $pedido['EnderecoCobranca']['Cidade']['uf'],
			'Cobranca_CEP' => $this->converterCep($pedido['EnderecoCobranca']['cep']),
			'Cobranca_Pais' => 'Bra',
			'Cobranca_DDD_Telefone_1' => $telefone['ddd'], // Obrigatório
			'Cobranca_Telefone_1' => $telefone['numero'], // Obrigatório
			// 'Cobranca_DDD_Celular' => ,
			// 'Cobranca_Celular' =>
		);

		if ($pedido['EnderecoCobranca']['complemento'] != '') {
			$params = $params + array(
				'Cobranca_Logradouro_Complemento' => $pedido['EnderecoCobranca']['complemento'],
			);
		}

		// Dados de Entrega
		$params = $params + array(
			'Entrega_Nome' => $pedido['Cliente']['nome'],
			'Entrega_Nascimento' => date('d/m/Y H:i:s', strtotime($pedido['Cliente']['data_nascto'])),
			'Entrega_Email' => $pedido['Cliente']['email'],
			'Entrega_Documento' => $this->converterCpf($pedido['Cliente']['cpf']),
			'Entrega_Logradouro' => $pedido['EnderecoEntrega']['endereco'],
			'Entrega_Logradouro_Numero' => $pedido['EnderecoEntrega']['numero'],
			'Entrega_Bairro' => $pedido['EnderecoEntrega']['bairro'],
			'Entrega_Cidade' => $pedido['EnderecoEntrega']['Cidade']['nome'],
			'Entrega_Estado' => $pedido['EnderecoEntrega']['Cidade']['uf'],
			'Entrega_CEP' => $this->converterCep($pedido['EnderecoEntrega']['cep']),
			'Entrega_Pais' => 'Bra',
			'Entrega_DDD_Telefone_1' => $telefone['ddd'],
			'Entrega_Telefone_1' => $telefone['numero'],
			// 'Entrega_DDD_Celular' => ,
			// 'Entrega_Celular' =>
		);

		if ($pedido['EnderecoEntrega']['complemento'] != '') {
			$params = $params + array(
				'Entrega_Logradouro_Complemento' => $pedido['EnderecoEntrega']['complemento'],
			);
		}

		// Dados do(s) Intem(ns)
		$item = 1;
		foreach($pedido['PedidoSku'] as $sku) {
			$params = $params + array(
				'Item_ID_' . $item  => $sku['sku'],
				'Item_Nome_' . $item => $sku['descricao_produto'] . ' - ' . $sku['descricao_sku'], // Obrigatório
				'Item_Qtd_' . $item => $sku['qtd'],
				'Item_Valor_' . $item =>  $sku['preco_unitario_desconto'] > 0 ? $sku['preco_unitario_desconto'] : $sku['preco_unitario'],
			);

			$item++;
		}

		$HttpSocket = new HttpSocket();
		$HttpSocket->post($clear_sale_url, $params);
	}

	private function converterCpf($cpf) {
		$cpf = str_replace('-', '', $cpf);
		$cpf = str_replace('.', '', $cpf);

		return $cpf;
	}

	private function converterTelefone($telefone) {
		$ddd = substr($telefone, 1, 2);
		$numero = substr($telefone, 4, strlen($telefone));
		$numero = str_replace('-', '', $numero);

		$telefone = array();
		$telefone['ddd'] = $ddd;
		$telefone['numero'] = $numero;

		return $telefone;
	}

	private function converterCep($cep) {
		$cep = str_replace('-', '', $cep);

		return $cep;
	}

	private function getCodigoFormaPagamento() {
		return array(
			'CartaoCredito' => 1,
			'BoletoBancario' => 2,
			'DebitoBancario' => 3,
			'DebitoBancarioDinheiro' => 4,
			'DebitoBancarioCheque' => 5,
			'TransferenciaBancario' => 6,
			'SedexACobrar' => 7,
			'Cheque' => 8,
			'Dinheiro' => 9,
			'Financiamento' => 10,
			'Fatura' => 11,
			'Cupom' => 12,
			'Multicheque' => 13,
			'Outros' => 14
		);
	}
}
?>