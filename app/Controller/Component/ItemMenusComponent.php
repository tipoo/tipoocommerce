<?php
App::uses('Component', 'Controller');
class ItemMenusComponent extends Component {

	private $ItemMenu;
	private $Controller;

	public function initialize(Controller $controller) {
		parent::initialize($controller);

		$this->Controller = $controller;

		App::uses('ItemMenu', 'Model');
		$this->ItemMenu = new ItemMenu();

		App::uses('Menu', 'Model');
		$this->Menu = new Menu();
	}

	public function setMenu() {
		$menu_itens = $this->ItemMenu->find('threaded', array(
			'parent' => 'item_menu_pai_id',
			'conditions' => array('ItemMenu.ativo'),
			'order' => 'ordem'
		));

		$menus = $this->Menu->find('all', array(
			'conditions' => array(
				'Menu.ativo' => true
			)
		));

		$controleMenus = array();
		foreach ($menus as $menu) {

			foreach ($menu_itens  as $menu_item) {
				if ($menu_item['ItemMenu']['menu_id'] == $menu['Menu']['id']) {
					$controleMenus[] = $menu_item;
				}
			}
		}

		$this->Controller->set(compact('controleMenus'));
	}

}
?>