<?php
App::uses('RegrasPromocoesComponent', 'Controller/Component');

class RegrasPromocoesCompreJuntoComponent extends RegrasPromocoesComponent {

	public $Controller;
	public $Configuracao;
	public $AfinidadeProduto;
	public $CompreJuntoPromocao;

	public function initialize(Controller $controller) {
		parent::initialize($controller);

		$this->Controller = $controller;

		App::uses('Configuracao', 'Model');
		App::uses('AfinidadeProduto', 'Model');
		App::uses('CompreJuntoPromocao', 'Model');
		$this->Configuracao = new Configuracao();
		$this->AfinidadeProduto = new AfinidadeProduto();
		$this->CompreJuntoPromocao = new CompreJuntoPromocao();
	}

	public function definir_promocoes_skus($skus) {
		// Juntar os skus do mesmo produto e somando a quantidade de pecas no carrinho
		$produtos = array();
		foreach ($skus as $id => $sku) {
			if (!array_key_exists($sku['Produto']['id'], $produtos)) {
				$produtos[$sku['Produto']['id']] = array(
					'Produto' => $sku['Produto'],
					'Sku' => array($sku['Sku']['id'] => $sku),
					'qtd' => $sku['qtd'],
					'preco_unitario' => $sku['preco_unitario'],
					'preco_unitario_desconto' => isset($sku['preco_unitario_desconto']) ? $sku['preco_unitario_desconto'] : null,
					'total' => 0
				);
			} else {
				$qtd_atual = $produtos[$sku['Produto']['id']]['qtd'];
				$produtos[$sku['Produto']['id']]['Sku'][$sku['Sku']['id']] = $sku;
				$produtos[$sku['Produto']['id']]['qtd'] = $qtd_atual + $sku['qtd'];
			}
		}

		$produtos_alterados = array();
		foreach ($produtos as $produto_principal_id => $produto_principal) {
			$afinidade_produtos = $this->AfinidadeProduto->find('all', array(
				'contain' => false,
				'conditions' => array(
					'AfinidadeProduto.tipo' => 'CJ',
					'AfinidadeProduto.produto_principal_id' => $produto_principal_id,
					'AfinidadeProduto.pedido_sku_id is null',
					'AfinidadeProduto.ativo'
				)
			));

			if (count($afinidade_produtos) > 0) {
				$tabela_desconto = $this->tabela_desconto($produto_principal);

				if (count($tabela_desconto)) {
					$total = 0;
					$tem_desconto = false;
					for ($i = 1; $i <= $produto_principal['qtd']; $i++) {
						$qtd_compre_junto = 0;

						foreach ($produtos as $produto_secundario_id => $produto_secundario) {
							if ($produto_secundario_id != $produto_principal_id) {
								foreach ($afinidade_produtos as $afinidade_produto) {
									if ($afinidade_produto['AfinidadeProduto']['produto_secundario_id'] == $produto_secundario_id) {
										if ($produto_secundario['qtd'] >= $i) {
											if (!isset($produto_principal['AfinidadeProduto'])) {
												$produto_principal['AfinidadeProduto'] = array();
											}
											$produto_principal['AfinidadeProduto'][] = $afinidade_produto['AfinidadeProduto'];

											$qtd_compre_junto++;
										}

										break;
									}
								}
							}
						}

						if (count($tabela_desconto) && $qtd_compre_junto > 0) {

							if (count($tabela_desconto) >= $qtd_compre_junto) {
								$tabela_desconto[$qtd_compre_junto]['qtd_compre_junto'] = $qtd_compre_junto;
								if (!isset($produto_principal['CompreJuntoPromocao'])) {
									$produto_principal['CompreJuntoPromocao'] = array();
								}
								$produto_principal['CompreJuntoPromocao'][] = $tabela_desconto[$qtd_compre_junto];

								$total += $tabela_desconto[$qtd_compre_junto]['valor'];

							} else {
								$tabela_desconto[count($tabela_desconto)]['qtd_compre_junto'] = $qtd_compre_junto;
								if (!isset($produto_principal['CompreJuntoPromocao'])) {
									$produto_principal['CompreJuntoPromocao'] = array();
								}
								$produto_principal['CompreJuntoPromocao'][] = $tabela_desconto[count($tabela_desconto)];

								$total += $tabela_desconto[count($tabela_desconto)]['valor'];

							}
							$tem_desconto = true;

						} else {
							$total += isset($produto_principal['preco_unitario_desconto']) && !empty($produto_principal['preco_unitario_desconto']) ? $produto_principal['preco_unitario_desconto'] : $produto_principal['preco_unitario'];
						}
					}

					if ($tem_desconto) {
						$produto_principal['preco_unitario_desconto'] = $total / $produto_principal['qtd'];
						$produtos_alterados[] = $produto_principal;
					}

				}
			}
		}

		foreach ($produtos_alterados as $produto_alterado) {
			foreach ($produto_alterado['Sku'] as $sku_id => $sku) {
				$skus[$sku_id]['preco_unitario_desconto'] = $produto_alterado['preco_unitario_desconto'];
				if (!isset($skus[$sku_id]['Promocao'])) {
					$skus[$sku_id]['Promocao'] = array();
				}
				$skus[$sku_id]['Promocao']['melhor_compre_junto'] = array(
					'valor' => $skus[$sku_id]['preco_unitario_desconto'],
					'promocao' => array(
						'Promocao' => array(
							'descricao' => 'Promoção Compre Junto'
						),
						'CompreJuntoPromocao' => $produto_alterado['CompreJuntoPromocao'],
						'AfinidadeProduto' => $produto_alterado['AfinidadeProduto']
					)
				);
			}
		}

		return $skus;
	}

	public function tabela_desconto($produto_principal) {
		$compre_junto_promocoes = $this->CompreJuntoPromocao->find('all', array(
			'contain' => false,
			'conditions' => array(
				'CompreJuntoPromocao.produto_id' => $produto_principal['Produto']['id'],
				'CompreJuntoPromocao.pedido_sku_id is null',
				'CompreJuntoPromocao.ativo'
			)
		));

		$tabela_desconto = array();
		$count = 0;

		foreach ($compre_junto_promocoes as $compre_junto_promocao) {
			$count++;
			$preco = isset($produto_principal['preco_unitario_desconto']) && !empty($produto_principal['preco_unitario_desconto']) ? $produto_principal['preco_unitario_desconto'] : $produto_principal['preco_unitario'];
			$tabela_desconto[$count]['compre_junto_promocao'] = $compre_junto_promocao;
			$tabela_desconto[$count]['valor'] = $this->calcular_valor_total_monetario_com_desconto($preco, $compre_junto_promocao['CompreJuntoPromocao']['valor'], $compre_junto_promocao['CompreJuntoPromocao']['tipo_valor']);
		}

		$price = array();
		foreach ($tabela_desconto as $key => $row)
		{
			$price[$key] = $row['compre_junto_promocao']['CompreJuntoPromocao']['valor'];
		}
		array_multisort($price, SORT_ASC, $tabela_desconto);

		$count = count($tabela_desconto);
		for ($i = $count; $i > 0; $i--) {
			$tabela_desconto[$i] = $tabela_desconto[$i - 1];
		}
		unset($tabela_desconto[0]);

		return $tabela_desconto;
	}

}
?>