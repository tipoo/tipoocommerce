<?php 
App::uses('BasicAuthenticate', 'Controller/Component/Auth');

class ApiAuthenticate extends BasicAuthenticate {
	public function getUser($request) {
		$username = env('PHP_AUTH_USER');
		$pass = env('PHP_AUTH_PW');

		if (empty($username) || empty($pass)) {
			return false;
		}

		$config = ClassRegistry::init('Configuracao');
		
		if ($username == $config->get('api_user') && 
				$pass == $config->get('api_pass')) {

			return array('api_user' => $username);
		}

		return false;
	}	

}
?>