<?php
class PermissoesComponent extends Component {

	private $controller;

	public function initialize(Controller $controller) {
		parent::initialize($controller);
		$this->controller = $controller;
		$this->controller->loadModel('Objeto');
		$this->controller->loadModel('Permissao');
		$this->controller->loadModel('Perfil');

	}

	public function check($perfil_id, $controller, $action = 'index', $admin = false, $plugin = null) {
		if ($admin) {
			$action = 'admin_'.$action;
		}

		$objeto = $this->controller->Objeto->find('first', array(
			'conditions' => array(
				'ativo' => true,
				'plugin' => $plugin,
				'controller' => $controller,
				'action' => $action
			)
		));

		$count = $this->controller->Permissao->find('count', array(
			'conditions' => array(
				'perfil_id' => $perfil_id,
				'objeto_id' => $objeto['Objeto']['id']
			)
		));

		return (bool)$count;
	}
}
?>
