<?php
App::uses('RegrasPromocoesComponent', 'Controller/Component');

class RegrasPromocoesProdutoComponent extends RegrasPromocoesComponent {

	public function definir_promocoes_produto($produto) {
		$melhor_desconto_produto = $this->definir_melhor_promocao($produto['Produto'], $produto['Promocao']['desconto_produto']);

		if ($melhor_desconto_produto['promocao']) {
			$produto['Promocao']['melhor_desconto_produto'] = $melhor_desconto_produto;
			$produto['Produto'] = $this->recalcular_valores_produto($produto['Produto'], $produto['Promocao']['melhor_desconto_produto']);
		}

		return $produto;
	}

	public function definir_promocoes_skus($skus = array()) {
		foreach ($skus as $key => $sku) {
			$melhor_desconto_produto = $this->definir_melhor_promocao($sku['Produto'], $sku['Promocao']['desconto_produto']);

			if ($melhor_desconto_produto['promocao']) {
				$skus[$key]['Promocao']['melhor_desconto_produto'] = $melhor_desconto_produto;
			}
		}

		return $skus;
	}

	private function definir_melhor_promocao($produto, $promocoes = array()) {
		// Definindo a melhor promoção de desconto no produto, pois só uma delas é apresentada ao cliente
		$melhor_desconto_produto = array(
			'valor' => 0,
			'promocao' => false
		);

		foreach ($promocoes as $promocao) {
			$valor = $this->calcular_valor_total_monetario_com_desconto($produto['preco_por'], $promocao['PromocaoEfeito']['valor'], $promocao['PromocaoEfeito']['tipo_valor']);

			if (($melhor_desconto_produto['valor'] == 0 || $melhor_desconto_produto['valor'] > $valor) && $valor > 0) {
				$melhor_desconto_produto['valor'] = $valor;
				$melhor_desconto_produto['promocao'] = $promocao;
			}
		}

		return $melhor_desconto_produto;
	}

}
?>