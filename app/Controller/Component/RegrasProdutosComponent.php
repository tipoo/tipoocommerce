<?php
App::uses('Component', 'Controller');

class RegrasProdutosComponent extends Component {

	public $Controller;
	public $Configuracao;

	public $components = array(
		'Promocoes',
		'RegrasPromocoesProduto',
		'RegrasPromocoesPrecos'
	);

	public function initialize(Controller $controller) {
		parent::initialize($controller);

		$this->Controller = $controller;

		App::uses('Configuracao', 'Model');
		$this->Configuracao = new Configuracao();
	}

	public function aplicar_regras($produto, $qtd = null, $cep = null, $sub_total_carrinho = null, $qtd_pecas_carrinho = null, $cupom = null) {
		// Armazenando os valores originais antes de aplicar as promoções
		$produto['Produto']['preco_de_original'] = $produto['Produto']['preco_de'];
		$produto['Produto']['preco_por_original'] = $produto['Produto']['preco_por'];

		// Aplica as regras das promoções e adiciona os selos
		$produto = $this->Promocoes->aplicar_promocoes($produto, $qtd, $cep, $sub_total_carrinho, $qtd_pecas_carrinho, $cupom);

		// Definindo a melhor promoção de preço para esse produto
		$produto = $this->RegrasPromocoesProduto->definir_promocoes_produto($produto);

		// calculando os valores de parcelamento
		$parcelamento = $this->calcular_parcelamento_maximo($produto['Produto']['preco_por'], $this->Configuracao->get('parc_parcelas'), $this->Configuracao->get('parc_valor_min_parcela'), $this->Configuracao->get('parc_juros'));
		$produto['Produto']['parcelas'] = $parcelamento['parcelas'];
		$produto['Produto']['valor_parcelas'] = $parcelamento['valor_parcelas'];

		// Verificando se os SKUS estão disponíveis para a venda, pré-venda, ou apenas para avise-me quanto chegar
		// Caso todos os skus estejam sem estoque, o produto é tratado como esgotado
		$produto_esgotado = true;
		foreach ($produto['Sku'] as $key => $sku) {
			$produto['Sku'][$key]['situacao_estoque'] = $this->situacao_estoque_sku(array('Sku' => $sku));

			if (($produto['Sku'][$key]['situacao_estoque'] == 'Disponivel' || $produto['Sku'][$key]['situacao_estoque'] == 'PreVenda' ) && $produto_esgotado) {
				$produto_esgotado = false;
			}
		}
		$produto['Produto']['esgotado'] = $produto_esgotado;
		return $produto;
	}

	public function calcular_parcelamento_maximo($preco, $max_parcelas = 12, $valor_min_parcela = 0, $juros = 0) {
		/*
		M = C * (1 + i)t
		M = Montante
		C = Capital Inicial
		i = Taxa de juros
		t = Tempo

		M = ? (é o valor que queremos saber)
		C = R$ 4000,00
		i = 4% /100 = 0,04
		t = 5

		M = 4000 * (1 + 0,04)5
		M= 4000 * (1,04)5
		M= 4000 * 1,2165
		M= 4866
		*/

		$retorno = array(
			'parcelas' => 0,
			'valor_parcelas' => 0
		);

		$parcelas_atual = $max_parcelas;
		$valor_parcelas_atual = 0;
		$continuar = true;

		do {
			if ($juros > 0) {
				$preco_final = $preco * pow((1 + ($juros / 100)), $parcelas_atual);
			} else {
				$preco_final = $preco;
			}

			$valor_parcelas_atual = ($preco_final / $parcelas_atual);

			if ($valor_parcelas_atual >= $valor_min_parcela) {
				$retorno['parcelas'] = $parcelas_atual;
				$retorno['valor_parcelas'] = $valor_parcelas_atual;
				$continuar = false;

			} else if ($parcelas_atual === 1) {
				$retorno['parcelas'] = 1;
				$retorno['valor_parcelas'] = $preco;
				$continuar = false;
			}

			$parcelas_atual--;
		} while ($continuar);

		return $retorno;

	}

	public function situacao_estoque_sku($sku, $qtd_pretendida = 1) {
		if ($qtd_pretendida > $sku['Sku']['qtd_estoque']) {
			if ($sku['Sku']['permite_pre_venda']) {
				return 'PreVenda';
			} else {
				return 'AviseMe';
			}
		} else {
			return 'Disponivel';
		}
	}
}
?>