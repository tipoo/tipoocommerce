<?php
App::uses('Component', 'Controller');
App::uses('Produto', 'Model');

class ElasticSearchProdutoComponent extends Component {

	private $ElasticSearch;
	private $Produto;
	private $type = 'produto';
	private $contain = array(
		'contain' => array(
			'Marca',
			'Categoria',
			'TagsProduto'
		)
	);

	public function initialize(Controller $controller) {
		parent::initialize($controller);
		$this->Controller = $controller;

		$this->ElasticSearch = $this->Controller->ElasticSearch->getInstance();
		$this->ElasticSearch->setType($this->type);
		$this->Produto = new Produto();
		
	}

	public function indexar($id) {
		$produto = $this->Produto->find('first', array(
			$this->contain,
			'conditions' => array(
				'Produto.id' => $id,
				'Produto.ativo'
			)
		));

		if ($produto) {
			$doc = $this->criarDoc($produto);

			$this->ElasticSearch->setId($id);
			$this->ElasticSearch->setData($doc);
			$this->ElasticSearch->update();

			return true;
		}

		return false;
		
	}

	public function indexarTodos() {
		$produto = $this->Produto->find('all', array(
			$this->contain,
			'conditions' => array(
				'Produto.ativo'
			)
		));

		if ($produto) {
			$doc = $this->criarDoc($produto);

			$this->ElasticSearch->setId($id);
			$this->ElasticSearch->setData($doc);
			$this->ElasticSearch->update();

			return true;
		}

		return false;	
	}

	private function criarDoc($produto) {
		$doc = array(
			'id' => $produto['Produto']['id'],
			'descricao' => $produto['Produto']['descricao'],
			'sinonimos' => $produto['Produto']['sinonimos'],
			'categoria' => 'deded',
			'tags' => 'sdsd'
		);

		return $doc;
	}

	private function serializarCategorias() {

	}

	private function serializarTags() {

	}
}