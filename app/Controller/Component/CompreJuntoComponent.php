<?php
App::uses('BaseComponent', 'Controller/Component');

class CompreJuntoComponent extends BaseComponent {

	public $Controller;

	public function initialize(Controller $controller) {
		parent::initialize($controller);
		$this->load_models('Produto', 'CompreJuntoPromocao', 'AfinidadeProduto', 'Configuracao');
	}

	public function get($produto) {
		$compre_junto = array();

		$afinidade_produtos = $this->AfinidadeProduto->find('all', array(
			'contain' => array(
				'Produto.ativo'
			),
			'conditions' => array(
				'AfinidadeProduto.ativo',
				'AfinidadeProduto.pedido_sku_id is null',
				'AfinidadeProduto.tipo' => 'CJ',
				'AfinidadeProduto.produto_principal_id' => $produto['Produto']['id'],
				'Produto.ativo'
			)
		));

		// Adicionando o preço por como preço unitário do produto para efeitos de cáculo da promocao compre junto
		$produto['preco_unitario_desconto'] = $produto['Produto']['preco_por'];
		$produto['preco_unitario'] = $produto['Produto']['preco_por'];

		$tabela_desconto = $this->Controller->RegrasPromocoesCompreJunto->tabela_desconto($produto);

		$ids_todos = array();
		foreach ($afinidade_produtos as $key => $afinidade_produto) {
			$ids_todos[] = $afinidade_produto['AfinidadeProduto']['produto_secundario_id'];

			$produto_secundario = $this->get_produtos($afinidade_produto['AfinidadeProduto']['produto_secundario_id'], 'first');

			// verificando se tem desconto para o produto principal na condicao de compra junto
			if (count($tabela_desconto)) {
				$produto_principal_preco_com_desconto = $tabela_desconto[1]['valor'];

				// verificar se recíproca é verdadeira, ou seja, se o produto secundario tem desconto quando comprado junto com o produto pricipal
				$afinidade_produto_secundario = $this->AfinidadeProduto->find('all', array(
					'contain' => false,
					'conditions' => array(
						'AfinidadeProduto.ativo',
						'AfinidadeProduto.pedido_sku_id is null',
						'AfinidadeProduto.tipo' => 'CJ',
						'AfinidadeProduto.produto_principal_id' => $produto_secundario['Produto']['id'],
						'AfinidadeProduto.produto_secundario_id' => $produto['Produto']['id'],
					)
				));

				if ($afinidade_produto_secundario) {
					$tabela_desconto_secundario = $this->Controller->RegrasPromocoesCompreJunto->tabela_desconto($produto_secundario);

					// verificando se tem desconto para o produto secundario na condicao de compra junto com o produto principal
					if (count($tabela_desconto_secundario)) {
						$produto_secundario_preco_com_desconto = $tabela_desconto_secundario[1]['valor'];
					}
				} else {
					$produto_secundario_preco_com_desconto = $produto_secundario['Produto']['preco_por'];
				}

				$produto_principal_economize = ($produto['Produto']['preco_de'] != '' ? $produto['Produto']['preco_de'] : $produto['Produto']['preco_por']) - $produto_principal_preco_com_desconto;
				$produto_secundario_economize = ($produto_secundario['Produto']['preco_de'] != '' ? $produto_secundario['Produto']['preco_de'] : $produto_secundario['Produto']['preco_por']) - $produto_secundario_preco_com_desconto;

				$economize = $produto_principal_economize + $produto_secundario_economize;

				$produto_secundario['Produto']['preco_por'] = $produto_secundario_preco_com_desconto;
				$produto_secundario['Produto']['economize'] = $produto_secundario_economize;

				$preco_total_com_desconto = $produto_principal_preco_com_desconto + $produto_secundario_preco_com_desconto;
				$parcelamento = $this->Controller->RegrasProdutos->calcular_parcelamento_maximo($preco_total_com_desconto, $this->Configuracao->get('parc_parcelas'), $this->Controller->Configuracao->get('parc_valor_min_parcela'), $this->Configuracao->get('parc_juros'));

				// Passar preço para dentro do produto secundario, mas antes descobrir porque a promoção geral não está funcionando
				$compre_junto[] = array(
					'produtos_secundarios' => array($produto_secundario),
					'preco_total' => ($produto['Produto']['preco_de'] != '' ? $produto['Produto']['preco_de'] : $produto['Produto']['preco_por_original']) + ($produto_secundario['Produto']['preco_de'] != '' ? $produto_secundario['Produto']['preco_de'] : $produto_secundario['Produto']['preco_por_original']),
					'preco_total_com_desconto' => $preco_total_com_desconto,
					'parcelamento' => $parcelamento,
					'economize' => $economize,
					'produto_principal_por' => $produto_principal_preco_com_desconto,
					'produto_principal_economize' => $produto_principal_economize,
					'esgotado' => $produto_secundario['Produto']['esgotado'] // se o produto estiver esgotado não mostra o compre junto
				);
			}
		}

		// Faz o mesmo do loop acima para todos os produtos secundarios juntos se tiver mais de dois produtos secundarios
		if (count($ids_todos) >= 2) {
			$produtos_secundarios = $this->get_produtos($ids_todos, 'all');

			// verificando se tem desconto para o produto principal na condicao de compra junto
			if (count($tabela_desconto)) {

				// Verifica se há desconto para a quantidade de produtos secundarios, se não houver pega o desconto máximo cadastrado
				if (count($tabela_desconto) >= count($ids_todos)) {
					$preco_total_com_desconto = $tabela_desconto[count($ids_todos)]['valor'];
				} else {
					$preco_total_com_desconto = $tabela_desconto[count($tabela_desconto)]['valor'];
				}

				$produto_principal_preco_com_desconto = $preco_total_com_desconto;

				$preco_por_total_secundario = 0;

				$esgotado = false;
				foreach ($produtos_secundarios as $key_secundario => $produto_secundario) {
					// verifica produto esgotado
					if ($produto_secundario['Produto']['esgotado'] && !$esgotado) {
						$esgotado = true;
					}

					// verificar se recíproca é verdadeira, ou seja, se o produto secundario tem desconto quando comprado junto com o produto pricipal
					$ids_secundarios_secundario_in = $ids_todos;
					$ids_secundarios_secundario_in[] = $produto['Produto']['id'];
					$qtd_afinidade_produtos_secundario = $this->AfinidadeProduto->find('count', array(
						'contain' => false,
						'conditions' => array(
							'AfinidadeProduto.ativo',
							'AfinidadeProduto.pedido_sku_id is null',
							'AfinidadeProduto.tipo' => 'CJ',
							'AfinidadeProduto.produto_principal_id' => $produto_secundario['Produto']['id'],
							'AfinidadeProduto.produto_secundario_id' => $ids_secundarios_secundario_in,
						)
					));

					if ($qtd_afinidade_produtos_secundario) {
						$tabela_desconto_secundario = $this->Controller->RegrasPromocoesCompreJunto->tabela_desconto($produto_secundario);
						// verificando se tem desconto para o produto secundario na condicao de compra junto com o produto principal
						if (count($tabela_desconto_secundario)) {
							// Verifica se há desconto para a quantidade de produtos secundarios dos secundarios, se não houver pega o desconto máximo cadastrado
							if (count($tabela_desconto_secundario) >= $qtd_afinidade_produtos_secundario) {
								$preco_total_com_desconto += $tabela_desconto_secundario[$qtd_afinidade_produtos_secundario]['valor'];
								$produtos_secundarios[$key_secundario]['Produto']['preco_por'] = $tabela_desconto_secundario[$qtd_afinidade_produtos_secundario]['valor'];
								$produtos_secundarios[$key_secundario]['Produto']['economize'] = $produtos_secundarios[$key_secundario]['Produto']['preco_de'] - $tabela_desconto_secundario[$qtd_afinidade_produtos_secundario]['valor'];
							} else {
								$preco_total_com_desconto += $tabela_desconto_secundario[count($tabela_desconto_secundario)]['valor'];
								$produtos_secundarios[$key_secundario]['Produto']['preco_por'] = $tabela_desconto_secundario[count($tabela_desconto_secundario)]['valor'];
								$produtos_secundarios[$key_secundario]['Produto']['economize'] = $produtos_secundarios[$key_secundario]['Produto']['preco_de'] - $tabela_desconto_secundario[count($tabela_desconto_secundario)]['valor'];
							}
						} else {
							$preco_total_com_desconto += $produto_secundario['Produto']['preco_por'];
						}
					} else {
						$preco_total_com_desconto += $produto_secundario['Produto']['preco_por'];
					}

					$preco_por_total_secundario += ($produto_secundario['Produto']['preco_de'] != '' ? $produto_secundario['Produto']['preco_de'] : $produto_secundario['Produto']['preco_por_original']);

				}

				$produto_principal_economize = ($produto['Produto']['preco_de'] != '' ? $produto['Produto']['preco_de'] : $produto['Produto']['preco_por_original']) - $produto_principal_preco_com_desconto;

				$preco_total = ($produto['Produto']['preco_de'] != '' ? $produto['Produto']['preco_de'] : $produto['Produto']['preco_por_original']) + $preco_por_total_secundario;

				$economize = $preco_total - $preco_total_com_desconto;

				$parcelamento = $this->Controller->RegrasProdutos->calcular_parcelamento_maximo($preco_total_com_desconto, $this->Configuracao->get('parc_parcelas'), $this->Configuracao->get('parc_valor_min_parcela'), $this->Configuracao->get('parc_juros'));

				$compre_junto[] = array(
					'produtos_secundarios' => $produtos_secundarios,
					'preco_total' => $preco_total,
					'preco_total_com_desconto' => $preco_total_com_desconto,
					'parcelamento' => $parcelamento,
					'economize' => $economize,
					'produto_principal_por' => $produto_principal_preco_com_desconto,
					'produto_principal_economize' => $produto_principal_economize,
					'esgotado' => $esgotado // se no mínimo um produto estiver esgotado não mostra o compre junto
				);
			}

		}

		return $compre_junto;
	}

	private function get_produtos($ids, $tipo = 'all') {

		$produtos = $this->Produto->find($tipo, array(
			'contain' => array(
				'Marca' => array(
					'id',
					'descricao'
				),
				'Categoria' => array(
					'id',
					'descricao'
				),
				'TagsProduto.Tag.Slug',
				'Sku' => array(
					'CaracteristicasValoresSelecionado' => array(
						'CaracteristicasValor',
						'Caracteristica'
					),
					'conditions' => array(
						'Sku.ativo' => true
					),
					'order' => array(
						'ordem' => 'ASC',
						'descricao' => 'ASC'
					)
				),
				'Slug',
				'Imagem' => array(
					'conditions' => array(
						'Imagem.ativo' => true,
						'Imagem.produto_id !=' => ''
					),
					'order' => array(
						'Imagem.ordem' => 'ASC',
					)
				),
				'CaracteristicasValoresSelecionado' => array(
					'CaracteristicasValor',
					'Caracteristica'
				)
			),
			'conditions' => array(
				'Produto.id' => $ids,
				'Produto.ativo'
			)
		));

		// Adicionando o preço por como preço unitário do produto para efeitos de cáculo da promocao compre junto
		if ($tipo == 'all') {
			foreach ($produtos as $key => $produto) {
				$produtos[$key] = $this->Controller->RegrasProdutos->aplicar_regras($produto);
				$produtos[$key]['preco_unitario_desconto'] = $produtos[$key]['Produto']['preco_por'];
				$produtos[$key]['preco_unitario'] = $produtos[$key]['Produto']['preco_por'];
			}

		} else {
			$produtos = $this->Controller->RegrasProdutos->aplicar_regras($produtos);
			$produtos['preco_unitario_desconto'] = $produtos['Produto']['preco_por'];
			$produtos['preco_unitario'] = $produtos['Produto']['preco_por'];
		}

		return $produtos;
	}
}
?>