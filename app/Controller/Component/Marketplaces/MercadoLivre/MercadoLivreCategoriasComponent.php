<?php
class MercadoLivreCategoriasComponent extends MercadoLivreAuthComponent {

	public function initialize(Controller $controller) {
		parent::initialize($controller);
	}

	public function set_categorias() {

		$categorias = $this->meli->get('/sites/MLB/categories');

		$meli_categorias = array();
		foreach ($categorias['body'] as $categoria) {
			$meli_categorias[$categoria->id] = $categoria->name;
		}

		$this->Controller->set('mercado_livre_categorias', $meli_categorias);
	}

	public function get_sub_categorias($categoria_pai_id) {

		$sub_categorias = $this->meli->get('/categories/' . $categoria_pai_id);

		$meli_sub_categorias = array();
		$meli_sub_categorias['children_categories'] = array();
		foreach ($sub_categorias['body']->children_categories as $sub_categoria) {
			$meli_sub_categorias['children_categories'][$sub_categoria->id] = $sub_categoria->name;
		}

		$meli_sub_categorias['attribute_types'] = $sub_categorias['body']->attribute_types;

		return $meli_sub_categorias;
	}

	public function get_variacoes($categoria_id) {

		$meli_variacoes = $this->meli->get('/categories/' . $categoria_id . '/attributes');

		return $meli_variacoes['body'];
	}
}
?>