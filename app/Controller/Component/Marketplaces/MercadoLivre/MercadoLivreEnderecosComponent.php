<?php
class MercadoLivreEnderecosComponent extends MercadoLivreAuthComponent {

	public $Configuracao;
	public $Logradouro;
	public $Cidade;

	public function initialize(Controller $controller) {
		parent::initialize($controller);

		$this->load_models(
			'Configuracao',
			'Logradouro',
			'Cidade'
		);
	}

	public function get_params_entrega($meli_frete) {
		/*
		*	Se não vier endereço de entrega no pedido do Mercado Livre, pode ser que o comprador tenha combinado com o vendedor.
		*	Como é obrigatório na plataforma ter um endereço de entrega vinculado ao pedido, gravamos o endereço de origem da loja
		*	cadastrado nas configurações
		*/
		if (isset($meli_frete->id)) {
			$cep = $this->converter_cep($meli_frete->receiver_address->zip_code);
		} else {
			$cep = $this->converter_cep($this->Configuracao->get('frete_cep_origem'));
		}

		$cidade_id = $this->get_cidade_id($cep);

		$endereco = array(
			'Endereco' => array(
				'descricao' => 'Mercado Livre',
				'endereco' => $this->get_endereco($meli_frete),
				'numero' => $this->get_numero($meli_frete),
				'complemento' => $this->get_complemento($meli_frete),
				'bairro' => $this->get_bairro($meli_frete),
				'cidade_id' => $cidade_id,
				'cep' => $cep,
			)
		);

		return $endereco;
	}

	private function converter_cep($cep) {
		if (!strstr($cep, '-')) {
			$cep = substr($cep , 0, 5) . '-' . substr($cep, -3);
		}

		return $cep;
	}

	private function get_cidade_id($cep) {
		$logradouro = $this->Logradouro->find('first', array(
			'contain' => false,
			'conditions' => array('Logradouro.cep' => $cep)
		));

		if ($logradouro) {
			$cidade_id = $logradouro['Logradouro']['cidade_id'];
		} else {
			$cidade = $this->Cidade->find('first', array(
				'contain' => false,
				'conditions' => array('Cidade.cep' => $cep)
			));

			if ($cidade) {
				$cidade_id = $cidade['Cidade']['id'];
			} else {
				$cidade_id = null;
			}
		}

		return $cidade_id;
	}

	private function get_endereco($meli_frete) {
		if (isset($meli_frete->receiver_address->street_name)) {
			return $meli_frete->receiver_address->street_name;
		} else {
			return null;
		}
	}

	private function get_numero($meli_frete) {
		if (isset($meli_frete->receiver_address->street_number)) {
			return $meli_frete->receiver_address->street_number;
		} else {
			return null;
		}
	}

	private function get_complemento($meli_frete) {
		if (isset($meli_frete->receiver_address->comment)) {
			return $meli_frete->receiver_address->comment;
		} else {
			return null;
		}
	}

	private function get_bairro($meli_frete) {
		if (isset($meli_frete->receiver_address->neighborhood->name)) {
			return $meli_frete->receiver_address->neighborhood->name;
		} else {
			return null;
		}
	}
}
?>