<?php
class MercadoLivreFeedbackComponent extends MercadoLivreAuthComponent {

	public function initialize(Controller $controller) {
		parent::initialize($controller);
	}

	public function get($meli_pedido_id) {

		if ($this->validar_token()) {
			$meli_pedido = $this->meli->get('/orders/' . $meli_pedido_id, array('access_token' => $this->appAccessToken));

			if (isset($meli_pedido['body']->error)) {
				$this->Controller->MercadoLivreLogs->gravar_pedido($meli_pedido, $meli_pedido_id);
			} else {
				// Verificando se o feedback existe
				if ((isset($meli_pedido['body']->feedback->purchase) && $meli_pedido['body']->feedback->purchase) || (isset($meli_pedido['body']->feedback->sale) && $meli_pedido['body']->feedback->sale)) {

					if ($this->validar_token()) {
						$feedback = $this->meli->get('/orders/' . $meli_pedido_id . '/feedback', array('access_token' => $this->appAccessToken));

						if (isset($feedback['body']->error)) {
							$this->Controller->MercadoLivreLogs->gravar_pedido($feedback, $meli_pedido_id);
							return false;
						} else {
							$razoes = $this->get_all_reasons();

							if (isset($feedback['body']->purchase->reason)) {
								$feedback['body']->purchase->reason = $feedback['body']->purchase->reason ? $feedback['body']->purchase->reason : null;
							}

							if (isset($feedback['body']->sale->reason)) {
								$feedback['body']->sale->reason = $feedback['body']->sale->reason ? $feedback['body']->sale->reason : null;
							}

							return $feedback;
						}
					}
				} else {
					return true;
				}
			}
		}
	}

	public function set($pedido) {
		if ($pedido['Pedido']['mercado_livre_pedido_id'] != '') {
			$feedback = $this->get($pedido['Pedido']['mercado_livre_pedido_id']);
			if ($feedback) {

				$razoes = array();
				// Verificando se o status do pagamento está como reembolsado
				if ($this->validar_token()) {
					$meli_pedido = $this->meli->get('/orders/' . $pedido['Pedido']['mercado_livre_pedido_id'], array('access_token' => $this->appAccessToken));

					if (isset($meli_pedido['body']->error)) {
						$this->Controller->MercadoLivreLogs->gravar_pedido($meli_pedido, $pedido['Pedido']['mercado_livre_pedido_id']);
					} else {
						if ($meli_pedido['body']->payments[0]->status != 'refunded') {
							$razoes = $this->reasons();
						} else {
							$razoes = $this->_reasons();
						}
					}
				}

				if (isset($meli_pedido['body']->feedback->sale->id)) {
					$this->Controller->set('meli_feedback_sale_id', $meli_pedido['body']->feedback->sale->id);
				}

				if (isset($meli_pedido['body']->feedback->purchase->id)) {
					$this->Controller->set('meli_feedback_purchase_id', $meli_pedido['body']->feedback->purchase->id);
				}

				$this->Controller->set('meli_feedback', $feedback);
				$this->Controller->set('meli_razoes', $razoes);
			}
		}
	}

	public function enviar($feedback) {

		$params = array(
			'fulfilled' => $feedback['MercadoLivreFeedbackSale']['fulfilled'] ? true : false,
			'rating' => $feedback['MercadoLivreFeedbackSale']['rating'],
			'message' => $feedback['MercadoLivreFeedbackSale']['message']
		);

		if ($feedback['MercadoLivreFeedbackSale']['reason'] != '') {
			$params['reason'] = $feedback['MercadoLivreFeedbackSale']['reason'];
		}

		if ($feedback['MercadoLivreFeedbackSale']['fulfilled']) {
			$params['restock_item'] = false;
		} else {
			$params['restock_item'] = true;
		}

		if ($this->validar_token()) {
			$meli_feedback = $this->meli->post('/orders/' . $feedback['MercadoLivreFeedbackSale']['mercado_livre_pedido_id'] . '/feedback', $params, array('version' => '3.0', 'access_token' => $this->appAccessToken));

			if (isset($meli_feedback['body']->error)) {
				$this->Controller->MercadoLivreLogs->gravar_feedback($meli_feedback, $feedback['MercadoLivreFeedbackSale']['mercado_livre_pedido_id']);
				return false;
			} else {
				return true;
			}
		}
	}

	public function modificar($feedback) {
		$params = array(
			'fulfilled' => $feedback['MercadoLivreFeedbackSale']['fulfilled'] ? true : false,
			'rating' => $feedback['MercadoLivreFeedbackSale']['rating'],
			'message' => $feedback['MercadoLivreFeedbackSale']['message']
		);

		if ($feedback['MercadoLivreFeedbackSale']['reason'] != '') {
			$params['reason'] = $feedback['MercadoLivreFeedbackSale']['reason'];
		}

		if ($feedback['MercadoLivreFeedbackSale']['fulfilled']) {
			$params['restock_item'] = false;
		} else {
			$params['restock_item'] = true;
		}

		if ($this->validar_token()) {
			$meli_feedback = $this->meli->put('/orders/' . $feedback['MercadoLivreFeedbackSale']['mercado_livre_pedido_id'] . '/feedback/sale', $params, array('version' => '3.0', 'access_token' => $this->appAccessToken));

			if (isset($meli_feedback['body']->error)) {
				$this->Controller->MercadoLivreLogs->gravar_feedback($meli_feedback, $feedback['MercadoLivreFeedbackSale']['mercado_livre_pedido_id']);
				return false;
			} else {
				return true;
			}
		}
	}

	public function reply($feedback) {

		$params = array(
			'reply' => $feedback['MercadoLivreFeedbackReply']['message']
		);

		if ($this->validar_token()) {
			$meli_feedback = $this->meli->post('/feedback/' . $feedback['MercadoLivreFeedbackReply']['meli_feedback_purchase_id'] . '/reply', $params, array('version' => '3.0', 'access_token' => $this->appAccessToken));

			if (isset($meli_feedback['body']->error)) {
				$this->Controller->MercadoLivreLogs->gravar_feedback($meli_feedback, $feedback['MercadoLivreFeedbackReply']['meli_feedback_purchase_id']);
				return false;
			} else {
				return true;
			}
		}
	}

	// Reembolso automático
	public function reasons() {
		return array(
			'THEY_DIDNT_ANSWER' => 'O comprador não respondeu as minhas mensagens',
			'BUYER_REGRETS' => 'O comprador se arrependeu de comprá-lo',
			'SELLER_REGRETS' => 'Decidi não vendê-lo',
			'SELLER_OUT_OF_STOCK' => 'Fiquei sem estoque',
			'SELLER_DIDNT_TRY_TO_CONTACT_BUYER' => 'Não tentei entrar em contato com o comprador',
			'BUYER_NOT_ENOUGH_MONEY' => 'O comprador não tinha dinheiro',
			'THEY_NOT_HONORING_POLICIES' => 'O comprador não respeitou as políticas de envio e pagamento do produto',
			'OTHER_MY_RESPONSIBILITY' => 'Outros motivos por responsabilidade do vendedor',
			'OTHER_THEIR_RESPONSIBILITY' => 'Outros motivos por responsabilidade do comprador'
		);
	}

	// Se o comprador já foi reembolsado, para não fazer o reembolso
	public function _reasons() {
		return array(
			'THEY_DIDNT_ANSWER_AND_RETURNED_MONEY' => 'O comprador não respondeu as minhas mensagens',
			'BUYER_REGRETS_AND_RETURNED_MONEY' => 'O comprador se arrependeu de comprá-lo',
			'SELLER_REGRETS_AND_RETURNED_MONEY' => 'Decidi não vendê-lo',
			'SELLER_OUT_OF_STOCK_AND_RETURNED_MONEY' => 'Fiquei sem estoque',
			'SELLER_DIDNT_TRY_TO_CONTACT_BUYER_AND_RETURNED_MONEY' => 'Não tentei entrar em contato com o comprador',
			'BUYER_NOT_ENOUGH_MONEY_AND_RETURNED_MONEY' => 'O comprador não tinha dinheiro',
			'THEY_NOT_HONORING_POLICIES_AND_RETURNED_MONEY' => 'O comprador não respeitou as políticas de envio e pagamento do produto',
			'OTHER_MY_RESPONSIBILITY_AND_RETURNED_MONEY' => 'Outros motivos por responsabilidade do vendedor',
			'OTHER_THEIR_RESPONSIBILITY_AND_RETURNED_MONEY' => 'Outros motivos por responsabilidade do comprador'
		);
	}

	public function get_all_reasons() {
		return array(
			'THEY_DIDNT_ANSWER' => 'O comprador não respondeu as minhas mensagens',
			'BUYER_REGRETS' => 'O comprador se arrependeu de comprá-lo',
			'SELLER_REGRETS' => 'Decidi não vendê-lo',
			'SELLER_OUT_OF_STOCK' => 'Fiquei sem estoque',
			'SELLER_DIDNT_TRY_TO_CONTACT_BUYER' => 'Não tentei entrar em contato com o comprador',
			'BUYER_NOT_ENOUGH_MONEY' => 'O comprador não tinha dinheiro',
			'THEY_NOT_HONORING_POLICIES' => 'O comprador não respeitou as políticas de envio e pagamento do produto',
			'OTHER_MY_RESPONSIBILITY' => 'Outros motivos por responsabilidade do vendedor',
			'OTHER_THEIR_RESPONSIBILITY' => 'Outros motivos por responsabilidade do comprador',
			'I_COULDNT_ANSWER' => 'Nunca recebi o produto',
			'DESCRIPTION_DIDNT_MATCH_ARTICLE' => 'É diferente do que comprei/ Chegou com defeito',
			'BUYER_PAID_BUT_DID_NOT_RECEIVE' => 'É diferente do que comprei/ Chegou com defeito',
			'THEY_DIDNT_ANSWER_AND_RETURNED_MONEY' => 'O comprador não respondeu as minhas mensagens',
			'BUYER_REGRETS_AND_RETURNED_MONEY' => 'O comprador se arrependeu de comprá-lo',
			'SELLER_REGRETS_AND_RETURNED_MONEY' => 'Decidi não vendê-lo',
			'SELLER_OUT_OF_STOCK_AND_RETURNED_MONEY' => 'Fiquei sem estoque',
			'SELLER_DIDNT_TRY_TO_CONTACT_BUYER_AND_RETURNED_MONEY' => 'Não tentei entrar em contato com o comprador',
			'BUYER_NOT_ENOUGH_MONEY_AND_RETURNED_MONEY' => 'O comprador não tinha dinheiro',
			'THEY_NOT_HONORING_POLICIES_AND_RETURNED_MONEY' => 'O comprador não respeitou as políticas de envio e pagamento do produto',
			'OTHER_MY_RESPONSIBILITY_AND_RETURNED_MONEY' => 'Outros motivos por responsabilidade do vendedor',
			'OTHER_THEIR_RESPONSIBILITY_AND_RETURNED_MONEY' => 'Outros motivos por responsabilidade do comprador'
		);
	}
}
?>