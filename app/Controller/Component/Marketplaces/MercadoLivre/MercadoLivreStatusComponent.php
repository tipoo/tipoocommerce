<?php
class MercadoLivreStatusComponent extends MercadoLivreAuthComponent {

	public $StatusPedido;
	public $Pedido;

	public function initialize(Controller $controller) {
		parent::initialize($controller);

		$this->load_models(
			'StatusPedido',
			'Pedido'
		);
	}

	public function alterar_status($pedido, $meli_status_transacao, $meli_status_frete) {

		$status_transacao = $this->status_transacao($meli_status_transacao);
		$status_frete = $this->status_frete($meli_status_frete);

		if ($status_transacao || $status_frete) {

			$status_pedido = $this->get_status($status_transacao, $status_frete);

			/*
			** Virifica se o status em que a notificação foi recebida,
			** já não é o status atual na plataforma, para evitar loop infinito.
			*/
			if ($status_pedido && $status_pedido['StatusPedido']['id'] != $pedido['StatusPedido']['id']) {
				$this->Pedido->saveField('status_id', $status_pedido['StatusPedido']['id']);
				$this->Controller->RegrasStatus->status_alterado($this->Pedido->id);
			}
		}
	}

	public function alterar_meli_status($pedido) {
		$this->alterar_meli_status_frete($pedido);
	}

	public function alterar_meli_status_frete($pedido) {

		$status = $this->status_meli_frete($pedido['StatusPedido']);

		if ($status) {

			$meli_frete_id = $pedido['Pedido']['mercado_livre_frete_id'];
			$pedido_id = $pedido['Pedido']['id'];

			// Se não tem código frete id do Mercado Livre, encerra aqui
			if ($meli_frete_id == '') {
				return false;
			}

			/*
				TODO - ME1 (http://developers.mercadolibre.com/listing-with-me1)

				$params = array(
					'tracking_number' => 'TR1234567891',
					'service_id' => 1 // service_id = 11 se for outro serviço
				);
			*/

			$params = array(
				'status' => $status
			);

			if ($status == 'shipped') {
				$codigo_rastreamento = $pedido['Pedido']['codigo_rastreamento'];

				if ($codigo_rastreamento != '') {
					$params += array(
						'tracking_number' => $codigo_rastreamento
					);
				}
			}

			if ($this->validar_token()) {
				$meli_pedido = $this->meli->put('/shipments/' . $meli_frete_id, $params, array('access_token' => $this->appAccessToken));

				if (isset($meli_pedido['body']->error)) {
					$this->Controller->MercadoLivreLogs->gravar_pedido($meli_pedido, $pedido_id);
				}
			}
		}
	}

	private function get_status($status_transacao, $status_frete) {
		$conditions = array(
			'StatusPedido.ativo' => true
		);

		if ($status_transacao) {
			$conditions['StatusPedido.' . $status_transacao] = true;
		}

		if ($status_frete) {
			$conditions['StatusPedido.' . $status_frete] = true;
		}

		$status_pedido = $this->StatusPedido->find('first', array(
			'contain' => false,
			'conditions' => array(
				$conditions
			)
		));

		return $status_pedido;
	}

	/*
	** Status da plataforma
	*/

	private function status_transacao($meli_status_transacao) {

		switch ($meli_status_transacao) {
			case 'confirmed':
				$status = 'status_mercado_livre_confirmed';
				break;
			case 'payment_required':
				$status = 'status_mercado_livre_payment_required';
				break;
			case 'payment_in_process':
				$status = 'status_mercado_livre_payment_in_process';
				break;
			case 'paid':
				$status = 'status_mercado_livre_paid';
				break;
			case 'status_mercado_livre_cancelled':
				$status = 'status_cancelado';
				break;
			default:
				$status = false;
				break;
		}

		return $status;
	}

	private function status_frete($meli_status_frete) {

		switch ($meli_status_frete) {
			case 'pending':
				$status = 'status_mercado_livre_frete_pending';
				break;
			case 'ready_top_ship':
				$status = 'status_mercado_livre_frete_ready_top_ship';
				break;
			case 'shipped':
				$status = 'status_mercado_livre_frete_shipped';
				break;
			case 'delivered':
				$status = 'status_mercado_livre_frete_delivered';
				break;
			case 'not_delivered':
				$status = 'status_mercado_livre_frete_not_delivered';
				break;
			case 'cancelled':
				$status = 'status_mercado_livre_frete_cancelled';
				break;
			default:
				$status = false;
				break;
		}

		return $status;
	}

	/*
	** Status do Mercado Livre
	*/

	private function status_meli_frete($status_frete) {
		$status = false;

		if ($status_frete['status_mercado_livre_frete_ready_top_ship']) {
			$status = 'ready_top_ship';
		} else if ($status_frete['status_mercado_livre_frete_shipped']) {
			$status = 'shipped';
		} else if ($status_frete['status_mercado_livre_frete_delivered']) {
			$status = 'delivered';
		} else if ($status_frete['status_mercado_livre_frete_not_delivered']) {
			$status = 'not_delivered';
		} else if ($status_frete['status_mercado_livre_frete_cancelled']) {
			$status = 'cancelled';
		}

		return $status;
	}
}
?>