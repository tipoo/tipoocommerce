<?php
class MercadoLivreProdutosComponent extends MercadoLivreAuthComponent {

	public $Colecao;
	public $Produto;

	public function initialize(Controller $controller) {
		parent::initialize($controller);

		$this->load_models(
			'Colecao',
			'Produto'
		);
	}

	public function adicionar_produtos() {
		$error = false;
		$produtos = $this->get_produtos();

		foreach ($produtos as $produto) {
			if ($produto['Produto']['mercado_livre_produto_id'] == '') {

				$params = $this->get_params_add($produto);
				if ($this->validar_produto($params, $produto['Produto']['id'])) {
					if ($this->validar_token()) {

						$meli_produto = $this->meli->post('/items', $params, array('access_token' => $this->appAccessToken));

						if (!$this->salvar_meli_produto_id($meli_produto, $produto)) {
							$error = true;
						}
					}
				} else {
					$error = true;
				}
			}
		}

		if ($error) {
			$this->Controller->Session->setFlash('<strong>ATENÇÃO!</strong> Ocorreu um erro ao tentar integrar 1 ou mais produtos. Por favor, informe o suporte técnico.', FLASH_ERROR);
		} else {
			$this->Controller->Session->setFlash('Integração realizada com sucesso.', FLASH_SUCCESS);
		}
	}

	public function editar_produto($produto_id) {

		$produto = $this->get_produto($produto_id);

		// Verifica se o produto faz parte da coleção do Mercado Livre
		if (!$produto) {
			return false;
		}

		$meli_produto = $this->get_meli_produto($produto['Produto']['mercado_livre_produto_id']);

		if ($this->verificar_status($meli_produto) && $this->verificar_vendas($meli_produto)) {
			if ($this->validar_token()) {
				$params = $this->get_params_editar($produto);

				$meli_produto = $this->meli->put('/items/' . $produto['Produto']['mercado_livre_produto_id'], $params, array('access_token' => $this->appAccessToken));

				if (!isset($meli_produto['body']->error)) {
					// Editar descrição
					$descricao = array('text' => $produto['Produto']['detalhes']);
					if ($produto['Produto']['detalhes'] == '') {
						$descricao = array('text' => ' ');
					}

					$meli_produto = $this->meli->put('/items/' . $produto['Produto']['mercado_livre_produto_id'] . '/description', $descricao, array('access_token' => $this->appAccessToken));
					if (isset($meli_produto['body']->error)) {
						$this->Controller->MercadoLivreLogs->gravar_produto($meli_produto, $params, $produto['Produto']['id']);
						$this->Controller->Session->setFlash('<strong>ATENÇÃO!</strong> Ocorreu um erro ao tentar fazer a edição no Mercado Livre. Por favor, informe o suporte técnico.', FLASH_ERROR);
					}
				} else {
					$this->Controller->MercadoLivreLogs->gravar_produto($meli_produto, $params, $produto['Produto']['id']);
					$this->Controller->Session->setFlash('<strong>ATENÇÃO!</strong> Ocorreu um erro ao tentar fazer a edição no Mercado Livre. Por favor, informe o suporte técnico.', FLASH_ERROR);
				}
			}
		} else {
			$this->Controller->Session->setFlash('<strong>ATENÇÃO!</strong> Este produto encontra-se inativo OU posssui vendas. O que impossibilitou a edição do mesmo no Mercado Livre.', FLASH_WARNING);
		}
	}

	public function get_meli_produto($produto_mercado_livre_id) {
		$meli_produto = $this->meli->get('/items/' . $produto_mercado_livre_id);

		return $meli_produto;
	}

	public function get_produtos() {
		$produtos = $this->Colecao->find('first', array(
			'contain' => array(
				'ColecoesProduto.Produto' => array(
					'Categoria',
					'Imagem' => array(
						'order' => array(
							'Imagem.ordem' => 'ASC'
						)
					),
					'Sku' => array(
							'CaracteristicasValoresSelecionado' => array(
							'Caracteristica',
							'CaracteristicasValor'
						),
						'order' => array(
							'Sku.ordem' => 'ASC'
						)
					)
				)
			),
			'conditions' => array(
				'Colecao.id' => $this->Configuracao->get('mercado_livre_colecao_id')
			)
		));

		return $produtos['ColecoesProduto'];
	}

	public function get_produto($produto_id) {
		$produto = $this->Produto->find('first', array(
			'contain' => array(
				'Imagem' => array(
					'order' => array(
						'Imagem.ordem' => 'ASC'
					)
				),
				'Sku' => array(
						'CaracteristicasValoresSelecionado' => array(
						'Caracteristica',
						'CaracteristicasValor'
					),
					'order' => array(
						'Sku.ordem' => 'ASC'
					)
				),
				'ColecoesProduto' => array(
					'conditions' => array(
						'ColecoesProduto.colecao_id' => $this->Configuracao->get('mercado_livre_colecao_id')
					)
				)
			),
			'conditions' => array(
				'Produto.id' => $produto_id
			)
		));

		if (count($produto['ColecoesProduto']) > 0 && $produto['Produto']['mercado_livre_produto_id'] != '') {
			return $produto;
		} else {
			return false;
		}
	}

	public function verificar_status($meli_produto) {
		if ($meli_produto['body']->status == 'active') {
			return true;
		} else {
			return false;
		}
	}

	public function verificar_vendas($meli_produto) {
		if ($meli_produto['body']->sold_quantity == 0) {
			return true;
		} else {
			return false;
		}
	}

	private function get_params_add($produto) {

		$params = array(
			'title' => $produto['Produto']['descricao'],
			'category_id' => $produto['Produto']['Categoria']['mercado_livre_categoria_id'],
			'price' => $produto['Produto']['preco_por'],
			'currency_id' => 'BRL',
			'buying_mode' => 'buy_it_now',
			'listing_type_id' => 'bronze', // free, bronze, silver, gold, gold_special, gold_premium e gold_pro
			'condition' => 'new',
			'description' => $produto['Produto']['detalhes'],
			'accepts_mercadopago' => true
			// 'video_id' => 'YOUTUBE_ID_HERE',
			// 'warranty' => '12 month by Ray Ban',
		);

		// Imagens - Só pode mandar no máximo 6 imagens
		$count_img = 1;
		foreach ($produto['Produto']['Imagem'] as $imagem) {
			$params['pictures'][] = array(
				'source' => Router::url('/files/' . PROJETO . '/produtos/' . $imagem['imagem'], true)
			);

			if ($count_img >= 6) {
				break;
			}

			$count_img++;
		}

		// Skus
		foreach ($produto['Produto']['Sku'] as $sku_key => $sku) {
			foreach ($sku['CaracteristicasValoresSelecionado'] as $caracteristica) {
				if ($caracteristica['Caracteristica']['mercado_livre_caracteristica_id'] != '' && is_numeric($caracteristica['Caracteristica']['mercado_livre_caracteristica_id'])) {
					$params['variations'][$sku_key]['attribute_combinations'][] = array(
						'id' => $caracteristica['Caracteristica']['mercado_livre_caracteristica_id'],
						'value_id' => $caracteristica['CaracteristicasValor']['mercado_livre_caracteristicas_valor_id']
					);
				}
			}

			if (isset($params['variations'])) {
				$params['variations'][$sku_key]['picture_ids'] = array(
					Router::url('/files/' . PROJETO . '/produtos/' . $produto['Produto']['Imagem']['0']['imagem'], true)
				);
				$params['variations'][$sku_key]['price'] = $produto['Produto']['preco_por'];
				$params['variations'][$sku_key]['available_quantity'] = (int)$sku['qtd_estoque'];
			} else {
				$params['available_quantity'] = (int)$sku['qtd_estoque'];
			}
		}

		// Frete - O primeiro sku serve para buscarmos a medida
		$params += $this->Controller->MercadoLivreFrete->get_params($produto['Produto']['Categoria']['mercado_livre_categoria_id'], $produto['Produto']['Sku']['0']);

		return $params;
	}

	private function get_params_editar($produto) {

		$params = array(
			'title' => $produto['Produto']['descricao'], // Não pode editar após ter uma peça vendida
			// 'video_id' => 'YOUTUBE_ID_HERE',
			// 'category_id' => Não pode ser modificada,
			// 'listing_type_id' => Só pode ser modificado 1 vez para fazer upgrade apenas ( free, bronze, silver, gold, gold_special, gold_premium e gold_pro)
			// 'condition' => Não pode editar após ter uma peça vendida
			// 'warranty' => Não pode editar após ter uma peça vendida
			'accepts_mercadopago' => true
		);

		// Imagens - Só pode mandar no máximo 6 imagens
		$count_img = 1;
		foreach ($produto['Imagem'] as $imagem) {
			$params['pictures'][] = array(
				'source' => Router::url('/files/' . PROJETO . '/produtos/' . $imagem['imagem'], true)
			);

			if ($count_img >= 6) {
				break;
			}

			$count_img++;
		}

		// Skus
		foreach ($produto['Sku'] as $sku_key => $sku) {
			$variacao = false;
			foreach ($sku['CaracteristicasValoresSelecionado'] as $caracteristica) {
				if ($caracteristica['Caracteristica']['mercado_livre_caracteristica_id'] != '' && is_numeric($caracteristica['Caracteristica']['mercado_livre_caracteristica_id'])) {
					$variacao = true;

					$params['variations'][$sku_key]['attribute_combinations'][] = array(
						'id' => $caracteristica['Caracteristica']['mercado_livre_caracteristica_id'],
						'value_id' => $caracteristica['CaracteristicasValor']['mercado_livre_caracteristicas_valor_id']
					);
				}
			}

			if ($variacao) {
				$params['variations'][$sku_key]['id'] = $sku['mercado_livre_variacao_id'];
				$params['variations'][$sku_key]['price'] = $produto['Produto']['preco_por'];
				$params['variations'][$sku_key]['available_quantity'] = (int)$sku['qtd_estoque'];
				$params['variations'][$sku_key]['picture_ids'] = array(
					Router::url('/files/' . PROJETO . '/produtos/' . $produto['Imagem']['0']['imagem'], true)
				);
			} else {
				$params['price'] = $produto['Produto']['preco_por'];
				$params['available_quantity'] = (int)$sku['qtd_estoque'];
			}
		}

		return $params;
	}

	private function validar_produto($params, $produto_id) {
		if ($this->validar_token()) {
			$validate = $this->meli->post('/items/validate', $params, array('access_token' => $this->appAccessToken));

			if (isset($validate['body']->error)) {
				$this->Controller->MercadoLivreLogs->gravar_produto($validate, $params, $produto_id);

				return false;
			} else {
				return true;
			}
		}
	}

	private function salvar_meli_produto_id($meli_produto, $produto) {
		$this->Produto->id = $produto['Produto']['id'];

		$data['Produto'] = array(
			'id' => $produto['Produto']['id'],
			'mercado_livre_produto_id' => $meli_produto['body']->id
		);

		$i = 0;
		foreach ($produto['Produto']['Sku'] as $sku) {
			if (isset($meli_produto['body']->variations[$i]->id)) {
				$data['Sku'][] = array(
					'id' => $sku['id'],
					'mercado_livre_variacao_id' => $meli_produto['body']->variations[$i]->id
				);
			}

			$i++;
		}

		if ($this->Produto->saveAll($data)) {
			return true;
		} else {
			return false;
		}
	}

// 	public function relist($produto_id) {
// 		$produto = $this->MercadoLivreProdutos->get_produto($produto_id);
// 		$params = $this->get_params_relist($produto);

// 		if ($this->validar_token()) {
// 			$meli_produto = $this->meli->post('/items/' . $produto['Produto']['mercado_livre_produto_id'] . '/relist', $params, array('access_token' => $this->appAccessToken));

// $this->log($meli_produto, LOG_DEBUG);
// 			if (!isset($meli_produto['body']->error)) {
// 				// Ok
// 			} else {
// 				$this->Controller->MercadoLivreLogs->gravar_produto($meli_produto, $params, $produto['Produto']['id']);
// 				$this->Controller->Session->setFlash('<strong>ATENÇÃO!</strong> Ocorreu um erro ao tentar relistar o produto no Mercado Livre. Por favor, informe o suporte técnico.', FLASH_ERROR);
// 			}
// 		}
// 	}

// 	private function get_params_relist($produto) {

// 		$params = array(
// 			'listing_type_id' => 'bronze', // free, bronze, silver, gold, gold_special, gold_premium e gold_pro
// 		);

// 		// Skus
// 		foreach ($produto['Sku'] as $sku_key => $sku) {
// 			$variacao = false;
// 			foreach ($sku['CaracteristicasValoresSelecionado'] as $caracteristica) {
// 				if ($caracteristica['Caracteristica']['mercado_livre_caracteristica_id'] != '' && is_numeric($caracteristica['Caracteristica']['mercado_livre_caracteristica_id'])) {
// 					$variacao = true;
// 				}
// 			}

// 			if ($variacao) {
// 				$params['variations'][$sku_key]['id'] = $sku['mercado_livre_variacao_id'];
// 				$params['variations'][$sku_key]['price'] = (float)$produto['Produto']['preco_por'];
// 				$params['variations'][$sku_key]['quantity'] = (int)$sku['qtd_estoque'];
// 			} else {
// 				$params['price'] = (float)$produto['Produto']['preco_por'];
// 				$params['quantity'] = (int)$sku['qtd_estoque'];
// 			}
// 		}

// $this->log($params, LOG_DEBUG);

// 		return $params;
// 	}
}
?>