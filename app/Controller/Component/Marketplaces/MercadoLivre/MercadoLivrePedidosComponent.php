<?php
class MercadoLivrePedidosComponent extends MercadoLivreAuthComponent {

	public $Produto;
	public $Sku;
	public $StatusPedido;
	public $Pedido;

	public function initialize(Controller $controller) {
		parent::initialize($controller);

		$this->load_models(
			'Produto',
			'Sku',
			'StatusPedido',
			'Pedido'
		);
	}

	public function get_meli_pedido($resource = '/orders/915642537') { //899074876, 897949788, 900227068, 906450661, 913950451, 914350959

		if ($this->validar_token()) {
			$meli_pedido = $this->meli->get($resource, array('access_token' => $this->appAccessToken));

			if (isset($meli_pedido['body']->error)) {
				$this->Controller->MercadoLivreLogs->gravar_pedido($meli_pedido, $resource);
			} else {

				if (!$this->verificar_pedido_existente($meli_pedido['body']->id)) {

					// Se o pedido é novo, o cliente é bloqueado temporariamente para evitar que ele faça varias vezes a mesma compra
					$this->Controller->MercadoLivreClientes->bloquear($meli_pedido['body']->seller, $meli_pedido['body']->buyer);
					$this->Controller->MercadoLivreClientes->desbloquear($meli_pedido['body']->seller, $meli_pedido['body']->buyer);

					$pedido = $this->get_params($meli_pedido);

					if (!$this->salvar_novo_pedido($pedido) && !$pedido) {
						$this->Controller->Session->setFlash('<strong>ATENÇÃO!</strong> Ocorreu um erro ao tentar salvar o pedido em nossa base. Por favor, informe o suporte técnico.', FLASH_ERROR);
					}

				} else {
					$pedido = $this->get_pedido($meli_pedido['body']->id);

					$this->Controller->MercadoLivreStatus->alterar_status($pedido, $meli_pedido['body']->status, $meli_pedido['body']->shipping->status);
				}
			}
		}
	}

	private function verificar_pedido_existente($meli_pedido_id) {
		$pedido = $this->get_pedido($meli_pedido_id);

		if ($pedido) {
			return true;
		} else {
			return false;
		}
	}

	private function get_pedido($meli_pedido_id) {
		$pedido = $this->Pedido->find('first', array(
			'contain' => array(
				'StatusPedido'
			),
			'conditions' => array(
				'Pedido.mercado_livre_pedido_id' => $meli_pedido_id
			)
		));

		return $pedido;
	}

	private function salvar_novo_pedido($pedido) {
		$this->Pedido->create();

		if ($this->Pedido->saveAssociated($pedido, array('deep' => true))) {
			$this->Controller->RegrasStatus->status_alterado($this->Pedido->id);

			return true;
		} else {
			return false;
		}
	}

	private function get_params($meli_pedido) {
		$status_id = $this->StatusPedido->field('StatusPedido.id', array('StatusPedido.status_mercado_livre_confirmed'));

		// Pedido
		$pedido = array(
			'Pedido' => array(
				'status_id' => $status_id,
				'data_hora' => date('d/m/Y H:i:s', strtotime($meli_pedido['body']->date_created)),
				'forma_pagamento' => 'MercadoPago',
				'valor_produtos' => $meli_pedido['body']->total_amount,
				'valor_total' => $meli_pedido['body']->total_amount_with_shipping,
				'valor_servicos' => 0,
				'valor_desconto' => 0,
				'mercado_livre_pedido_id' => $meli_pedido['body']->id
			)
		);

		$meli_frete = $meli_pedido['body']->shipping;

		// IMPORTANTE - O "=" está atribuindo o retorno da função à variável
		if ($meli_frete_id = $this->Controller->MercadoLivreFrete->get_meli_frete_id($meli_frete)) {
			$meli_frete = $this->Controller->MercadoLivreFrete->get_meli_frete($meli_frete_id);

			if (!$meli_frete) {
				return false;
			}
		}

		// Frete
		$pedido['Pedido'] += $this->Controller->MercadoLivreFrete->get_params_pedido($meli_frete);

		// Endereço de Entrega
		$pedido += $this->Controller->MercadoLivreEnderecos->get_params_entrega($meli_frete);

		// Cliente
		$pedido += $this->Controller->MercadoLivreClientes->get_params_pedido($meli_pedido['body']->buyer);

		// Skus
		$i = 0;

		foreach ($meli_pedido['body']->order_items as $meli_produto) {
			if (isset($meli_produto->item->variation_id) && $meli_produto->item->variation_id != '') {

				$sku = $this->Sku->find('first', array(
					'contain' => array(
						'Produto' => array(
							'Marca',
							'CaracteristicasValoresSelecionado' => array(
								'CaracteristicasValor',
								'Caracteristica'
							)
						),
						'CaracteristicasValoresSelecionado' => array(
							'CaracteristicasValor',
							'Caracteristica'
						),
					),
					'conditions' => array(
						'Sku.mercado_livre_variacao_id' => $meli_produto->item->variation_id
					)
				));

			} else {

				$sku = $this->Sku->find('first', array(
					'contain' => array(
						'Produto' => array(
							'Marca',
							'CaracteristicasValoresSelecionado' => array(
								'CaracteristicasValor',
								'Caracteristica'
							)
						),
						'CaracteristicasValoresSelecionado' => array(
							'CaracteristicasValor',
							'Caracteristica'
						),
					),
					'conditions' => array(
						'Produto.mercado_livre_produto_id' => $meli_produto->item->id
					)
				));
			}

			$pedido['PedidoSku'][$i] = array(
				'sku_id' => $sku['Sku']['id'],
				'mercado_livre_variacao_id' => $sku['Sku']['mercado_livre_variacao_id'],
				'mercado_livre_produto_id' => $sku['Produto']['mercado_livre_produto_id'],
				'sku' => $sku['Sku']['sku'],
				'descricao_marca' => $sku['Produto']['Marca']['descricao'],
				'descricao_produto' => $sku['Produto']['descricao'],
				'descricao_sku' => $sku['Sku']['descricao'],
				'preco_unitario' => $meli_produto->unit_price,
				'preco_total' => $meli_produto->unit_price * $meli_produto->quantity,
				'peso_unitario' => $sku['Sku']['peso'],
				'peso_total' => $sku['Sku']['peso'] * $meli_produto->quantity,
				'largura_unitario' => $sku['Sku']['largura'],
				'altura_unitario' => $sku['Sku']['altura'],
				'profundidade_unitario' => $sku['Sku']['profundidade'],
				'qtd' => $meli_produto->quantity,
				'situacao_estoque' => 'Reserva'
			);

			// Removendo do estoque os itens que estão entrando em reserva
			$nova_qtd = $sku['Sku']['qtd_estoque'] - $meli_produto->quantity;
			$this->Sku->id = $sku['Sku']['id'];
			$this->Sku->saveField('qtd_estoque', $nova_qtd);

			// Características do produto
			if (count($sku['Produto']['CaracteristicasValoresSelecionado'])) {
				foreach ($sku['Produto']['CaracteristicasValoresSelecionado'] as $caracteristica) {
					$valor = '';
					if ($caracteristica['Caracteristica']['tipo'] == 'T') {
						if (isset($caracteristica['descricao'])) {
							$valor = $caracteristica['descricao'];
						}
					} else if ($caracteristica['Caracteristica']['tipo'] == 'TG') {
						if (isset($caracteristica['descricao_grande'])) {
							$valor = $caracteristica['descricao_grande'];
						}
					} else {
						if (isset($caracteristica['CaracteristicasValor']['descricao'])) {
							$valor = $caracteristica['CaracteristicasValor']['descricao'];
						}
					}
					$pedido['PedidoSku'][$i]['PedidoCaracteristica'][] = array(
						'referente' => 'P',
						'caracteristica' => $caracteristica['Caracteristica']['descricao'],
						'valor' => $valor
					);
				}
			}

			// Características do sku
			if (count($sku['CaracteristicasValoresSelecionado'])) {
				foreach ($sku['CaracteristicasValoresSelecionado'] as $caracteristica) {
					$valor = '';
					if ($caracteristica['Caracteristica']['tipo'] == 'T') {
						if (isset($caracteristica['descricao'])) {
							$valor = $caracteristica['descricao'];
						}
					} else if ($caracteristica['Caracteristica']['tipo'] == 'TG') {
						if (isset($caracteristica['descricao_grande'])) {
							$valor = $caracteristica['descricao_grande'];
						}
					} else {
						if (isset($caracteristica['CaracteristicasValor']['descricao'])) {
							$valor = $caracteristica['CaracteristicasValor']['descricao'];
						}

						if (isset($caracteristica['CaracteristicasValor']['mercado_livre_caracteristicas_valor_descricao'])) {
							$valor = $caracteristica['CaracteristicasValor']['mercado_livre_caracteristicas_valor_descricao'];
						}
					}

					$pedido['PedidoSku'][$i]['PedidoCaracteristica'][] = array(
						'referente' => 'S',
						'caracteristica' => $caracteristica['Caracteristica']['descricao'] != '' ? $caracteristica['Caracteristica']['descricao'] : $caracteristica['Caracteristica']['mercado_livre_caracteristica_descricao'],
						'valor' => $valor
					);
				}
			}

			$i++;
		}

		return $pedido;
	}
}
?>