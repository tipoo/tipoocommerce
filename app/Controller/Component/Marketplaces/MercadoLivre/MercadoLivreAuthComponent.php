<?php
App::uses('BaseComponent', 'Controller/Component');

class MercadoLivreAuthComponent extends BaseComponent {

	public $Controller;
	public $Configuracao;

	private $appId;
	private $appSecret;
	private $appRedirectUrl;
	private $appTokenExpiresIn;
	private $appRefreshToken;

	protected $appAccessToken;
	protected $meli;

	public function initialize(Controller $controller) {
		parent::initialize($controller);
		$this->Controller = $controller;

		App::import(
			'Vendor',
			'Meli',
			array('file' => 'MercadoLivre' . DS . 'MercadoLivre' . DS . 'meli.php')
		);

		$this->load_models(
			'Configuracao'
		);

		$this->set_default_app();
	}

	public function login() {

		if ((isset($_GET['code']) && $_GET['code']) || !empty($this->appAccessToken)) {

			if(isset($_GET['code']) && $_GET['code'] && empty($this->appAccessToken)) {
				$oAuth = $this->meli->authorize($_GET['code'], $this->appRedirectUrl);

				if (!isset($oAuth['body']->error)) {
					$this->salvar_token($oAuth['body']->access_token, time() + $oAuth['body']->expires_in, $oAuth['body']->refresh_token);
				} else {
					$this->Controller->Session->setFlash($oAuth['body']->message, FLASH_ERROR);
				}
			}
		} else {
			$meliRedirectUrl = $this->meli->getAuthUrl($this->appRedirectUrl);
			$this->Controller->set('meliRedirectUrl', $meliRedirectUrl);
		}
	}

	protected function validar_token() {
		if (!empty($this->appAccessToken)) {
			if($this->appTokenExpiresIn < time()) {
				try {
					$refresh = $this->meli->refreshAccessToken();
					$this->salvar_token($refresh['body']->access_token, time() + $refresh['body']->expires_in, $refresh['body']->refresh_token);
				} catch (Exception $e) {
					$this->Controller->Session->setFlash("Exception: ",  $e->getMessage(), "\n", FLASH_ERROR);
				}
			}

			return true;
		} else {
			$this->Controller->Session->setFlash('<strong>ATENÇÃO!</strong> Vá em "Configurações > Marketplace" e autorize o app do Mercado Livre', FLASH_ERROR);
			return false;
		}
	}

	private function set_default_app() {
		// App
		$this->appId = $this->Configuracao->get('mercado_livre_app_id');
		$this->appSecret = $this->Configuracao->get('mercado_livre_secret_key');
		$this->appRedirectUrl = $this->Configuracao->get('mercado_livre_redirect_url');

		// Token
		$this->appAccessToken = $this->Configuracao->get('mercado_livre_access_token');
		$this->appTokenExpiresIn = $this->Configuracao->get('mercado_livre_token_expires_in');
		$this->appRefreshToken = $this->Configuracao->get('mercado_livre_refresh_token');

		$this->meli = new Meli($this->appId, $this->appSecret, $this->appAccessToken, $this->appRefreshToken);
	}

	private function refresh_default_app() {
		$this->Configuracao = new Configuracao();

		$this->set_default_app();
	}

	private function salvar_token($token, $token_expires_in, $refresh_token) {
		$this->Configuracao->id = $this->Configuracao->get('id');

		$data = array(
			'mercado_livre_access_token' => $token,
			'mercado_livre_token_expires_in' => $token_expires_in,
			'mercado_livre_refresh_token' => $refresh_token
		);

		if ($this->Configuracao->save($data)) {
			$this->refresh_default_app();
		} else {
			$this->Controller->Session->setFlash('<strong>ATENÇÃO!</strong> Ocorreu um erro ao tentar salvar o token de acesso. Por favor, informe o suporte técnico.', FLASH_ERROR);
		}
	}

	/*
	*
	* Cria usuários de teste
	*
	*/

	/*
	public function criar_usuario_teste() {
		$params = array(
		  "site_id" => "MLB",
		);

		if ($this->validar_token()) {
			$users = $this->meli->post('/users/test_user', $params, array('access_token' => $this->appAccessToken));
			$this->log($users, LOG_DEBUG);
		}
	}
	*/
}
?>