<?php
class MercadoLivreLogsComponent extends MercadoLivreAuthComponent {

	public function initialize(Controller $controller) {
		parent::initialize($controller);
	}

	public function gravar_produto($error, $params, $produto_id) {
		$msg = '';
		if (count($error['body']->cause)) {
			foreach ($error['body']->cause as $key => $causa) {
				$msg .= 'causa ' . $key . ': (' . $causa->code . ') '. $causa->message . "\n";
			}
		}
		CakeLog::write('meli', PROJETO . ' (' . $error['body']->status . ')' . "\n" .  'error: (' . $error['body']->error  . ') ' . $error['body']->message . "\n" . $msg . 'produto: (#' . $produto_id . ') ' . "\n", array('MercadoLivre'));
	}

	public function gravar_pedido($error, $pedido_id) {
		$msg = '';
		if (count($error['body']->cause)) {
			foreach ($error['body']->cause as $key => $causa) {
				$msg .= 'causa ' . $key . ': (' . $causa->code . ') '. $causa->message . "\n";
			}
		}
		CakeLog::write('meli', PROJETO . ' (' . $error['body']->status . ')' . "\n" .  'error: (' . $error['body']->error  . ') ' . $error['body']->message . "\n" . $msg . 'Pedido: (#' . $pedido_id . ') ' . "\n", array('MercadoLivre'));
	}

	public function gravar_feedback($error, $feedback_id) {
		$msg = '';
		if (count($error['body']->cause)) {
			foreach ($error['body']->cause as $key => $causa) {
				$msg .= 'causa ' . $key . ': (' . $causa->code . ') '. $causa->message . "\n";
			}
		}
		CakeLog::write('meli', PROJETO . ' (' . $error['body']->status . ')' . "\n" .  'error: (' . $error['body']->error  . ') ' . $error['body']->message . "\n" . $msg . 'Feedback: (#' . $feedback_id . ') ' . "\n", array('MercadoLivre'));
	}

	public function gravar_cliente($error, $cliente_id) {
		$msg = '';
		if (count($error['body']->cause)) {
			foreach ($error['body']->cause as $key => $causa) {
				$msg .= 'causa ' . $key . ': (' . $causa->code . ') '. $causa->message . "\n";
			}
		}
		CakeLog::write('meli', PROJETO . ' (' . $error['body']->status . ')' . "\n" .  'error: (' . $error['body']->error  . ') ' . $error['body']->message . "\n" . $msg . 'Cliente: (#' . $cliente_id . ') ' . "\n", array('MercadoLivre'));
	}

	public function gravar_frete($error, $frete_id) {
		$msg = '';
		if (count($error['body']->cause)) {
			foreach ($error['body']->cause as $key => $causa) {
				$msg .= 'causa ' . $key . ': (' . $causa->code . ') '. $causa->message . "\n";
			}
		}
		CakeLog::write('meli', PROJETO . ' (' . $error['body']->status . ')' . "\n" .  'error: (' . $error['body']->error  . ') ' . $error['body']->message . "\n" . $msg . 'Frete: (#' . $frete_id . ') ' . "\n", array('MercadoLivre'));
	}

}
?>