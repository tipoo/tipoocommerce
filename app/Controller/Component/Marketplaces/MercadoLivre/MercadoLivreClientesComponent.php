<?php
class MercadoLivreClientesComponent extends MercadoLivreAuthComponent {

	public $Cliente;

	public function initialize(Controller $controller) {
		parent::initialize($controller);

		$this->load_models(
			'Cliente'
		);
	}

	public function get_params_pedido($meli_cliente) {
		$cliente = array(
			'Cliente' => array()
		);

		$cpf = $this->converter_cpf($meli_cliente);

		// IMPORTANTE - O "=" está atribuindo o retorno da função à variável
		if ($cliente_existente = $this->verificar_cliente_existente($meli_cliente, $cpf)) {
			$cliente['Cliente'] += array(
				'id' => $cliente_existente['Cliente']['id']
			);
		} else {
			// Não atualiza essas informações sem o consentimento do cliente
			$cliente['Cliente'] += array(
				'tipo_pessoa' => 'F',
				'cpf' => $cpf,
				'nome' => $meli_cliente->first_name . ' ' . $meli_cliente->last_name,
				'email' => $meli_cliente->email,
				'senha' => $this->Controller->Random->gerarString()
				// 'data_nascto'
				// 'sexo'
			);
		}

		$cliente['Cliente'] += array(
			'mercado_livre_cliente_id' => $meli_cliente->id
		);

		// Telefone 01
		if ($meli_cliente->phone->number != '') {
			$cliente['Cliente'] += array(
				'tel_residencial' => '(' . $meli_cliente->phone->area_code . ')' . $meli_cliente->phone->number
			);
		}

		// Telefone 02
		if ($meli_cliente->alternative_phone->number != '') {
			$cliente['Cliente'] += array(
				'celular' => '(' . $meli_cliente->alternative_phone->area_code . ')' . $meli_cliente->alternative_phone->number
			);
		}

		return $cliente;
	}

	public function bloquear($meli_vendedor, $meli_cliente) {

		if ($this->validar_token()) {
			$params = array(
				'user_id' => $meli_cliente->id
			);

			$meli_cliente_bloqueado = $this->meli->post('/users/' . $meli_vendedor->id . '/order_blacklist', $params, array('access_token' => $this->appAccessToken));

			if (isset($meli_cliente_bloqueado['body']->error)) {
				$this->Controller->MercadoLivreLogs->gravar_cliente($meli_cliente_bloqueado, $meli_cliente->id);
			}
		}

	}

	public function desbloquear($meli_vendedor, $meli_cliente) {
		sleep(30);

		if ($this->validar_token()) {
			$meli_cliente_desbloqueado = $this->meli->delete('/users/' . $meli_vendedor->id . '/order_blacklist/' . $meli_cliente->id, array('access_token' => $this->appAccessToken));

			if (isset($meli_cliente_desbloqueado['body']->error)) {
				$this->Controller->MercadoLivreLogs->gravar_cliente($meli_cliente_desbloqueado, $meli_cliente->id);
			}
		}
	}

	private function verificar_cliente_existente($meli_cliente, $cpf) {
		$cliente = $this->get_cliente($meli_cliente, $cpf);

		if ($cliente) {
			return $cliente;
		} else {
			return false;
		}
	}

	private function get_cliente($meli_cliente, $cpf) {
		$cliente = $this->Cliente->find('first', array(
			'contain' => false,
			'conditions' => array(
				'OR' => array(
					'Cliente.mercado_livre_cliente_id' => $meli_cliente->id,
					'Cliente.email' => $meli_cliente->email,
					'Cliente.cpf' => $cpf
				),
				'Cliente.ativo'
			)
		));

		return $cliente;
	}

	private function converter_cpf($meli_cliente) {
		if ($meli_cliente->billing_info->doc_type == 'CPF') {
			$cpf = $meli_cliente->billing_info->doc_number;
			$cpf = substr($cpf , 0, 3) . '.' . substr($cpf , 3, 3) . '.' . substr($cpf , 6, 3) . '-' . substr($cpf , 9, 2);
		} else {
			$cpf = 'Não especificado';
		}

		return $cpf;
	}
}
?>