<?php
class MercadoLivreFreteComponent extends MercadoLivreAuthComponent {

	public $FreteTransportadora;

	public function initialize(Controller $controller) {
		parent::initialize($controller);

		$this->load_models(
			'FreteTransportadora'
		);
	}

	public function get_meli_frete($meli_frete_id) {
		if ($this->validar_token()) {
			$meli_frete = $this->meli->get('/shipments/' . $meli_frete_id, array('access_token' => $this->appAccessToken));

			if (isset($meli_frete['body']->error)) {
				$this->Controller->MercadoLivreLogs->gravar_frete($meli_frete, $meli_frete_id);
				return false;
			}

			$meli_frete = $meli_frete['body'];

			return $meli_frete;
		}
	}

	public function atualizar_frete() {
		$produtos = $this->Controller->MercadoLivreProdutos->get_produtos();

		foreach ($produtos as $produto) {
			// Verifica se o produto que está na coleção está integrado no mercado livre
			if ($produto['Produto']['mercado_livre_produto_id'] != '') {

				$meli_produto = $this->Controller->MercadoLivreProdutos->get_meli_produto($produto['Produto']['mercado_livre_produto_id']);

				if ($this->Controller->MercadoLivreProdutos->verificar_status($meli_produto) && $this->Controller->MercadoLivreProdutos->verificar_vendas($meli_produto)) {
					$params = $this->get_params($produto['Produto']['Categoria']['mercado_livre_categoria_id'], $produto['Produto']['Sku']['0']);

					if ((count($params) > 0 && $params['shipping']['mode'] == 'custom')) {
						if ($this->validar_token()) {
							$meli_produto = $this->meli->put('/items/' . $produto['Produto']['mercado_livre_produto_id'], $params, array('access_token' => $this->appAccessToken));

							if (isset($meli_produto['body']->error)) {
								$this->Controller->MercadoLivreLogs->gravar_produto($meli_produto, $params, $produto['Produto']['id']);
								$this->Controller->Session->setFlash('<strong>ATENÇÃO!</strong> Ocorreu um erro ao tentar integrar a transportadora. Por favor, informe o suporte técnico.', FLASH_ERROR);
							}
						}
					} else {
						$this->Controller->Session->setFlash('<strong>ATENÇÃO!</strong> Alguns produtos usam o Mercado Envios para calculo de frete. O que impossibilitou a edição dos mesmos no Mercado Livre.', FLASH_WARNING);
					}
				} else {
					$this->Controller->Session->setFlash('<strong>ATENÇÃO!</strong> Alguns produtos encontram-se inativos OU posssuem vendas. O que impossibilitou a edição dos mesmos no Mercado Livre.', FLASH_WARNING);
				}
			}
		}
	}

	public function get_params($categoria_id, $first_sku) {
		$params = array();

		if ($this->validar_token()) {

			$me = $this->meli->get('/users/me', array('access_token' => $this->appAccessToken));

			if (isset($me['body']->error)) {
				$this->Controller->MercadoLivreLogs->gravar_cliente($me, 'me');
				$this->Controller->Session->setFlash('Ocorreu um erro ao tentar cadastrar as transportas. Por favor entre em contato com o suporte técnico.', FLASH_ERROR);
				return $params;
			}

			$dimensoes = $first_sku['largura'] . 'x' . $first_sku['altura'] . 'x' . $first_sku['profundidade'] . ',' . $first_sku['peso'];
			$frete_disponivel = $this->meli->get('/users/' . $me['body']->id . '/shipping_modes', array('category_id' => $categoria_id, 'dimensions' => $dimensoes));

			foreach ($frete_disponivel['body'] as $frete) {
				if ($frete->mode == 'custom') {
					$params = $this->get_params_custom();
					break;
				} else if ($frete->mode == 'me1') {
					$params = array(
						'shipping' => array(
							'mode' => 'me1',
							'local_pick_up' => false,
							'dimensions' => $dimensoes
						)
					);
					break;
				} else if ($frete->mode == 'me2') {
					$params = array(
						'shipping' => array(
							'mode' => 'me2',
							'local_pick_up' => false
						)
					);
					break;
				}
			}
		}

		return $params;
	}

	public function get_params_pedido($meli_frete) {
		$frete = array(
			'prazo_frete' => $this->converter_prazo($meli_frete),
			'valor_frete' => $this->get_valor($meli_frete),
			'codigo_rastreamento' => $this->get_codigo_rastreamento($meli_frete),
			'mercado_livre_frete_id' => $this->get_meli_frete_id($meli_frete),
			'mercado_livre_frete_modo' => $this->get_meli_frete_modo($meli_frete),
			'mercado_livre_frete_transportadora' => $this->get_meli_frete_transportadora($meli_frete)
		);

		return $frete;
	}

	public function set_url_etiqueta($pedido) {
		if ($pedido['Pedido']['mercado_livre_frete_modo'] == 'me2') {

			$meli_frete_id = $pedido['Pedido']['mercado_livre_frete_id'];

			if ($this->validar_token()) {
				$meli_etiqueta = $this->meli->get('/shipment_labels', array('shipment_ids' => $meli_frete_id, 'savePdf' => 'Y', 'access_token' => $this->appAccessToken));

				if ($meli_etiqueta['body']->status == 200) {
					if ($this->validar_token()) {
						$url_etiqueta = 'https://api.mercadolibre.com/shipment_labels?shipment_ids=' . $meli_frete_id . '&savePdf=Y&access_token=' . $this->appAccessToken;
						$this->Controller->set('meli_url_etiqueta', $meli_url_etiqueta);
					}
				}
			}
		}
	}

	public function get_meli_frete_id($meli_frete) {
		if (isset($meli_frete->id)) {
			return $meli_frete->id;
		} else {
			return null;
		}
	}

	private function get_meli_frete_transportadora($meli_frete) {
		if (isset($meli_frete->shipping_option->name)) {
			return $meli_frete->shipping_option->name;
		} else {
			return null;
		}
	}

	private function get_meli_frete_modo($meli_frete) {
		if (isset($meli_frete->mode)) {
			return $meli_frete->mode;
		} else {
			return null;
		}
	}

	private function get_codigo_rastreamento($meli_frete) {
		if (isset($meli_frete->tracking_number)) {
			return $meli_frete->tracking_number;
		} else {
			return null;
		}
	}

	private function converter_prazo($meli_frete) {
		if (isset($meli_frete->shipping_option->speed->shipping)) {
			// Transforma horas em dias
			$dias = $meli_frete->shipping_option->speed->shipping / 24;
			return $dias;
		} else {
			return null;
		}
	}

	private function get_valor($meli_frete) {
		if (isset($meli_frete->shipping_option->cost)) {
			return $meli_frete->shipping_option->cost;
		} else {
			return null;
		}
	}

	private function get_params_custom() {
		$params = array();

		$transportadoras = $this->FreteTransportadora->find('all', array(
			'contain' => array(
				'FreteValor' => array(
					'conditions' => array(
						'FreteValor.ativo' => true
					)
				)
			),
			'conditions' => array(
				'FreteTransportadora.ativo' => true,
				'FreteTransportadora.frete_tipo' => 'T',
				'FreteTransportadora.mercado_livre' => true
			)
		));

		if (count($transportadoras) > 0) {
			$params = array(
				'shipping' => array(
					'mode' => 'custom',
					'local_pick_up' => false,
					'free_shipping' => false,
					'methods' => array()
				)
			);

			//Só pode mandar no máximo 10 transportadoras
			$count_t = 1;
			foreach ($transportadoras as $transportadora) {
				$params['shipping']['costs'][] = array(
					'description' => $transportadora['FreteTransportadora']['descricao'],
					'cost' => (float)$transportadora['FreteValor']['0']['valor']
				);

				if ($count_t >= 10) {
					break;
				}

				$count_t++;
			}
		}

		return $params;
	}
}
?>