<?php
App::uses('AppController', 'Controller');

class MercadoLivreController extends AppController {

	public $uses = array('MercadoLivre', 'Configuracao', 'Colecao');

	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('notificacoes');
	}

	public function admin_index() {
		$configuracao = $this->Configuracao->find();
		$this->set('configuracao', $configuracao);

		if ($this->Configuracao->get('mercado_livre_ativo')) {
			$this->MercadoLivreAuth->login();
		}
	}

	public function admin_meli_adicionar_colecao() {
		$this->Configuracao->id = $this->Configuracao->get('id');

		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Configuracao->saveField('mercado_livre_colecao_id', $this->request->data['Configuracao']['mercado_livre_colecao_id'], false)) {
				$this->Session->setFlash('Coleção vinculado ao marketplace do Mercado Livre com sucesso.', FLASH_SUCCESS);
				$this->redirect($this->referer());
			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar vincular esta coleção ao marketplace do Mercado Livre. Por favor, tente novamente.', FLASH_ERROR);
			}
		}
	}

	public function admin_adicionar_produtos() {

		if ($this->request->is('post')) {
			$this->MercadoLivreProdutos->adicionar_produtos();
			$this->backToPaginatorIndex();
		}
	}

	public function notificacoes() {
		// $this->MercadoLivre->relist(84);
		// $this->MercadoLivrePedidos->get_meli_pedido();
		$this->MercadoLivreProdutos->editar_produto(84);
	}

	public function admin_ajax_enviar_feedback($type = 'post') {
		if ($this->request->is('post')) {

			if ($type == 'post') {
				$retorno = array(
					'sucesso' => $this->MercadoLivreFeedback->enviar($this->request->data)
				);
			} else {
				$retorno = array(
					'sucesso' => $this->MercadoLivreFeedback->modificar($this->request->data)
				);
			}

			$this->renderJson($retorno);
		}

	}

	public function admin_ajax_reply_feedback() {
		if ($this->request->is('post')) {

			$retorno = array(
				'sucesso' => $this->MercadoLivreFeedback->reply($this->request->data)
			);

			$this->renderJson($retorno);
		}

	}

}

?>