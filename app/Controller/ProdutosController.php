<?php
App::uses('AppController', 'Controller');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
App::uses('CropImage', 'Vendor');

class ProdutosController extends AppController {

	public $uses = array(
		'Produto',
		'Marca',
		'Categoria',
		'Caracteristica',
		'Slug',
		'CaracteristicasValoresSelecionado',
		'Sku',
		'Tag',
		'TagsProduto',
		'CompreJuntoPromocao',
		'AfinidadeProduto',
		'Avaliacao',
		'Configuracao',
		'Servico',
		'ServicosProduto'
	);

	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('ver', 'ajax_avaliar');
	}

	public function admin_index($codigo_produto = null) {

		$categoria_id = null;
		$this->request->data['Filtro']['categoria_id'] = '';
		if (isset($this->request->params['named']['categoria_id'])) {
			$categoria_id = $this->request->params['named']['categoria_id'];
			$this->request->data['Filtro']['categoria_id'] = $categoria_id;
		}

		$marca_id = null;
		if (isset($this->request->params['named']['marca_id'])) {
			$marca_id = $this->request->params['named']['marca_id'];
			$this->request->data['Filtro']['marca_id'] = $marca_id;
		}

		$nome = null;
		if (isset($this->request->params['named']['nome'])) {
			$nome = $this->request->params['named']['nome'];
			$this->request->data['Filtro']['nome'] = $nome;
		}

		$contain = array(
			'ColecoesProduto.Colecao',
			'ColecoesProduto' => array(
				'conditions' => array(
					'ColecoesProduto.ativo' => true

				),
			),
			'Marca',
			'Slug.url',
			'Imagem' => array(
				'conditions' => array(
					'Imagem.destaque' => true
				),
				'fields' => array('imagem')
			),
			'Sku' => array(
				'conditions' => array(
					'Sku.ativo' => true
				)
			)
		);

		$conditions = array();

		if ($categoria_id != '') {
			$conditions['Produto.categoria_id'] = $this->MenuCategorias->getCategoriasIn($categoria_id);
		}

		if ($marca_id != '') {
			$conditions['Produto.marca_id'] = $marca_id;
		}

		if ($nome != '') {
			$conditions[] = 'Produto.descricao LIKE "%' . $nome . '%"';
		}

		if ($codigo_produto != '') {

			$skus =  $this->Sku->find('list', array(
				'contais' => false,
				'conditions' => array(
					'Sku.sku LIKE "%' . $codigo_produto . '%"',
				),
				'fields' => array(
					'Sku.sku',
					'Sku.produto_id'
				)
			));

			$produtos_id = array();
			foreach ($skus as $produto) {
				$produtos_id[] = $produto;
			}

			$produtos_id[] = $codigo_produto;

			$conditions['OR'] = array(
				'Produto.id' => $produtos_id,
				'Produto.codigo_de_referencia' => $codigo_produto
			);

		}

		$order = array(
			'Produto.created' => 'DESC'
		);

		$this->paginate = array(
			'contain' => $contain,
			'conditions' => $conditions,
			'order' => $order,
			'limit' => 50
		);

		/* Marcas */

		$marcas = $this->Marca->find('list', array(
			'conditions' => array(
				'Marca.ativo' => true
			)
		));

		$this->set('produtos', $this->paginate());
		$this->set('categoria_id', $categoria_id);
		$this->set('marcas', $marcas);
		$this->set('combo_categorias', $this->MenuCategorias->getArvoreCategorias());
	}

	public function admin_adicionar() {

		$configuracao = $this->Configuracao->find('first');

		$tags = $this->Tag->find('all', array(
			'conditions' => array(
				'Tag.ativo' => true
			),
			'order' => 'nome'
		));

		$servicos = $this->Servico->find('all', array(
			'conditions' => array(
				'Servico.ativo' => true
			),
			'order' => 'nome'
		));

		if ($this->request->is('post')) {

			$verifica_codigo_referencia = $this->Produto->find('count', array(
				'contain' => false,
				'conditions' => array(
					'Produto.codigo_de_referencia' => $this->request->data['Produto']['codigo_de_referencia']
				),
			));

			if ($verifica_codigo_referencia == 0) {
				$categoria_id = $this->request->data['Produto']['categoria_id'];

				/* Imagem */

				$diretorio = $this->getDiretorioProdutos();
				$this->FileUpload->permitirSomenteImagens();

				if ($this->request->data['Imagem'][0]['imagem']['name'] == '') {
					unset($this->request->data['Imagem']);
				} else {
					$path_parts = pathinfo($this->request->data['Imagem'][0]['imagem']['name']);
					$nomeImagem = strtolower($this->request->data['Produto']['descricao']);

					$this->request->data['Imagem'][0]['imagem']['name'] = Inflector::slug($nomeImagem, '_') . '.' . $path_parts['extension'];
					$this->request->data['Imagem'][0]['imagem'] = $this->FileUpload->upload($diretorio, array($this->request->data['Imagem'][0]['imagem']), true);

					/* Recorte e Resize */

					$image = new CropImage();
					$image->setImage($diretorio . DS . $this->request->data['Imagem'][0]['imagem']);
					$image->createThumb($diretorio . DS . 'big_' . $this->request->data['Imagem'][0]['imagem'], $configuracao['Configuracao']['img_big_w'] , $configuracao['Configuracao']['img_big_h']);
					$image->createThumb($diretorio . DS . 'medium_' . $this->request->data['Imagem'][0]['imagem'], $configuracao['Configuracao']['img_medium_w'] , $configuracao['Configuracao']['img_medium_h']);
					$image->createThumb($diretorio . DS . 'normal_' . $this->request->data['Imagem'][0]['imagem'], $configuracao['Configuracao']['img_normal_w'] , $configuracao['Configuracao']['img_normal_h']);
					$image->createThumb($diretorio . DS . 'small_' . $this->request->data['Imagem'][0]['imagem'], $configuracao['Configuracao']['img_small_w'] , $configuracao['Configuracao']['img_small_h']);
					$image->createThumb($diretorio . DS . 'thumb_' . $this->request->data['Imagem'][0]['imagem'], $configuracao['Configuracao']['img_thumb_w'] , $configuracao['Configuracao']['img_thumb_h']);

					/* Fim Recorte e Resize */
				}

				/* Fim Imagem */

				/* Slug */

				$count_slug = 0;
				do {
					if ($count_slug == 0) {
						$slug = '/' . Inflector::slug($this->request->data['Produto']['descricao'], '-');
					} else {
						$slug = '/' . Inflector::slug($this->request->data['Produto']['descricao'], '-') . '-' . $count_slug;
					}
					$slug_find = $this->Slug->find('first', array(
						'contain' => false,
						'conditions' => array(
							'Slug.url' => $slug
						)
					));

					$count_slug++;

				} while ($slug_find);

				$this->request->data['Slug']['url'] = strtolower($slug);

				/* Slug - Fim */

				$this->request->data['Slug']['controller'] = 'catalogo';
				$this->request->data['Slug']['action'] = 'produto';
				$this->request->data['Slug']['custom_parser_class'] = 'ProdutoParser';

				if ($this->request->data['Produto']['preco_de'] == '0,00') {
					$this->request->data['Produto']['preco_de'] = '';
				}

				$this->request->data['Produto']['preco_de'] = str_replace('.', '', $this->request->data['Produto']['preco_de']);
				$this->request->data['Produto']['preco_de'] = str_replace(',', '.', $this->request->data['Produto']['preco_de']);
				$this->request->data['Produto']['preco_por'] = str_replace('.', '', $this->request->data['Produto']['preco_por']);
				$this->request->data['Produto']['preco_por'] = str_replace(',', '.', $this->request->data['Produto']['preco_por']);

				/* Limpa do array os valores vazios */

				if (isset($this->request->data['CaracteristicasValoresSelecionado'])) {
					foreach ($this->request->data['CaracteristicasValoresSelecionado'] as $key => $caracteristicas_valor_selecionado) {
						if ($caracteristicas_valor_selecionado['caracteristica_tipo'] == 'L') {
							if ($caracteristicas_valor_selecionado['caracteristicas_valor_id'] == '') {
								unset($this->request->data['CaracteristicasValoresSelecionado'][$key]);
							}
						} else if ($caracteristicas_valor_selecionado['caracteristica_tipo'] == 'TG') {
							if ($caracteristicas_valor_selecionado['descricao_grande'] == '') {
								unset($this->request->data['CaracteristicasValoresSelecionado'][$key]);
							}
						} else {
							if ($caracteristicas_valor_selecionado['descricao'] == '') {
								unset($this->request->data['CaracteristicasValoresSelecionado'][$key]);
							}
						}
					}
				}

				/* Fim - Limpa do array os valores vazios */

				$this->Produto->create();


				if ($this->Produto->saveAll($this->request->data)) {

					$this->Categoria->id = $categoria_id;
					if ($this->Categoria->saveField('descendente', 'P', false)) {
						$this->Session->setFlash('Produto salvo com sucesso.', FLASH_SUCCESS);
						$this->backToPaginatorIndex();
					} else {
						$this->Session->setFlash('Ocorreu um erro ao tentar relacionar a categoria ao produto. Por favor, tente novamente.', FLASH_ERROR);
					}

				} else {
					$this->Session->setFlash('Ocorreu um erro ao tentar salvar o Produto. Por favor, tente novamente.', FLASH_ERROR);
				}
			} else {
				$this->Session->setFlash('Já existe um produto com esse código de referência. Por favor, cadastre um código diferente.', FLASH_ERROR);

			}
		}

		$marcas = $this->Marca->find('list', array(
			'conditions' => array(
				'Marca.ativo' => true
			),
			'order' => array(
				'descricao' => 'ASC'
			)
		));

		$caracteristicas_gerais = $this->getCaracteristicasGerais();

		$this->set('servicos', $servicos);
		$this->set('tags', $tags);
		$this->set('marcas', $marcas);
		$this->set('combo_categorias', $this->MenuCategorias->getArvoreCategorias());
		$this->set('caracteristicas_gerais', $caracteristicas_gerais);
		$this->set('origens', $this->get_origens_fiscais());

	}

	public function admin_editar($id = null) {

		$this->Produto->id = $id;
		$this->request->data['Produto']['id'] = $id;

		$produto = $this->Produto->find('first', array(
			'contain' => array(
				'Sku',
				'TagsProduto.Tag',
				'Slug',
				'Categoria',
				'Marca',
				'CaracteristicasValoresSelecionado' => array(
					'conditions' => array(
						'CaracteristicasValoresSelecionado.ativo' => true
					)
				),
				'Imagem',
				'Servico',
				'ColecoesProduto' => array(
					'conditions' => array(
						'ColecoesProduto.colecao_id' => $this->Configuracao->get('mercado_livre_colecao_id')
					)
				)
			),
			'conditions' => array(
				'Produto.id' => $id
			)
		));

		$tags = $this->Tag->find('all', array(
			'conditions' => array(
				'Tag.ativo' => true
			),
			'order' => 'nome'
		));

		$tags_selecionadas_id = array();

		if ($produto['TagsProduto'] != '') {
			foreach ($produto['TagsProduto'] as $tags_produto) {

				if (isset($tags_produto['Tag']['id'])) {
					$tags_selecionadas_id[] = $tags_produto['Tag']['id'];
				}

			}
		}

		$servicos = $this->Servico->find('all', array(
			'conditions' => array(
				'Servico.ativo' => true
			),
			'order' => 'nome'
		));

		$servicos_selecionados_id = array();

		if (count($produto['Servico']) > 0) {
			foreach ($produto['Servico'] as $servico) {
				$servicos_selecionados_id[] = $servico['id'];
			}
		}

		if (!$this->Produto->exists()) {
			throw new NotFoundException('Produto inexistente.');
		}

		if ($this->request->is('post') || $this->request->is('put')) {

			$verifica_codigo_referencia =$this->Produto->find('count', array(
				'contain' => false,
				'conditions' => array(
					'Produto.codigo_de_referencia' => $this->request->data['Produto']['codigo_de_referencia']
				),
			));

			$verifica_codigo_produto = $this->Produto->find('first', array(
				'contain' => false,
				'conditions' => array(
					'Produto.codigo_de_referencia' => $this->request->data['Produto']['codigo_de_referencia']
				),
			));

			if ($verifica_codigo_referencia == 0 || ($verifica_codigo_produto ['Produto']['id'] == $this->request->data['Produto']['id'])) {
				$categoria_id = $this->request->data['Produto']['categoria_id'];

				if ($this->request->data['Produto']['preco_de'] == '0,00') {
					$this->request->data['Produto']['preco_de'] = '';
				}

				$this->request->data['Produto']['preco_de'] = str_replace('.', '', $this->request->data['Produto']['preco_de']);
				$this->request->data['Produto']['preco_de'] = str_replace(',', '.', $this->request->data['Produto']['preco_de']);
				$this->request->data['Produto']['preco_por'] = str_replace('.', '', $this->request->data['Produto']['preco_por']);
				$this->request->data['Produto']['preco_por'] = str_replace(',', '.', $this->request->data['Produto']['preco_por']);
				// $this->request->data['Produto']['peso'] = str_replace('.', '', $this->request->data['Produto']['peso']);
				// $this->request->data['Produto']['peso'] = str_replace(',', '.', $this->request->data['Produto']['peso']);

				$caracteristicas_sucesso = $this->CaracteristicasValoresSelecionado->deleteAll(array('CaracteristicasValoresSelecionado.produto_id' => $id));
				$caracteristicas_valores_selecionados_skus_excluidos = false;

				if ($produto['Produto']['categoria_id'] != $this->request->data['Produto']['categoria_id']) {
					if (count($produto['Sku']) > 0) {
						$skus_id = array();
						foreach ($produto['Sku'] as $sku) {
							$skus_id[] = $sku['id'];
						}
						$caracteristicas_sucesso = $this->CaracteristicasValoresSelecionado->deleteAll(array('CaracteristicasValoresSelecionado.sku_id' => $skus_id));
						$caracteristicas_valores_selecionados_skus_excluidos = true;
					}
				}

				/* Limpa do array os valores vazios */

				if (isset($this->request->data['CaracteristicasValoresSelecionado'])) {
					foreach ($this->request->data['CaracteristicasValoresSelecionado'] as $key => $caracteristicas_valor_selecionado) {
						if ($caracteristicas_valor_selecionado['caracteristica_tipo'] == 'L') {
							if ($caracteristicas_valor_selecionado['caracteristicas_valor_id'] == '') {
								unset($this->request->data['CaracteristicasValoresSelecionado'][$key]);
							}
						} else if ($caracteristicas_valor_selecionado['caracteristica_tipo'] == 'TG') {
							if ($caracteristicas_valor_selecionado['descricao_grande'] == '') {
								unset($this->request->data['CaracteristicasValoresSelecionado'][$key]);
							}
						} else {
							if ($caracteristicas_valor_selecionado['descricao'] == '') {
								unset($this->request->data['CaracteristicasValoresSelecionado'][$key]);
							}
						}
					}
				}

				/* Fim - Limpa do array os valores vazios */

/*				$this->request->data['Slug']['url'] = '/' . Inflector::slug($this->request->data['Slug']['url'], '-');
				$this->request->data['Slug']['url'] = strtolower($this->request->data['Slug']['url']);


				$verificaSlug = $this->Slug->find('first', array(
					'conditions' => array(
						'Slug.url' => $this->request->data['Slug']['url']
					)
				));*/

				//if(count($verificaSlug) == 0 || $verificaSlug['Slug']['id'] == $produto['Produto']['slug_id']) {

					$this->TagsProduto->deleteAll(array('TagsProduto.produto_id' => $id));

					if (!isset($this->request->data['Servico'])) {
						$this->ServicosProduto->deleteAll(array('ServicosProduto.produto_id' => $id));
					}

					if ($this->Produto->saveAll($this->request->data)) {

						$this->Categoria->id = $categoria_id;
						if ($this->Categoria->saveField('descendente', 'P', false)) {

							if ($caracteristicas_sucesso) {
								if ($caracteristicas_valores_selecionados_skus_excluidos) {
									$this->Session->setFlash('Produto alterado com sucesso. Verifique as características de cada sku, pois ao mudar de categoria seus valores foram apagados.', FLASH_WARNING);
								} else {
									$this->Session->setFlash('Produto alterado com sucesso.', FLASH_SUCCESS);
								}

								// Mercado Livre
								if ($this->Configuracao->get('mercado_livre_ativo')) {
									$this->MercadoLivreProdutos->editar_produto($id);
								}
								// Fim - Mercado Livre

								$this->backToPaginatorIndex();
							} else {
								$this->Session->setFlash('Ocorreu um erro ao tentar salvar as características. Por favor, tente novamente.', FLASH_ERROR);
							}

						} else {
							$this->Session->setFlash('Ocorreu um erro ao tentar relacionar a categoria ao produto. Por favor, tente novamente.', FLASH_ERROR);
						}

					} else {
						$this->Session->setFlash('Ocorreu um erro ao tentar salvar o Produto. Por favor, tente novamente.', FLASH_ERROR);
					}

	/*			} else {
					$this->Session->setFlash('Já existe uma url  "<strong>' .  $this->request->data['Slug']['url']  . '</strong>". Por favor, cadastre uma url diferente.', FLASH_ERROR);
					$this->request->data['Slug']['url'] = $produto['Slug']['url'];
				}*/

			} else {
				$this->Session->setFlash('Já existe um produto com esse código de referência. Por favor, cadastre um código diferente.', FLASH_ERROR);

			}


		} else {
			$caracteristicas_valores_selecionados = array();
			foreach ($produto['CaracteristicasValoresSelecionado'] as $key => $caracteristicas_valores_selecionado) {
				$caracteristicas_valores_selecionados[$caracteristicas_valores_selecionado['caracteristica_id']] = $caracteristicas_valores_selecionado;
			}

			$produto['CaracteristicasValoresSelecionado'] = $caracteristicas_valores_selecionados;
			$this->request->data = $produto;
		}

		$marcas = $this->Marca->find('list', array(
			'conditions' => array(
				'Marca.ativo' => true
			),
			'order' => array(
				'descricao' => 'ASC'
			)
		));

		$caracteristicas_categoria = $this->getCaracteristicasCategoria($produto['Produto']['categoria_id']);
		$caracteristicas_gerais = $this->getCaracteristicasGerais();

		$this->set('tags_selecionadas_id', $tags_selecionadas_id);
		$this->set('servicos_selecionados_id', $servicos_selecionados_id);

		$this->set('servicos', $servicos);
		$this->set('tags', $tags);
		$this->set('caracteristicas_categoria', $caracteristicas_categoria);
		$this->set('marcas', $marcas);
		$this->set('combo_categorias', $this->MenuCategorias->getArvoreCategorias());
		$this->set('caracteristicas_gerais', $caracteristicas_gerais);
		$this->set('origens', $this->get_origens_fiscais());

	}

	public function admin_excluir($id = null) {
		$this->Produto->id = $id;

		if (!$this->Produto->exists()) {
			throw new NotFoundException('Produto inexistente.');
		}

		if ($this->request->is('post')) {
			if ($this->Produto->saveField('ativo', false, false)) {

				$produto = $this->Produto->find('first', array(
					'contain' => array(
						'Slug'
					),
					'conditions' => array(
						'Produto.ativo' => false,
						'Produto.id' => $id
					)
				));

				$this->Slug->id = $produto['Slug']['id'];
				if ($this->Slug->saveField('ativo', false, false)) {
					$this->Session->setFlash('Produto desativado com sucesso.', FLASH_SUCCESS);
					$this->backToPaginatorIndex();
				} else {
					$this->Session->setFlash('Ocorreu um erro ao tentar desativar a url do Produto. Por favor, informe nossa equipe sobre o ocorrido.', FLASH_ERROR);
				}

			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar desativar o Produto. Por favor, informe nossa equipe sobre o ocorrido.', FLASH_ERROR);
			}
		}
	}

	public function admin_ativar($id = null) {
		$this->Produto->id = $id;

		if (!$this->Produto->exists()) {
			throw new NotFoundException('Produto inexistente.');
		}

		if ($this->request->is('post')) {

			$produto = $this->Produto->find('first', array(
				'contain' => array(
					'Slug',
					'Sku',
					'Marca',
					'Imagem'
				),
				'conditions' => array(
					'Produto.id' => $id
				)
			));

			if ($this->Produto->saveField('ativo', true, false)) {
				$this->Slug->id = $produto['Slug']['id'];
				if ($this->Slug->saveField('ativo', true, false)) {
					$this->Session->setFlash('Produto ativado com sucesso.', FLASH_SUCCESS);
					$this->backToPaginatorIndex();
				} else {
					$this->Session->setFlash('Ocorreu um erro ao tentar ativar a url do Produto. Por favor, informe nossa equipe sobre o ocorrido.', FLASH_ERROR);
				}
			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar ativar o produto. Por favor, informe nossa equipe sobre o ocorrido.', FLASH_ERROR);
			}

		}
	}

	public function admin_ajax_carregar_caracteristicas($categoria_id = null, $produto_id = null) {

		$contador = 0;
		if (isset($this->request->params['named']['contador'])) {
			$contador = $this->request->params['named']['contador'];
		}

		$this->layout = 'ajax';

		$caracteristicas_categoria = $this->getCaracteristicasCategoria($categoria_id);

		if ($produto_id != null) {

			$produto = $this->Produto->find('first', array(
				'contain' => array(
					'CaracteristicasValoresSelecionado' => array(
						'conditions' => array(
							'CaracteristicasValoresSelecionado.ativo' => true
						)
					),
				),
				'conditions' => array(
					'Produto.id' => $produto_id
				)
			));

			$caracteristicas_valores_selecionados = array();
			foreach ($produto['CaracteristicasValoresSelecionado'] as $key => $caracteristicas_valores_selecionado) {
				$caracteristicas_valores_selecionados[$caracteristicas_valores_selecionado['caracteristica_id']] = $caracteristicas_valores_selecionado;
			}

			$produto['CaracteristicasValoresSelecionado'] = $caracteristicas_valores_selecionados;

			$this->request->data = $produto;

		}

		$this->set('caracteristicas_categoria', $caracteristicas_categoria);
		$this->set('contador', $contador);
	}

	private function getCaracteristicasGerais() {
		$caracteristicas = $this->Caracteristica->find('all', array(
			'contain' => array(
				'CaracteristicasValor' => array(
					'conditions' => array(
						'CaracteristicasValor.ativo' => true
					)
				)
			),
			'conditions' => array(
				'Caracteristica.ativo',
				'Caracteristica.categoria_id' => '',
				'Caracteristica.referente' => 'P'
			)
		));

		return $caracteristicas;
	}

	private function getCaracteristicasCategoria($categoria_id, $caracteristicas = array()) {
		$categoria = $this->Categoria->find('first', array(
			'contain' => array(
				'CategoriaPai' => array(
					'conditions' => array(
						'Categoria.ativo'
					)
				),
				'Caracteristica' => array(
					'CaracteristicasValor' => array(
						'conditions' => array(
							'CaracteristicasValor.ativo' => true
						)
					),
					'conditions' => array(
						'Caracteristica.ativo',
						'Caracteristica.referente' => 'P',
					)
				)
			),
			'conditions' => array(
				'Categoria.id' => $categoria_id,
				'Categoria.ativo'
			)
		));

		if ($categoria) {

			if (count($categoria['Caracteristica'])) {
				$caracteristicas = array_merge($caracteristicas, $categoria['Caracteristica']);
			}

			if ($categoria['Categoria']['categoria_pai_id']) {
				$caracteristicas = $this->getCaracteristicasCategoria($categoria['Categoria']['categoria_pai_id'], $caracteristicas);
			}

			return $caracteristicas;

		} else {
			return $caracteristicas;
		}

	}

	private function getDiretorioProdutos() {
		$diretorio = $this->getDiretorioProjeto('produtos');
		new Folder($diretorio, true);

		return $diretorio;
	}

	public function admin_visualizar_detalhes_produto( $id = null) {

		$produto = $this->Produto->find('first', array(
			'conditions' => array(
				'Produto.id' => $id,
			),
		));

		$this->set('produto', $produto);

	}

	public function admin_compre_junto($id = null) {

		$produto = $this->Produto->find('first', array(
			'conditions' => array(
				'Produto.id' => $id,
			),
			'fields' => array(
				'id',
				'descricao',
				'codigo_de_referencia'
			)
		));

		$produtos_associados = $this->AfinidadeProduto->find('all', array(
			'contain' => array(
				'Produto.Marca'
			),
			'conditions' => array(
				'AfinidadeProduto.produto_principal_id' => $id,
				'AfinidadeProduto.pedido_sku_id is null',
				'AfinidadeProduto.ativo' => true
			),
			'order' => array(
				'AfinidadeProduto.created' => 'ASC'
			)

		));

		$descontos_associados = $this->CompreJuntoPromocao->find('all', array(
			'conditions' => array(
				'CompreJuntoPromocao.produto_id' => $id,
				'CompreJuntoPromocao.pedido_sku_id is null'
			),
			'order' => array(
				'CompreJuntoPromocao.created' => 'ASC'
			)

		));

		$this->set('produto', $produto);
		$this->set('produtos_associados', $produtos_associados);
		$this->set('descontos_associados', $descontos_associados);

	}

	public function admin_ajax_buscar_produtos($descricao = null) {
		$produtos = $this->Produto->find('all', array(
			'contain' => array(
				'Marca.descricao'
			),
			'conditions' => array(
				'Produto.ativo',
				'Produto.descricao LIKE "%'.$descricao.'%"'
			)
		));

		$produtosArray = array();
		foreach ($produtos as $produto) {
			$produtosArray[] = array(
				'id' => $produto['Produto']['id'],
				'descricao' => $produto['Produto']['descricao'],
				'marca' => $produto['Marca']['descricao'],
				'value' => $produto['Produto']['descricao'] . ' - ' . $produto['Marca']['descricao'] . ' - #' . $produto['Produto']['id']
			);
		}

		$this->renderJson($produtosArray);
	}

	public function admin_ajax_adicionar_produto_compre_junto($produto_principal_id = null, $produto_secundario_id = null) {

		$produto_selecionado = $this->AfinidadeProduto->find('first', array(
			'conditions' => array(
				'AfinidadeProduto.produto_principal_id' => $produto_principal_id,
				'AfinidadeProduto.produto_secundario_id' => $produto_secundario_id,
				'AfinidadeProduto.pedido_sku_id is NULL',
				'AfinidadeProduto.tipo' => 'CJ',
				'AfinidadeProduto.ativo' => true
			)
		));

		if (!count($produto_selecionado)) {

			$this->AfinidadeProduto->create();

			$data = array(
				'produto_principal_id' => $produto_principal_id,
				'produto_secundario_id' => $produto_secundario_id,
				'tipo' => 'CJ'
			);

			if ($this->AfinidadeProduto->save($data)) {

				$produto_associado = $this->AfinidadeProduto->find('first', array(
					'conditions' => array(
						'AfinidadeProduto.id' => $this->AfinidadeProduto->id
					)
				));

				$json = array('sucesso' => true, 'produto_associado' => $produto_associado);

			}

		} else {

			$json = array('sucesso' => false,'mensagem' => 'Esse produto já está cadastrado.');

		}

		$this->renderJson($json);

	}

	public function admin_ajax_excluir_produto_compre_junto($id_associado = null) {

		$this->AfinidadeProduto->id = $id_associado;

		if ($this->AfinidadeProduto->saveField('ativo', false, false)) {

			$json = array('sucesso' => true);

		}  else {

			$json = array('sucesso' => false,'mensagem' => 'Ocorreu um erro ao excluir esse produto.');

		}

		$this->renderJson($json);

	}

	public function admin_ajax_adicionar_desconto_compre_junto($tipo_valor = null, $valor = null, $produto_id = null) {

		$desconto_selecionado = $this->CompreJuntoPromocao->find('first', array(
			'conditions' => array(
				'CompreJuntoPromocao.tipo_valor' => $tipo_valor,
				'CompreJuntoPromocao.valor' => $valor,
				'CompreJuntoPromocao.produto_id' => $produto_id
			)
		));

		if (!count($desconto_selecionado)) {

			$this->CompreJuntoPromocao->create();

			$valor = str_replace('.', '', $valor);
			$valor = str_replace(',', '.', $valor);

			$data = array(
				'tipo_valor' => $tipo_valor,
				'valor' => $valor,
				'produto_id' => $produto_id
			);

			if ($this->CompreJuntoPromocao->save($data)) {

				$desconto_associado = $this->CompreJuntoPromocao->find('first', array(
					'conditions' => array(
						'CompreJuntoPromocao.tipo_valor' => $tipo_valor,
						'CompreJuntoPromocao.valor' => $valor,
						'CompreJuntoPromocao.produto_id' => $produto_id
					)
				));

				$json = array('sucesso' => true, 'desconto_associado' => $desconto_associado);

			}

		} else {

			$json = array('sucesso' => false,'mensagem' => 'Esse desconto já está cadastrado.');

		}

		$this->renderJson($json);

	}

	public function admin_ajax_excluir_desconto_compre_junto($id_associado = null) {

		$this->CompreJuntoPromocao->id = $id_associado;

		if ($this->CompreJuntoPromocao->delete()) {

			$json = array('sucesso' => true);

		}  else {

			$json = array('sucesso' => false,'mensagem' => 'Ocorreu um erro ao excluir esse desconto.');

		}

		$this->renderJson($json);

	}

	public function ajax_avaliar() {

		if ($this->request->is('post')) {

			$verifica_avaliacao = $this->Avaliacao->find('first', array(
				'conditions' => array(
					'Avaliacao.produto_id' => $this->request->data['Avaliacao']['produto_id'],
					'Avaliacao.email' => $this->request->data['Avaliacao']['email']
				)
			));

			if (count($verifica_avaliacao) == 0) {

				$this->Avaliacao->create();

				if ($this->request->data['score'] == '') {
					$this->request->data['score'] = 0;
				}

				$produto_id = $this->request->data['Avaliacao']['produto_id'];
				$nome = $this->request->data['Avaliacao']['nome'];
				$email = $this->request->data['Avaliacao']['email'];
				$pontuacao = $this->request->data['score'];

				$dados = array(
					'produto_id' => $produto_id,
					'nome' => $nome,
					'email' => $email,
					'avaliacao' => $pontuacao
				);
				if ($this->Avaliacao->save($dados)) {

					$avaliacoes_produto = $this->Avaliacao->find('list', array(
						'conditions' => array(
							'Avaliacao.produto_id' => $produto_id,
						),
						'fields' => 'avaliacao'
					));

					$produto = $this->Produto->find('list', array(
						'conditions' => array(
							'Produto.id' => $produto_id,
						),
						'fields' => array(
							'avaliacao',
						),

					));

					$total_de_pontos = 0;
					foreach ($avaliacoes_produto as $pontos) {
						$total_de_pontos += $pontos;
					}

					$media = $total_de_pontos / count($avaliacoes_produto);

					$this->Produto->id = $produto_id;
					$this->Produto->saveField('avaliacao', $media, false);

					$json = array('sucesso' => true, 'media_avaliacao' => $media, 'produto_id' => $produto_id);
				} else {
					$json = array('sucesso' => false,'mensagem' => 'Ocorreu um erro ao avaliar esse produto. Por favor, tente novamente.');
				}
			} else {
				$json = array('sucesso' => false,'mensagem' => 'Você já avaliou esse produto.');

			}

			$this->renderJson($json);
		}

	}

	private function get_origens_fiscais() {
		return array(
			0 => '0 - Nacional, exceto as indicadas nos códigos 3 a 5',
			1 => '1 - Estrangeira - Importação direta, exceto a indicada no código 6',
			2 => '2 - Estrangeira - Adquirida no mercado interno, exceto a indicada no código 7',
			3 => '3 - Nacional, mercadoria ou bem com Conteúdo de Importação superior a 40%',
			4 => '4 - Nacional, cuja produção tenha sido feita em conformidade com os processos produtivos básicos de que tratam as legislações citadas nos Ajustes',
			5 => '5 - Nacional, mercadoria ou bem com Conteúdo de Importação inferior ou igual a 40%',
			6 => '6 - Estrangeira - Importação direta, sem similar nacional, constante em lista da CAMEX',
			7 => '7 - Estrangeira - Adquirida no mercado interno, sem similar nacional, constante em lista da CAMEX'
		);
	}

}
?>