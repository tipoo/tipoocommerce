<?php
App::uses('AppController', 'Controller');

class CaracteristicasValoresController  extends AppController {

	public function admin_index($id = null) {

		$conditions = array(
			'CaracteristicasValor.ativo' => true,
			'CaracteristicasValor.caracteristica_id' => $id
		);

		$order = array(
			'CaracteristicasValor.descricao' => 'ASC'
		);

		$this->paginate = array(
			'conditions' => $conditions,
			'order' => $order,
			'limit' => Configure::read('Sistema.Paginacao.limit')
		);
		$this->set('caracteristicasValores', $this->paginate());
		$this->set('caracteristicaId', $id);

	}

	public function admin_adicionar($id = null) {

		if ($this->request->is('post')) {

			$this->request->data['CaracteristicasValor']['caracteristica_id'] = $id;

			$this->CaracteristicasValor->create();
			if ($this->CaracteristicasValor->save($this->request->data)) {
				$this->Session->setFlash('Valor salvo com sucesso.', FLASH_SUCCESS);
				$this->backToPaginatorIndex();
			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar salvar os valores. Por favor, tente novamente.', FLASH_ERROR);
			}
		}

	}

	public function admin_editar($id = null) {
		$this->CaracteristicasValor->id = $id;

		if (!$this->CaracteristicasValor->exists()) {
			throw new NotFoundException('Valor inexistente.');
		}
		if ($this->request->is('post') || $this->request->is('put')) {

			if ($this->CaracteristicasValor->save($this->request->data)) {
				$this->Session->setFlash('Valor editado com sucesso.', FLASH_SUCCESS);
				$this->backToPaginatorIndex();
			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar editar o Valor. Por favor, tente novamente.', FLASH_ERROR);
			}
		} else {
			$this->request->data = $this->CaracteristicasValor->read(null, $id);
		}
	}

	public function admin_excluir($id = null) {
		$this->CaracteristicasValor->id = $id;

		if (!$this->CaracteristicasValor->exists()) {
			throw new NotFoundException('Valor inexistente.');
		}

		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->CaracteristicasValor->saveField('ativo', false, false)) {
				$this->Session->setFlash('Valor excluído com sucesso.', FLASH_SUCCESS);
				$this->backToPaginatorIndex();
			} else {
				$this->Session->setFlash('Ocorreu um erro ao tentar excluir o Valor. Por favor, tente novamente.', FLASH_ERROR);
			}
		}
	}

}
?>