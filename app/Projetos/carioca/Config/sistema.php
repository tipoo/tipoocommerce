<?php

$config = array(
	'Sistema' => array(
		'Paginacao' => array(
			'limit' => 10
		),
		'Catalogo' => array(
			'qtd_produtos_pagina' => 12,
			'qtd_inicial_produtos' => 12,
			'qtd_mais_produtos' => 4,
			'offset_rodape' => 342
		),
		'Checkout' => array(
			'PagSeguro' => array(
				'ativo' => false,
				'email' => '',
				'token' => '',
				'url_retorno' => ''
			)
		),
		'Recortes' => array(
			'thumb' => array(
				'w' => 65,
				'h' => 92
			),
			'small' => array(
				'w' => 103,
				'h' => 145
			),
			'normal' => array(
				'w' => 185,
				'h' => 262
			),
			'medium' => array(
				'w' => 418,
				'h' => 591
			),
			'big' => array(
				'w' => 708,
				'h' => 1000
			)
		),
		'Colecoes' => array(
			'Home' => array(
				'id' => 1
			)
		),
		'Paginas' => array(
			'Home' => array(
				'id' => 1
			)
		),
		'StatusPedidos' => array(
			'notificacaoStatus' => 'contato@sorellina.com.br'
		),
		'Contato' => array(
			'email' => 'contato@sorellina.com.br'
		)
	)
);

?>