<?php

$config = array(
	'Sistema' => array(
		'Paginacao' => array(
			'limit' => 10
		),
		'Catalogo' => array(
			'qtd_produtos_pagina' => 3,
			'qtd_inicial_produtos' => 3,
			'qtd_mais_produtos' => 3,
			'offset_rodape' => 300
		),
		'Checkout' => array(
			'PagSeguro' => array(
				'ativo' => true,
				'email' => 'pedro__savi@hotmail.com',
				'token' => '4EC10392958A47DCB84996CEEF9BA513',
				'url_retorno' => 'https://tipoocommerce.com.br/checkout/compra_finalizada'
			)
		),
		'Recortes' => array(
			'thumb' => array(
				'w' => 86,
				'h' => 63
			),
			'small' => array(
				'w' => 121,
				'h' => 89
			),
			'normal' => array(
				'w' => 231,
				'h' => 169
			),
			'medium' => array(
				'w' => 500,
				'h' => 365
			),
			'big' => array(
				'w' => 1000,
				'h' => 730
			)
		),
		'Colecoes' => array(
			'Home' => array(
				'id' => 1
			)
		),
		'Paginas' => array(
			'Home' => array(
				'id' => 1
			)
		),
		'StatusPedidos' => array(
			'notificacaoStatus' => 'thiagoalgo@gmail.com'
		),
		'Contato' => array(
			'email' => 'pedrosavi@gmail.com'
		)
	)
);

?>