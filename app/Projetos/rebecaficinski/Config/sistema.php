<?php

$config = array(
	'Sistema' => array(
		'Paginacao' => array(
			'limit' => 50
		),
		'Catalogo' => array(
			'qtd_inicial_produtos' => 12,
			'qtd_mais_produtos' => 4,
			'offset_rodape' => 342
		),
		'Checkout' => array(
			'PagSeguro' => array(
				'ativo' => true,
				'email' => 'rebecaficinski@gmail.com',
				'token' => 'A2522C20137E485BB8F04F131A81541C',
				'url_retorno' => 'http://rebecaficinski.com.br/checkout/compra_finalizada'
			)
		),
		'Colecoes' => array(
			'Home' => array(
				'id' => 1
			)
		),
		'Paginas' => array(
			'Home' => array(
				'id' => 1
			)
		),
		'StatusPedidos' => array(
			'notificacaoStatus' => 'contato@rebecaficinski.com.br'
		),
		'Contato' => array(
			'email' => 'contato@rebecaficinski.com.br'
		)
	)
);

?>