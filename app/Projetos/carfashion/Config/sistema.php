<?php

$config = array(
	'Sistema' => array(
		'Paginacao' => array(
			'limit' => 50
		),
		'Catalogo' => array(
			'qtd_inicial_produtos' => 3,
			'qtd_mais_produtos' => 3,
			'offset_rodape' => 300
		),
		'Checkout' => array(
			'PagSeguro' => array(
				'ativo' => false
			)
		),
		'Colecoes' => array(
			'Home' => array(
				'id' => 1
			)
		),
		'Paginas' => array(
			'Home' => array(
				'id' => 1
			)
		),
		'StatusPedidos' => array(
			'notificacaoStatus' => 'sac@carfashion.com.br'
		),
		'Contato' => array(
			'email' => 'sac@carfashion.com.br'
		)
	)
);

?>