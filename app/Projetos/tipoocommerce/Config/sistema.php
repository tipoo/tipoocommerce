<?php
$config = array(
	'Sistema' => array(
		'Paginacao' => array(
			'limit' => 50
		),
		'Catalogo' => array(
			'qtd_inicial_produtos' => 3,
			'qtd_mais_produtos' => 3,
			'offset_rodape' => 300
		),
		'Checkout' => array(
			'PagSeguro' => array(
				'ativo' => false,
				'email' => 'pedro__savi@hotmail.com',
				'token' => '4EC10392958A47DCB84996CEEF9BA513'
			)
		),
		'Colecoes' => array(
			'Home' => array(
				'id' => 1
			)
		),
		'Paginas' => array(
			'Home' => array(
				'id' => 1
			)
		),
		'StatusPedidos' => array(
			'notificacaoStatus' => 'thiagoalgo@gmail.com'
		),
		'Contato' => array(
			'email' => 'pedrosavi@gmail.com'
		)
	)
);

?>