<?php

$config = array(
	'Sistema' => array(
		'Paginacao' => array(
			'limit' => 50
		),
		'Catalogo' => array(
			'qtd_inicial_produtos' => 8,
			'qtd_mais_produtos' => 3,
			'offset_rodape' => 342
		),
		'Checkout' => array(
			'PagSeguro' => array(
				'ativo' => true,
				//'email' => 'luanaioppi@gmail.com',
				'email' => 'contato@lacelab.com.br',
				//'token' => '79DD81D9CC3D4057BA37D22DA105EFA6',
				'token' => '723D81E0930B44D5A5C75D7FE2501D66',
				'url_retorno' => 'http://lacelab.com.br/checkout/compra_finalizada'
			)
		),
		'Colecoes' => array(
			'Home' => array(
				'id' => 1
			)
		),
		'Paginas' => array(
			'Home' => array(
				'id' => 1
			)
		),
		'StatusPedidos' => array(
			'notificacaoStatus' => 'contato@lacelab.com.br'
		),
		'Contato' => array(
			'email' => 'contato@lacelab.com.br'
		)
	)
);

?>