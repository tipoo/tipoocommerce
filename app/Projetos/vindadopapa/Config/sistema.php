<?php

$config = array(
	'Sistema' => array(
		'Paginacao' => array(
			'limit' => 10
		),
		'Catalogo' => array(
			'qtd_produtos_pagina' => 9,
			'qtd_inicial_produtos' => 9,
			'qtd_mais_produtos' => 3,
			'offset_rodape' => 0
		),
		'Checkout' => array(
			'PagSeguro' => array(
				'ativo' => false,
				'email' => '',
				'token' => ''
			)
		),
		'Recortes' => array(
			'thumb' => array(
				'w' => 65,
				'h' => 65
			),
			'small' => array(
				'w' => 100,
				'h' => 100
			),
			'normal' => array(
				'w' => 262,
				'h' => 175
			),
			'medium' => array(
				'w' => 500,
				'h' => 500
			),
			'big' => array(
				'w' => 1000,
				'h' => 1000
			)
		),
		'Colecoes' => array(
			'Home' => array(
				'id' => 1
			)
		),
		'Paginas' => array(
			'Home' => array(
				'id' => 1
			)
		),
		'StatusPedidos' => array(
			'notificacaoStatus' => 'thiagoalgo@gmail.com'
		),
	)
);

?>