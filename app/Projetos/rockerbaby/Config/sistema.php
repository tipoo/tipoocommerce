<?php

$config = array(
	'Sistema' => array(
		'Paginacao' => array(
			'limit' => 10
		),
		'Catalogo' => array(
			'qtd_inicial_produtos' => 6,
			'qtd_mais_produtos' => 3,
			'offset_rodape' => 700
		),
		'Checkout' => array(
			'PagSeguro' => array(
				'ativo' => true,
				'email' => 'contato@rockerbaby.com.br',
				'token' => '1A78F2752B864C30AD874992C44C9297',
				'url_retorno' => 'http://www.rockerbaby.com.br/checkout/compra_finalizada'
			)
		),
		'Colecoes' => array(
			'Home' => array(
				'id' => 1
			)
		),
		'Paginas' => array(
			'Home' => array(
				'id' => 1
			)
		),
		'StatusPedidos' => array(
			'notificacaoStatus' => 'contato@rockerbaby.com.br'
		),
		'Contato' => array(
			'email' => 'contato@rockerbaby.com.br'
		)
	)
);

?>