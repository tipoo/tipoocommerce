<?php

$config = array(
	'Sistema' => array(
		'Paginacao' => array(
			'limit' => 50
		),
		'Catalogo' => array(
			'qtd_inicial_produtos' => 12,
			'qtd_mais_produtos' => 4,
			'offset_rodape' => 342
		),
		'Checkout' => array(
			'PagSeguro' => array(
				'ativo' => false,
				'email' => '',
				'token' => '',
				'url_retorno' => ''
			)
		),
		'Colecoes' => array(
			'Home' => array(
				'id' => 1
			)
		),
		'Paginas' => array(
			'Home' => array(
				'id' => 1
			)
		),
		'StatusPedidos' => array(
			'notificacaoStatus' => 'sac@joalheriabiz.com.br'
		),
		'Contato' => array(
			'email' => 'sac@joalheriabiz.com.br'
		)
	)
);

?>