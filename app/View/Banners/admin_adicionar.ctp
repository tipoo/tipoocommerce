<?php
	$this->Html->script(array('jquery.numeric', 'Views/Banners/admin_adicionar'), array('inline' => false));
?>

<ol class="breadcrumb">
	<li><?php echo $this->Html->link('Gestão de Banners', array('controller' => 'bannerTipos', 'action' => 'backToPaginatorIndex', 'admin' => true)); ?></li>
	<li><?php echo $this->Html->link('Banners', array('action' => 'backToPaginatorIndex', 'admin' => true)); ?></li>
	<li class="active">Adicionar</li>
</ol>

<div class="well">
	<h4><?php echo $bannerTipo['BannerTipo']['descricao']?></h4>
	<p><strong>Tipo: </strong> <?php echo $tipo[$bannerTipo['BannerTipo']['tipo']]?></p>
	<p><strong>Limite de banners: </strong> <?php echo $bannerTipo['BannerTipo']['qtd']?></p>
	<p><strong>Chave: </strong><?php echo $bannerTipo['BannerTipo']['chave']?></p>
</div>
<?php
	if ($bannerTipo['BannerTipo']['categoria_id'] != '') {
?>
		<p><strong>Categoria: </strong> <?php echo $bannerTipo['Categoria']['descricao']?></p>
<?php
	}
?>

<?php
	if ($bannerTipo['BannerTipo']['marca_id'] != '') {
?>
		<p><strong>Marca: </strong> <?php echo $bannerTipo['Marca']['descricao']?></p>
<?php
	}
?>

<br />

<?php
	echo $this->Form->create('Banner', array('type' => 'file'));
	echo $this->Form->input('descricao', array('row' => true, 'div' => 'col-md-4','type' => 'text', 'label' => 'Título*'));
	echo $this->Form->input('label', array('row' => true, 'div' => 'col-md-4','type' => 'text', 'label' => 'Label'));
	echo $this->Form->input('texto', array('row' => true, 'div' => 'col-md-4','type' => 'textarea', 'label' => 'Descrição'));
	echo $this->Form->input('data_inicio',array('row' => true, 'div' => 'col-md-2','type' => 'text' ,'label' => 'Data/Hora de Início*', 'value' => $this->Formatacao->dataHora(date('Y-m-d H:i:s'))));
	echo $this->Form->input('data_fim',array('row' => true, 'div' => 'col-md-2','type' => 'text' ,'label' => 'Data/Hora de Fim'));
	echo $this->Form->input('imagem', array('row' => true, 'div' => 'col-md-4','type' => 'file', 'label' => 'Imagem*'));
	echo $this->Form->input('nova_aba', array('row' => true, 'div' => 'col-md-3', 'type' => 'checkbox', 'value' => false, 'label' => 'Abrir em uma nova aba'));
	echo $this->Form->input('link', array('row' => true, 'div' => 'col-md-8','placeholder' => 'http://'));
	echo $this->Form->actions();
	echo $this->Form->end();
?>
