<?php
	$this->Html->script(array('Views/Banners/admin_index'), array('inline' => false));
?>

<ol class="breadcrumb">
	<li><?php echo $this->Html->link('Gestão de Banners', array('controller' => 'bannerTipos', 'action' => 'backToPaginatorIndex', 'admin' => true)); ?> <span class="divider"></li>
	<li class="active">Banners</li>
</ol>

	<?php echo $this->Form->input('banner_tipo_id', array('type' => 'hidden', 'value' => $banner_tipo_id)); ?>
<div class="well">
	<h4><?php echo $bannerTipo['BannerTipo']['descricao']?></h4>
	<p><strong>Tipo: </strong> <?php echo $tipo[$bannerTipo['BannerTipo']['tipo']]?></p>
	<p><strong>Chave: </strong><?php echo $bannerTipo['BannerTipo']['chave']?></p>
	<p>
		<strong>Limite de banners: </strong> 
		<?php 
			if ( $bannerTipo['BannerTipo']['qtd'] != '') {

				 echo $bannerTipo['BannerTipo']['qtd']
		 ?>
				<div class="alert alert-warning">
					<strong>ATENÇÃO : </strong> No caso da quantidade de banners cadastrados para mesma data de exibição for maior que o limite de banners, prevalece o que foi cadastrado primeiro.
				</div>
		<?php
			} else {
				echo 'Sem limite';
			}
		 ?>
	 </p>


</div>
<?php
	if ($bannerTipo['BannerTipo']['categoria_id'] != '') {
?>
		<p><strong>Categoria: </strong> <?php echo $bannerTipo['Categoria']['descricao']?></p>
<?php
	}
?>

<?php
	if ($bannerTipo['BannerTipo']['marca_id'] != '') {
?>
		<p><strong>Marca: </strong> <?php echo $bannerTipo['Marca']['descricao']?></p>
<?php
	}
?>

<br />

<div class="alert alert-info">
	<i class="glyphicon glyphicon-info-sign"></i> <strong>ORDENAÇÃO: </strong> Arraste para ordenar os Banners.
</div>

<div class="table-responsive">
	<table class="table table-striped table-hover">
		<thead>
			<tr>
				<th class="col-md-1 text-center">#</th>
				<th class="col-xs-8 col-sm-8 col-md-10">Banner</th>
				<th class="visible-xs visible-sm"></th>
				<th class="visible-xs visible-sm"></th>
				<th class="text-center">
					<?php echo $this->Html->link('<span class="glyphicon glyphicon-plus"></span>', array('action' => 'adicionar', $banner_tipo_id), array('class' => 'btn btn-sm btn-primary', 'escape' => false,'data-placement' => 'left', 'rel' => 'tooltip', 'title' => 'Adicionar Novo')); ?>
				</th>
			</tr>
		</thead>

		<tbody id="sortable">
			<?php
				foreach ($banners as $banner) {
			?>
				<tr class="ui-state-default" data-id="<?php echo $banner['Banner']['id'] ; ?>">
					<td class="text-center"><?php echo $banner['Banner']['id'] ?></td>
					<td><?php echo $banner['Banner']['descricao'] ?></td>

					<td class="visible-xs visible-sm text-center seta-ordenacao" >
						<div class="ordenacao-desce " data-indice="<?php echo $banner['Banner']['id']; ?>"><span class="glyphicon glyphicon-arrow-down"></span></div>
					</td>
					<td class="visible-xs visible-sm text-center seta-ordenacao" >
						<div class="ordenacao-sobe " data-indice="<?php echo $banner['Banner']['id']; ?>"><span class="glyphicon glyphicon-arrow-up"></span></div>
					</td>

					<td class="text-center">
						<div class="btn-group">
							<button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu pull-right text-left" role="menu">
								<li><?php echo $this->Html->link('<span class="glyphicon glyphicon-edit"></span> Editar', array('action' => 'editar', $banner['Banner']['id'], $banner_tipo_id), array('escape' => false));?> </li>
								<li><?php echo $this->Form->postLink('<span class="glyphicon glyphicon-trash"></span> Excluir', array('action' => 'excluir', $banner['Banner']['id'], $banner_tipo_id), array('escape' => false), 'Deseja realmente excluir o Banner #' . $banner['Banner']['id'] . '?'); ?> </li>
							</ul>
						</div>
					</td>
				</tr>
			<?php
				}
			?>
		</tbody>
	</table>
</div>

<?php echo $this->element('Paginacao/admin_paginacao'); ?>