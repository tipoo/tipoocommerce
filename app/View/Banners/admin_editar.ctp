<?php
	$this->Html->script(array('jquery.numeric', 'Views/Banners/admin_editar'), array('inline' => false));
?>

<ol class="breadcrumb">
	<li><?php echo $this->Html->link('Gestão de Banners', array('controller' => 'bannerTipos', 'action' => 'backToPaginatorIndex', 'admin' => true)); ?></li>
	<li><?php echo $this->Html->link('Banners', array('action' => 'backToPaginatorIndex', 'admin' => true)); ?></li>
	<li class="active">Editar</li>
</ol>

<div class="well">
	<h4><?php echo $bannerTipo['BannerTipo']['descricao']?></h4>
	<p><strong>Tipo: </strong> <?php echo $tipo[$bannerTipo['BannerTipo']['tipo']]?></p>
	<p><strong>Limite de banners: </strong> <?php echo $bannerTipo['BannerTipo']['qtd']?></p>
	<p><strong>Chave: </strong><?php echo $bannerTipo['BannerTipo']['chave']?></p>
</div>
<?php
	if ($bannerTipo['BannerTipo']['categoria_id'] != '') {
?>
		<p><strong>Categoria: </strong> <?php echo $bannerTipo['Categoria']['descricao']?></p>
<?php
	}
?>

<?php
	if ($bannerTipo['BannerTipo']['marca_id'] != '') {
?>
		<p><strong>Marca: </strong> <?php echo $bannerTipo['Marca']['descricao']?></p>
<?php
	}
?>

<br />

<?php

	$data_inicio = $this->request->data['Banner']['data_inicio'] == '' ? null : $this->Formatacao->dataHora($this->request->data['Banner']['data_inicio']);
	$data_fim = $this->request->data['Banner']['data_fim'] == '' ? null : $this->Formatacao->dataHora($this->request->data['Banner']['data_fim']);
	echo $this->Form->create('Banner', array('type' => 'file'));
	echo $this->Form->input('descricao', array('row' => true, 'div' => 'col-md-4','type' => 'text', 'label' => 'Título*'));
	echo $this->Form->input('label', array('row' => true, 'div' => 'col-md-4','type' => 'text', 'label' => 'Label'));
	echo $this->Form->input('texto', array('row' => true, 'div' => 'col-md-4','type' => 'textarea', 'label' => 'Descrição'));
	echo $this->Form->input('data_inicio',array('row' => true, 'div' => 'col-md-2','type' => 'text' ,'label' => 'Data/Hora de Início*', 'value' => $data_inicio));
	echo $this->Form->input('data_fim',array('row' => true, 'div' => 'col-md-2','type' => 'text' ,'label' => 'Data/Hora de Fim', 'value' => $data_fim));
	echo $this->Form->input('imagem', array('row' => true, 'div' => 'col-md-4','type' => 'file', 'label' => 'Imagem*'));
?>
	<div class="control-group">
		<label class="control-label" for="BannerImagem"></label>
		<div class="controls">
			<img style="max-width: 300px; max-height: 300px" src="<?php echo $this->webroot ?>files/<?php echo PROJETO ?>/banners/<?php echo $this->request->data['Banner']['imagem'] ?>" />
		</div>
	</div>
	<br />
<?php
	echo $this->Form->input('nova_aba', array('row' => true, 'div' => 'col-md-3', 'type' => 'checkbox', 'label' => 'Abrir em uma nova aba', 'value' => false));
	echo $this->Form->input('link', array('row' => true, 'div' => 'col-md-8', 'placeholder' => 'http://'));
	echo $this->Form->actions();
	echo $this->Form->end();
?>
