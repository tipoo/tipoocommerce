<?php
	if (isset($frete_transportadoras)) {
?>
		<div class="calculo-frete">
			<?php
				if (count($frete_transportadoras) > 0) {
			?>
					<div class="calculo-frete-transportadoras">
						<h5>Opções de frete disponíveis para este cep</h5>
						<ul class="transportadoras">
							<?php
								foreach ($frete_transportadoras as $frete_transportadora) {
									$transportadora_nome = $frete_transportadora['FreteTransportadora']['descricao'];
									$transportadora_prazo_entrega = $frete_transportadora['FreteTransportadora']['prazo_entrega_frete'];

									if ($transportadora_valor_frete = $frete_transportadora['FreteTransportadora']['valor_frete'] <= 0) {
										$transportadora_valor_frete = 'Frete Grátis';
										$transportadora_valor_total_frete = $this->Formatacao->moeda($valor_sub_total);
									} else {
										$transportadora_valor_frete = $this->Formatacao->moeda($frete_transportadora['FreteTransportadora']['valor_frete']);
										$transportadora_valor_total_frete = $this->Formatacao->moeda($valor_sub_total + $frete_transportadora['FreteTransportadora']['valor_frete']);
									}
							?>
									<li class="transportadora transportadora-<?php echo strtolower(Inflector::slug($frete_transportadora['FreteTransportadora']['descricao'], '-')); ?>">
										<p class="transportadora-nome"><strong><?php echo $transportadora_nome; ?></strong></p>
										<p class="transportadora-valor-frete">Valor do frete: <span><?php echo $transportadora_valor_frete; ?></span></p>
										<p class="transportadora-valor-total-frete">Valor total com frete: <span><?php echo $transportadora_valor_total_frete; ?></span></p>
										<p class="transportadora-prazo-entrega">Prazo de entrega: <span><?php echo $transportadora_prazo_entrega; ?></span> <span>dia(s) útil(eis)</span></p>
									</li>
							<?php
								}
							?>
						</ul>
						<p class="msg-prazo-entrega"><strong>Atenção!</strong> O prazo de entrega terá início somente após a confirmação do pagamento</p>
					</div>
			<?php
				} else {
			?>
					<div class="carrinho-msg-regiao-cep-invalido">
						<h5>Opções de entrega para o cep selecionado</h5>
						<p>Não possuimos entrega para sua região, por favor digite um novo cep.</p>
					</div>
			<?php
				}
			?>
		</div>
<?php
	}
?>