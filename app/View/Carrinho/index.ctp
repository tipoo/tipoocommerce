<?php
	$this->Html->script(array(
			'validate/jquery.validate.min',
			'validate/messages_ptbr',
			'jquery.maskedinput.min',
			'jquery.numeric',
			'Controles/tipoo.carrinho'
		),
		array('inline' => false)
	);
?>

<div id="carrinho-container">
	<h1>Carrinho de compras</h1>

	<div class="carrinho-content">
		<?php
			if (!count($skus)) {
		?>
				<div class="carrinho-vazio">
					<p>Seu carrinho ainda está vazio</p>
					<?php echo $this->Html->link('Ir às compras', '/');?>
				</div>
		<?php
			} else {
		?>
				<div class="carrinho-msg-estoque">
					<!-- Mensagem Estoque -->
					<?php
						if (count($msg_skus)) {
					?>
							<div class="carrinho-msg-skus">
								<p>Devido a mudanças na disponibilidade em estoque ocorreram as seguintes alterações no seu carrinho:</p>
								<ul>
									<?php
										foreach ($msg_skus as $msg) {
									?>
											<li><?php echo $msg ?></li>
									<?php
										}
									?>
								</ul>
							</div>
					<?php
						}
					?>
					<!-- .end Mensagem Estoque -->

					<!-- Mensagem Pré-Venda -->
					<?php
						$skus_em_pre_venda = $this->Carrinho->skus_em_pre_venda();
						if ($skus_em_pre_venda) {
					?>
							<div class="carrinho-pre-venda">
								<p>No seu carrinho de compras há os seguintes produtos em pré-venda (sob-encomenda):</p>
								<ul>
									<?php
										foreach ($skus_em_pre_venda as $sku_em_pre_venda) {
									?>
											<li><?php echo $sku_em_pre_venda['Produto']['descricao'] . ' - ' . $sku_em_pre_venda['Sku']['descricao'] ?></li>
									<?php
										}
									?>
								</ul>
								<p>A previsão de entrega para o seu pedido é <?php echo $this->Formatacao->data($this->Carrinho->data_entrega_pre_venda()); ?></p>
							</div>
					<?php
						}
					?>
					<!-- .end Mensagem Pré-Venda -->
				</div>
				<div class="botoes top">
					<?php echo $this->Html->link('Continuar comprando', '/', array('class' => 'continuar-comprando'));?>
					<?php echo $this->Html->link('Finalizar compra', str_replace('http://', 'http://', $this->Html->url(array('controller' => 'checkout', 'action' => 'entrega', '?' => array('redirect' => '/checkout/entrega')), true)), array('class' => 'finalizar-compra')) ?>
				</div>

				<table class="carrinho-skus">
					<thead>
						<tr>
							<th colspan="2" class="col-produtos">Produto</th>
							<th class="col-quantidade">Quantidade</th>
							<th class="col-val-unit">Valor Unitário</th>
							<th class="col-val-total">Valor Total</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php

							$count_sku = 0;
							foreach ($skus as $sku) {
								if (isset($sku['Imagem'][0]['imagem'])) {
									$imagem_produto = $sku['Imagem'][0]['imagem'];
								} else {
									$imagem_produto = $sku['Produto']['Imagem'][0]['imagem'];
								}
						?>
								<tr class="produtos-skus">
									<td class="col-img">
										<img src="<?php echo $this->webroot ?>files/<?php echo PROJETO ?>/produtos/<?php echo $db_config['Configuracao']['img_carrinho'] ?>_<?php echo $imagem_produto ?>">
									</td>
									<td class="col-produtos">
										<span><?php echo $sku['Produto']['descricao'] . ' - ' . $sku['Sku']['descricao'] ?></span>

										<!-- Mensagem Desconto -->
										<?php
											if (isset($sku['Promocao']['melhor_desconto_produto']['promocao'])) {
										?>
												<small class="mensagem-desconto melhor-desconto-produto">Desconto: "<?php echo $sku['Promocao']['melhor_desconto_produto']['promocao']['Promocao']['descricao'] ?>"</small>
										<?php
											}
										?>

										<?php
											if (isset($sku['Promocao']['melhor_compre_junto']['promocao'])) {
										?>
												<small class="mensagem-desconto melhor-desconto-produto compre-junto">Desconto: "<?php echo $sku['Promocao']['melhor_compre_junto']['promocao']['Promocao']['descricao'] ?>"</small>
										<?php
											}
										?>

										<?php
											if (isset($sku['Promocao']['melhor_leve_x_pague_y']['promocao'])) {
										?>
												<small class="mensagem-desconto melhor-leve-x-pague-y">Desconto: "<?php echo $sku['Promocao']['melhor_leve_x_pague_y']['promocao']['Promocao']['descricao'] ?>"</small>
										<?php
											}
										?>

										<!-- .end Mensagem Desconto -->

									</td>

									<td class="col-quantidade">
										<?php echo $this->Form->input('quantidade', array('label' => false, 'class' => 'input-quantidade', 'id' => 'quantidade-' . $sku['Sku']['id'], 'data-id' => $sku['Sku']['id'], 'value' => $sku['qtd'])); ?>
									</td>

									<td class="col-val-unit">
										<?php
											if (isset($sku['Promocao']['melhor_leve_x_pague_y']['promocao']) || isset($sku['Promocao']['melhor_desconto_produto']['promocao']) || isset($sku['Promocao']['melhor_compre_junto']['promocao'])) {
										?>
												<small class="desconto"><?php echo $sku['Produto']['preco_de'] != '' ? $this->Formatacao->moeda($sku['Produto']['preco_de']) : $this->Formatacao->moeda($sku['Produto']['preco_por']); ?></small>
										<?php
												echo $this->Formatacao->moeda($sku['preco_unitario_desconto']);
											} else {
												if ($sku['Produto']['preco_de'] != '') {
										?>
													<small class="desconto"><?php echo $this->Formatacao->moeda($sku['Produto']['preco_de']); ?></small>
										<?php
												}
												echo $this->Formatacao->moeda($sku['preco_unitario']);
											}
										?>

									</td>

									<td class="col-val-total">
										<?php echo $this->Formatacao->moeda($sku['preco_total']); ?>
									</td>

									<td>
										<?php echo $this->Html->link('Remover', '#', array('class' => 'remover-sku', 'data-id' => $sku['Sku']['id'])) ?>
									</td>
								</tr>

								<!-- Serviço -->

								<?php
									if (count($sku['Produto']['Servico']) > 0) {
								?>
										<tr class="servicos-skus">
											<td></td>
											<td class="col-produtos-servicos" colspan="3">

												<div class="produto-servicos">
													<ul class="servicos">
														<?php
															foreach ($sku['Produto']['Servico'] as $servico) {
														?>
																<li>

														<?php
																if (count($servico['ServicoAnexo']) > 0) {
														?>
																	<strong><?php echo $servico['nome']; ?> (<?php echo $servico['valor'] ? '+ ' . $this->Formatacao->moeda($servico['valor']) : 'Grátis'; ?>)</strong>

																	<?php
																		foreach ($servico['ServicoAnexo'] as $key_servicos => $anexo) {
																			if ($anexo['tipo'] == 'T') {
																	?>
																				<?php
																					if (!isset($sku['Servico'][$servico['id']]['ServicoAnexo'][$anexo['id']])) {
																						$ok = '<button class="t-adicionar-anexo" data-sku-id="' . $sku['Sku']['id'] . '" data-anexo-id="' . $anexo['id'] . '">Ok</button>';
																						echo $this->Form->input('Sku.' . $sku['Sku']['id'] . '.Servico.' . $key_servicos . '.anexo', array('label' => $anexo['nome'], 'class' => 'input-anexo', 'after' => $ok));
																					} else {
																				?>
																						<div class="input text">
																							<label for="Sku<?php echo $sku['Sku']['id'] ?>Servico<?php echo $key_servicos; ?>Anexo"><?php echo $anexo['nome'] ?>:</label>
																							<span class="anexo-valor"><?php echo $sku['Servico'][$servico['id']]['ServicoAnexo'][$anexo['id']]['AnexoValor']['valor'] ?></span>
																							<button class="t-remover-anexo" data-sku-id="<?php echo $sku['Sku']['id'] ?>" data-servico-id="<?php echo $servico['id'] ?>" data-anexo-id="<?php echo $anexo['id'] ?>">x</button>
																						</div>
																				<?php
																					}
																				?>

																	<?php
																			} else if ($anexo['tipo'] == 'S') {
																				if (!isset($sku['Servico'][$servico['id']]['ServicoAnexo'][$anexo['id']])) {
																					$ok = '<button class="t-adicionar-anexo" data-sku-id="' . $sku['Sku']['id'] . '" data-anexo-id="' . $anexo['id'] . '">Ok</button>';
																					$valores_possiveis = explode(PHP_EOL, $anexo['valores_possiveis']);
																					$options = array();
																					foreach ($valores_possiveis as $valor_possivel) {
																						$options[$valor_possivel] = $valor_possivel;
																					}

																					echo $this->Form->input('Sku.' . $sku['Sku']['id'] . '.Servico.' . $key_servicos . '.anexo', array('type' => 'select', 'empty' => 'Selecione', 'options' => $options, 'label' => $anexo['nome'], 'class' => 'input-anexo', 'after' => $ok));
																				} else {
																		?>
																					<div class="input text">
																						<label for="Sku<?php echo $sku['Sku']['id'] ?>Servico<?php echo $key_servicos; ?>Anexo"><?php echo $anexo['nome'] ?>:</label>
																						<span class="anexo-valor"><?php echo $sku['Servico'][$servico['id']]['ServicoAnexo'][$anexo['id']]['AnexoValor']['valor'] ?></span>
																						<button class="t-remover-anexo" data-sku-id="<?php echo $sku['Sku']['id'] ?>" data-servico-id="<?php echo $servico['id'] ?>" data-anexo-id="<?php echo $anexo['id'] ?>">x</button>
																					</div>
																		<?php
																				}
																			}
																		}
																	?>
															<?php
																} else {
															?>
																	<?php
																		if (!isset($sku['Servico'][$servico['id']])) {
																	?>
																			<button class="t-adicionar-servico" data-sku-id="<?php echo $sku['Sku']['id'] ?>" data-servico-id="<?php echo $servico['id'] ?>"><?php echo $servico['nome']; ?> (<?php echo $servico['valor'] ? '+ ' . $this->Formatacao->moeda($servico['valor']) : 'Grátis'; ?>)</button>
																	<?php
																		} else {
																	?>
																			<span><?php echo $servico['nome']; ?> (<?php echo $servico['valor'] ? '+ ' . $this->Formatacao->moeda($servico['valor']) : 'Grátis'; ?>)</span><button class="t-remover-servico" data-sku-id="<?php echo $sku['Sku']['id'] ?>" data-servico-id="<?php echo $servico['id'] ?>">x</button>
																	<?php
																		}
																	?>
															<?php
																}
															?>

																</li>
														<?php
															}
														?>
													</ul>
												</div>
											</td>
											<td>
												<ul>
													<?php
														foreach ($sku['Produto']['Servico'] as $servico) {
															if (isset($sku['Servico'][$servico['id']])) {
													?>
																<li class="servico-preco servico-<?php echo $servico['id']; ?>">
																	<?php echo $sku['Servico'][$servico['id']]['valor'] ? '+ ' . $this->Formatacao->moeda($sku['Servico'][$servico['id']]['valor']  * $sku['qtd']) : 'Grátis'; ?>
																</li>
													<?php
															} else {
													?>
																<li class="servico-preco servico-<?php echo $servico['id']; ?> vazio"></li>
													<?php
															}
														}
													?>
												</ul>
											</td>
											<td></td>
										</tr>
								<?php
									}
								?>

								<!-- .end Serviço -->
						<?php
								$count_sku++;
							}
						?>

						<tr class="cupom-desconto">
							<td class="col-form-cupom" colspan="4">
								<?php
									echo $this->Form->create('Cupom');
									echo $this->Form->input('codigo', array('type' => 'text', 'class' => 'codigo-cupom-desconto', 'label' => 'Cupom de desconto', 'value' => $cupom));
									echo $this->Form->end('Aplicar');
								?>

								<!-- Mensagem Cupom -->
								<?php
									if (!empty($cupom)) {
										if (isset($valor_sub_total['Promocao']['cupom_desconto']) && ($valor_sub_total['Promocao']['cupom_desconto']['cupom_utilizado'] && !$valor_sub_total['Promocao']['cupom_desconto']['cupom_reutilizavel'])) {
								?>
											<small class="mensagem-desconto error cupom-desconto">
												Você já utilizou este cupom em uma compra anterior.
												<span>Obs: Só é permitido um uso por cliente.</span>
											</small>
								<?php
										} else if (!isset($valor_sub_total['Promocao']['cupom_desconto']) && $usuarioAutenticado != '') {
								?>
											<small class="mensagem-desconto error cupom-desconto">Cupom inválido</small>
											<ul class="mensagem-desconto error cupom-desconto">
												<li>Verifique se você digitou corretamente o código do cupom.</li>
												<li>Verifique se as condições do carrinho são válidas para este cupom.</li>
												<li>Verifique a data de validade do cupom.</li>
											</ul>
								<?php
										}
									}
								?>
								<!-- .end Mensagem Cupom -->
							</td>
							<td class="col-val-cupom">
								<?php
									if (isset($valor_sub_total['Promocao']['cupom_desconto']) &&  (!$valor_sub_total['Promocao']['cupom_desconto']['cupom_utilizado'] || $valor_sub_total['Promocao']['cupom_desconto']['cupom_reutilizavel'])) {
								?>
										<span>-<?php echo $this->Formatacao->moeda($valor_sub_total['Promocao']['cupom_desconto']['valor_desconto']); ?></span>
								<?php
									} else {
								?>
										<span>R$ 0,00</span>
								<?php
									}
								?>
							</td>
							<td class="col-vazio"></td>
						</tr>
					</tbody>
					<tfoot>
						<tr class="total">
							<td colspan="4" class="col-label-total">
								Total
								<?php
									if (isset($valor_sub_total['Promocao']['melhor_desconto_pedido']['promocao'])) {
								?>
										<small class="melhor-desconto-pedido">Desconto: "<?php echo $valor_sub_total['Promocao']['melhor_desconto_pedido']['promocao']['Promocao']['descricao'] ?>"</small>
								<?php
									}
								?>
							</td>
							<td class="col-total">
								<?php
									if (isset($valor_sub_total['Promocao']['melhor_desconto_pedido']['promocao'])) {
								?>
										<small class="col-val-desconto-pedido">
											<span data-valor-total="<?php echo $valor_sub_total['valor_produtos']; ?>"><?php echo $this->Formatacao->moeda($valor_sub_total['valor_produtos']); ?></span>
										</small>
								<?php
									}
								?>
									<span data-valor-total="<?php echo $valor_sub_total['valor_total']; ?>"><?php echo $this->Formatacao->moeda($valor_sub_total['valor_total']); ?></span>
							</td>
							<td></td>
						</tr>
					</tfoot>
				</table>

				<div class="botoes bottom">
					<?php echo $this->Html->link('Continuar comprando', '/', array('class' => 'continuar-comprando'));?>
					<?php echo $this->Html->link('Finalizar compra', str_replace('http://', 'http://', $this->Html->url(array('controller' => 'checkout', 'action' => 'entrega', '?' => array('redirect' => '/checkout/entrega')), true)), array('class' => 'finalizar-compra')) ?>
				</div>

				<!-- Calculo do frete -->
				<div class="frete">
					<div class="calcular-frete">
						<h4>Simular o valor do frete </h4>
						<?php
							echo $this->Form->create('Frete');
							echo $this->Form->input('calc_valor_frete', array('type' => 'text', 'class' => 'calc-valor-frete', 'label' => false, 'value' => $cep));
							echo $this->Form->end('Calcular');
						?>
						<a href="http://www.buscacep.correios.com.br/" target="_blank">Não sei meu CEP</a>
					</div>

					<div class="t-frete-transportadoras">
						<!-- Ajax -->
					</div>
				</div>
				<!-- .end Calculo do frete -->
			<?php
				}
			?>
	</div>
</div>




