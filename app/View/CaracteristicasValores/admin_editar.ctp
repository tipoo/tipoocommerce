<?php
	$this->Html->script(array('Views/CaracteristicasValores/admin_editar'), array('inline' => false));
?>

<ol class="breadcrumb">
	<li ><?php echo $this->Html->link('Categorias', array('controller' => 'categorias', 'action' => 'backToPaginatorIndex', 'admin' => true)); ?></li>
	<li><?php echo $this->Html->link('Características', array('controller' => 'caracteristicas', 'action' => 'backToPaginatorIndex', 'admin' => true)); ?></li>
	<li><?php echo $this->Html->link('Valores', array('action' => 'backToPaginatorIndex', 'admin' => true)); ?></li>
	<li class="active">Editar</li>
</ol>

<?php
	echo $this->Form->create('CaracteristicasValor');
	echo $this->Form->input('descricao', array('row' => true, 'div' => 'col-md-4', 'label' => 'Descrição*'));
	echo $this->Form->actions();
	echo $this->Form->end();
?>