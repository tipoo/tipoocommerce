<ol class="breadcrumb">
	<li class="active">Campos Complementares</li>
</ol>

<div class="table-responsive">
	<table class="table table-striped table-hover">
		<thead>
			<tr>
				<th class="col-md-1 text-center">#</th>
				<th class="col-md-4">Descrição</th>
				<th class="col-md-3">Referente à</th>
				<th class="col-md-3">Tipo</th>
				<th class="text-center"><?php echo $this->Html->link('<span class="glyphicon glyphicon-plus"></span>', array('action' => 'adicionar'), array('class' => 'btn btn-primary btn-sm','data-placement' => 'left', 'escape' => false, 'rel' => 'tooltip', 'title' => 'Adicionar Novo')); ?></th>
			</tr>
		</thead>

		<tbody>
			<?php
				foreach ($campos_complementares as $campo) {
			?>
				<tr>
					<td class="text-center"><?php echo $campo['CamposComplementar']['id'] ?></td>
					<td><?php echo $campo['CamposComplementar']['descricao'] ?></td>
					<td><?php echo $referencia[$campo['CamposComplementar']['referente']]; ?></td>
					<td><?php echo $tipo[$campo['CamposComplementar']['tipo']]; ?></td>
					<td class="text-center">
						<div class="btn-group">
							<button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu pull-right text-left" role="menu">
								<li><?php echo $this->Html->link('<span class="glyphicon glyphicon-edit"></span> Editar', array('action' => 'editar', $campo['CamposComplementar']['id']), array('escape' => false)); ?></li>
								<li><?php echo $this->Form->postLink('<span class="glyphicon glyphicon-trash"></span> Excluir', array('action' => 'excluir', $campo['CamposComplementar']['id']), array('escape' => false), 'Deseja realmente excluir o campo complementar #' . $campo['CamposComplementar']['id'] . '?');?></li>
								<?php
									if ($campo['CamposComplementar']['tipo'] == 'L') {
								?>
										<li><?php echo $this->Html->link('<span class="glyphicon "></span>Valores', array('controller' => 'camposComplementaresValores', 'action' => 'index', $campo['CamposComplementar']['id']), array('escape' => false));?></li>
								<?php
									}
								?>
							</ul>
						</div>
					</td>
				</tr>
			<?php
				}
			?>
		</tbody>
	</table>
</div>

<?php echo $this->element('Paginacao/admin_paginacao'); ?>