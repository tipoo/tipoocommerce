<?php
	$this->Html->script(array('Views/CamposComplementares/admin_editar'), array('inline' => false));
?>

<ol class="breadcrumb">
	<li><?php echo $this->Html->link('Campos Complementares', array('action' => 'backToPaginatorIndex', 'admin' => false)); ?></li>
	<li class="active">Editar</li>
</ol>

<div class="alert alert-warning">
	<i class="glyphicon glyphicon-exclamation-sign"></i> <strong>Atenção!</strong> Para editar campos bloqueados, por favor entre em contato com o suporte técnico.
</div>

<?php
	echo $this->Form->create('CamposComplementar');
	echo $this->Form->input('nao_editavel', array('disabled' => true, 'placeholder' => $referencia[$campoComplementar['CamposComplementar']['referente']], 'row' => true, 'div' => 'col-md-4','type' => 'text', 'label' => 'Referente à :'));
	echo $this->Form->input('nao_editavel', array('disabled' => true, 'placeholder' => $tipo[$campoComplementar['CamposComplementar']['tipo']], 'row' => true, 'div' => 'col-md-4','type' => 'text', 'label' => 'Tipo :'));

	if ($campoComplementar['CamposComplementar']['mascara'] != '') {

		echo $this->Form->input('nao_editavel', array('disabled' => true, 'placeholder' => $mascara[$campoComplementar['CamposComplementar']['mascara']], 'row' => true, 'div' => 'col-md-4','type' => 'text', 'label' => 'Máscara :'));

	}

	echo $this->Form->input('descricao', array( 'row' => true, 'div' => 'col-md-4', 'label' => 'Descrição*'));
	echo $this->Form->input('campo_obrigatorio', array('row' => true, 'div' => 'col-md-4', 'type' => 'checkbox', 'label' => 'Campo Obrigatório'));
	echo $this->Form->actions();
	echo $this->Form->end();
?>
