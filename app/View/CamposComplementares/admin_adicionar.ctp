<?php
	$this->Html->script(array('Views/CamposComplementares/admin_adicionar'), array('inline' => false));
?>

<ol class="breadcrumb">
	<li><?php echo $this->Html->link('Campos Complementares', array('action' => 'backToPaginatorIndex', 'admin' => false)); ?></li>
	<li class="active">Adicionar</li>
</ol>
<?php
	echo $this->Form->create('CamposComplementar');
	echo $this->Form->input('descricao', array('row' => true, 'div' => 'col-md-4', 'label' => 'Descrição*'));
	echo $this->Form->input('campo_obrigatorio', array('row' => true, 'div' => 'col-md-4', 'type' => 'checkbox', 'label' => 'Campo Obrigatório'));
	echo $this->Form->input('referente', array('row' => true, 'div' => 'col-md-4', 'type' => 'select', 'options' => $referencia, 'empty' => 'Selecione', 'label' => 'Referente à*'));
	echo $this->Form->input('tipo', array('row' => true, 'div' => 'col-md-4', 'type' => 'select', 'options' => $tipo, 'empty' => 'Selecione', 'label' => 'Tipo*'));
?>
	<div class="campo-mascara">
		<?php
			echo $this->Form->input('mascara', array('row' => true, 'div' => 'col-md-4', 'type' => 'select', 'options' => $mascara, 'empty' => 'Selecione', 'label' => 'Máscara'));
		?>
	</div>
<?php
	echo $this->Form->actions();
	echo $this->Form->end();
?>