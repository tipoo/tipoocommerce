<?php
	echo $this->Html->script(array('Views/Vitrines/admin_ajax_adicionar_colecoes'), array('inline' => true));
?>

<?php
	echo $this->Form->create('ColecoesLocal');
	echo $this->Form->input('descricao', array('row' => true, 'div' => 'col-md-12', 'label' => 'Descrição'));
	echo $this->Form->input('rand', array('type' => 'checkbox', 'row' => true, 'div' => 'col-md-4', 'label' => 'Ordem Aleatória'));
	echo $this->Form->input('qtd_max_produtos', array('type' => 'text', 'row' => true, 'div' => 'col-md-3', 'label' => 'Qtd. Máxima', 'help' => array('text' => 'Quantidade máxima de produtos mostrados na página.', 'class' => 'col-md-12')));
	echo $this->Form->input('data_inicio', array('type' => 'text', 'row' => true, 'div' => 'col-md-4', 'label' => 'Data/Hora de Início*', 'value' => $this->Formatacao->dataHora(date('Y-m-d H:i:s'))));
	echo $this->Form->input('data_fim', array('type' => 'text', 'row' => true, 'div' => 'col-md-4', 'label' => 'Data/Hora de Fim'));
	echo $this->Form->input('colecao', array('row' => true, 'div' => 'localizar-colecao col-md-12', 'class' => 'typeahead', 'label' => 'Localizar coleção', 'placeholder' => 'Digite o nome do coleção', 'autocomplete' => 'off'));
	echo $this->Form->input('colecao_id', array('type' => 'hidden'));
	echo $this->Form->actions();
	echo $this->Form->end();
?>