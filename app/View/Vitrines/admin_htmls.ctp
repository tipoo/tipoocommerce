<?php
	$this->Html->css(array('Views/Vitrines/admin_htmls'), 'stylesheet', array('inline' => false));
?>

<ol class="breadcrumb">
	<li class="active"><?php echo $this->Html->link($breadcrumb, array('controller' => $htmlsBackToPaginatorIndex, 'action' => 'backToPaginatorIndex', 'admin' => true)); ?></li>
	<li class="active">Texto/Html</li>
</ol>

<?php

	if (count($chaves) == 0) {
?>
		<br>
		<div class="text-center">
			<span class="text-muted "> Nenhum registro encontrado.</span>
		</div>

<?php
	} else {
?>
		<?php
			foreach ($chaves as $key => $chave) {
		?>
				<?php
					$html = '';
					if (isset($chave[0]['html'])) {
						$html = $chave[0]['html'];
					}
				?>

				<h4 class="descricao-chave"><?php echo $key; ?></h4>
				<?php
					echo $this->Form->create('HtmlsLocal', array('url' => array('controller' => 'vitrines', 'action' => 'adicionar_htlm', 'admin' => true, $htmlsLocais[$model]['id'], $key, 'find_model_id' => $find_model_id)));
					if (isset($chave[0]['id'])) {
						echo $this->Form->input('id', array('value' => $chave[0]['id']));
					}
					echo $this->Form->input('html', array('row' => true, 'div' => 'col-md-12', 'label' => false, 'type' => 'textarea', 'value' => $html));
					echo $this->Form->actions();
					echo $this->Form->end();
				?>
		<?php
			}
		?>
<?php
	}
?>
