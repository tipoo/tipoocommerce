<?php
	$this->Html->css(array('typeahead.js-bootstrap', 'Views/Vitrines/admin_colecoes'), 'stylesheet', array('inline' => false));
	$this->Html->script(array('hogan-2.0.0.js','typeahead.min', 'jquery.numeric', 'jquery.maskedinput.min', 'Views/Vitrines/admin_colecoes'), array('inline' => false));
?>

<ol class="breadcrumb">
	<li class="active"><?php echo $this->Html->link($breadcrumb, array('controller' => $vitrinesBackToPaginatorIndex, 'action' => 'backToPaginatorIndex', 'admin' => true)); ?></li>
	<li class="active">Vitrine de Coleções</li>
</ol>

<?php

	if (count($chaves) == 0) {
?>
		<br>
		<div class="text-center">
			<span class="text-muted "> Nenhum registro encontrado.</span>
		</div>

<?php
	} else {

		foreach ($chaves as $key => $chave) {
?>
			<div class="table-responsive">
				<table class="table table-striped table-hover">
					<thead>
						<tr>
							<th colspan="6"><h4 class="descricao-chave"><?php echo $key; ?></h4></th>
							<th class="text-center"><?php echo $this->Html->link('<span class="glyphicon glyphicon-plus"></span>', array('action' => 'ajax_adicionar_colecoes', $colecoesLocais[$model]['id'], $key, 'find_model_id' => $find_model_id), array('class' => 'btn btn-sm btn-primary adicionar-vitrine-colecao', 'escape' => false,'data-placement' => 'left', 'rel' => 'tooltip', 'title' => 'Adicionar Novo')); ?></th>
						</tr>
						<tr>
							<th class="col-md-1 text-center">#</th>
							<th class="col-md-3">Descrição</th>
							<th class="col-md-3">Coleção</th>
							<th class="col-md-2">Início - Fim</th>
							<th class="col-md-1">Qtd.</th>
							<th class="col-md-1">Aleat.</th>
							<th class="col-md-1 text-center"></th>
						</tr>
					</thead>
					<tbody>
					<?php
						foreach ($chave as $vitrine_colecao) {
					?>
							<tr class="modal_" data-id="<?php echo $vitrine_colecao['id'] ?>">
								<td  class="text-center"><?php echo $vitrine_colecao['id'] ?></td>
								<td><?php echo $vitrine_colecao['descricao'] ?></td>
								<td><?php echo $vitrine_colecao['Colecao']['descricao'] ?></td>
								<td>
									<?php
										if ($vitrine_colecao['data_inicio'] != '') {
											echo $this->Formatacao->dataHora($vitrine_colecao['data_inicio']);
										} else {
											echo 'Não informado';
										}
									?>
									 -
									<?php
										if ($vitrine_colecao['data_fim'] != '') {
											echo $this->Formatacao->dataHora($vitrine_colecao['data_fim']);
										} else {
											echo 'Não informado';
										}
									?>
								</td>
								<td><?php echo $vitrine_colecao['qtd_max_produtos'] ?></td>
								<td>
									<?php if ($vitrine_colecao['rand']) {
										$rand = '<span class="glyphicon glyphicon-ok"></span>';
									} else {
										$rand = '';
									}
									echo $rand;
									?>
								</td>
								<td class="text-center">
									<div class="btn-group">
										<button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
											<span class="caret"></span>
										</button>
										<ul class="dropdown-menu pull-right text-left" role="menu">
											<li><?php echo $this->Html->link('<span class="glyphicon glyphicon-edit"></span> Editar', array('action' => 'ajax_editar_colecoes', $vitrine_colecao['id'], $vitrine_colecao['colecao_id']) , array('class' => 'editar-vitrine-colecao', 'escape' => false)); ?></li>
											<li><?php echo $this->Form->postLink('<span class="glyphicon glyphicon-trash"></span> Excluir', array('action' => 'excluir_colecoes', $vitrine_colecao['id']), array('escape' => false), 'Deseja realmente remover esta vitrine de coleção?'); ?></li>
										</ul>
									</div>
								</td>
							</tr>
					<?php
						}
					?>
					</tbody>
				</table>
			</div>
			<br />
<?php
		}

	}

?>
