<?php
	$this->Html->script(array('Views/Servicos/admin_index'), array('inline' => false));
?>
<ol class="breadcrumb">
	<li class="active">Serviços</li>
</ol>

<div class="table-responsive">
	<table class="table table-striped table-hover">
		<thead>
			<tr>
				<th class="col-md-1 text-center">#</th>
				<th class="col-md-4">Nome</th>
				<th class="col-md-6">Valor</th>
				<th class="text-center"><?php echo $this->Html->link('<span class="glyphicon glyphicon-plus"></span>', array('action' => 'adicionar'), array('class' => 'btn btn-primary btn-sm','data-placement' => 'left', 'escape' => false, 'rel' => 'tooltip', 'title' => 'Adicionar Novo')); ?></th>
			</tr>
		</thead>

		<tbody>
			<?php
				foreach ($servicos as $servico) {
			?>
				<tr>
					<td class="text-center"><?php echo $servico['Servico']['id'] ?></td>
					<td>
						<?php echo $servico['Servico']['nome'] ?>
						<?php echo $servico['Servico']['ativo'] == true ? '<span class="label label-success">Ativo</span>' : '<span class="label label-danger">Inativo</span>'; ?>
					</td>
					<td><?php echo $servico['Servico']['valor'] ? $this->Formatacao->moeda($servico['Servico']['valor']) : 'Grátis' ?></td>
					<td class="text-center">
						<div class="btn-group">
							<button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu pull-right text-left" role="menu">
								<li><?php echo $this->Html->link('<span class="glyphicon glyphicon-edit"></span> Editar', array('action' => 'editar', $servico['Servico']['id']), array('escape' => false)); ?></li>
								<li><?php echo $this->Html->link('<span class="glyphicon"></span> Anexos', array('controller' => 'servicoAnexos', 'action' => 'index', $servico['Servico']['id']), array('escape' => false)); ?></li>
								<?php
									if ($servico['Servico']['ativo']) {
								?>
										<li><?php echo $this->Form->postLink('<span class="glyphicon"></span> Desativar', array('action' => 'excluir', $servico['Servico']['id']), array('escape' => false), 'Deseja realmente excluir o Serviço #' . $servico['Servico']['id'] . '?');?></li>
								<?php
									} else {
								?>
										<li><?php echo $this->Form->postLink('<span class="glyphicon"></span> Ativar', array('action' => 'ativar', $servico['Servico']['id']), array('escape' => false));?></li>
								<?php
									}
								 ?>
							</ul>
						</div>
					</td>
				</tr>
			<?php
				}
			?>
		</tbody>
	</table>
</div>

<?php echo $this->element('Paginacao/admin_paginacao'); ?>