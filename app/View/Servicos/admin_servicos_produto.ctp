<?php
	$this->Html->script(array('Views/Servicos/admin_servicos_produto','jquery.price_format.2.0.min', 'jquery.numeric'), array('inline' => false));
		$this->Html->css(array('Views/Servicos/admin_servicos_produto'), 'stylesheet', array('inline' => false));
?>

<ol class="breadcrumb">
	<li><?php echo $this->Html->link('Produtos', array('controller' => 'produtos', 'action' => 'backToPaginatorIndex', 'admin' => true)); ?></li>
	<li class="active">Serviços</li>
	<li class="active">Produto - <?php echo $produto['Produto']['descricao'] ?> <small> (#<?php echo $produto['Produto']['id']?>)</small></li>
</ol>

<div class="alert alert-info">
	<i class="glyphicon glyphicon-info-sign"></i> Para configurar o serviço como gratuito, preencha o campo de valor com <i>zero</i>.
</div>

<?php 
	if (count($servicos) == 0) {
?>
		<br>
		<div class="text-center">
			<span class="text-muted "> Nenhum serviço cadastrado.</span>
		</div>
<?php
	} else {

		echo $this->Form->create('ServicosOpcoesSelecionado');
?>
		<div class="lista-servicos">

<?php
		foreach ($servicos as $key => $servico) {

			$selecionado = '';
			if (isset($servico['ServicosOpcoesSelecionado'][0])) {
				$selecionado = 'checked';
			}

?>
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">

						<div class="panel-heading">
							<h3 class="panel-title">
								<?php 
									echo $this->Form->input($key.'.servico_selecionado', array('type' => 'checkbox', 'checked' => $selecionado, 'label' => $servico['Servico']['descricao'])); 
									echo $this->Form->input($key.'.servico_id', array('type' => 'hidden', 'value' => $servico['Servico']['id']));
								?>
							</h3>
						</div>
						<div class="panel-body">
							<span class="text-muted"><?php echo $servico['Servico']['texto'] ?></span >
							 <br>
							 <br>
							<div class="selecao-opcao">
								<ul class="lista-opcoes-<?php echo $key?> hide">
								<?php
									if (isset($servico['ServicosOpcao'][0])) {

										echo $this->Form->input($key.'.valores', array('type' => 'hidden', 'value' => count($servico['ServicosOpcao'])));

										foreach ($servico['ServicosOpcao'] as $k => $opcao) {

											$valor_selecionado = '';
											$hide = 'hide';
											$valor = '';
											$valor_tipo = 'M';
											if (isset($servico['ServicosOpcoesSelecionado'][0])) {

												foreach ($servico['ServicosOpcoesSelecionado'] as $opcao_selecionada) {

														if ($opcao_selecionada['servico_opcao_id'] == $opcao['id']) {
														$valor_selecionado = 'checked';
														$hide = '';

														if ($opcao_selecionada['tipo'] == 'P') {
															$valor_tipo = $opcao_selecionada['tipo'];
														}

														if ($opcao_selecionada['tipo'] == 'P') {
															$valor = intval($opcao_selecionada['valor']);
														} else {
															$valor = $opcao_selecionada['valor'];
														}

													break;
													}
												}
											}


								?>
											<li class="opcoes">
												<div class="row">
													<div class="col-md-2">
														<?php echo $this->Form->input($key.'.servico_opcao.'.$k. '.id', array('type' => 'checkbox', 'checked' => $valor_selecionado, 'label' => $opcao['descricao'], 'value' => $opcao['id'])); ?>
													</div>
													<div class="campos-valores-<?php echo $key?>-<?php echo $k ?> <?php echo $hide ?>">
														<div class="col-md-2">
															<?php echo $this->Form->input($key.'.servico_opcao.'.$k.'.valor', array('type' => 'text', 'placeholder' => 'Valor', 'label' => false, 'value' => $valor)); ?>
														</div>
														<div class="col-md-8">
															<div class="col-md-3">
																<?php echo $this->Form->input($key.'.servico_opcao.'.$k.'.tipo', array('options' => $tipo_valor, 'label' => false, 'value' => $valor_tipo)); ?>
															</div>
														</div>
													</div>
												</div>
											</li>
								<?php
										}
					
									} else {

										$valor = '';
										$valor_tipo = 'M';
										if (isset($servico['ServicosOpcoesSelecionado'][0])) {

											foreach ($servico['ServicosOpcoesSelecionado'] as $opcao_selecionada) {

												if ($opcao_selecionada['tipo'] == 'P') {
													$valor_tipo = $opcao_selecionada['tipo'];
												}

												if ($opcao_selecionada['tipo'] == 'P') {
													$valor = intval($opcao_selecionada['valor']);
												} else {
													$valor = $opcao_selecionada['valor'];
												}
											}
										}


										echo $this->Form->input($key.'.valores', array('type' => 'hidden', 'value' => 0));
								?>
										<li class="opcoes">
											<div class="row">
													<div class="col-md-2">
														<div class="form-group">
															<div class="radio">
																<label>
																	<input type="radio" checked="checked" name="data[ServicosOpcoesSelecionado][<?php echo $key?>][servico_opcao][0][id]" value="" id="ServicosOpcoesSelecionado<?php echo $key?>ServicoOpcao0Id"> 
																	<strong>Personalizável </strong>
																</label>
															</div>
														</div>
													</div>
													<div class="col-md-2">
														<?php echo $this->Form->input($key.'.servico_opcao.0.valor', array('type' => 'text', 'placeholder' => 'Valor', 'label' => false, 'value' => $valor)); ?>
													</div>
													<div class="col-md-8">
														<div class="col-md-3">
															<?php echo $this->Form->input($key.'.servico_opcao.0.tipo', array('options' => $tipo_valor, 'label' => false, 'value' => $valor_tipo)); ?>
														</div>
													</div>
									
											</div>
										</li>
								<?php
									}
								 ?>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>

	<?php
		}
	?>
		</div>
<?php
	echo $this->Form->actions();
	echo $this->Form->end();
}
?>


