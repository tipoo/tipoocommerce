<?php
	$this->Html->script(array('jquery.price_format.2.0.min', 'Views/Servicos/admin_editar'), array('inline' => false));
?>

<ol class="breadcrumb">
	<li><?php echo $this->Html->link('Serviços', array('action' => 'backToPaginatorIndex', 'admin' => false)); ?></li>
	<li class="active">Editar</li>
</ol>

<?php
	echo $this->Form->create('Servico');
	echo $this->Form->input('id');
	echo $this->Form->input('nome', array('row' => true, 'div' => 'col-md-4', 'label' => 'Nome*'));
	echo $this->Form->input('valor', array('row' => true, 'div' => 'col-md-2', 'label' => 'Valor(R$)', 'type' => 'text', 'class' => 'mascara-decimal'));
	echo $this->Form->actions();
	echo $this->Form->end();
?>