<?php
	$this->Html->script(array('Views/Tags/admin_index'), array('inline' => false));
?>

<ol class="breadcrumb">
	<li class="active">Tags</li>
</ol>

<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-3">
				<label>Pesquisar por nome</label>
				<div class="input-group">
					<?php echo $this->Form->input('Filtro.tag', array('id' => 'FiltrarTag', 'type' => 'text', 'placeholder' => 'Tag' ,'label' => false, 'div' => false)); ?>
					<span class="input-group-btn">
						<?php echo $this->Form->button('<span class="glyphicon glyphicon-search"></span>', array('id' =>'BotaoFiltroTag','class' => 'btn btn-default', 'type' => 'button')); ?>
					</span>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="table-responsive">
	<table class="table table-striped table-hover">
		<thead>
			<tr>
				<th class="col-md-1 text-center">#</th>
				<th class="col-md-5">Nome</th>
				<th class="col-md-5">Url</th>
				<th class="text-center"><?php echo $this->Html->link('<span class="glyphicon glyphicon-plus"></span>', array('action' => 'adicionar'), array('class' => 'btn btn-sm btn-primary', 'escape' => false,'data-placement' => 'left', 'rel' => 'tooltip', 'title' => 'Adicionar Novo')); ?></th>
			</tr>
		</thead>

		<tbody>
			<?php
				foreach ($tags as $tag) {
			?>
				<tr>
					<td class="text-center"><?php echo $tag['Tag']['id'] ?></td>
					<td><?php echo $tag['Tag']['nome'] ?></td>
					<td><?php echo $tag['Slug']['url'] ?></td>
					<td class="text-center">
						<div class="btn-group">
							<button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu pull-right text-left" role="menu">
								<li><?php echo $this->Html->link('<span class="glyphicon glyphicon-edit"></span> Editar', array('action' => 'editar', $tag['Tag']['id']), array('escape' => false)); ?></li>
								<li><?php echo $this->Form->postLink('<span class="glyphicon glyphicon-trash"></span> Excluir', array('action' => 'excluir', $tag['Tag']['id']), array('escape' => false), 'Deseja realmente excluir a Tag #' . $tag['Tag']['id'] . '?'); ?></li>
							</ul>
						</div>
					</td>
				</tr>
			<?php
				}
			?>
		</tbody>
	</table>
</div>

<?php echo $this->element('Paginacao/admin_paginacao'); ?>