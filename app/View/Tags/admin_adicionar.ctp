<?php
	$this->Html->script(array('Views/Tags/admin_adicionar'), array('inline' => false));
?>

<ol class="breadcrumb">
	<li ><?php echo $this->Html->link('Tags', array('action' => 'backToPaginatorIndex', 'admin' => true)); ?></li>
	<li class="active">Adicionar</li>
</ol>

<?php
	echo $this->Form->create('Tag');
	echo $this->Form->input('nome', array('row' => true, 'div' => 'col-md-4', 'label' => 'Nome*'));
	echo $this->Form->actions();
	echo $this->Form->end();
?>
