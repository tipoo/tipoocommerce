<?php
	$this->Html->script(array('Views/Tags/admin_editar'), array('inline' => false));
	$this->Html->css(array('Views/Tags/admin_editar'), 'stylesheet', array('inline' => false));
?>

<ul class="breadcrumb">
	<li ><?php echo $this->Html->link('Tags', array('action' => 'backToPaginatorIndex', 'admin' => true)); ?></li>
	<li class="active">Editar</li>
</ul>

<?php
	echo $this->Form->create('Tag');
	echo $this->Form->input('id');
?>
	<div class="well hide">
		<h4><i class="glyphicon glyphicon-exclamation-sign"></i> Aviso:
	       <small> ao editar a url ela perde a indexação atual, fazendo com que o Google e outras ferramentas de busca não consigam listá-la temporariamente.</small>
		</h4>
		<hr>
		<div class="row">
			<div class="col-md-12">
				<div class="form-inline"><label>Url*: </label>
				  <?php echo  Router::url('/', true); ?>
				  <input name="data[Slug][url]" row="1" class="form-control" type="text" value="<?php echo substr($this->request->data['Slug']['url'], 1) ?>" id="SlugUrl">
				  <input type="hidden" name="data[Slug][id]" value="<?php echo $this->request->data['Slug']['id'] ?>" class="form-control" id="SlugId">
				</div>
			</div>
		</div>
	</div>
	<br>

<?php
	echo $this->Form->input('nome', array('row' => true, 'div' => 'col-md-4', 'label' => 'Nome*'));
	echo $this->Form->actions();
	echo $this->Form->end();
?>