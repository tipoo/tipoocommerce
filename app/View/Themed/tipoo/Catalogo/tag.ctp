<tipoo:vitrineProdutosBusca>
	<div class="produto-busca">
		<input class="produto-busca-id" type="hidden" value="#{produto_id}">
		<div class="produto-busca-img">
			#{img}
		</div>
		<div class="produto-zoom"></div>
		<div class="imagens_relacionadas">
			#{galeria_img}
		</div>
		<div class="produto-busca-descricao">#{link_produto}</div>

		<input class="produto-sku-id" type="hidden" value="#{sku_destaque_id}">
		<div class="produto-busca-sku"><a href="#{url_produto}">#{nome_sku_destaque}</a></div>

		<div class="produto-busca-detalhes">#{descricao_resumida}</div>
		<div class="produto-busca-marca">#{marca}</div>
		#{if:preco_de}
			<div class="produto-busca-preco-de">De: <span>R$#{preco_de}</span></div>
		#{end:preco_de}

		<div class="produto-busca-preco-por"><span  class="preco-por">Por: </span><span>R$#{preco_por}</span></div>

		#{if:economize}
			<div class="produto-busca-economize">Economize: <span>R$#{economize}</span></div>
		#{end:economize}

		#{if:parcelamento}
			<div class="produto-busca-parcelamento">
				ou em até <span class="produto-busca-parcelamento-parcelas">#{parcelas}X</span> de <span class="produto-busca-parcelamento-valor-parcelas">R$#{valorParcelas}</span>
			</div>
		#{end:parcelamento}

		#{selos}
		#{tags}
		<div class="produto-busca-mais-detalhes"><a href="#{url_produto}">Mais Detalhes</a></div>
		<div class="produto-carrinho"></div>
	</div>
</tipoo:vitrineProdutosBusca>