<tipoo:css src="Views/Catalogo/departamento" />
<tipoo:script src="Views/Catalogo/departamento.js" />

<tipoo:breadCrumb />

<tipoo:menuCategoria />

<div class="produtos-buscas-titulo">
	<tipoo:vitrineProdutosBuscaTotalProdutos />
	<tipoo:vitrineProdutosBuscaOrdenacao tipo="lista"/>
	<span>Ordem: </span>
</div>

<tipoo:conteudoHtml chave="Texto Marketeiro" />

<tipoo:conteudoHtml chave="Texto Marketeiro 2" />

<!-- Banner -->
<tipoo:banner chave="Lateral" tipo="slide" />
<!-- Banner -->
<div class="listagem-produtos">
	<tipoo:vitrineProdutosBusca>
		<div class="produto-busca">
			<input class="produto-busca-id" type="hidden" value="#{produto_id}">
			<div class="produto-busca-img">
				#{img}
			</div>
			<div class="produto-zoom"></div>
			<div class="imagens_relacionadas">
				#{galeria_img}
			</div>
			<div class="produto-busca-descricao">#{link_produto}</div>

			<input class="produto-sku-id" type="hidden" value="#{sku_destaque_id}">
			<div class="produto-busca-sku"><a href="#{url_produto}">#{nome_sku_destaque}</a></div>

			<div class="produto-busca-detalhes">#{descricao_resumida}</div>
			<div class="produto-busca-marca">#{marca}</div>
			#{if:preco_de}
				<div class="produto-busca-preco-de">De: <span>R$#{preco_de}</span></div>
			#{end:preco_de}

			<div class="produto-busca-preco-por"><span  class="preco-por">Por: </span><span>R$#{preco_por}</span></div>

			#{if:economize}
				<div class="produto-busca-economize">Economize: <span>R$#{economize}</span></div>
			#{end:economize}

			#{if:parcelamento}
				<div class="produto-busca-parcelamento">
					ou em até <span class="produto-busca-parcelamento-parcelas">#{parcelas}X</span> de <span class="produto-busca-parcelamento-valor-parcelas">R$#{valorParcelas}</span>
				</div>
			#{end:parcelamento}

			#{selos}
			#{tags}

			#{caracteristicas}

			<div class="produto-busca-mais-detalhes"><a href="#{url_produto}">Mais Detalhes</a></div>
			<div class="produto-carrinho"></div>
		</div>
	</tipoo:vitrineProdutosBusca>
</div>
<hr>
<tipoo:vitrineProdutosBuscaPaginacao />
<a href="#" class="carregar-mais-produtos"><span>Carregar mais produtos</span></div>
<hr>

<tipoo:filtro tipo="preco" />

<tipoo:filtro tipo="marca" />

<tipoo:filtro tipo="caracteristica" />
<!--
<tipoo:filtro tipo="caracteristica" id="5" />
<tipoo:filtro tipo="caracteristica" id="7" />
<tipoo:filtro tipo="caracteristica" id="6" />

<tipoo:filtro tipo="caracteristica" id="2" />
<tipoo:filtro tipo="caracteristica" id="3" />
<tipoo:filtro tipo="caracteristica" id="4" />
-->
<tipoo:script src="script1.js" />
<tipoo:script src="script2.js" inline="false" />
<tipoo:script src="script3.js" inline="true" />

<tipoo:css src="css1.css" />
<tipoo:css src="css2" inline="false" />
<tipoo:css src="css3.css" inline="true" />

<tipoo:script inline="true">
	//alert('passou13');
</tipoo:script>

<!-- Banner Medalha -->
<tipoo:banner chave="medalhas" tipo="slide" />
<!-- Banner -->