<?php
	$this->Html->css(array('Views/Catalogo/produto'), 'stylesheet', array('inline' => false));
?>
<tipoo:produtoCompreJunto>
        #{if:modelos}
			#{modelos}
		#{end:modelos}


		#{avaliacaoForm}
		#{avaliacaoMedia}

            #{img}
</tipoo:produtoCompreJunto>

<div class="produto">
	<tipoo:breadCrumb />
	<div class="imagens">
		<tipoo:produtoFotoPrincipal />
		<tipoo:produtoThumbs tamanhoImagem="small" />
		<tipoo:produtoAvaliacao/>
	</div>
	<div class="infos">
		<tipoo:produtoAvaliacao formulario="false"/>
		<h1><tipoo:produto campo="descricao" /></h1>
		<!-- <h2>Caracteríticas<tipoo:produtoCaracteristicas/></h2> -->
		<div class="marca"><tipoo:produto campo="marca" /></div>
		<div class="precos">
			<div class="valores">
				<div class="preco-de">De: <span><tipoo:produto campo="preco_de" formato="monetario" /></span></div>
				<div class="preco-por">Por: <span><tipoo:produto campo="preco_por" formato="monetario" /></span></div>
				//Tag economize
				<tipoo:produtoEconomize>
					<div class="economize">Economize R$#{economize}</div>
				</tipoo:produtoEconomize>
				<hr>
				<div class="preco-de"><span><tipoo:produtoPrecos campo="preco_de" label="De: " cifrao="true" /></span></div>
				<div class="preco-por"><span><tipoo:produtoPrecos campo="preco_por" label="Por: " cifrao="true" /></span></div>
				<hr>
			</div>

			<hr>
			SELOS
			<tipoo:produtoSelos />
			<hr>
			<tipoo:produtoTags/>
			<hr>

			<tipoo:produtoParcelamento quantidadeMininaParcelas="2">
				ou em até <strong>#{parcelas}X</strong> de R$<strong>#{valor_parcelas}</strong>
			</tipoo:produtoParcelamento>

			<div class="comprar">
				<tipoo:produtoBotaoComprar texto="Comprar" />
			</div>

			<tipoo:produtoQuantidade />
		</div>

		<tipoo:produtoModelos tipo="lista" />

		<tipoo:produto campo="slug" />
		<tipoo:produto campo="url" />

		<tipoo:produto campo="detalhes" formato="areaTexto" />

		<tipoo:produtoFrete titulo="Calcule o frete" />

		<tipoo:produtoTabelaParcelamento />

	</div>
	<div class="clear"></div>
	<div class="detalhes">
		<h3>Detalhes</h3>
		<tipoo:produto campo="detalhes" formato="areaTexto" />
	</div>
</div>






