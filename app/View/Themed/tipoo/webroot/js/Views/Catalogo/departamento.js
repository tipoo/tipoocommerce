// categoria.js
$(document).ready(function(){
	$(".breadCrumb select").addClass("form-control");
	
	// Ver mais produtos
	var paginas = $('.produto-busca-paginacao ul li').length;
	
	if (paginas == 1) {
		$('.carregar-mais-produtos').hide();
	}
	
	var pagina = 1;
	var carregando = false;
	
	$('.carregar-mais-produtos').on('click', function(e) {
		e.preventDefault();
		
		var $next = $('.produto-busca-paginacao ul li').eq(pagina);
		
		if ($next.length && !carregando) {
			var url_next = $next.children('a').attr('href');
			
			$.ajax({
				url: url_next,
				type: 'POST',
				beforeSend: function() {
					carregando = true;
					$('.carregar-mais-produtos span').hide();
					$('.carregar-mais-produtos').append('<img class="ajax-loader" src="' + WEBROOT + 'theme/' + PROJETO + '/img/ajax-loader.gif">');
				}
			}).done(function(data) {
				$('.listagem-produtos').append($(data).find('.listagem-produtos').html());
				$('.ajax-loader').remove();
				$('.carregar-mais-produtos span').show();

				pagina++;				
				if (pagina == paginas) {
					$('.carregar-mais-produtos').hide();
				}

				carregando = false;
			}).fail(function(data) {
				$('.ajax-loader').remove();
				$('.carregar-mais-produtos span').show();
				
				console.log('Erro ao tentar consultar mais produtos!');
				carregando = false;
			});
		}
	});
	
});