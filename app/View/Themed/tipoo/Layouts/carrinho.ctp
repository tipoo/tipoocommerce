<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $title_for_layout; ?>
	</title>
	<?php
		echo $this->Html->meta('icon');
		echo $this->fetch('meta');

	?>
	<tipoo:config />
	<?php
		echo $this->Html->css(array('normalize', 'catalogo', 'carrinho'));
		echo $this->Html->script(array('jquery-1.9.1.min'));

		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
</head>
<body>

	<div class="topo">
		<div class="topo-container">
			<a href="<?php echo Router::url('/') ?>" class="logo" alt="Tipoo Modelo">Tipoo Modelo</a>
		</div>
	</div>

	<?php echo $this->Session->flash(); ?>
	<?php echo $this->fetch('content'); ?>

	<div class="rodape">
		<div class="rodape-container">
			© Copyright Tipoo TI
		</div>
	</div>

	<?php
		if (AMBIENTE == AMBIENTE_DEV && SQL_DUMP) {
			echo $this->element('sql_dump');
		}
	?>
</body>
</html>
