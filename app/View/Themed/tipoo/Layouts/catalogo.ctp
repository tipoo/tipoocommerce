<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<tipoo:metaTags />
	<tipoo:fetch tipo="meta" />

	<tipoo:icon />

	<tipoo:title />

	<tipoo:config />
	<tipoo:data />

	<tipoo:script src="jquery-1.10.2.min" inline="true" />
	<tipoo:script src="jquery-migrate-1.2.1.min" inline="true" />

	<tipoo:css src="normalize.css" inline="true" />
	<tipoo:css src="general.css" inline="true" />

	<tipoo:fetch tipo="script" />
	<tipoo:fetch tipo="css" />

</head>
<body>
	<?php echo $this->Session->flash(); ?>

	<header>
		<div class="topo-container">
			<a href="<?php echo Router::url('/') ?>" class="logo" alt="Tipoo Modelo">Tipoo Modelo</a>
			<div class="bem-vindo">

				<!-- Tag Bem Vindo -->
				<tipoo:bemVindo>
					#{if:autenticado}
						Olá #{nome}. Se não é você clique aqui para <a class="logout" href="#{link_logout}">sair</a>.
					#{end:autenticado}
					#{if:not:autenticado}
						Se você já é cadastrado faça <a class="login" href="#{link_login}">login</a>, senão <a class="cadastre-se" href="#{link_cadastro}">cadastre-se</a>.
					#{end:not:autenticado}
				</tipoo:bemVindo>
				<!-- .end Tag Bem Vindo -->
			</div>
			<div class="busca">

				<!-- Tag Formulário de Busca -->
				<tipoo:vitrineProdutosBuscaForm />
				<!-- .end Tag Formulário de Busca -->

			</div>
			<div class="meu-carrinho">
				<?php echo $this->Html->image('cart.png') ?>
				<?php echo $this->Html->link('Meu Carrinho', array('controller' => 'carrinho', 'action' => 'index')) ?>

				<tipoo:carrinhoTotalItens /> itens
			</div>
		</div>
		<div class="topo-menu">
			<tipoo:menu id="1" />
		</div>

	</header>

	<div class="conteudo container">
		<tipoo:menuCategorias class="sf-menu" />
		<tipoo:fetch tipo="content"/>
	</div>

	<footer>
		<div class="copyright">
			© Copyright Tipoo TI
		</div>
	</footer>
</body>
</html>