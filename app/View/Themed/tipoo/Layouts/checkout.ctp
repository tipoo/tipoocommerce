<!DOCTYPE html>
<html>
<head>
	<?php //echo $this->Html->charset(); ?>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
	<title>
		<?php echo $title_for_layout; ?>
	</title>
	<?php
		echo $this->Html->meta('icon');
		echo $this->fetch('meta');

	?>
	<tipoo:config />
	<?php
		echo $this->Html->css(array('normalize', 'catalogo', 'checkout'));

		// Verificando se o pagamento é com moip. Se for, não inclui o jquery, pois o próprio moip inclui a versão 1.7.0
		if (isset($dados_moip) || isset($dados_boleto_moip)) {
			echo $this->Html->script(array('http://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js'));	
			echo '<script type="text/javascript" src="https://desenvolvedor.moip.com.br/sandbox/transparente/MoipWidget-v2.js" charset="UTF-8"></script>';
		} else {
			echo $this->Html->script(array(
				'jquery-1.10.2.min',
				'jquery-migrate-1.2.1.min'
			));
		}

		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
</head>
<body>

	<div class="topo">
		<div class="topo-container">
			<a href="<?php echo Router::url('/') ?>" class="logo" alt="Tipoo Modelo">Tipoo Modelo</a>
		</div>
	</div>

	<?php //echo $this->fetch('content'); ?>
	<tipoo:fetch tipo="content"/>

	<div class="rodape">
		<div class="rodape-container">
			© Copyright Tipoo TI
		</div>
	</div>

	<?php
		if (AMBIENTE == AMBIENTE_DEV && SQL_DUMP) {
			echo $this->element('sql_dump');
		}
	?>
</body>
</html>
