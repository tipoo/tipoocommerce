<!-- Script e Css: Esses scripts serão renderizados no head da página onde se encontram as tags fetch -->
<tipoo:css src="Views/Catalogo/produto.css" />
<tipoo:script src="Views/Catalogo/produto.js" />
<!-- .end Script e Css -->

<!-- Tag Breadcrumb -->
<tipoo:breadCrumb />
<!-- .end Tag Breadcrumb -->

<div class="product">
	<div class="images">
		<!-- Tag da Imagem Principal do Produto -->
		<tipoo:produtoFotoPrincipal />
		<!-- .end Tag da Imagem Principal do Produto -->

		<!-- Tag das Thumbs do Produto -->
		<tipoo:produtoThumbs tamanhoImagem="small" />
		<!-- .end Tag das Thumbs do Produto -->
	</div>
	<div class="infos">
		<h1>
			<!-- Tag do Nome do Produto -->
			<tipoo:produto campo="descricao" />
			<!-- .end Tag do Nome do Produto -->
		</h1>

		<div class="brand">
			<!-- Tag da Marca do Produto -->
			<tipoo:produto campo="marca" />
			<!-- .end Tag da Marca do Produto -->
		</div>

		<div class="box">
			<!-- Tag de Selos -->
			<tipoo:produtoSelos />
			<!-- .end Tag de Selos -->

			<!-- Tag de Tags -->
			<tipoo:produtoTags/>
			<!-- .end Tag de Tags -->
		</div>

		<div class="box">
			<div class="price">
				<div class="preco-de">
					<!-- Tag Preço De -->
					<tipoo:produtoPrecos campo="preco_de" label="De: " cifrao="true" />
					<!-- .end Tag Preço De -->
				</div>
				<div class="preco-por">
					<!-- Tag Preço Por -->
					<tipoo:produtoPrecos campo="preco_por" label="Por: " cifrao="true" />
					<!-- .end Tag Preço Por -->
				</div>

				<!-- Tag Economize -->
				<tipoo:produtoEconomize>
					<div class="economize">Economize R$#{economize}</div>
				</tipoo:produtoEconomize>
				<!-- .end Tag Economize -->
			</div>

			<!-- Tag de Parcelamento -->
			<tipoo:produtoParcelamento quantidadeMininaParcelas="2">
				ou em até <strong>#{parcelas}X</strong> de R$<strong>#{valor_parcelas}</strong>
			</tipoo:produtoParcelamento>
			<!-- .end Tag de Parcelamento -->

			<div class="buy-button">
				<!-- Tag Quantidade -->
				<tipoo:produtoQuantidade />
				<!-- .end Tag Quantidade -->
				<!-- Tag Botão Comprar -->
				<tipoo:produtoBotaoComprar texto="Comprar" />
				<!-- .end Tag Botão Comprar -->
			</div>
		</div>

		<div class="skus-select">
			<!-- Tag de Seleção de Skus -->
			<tipoo:produtoModelos tipo="select" />
			<!-- .end Tag de Seleção de Skus -->
		</div>
	</div>
</div>