<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<tipoo:metaTags />
	<tipoo:fetch tipo="meta" />

	<tipoo:title />

	<tipoo:config />

	<tipoo:script src="jquery-1.10.2.min.js" inline="true" />
	<tipoo:script src="jquery-migrate-1.2.1.min.js" inline="true" />
	<tipoo:script src="general.js" inline="true" />
	<tipoo:script src="Layouts/catalogo.js" inline="true" />

	<tipoo:css src="normalize.css" inline="true" />
	<tipoo:css src="general.css" inline="true" />
	<tipoo:css src="Layouts/catalogo.css" inline="true" />

	<!-- Fetch: Essas tags não podem ser removidas do Layout, leia a documentação para mais informações. -->
	<tipoo:fetch tipo="script" />
	<tipoo:fetch tipo="css" />
	<!-- .end Fetch -->
</head>
<body>
	<header>
		<div class="welcome">
			<!-- Tag Bem Vindo -->
			<tipoo:bemVindo>
				#{if:autenticado}
					Olá #{nome}. Se não é você clique aqui para <a class="logout" href="#{link_logout}">sair</a>.
				#{end:autenticado}
				#{if:not:autenticado}
					Se você já é cadastrado faça <a class="login" href="#{link_login}">login</a>, senão <a class="cadastre-se" href="#{link_cadastro}">cadastre-se</a>.
				#{end:not:autenticado}
			</tipoo:bemVindo>
			<!-- .end Tag Bem Vindo -->
		</div>

		<div class="search">
			<!-- Tag Formulário de Busca -->
			<tipoo:vitrineProdutosBuscaForm />
			<!-- .end Tag Formulário de Busca -->
		</div>

		<div class="cart">
			<!-- Tag Html -->
			<tipoo:html tag="a" href="/carrinho">Meu Carrinho</tipoo:html>
			<!-- .end Tag Html -->

			<!-- Tag Total de Intes no Carrinho -->
			<tipoo:carrinhoTotalItens /> itens
			<!-- .end Tag Total de Intes no Carrinho -->
		</div>
	</header>

	<nav>
		<!-- Tag Menu de Categorias -->
		<tipoo:menuCategorias />
		<!-- .end Tag Menu de Categorias -->
	</nav>

	<div class="content">
		<tipoo:fetch tipo="content"/>
	</div>

	<footer>
		<div class="copyright">
			<p>© Copyright Tipoo TI</p>
		</div>
	</footer>
</body>
</html>