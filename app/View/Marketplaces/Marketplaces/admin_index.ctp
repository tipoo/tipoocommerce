<ol class="breadcrumb">
	<li class="active">Marketplaces</li>
</ol>

<div class="table-responsive">
	<table class="table table-striped table-hover">
		<thead>
			<tr>
				<th class="col-md-2">Imagem</th>
				<th class="col-md-9">Marketplace</th>
				<th class="text-center"></th>
			</tr>
		</thead>

		<tbody>
			<tr>
				<td><?php echo $this->Html->image('marketplace-mercadolivre-logo.png') ?></td>
				<td>Mercado Livre</td>
				<td class="text-center">
					<div class="btn-group">
						<button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
							<span class="caret"></span>
						</button>
						<ul class="dropdown-menu pull-right text-left" role="menu">
							<?php
								if ($this->Permissoes->check(array('controller' => 'mercadoLivre', 'action' => 'index', 'admin' => true, 'plugin' => null))) {
							?>
									<li><?php echo $this->Html->link('<span class="glyphicon "></span>Configurações', array('controller' => 'mercadoLivre', 'action' => 'index'), array('escape' => false));?></li>
							<?php
							 	} else {
							?>
									<li class="disabled"><a href="javascript: void(0)" data-placement="left" rel="tooltip" title="" data-original-title="Para contratar este serviço entre em contato com o nosso comercial"><span class="glyphicon"></span> Configurações</a></li>
							<?php
								}
							?>
						</ul>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
</div>