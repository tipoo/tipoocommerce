<?php
	$this->Html->script(array('hogan-2.0.0.js','typeahead.min', 'Views/Marketplaces/MercadoLivre/admin_index'), array('inline' => false));
	$this->Html->css(array('typeahead.js-bootstrap', 'Views/Marketplaces/MercadoLivre/admin_index'), 'stylesheet', array('inline' => false));
?>

<ol class="breadcrumb">
	<li ><?php echo $this->Html->link('Marketplaces', array('controller' => 'marketplaces', 'action' => 'backToPaginatorIndex', 'admin' => true)); ?></li>
	<li class="active">Mercado Livre</li>
</ol>

<div class="row">
	<!-- Autenticação -->
		<div class="col-md-6">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Autenticação</h3>
			</div>
			<ul class="list-group">
				<?php
					if ($configuracao['Configuracao']['mercado_livre_ativo']) {
				?>
						<li class="list-group-item">
							<h5 class="form-titulos-admin">Autenticação</h5>
							<div class="row">
								<div class="col-md-12">
									<?php
										if (isset($meliRedirectUrl)) {
									?>
											<a href="<?php echo $meliRedirectUrl ?>" class="btn btn-primary btn-login-mercado-livre"><span></span>Login</a>
									<?php
										} else {
									?>
											<span class="glyphicon glyphicon-ok"></span>
									<?php
										}
									?>
								</div>
							</div>
						</li>
						<li class="list-group-item">
							<h5 class="form-titulos-admin">App Id</h5>
							<div class="row">
								<div class="col-md-12">
									<?php
										if ($configuracao['Configuracao']['mercado_livre_app_id'] != '') {

											echo $configuracao['Configuracao']['mercado_livre_app_id'];

										} else {

											echo '-';

										}

									?>
								</div>
							</div>
						</li>

						<li class="list-group-item">
							<h5 class="form-titulos-admin">Secret Key</h5>
							<div class="row">
								<div class="col-md-12">
									<?php
										if ($configuracao['Configuracao']['mercado_livre_secret_key'] != '') {

											echo '***************************';

										} else {

											echo '-';

										}

									?>
								</div>
							</div>
						</li>

						<li class="list-group-item">
							<h5 class="form-titulos-admin">Url de Redirecionamento</h5>
							<div class="row">
								<div class="col-md-12">
									<?php
										if ($configuracao['Configuracao']['mercado_livre_redirect_url'] != '') {

											echo $configuracao['Configuracao']['mercado_livre_redirect_url'];

										} else {

											echo '-';

										}

									?>
								</div>
							</div>
						</li>
				<?php
					} else {
				?>
						<li class="list-group-item">
							<div class="row">
								<div class="col-md-12">
									Para habilitar o marketplace do Mercado Livre, entre em contato com o suporte.
								</div>
							</div>
						</li>
				<?php
					}
				?>
			</ul>
		</div>
	</div>
	<!-- .end Autenticação -->
	<!-- Integração -->
		<div class="col-md-6">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Integração</h3>
			</div>
			<ul class="list-group">
				<?php
					if ($configuracao['Configuracao']['mercado_livre_ativo']) {
				?>
						<li class="list-group-item">
							<h5 class="form-titulos-admin">Coleção</h5>
							<div class="row">
								<div class="col-md-12">
									<?php
										echo $this->Form->create('Configuracao', array('url' => array('controller' => 'mercadoLivre', 'action' => 'meli_adicionar_colecao', 'admin' => true)));
										echo $this->Form->input('mercado_livre_colecao', array('row' => true, 'div' => 'col-md-12', 'class' => 'typeahead', 'label' => 'Localizar coleção', 'placeholder' => 'Digite o nome do coleção', 'autocomplete' => 'off'));
										echo $this->Form->input('mercado_livre_colecao_id', array('type' => 'hidden'));
										echo $this->Form->actions();
										echo $this->Form->end();
									?>
								</div>
							</div>
						</li>

						<?php if ($configuracao['Configuracao']['mercado_livre_colecao_id'] != '') { ?>
								<li class="list-group-item">
									<h5 class="form-titulos-admin">Integrar ao marketplace - Coleção #<?php echo $configuracao['Configuracao']['mercado_livre_colecao_id']; ?></h5>
									<div class="row">
										<div class="col-md-12">
											<?php echo $this->Form->postLink('<span></span> Integrar', array('controller' => 'mercadoLivre', 'action' => 'adicionar_produtos', 'admin' => true), array('class' => 'btn btn-primary btn-integrar-mercado-livre', 'data-loading-text' => 'Aguarde...', 'escape' => false)); ?>
										</div>
									</div>
								</li>
						<?php } ?>
				<?php
					} else {
				?>
						<li class="list-group-item">
							<div class="row">
								<div class="col-md-12">
									Para habilitar o marketplace do Mercado Livre, entre em contato com o suporte.
								</div>
							</div>
						</li>
				<?php
					}
				?>
			</ul>
		</div>
	</div>
	<!-- .end Integração -->
</div>