<?php
	$this->Html->script(array('bootstrap-datepicker','bootstrap-datepicker.pt-BR', 'xdate', 'Views/Relatorios/admin_relatorio_termos_buscados'), array('inline' => false));
	$this->Html->css(array('datepicker'), 'stylesheet', array('inline' => false));
?>

<div class="alert alert-danger hide" id="alert-datepicker">
	<span>Mensagem de erro: Intervalo entre datas incorreto.</span>
</div>

<ol class="breadcrumb">
	<li class="active">Relatórios de termos mais buscados</li>
</ol>

<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-2 input-append date" id="dp1" data-date-format="dd/mm/yyyy">
				<label>De</label>
				<div class="input-group">
					<span class="input-group-btn">
						<button class="btn btn-default add-on" type="button">
							<span class="glyphicon glyphicon-calendar"></span>
						</button>
					</span>
					<input name="data[Filtro][produto]" id="dp1-mask" size="16" class="form-control" type="text" data-date="<?php echo $this->Formatacao->data($de) ?>" value="<?php echo $this->Formatacao->data($de) ?>" >
				</div>
			</div>
			<div class="col-md-2 input-append date" id="dp2" data-date-format="dd/mm/yyyy">
				<label>Até</label>
				<div class="input-group">
					<span class="input-group-btn">
						<button class="btn btn-default add-on" type="button">
							<span class="glyphicon glyphicon-calendar"></span>
						</button>
					</span>
					<input name="data[Filtro][produto]" id="dp2-mask" size="16" class="form-control" type="text" data-date="<?php echo $this->Formatacao->data($ate) ?>" value="<?php echo $this->Formatacao->data($ate) ?>" >
				</div>
			</div>
			<div class="col-md-2">
				<label for="enviarFiltros" class="control-label">&nbsp;</label>
				<button class="btn btn-primary form-control" id="enviarFiltros" type="button">Filtrar</button>
			</div>
		</div>
	</div>
</div>

<div class="table-responsive">
	<table class="table table-striped table-hover">
		<thead>
			<tr>
				<th class="col-md-1 text-center">Ranking</th>
				<th class="col-md-9">Termo</th>
				<th class="col-md-2 text-center">Quantidade</th>
			</tr>
		</thead>

		<tbody>
			<?php
				foreach ($termos_buscados as $key => $termos_buscado) {
					$ordem_mais_buscados = ($key + 1);
			?>
				<tr>
					<td class="text-center"><?php echo $ordem_mais_buscados ?>º</td>
					<td><?php echo $termos_buscado['TermosBuscado']['termo'] ?></td>
					<td class="text-center"><?php echo $termos_buscado[0]['qtd_termos'] ?></td>
				</tr>
			<?php
				}
			?>
		</tbody>
	</table>
</div>