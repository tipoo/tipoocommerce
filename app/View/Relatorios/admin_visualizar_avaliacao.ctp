<div class="table-responsive">
	<table class="table table-striped table-hover">
		<thead>
			<tr>
				<th class="col-md-4">Produto</th>
				<th class="col-md-3">Nome</th>
				<th class="col-md-3">E-mail</th>
				<th class="col-md-1 text-center">Avaliação</th>
				<th class="col-md-1">Data da avaliação</th>
			</tr>
		</thead>

		<tbody>
			<?php
				foreach ($avaliacoes as $avaliacao) {
			?>
				<tr>
					<td> - <?php echo $avaliacao['Produto']['descricao'] ?></td>
					<td><?php echo $avaliacao['Avaliacao']['nome'] ?></td>
					<td><?php echo $avaliacao['Avaliacao']['email'] ?></td>
					<td class="text-center"><?php echo $avaliacao['Avaliacao']['avaliacao'] ?></td>
					<td><?php echo $this->Formatacao->dataHora($avaliacao['Avaliacao']['created']) ?></td>
				</tr>
			<?php
				}
			?>
		</tbody>
	</table>
</div>