<div class="table-responsive">
	<table class="table table-striped table-hover">
		<thead>
			<tr>
				<th class="col-md-6">Produto - Sku </th>
				<th class="col-md-1"></th>
				<th class="col-md-3">E-mail</th>
				<th class="col-md-1"></th>
				<th class="col-md-1">Data de cadastro</th>
			</tr>
		</thead>

		<tbody>
			<?php
				foreach ($pedidos_avise_me as $pedido_avise_me) {
			?>
				<tr>
					<td> - <?php echo $pedido_avise_me['Sku']['Produto']['descricao'] ?> - 
						<?php echo $pedido_avise_me['Sku']['descricao'] ?> (<?php echo $pedido_avise_me['Sku']['sku'] ?>)
					</td>
					<td></td>
					<td><?php echo $pedido_avise_me['AviseMe']['email'] ?></td>
					<td></td>
					<td><?php echo $this->Formatacao->dataHora($pedido_avise_me['AviseMe']['created']) ?></td>
				</tr>
			<?php
				}
			?>
		</tbody>
	</table>
</div>