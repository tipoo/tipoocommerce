<?php
	$this->Html->script(array('bootstrap-datepicker', 'xdate', 'Views/Relatorios/admin_estoque'), array('inline' => false));
	$this->Html->css(array('datepicker'), 'stylesheet', array('inline' => false));
?>

<ol class="breadcrumb">
	<li class="active">Relatórios de Estoque</li>
</ol>

<div class="alert alert-info">
	<i class="glyphicon glyphicon-info-sign"></i> <strong>Sobre as quantidades dos SKU's</strong>:<br><br>
	<ul>
		<li><strong>Disponíveis</strong>: são os SKU's que estão no estoque e disponíveis para novas vendas;</li>
		<li><strong>Reservados</strong>: são os SKU's que estão no estoque, mas encontram-se reservados em pedidos que ainda não foram pagos;</li>
		<li><strong>Em Pré-Venda</strong>: são os SKU's que foram vendidos antecipamente, ou seja, não estão no estoque ainda, mas já foram comercializados e precisam ser entregues quando derem entrada no estoque;</li>
		<li><strong>Entregues</strong>: são os SKU's que já foram vendidos e entregues.</li>
	</ul>
</div>

<div class="row">
	<div class="col-md-9 col-lg-10">
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="row">

					<div class="col-md-4">
						<div class="input-group">
							<?php echo $this->Form->input('Filtro.sku', array('type' => 'text', 'placeholder' =>'Busca por SKU' ,'label' => false, 'div' => false)); ?>
							<span class="input-group-btn">
								<?php echo $this->Form->button('<span class="glyphicon glyphicon-search"></span>', array('id' =>'FiltrarBuscaSku','class' => 'btn btn-default buscar', 'type' => 'button')); ?>
							</span>
						</div>
					</div>

					<div class="col-md-4">
						<div class="input-group">
							<?php echo $this->Form->input('Filtro.produto', array('type' => 'text', 'placeholder' =>'Busca por Produto' ,'label' => false, 'div' => false)); ?>
							<span class="input-group-btn">
								<?php echo $this->Form->button('<span class="glyphicon glyphicon-search"></span>', array('id' =>'FiltrarBuscaProduto','class' => 'btn btn-default buscar', 'type' => 'button')); ?>
							</span>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
	<div class="col-md-3 col-lg-2 text-right">
		<div class="panel panel-default">
			<div class="panel-body">
				<?php
					$urlRelatorioEstoque = array('controller' => 'relatorios', 'action' => 'baixar_relatorio_estoque', 'admin' => true, 'plugin' => null);

					if ($this->Permissoes->check($urlRelatorioEstoque)) {
						echo $this->Html->link('<span class="glyphicon glyphicon-download-alt"></span> Baixar CSV', $urlRelatorioEstoque, array('id' => 'baixar-csv','class ' => 'btn btn-success form-control', 'escape' => false));
					}
				?>
			</div>
		</div>
	</div>
</div>

<div class="table-responsive">
	<table class="table table-striped table-hover">
		<thead>
			<tr>
				<th class="col-md-1 text-center">#</th>
				<th class="col-md-3">(Sku)-Descrição</th>
				<th class="col-md-3">Produto</th>
				<th class="col-md-1">Disponíveis</th>
				<th class="col-md-1">Reservados</th>
				<th class="col-md-1">Em Pré-venda</th>
				<th class="col-md-1 text-center" >Entregues</th>
				<th class="col-md-1 text-center"></th>
			</tr>
		</thead>

		<tbody>
			<?php
				foreach ($skus as $sku) {
			?>
				<tr>
					<td class="text-center"><?php echo $sku['Sku']['id'] ?></td>
					<td>(<?php echo $sku['Sku']['sku'] ?>)-<?php echo $sku['Sku']['descricao'] ?></td>
					<td>
					<?php
						if ($sku['Produto']['ativo']) {
							 echo $sku['Produto']['descricao'];
						} else {
							echo $sku['Produto']['descricao'];
							?>
							  <span class="label label-danger arrowed">Inativo</span>
							<?php
						}
					 ?>
					 </td>
					<td class="text-center"><?php echo $sku['Sku']['qtd_estoque'] ?></td>

					<?php
						$qtd_pedido = 0;
						$qtd_pre_venda = 0;
						$qtd_reservas = 0 ;
						foreach ($pedidos_skus as $pedido) {
							if ($pedido['PedidoSku']['sku_id'] == $sku['Sku']['id']) {

								if ($pedido['PedidoSku']['situacao_estoque'] == 'Pedido') {
									$qtd_pedido += $pedido['PedidoSku']['qtd'];
								}

								if ($pedido['PedidoSku']['situacao_estoque'] == 'PreVenda') {
									$qtd_pre_venda += $pedido['PedidoSku']['qtd'];
								}

								if ($pedido['PedidoSku']['situacao_estoque'] == 'Reserva') {
									$qtd_reservas += $pedido['PedidoSku']['qtd'];
								}
							}
						}
					?>
					<td class="text-center"><?php echo $qtd_reservas?></td>
					<td class="text-center"><?php echo $qtd_pre_venda?></td>
					<td class="text-center"><?php echo $qtd_pedido?></td>
					<td class="text-center">
						<div class="btn-group">
							<button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu pull-right text-left" role="menu">
								<li><?php echo $this->Html->link('<span class="glyphicon"></span> Alterar Estoque', array('controller' => 'estoque', 'action' => 'movimentacao', 'admin' => true, $sku['Sku']['id']), array('escape' => false));?></li>
							</ul>
						</div>
					</td>
				</tr>
			<?php
				}
			?>
		</tbody>
	</table>
</div>