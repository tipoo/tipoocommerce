<?php
	$this->Html->script(array('bootstrap-datepicker', 'bootstrap-datepicker.pt-BR', 'xdate', 'Views/Relatorios/admin_relatorio_avaliacoes'), array('inline' => false));
	$this->Html->css(array('datepicker'), 'stylesheet', array('inline' => false));
?>

<div class="alert alert-danger hide" id="alert-datepicker">
	<span>Mensagem de erro: Intervalo entre datas incorreto.</span>
</div>

<ol class="breadcrumb">
	<li class="active">Relatórios de avaliações</li>
</ol>
<div class="row">
	<div class="col-md-11 col-lg-12">
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="row">
					<div class="col-md-3 input-append date" id="dp1" data-date-format="dd/mm/yyyy">
						<label>De</label>
						<div class="input-group">
							<span class="input-group-btn">
								<button class="btn btn-default add-on" type="button">
									<span class="glyphicon glyphicon-calendar"></span>
								</button>
							</span>
							<input name="data[Filtro][produto]" id="dp1-mask" size="16" class="form-control" type="text" data-date="<?php echo $this->Formatacao->data($de) ?>" value="<?php echo $this->Formatacao->data($de) ?>" >
						</div>
					</div>
					<div class="col-md-3 input-append date" id="dp2" data-date-format="dd/mm/yyyy">
						<label>Até</label>
						<div class="input-group">
							<span class="input-group-btn">
								<button class="btn btn-default add-on" type="button">
									<span class="glyphicon glyphicon-calendar"></span>
								</button>
							</span>
							<input name="data[Filtro][produto]" id="dp2-mask" size="16" class="form-control" type="text" data-date="<?php echo $this->Formatacao->data($ate) ?>" value="<?php echo $this->Formatacao->data($ate) ?>" >
						</div>
					</div>
					<div class="col-md-2">
						<label for="enviarFiltros" class="control-label">&nbsp;</label>
						<button class="btn btn-primary form-control" id="enviarFiltros" type="button">Filtrar</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="table-responsive">
	<table class="table table-striped table-hover">
		<thead>
			<tr>
				<th class="col-md-1 text-center">#</th>
				<th class="col-md-6">Produto</th>
				<th class="col-md-2 text-center">Avaliações</th>
				<th class="col-md-2 text-center">Média de pts</th>
				<th class="col-md-1"> </th>
			</tr>
		</thead>

		<tbody>
			<?php
				foreach ($avaliacoes as $avaliacao) {
			?>
				<tr>
					 <td class="text-center"><?php echo $avaliacao['Produto']['id'] ?></td> 
					<td><?php echo $avaliacao['Produto']['descricao'] ?></td>
					<td class="text-center">
					<?php 
						$qtd_avaliacoes = 0 ;
						foreach ($total_avaliacoes as $total_avaliacao) {
							if ( $total_avaliacao['Produto']['id'] == $avaliacao['Produto']['id']) {
								$qtd_avaliacoes++;
							}
						}

						echo $qtd_avaliacoes;
					 ?> 

					</td>
					<td class="text-center"><?php echo $avaliacao['Produto']['avaliacao'] ?></td>
					<td class="text-center">
						<div class="btn-group">
							<button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu pull-right text-left" role="menu">
								<li><?php echo $this->Html->link('<span class="glyphicon"></span> Visualizar', array('action' => 'visualizar_avaliacao', $avaliacao['Produto']['id'], 'de' => $de, 'ate' => $ate), array('class' => 'detalhes-avaliacao', 'escape' => false)); ?></li>
							</ul>
						</div>
					</td>
				</tr>
			<?php
				}
			?>
		</tbody>
	</table>
</div>