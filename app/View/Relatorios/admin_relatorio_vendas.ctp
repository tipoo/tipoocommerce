<?php
	$this->Html->script(array('bootstrap-datepicker', 'bootstrap-datepicker.pt-BR', 'xdate', 'Views/Relatorios/admin_relatorio_vendas'), array('inline' => false));
	$this->Html->css(array('datepicker', 'Views/Relatorios/admin_relatorio_vendas'), 'stylesheet', array('inline' => false));
?>

<div class="alert alert-danger hide" id="alert-datepicker">
	<span>Mensagem de erro: Intervalo entre datas incorreto.</span>
</div>

<ol class="breadcrumb">
	<li class="active">Relatórios de Vendas</li>
</ol>

<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-2 input-append date" id="dp1" data-date-format="dd/mm/yyyy">
				<label>De</label>
				<div class="input-group">
					<span class="input-group-btn">
						<button class="btn btn-default add-on" type="button">
							<span class="glyphicon glyphicon-calendar"></span>
						</button>
					</span>
					<input name="data[Filtro][produto]" id="dp1-mask" size="16" class="form-control" type="text" data-date="<?php echo $this->Formatacao->data($de) ?>" value="<?php echo $this->Formatacao->data($de) ?>">
				</div>
			</div>
			<div class="col-md-2 input-append date" id="dp2" data-date-format="dd/mm/yyyy">
				<label>Até</label>
				<div class="input-group">
					<span class="input-group-btn">
						<button class="btn btn-default add-on" type="button">
							<span class="glyphicon glyphicon-calendar"></span>
						</button>
					</span>
					<input name="data[Filtro][produto]" id="dp2-mask" size="16" class="form-control" type="text" data-date="<?php echo $this->Formatacao->data($ate) ?>" value="<?php echo $this->Formatacao->data($ate) ?>" >
				</div>
			</div>
			<div class="col-md-2">
				<label for="enviarFiltros" class="control-label">&nbsp;</label>
				<button class="btn btn-primary form-control" id="enviarFiltros" type="button">Filtrar</button>
			</div>
		</div>
	</div>
</div>

<?php
	if (count($vendas) > 0) {
?>
		<div class="relatorio">
			<div class="row">
				<div class="col-md-4">
					<div class="well">
						<strong>Quantidade de Vendas</strong>
						<br />
						<p class="lead text-success"><?php echo $qtd_vendas ?></p>

						<strong>Receita Bruta (c/ frete)</strong>
						<br />
						<p class="lead text-success">R$ <?php echo number_format($receita_bruta, 2, ',', '.') ?></p>

						<strong>Ticket Médio</strong>
						<br />
						<p class="lead text-success">R$ <?php echo number_format($ticket_medio, 2, ',', '.') ?></p>
					</div>
				</div>
				<div class="col-md-4">
					<div class="well">
						<strong>Quantidade de Skus Vendidos</strong>
						<br />
						<p class="lead text-success"><?php echo $qtd_skus_vendidos ?></p>

						<strong>Valor Total de Skus Vendidos</strong>
						<br />
						<p class="lead text-success">R$ <?php echo number_format($valor_total_vendas_liquido, 2, ',', '.') ?></p>
					</div>
				</div>
				<div class="col-md-4">
					<div class="well">
						<strong>Quantidade de Vendas com Frete</strong>
						<br />
						<p class="lead text-success"><?php echo $qtd_frete ?></p>

						<strong>Valor Total do Frete</strong>
						<br />
						<p class="lead text-success">R$ <?php echo number_format($total_valor_frete, 2, ',', '.') ?></p>
					</div>
				</div>
			</div>


			<div class="table-responsive">
				<table class="table table-striped table-hover">
					<thead>
						<tr>
							<th class="col-md-2">Data</th>
							<th class="col-md-3">Cliente</th>
							<th class="col-md-2">Cidade</th>
							<th class="col-md-1">Qtd. Skus</th>
							<th class="col-md-1">Subtotal</th>
							<th class="col-md-1">Frete</th>
							<th class="col-md-1">Total</th>
						</tr>
					</thead>

					<tbody>
						<?php
							foreach ($vendas as $key => $venda) {
						?>
							<tr>
								<td><?php echo $this->Formatacao->dataHora($venda['Pedido']['data_hora']) ?></td>
								<td><?php echo $venda['Cliente']['nome'] ?></td>
								<td><?php echo $venda['Endereco']['Cidade']['nome'] . '/' . $venda['Endereco']['Cidade']['uf']; ?></td>
								<td><?php echo count($venda['PedidoSku']) ?></td>
								<td><?php echo $this->Formatacao->moeda($venda['Pedido']['valor_produtos']); ?></td>
								<td><?php echo $this->Formatacao->moeda($venda['Pedido']['valor_frete']); ?></td>
								<td><?php echo $this->Formatacao->moeda($venda['Pedido']['valor_total']); ?></td>
							</tr>
						<?php
							}
						?>
					<tbody>

				</table>
			</div>
		</div>
<?php
	} else {
?>
		<div class="well">
			<span>Nenhum resultado encontrado.</span>
		</div>

<?php
	}
?>

