<?php
	$this->Html->script(array('bootstrap-datepicker', 'bootstrap-datepicker.pt-BR', 'xdate', 'Views/Relatorios/admin_relatorio_avise_me'), array('inline' => false));
	$this->Html->css(array('datepicker'), 'stylesheet', array('inline' => false));
?>

<div class="alert alert-danger hide" id="alert-datepicker">
	<span>Mensagem de erro: Intervalo entre datas incorreto.</span>
</div>

<ol class="breadcrumb">
	<li class="active">Relatórios de pedidos de Avise-me</li>
</ol>
<div class="row">
	<div class="col-md-9 col-lg-10">
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="row">
					<div class="col-md-3 input-append date" id="dp1" data-date-format="dd/mm/yyyy">
						<label>De</label>
						<div class="input-group">
							<span class="input-group-btn">
								<button class="btn btn-default add-on" type="button">
									<span class="glyphicon glyphicon-calendar"></span>
								</button>
							</span>
							<input name="data[Filtro][produto]" id="dp1-mask" size="16" class="form-control" type="text" data-date="<?php echo $this->Formatacao->data($de) ?>" value="<?php echo $this->Formatacao->data($de) ?>" >
						</div>
					</div>
					<div class="col-md-3 input-append date" id="dp2" data-date-format="dd/mm/yyyy">
						<label>Até</label>
						<div class="input-group">
							<span class="input-group-btn">
								<button class="btn btn-default add-on" type="button">
									<span class="glyphicon glyphicon-calendar"></span>
								</button>
							</span>
							<input name="data[Filtro][produto]" id="dp2-mask" size="16" class="form-control" type="text" data-date="<?php echo $this->Formatacao->data($ate) ?>" value="<?php echo $this->Formatacao->data($ate) ?>" >
						</div>
					</div>
					<div class="col-md-2">
						<label for="enviarFiltros" class="control-label">&nbsp;</label>
						<button class="btn btn-primary form-control" id="enviarFiltros" type="button">Filtrar</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-3 col-lg-2 text-right">
		<div class="panel panel-default">
			<div class="panel-body">
				<label for="btnFiltrar" class="control-label hidden-sm hidden-xs">&nbsp;</label>
				<?php echo $this->Form->postLink('<span class="glyphicon glyphicon-download-alt"></span> Baixar CSV', array('action' => 'baixar_avise_me', 'de' => $de, 'ate' => $ate), array('class ' => 'btn btn-success form-control', 'escape' => false, 'rel' => 'tooltip', 'title' => 'Download'));?>
			</div>
		</div>
	</div>
</div>


<div class="table-responsive">
	<table class="table table-striped table-hover">
		<thead>
			<tr>
				<th class="col-md-1 text-center">#</th>
				<th class="col-md-8">Produto - Sku </th>
				<th class="col-md-2 text-center">Quantidade</th>
				<th class="col-md-1"> </th>
			</tr>
		</thead>

		<tbody>
			<?php
				foreach ($pedidos_grupo as $pedido_grupo) {
			?>
				<tr>
					 <td class="text-center"><?php echo $pedido_grupo['Sku']['Produto']['id'] ?></td> 
					<td><?php echo $pedido_grupo['Sku']['Produto']['descricao'] ?> - 
						<?php echo $pedido_grupo['Sku']['descricao'] ?> (<?php echo $pedido_grupo['Sku']['sku'] ?>)
					</td>
					<td class="text-center">
						<?php 
						$qtd_pedidos = 0 ;
							foreach ($pedidos_aviso as $pedido_aviso) {
								if ( $pedido_aviso['Sku']['Produto']['id'] == $pedido_grupo['Sku']['Produto']['id']) {
									$qtd_pedidos++;
								}
							}

							echo $qtd_pedidos;
						 ?>

					</td>
					<td class="text-center">
						<div class="btn-group">
							<button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu pull-right text-left" role="menu">
								<li><?php echo $this->Html->link('<span class="glyphicon"></span> Visualizar', array('action' => 'visualizar_avise_me', $pedido_grupo['Sku']['id'], 'de' => $de, 'ate' => $ate), array('escape' => false , 'class' => 'detalhes-avise-me')); ?></li>
							</ul>
						</div>
					</td>
				</tr>
			<?php
				}
			?>
		</tbody>
	</table>
</div>