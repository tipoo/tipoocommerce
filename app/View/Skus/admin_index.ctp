<?php
	$this->Html->script(array('Views/Skus/admin_index'), array('inline' => false));
?>
<ol class="breadcrumb">
	<li><?php echo $this->Html->link('Produtos', array('controller' => 'produtos', 'action' => 'backToPaginatorIndex', 'admin' => true)); ?></li>
	<li class="active">Sku</li>
</ol>

<div class="page-header">
	<?php echo $this->Form->input('produto_id', array('type' => 'hidden', 'value' => $produto_id)); ?>
	<h4><?php echo $produto['Produto']['descricao']; ?></h4>
</div>

<div class="alert alert-info">
	<i class="glyphicon glyphicon-info-sign"></i> <strong>ORDENAÇÃO: </strong> Arraste para ordenar os skus.
</div>

<div class="table-responsive">
	<table class="table table-striped table-hover">
		<thead>
			<tr>
				<th class="col-md-1 text-center">#</th>
				<th class="col-xs-3 col-sm-3 col-md-5">SKU</th>
				<th class="col-xs-3 col-sm-3 col-md-5">Descrição</th>
				<th class="visible-xs visible-sm"></th>
				<th class="visible-xs visible-sm"></th>
				<th class="text-center"><?php echo $this->Html->link('<span class="glyphicon glyphicon-plus"></span>', array('action' => 'adicionar', $produto['Produto']['id']), array('class' => 'btn btn-sm btn-primary', 'escape' => false,'data-placement' => 'left', 'rel' => 'tooltip', 'title' => 'Adicionar Novo')); ?></th>
			</tr>
		</thead>
		<tbody id="sortable">
			<?php
				foreach ($skus as $sku) {
			?>
					<tr class="ui-state-default" data-id="<?php echo $sku['Sku']['id']; ?>">
						<td class="text-center"><?php echo $sku['Sku']['id'] ?></td>
						<td><?php echo $sku['Sku']['sku'] ?>
							<br>
							<?php
								if (!$sku['Sku']['ativo']) {
									$status = '<span class="label label-danger">Inativo</span>';
								} else {
									$status = '<span class="label label-success">Ativo</span> ';
								}

								echo $status;
							?>
						</td>
						<td><?php echo $sku['Sku']['descricao'] ?></td>

						<td class="visible-xs visible-sm text-center seta-ordenacao" >
							<div class="ordenacao-desce " data-indice="<?php echo $sku['Sku']['id']; ?>"><span class="glyphicon glyphicon-arrow-down"></span></div>
						</td>
						<td class="visible-xs visible-sm text-center seta-ordenacao" >
							<div class="ordenacao-sobe " data-indice="<?php echo $sku['Sku']['id']; ?>"><span class="glyphicon glyphicon-arrow-up"></span></div>
						</td>

						<td class="text-center">
							<div class="btn-group">
								<button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu pull-right text-left" role="menu">
									<li><?php echo $this->Html->link('<span class="glyphicon glyphicon-edit"></span> Editar', array('action' => 'editar', $sku['Sku']['id'], $produto_id), array('escape' => false));?></li>
									<li><?php echo $this->Html->link('<span class="glyphicon "></span>Imagens', array('controller' => 'imagens', 'action' => 'index', 'referente' => 'sku_id', $sku['Sku']['id']), array('escape' => false));?></li>
									<?php
										$urlEstoque = array('controller' => 'estoque', 'action' => 'index', 'admin' => true, 'plugin' => null);

										if ($this->Permissoes->check($urlEstoque)) {

									?>
											<li><?php echo $this->Html->link('<span class="glyphicon"></span>Estoque', array('controller' => 'estoque', 'action' => 'movimentacao', 'admin' => true, $sku['Sku']['id']), array('escape' => false));?></li>
										<?php
											}
										if ($sku['Sku']['ativo']) {
									?>
											<li><?php echo $this->Form->postLink('<span class="glyphicon"></span> Desativar', array('action' => 'excluir', $sku['Sku']['id']), array('escape' => false), 'Deseja realmente desativar o Sku #' . $sku['Sku']['id'] . '?');?></li>
									<?php
										} else {

									?>
											<li><?php echo $this->Form->postLink('<span class="glyphicon"></span> Ativar', array('action' => 'ativar', $sku['Sku']['id']), array('escape' => false), 'Deseja realmente ativar o Sku #' . $sku['Sku']['id'] . '?');?></li>
									<?php
										}
									?>
								</ul>
							</div>
						</td>
					</tr>
			<?php
				}
			?>
		</tbody>
	</table>
</div>

