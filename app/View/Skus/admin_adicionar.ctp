<?php
	$this->Html->script(array('jquery.price_format.2.0.min', 'jquery.numeric','bootstrap-datepicker', 'bootstrap-datepicker.pt-BR', 'Views/Skus/admin_adicionar'), array('inline' => false));
	$this->Html->css(array('datepicker'), 'stylesheet', array('inline' => false));
?>

<ol class="breadcrumb">
	<li><?php echo $this->Html->link('Produtos', array('controller' => 'produtos', 'action' => 'backToPaginatorIndex', 'admin' => true)); ?></li>
	<li><?php echo $this->Html->link('Sku', array('action' => 'backToPaginatorIndex', 'admin' => true)); ?></li>
	<li  class="active">Adicionar</li>
</ol>

<?php
	echo $this->Form->create('Sku', array('type' => 'file'));
?>

<div class="row">
	<div class="col-md-6">

		<?php
			echo $this->Form->input('codigo_de_referencia', array('row' => true, 'div' => 'col-md-8', 'type' => 'text', 'label' => 'Código de Referência'));
			echo $this->Form->input('ean', array('row' => true, 'div' => 'col-md-8', 'type' => 'text', 'label' => 'EAN'));
			echo $this->Form->input('sku', array('row' => true, 'div' => 'col-md-8', 'type' => 'text', 'label' => 'SKU*'));
			echo $this->Form->input('descricao', array('row' => true, 'div' => 'col-md-8','type' => 'text', 'label' => 'Descrição*'));
			echo $this->Form->input('destaque', array('row' => true, 'div' => 'col-md-4', 'type' => 'checkbox', 'value' => true, 'label' => 'Destaque'));
			echo $this->Form->input('palavras_chave', array('row' => true, 'div' => 'col-md-8', 'label' => 'Palavras-chave', 'placeholder' => 'Palavras separadas por vírgula.'));
		?>

		<h3 class="form-titulos-admin">Estoque</h3>
		<?php
			echo $this->Form->input('qtd_estoque', array('row' => true, 'div' => 'col-md-4','type' => 'text', 'label' => 'Quantidade em Estoque'));
		?>
	</div>
	<div class="col-md-6">

		<h3 class="form-titulos-admin">Frete</h3>
		<?php
			echo $this->Form->input('peso', array('row' => true, 'div' => 'col-md-4','type' => 'text', 'label' => 'Peso (g)*'));
			echo $this->Form->input('largura', array('row' => true, 'div' => 'col-md-4','type' => 'text', 'label' => 'Largura (cm)*'));
			echo $this->Form->input('altura', array('row' => true, 'div' => 'col-md-4','type' => 'text', 'label' => 'Altura (cm)*'));
			echo $this->Form->input('profundidade', array('row' => true, 'div' => 'col-md-4','type' => 'text', 'label' => 'Profundidade (cm)*'));
		?>

		<!-- Pré-Venda -->
		<h3 class="form-titulos-admin">Pré-venda</h3>

		<?php
			echo $this->Form->input('permite_pre_venda', array('row' => true, 'div' => 'col-md-4','type' => 'checkbox', 'label' => 'Permite pré-venda'));
			$data_atual = date('Y-m-d');
			echo $this->Form->input('data_atual',array('type' => 'hidden', 'value' => $this->Formatacao->data($data_atual)));
		?>
		<div class="row previsao-entrega hide">
			<div class="col-md-4 form-group input-append date" id="dp1"  data-date-format="dd/mm/yyyy">
				<p class="text-muted">
					Com a opção de pré-venda você pode escolher a data de previsão de entrega OU adicionar uma carência em dias.
				</p>
				<label>Previsão de entrega</label>
				<div class="input-group">
					<span class="input-group-btn">
						<button class="btn btn-default add-on" type="button">
							<span class="glyphicon glyphicon-calendar"></span>
						</button>
					</span>
					<input name="data[Sku][previsao_entrega_pre_venda]" id="dp1-mask" data-date="<?php echo $this->Formatacao->data(date('Y-m-d')); ?>" size="16" class="form-control" type="text" >
				</div>
			</div>
		</div>

		<div class="row previsao-entrega hide">
			<?php echo $this->Form->input('carencia_pre_venda', array('row' => false, 'div' => 'col-md-4','type' => 'text', 'label' => 'Carência em dias')); ?>
		</div>
		<!-- .end Pré-Venda -->

		<h3 class="form-titulos-admin">Imagens</h3>

		<?php
			echo $this->Form->input('Imagem.0.imagem', array('type' => 'file', 'label' => 'Destaque'));
			echo $this->Form->input('Imagem.0.destaque', array('type' => 'hidden', 'value' => true));
		?>
	</div>
</div>

<hr />

<h3>Características</h3>
<div class="row">
	<div class="col-md-6">
		<h4 class="form-titulos-admin">Características Gerais</h4>
		<div class="row">
			<div id="caracteristicasGerias" class="col-md-8">
				<?php
					if (count($caracteristicas_gerais)) {

						foreach ($caracteristicas_gerais as $key => $caracteristica) {

							echo $this->Form->input('CaracteristicasValoresSelecionado.' . $caracteristica['Caracteristica']['id'] . '.caracteristica_id', array('type' => 'hidden', 'value' => $caracteristica['Caracteristica']['id']));
							echo $this->Form->input('CaracteristicasValoresSelecionado.' . $caracteristica['Caracteristica']['id'] . '.caracteristica_tipo', array('type' => 'hidden', 'value' => $caracteristica['Caracteristica']['tipo']));

							if ($caracteristica['Caracteristica']['tipo'] == 'T') {
								echo $this->Form->input('CaracteristicasValoresSelecionado.' . $caracteristica['Caracteristica']['id'] . '.descricao', array('label' => $caracteristica['Caracteristica']['descricao'], 'type' => 'text'));
							} else if ($caracteristica['Caracteristica']['tipo'] == 'TG') {
								echo $this->Form->input('CaracteristicasValoresSelecionado.' . $caracteristica['Caracteristica']['id'] . '.descricao_grande', array('label' => $caracteristica['Caracteristica']['descricao'], 'type' => 'textarea'));
							} else if ($caracteristica['Caracteristica']['tipo'] == 'L') {
								$listaValores = array();
								foreach ($caracteristica['CaracteristicasValor'] as $caracteristicaValor) {
									$listaValores[$caracteristicaValor['id']] = $caracteristicaValor['descricao'];
								}
								echo $this->Form->input('CaracteristicasValoresSelecionado.' . $caracteristica['Caracteristica']['id'] . '.caracteristicas_valor_id', array('empty' => 'Selecione', 'label' => $caracteristica['Caracteristica']['descricao'], 'options' => $listaValores));
							} else {
								if ($caracteristica['Caracteristica']['mascara'] == 'I') {
									$mascara = 'mascara-inteiro';
								} else {
									$mascara = 'mascara-decimal';
								}
								echo $this->Form->input('CaracteristicasValoresSelecionado.' . $caracteristica['Caracteristica']['id'] . '.descricao', array('class' => 'tipo-numero ' . $mascara, 'label' => $caracteristica['Caracteristica']['descricao']));
							}
						}
					} else {
				?>
						<div class="controls well">
							<input id="contador" type="hidden" value="0">
							<span>Nenhuma característica geral encontrada.</span>
						</div>
				<?php
					}
				?>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<h4 class="form-titulos-admin">Características da Categoria</h4>
		<div class="row">
			<div id="caracteristicasCategoria" class="col-md-8">
				<?php
					if (count($caracteristicas_categoria)) {

						foreach ($caracteristicas_categoria as $key => $caracteristica) {

							echo $this->Form->input('CaracteristicasValoresSelecionado.' . $caracteristica['id'] . '.caracteristica_id', array('type' => 'hidden', 'value' => $caracteristica['id']));
							echo $this->Form->input('CaracteristicasValoresSelecionado.' . $caracteristica['id'] . '.caracteristica_tipo', array('type' => 'hidden', 'value' => $caracteristica['tipo']));

							if ($caracteristica['tipo'] == 'T') {
								echo $this->Form->input('CaracteristicasValoresSelecionado.' . $caracteristica['id'] . '.descricao', array('label' => $caracteristica['descricao'], 'type' => 'text'));
							} else if ($caracteristica['tipo'] == 'TG') {
								echo $this->Form->input('CaracteristicasValoresSelecionado.' . $caracteristica['id'] . '.descricao_grande', array('label' => $caracteristica['descricao'], 'type' => 'textarea'));
							} else if ($caracteristica['tipo'] == 'L') {
								$listaValores = array();
								foreach ($caracteristica['CaracteristicasValor'] as $caracteristicaValor) {
									$listaValores[$caracteristicaValor['id']] = $caracteristicaValor['descricao'];
								}
								echo $this->Form->input('CaracteristicasValoresSelecionado.' . $caracteristica['id'] . '.caracteristicas_valor_id', array('empty' => 'Selecione', 'label' => $caracteristica['descricao'], 'options' => $listaValores));
							} else {
								if ($caracteristica['mascara'] == 'I') {
									$mascara = 'mascara-inteiro';
								} else {
									$mascara = 'mascara-decimal';
								}
								echo $this->Form->input('CaracteristicasValoresSelecionado.' . $caracteristica['id'] . '.descricao', array('class' => 'tipo-numero ' . $mascara, 'label' => $caracteristica['descricao']));
							}
						}
					} else {
				?>
						<div class="controls well">
							<input id="contador" type="hidden" value="0">
							<span>Nenhuma característica encontrada para a categoria deste sku.</span>
						</div>
				<?php
					}
				?>
			</div>
		</div>
	</div>
</div>

<?php
	if ($this->Configuracoes->get('mercado_livre_ativo')) {
?>
		<hr />

		<h3>Marketplace - Características</h3>
		<div class="row">
			<div class="col-md-6">
				<h4 class="form-titulos-admin">Mercado Livre</h4>
				<div class="row">
					<div id="caracteristicasMercadoLivre" class="col-md-8">
						<?php
							if (count($mercado_livre_caracteristicas_categoria)) {

								foreach ($mercado_livre_caracteristicas_categoria as $key => $caracteristica) {

									echo $this->Form->input('CaracteristicasValoresSelecionado.' . $caracteristica['id'] . '.caracteristica_id', array('type' => 'hidden', 'value' => $caracteristica['id']));
									echo $this->Form->input('CaracteristicasValoresSelecionado.' . $caracteristica['id'] . '.caracteristica_tipo', array('type' => 'hidden', 'value' => $caracteristica['tipo']));

									$listaValores = array();
									foreach ($caracteristica['CaracteristicasValor'] as $caracteristicaValor) {
										$listaValores[$caracteristicaValor['id']] = $caracteristicaValor['mercado_livre_caracteristicas_valor_descricao'];
									}

									$label = $caracteristica['mercado_livre_caracteristica_descricao'];
									if ($caracteristica['mercado_livre_caracteristica_campo_obrigatorio']) {
										$label = $label . '*';
									}

									echo $this->Form->input('CaracteristicasValoresSelecionado.' . $caracteristica['id'] . '.caracteristicas_valor_id', array('empty' => 'Selecione', 'label' => $label, 'options' => $listaValores, 'data-required' => $caracteristica['mercado_livre_caracteristica_campo_obrigatorio']));
								}
							} else {
						?>
								<div class="controls well">
									<input id="contador" type="hidden" value="0">
									<span>Nenhuma característica encontrada para a categoria do Mercado Livre deste sku.</span>
								</div>
						<?php
							}
						?>
					</div>
				</div>
			</div>
		</div>

<?php
	}
?>

<?php
	echo $this->Form->actions();
	echo $this->Form->end();
?>
