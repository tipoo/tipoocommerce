<?php echo $this->Html->scriptBlock('var tipooPagseguroSessionId = "' . $session_id . '";', array('inline' => false)); ?>
<?php echo $this->Html->script(array('Controles/tipoo.pagseguro-transparente'), array('inline' => false)); ?>
<?php echo $this->Html->css(array('Controles/tipoo.pagseguro-transparente'), 'stylesheet', array('inline' => false)); ?>

<script type="text/javascript" src="https://stc.<?php echo $this->Configuracoes->get('pagseguro_transparente_url') ?>/pagseguro/api/v2/checkout/pagseguro.directpayment.js"></script>
<script type="text/javascript">
	PagSeguroDirectPayment.setSessionId(tipooPagseguroSessionId);
</script>

<?php echo $this->Session->flash() ?>
<div id="PagseguroTransparente" class="pagseguro-transparente forma-pagamento">

	<div class="pagseguro-transparente-boleto">
		<h2>Pagamento com Boleto Bancário</h2>
		<?php
			echo $this->Form->create('PagSeguroTransparente', array('class' => 't-pagseguro-transparente-pagamento-boleto-form'));
			echo $this->Form->input('pedido_id', array('type' => 'hidden', 'value' => $pedido['Pedido']['id']));
			echo $this->Form->input('meio_pagamento', array('type' => 'hidden', 'value' => 'boleto'));
			echo $this->Form->input('sender_hash', array('type' => 'hidden', 'class' => 't-sender-hash'));
			echo $this->Form->submit('Pagar com boleto bancário', array('class' => 't-pagar-boleto', 'data-forma' => 'boleto'));
			echo $this->Form->end();
		?>
	</div>

	<div class="pagseguro-transparente-cartao-credito">
		<h2>Pagamento com Cartão de Crédito</h2>
		<?php
			echo $this->Form->create('PagSeguroTransparente', array('class' => 't-pagseguro-transparente-pagamento-cartao-credito-form'));
			echo $this->Form->input('pedido_id', array('type' => 'hidden', 'value' => $pedido['Pedido']['id']));
		?>
		<ul>
			<?php
				$selecionado = ' checked';
				foreach ($bandeiras_pagseguro_cc as $bandeira_pagseguro) {
			?>
				<li>
					<?php echo $this->Html->image('pagseguro/' . $bandeira_pagseguro['PagseguroTransparenteBandeira']['imagem']); ?>
					<input type="radio" name="data[PagSeguroTransparente][cc_bandeira]" value="<?php echo $bandeira_pagseguro['PagseguroTransparenteBandeira']['name'] ?>" <?php echo $selecionado ?> class="t-pagseguro-transparente-cc-bandeira" id="PagseguroCcBandeira<?php echo $bandeira_pagseguro['PagseguroTransparenteBandeira']['codigo'] ?>" data-name="<?php echo $bandeira_pagseguro['PagseguroTransparenteBandeira']['name'] ?>">
				</li>
			<?php
					$selecionado = '';
				}
			?>
		</ul>
		<?php
			echo $this->Form->input('cc_numero', array('label' => 'Número do cartão', 'class' => 't-pagseguro-transparente-cc-numero'));
			echo $this->Form->input('cc_codigo_seguranca', array('label' => 'Código de segurança', 'class' => 't-pagseguro-transparente-cc-codigo-seguranca'));
			echo $this->Form->input('cc_validade_mes', array('label' => 'Validade (mês)', 'class' => 't-pagseguro-transparente-cc-validade-mes'));
			echo $this->Form->input('cc_validade_ano', array('label' => 'Validade (ano)', 'class' => 't-pagseguro-transparente-cc-validade-ano'));
			echo $this->Form->input('cc_senha', array('label' => 'Senha do Cartão', 'class' => 't-pagseguro-transparente-cc-senha', 'div' => 't-pagseguro-transparente-cc-senha-container'));
			echo $this->Form->input('cc_parcelas', array('type' => 'select', 'options' => array('1' => '1x ' . $this->Formatacao->moeda($pedido['Pedido']['valor_total']) . ' sem juros'), 'class' => 't-pagseguro-transparente-cc-parcelas'));
			echo $this->Form->input('cc_valor_parcelas', array('type' => 'hidden', 'value' => $pedido['Pedido']['valor_total'], 'class' => 't-pagseguro-transparente-cc-valor-parcelas'));
			echo $this->Form->input('cc_token', array('type' => 'hidden', 'class' => 't-pagseguro-transparente-cc-token'));
			echo $this->Form->input('meio_pagamento', array('type' => 'hidden', 'value' => 'creditCard'));
			echo $this->Form->input('sender_hash', array('type' => 'hidden', 'class' => 't-sender-hash'));
		?>
		<div class="t-pagseguro-transparente-dados-dono-cartao">
			<h3>Dados do dono do cartão</h3>
			<div class="t-text t-dados-dono-cartao">
				<div class="input text">
					<label for="DonoCartaoNome">Nome impresso no cartão:</label>
					<span><?php echo $pedido['Cliente']['nome'] ?></span>
				</div>
				<div class="input text">
					<label for="DonoCartaoDataNascto">Data de nascimento:</label>
					<span><?php echo $this->Formatacao->data($pedido['Cliente']['data_nascto']) ?></span>
				</div>
				<div class="input text">
					<label for="DonoCartaoCpf">CPF:</label>
					<span><?php echo $pedido['Cliente']['cpf'] ?></span>
				</div>
				<div class="input text">
					<label for="DonoCartaoTelefone">Telefone:</label>
					<span><?php echo $pedido['Cliente']['tel_residencial'] ?></span>
				</div>
			</div>
			<div class="t-input t-dados-dono-cartao" style="display: none;">
				<?php
					echo $this->Form->input('DonoCartao.nome', array('label' => 'Nome impresso no cartão', 'value' => $pedido['Cliente']['nome']));
					echo $this->Form->input('DonoCartao.data_nascto', array('label' => 'Data de nascimento', 'value' => $pedido['Cliente']['data_nascto'] != '' ? $this->Formatacao->data($pedido['Cliente']['data_nascto']) : ''));
					echo $this->Form->input('DonoCartao.cpf', array('label' => 'CPF', 'value' => $pedido['Cliente']['cpf']));
					if ($pedido['Cliente']['tipo_pessoa'] == 'F') {
						echo $this->Form->input('DonoCartao.telefone', array('label' => 'Telefone', 'value' => $pedido['Cliente']['tel_residencial']));
					} else {
						echo $this->Form->input('DonoCartao.telefone', array('label' => 'Telefone', 'value' => $pedido['Cliente']['tel_comercial_pj']));
					}
				?>
			</div>
			<a href="#" class="t-editar">Editar</a>
		</div>
		<?php
			echo $this->Form->submit('Pagar com cartão de crédito', array('class' => 't-pagar-cartao-credito', 'data-forma' => 'creditCard'));
			echo $this->Form->end();
		?>
	</div>

	<div class="pagseguro-transparente-debito">
		<h2>Pagamento com Débito Online</h2>
		<?php
			echo $this->Form->create('PagSeguroTransparente', array('class' => 't-pagseguro-transparente-pagamento-debito-form'));
			echo $this->Form->input('pedido_id', array('type' => 'hidden', 'value' => $pedido['Pedido']['id']));
			echo $this->Form->input('meio_pagamento', array('type' => 'hidden', 'value' => 'eft'));
			echo $this->Form->input('sender_hash', array('type' => 'hidden', 'class' => 't-sender-hash'));
			echo $this->Form->input('etf_bank', array('type' => 'hidden', 'class' => 't-bank', 'value' => 'bradesco'));

		?>
		<ul>
			<?php
				$selecionado = ' checked';
				foreach ($bandeiras_pagseguro_debito as $bandeira_pagseguro) {
			?>
				<li>
					<?php echo $this->Html->image('pagseguro/' . $bandeira_pagseguro['PagseguroTransparenteBandeira']['imagem']); ?>
					<input type="radio" name="data[PagSeguroTransparente][etf_bank]" value="<?php echo $bandeira_pagseguro['PagseguroTransparenteBandeira']['name'] ?>" <?php echo $selecionado ?> class="t-pagseguro-transparente-debito-bandeira" id="PagseguroDebitoBandeira<?php echo $bandeira_pagseguro['PagseguroTransparenteBandeira']['codigo'] ?>" data-name="<?php echo $bandeira_pagseguro['PagseguroTransparenteBandeira']['name'] ?>">
				</li>
			<?php
					$selecionado = '';
				}
			?>
		</ul>
		<?php
			echo $this->Form->submit('Pagar com débito online', array('class' => 't-pagar-debito', 'data-forma' => 'eft'));
			echo $this->Form->end();
		?>
	</div>

	<div class="mensagem-alerta t-pagseguro-transparente-erro">
		<ul></ul>
	</div>

	<?php echo $this->Html->image('pagseguro/selo-pagseguro-transparente.gif'); ?>

</div>



