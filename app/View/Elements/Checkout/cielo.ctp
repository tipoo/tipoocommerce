<?php
	if ($this->Configuracoes->get('moip_boleto_ativo')) {
		echo $this->Html->scriptBlock('var tipooMoipCliente = ' . $cliente_boleto_moip . ';', array('inline' => false));
		echo $this->Html->script(array('json2', 'Controles/tipoo.moip'), array('inline' => false));
		echo $this->Html->css(array('Controles/tipoo.moip'), 'stylesheet', array('inline' => false));
?>

<div id="MoipWidget"
	data-token="<?php echo $dados_boleto_moip['token'] ?>"
	callback-method-success="tipooMoip.sucesso"
	callback-method-error="tipooMoip.falha"
	class="moip forma-pagamento">

	<div class="moip-erro">
		<ul></ul>
	</div>

	<div class="moip-form">
		<?php
			echo $this->Form->create('Moip', array('id' => 'MoipFinalizar', 'url' => array('controller' => 'checkout', 'action' => 'compra_finalizada')));
			echo $this->Form->input('pedido_id', array('type' => 'hidden', 'value' => $pedido['Pedido']['id']));
			echo $this->Form->input('forma', array('type' => 'hidden'));
			echo $this->Form->input('retorno', array('type' => 'hidden'));
			echo $this->Form->end();
		?>
	</div>

	<div class="moip-boleto">
		<h2>Pagamento com Boleto Bancário</h2>
		<button class="pagar-boleto" data-forma="BoletoBancario"> Pagar com boleto bancário </button>
	</div>
</div>

<?php
	}
?>

<div class="cielo forma-pagamento">
	<h2>Pagamento com Cartão</h2>
	<div class="mensagem-alerta mensagem-pagamento mensagem-cielo">
		<tipoo:mensagem chave="cielo" />
	</div>
	<?php
		echo $this->Form->create('Cielo');
		echo $this->Form->input('pedido_id', array('type' => 'hidden', 'value' => $pedido['Pedido']['id']));
		echo $this->Form->input('tipo', array('type' => 'hidden', 'value' => 'cielo'));
		if (isset($this->request->data['Cielo']['cc_bandeira'])) {
			echo $this->Form->input('cc_bandeira_ativa', array('type' => 'hidden', 'value' => $this->request->data['Cielo']['cc_bandeira']));
			echo $this->Form->input('parcela_id_ativa', array('type' => 'hidden', 'value' => $this->request->data['Cielo']['parcela_id']));
		}

		foreach ($cielo_bandeiras as $cielo_bandeira) {
	?>
			<div class="bandeira">
				<input type="radio" name="data[Cielo][cc_bandeira]" id="<?php echo $cielo_bandeira['CieloBandeira']['codigo'] ?>" class="cc_bandeira" value="<?php echo $cielo_bandeira['CieloBandeira']['codigo'] ?>" data-mascara-cc-numero="<?php echo $cielo_bandeira['CieloBandeira']['mascara_cc_numero'] ?>"  data-mascara-cc-codigo-seguranca="<?php echo $cielo_bandeira['CieloBandeira']['mascara_cc_codigo_seguranca'] ?>">
				<label for="<?php echo $cielo_bandeira['CieloBandeira']['codigo'] ?>">
					<?php echo $this->Html->image('cielo/'.$cielo_bandeira['CieloBandeira']['imagem']); ?>
				</label>
				<?php echo $this->Form->input('parcelas_'.$cielo_bandeira['CieloBandeira']['codigo'], array('type' => 'select', 'class' => 'parcelas '.$cielo_bandeira['CieloBandeira']['codigo'], 'options' => $this->Parcelas->gerarListaParcerlas($cielo_bandeira['Parcela'], $total_pedido))); ?>
			</div>
	<?php
		}
	?>
	<div class="clear-both"></div>
	<?php
		echo $this->Form->input('cc_numero', array('label' => 'Número do Cartão'));
		echo $this->Form->input('cc_codigo_seguranca', array('label' => 'Código de Segurança'));
		echo $this->Form->input('cc_validade_mes', array('label' => 'Validade (Mês)'));
		echo $this->Form->input('cc_validade_ano', array('label' => 'Validade (Ano)'));
		echo $this->Form->input('parcela_id', array('type' => 'select', 'label' => 'Parcelas'));
		echo $this->Form->submit('Finalizar Compra');
		echo $this->Form->end();
	?>
</div>

<?php
	if ($this->Configuracoes->get('moip_boleto_ativo')) {
?>
		<p>*O boleto será processado por <a href="https://site.moip.com.br/" target="_blank">Moip Pagamentos</a></p>
<?php
	}
?>