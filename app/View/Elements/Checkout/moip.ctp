<?php echo $this->Html->script(array('json2', 'Controles/tipoo.moip'), array('inline' => false)); ?>
<?php echo $this->Html->css(array('Controles/tipoo.moip'), 'stylesheet', array('inline' => false)); ?>

<?php echo $this->Session->flash() ?>
<div id="MoipWidget"
	data-token="<?php echo $dados_moip['token'] ?>"
	callback-method-success="tipooMoip.sucesso"
	callback-method-error="tipooMoip.falha"
	class="moip forma-pagamento">

	<div class="moip-erro">
		<ul></ul>
	</div>

	<div class="moip-form">
		<?php
			echo $this->Form->create('Moip', array('id' => 'MoipFinalizar', 'url' => array('controller' => 'checkout', 'action' => 'compra_finalizada')));
			echo $this->Form->input('pedido_id', array('type' => 'hidden', 'value' => $pedido['Pedido']['id']));
			echo $this->Form->input('forma', array('type' => 'hidden'));
			echo $this->Form->input('retorno', array('type' => 'hidden'));
			echo $this->Form->end();
		?>
	</div>

	<div class="moip-boleto">
		<h2>Pagamento com Boleto Bancário</h2>
		<button class="pagar-boleto" data-forma="BoletoBancario"> Pagar com boleto bancário </button>
	</div>

	<div class="moip-cartao-credito">
		<h2>Pagamento com Cartão de Crédito</h2>
		<?php
			echo $this->Form->create('Moip');
		?>
		<ul>
		<?php
			$selecionado = ' checked';
			foreach ($instituicoes_moip_cc as $instituicao_moip) {
		?>
			<li>
				<?php echo $this->Html->image('moip/'.$instituicao_moip['MoipInstituicao']['imagem']); ?>
				<input type="radio" name="data[Moip][cc_instituicao]" value="<?php echo $instituicao_moip['MoipInstituicao']['codigo'] ?>" <?php echo $selecionado ?> class="cc-instituicao" id="MoipCcInstituicao">
			</li>
		<?php
				$selecionado = '';
			}
		?>
		</ul>
		<div class="t-dados-dono-cartao" style="display: none;">
			<h3>Dados do dono do cartão</h3>
			<?php
				echo $this->Form->input('cc_dono_nome', array('label' => 'Nome impresso no cartão', 'value' => $cliente_moip['Nome']));
				echo $this->Form->input('cc_dono_data_nascto', array('label' => 'Data de nascimento', 'value' => $cliente_moip['DataNascimento']));
				echo $this->Form->input('cc_dono_cpf', array('label' => 'CPF', 'value' => $cliente_moip['Identidade']));
				echo $this->Form->input('cc_dono_telefone', array('label' => 'Telefone', 'value' => $cliente_moip['Telefone']));
			?>
		</div>
		<?php
			echo $this->Form->input('cc_numero', array('label' => 'Número do cartão'));
			echo $this->Form->input('cc_codigo_seguranca', array('label' => 'Código de segurança'));
			echo $this->Form->input('cc_validade_mes', array('label' => 'Validade (mês)'));
			echo $this->Form->input('cc_validade_ano', array('label' => 'Validade (ano)'));
			echo $this->Form->input('parcelas', array('type' => 'select', 'options' => $dados_moip['parcelas']));
			echo '<button class="pagar-cartao-credito" data-forma="CartaoCredito"> Pagar com cartão de crédito </button>';
			echo $this->Form->end();
		?>
	</div>
	<p>Este pagamento será processado por <a href="https://site.moip.com.br/" target="_blank">Moip Pagamentos</a></p>
</div>



