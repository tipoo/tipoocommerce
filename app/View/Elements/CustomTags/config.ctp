<?php
require_once APP.'Vendor'.DS.'CustomTags'.DS.'CustomTags.php';

$ct = new CustomTags(
	array(
		'parse_on_shutdown' => true,
		'tag_directory' => APP.'Vendor'.DS.'CustomTags'.DS.'tags'.DS,
		'sniff_for_buried_tags' => true,
		'tag_name' => 'tipoo',
		'tag_callback_prefix' => 'tipoo_',
		'view_context' => $this
	)
);
?>