<?php
	if ($this->Configuracoes->get('mercado_livre_ativo') && (isset($meli_url_etiqueta) || isset($meli_feedback))) {
?>
		<hr>
		<div class="mercado-livre">
			<h3>Mercado Livre</h3>
			<?php
				if (isset($meli_url_etiqueta)) {
			?>
					<p><strong>Etiqueta: </strong><?php echo $this->Html->link('Imprimir', array('controller' => 'pedidos', 'action' => 'imprimir_pdf', 'admin' => true, $pedido['Pedido']['id']), array('target' => '_blank')) ?></p>
			<?php
				}
			?>

			<?php
				if (isset($meli_feedback)) {
			?>
					<div class="meli-feedback row">
						<!-- Feedback Venda -->
						<div class="col-md-6">
							<div class="meli-feedback-venda well">
								<h4>Feedback Venda</h4>
								<?php
									if (isset($meli_feedback['body']->sale) && $meli_feedback['body']->sale) {
								?>
										<div class="feedback">
											<?php
												switch ($meli_feedback['body']->sale->rating) {
													case 'POSITIVE':
														echo '<div class="text-success"><span class="glyphicon glyphicon-thumbs-up"></span> Positivo</div>';
														break;
													case 'NEUTRAL':
														echo '<div><span class="glyphicon glyphicon-ban-circle"></span> Neutro</div>';
														break;
													case 'NEGATIVE':
														echo '<div class="text-danger"><span class="glyphicon glyphicon-thumbs-down text-danger"></span> Negativo</div>';
														break;
												}
											?>
											<br>
											<?php
												if ($meli_feedback['body']->sale->reason) {
											?>
													<p><strong>Razão:</strong> <?php echo $meli_razoes[$meli_feedback['body']->sale->reason] ?></p>
											<?php
												}
											?>
											<p><strong>Mensagem:</strong> <?php echo $meli_feedback['body']->sale->message; ?></p>
											<p><strong>Réplica:</strong> <?php echo $meli_feedback['body']->sale->reply ? $meli_feedback['body']->sale->reply : '<small class="text-muted">Não informado</small>'; ?></p>
											<br>
											<a href="#" class="btn-modificar-feedback">Modificar</a>
										</div>

										<div class="modificar-feedback hide">
											<?php
												echo $this->Form->create('MercadoLivreFeedbackSale');
												echo $this->Form->input('mercado_livre_pedido_id', array('type' => 'hidden', 'value' => $pedido['Pedido']['mercado_livre_pedido_id']));
												echo $this->Form->input('meli_feedback_sale_id', array('type' => 'hidden', 'value' => $meli_feedback_sale_id));
												echo $this->Form->input('fulfilled', array('row' => true, 'div' => 'col-md-12', 'type' => 'select', 'options' => array(true => 'Sim, já entreguei!', false => 'Não, não vou entregá-lo.'), 'empty' => 'Selecione', 'label' => 'Você já entregou o produto?*', 'value' => ($meli_feedback['body']->sale->fulfilled ? 1 : 0)));
											?>
											<div class="razoes" style="display: none;">
											<?php $this->log($meli_feedback['body']->sale->reason, LOG_DEBUG); ?>
												<?php echo $this->Form->input('reason', array('row' => true, 'div' => 'col-md-offset-1 col-md-11', 'type' => 'select', 'options' => $meli_razoes, 'empty' => 'Selecione', 'label' => 'Por que?*', 'value' => $meli_feedback['body']->sale->reason)); ?>
											</div>
											<?php
												echo $this->Form->input('rating', array('row' => true, 'div' => 'col-md-12', 'type' => 'select', 'options' => array('POSITIVE' => 'Sim', 'NEUTRAL' => 'Não tenho certeza', 'NEGATIVE' => 'Não'), 'empty' => 'Selecione', 'label' => 'Você o recomendaria para outros vendedores?*', 'value' => $meli_feedback['body']->sale->rating));
												echo $this->Form->input('message', array('row' => true, 'div' => 'col-md-12', 'type' => 'textarea', 'label' => 'Mensagem*', 'value' => $meli_feedback['body']->sale->message, 'maxlength' => 160, 'after' => '<small class="pull-right num-caracteres">Nº de caracteres: 160</small>'));
												echo '<a class="btn-cancelar" href="#" data-loading-text="Cancelar">Cancelar</a>';
												echo $this->Form->submit('Enviar', array('class' => 'btn btn-primary', 'data-loading-text' => 'Enviando...'));
												echo $this->Form->end();
											?>
										</div>

								<?php
									} else {
								?>
										<div class="enviar-feedback">
											<?php
												echo $this->Form->create('MercadoLivreFeedbackSale');
												echo $this->Form->input('mercado_livre_pedido_id', array('type' => 'hidden', 'value' => $pedido['Pedido']['mercado_livre_pedido_id']));
												echo $this->Form->input('fulfilled', array('row' => true, 'div' => 'col-md-12', 'type' => 'select', 'options' => array(true => 'Sim, já entreguei!', false => 'Não, não vou entregá-lo.'), 'empty' => 'Selecione', 'label' => 'Você já entregou o produto?*'));
											?>
											<div class="razoes" style="display: none;">
												<?php echo $this->Form->input('reason', array('row' => true, 'div' => 'col-md-offset-1 col-md-11', 'type' => 'select', 'options' => $meli_razoes, 'empty' => 'Selecione', 'label' => 'Por que?*')); ?>
											</div>
											<?php
												echo $this->Form->input('rating', array('row' => true, 'div' => 'col-md-12', 'type' => 'select', 'options' => array('positive' => 'Sim', 'neutral' => 'Não tenho certeza', 'negative' => 'Não'), 'empty' => 'Selecione', 'label' => 'Você o recomendaria para outros vendedores?'));
												echo $this->Form->input('message', array('row' => true, 'div' => 'col-md-12', 'type' => 'textarea', 'label' => 'Mensagem*', 'maxlength' => 160, 'after' => '<small class="pull-right num-caracteres">Nº de caracteres: 160</small>'));
												echo $this->Form->submit('Enviar', array('class' => 'btn btn-primary', 'data-loading-text' => 'Enviando...'));
												echo $this->Form->end();
											?>
										</div>
								<?php
									}
								?>
							</div>
						</div>
						<!-- .end Feedback Venda -->

						<!-- Feedback Compra -->
						<div class="col-md-6">
							<div class="meli-feedback-compra well">
								<h4>Feedback Compra (Cliente)</h4>
								<?php
									if (isset($meli_feedback['body']->purchase) && $meli_feedback['body']->purchase) {

										switch ($meli_feedback['body']->purchase->rating) {
											case 'POSITIVE':
												echo '<div class="text-success"><span class="glyphicon glyphicon-thumbs-up"></span> Positivo</div>';
												break;
											case 'NEUTRAL':
												echo '<div><span class="glyphicon glyphicon-ban-circle"></span> Neutro</div>';
												break;
											case 'NEGATIVE':
												echo '<div class="text-danger"><span class="glyphicon glyphicon-thumbs-down text-danger"></span> Negativo</div>';
												break;
										}
								?>
										<br>
										<?php
											if ($meli_feedback['body']->purchase->reason) {
										?>
												<p><strong>Razão:</strong> <?php echo $meli_razoes[$meli_feedback['body']->purchase->reason] ?></p>
										<?php
											}
										?>
										<p><strong>Mensagem:</strong> <?php echo $meli_feedback['body']->purchase->message ?></p>

										<?php
											if ($meli_feedback['body']->purchase->reply) {
										?>
												<p><strong>Réplica:</strong> <?php echo $meli_feedback['body']->purchase->reply; ?></p>
										<?php
											} else {
										?>
												<div class="reply-feedback hide">
													<?php
														echo $this->Form->create('MercadoLivreFeedbackReply');
														echo $this->Form->input('meli_feedback_purchase_id', array('type' => 'hidden', 'value' => $meli_feedback_purchase_id));
														echo $this->Form->input('message', array('row' => true, 'div' => 'col-md-12', 'type' => 'textarea', 'label' => 'Sua Réplica*', 'maxlength' => 250, 'after' => '<small class="pull-right num-caracteres-reply">Nº de caracteres: 250</small>'));
														echo '<a class="btn-cancelar-replica" href="#" data-loading-text="Cancelar">Cancelar</a>';
														echo $this->Form->submit('Replicar', array('class' => 'btn btn-primary', 'data-loading-text' => 'Enviando...'));
														echo $this->Form->end();
													?>
												</div>

												<a href="#" class="btn-reply-feedback">Replicar</a>
										<?php
											}
										?>
								<?php
									} else {
								?>
										<small class="text-muted">O cliente ainda não classificou a compra!</small>
								<?php
									}
								?>
							</div>
						</div>
						<!-- .end Feedback Compra -->
					</div>
			<?php
				}
			?>
		</div>
<?php
	}
?>