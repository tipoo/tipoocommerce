<div class="text-center">
	<?php
		if ($this->Paginator->getValor('pages') > 1) {
	?>
			<ul class="pagination">
				<?php
					echo $this->Paginator->prev('«', array('class' => 'prev', 'tag' => 'li', 'escape' => false), '<a href="javascript: void(0)">«</a>', array('class' => 'disabled', 'tag' => 'li', 'escape' => false));
					echo $this->Paginator->numbers(array('tag' => 'li', 'separator' => null));
					echo $this->Paginator->next('»', array('class' => 'next', 'tag' => 'li', 'escape' => false), '<a href="javascript: void(0)">»</a>', array('class' => 'disabled', 'tag' => 'li', 'escape' => false));
				?>
			</ul>
	<?php
		}
	?>

	<p class="counter well">
		<?php
			$count = $this->Paginator->getValor('count') ."<br>";
			if ($count == 1) {
				echo $this->Paginator->counter(array('format' => 'Foi encontrado {:count} registro. Exibindo registros de {:start} até {:end}. Página {:page} de {:pages}.'));
			} else if ($count > 1) {
				echo $this->Paginator->counter(array('format' => 'Foram encontrados {:count} registros. Exibindo registros de {:start} até {:end}. Página {:page} de {:pages}.'));
			} else {
				echo 'Nenhum registro foi encontrado.';
			}
		?>
	</p>
</div>