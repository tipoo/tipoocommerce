<nav class="navbar navbar-static-top navbar-default" role="navigation">
	<div class="container">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-navbar-collapse-admin-menu">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<?php echo $this->Html->link('<img src="' . $this->webroot . 'img/logo_tipoocommerce_admin_gray.jpg" />', array('controller' => 'paginas', 'action' => 'dashboard', 'admin' => true), array('class' => 'navbar-brand', 'escape' => false)) ?>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-navbar-collapse-admin-menu">
			<ul class="nav navbar-nav">
				<?php
					$urlMarcas = array('controller' => 'marcas', 'action' => 'index', 'admin' => true, 'plugin' => null);
					$urlCategorias = array('controller' => 'categorias', 'action' => 'index', 'admin' => true, 'plugin' => null);
					$urlProdutos = array('controller' => 'produtos', 'action' => 'index', 'admin' => true, 'plugin' => null);
					$urlColecoes = array('controller' => 'colecoes', 'action' => 'index', 'admin' => true, 'plugin' => null);
					$urlTags = array('controller' => 'tags', 'action' => 'index', 'admin' => true, 'plugin' => null);
					$urlServicos = array('controller' => 'servicos', 'action' => 'index', 'admin' => true, 'plugin' => null);

					if ($this->Permissoes->checkGroupOr($urlMarcas, $urlCategorias, $urlProdutos, $urlColecoes, $urlTags, $urlServicos)) {
				?>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">Catálogo <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<?php if ($this->Permissoes->check($urlProdutos)) { ?>
										<li>
											<?php echo $this->Html->link('Produtos e Skus', $urlProdutos); ?>
										</li>
								<?php } ?>

								<?php if ($this->Permissoes->check($urlCategorias)) { ?>
										<li>
											<?php echo $this->Html->link('Categorias', $urlCategorias); ?>
										</li>
								<?php } ?>

								<?php if ($this->Permissoes->check($urlMarcas)) { ?>
										<li>
											<?php echo $this->Html->link('Marcas', $urlMarcas); ?>
										</li>
								<?php } ?>

								<?php if ($this->Permissoes->check($urlColecoes)) { ?>
										<li>
											<?php echo $this->Html->link('Coleções', $urlColecoes); ?>
										</li>
								<?php } ?>

								<?php if ($this->Permissoes->check($urlTags)) { ?>
										<li>
											<?php echo $this->Html->link('Tags', $urlTags); ?>
										</li>
								<?php } ?>

								<?php if ($this->Permissoes->check($urlServicos)) { ?>
										<li>
											<?php echo $this->Html->link('Serviços', $urlServicos); ?>
										</li>
								<?php } ?>
							</ul>
						</li>
				<?php
					}
				?>

				<?php
					$urlPromocoes = array('controller' => 'promocoes', 'action' => 'index', 'admin' => true, 'plugin' => null);

					if ($this->Permissoes->checkGroupOr($urlPromocoes)) {
				?>
						<li class="dropdown">
							<?php if ($this->Permissoes->check($urlPromocoes)) { ?>
								<li>
									<?php echo $this->Html->link('Promoções', $urlPromocoes); ?>
								</li>
							<?php } ?>
						</li>
				<?php
					}
				?>

				<?php
					$urlStatusPedidos = array('controller' => 'statusPedidos', 'action' => 'index', 'admin' => true, 'plugin' => null);
					$urlPedidos = array('controller' => 'pedidos', 'action' => 'index', 'admin' => true, 'plugin' => null);

					if ($this->Permissoes->checkGroupOr($urlStatusPedidos, $urlPedidos)) {
				?>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">Pedidos <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<?php if ($this->Permissoes->check($urlPedidos)) { ?>
										<li>
											<?php echo $this->Html->link('Pedidos', $urlPedidos); ?>
										</li>
								<?php } ?>

								<?php if ($this->Permissoes->check($urlStatusPedidos)) { ?>
										<li>
											<?php echo $this->Html->link('Status de Pedidos', $urlStatusPedidos); ?>
										</li>
								<?php } ?>
							</ul>
						</li>
				<?php
					}
				?>

				<?php
					$urlNewsletter = array('controller' => 'newsletters', 'action' => 'index', 'admin' => true, 'plugin' => null);
					$urlCadastros = array('controller' => 'clientes', 'action' => 'index', 'admin' => true, 'plugin' => null);

					if ($this->Permissoes->checkGroupOr($urlNewsletter, $urlCadastros)) {
				?>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">Clientes <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<?php
									if ($this->Permissoes->check($urlCadastros)) { ?>
										<li>
											<?php echo $this->Html->link('Cadastros', $urlCadastros); ?>
										</li>
								<?php
									}
								?>
								<?php
									if ($this->Permissoes->check($urlNewsletter)) { ?>
										<li>
											<?php echo $this->Html->link('Newsletter', $urlNewsletter); ?>
										</li>
								<?php
									}
								?>
							</ul>
						</li>
				<?php
					}
				?>

				<?php
					$urlRelatorioVendas = array('controller' => 'relatorios', 'action' => 'relatorio_vendas', 'admin' => true, 'plugin' => null);
					$urlRelatorioEstoque = array('controller' => 'relatorios', 'action' => 'estoque', 'admin' => true, 'plugin' => null);
					$urlRelatorioAviseMe = array('controller' => 'relatorios', 'action' => 'relatorio_avise_me', 'admin' => true, 'plugin' => null);
					$urlRelatorioTermosBuscados = array('controller' => 'relatorios', 'action' => 'relatorio_termos_buscados', 'admin' => true, 'plugin' => null);
					$urlRelatorioAvaliacoes = array('controller' => 'relatorios', 'action' => 'relatorio_avaliacoes', 'admin' => true, 'plugin' => null);

					if ($this->Permissoes->checkGroupOr($urlRelatorioVendas, $urlRelatorioEstoque, $urlRelatorioAviseMe, $urlRelatorioAvaliacoes)) {
				?>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">Relatórios <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<?php if ($this->Permissoes->check($urlRelatorioAvaliacoes)) { ?>
										<li>
											<?php echo $this->Html->link('Relatório de Avaliações', $urlRelatorioAvaliacoes); ?>
										</li>
								<?php } ?>
								<?php if ($this->Permissoes->check($urlRelatorioAviseMe)) { ?>
										<li>
											<?php echo $this->Html->link('Relatório de Avise-me', $urlRelatorioAviseMe); ?>
										</li>
								<?php } ?>

								<?php if ($this->Permissoes->check($urlRelatorioEstoque)) { ?>
										<li>
											<?php echo $this->Html->link('Relatório de Estoque', $urlRelatorioEstoque); ?>
										</li>
								<?php } ?>
								<?php if ($this->Permissoes->check($urlRelatorioTermosBuscados)) { ?>
										<li>
											<?php echo $this->Html->link('Relatório de Termos Buscados', $urlRelatorioTermosBuscados); ?>
										</li>
								<?php } ?>
								<?php if ($this->Permissoes->check($urlRelatorioVendas, $urlRelatorioEstoque)) { ?>
										<li>
											<?php echo $this->Html->link('Relatório de Vendas', $urlRelatorioVendas); ?>
										</li>
								<?php } ?>
							</ul>
						</li>
				<?php
					}
				?>

				<?php
					$urlBanners = array('controller' => 'bannerTipos', 'action' => 'index', 'admin' => true, 'plugin' => null);
					$urlPaginas = array('controller' => 'paginas', 'action' => 'index', 'admin' => true, 'plugin' => null);
					$urlCms = array('controller' => 'cms', 'action' => 'index', 'admin' => true, 'plugin' => null);
					$urlMenus = array('controller' => 'menus', 'action' => 'index', 'admin' => true, 'plugin' => null);
					$urlLojas = array('controller' => 'lojas', 'action' => 'index', 'admin' => true, 'plugin' => null);

					if ($this->Permissoes->checkGroupOr($urlBanners, $urlPaginas, $urlCms, $urlMenus, $urlLojas)) {
				?>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">Conteúdos <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<?php if ($this->Permissoes->check($urlBanners)) { ?>
										<li>
											<?php echo $this->Html->link('Banners', $urlBanners); ?>
										</li>
								<?php } ?>

								<?php if ($this->Permissoes->check($urlPaginas)) { ?>
										<li>
											<?php echo $this->Html->link('Páginas', $urlPaginas); ?>
										</li>
								<?php } ?>

								<?php if ($this->Permissoes->check($urlMenus)) { ?>
										<li>
											<?php echo $this->Html->link('Menus', $urlMenus); ?>
										</li>
								<?php } ?>

								<?php if ($this->Permissoes->check($urlLojas)) { ?>
										<li>
											<?php echo $this->Html->link('Lojas', $urlLojas, array('tabindex' => '-1')); ?>
										</li>
								<?php } ?>

								<?php if ($this->Permissoes->check($urlCms)) { ?>
										<li>
											<?php echo $this->Html->link('CMS', $urlCms); ?>
										</li>
								<?php } ?>
							</ul>
						</li>
				<?php
					}
				?>

				<?php
					$urlConfiguracoesGerais = array('controller' => 'configuracoes', 'action' => 'index', 'admin' => true, 'plugin' => null);
					$urlLogradouros = array('controller' => 'enderecos', 'action' => 'logradouro', 'admin' => true, 'plugin' => null);
					$urlFreteTransportadoras = array('controller' => 'freteTransportadoras', 'action' => 'index', 'admin' => true, 'plugin' => null);
					$urlPerfis = array('controller' => 'perfis', 'action' => 'index', 'admin' => true, 'plugin' => null);
					$urlObjetos = array('controller' => 'objetos', 'action' => 'index', 'admin' => true, 'plugin' => null);
					$urlPermissoes = array('controller' => 'permissoes', 'action' => 'index', 'admin' => true, 'plugin' => null);
					$urlUsuarios = array('controller' => 'usuarios', 'action' => 'index', 'admin' => true, 'plugin' => null);
					$urlSlugs = array('controller' => 'slugs', 'action' => 'index', 'admin' => true, 'plugin' => null);
					$urlMarketplaces = array('controller' => 'marketplaces', 'action' => 'index', 'admin' => true, 'plugin' => null);
					$urlCamposComplementares = array('controller' => 'camposComplementares', 'action' => 'index', 'admin' => true, 'plugin' => null);

					if ($this->Permissoes->checkGroupOr($urlLogradouros, $urlFreteTransportadoras, $urlPerfis, $urlObjetos, $urlPermissoes, $urlUsuarios, $urlConfiguracoesGerais, $urlMarketplaces)) {
				?>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">Configurações <b class="caret"></b></a>
							<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
								<?php if ($this->Permissoes->check($urlConfiguracoesGerais)) { ?>
										<li>
											<?php echo $this->Html->link('Configurações Gerais', $urlConfiguracoesGerais, array('tabindex' => '-1')); ?>
										</li>
								<?php } ?>
								<?php if ($this->Permissoes->check($urlLogradouros)) { ?>
										<li>
											<?php echo $this->Html->link('Logradouros', $urlLogradouros, array('tabindex' => '-1')); ?>
										</li>
								<?php } ?>

								<?php if ($this->Permissoes->check($urlFreteTransportadoras)) { ?>
										<li>
											<?php echo $this->Html->link('Frete Transportadoras', $urlFreteTransportadoras, array('tabindex' => '-1')); ?>
										</li>
								<?php } ?>
								<?php if ($this->Permissoes->check($urlMarketplaces)) { ?>
										<li>
											<?php echo $this->Html->link('Marketplaces', $urlMarketplaces); ?>
										</li>
								<?php } ?>
								<?php if ($this->Permissoes->check($urlObjetos)) { ?>
										<li>
											<?php echo $this->Html->link('Objetos', $urlObjetos); ?>
										</li>
								<?php } ?>

								<?php if ($this->Permissoes->check($urlPerfis)) { ?>
										<li>
											<?php echo $this->Html->link('Perfis', $urlPerfis); ?>
										</li>
								<?php } ?>

								<?php if ($this->Permissoes->check($urlPermissoes)) { ?>
										<li>
											<?php echo $this->Html->link('Permissões', $urlPermissoes); ?>
										</li>
								<?php } ?>

								<?php if ($this->Permissoes->check($urlUsuarios)) { ?>
										<li>
											<?php echo $this->Html->link('Usuários', $urlUsuarios); ?>
										</li>
								<?php } ?>

								<?php if ($this->Permissoes->check($urlSlugs)) { ?>
										<li>
											<?php echo $this->Html->link('Mapeamento de Url', $urlSlugs); ?>
										</li>
								<?php } ?>

								<?php if ($this->Permissoes->check($urlCamposComplementares)) { ?>
										<li>
											<?php echo $this->Html->link('Campos Complementares', $urlCamposComplementares); ?>
										</li>
								<?php } ?>
							</ul>
						</li>
				<?php
					}
				?>

			</ul>

			<?php if ($usuarioAutenticado) { ?>
					<ul class="nav navbar-nav navbar-right">
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span> <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li>
									<?php echo $this->Html->link('Alterar Senha', array('controller' => 'usuarios', 'action' => 'alterar_senha', 'admin' => true)); ?>
								</li>
								<li class="divider"></li>
								<li>
									<?php echo $this->Html->link('Sair', array('controller' => 'usuarios', 'action' => 'logout', 'admin' => true)); ?>
								</li>
							</ul>
						</li>
					</ul>
			<?php } ?>
		</div><!-- /.navbar-collapse -->
	</div>
</nav>