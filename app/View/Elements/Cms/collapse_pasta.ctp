<div class="collapse-ctrl pasta panel-group" id="collapse-parent-pasta">

	<!-- CRIAR PASTA -->
	<div class="panel panel-default">
		<div class="panel-heading">
			<h4 class="panel-title">
				<a class="collapsed" data-toggle="collapse" data-parent="#collapse-parent-pasta" href="#collapsePastaCriarPasta">
					<span class="glyphicon glyphicon-folder-open"></span>&nbsp;
					Criar pasta
				</a>
			</h4>
		</div>
		<div id="collapsePastaCriarPasta" class="panel-collapse collapse">
			<div class="panel-body">
				<?php
					echo $this->Form->create('Cms');
					echo $this->Form->input('nova_pasta', array('type' => 'text', 'label' => 'Nova pasta: '));
					echo $this->Html->link('<span class="glyphicon glyphicon-folder-open"></span> &nbsp;Criar pasta', '#', array('class' => 'btn btn-success btn-sm criar-pasta', 'escape' => false));
					echo $this->Form->end();
				?>
			</div>
		</div>
	</div>

	<!-- CRIAR ARQUIVO -->
	<div class="panel panel-default">
		<div class="panel-heading">
			<h4 class="panel-title">
				<a data-toggle="collapse" data-parent="#collapse-parent-pasta" href="#collapsePastaCriarArquivo">
					<span class="glyphicon glyphicon-file"></span>
					Criar arquivo
				</a>
			</h4>
		</div>
		<div id="collapsePastaCriarArquivo" class="panel-collapse in">
			<div class="panel-body">
				<?php
					echo $this->Form->create('Cms', array('id' => 'pasta-criar-arquivo'));
					echo $this->Form->input('novo_arquivo', array('type' => 'text', 'label' => 'Novo arquivo: '));
					echo $this->Html->link('<span class="glyphicon glyphicon-file"></span> Criar arquivo', '#', array('class' => 'btn btn-success btn-sm criar-arquivo', 'escape' => false));
					echo $this->Form->end();
				?>
			</div>
		</div>
	</div>

	<!-- ENVIAR ARQUIVOS -->
	<div class="panel panel-default">
		<div class="panel-heading">
			<h4 class="panel-title">
				<a class="collapsed" data-toggle="collapse" data-parent="#collapse-parent-pasta" href="#collapsePastaEnviarArquivos">
					<span class="glyphicon glyphicon-upload"></span>
					Enviar arquivos
				</a>
			</h4>
		</div>
		<div id="collapsePastaEnviarArquivos" class="panel-collapse collapse">
			<div class="panel-body">
				<button class="btn btn-success btn-sm fileinput-button">
				<span class="glyphicon glyphicon-upload"></span>
					Enviar arquivos
					<!-- The file input field used as target for the file upload widget -->
					<input id="fileupload-pasta" class="fileupload" type="file" name="files[]" data-url="<?php echo Router::url('/admin/cms/ajax_upload'); ?>" multiple>
				</button>
			</div>
		</div>
	</div>

	<!-- EXCLUIR PASTA -->
	<div class="panel panel-default">
		<div class="panel-heading">
			<h4 class="panel-title">
				<a class="collapsed" data-toggle="collapse" data-parent="#collapse-parent-pasta" href="#collapsePastaExcluirPasta">
					<span class="glyphicon glyphicon-trash"></span>
					Excluir pasta
				</a>
			</h4>
		</div>
		<div id="collapsePastaExcluirPasta" class="panel-collapse collapse">
			<div class="panel-body">
					<div class="alert alert-warning alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<strong>ATENÇÃO: </strong> Antes de excluir uma pasta é recomendado que você <?php echo $this->Html->link('faça um backup', array('action' => 'backup'), array('class' => 'btn btn-success btn-xs backup')); ?> completo do tema da sua loja.
					</div>
					<?php
						echo $this->Html->link('<span class="glyphicon glyphicon-trash"></span> Excluir esta pasta', '#', array('class' => 'btn btn-danger btn-sm excluir-pasta', 'escape' => false));
					?>
			</div>
		</div>
	</div>
</div>