<div class="collapse-ctrl arquivo panel-group" id="collapse-parent-arquivo">

	<!-- EXCLUIR ARQUIVO -->
	<div class="panel panel-default">
		<div class="panel-heading">
			<h4 class="panel-title">
				<a data-toggle="collapse" data-parent="#collapse-parent-arquivo" href="#collapseArquivoExcluir">
					<span class="glyphicon glyphicon-trash"></span>
					Excluir arquivo
				</a>
			</h4>
		</div>
		<div id="collapseArquivoExcluir" class="panel-collapse in">
			<div class="panel-body">
				<?php
					echo $this->Html->link('<span class="glyphicon glyphicon-trash"></span> Excluir este arquivo', '#', array('class' => 'btn btn-danger btn-sm excluir-arquivo', 'escape' => false));
				?>
			</div>
		</div>
	</div>
</div>