<div class="collapse-ctrl raiz panel-group" id="collapse-parent-raiz">

	<!-- CRIAR BACKUP -->
	<div class="panel panel-default">
		<div class="panel-heading">
			<h4 class="panel-title">
				<a data-toggle="collapse" data-parent="#collapse-parent-raiz" href="#collapseRaizBackup">
					<span class="glyphicon glyphicon-download-alt"></span>
					Backup
				</a>
			</h4>
		</div>
		<div id="collapseRaizBackup" class="panel-collapse in">
			<div class="panel-body">
				<div class="alert alert-warning alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<strong>ATENÇÃO: </strong> Para sua segurança efetue antes de iniciar qualquer alteração efetue backup. Repita esta ação sempre que possível.
				</div>
				<p class="text-center">
				<?php
					echo $this->Html->link('<span class="glyphicon glyphicon-download-alt"></span> Fazer Backup', array('action' => 'backup'), array('class' => 'btn btn-success btn-lg backup', 'escape' => false));
				?>
				</p>
			</div>
		</div>
	</div>

	<!-- CRIAR PASTA -->
	<div class="panel panel-default">
		<div class="panel-heading">
			<h4 class="panel-title">
				<a class="collapsed" data-toggle="collapse" data-parent="#collapse-parent-raiz" href="#collapseRaizCriarPasta">
					<span class="glyphicon glyphicon-folder-open"></span>&nbsp;
					Criar pasta
				</a>
			</h4>
		</div>
		<div id="collapseRaizCriarPasta" class="panel-collapse collapse">
			<div class="panel-body">
				<?php
					echo $this->Form->create('Cms');
					echo $this->Form->input('nova_pasta', array('type' => 'text', 'label' => 'Nova pasta: '));
					echo $this->Html->link('<span class="glyphicon glyphicon-folder-open"></span> &nbsp;Criar pasta', '#', array('class' => 'btn btn-success btn-sm criar-pasta', 'escape' => false));
					echo $this->Form->end();
				?>
			</div>
		</div>
	</div>

	<!-- CRIAR ARQUIVO -->
	<div class="panel panel-default">
		<div class="panel-heading">
			<h4 class="panel-title">
				<a class="collapsed" data-toggle="collapse" data-parent="#collapse-parent-raiz" href="#collapseRaizCriarArquivo">
					<span class="glyphicon glyphicon-file"></span>
					Criar arquivo
				</a>
			</h4>
		</div>
		<div id="collapseRaizCriarArquivo" class="panel-collapse collapse">
			<div class="panel-body">
				<?php
					echo $this->Form->create('Cms', array('id' => 'raiz-criar-arquivo'));
					echo $this->Form->input('novo_arquivo', array('type' => 'text', 'label' => 'Novo arquivo: '));
					echo $this->Html->link('<span class="glyphicon glyphicon-file"></span> Criar arquivo', '#', array('class' => 'btn btn-success btn-sm criar-arquivo', 'escape' => false));
					echo $this->Form->end();
				?>
			</div>
		</div>
	</div>

	<!-- ENVIAR ARQUIVOS -->
	<div class="panel panel-default">
		<div class="panel-heading">
			<h4 class="panel-title">
				<a class="collapsed" data-toggle="collapse" data-parent="#collapse-parent-raiz" href="#collapseRaizEnviarArquivos">
					<span class="glyphicon glyphicon-upload"></span>
					Enviar arquivos
				</a>
			</h4>
		</div>
		<div id="collapseRaizEnviarArquivos" class="panel-collapse collapse">
			<div class="panel-body">
				<div class="alert alert-warning alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<strong>ATENÇÃO: </strong> Para substituir um arquivo é necessário exclui-lo previamente.
				</div>
				<button class="btn btn-success btn-sm fileinput-button">
					<span class="glyphicon glyphicon-upload"></span>
					Enviar arquivos
					<!-- The file input field used as target for the file upload widget -->
					<input id="fileupload-raiz" class="fileupload" type="file" name="files[]" data-url="<?php echo Router::url('/admin/cms/ajax_upload'); ?>" multiple />
				</button>
			</div>
		</div>
	</div>
</div>