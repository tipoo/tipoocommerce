<div class="collapse-ctrl arquivo_editor panel-group" id="collapse-parent-arquivo_editor">

	<!-- ABRIR ARQUIVO -->
	<div class="panel panel-default">
		<div class="panel-heading">
			<h4 class="panel-title">
				<a data-toggle="collapse" data-parent="#collapse-parent-arquivo_editor" href="#collapseArquivoEditorAbrir">
					<span class="glyphicon glyphicon-file"></span>
					Abrir arquivo
				</a>
			</h4>
		</div>
		<div id="collapseArquivoEditorAbrir" class="panel-collapse in">
			<div class="panel-body">
				<?php
					echo $this->Html->link('<span class="glyphicon glyphicon-file"></span> Abrir arquivo com editor on line', '#', array('class' => 'btn btn-success btn-sm abrir-editor', 'escape' => false));
				?>
			</div>
		</div>
	</div>

	<!-- EXCLUIR ARQUIVO -->
	<div class="panel panel-default">
		<div class="panel-heading">
			<h4 class="panel-title">
				<a class="collapsed" data-toggle="collapse" data-parent="#collapse-parent-arquivo_editor" href="#collapseArquivoEditorExcluir">
					<span class="glyphicon glyphicon-trash"></span>
					Excluir arquivo
				</a>
			</h4>
		</div>
		<div id="collapseArquivoEditorExcluir" class="panel-collapse collapse">
			<div class="panel-body">
				<?php
					echo $this->Html->link('<span class="glyphicon glyphicon-trash"></span> Excluir este arquivo', '#', array('class' => 'btn btn-danger btn-sm excluir-arquivo', 'escape' => false));
				?>
			</div>
		</div>
	</div>
</div>