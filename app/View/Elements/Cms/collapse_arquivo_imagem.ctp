<div class="collapse-ctrl arquivo_imagem panel-group" id="collapse-parent-arquivo_imagem">

	<!-- VISUALIZAR IMAGEM -->
	<div class="panel panel-default">
		<div class="panel-heading">
			<h4 class="panel-title">
				<a data-toggle="collapse" data-parent="#collapse-parent-arquivo_imagem" href="#collapseArquivoImagemVisualizar">
					<span class="glyphicon glyphicon-picture"></span>
					Visualizar
				</a>
			</h4>
		</div>
		<div id="collapseArquivoImagemVisualizar" class="panel-collapse in">
			<div class="panel-body text-center">
				<div class="visualizador-imagem">

				</div>
				<div class="alert alert-danger visualizador-nao-pode-exibir hide alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<strong>ERRO AO EXIBIR IMAGEM: </strong> Não é possível exibir a imagem selecionada pois ela não está disponível via web. Todas as imagens que serão exibidas nos navegadores dos clientes devem dentro de "webroot/img".
				</div>
			</div>
		</div>
	</div>

	<!-- EXCLUIR ARQUIVO -->
	<div class="panel panel-default">
		<div class="panel-heading">
			<h4 class="panel-title">
				<a class="collapsed" data-toggle="collapse" data-parent="#collapse-parent-arquivo_imagem" href="#collapseArquivoImagemExcluir">
					<span class="glyphicon glyphicon-trash"></span>
					Excluir arquivo
				</a>
			</h4>
		</div>
		<div id="collapseArquivoImagemExcluir" class="panel-collapse collapse">
			<div class="panel-body">
				<?php
					echo $this->Html->link('<span class="glyphicon glyphicon-trash"></span> Excluir este arquivo', '#', array('class' => 'btn btn-danger btn-sm excluir-arquivo', 'escape' => false));
				?>
			</div>
		</div>
	</div>
</div>
