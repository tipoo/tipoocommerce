<?php
	$this->Html->script(array('Views/Promocoes/leve_x_pague_y', 'hogan-2.0.0.js', 'typeahead.min'), array('inline' => false));
	$this->Html->css(array('typeahead.js-bootstrap', 'Views/Promocoes/geral'), 'stylesheet', array('inline' => false));
	echo $this->Form->input('aplicado_a', array('type' => 'hidden', 'value' => 'P'));
?>
<ol class="breadcrumb">
	<li><?php echo $this->Html->link('Promoções', array('action' => 'backToPaginatorIndex', 'admin' => true)); ?></li>
	<li class="active">Promoção leve X e pague Y</li>
</ol>
<div class="page-header">
	<h2>Leve X e pague Y<br /><small>Comprando uma determinada quantidade do mesmo produto, o cliente paga apenas parte deles. Por exemplo: "Leve 3 e pague 2".</small></h2>
</div>

<div class="alert alert-danger hide" id="alert-datepicker">
	<span>Mensagem de erro: Intervalo entre datas incorreto.</span>
</div>

<div class="row">
	<div class="col-md-4">
		<?php
			echo $this->Form->input('descricao', array('label' => 'Descrição da Promoção*'));
			echo $this->Form->input('PromocaoCausa.carrinho_valor_acima_de', array('type' => 'hidden', 'value' => '0,00'));

		?>
	</div>
	<div class="col-md-2">
		<div class="input-append date form-group" id="dp1" data-date-format="dd/mm/yyyy">
			<label>Data início*</label>
			<div class="input-group">
				<span class="input-group-btn">
					<button class="btn btn-default add-on" type="button">
						<span class="glyphicon glyphicon-calendar"></span>
					</button>
				</span>
				<?php
					if (isset($this->request->data['Promocao'])) {
				?>
					<input name="data[Promocao][data_inicio]" id="dp1-mask" size="16" class="form-control" type="text" data-date="<?php echo $this->Formatacao->data(date('Y-m-d')); ?>" value="<?php echo $this->CustomTime->formatarData($this->request->data['Promocao']['data_inicio']); ?>">

				<?php
					} else {
				?>
						<input name="data[Promocao][data_inicio]" id="dp1-mask" size="16" class="form-control" type="text" data-date="<?php echo $this->Formatacao->data(date('Y-m-d')); ?>" >

				<?php
					}

				?>
			</div>
		</div>
	</div>
	<div class="col-md-2">
		<?php

			if (isset($this->request->data['Promocao'])) {
				echo $this->Form->input('hora_inicio', array('type' => 'text', 'label' => 'Hora de Início*', 'value' => $this->CustomTime->formatarHora($this->request->data['Promocao']['data_inicio'], true)));
			} else {
				echo $this->Form->input('hora_inicio', array('type' => 'text', 'label' => 'Hora de Início*'));
			}

		?>
	</div>
	<div class="col-md-2">
		<div class="input-append date form-group" id="dp2" data-date-format="dd/mm/yyyy">
			<label>Data Fim*</label>
			<div class="input-group">
				<span class="input-group-btn">
					<button class="btn btn-default add-on" type="button">
						<span class="glyphicon glyphicon-calendar"></span>
					</button>
				</span>
				<?php
				if (isset($this->request->data['Promocao'])) {
				?>
							<input name="data[Promocao][data_fim]" id="dp2-mask" size="16" class="form-control" type="text" value="<?php echo $this->CustomTime->formatarData($this->request->data['Promocao']['data_fim']); ?>">
					<?php
				} else {

				?>
					<input name="data[Promocao][data_fim]" id="dp2-mask" size="16" class="form-control" type="text" data-date="<?php echo $this->Formatacao->data(date('Y-m-d')); ?>" >
				<?php
				}

				?>
			</div>
		</div>
	</div>
	<div class="col-md-2">
		<?php

		if (isset($this->request->data['Promocao'])) {
			echo $this->Form->input('hora_fim', array('type' => 'text', 'label' => 'Hora de Fim*', 'value' => $this->CustomTime->formatarHora($this->request->data['Promocao']['data_fim'], true)));
		} else {
			echo $this->Form->input('hora_fim', array('type' => 'text', 'label' => 'Hora de Fim*'));
		}

		?>
	</div>
</div>

<hr>

<div class="row">
                          	<!-- Causa -->
	<div class="col-md-6">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Causas</h3>
			</div>
			<ul class="list-group">
				<li class="list-group-item">
					<h5 class="form-titulos-admin">Quantidade de produtos comprada pelo cliente</h5>
					<div class="row">
						<div class="col-md-3">
							<?php
								echo $this->Form->input('PromocaoCausa.qtd_pecas', array('type' => 'text', 'label' => 'Leve*'));
							?>
						</div>
					</div>
				</li>
				<li class="list-group-item">
					<h5 class="form-titulos-admin">Faixa de Cep</h5>
					<p class="text-muted">Para definir uma faixa de cep para todo o território nacional use cep de início igual a 00000-000 e cep de fim igual a 99999-999.</p>
					<div class="row">
						<div class="col-md-6">
							<?php
								echo $this->Form->input('PromocaoCausa.cep_inicio', array('type' => 'text', 'label' => 'Cep de Início'));
							?>
						</div>
						<div class="col-md-6">
							<?php
								echo $this->Form->input('PromocaoCausa.cep_fim', array('type' => 'text', 'label' => 'Cep de Fim'));
							?>
						</div>
					</div>
				</li>
				<li class="list-group-item">
					<h5 class="form-titulos-admin"> Valor do Produto</h5>
					<div class="row">
						<div class="col-md-4">
							<?php
								echo $this->Form->input('PromocaoCausa.valor_acima_de', array('type' => 'text', 'label' => 'Acima de'));
							?>
						</div>
					</div>
				</li>
				<li class="list-group-item">
					<h5 class="form-titulos-admin"> Aplicada à</h5>
					<p class="text-muted">
						A promoção pode ser aplicada a um produto específico, à uma coleção, à uma categoria ou à uma marca. 
					</p>
					<p class="text-muted">
						Dica: Para aplicar a promoção a 2 ou mais produtos crie uma colação, adicione os produtos desejado à coleção e aplique a promoção a esta coleção.
					</p>
					<!-- Nav tabs -->


					<?php

						$produto = '';
						$colecao = '';
						$categoria = '';
						$marca = '';

						$produto_valor = '';
						$colecao_valor = '';
						$categoria_valor = '';
						$marca_valor = '';

						if (isset($this->request->data['PromocaoCausa'])) {

							$produto = 'active';

							if (isset($this->request->data['PromocaoCausa']['produto_id'])) {
								$produto = 'active';
								$produto_valor = $produto_descricao['Produto']['descricao'] . ' - ' . '#' . $produto_descricao['Produto']['id']  ;
							} else
							if (isset($this->request->data['PromocaoCausa']['colecao_id'])) {
								$colecao = 'active';
								$produto = '';
								$colecao_valor = $colecao_descricao['Colecao']['descricao'] . ' - ' . '#' . $colecao_descricao['Colecao']['id']  ;
							} else
							if (isset($this->request->data['PromocaoCausa']['categoria_id'])) {
								$categoria = 'active';
								$produto = '';
								$categoria_valor =  $categoria_descricao['Categoria']['descricao'] . ' - ' . '#' . $categoria_descricao['Categoria']['id']  ;
							} else
							if (isset($this->request->data['PromocaoCausa']['marca_id'])) {
								$marca = 'active';
								$produto = '';
								$marca_valor = $marca_descricao['Marca']['descricao'] . ' - ' . '#' . $marca_descricao['Marca']['id']  ;
							}

						 } else {
						 	$produto = 'active';
						 }
					 ?>

					<ul class="nav nav-tabs" id="opcao">
						<li class="<?php echo $produto ?>"><a href="#produto" data-toggle="tab">Produto</a></li>
						<li class="<?php echo $colecao ?>"><a href="#colecao" data-toggle="tab">Coleção</a></li>
						<li class="<?php echo $categoria ?>"><a href="#categoria" data-toggle="tab">Categoria</a></li>
						<li class="<?php echo $marca ?>"><a href="#marca" data-toggle="tab">Marca</a></li>
					</ul>
					<br />

					<!-- Tab panes -->
					<div class="tab-content">
					  	<div class="tab-pane <?php echo $produto ?>" id="produto">
							<div class="row">
								<div class="col-md-12">
									<?php
										echo $this->Form->input('produto', array('row' => true, 'div' => 'col-md-12', 'class' => 'typeahead produto', 'label' => 'Localizar produto', 'placeholder' => 'Digite o nome do produto', 'autocomplete' => 'off', 'value' => $produto_valor));
										echo $this->Form->input('PromocaoCausa.produto_id', array('type' => 'hidden', 'class' => 'produto', 'data-produto' => $produto_valor));
									?>
								</div>
							</div>
					  	</div>

					<div class="tab-pane <?php echo $colecao ?>" id="colecao">
						<div class="row">
							<div class="col-md-12">
								<?php
									echo $this->Form->input('colecao', array('row' => true, 'div' => 'col-md-12', 'class' => 'typeahead colecao', 'label' => 'Localizar coleção', 'placeholder' => 'Digite o nome da coleção', 'autocomplete' => 'off', 'value' => $colecao_valor));
									echo $this->Form->input('PromocaoCausa.colecao_id', array('type' => 'hidden', 'class' => 'colecao', 'data-colecao' => $colecao_valor));
								?>
							</div>
						</div>
					</div>

					<div class="tab-pane <?php echo $categoria ?>" id="categoria">
						<div class="row">
							<div class="col-md-12">
								<?php
									echo $this->Form->input('categoria', array('row' => true, 'div' => 'col-md-12', 'class' => 'typeahead categoria', 'label' => 'Localizar categoria', 'placeholder' => 'Digite o nome da categoria', 'autocomplete' => 'off', 'value' => $categoria_valor));
									echo $this->Form->input('PromocaoCausa.categoria_id', array('type' => 'hidden', 'class' => 'categoria','data-categoria' => $categoria_valor));
								?>
							</div>
						</div>
					</div>

					<div class="tab-pane <?php echo $marca ?>" id="marca">
						<div class="row">
							<div class="col-md-12">
								<?php
									echo $this->Form->input('marca', array('row' => true, 'div' => 'col-md-12', 'class' => 'typeahead marca', 'label' => 'Localizar marca', 'placeholder' => 'Digite o nome da marca', 'autocomplete' => 'off', 'value' => $marca_valor));
									echo $this->Form->input('PromocaoCausa.marca_id', array('type' => 'hidden', 'class' => 'marca', 'data-marca' => $marca_valor));
								?>
							</div>
						</div>
				  	</div>
				</div>

				</li>
				<li class="list-group-item">
					<h5 class="form-titulos-admin">Utm`s</h5>
					<p class="text-muted">As promoções por utm`s são aquelas onde o cliente acessa através de um link (url) específico. Normalmente elas são para dar descontos para clientes que vêm do e-mails marketing ou de sites de parceiros.</p>
					<p class="text-muted">Ex. http://<?php echo $_SERVER['SERVER_NAME'] ?>?utm_source=parceiro-de-disparo&utm_medium=e-mail&utm_campaign=campanha-de-natal</p>
					<div class="row">
						<div class="col-md-4">
							<?php
								echo $this->Form->input('PromocaoCausa.utm_source', array('type' => 'text', 'label' => 'utm_source'));
							?>
						</div>
						<div class="col-md-4">
							<?php
								echo $this->Form->input('PromocaoCausa.utm_medium', array('type' => 'text', 'label' => 'utm_medium'));
							?>
						</div>
						<div class="col-md-4">
							<?php
								echo $this->Form->input('PromocaoCausa.utm_campaign', array('type' => 'text', 'label' => 'utm_campaign'));
							?>
						</div>
					</div>
				</li>
			</ul>
		</div>
	</div>
	<!-- .end Causa -->


	<div class="col-md-6">
		<!-- Efeito -->
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Efeito</h3>
			</div>
			<div class="panel-body">
				<?php
					echo $this->Form->input('PromocaoEfeito.desconto_produto', array('type' => 'hidden', 'value' => true));
				?>
				<h5 class="form-titulos-admin">Quantidade de produtos que o cliente vai pagar</h5>
				<div class="row">
					<div class="col-md-3">
						<?php
							echo $this->Form->input('PromocaoEfeito.tipo_valor', array('type' => 'hidden', 'value' => 'U'));

							if (isset($this->request->data['Promocao'])) {
								echo $this->Form->input('PromocaoEfeito.valor', array('type' => 'text', 'label' => 'Pague*', 'value' => intval($this->request->data['PromocaoEfeito']['valor'])));
							} else {
								echo $this->Form->input('PromocaoEfeito.valor', array('type' => 'text', 'label' => 'Pague*'));
							}
						?>
					</div>
				</div>
			</div>
		</div>
		<!-- .end Efeito -->

		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Selo</h3>
			</div>
			<div class="panel-body">
				<p class="text-muted">Cria um elemento no código fonte para ser usado pela sua agência para estilizar o selo.</p>
				<div class="row">
					<div class="col-md-6">
						<?php
							echo $this->Form->input('selo', array('type' => 'checkbox', 'label' => 'Contém selo'));
						?>
					</div>
				</div>
			</div>
		</div>

	</div>

</div>