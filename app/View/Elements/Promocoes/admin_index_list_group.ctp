<?php
	$this->Html->script(array('Views/Promocoes/admin_index_list_group'),array('inline' =>false));
	$this->Html->css(array('Views/Promocoes/admin_index_list_group'), 'stylesheet', array('inline' => false));
?>
<div class="list-group">
	<div class="list-group-item">
		<h4 class="list-group-item-heading"><?php echo $titulo ?></h4>
		<p class="list-group-item-text">
			<div class="row">
				<div class="col-sm-9">
					<?php echo $descricao ?>
				</div>
				<div class="col-sm-3">
					<a href="<?php echo Router::url('promocao/element:'.$element) ?>" class="btn btn-xs btn-primary pull-right">
						<span class="glyphicon glyphicon-plus"></span>
						Criar
					</a>
				</div>
			</div>
		</p>
	</div>

	<?php
		foreach ($promocoes[$element] as $promocao) {
	?>
			<div class="list-group-item">
				<span>#<?php echo $promocao['Promocao']['id'] ?> - <?php echo $promocao['Promocao']['descricao'] ?></span>
				<div class="btn-group pull-right">
					<button type="button" class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
						<span class="caret"></span>
					</button>
					<ul class="dropdown-menu pull-right text-left" role="menu">
						<li><?php echo $this->Html->link('<span class="glyphicon glyphicon-edit"></span> Editar', array('action' => 'promocao', $promocao['Promocao']['id'], 'element' => $element), array('escape' => false)); ?></li>
						<li><?php echo $this->Form->postLink('<span class="glyphicon glyphicon-trash "></span> Excluir', array('action' => 'excluir', $promocao['Promocao']['id']), array('escape' => false), 'Deseja realmente excluir a Promoção #' . $promocao['Promocao']['id'] . '?'); ?></li>
						<li><?php echo $this->Html->link('<span class="glyphicon "></span> Visualizar', array('action' => 'admin_ajax_visualizar', $promocao['Promocao']['id']), array('class' => 'ver-detalhes-promocao' ,'escape' => false)); ?></li>

						<?php
							if ($promocao['Promocao']['situacao'] == 'A') {

						?>
								<li><?php echo $this->Form->postLink('<span class="glyphicon "></span> Desativar', array('action' => 'desativar', $promocao['Promocao']['id']), array('escape' => false), 'Deseja realmente desativar a Promoção #' . $promocao['Promocao']['id'] . '?'); ?></li>
						<?php
							} else {
						?>
						<li><?php echo $this->Form->postLink('<span class="glyphicon "></span> Ativar', array('action' => 'ativar', $promocao['Promocao']['id']), array('escape' => false), 'Deseja realmente ativar a Promoção #' . $promocao['Promocao']['id'] . '?'); ?></li>

						<?php
							}

						?>
					</ul>
				</div>
				<br>
				<p>
					<?php

						$promocao_inicio = $promocao['Promocao']['data_inicio'] <= date('Y-m-d H:i:s') ? true : false;
						$promocao_fim = $promocao['Promocao']['data_fim'] >= date('Y-m-d H:i:s') ? true : false;

						$ultimo_dia = date("d/m/Y", strtotime("-1 day",strtotime($promocao['Promocao']['data_fim'])));
						$ultimo_dia_promocao = $ultimo_dia == $this->Formatacao->data(date('Y-m-d')) ? true : false;

						$inicio = $this->Formatacao->dataHora($promocao['Promocao']['data_inicio']);
						$fim = $this->Formatacao->dataHora($promocao['Promocao']['data_fim']);

						$ultimo_dia_tooltips = '';

						if ($promocao_inicio && $promocao_fim) {

							if($ultimo_dia_promocao){
								$text_color = 'label-warning';
								$legenda = 'Em funcionamento';
								$ultimo_dia_tooltips ='<br><br>Último dia da promoção.<br><br>';

							} else {
								$text_color = 'label-success';
								$legenda = 'Em funcionamento';
							}

						} else if (!$promocao_inicio) {
							$text_color = 'label-info';
							$legenda = 'Aguardando data de início';
						} else if (!$promocao_fim) {
							$text_color = 'label-danger';
							$legenda = 'Promoção finalizada';
						}

						if ($promocao['Promocao']['situacao'] != 'I') {

					?>

							<span class="label <?php echo $text_color ?>" data-placement="top" data-html="true" data-original-title="<div><b>Início: </b><?php echo $inicio; ?>  <br><b>Fim: </b> <?php echo $fim; ?> <?php echo $ultimo_dia_tooltips; ?></div>" >
								<?php echo $legenda; ?>
							</span>
							&nbsp;

					<?php

						}

					 ?>
					<span>
						<?php
							if ($promocao['Promocao']['situacao'] == 'I') {
								$situacao = '<span class="label label-danger">Inativo</span>';
								echo $situacao;
							}
						?>
					</span>
				</p>
			</div>
	<?php
		}
	?>
</div>