<ul class="menu-painel-controle">
	<li><?php echo $this->Html->link('Início', array('controller' => 'clientes', 'action' => 'painel')); ?></li>
	<li><?php echo $this->Html->link('Meus pedidos', array('controller' => 'clientes', 'action' => 'meus_pedidos'), array('class' => 'bt-meus-pedidos')); ?></li>
	<li><?php echo $this->Html->link('Meus dados', array('controller' => 'clientes', 'action' => 'meus_dados'), array('class' => 'bt-meus-dados')); ?></li>
	<li><?php echo $this->Html->link('Meus endereços', array('controller' => 'clientes', 'action' => 'meus_enderecos'), array('class' => 'bt-meus-enderecos')); ?></li>
	<li><?php echo $this->Html->link('Sair', array('controller' => 'clientes', 'action' => 'logout'), array('class' => 'bt-sair')); ?></li>
</ul>