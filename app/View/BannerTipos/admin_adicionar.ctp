<?php
	$this->Html->script(array('Views/BannerTipos/admin_adicionar', 'jquery.numeric'), array('inline' => false));
?>

<ol class="breadcrumb">
	<li><?php echo $this->Html->link('Gestão de Banners', array('action' => 'backToPaginatorIndex', 'admin' => true)); ?></li>
	<li class="active">Adicionar</li>
</ol>

<?php
	echo $this->Form->create('BannerTipo');
	echo $this->Form->input('descricao', array('row' => true, 'div' => 'col-md-4','type' => 'text', 'label' => 'Descrição*'));
	echo $this->Form->input('chave', array('row' => true, 'div' => 'col-md-4','type' => 'text', 'label' => 'Chave*'));
	echo $this->Form->input('qtd', array('row' => true, 'div' => 'col-md-1','type' => 'text', 'label' => 'Quantidade'));
	echo $this->Form->input('tipo', array('row' => true, 'div' => 'col-md-4','type' => 'select','options' => $tipo, 'empty' => 'Selecione', 'label' => 'Página*'));
?>

	<div class="row" id="Pagina" style="display:none;">
		<div class="col-md-4 form-group">
			<label class="control-label" for=""></label>
			<div class="select">
			</div>
		</div>
	</div>
<?php
	echo $this->Form->actions();
	echo $this->Form->end();
?>
