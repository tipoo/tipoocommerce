<ol class="breadcrumb">
	<li class="active">Gestão de Banners</li>
</ol>

<div class="table-responsive">
	<table class="table table-striped table-hover">
		<thead>
			<tr>
				<th class="col-md-1 text-center">#</th>
				<th class="col-md-3">Descrição</th>
				<th class="col-md-1">Qtd</th>
				<th class="col-md-1">Chave</th>
				<th class="col-md-2">Página</th>
				<th class="col-md-2">Categoria</th>
				<th class="col-md-1">Marca</th>
				<th class="text-center">
					<?php
						$urlBannerTipoAdicionar = array('controller' => 'bannerTipos', 'action' => 'adicionar', 'admin' => true, 'plugin' => null);
						if ($this->Permissoes->check($urlBannerTipoAdicionar)) {
							echo $this->Html->link('<span class="glyphicon glyphicon-plus"></span>', array('action' => 'adicionar'), array('class' => 'btn btn-sm btn-primary', 'escape' => false,'data-placement' => 'left', 'rel' => 'tooltip', 'title' => 'Adicionar Novo'));
						}
					?>
				</th>
			</tr>
		</thead>

		<tbody>
			<?php
				foreach ($banners as $banner) {
			?>
				<tr>
					<td class="text-center"><?php echo $banner['BannerTipo']['id'] ?></td>
					<td><?php echo $banner['BannerTipo']['descricao'] ?></td>
					<td><?php echo $banner['BannerTipo']['qtd'] ?></td>
					<td><?php echo $banner['BannerTipo']['chave'] ?></td>
					<td><?php echo $tipo[$banner['BannerTipo']['tipo']] ?></td>
					<td>
						<?php
							if ($banner['BannerTipo']['categoria_id'] != '') {
								echo $banner['Categoria']['descricao'];
							}
						?>
					</td>
					<td>
						<?php
							if ($banner['BannerTipo']['marca_id'] != '') {
								echo $banner['Marca']['descricao'];
							}
						?>
					</td>
					<td class="text-center">
						<div class="btn-group">
							<button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu pull-right text-left" role="menu">
								<?php
									$urlBannerTipoEditar = array('controller' => 'bannerTipos', 'action' => 'editar', 'admin' => true, 'plugin' => null);
									if ($this->Permissoes->check($urlBannerTipoAdicionar)) {
								?>
									<li> <?php echo $this->Html->link('<span class="glyphicon glyphicon-edit"></span> Editar', array('action' => 'editar', $banner['BannerTipo']['id']), array('escape' => false));?></li>
								<?php
									}

									$urlBannerTipoExcluir = array('controller' => 'bannerTipos', 'action' => 'excluir', 'admin' => true, 'plugin' => null);
									if ($this->Permissoes->check($urlBannerTipoAdicionar)) {
								?>
									<li> <?php echo $this->Form->postLink('<span class="glyphicon glyphicon-trash"></span> Excluir', array('action' => 'excluir', $banner['BannerTipo']['id']), array('escape' => false), 'Deseja realmente excluir o Tipo de Banner #' . $banner['BannerTipo']['id'] . '?');?></li>
								<?php
									}

									$urlBanners = array('controller' => 'banners', 'action' => 'index', 'admin' => true, 'plugin' => null);
									if ($this->Permissoes->check($urlBanners)) {
								?>
									<li> <?php echo $this->Html->link('<span class="glyphicon"></span>Banners', array('controller' => 'banners', 'action' => 'index', $banner['BannerTipo']['id']), array('escape' => false));?></li>
								<?php
									}
								?>
							</ul>
						</div>
					</td>
				</tr>
			<?php
				}
			?>
		</tbody>
	</table>
</div>

<?php echo $this->element('Paginacao/admin_paginacao'); ?>