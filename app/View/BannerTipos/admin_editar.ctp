<?php
	$this->Html->script(array('Views/BannerTipos/admin_editar', 'jquery.numeric'), array('inline' => false));
?>

<ol class="breadcrumb">
	<li><?php echo $this->Html->link('Gestão de Banners', array('action' => 'backToPaginatorIndex', 'admin' => true)); ?></li>
	<li class="active">Editar</li>
</ol>

<div class="alert alert-warning">
	<i class="glyphicon glyphicon-exclamation-sign"></i> <strong>Atenção!</strong> Para editar campos bloqueados, por favor entre em contato com o suporte técnico.
</div>

<?php
	echo $this->Form->create('BannerTipo');
	echo $this->Form->input('descricao', array('row' => true, 'div' => 'col-md-4','type' => 'text', 'label' => 'Descrição*'));
	echo $this->Form->input('chave', array('disabled' => true, 'placeholder' => $this->request->data['BannerTipo']['chave'], 'row' => true, 'div' => 'col-md-4','type' => 'text', 'label' => 'Chave'));
/*?>
	<div class="control-group">
		<label class="control-label" for="BannerTipoChave">Chave</label>
		<div class="controls">
			<p class="no-edit"><strong><?php echo $this->request->data['BannerTipo']['chave']; ?></strong></p>
		</div>
	</div>
<?php*/
	echo $this->Form->input('qtd', array('row' => true, 'div' => 'col-md-1','type' => 'text', 'label' => 'Quantidade'));
	echo $this->Form->input('tipo', array('disabled' => true, 'placeholder' => $tipo[$this->request->data['BannerTipo']['tipo']], 'row' => true, 'div' => 'col-md-4','type' => 'text', 'label' => 'Página'));
?>
<!-- 	<div class="control-group">
		<label class="control-label" for="BannerTipoTipo">Página</label>
		<div class="controls">
			<p class="no-edit"><strong><?php//echo $tipo[$this->request->data['BannerTipo']['tipo']]; ?></strong></p>
		</div>
	</div> -->
<?php
	if ($this->request->data['BannerTipo']['marca_id'] != '') {

		echo $this->Form->input('Marca.descricao', array('id' => 'MarcaId','disabled' => true, 'placeholder' => $this->request->data['Marca']['descricao'], 'row' => true, 'div' => 'col-md-4','type' => 'text', 'label' => 'Marca'));
?>
	<!-- 	<div class="control-group" id="MarcaId">
			<label class="control-label" for="">Marca</label>
			<div class="controls">
				<p class="no-edit"><strong><?php //echo $this->request->data['Marca']['descricao']; ?></strong></p>
			</div>
		</div> -->
<?php
	}

	if ($this->request->data['BannerTipo']['categoria_id'] != '') {

		echo $this->Form->input('Categoria.descricao', array('id' => 'CategoriaId','disabled' => true, 'placeholder' => $this->request->data['Categoria']['descricao'], 'row' => true, 'div' => 'col-md-4','type' => 'text', 'label' => 'Categoria'));
?>
<!-- 		<div class="control-group" id="CategoriaId">
			<label class="control-label" for="">Categoria</label>
			<div class="controls">
				<p class="no-edit"><strong><?php //echo $this->request->data['Categoria']['descricao']; ?><strong></p>
			</div>
		</div> -->
<?php
	}
	echo $this->Form->actions();
	echo $this->Form->end();
?>
