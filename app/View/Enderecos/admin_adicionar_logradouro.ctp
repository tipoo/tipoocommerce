<?php
	$this->Html->script(array('Views/Enderecos/admin_adicionar_logradouro'), array('inline' => false));
?>

<ol class="breadcrumb">
	<li><?php echo $this->Html->link('Logradouros', array('controller' => 'enderecos','action' => 'logradouro', 'admin' => true)); ?></li>
	<li class="active">Adicionar</li>
</ol>
<?php
	echo $this->Form->create('Logradouro');
	echo $this->Form->input('cep', array('row' => true, 'div' => 'col-md-3', 'label' => 'Cep', 'readonly' => 'readonly', 'placeholder' => $cep));
	echo $this->Form->input('estado_id', array('row' => true, 'div' => 'col-md-3','options' => $estados, 'label' => 'Estado*', 'empty' => 'Selecione'));
	echo $this->Form->input('cidade_id', array('row' => true, 'div' => 'col-md-3','options' => 'Selecione', 'label' => 'Cidade*', 'empty' => 'Selecione'));
	echo $this->Form->input('bairro_id', array('row' => true, 'div' => 'col-md-3','options' => 'Selecione', 'label' => 'Bairro*', 'empty' => 'Selecione'));
	echo $this->Form->input('nomeclog', array('row' => true, 'div' => 'col-md-3', 'label' => 'Endereço*', 'placeholder' => 'ex. Rua Santa Maria'));
	echo $this->Form->actions();
	echo $this->Form->end();
?>
