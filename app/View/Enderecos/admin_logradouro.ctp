<?php
	$this->Html->script(array('Views/Enderecos/admin_logradouro'), array('inline' => false));
?>

<ol class="breadcrumb">
	<li class="active">Logradouros</li>
</ol>

<div class="row">
	<div class="col-md-11 col-lg-12">
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="row">
					<div class="col-md-3 campo-buscar-cliente">
						<div class="input-group">
							<?php echo $this->Form->input('Filtro.cep', array('id' => 'FiltrarCepLogradouros','type' => 'text', 'placeholder' =>'Busca por CEP' ,'label' => false,'div' => false)); ?>
							<span class="input-group-btn">
								<?php echo $this->Form->button('<span class="glyphicon glyphicon-search"></span>', array('id' =>'FiltrarLogradouros','class' => 'btn btn-default buscar-cep', 'type' => 'button')); ?>
							</span>
						</div>
					</div>
				</div>
				<small><a id="nao-sei-meu-cep" href="http://www.buscacep.correios.com.br/" target="_blank"> Buscar CEP no site dos Correios</a></small>
			</div>
		</div>
	</div>
</div>

<?php
	if (count($logradouros) > 0) {
?>
		<div class="table-responsive">
			<table class="table table-striped table-hover">
				<thead>
					<tr>
						<th class="col-md-1 text-center">#</th>
						<th class="col-md-4">Logradouro</th>
						<th class="col-md-3">Bairro</th>
						<th class="col-md-3">Cidade</th>
						<th class="col-md-1">UF</th>
					</tr>
				</thead>

				<tbody>
					<?php
						foreach ($logradouros as $logradouro) {
					?>
						<tr>
							<td class="text-center"><?php echo $logradouro['Logradouro']['id'] ?></td>
							<td><?php echo $logradouro['Logradouro']['nomeclog'] ?></td>
							<td><?php echo $logradouro['Bairro']['nome'] ?></td>
							<td><?php echo $logradouro['Cidade']['nome'] ?></td>
							<td><?php echo $logradouro['Bairro']['uf'] ?></td>
						</tr>
					<?php
						}
					?>
				</tbody>
			</table>
		</div>
		<?php echo $this->element('Paginacao/admin_paginacao'); ?>
<?php
	} else {
		if($cep != '') {
?>
			<div class="well ">
				<h4>Cep não encontrado</h4>
				<?php echo $this->Html->link('<i class=" icon-plus icon-white"></i> Cadastrar CEP', array('action' => 'adicionar_logradouro',$cep), array('class' => 'btn btn-primary btn-small', 'escape' => false)); ?>
			</div>
<?php
		}
	 }
 ?>

