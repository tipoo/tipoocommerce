<?php
	$this->Html->script(array('Views/Colecoes/admin_index'), array('inline' => false));
?>

<ol class="breadcrumb">
	<li class="active">Coleções</li>
</ol>


<div class="table-responsive">
	<table class="table table-striped table-hover">
		<thead>
			<tr>
				<th class="col-md-1 text-center">#</th>
				<th class="col-md-5">Descrição</th>
				<th class="col-md-5">Usado em (Vitrine)</th>
				<th class="text-center"><?php echo $this->Html->link('<span class="glyphicon glyphicon-plus"></span>', array('action' => 'adicionar'), array('class' => 'btn btn-sm btn-primary', 'escape' => false,'data-placement' => 'left', 'rel' => 'tooltip', 'title' => 'Adicionar Novo')); ?></th>
			</tr>
		</thead>

		<tbody>
			<?php
				foreach ($colecoes as $colecao) {

			?>
				<tr>
					<td class="text-center"><?php echo $colecao['Colecao']['id'] ?></td>
					<td><?php echo $colecao['Colecao']['descricao'] ?></td>
					<td>
						<ul class="list-unstyled">
						<?php
							foreach ($colecoes_local as  $vitrine) {

								if ($vitrine['ColecoesLocal']['colecao_id'] == $colecao['Colecao']['id'] ) {
						?>
									<li><?php echo $vitrine['ColecoesLocal']['descricao']; ?></li>
						<?php
								}

							}

						?>
						</ul>
					</td>
					<td class="text-center">
						<div class="btn-group">
							<button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu pull-right text-left" role="menu">
								<?php
									if (!$colecao['Colecao']['sistema']) {
								?>
										<li><?php echo $this->Html->link('<span class="glyphicon glyphicon-edit"></span> Editar', array('action' => 'editar', $colecao['Colecao']['id']), array('escape' => false));?></li>
										<li><?php echo $this->Form->postLink('<span class="glyphicon glyphicon-trash"></span> Excluir', array('action' => 'excluir', $colecao['Colecao']['id']), array('escape' => false), 'Deseja realmente excluir o Colecao #' . $colecao['Colecao']['id'] . '?');?></li>
								<?php
									} else {

								?>
										<li class="disabled"><a href="javascript: void(0)" data-placement="left" rel="tooltip" title="" data-original-title="Não é possível editar esta Coleção, pois ela está em uso."><span class="glyphicon glyphicon-edit"></span> Editar</a></li>
										<li class="disabled"><a href="javascript: void(0)" data-placement="left" rel="tooltip" title="" data-original-title="Não é possível excluir esta Coleção, pois ela está em uso."><span class="glyphicon glyphicon-trash"></span> Excluir</a></li>
								<?php
									}
								?>
								<li><?php echo $this->Html->link('<span class="glyphicon"></span>Produtos', array('controller' => 'colecoes', 'action' => 'produtos', $colecao['Colecao']['id']), array('escape' => false)); ?></li>
							</ul>
						</div>
					</td>
				</tr>
			<?php
				}
			?>
		</tbody>
	</table>
</div>

<?php echo $this->element('Paginacao/admin_paginacao'); ?>