<?php
	$this->Html->script(array('hogan-2.0.0.js','typeahead.min', 'Views/Colecoes/admin_produtos'), array('inline' => false));
	$this->Html->css(array('typeahead.js-bootstrap'), 'stylesheet', array('inline' => false));
?>

<ol class="breadcrumb">
	<li><?php echo $this->Html->link('Coleções', array('action' => 'backToPaginatorIndex', 'admin' => true)); ?></li>
	<li class="active">Produtos - <strong><?php echo $colecao['Colecao']['descricao'] ?></strong></li>
</ol>
<div class="alert alert-info">
	<i class="glyphicon glyphicon-info-sign"></i> <strong>ORDENAÇÃO: </strong> Arraste para ordenar os produtos.
</div>
<?php
	echo $this->Form->create('ColecoesProduto');
	echo $this->Form->input('colecao_id', array('type' => 'hidden'));
	echo $this->Form->input('produto_id', array('type' => 'hidden'));
	echo $this->Form->input('descricao', array('row' => true, 'div' => 'col-md-offset-2 col-md-8', 'class' => 'typeahead', 'autocomplete' => 'off', 'label' => 'Adicionar produto', 'placeholder' => 'Digite o nome do produto'));
	echo $this->Form->end();
?>

<?php
	if ($produtos) {
?>
	<div class="table-responsive">
		<table class="table table-striped table-hover">
			<thead>
				<tr>

					<th class="col-xs-8 col-sm-8 col-md-10">Descrição</th>
					<th class="visible-xs visible-sm"></th>
					<th class="visible-xs visible-sm"></th>
					<th class="col-md-1 text-center"> Acão</th>
				</tr>
			<thead>
			<tbody id="sortable">
				<?php
					foreach ($produtos as $produto) {
				?>
					<tr class="ui-state-default " data-id="<?php echo $produto['ColecoesProduto']['id']; ?>">
						<td><?php echo $produto['Produto']['descricao'] ?> - <?php echo $produto['Produto']['Marca']['descricao'] ?> - # <?php echo $produto['Produto']['id'] ?> </td>
						<td class="visible-xs visible-sm text-center seta-ordenacao" >
							<div class="ordenacao-desce " data-indice="<?php echo $produto['ColecoesProduto']['id']; ?>"><span class="glyphicon glyphicon-arrow-down"></span></div>
						</td>
						<td class="visible-xs visible-sm text-center seta-ordenacao" >
							<div class="ordenacao-sobe " data-indice="<?php echo $produto['ColecoesProduto']['id']; ?>"><span class="glyphicon glyphicon-arrow-up"></span></div>
						</td>
						<td class="text-center">
							<div class="btn-group">
								<button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu pull-right text-left" role="menu">
									<li><?php echo $this->Form->postLink('<span class="glyphicon glyphicon-trash"></span> Excluir', array('action' => 'excluir_produto', $produto['Colecao']['id'], $produto['ColecoesProduto']['id']), array('class' => 'exclui-colecao', 'escape' => false), 'Deseja realmente remover este produto da Coleção'); ?></li>
								</ul>
							</div>
						</td>
					</tr>
				<?php
					}
				?>
			</tbody>
		</table>
	</div>
<?php
	}
?>