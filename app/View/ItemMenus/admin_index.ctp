<?php
	$this->Html->script(array('Views/ItemMenus/admin_index'), array('inline' => false));
?>

<?php
	if (isset($id)) {
?>
	<ol class="breadcrumb">
		<li ><?php echo $this->Html->link('Itens do menu', array('action' => 'index', $menu_id, 'admin' => true)); ?></li>
		<li class="active"><span>Sub-Itens Menu</span></li>
	</ol>

<?php
	} else {

?>
	<ol class="breadcrumb">
		<li ><?php echo $this->Html->link('Menus', array('controller' => 'menus', 'action' => 'index', 'admin' => true)); ?></li>
		<li class="active"><span>Itens do menu</span></li>
	</ol>

<?php

	}
 ?>



 <?php 
 	echo $this->Form->input('menu_id', array('type' => 'hidden', 'value' => $menu_id)); 

 	if (isset($id)) {
 		echo $this->Form->input('item_pai_id', array('type' => 'hidden', 'value' => $id));
 	} else {
 		echo $this->Form->input('item_pai_id', array('type' => 'hidden', 'value' => ''));
 	}
?>

<div class="alert alert-info">
	<i class="glyphicon glyphicon-info-sign"></i> <strong>ORDENAÇÃO: </strong> Arraste para ordenar os itens de menu.
</div>

<div class="table-responsive">
	<table class="table table-striped table-hover">
		<thead>
			<tr>
				<th class="col-md-1 text-center">#</th>
				<th class="col-xs-3 col-sm-3 col-md-5">Item do menu</th>
				<th class="col-xs-3 col-sm-3 col-md-5">Link</th>
				<th class="visible-xs visible-sm"></th>
				<th class="visible-xs visible-sm"></th>
				<th class="text-right">
					<?php echo $this->Html->link('<span class="glyphicon glyphicon-plus"></span>', array('action' => 'adicionar', $menu_id, $id), array('class' => 'btn btn-sm btn-primary', 'escape' => false,'data-placement' => 'bottom', 'rel' => 'tooltip', 'title' => 'Adicionar Novo')); ?>
				</th>
			</tr>
		</thead>

		<tbody id="sortable">
			<?php
				foreach ($item_menus as $item_menu) {
			?>
				<tr class="ui-state-default" data-id="<?php echo $item_menu['ItemMenu']['id']; ?>">
					<td class="text-center"><?php echo $item_menu['ItemMenu']['id'] ?></td>
					<td><?php echo $item_menu['ItemMenu']['nome'] ?>
						<br>
						<?php
							if ($item_menu['ItemMenu']['ativo']) {
								$status = '<span class="label label-success">Ativo</span> ';
							} else {
								$status = '<span class="label label-danger">Inativo</span>';
							}

							echo $status;
						?>
					</td>
					<td>
						
						<?php
							$nova_aba = '';
							if ($item_menu['ItemMenu']['nova_aba']) {
								$nova_aba = '_blank';
							} 
						?>
						<a href="<?php echo $item_menu['ItemMenu']['link']?>" target="<?php echo $nova_aba?>"><?php echo $item_menu['ItemMenu']['link']?></a>
					</td>

					<td class="visible-xs visible-sm text-center seta-ordenacao" >
						<div class="ordenacao-desce " data-indice="<?php echo $item_menu['ItemMenu']['id']; ?>"><span class="glyphicon glyphicon-arrow-down"></span></div>
					</td>
					<td class="visible-xs visible-sm text-center seta-ordenacao" >
						<div class="ordenacao-sobe " data-indice="<?php echo $item_menu['ItemMenu']['id']; ?>"><span class="glyphicon glyphicon-arrow-up"></span></div>
					</td>

					<td class="text-right">
						<div class="btn-group">
							<button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu pull-right text-left" role="menu">
								<li><?php echo $this->Html->link('<span class="glyphicon glyphicon-edit"></span> Editar', array('action' => 'editar', $item_menu['ItemMenu']['id']), array('escape' => false));?></li>
								<li><?php echo $this->Html->link('<span class="glyphicon "></span>Sub-Menus', array('action' => 'index', $menu_id, $item_menu['ItemMenu']['id']), array('escape' => false));?></li>
							 	<?php 
									if ($item_menu['ItemMenu']['ativo']) {
								?>
										<li><?php echo $this->Form->postLink('<span class="glyphicon"></span> Desativar', array('action' => 'excluir', $item_menu['ItemMenu']['id']), array('escape' => false));?></li>
								<?php

									} else {
								?>
										<li><?php echo $this->Form->postLink('<span class="glyphicon"></span> Ativar', array('action' => 'ativar', $item_menu['ItemMenu']['id']), array('escape' => false));?></li>

								<?php
									}
							 	 ?>

							</ul>
						</div>
					</td>
				</tr>
			<?php
				}
			?>
		</tbody>
	</table>
</div>

<?php echo $this->element('Paginacao/admin_paginacao'); ?>