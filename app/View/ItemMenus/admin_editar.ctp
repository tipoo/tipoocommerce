<?php
	$this->Html->script(array('Views/ItemMenus/admin_editar'), array('inline' => false));
?>

<ol class="breadcrumb">
	<li><?php echo $this->Html->link('Itens do Menu', array('action' => 'backToPaginatorIndex', 'admin' => true)); ?></li>
	<li class="active">Editar</li>
</ol>


<?php
	echo $this->Form->create('ItemMenu');
	echo $this->Form->input('nome', array('row' => true, 'div' => 'col-md-4','type' => 'text', 'label' => 'Nome*'));
	echo $this->Form->input('nova_aba', array('row' => true, 'div' => 'col-md-3', 'type' => 'checkbox', 'value' => false, 'label' => 'Abrir em uma nova aba'));
	echo $this->Form->input('ativo', array('row' => true, 'div' => 'col-md-2', 'type' => 'checkbox', 'label' => 'Ativo'));
	echo $this->Form->input('classe_css', array('row' => true, 'div' => 'col-md-4','type' => 'text', 'label' => 'Classe Css'));
	echo $this->Form->input('link', array('row' => true, 'div' => 'col-md-12','type' => 'text', 'label' => 'Link*'));
	echo $this->Form->actions();
	echo $this->Form->end();
?>