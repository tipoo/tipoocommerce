<ol class="breadcrumb">
	<li class="active">Promoções</li>
</ol>

<div class="row">

	<div class="col-sm-4">
		<h2 class="lead">Produto</h2>
		<div class="well">
			<?php
				echo $this->element('Promocoes/admin_index_list_group', array(
					'promocoes' => $promocoes,
					'element' => 'desconto_produto',
					'titulo' => 'Desconto no valor do produto',
					'descricao' => 'Ofereça um desconto percentual ou em reais para um produto, para os produtos de uma coleção, para os produtos de uma categoria ou para os produtos de uma marca.'
				));
			?>
			<br>
			<?php
				echo $this->element('Promocoes/admin_index_list_group', array(
					'promocoes' => $promocoes,
					'element' => 'leve_x_pague_y',
					'titulo' => 'Leve X e pague Y',
					'descricao' => 'Comprando uma determinada quantidade do mesmo produto, o cliente paga apenas parte deles. Por exemplo: "Leve 3 e pague 2".'
				));
			?>
		</div>
	</div>

	<div class="col-sm-4">
		<h2 class="lead">Frete</h2>
		<div class="well">
			<?php
				echo $this->element('Promocoes/admin_index_list_group', array(
					'promocoes' => $promocoes,
					'element' => 'desconto_frete',
					'titulo' => 'Desconto no valor do frete',
					'descricao' => 'Ofereça um desconto percentual, em reais ou até mesmo FRETE GRÁTIS para um produto, para os produtos de uma coleção, para os produtos de uma categoria ou para os produtos de uma marca. Você também pode ofercer as mesmas vantagens para o carrinho inteiro.'
				));
			?>
		</div>
	</div>

	<div class="col-sm-4">
		<h2 class="lead">Pedido</h2>
		<div class="well">
			<?php
				echo $this->element('Promocoes/admin_index_list_group', array(
					'promocoes' => $promocoes,
					'element' => 'desconto_pedido',
					'titulo' => 'Desconto no valor do pedido',
					'descricao' => 'Ofereça um desconto percentual ou em reais no pedido. Pode ser aplicado para um produto, para os produtos de uma coleção, para os produtos de uma categoria ou para os produtos de uma marca.'
				));
			?>
			<br>
			<?php
				echo $this->element('Promocoes/admin_index_list_group', array(
					'promocoes' => $promocoes,
					'element' => 'cupom_desconto',
					'titulo' => 'Cupom de desconto',
					'descricao' => 'Ofereça um cupom de desconto percentual ou em reais, para a compra.'
				));
			?>
		</div>
	</div>

</div>