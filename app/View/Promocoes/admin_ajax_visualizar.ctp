<p><strong>#<?php echo $promocao['Promocao']['id'] ?> </strong>  - <strong> Promoção</strong> <?php echo $promocao['Promocao']['descricao'] ?></p>

<?php
	$promocao_inicio = $promocao['Promocao']['data_inicio'] <= date('Y-m-d H:i:s') ? true : false;
	$promocao_fim = $promocao['Promocao']['data_fim'] >= date('Y-m-d H:i:s') ? true : false;

	$inicio = $this->Formatacao->dataHora($promocao['Promocao']['data_inicio']);
	$fim = $this->Formatacao->dataHora($promocao['Promocao']['data_fim']);

	if ($promocao_inicio && $promocao_fim) {
		$text_color = 'label label-success';
		$legenda = 'Em funcionamento';
	} else if (!$promocao_inicio) {
		$text_color = 'label label-info';
		$legenda = 'Aguardando data de início';
	} else if (!$promocao_fim) {
		$text_color = 'label label-danger';
		$legenda = 'Promoção finalizada';
	}
 ?>
<span class="<?php echo $text_color ?>"><?php echo $legenda; ?></span>
<hr>
<p><strong>Data de início : </strong><?php echo $inicio; ?></p>
<p><strong>Data de fim : </strong><?php echo $fim; ?></p>

<?php

	switch ($promocao['Promocao']['element']) {
		case 'desconto_produto':
			$aplicado_a = 'Produto';

			break;

		case 'desconto_frete':
			$aplicado_a = 'Frete';

			break;

		case 'leve_x_pague_y':
			$aplicado_a = 'Produto';

			break;

		case 'desconto_pedido':

			$aplicado_a = 'Pedido';

			break;

		case 'cupom_desconto':

			$aplicado_a = 'Pedido';

			break;

		default:

			break;

	}


	$situacao = 'Ativo';
	$text_color = 'label label-success';
	if ($promocao['Promocao']['situacao'] == 'I') {
		$text_color = 'label label-danger';
	 	$situacao = 'Inativo';
	}
 ?>
<p><strong>Aplicado à : </strong><?php echo $aplicado_a; ?></p>
<p><strong>Status : </strong><span class="<?php echo $text_color ?>"><?php echo $situacao; ?></span></p>
<hr>

<div class="well">
	<h3>Resumo</h3>
	<?php
		switch ($promocao['Promocao']['element']) {
			case 'desconto_produto':
	?>
				<span >Oferece um desconto percentual ou em reais para um produto, para os produtos de uma coleção, para os produtos de uma categoria ou para os produtos de uma marca.</span>
				<hr>
				<?php

					$valor = null;
					if ($promocao['PromocaoEfeito']['tipo_valor'] == 'M') {
						$valor = $this->Formatacao->moeda($promocao['PromocaoEfeito']['valor']);

					} else if ($promocao['PromocaoEfeito']['tipo_valor'] == 'P') {
						$valor = intval($promocao['PromocaoEfeito']['valor']) .' %';
					}
				?>
					<p><strong>Desconto : </strong><?php echo $valor?> </p>
				<?php

					if (isset($produto)) {
				?>
						<p><strong>Referente ao Produto: </strong> <?php echo $produto['Produto']['descricao'] ?> - #<?php echo $produto['Produto']['id'] ?> </p>

				<?php
					} else if (isset($colecao)){
				?>
						<p><strong>Referente a Colecao: </strong> <?php echo $colecao['Colecao']['descricao'] ?> - #<?php echo $colecao['Colecao']['id'] ?></p>

				<?php
					} else if (isset($categoria)){

				?>
						<p><strong>Referente a Categoria: </strong><?php echo $categoria['Categoria']['descricao'] ?> - #<?php echo $categoria['Categoria']['id'] ?></p>

				<?php
					} else if (isset($marca)){

				?>
						<p><strong>Referente a Marca: </strong><?php echo $marca['Marca']['descricao'] ?> - #<?php echo $marca['Marca']['id'] ?> </p>
				<?php

					} else {
				?>
						<small>Nenhum item cadastrado.</small>
		<?php
					}


				break;

			case 'desconto_frete':

				$valor = null;
				if ($promocao['PromocaoEfeito']['tipo_valor'] == 'M') {
					$valor = $this->Formatacao->moeda($promocao['PromocaoEfeito']['valor']);

				} else if ($promocao['PromocaoEfeito']['tipo_valor'] == 'P') {
					$valor = intval($promocao['PromocaoEfeito']['valor']) .' %';
				}
		?>
				<span>Oferece um desconto percentual, em reais ou até mesmo FRETE GRÁTIS para um produto, para os produtos de uma coleção, para os produtos de uma categoria ou para os produtos de uma marca.</span>
				<hr>
				<p><strong>Desconto no Frete : </strong><?php echo $valor?> </p>

		<?php

				break;

			case 'leve_x_pague_y':

		?>
				<span>Comprando uma determinada quantidade do mesmo produto, o cliente paga apenas parte deles.</span>
				<hr>
				<p><strong>Leve </strong><?php echo $promocao['PromocaoCausa']['qtd_pecas'] ?> e <strong>pague </strong><?php echo intval($promocao['PromocaoEfeito']['valor'])?> </p>
			<?php

				if (isset($produto)) {
			?>
					<p><strong>Referente ao Produto: </strong> <?php echo $produto['Produto']['descricao'] ?> - #<?php echo $produto['Produto']['id'] ?> </p>

			<?php
				} else if (isset($colecao)){
			?>
					<p><strong>Referente a Colecao: </strong> <?php echo $colecao['Colecao']['descricao'] ?> - #<?php echo $colecao['Colecao']['id'] ?></p>

			<?php
				} else if (isset($categoria)){

			?>
					<p><strong>Referente a Categoria: </strong><?php echo $categoria['Categoria']['descricao'] ?> - #<?php echo $categoria['Categoria']['id'] ?></p>

			<?php
				} else if (isset($marca)){

			?>
					<p><strong>Referente a Marca: </strong><?php echo $marca['Marca']['descricao'] ?> - #<?php echo $marca['Marca']['id'] ?> </p>
			<?php
				} else {
			?>
				<small>Nenhum item cadastrado.</small>

			<?php
				}

				break;

			case 'desconto_pedido':

			?>
				<span>Oferece um desconto percentual ou em reais, no valor do pedido.</span>
				<hr>
				<?php

					$valor = null;
					if ($promocao['PromocaoEfeito']['tipo_valor'] == 'M') {
						$valor = $this->Formatacao->moeda($promocao['PromocaoEfeito']['valor']);

					} else if ($promocao['PromocaoEfeito']['tipo_valor'] == 'P') {
						$valor = intval($promocao['PromocaoEfeito']['valor']) .' %';
					}
				?>
					<p><strong>Desconto : </strong><?php echo $valor?> </p>
				<?php

					if (isset($produto)) {
				?>
						<p><strong>Referente ao Produto: </strong> <?php echo $produto['Produto']['descricao'] ?> - #<?php echo $produto['Produto']['id'] ?> </p>

				<?php
					} else if (isset($colecao)){
				?>
						<p><strong>Referente a Colecao: </strong> <?php echo $colecao['Colecao']['descricao'] ?> - #<?php echo $colecao['Colecao']['id'] ?></p>

				<?php
					} else if (isset($categoria)){

				?>
						<p><strong>Referente a Categoria: </strong><?php echo $categoria['Categoria']['descricao'] ?> - #<?php echo $categoria['Categoria']['id'] ?></p>

				<?php
					} else if (isset($marca)){

				?>
						<p><strong>Referente a Marca: </strong><?php echo $marca['Marca']['descricao'] ?> - #<?php echo $marca['Marca']['id'] ?> </p>
				<?php

					} else {
				?>
						<small class="text-muted">Nenhum item cadastrado.</small>
	<?php
					}

				break;

				case 'cupom_desconto':
	?>
					<span>Oferece um cupom de desconto percentual ou em reais, no valor da compra.</span>
					<hr>
					<?php
						$valor = null;
						if ($promocao['PromocaoEfeito']['tipo_valor'] == 'M') {
							$valor = $this->Formatacao->moeda($promocao['PromocaoEfeito']['valor']);

						} else if ($promocao['PromocaoEfeito']['tipo_valor'] == 'P') {
							$valor = intval($promocao['PromocaoEfeito']['valor']) .' %';
						}
					?>
						<p><strong>Cód. do Cupom : </strong><?php echo $promocao['PromocaoCausa']['cupom']?> </p>
						<p><strong>Cupom Reutilizável: </strong><?php echo $promocao['PromocaoCausa']['cupom_reutilizavel'] ? '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>' : '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>'; ?> </p>
						<p><strong>Desconto : </strong><?php echo $valor?> </p>
					<?php
					break;

			default:
				# code...
				break;
		}
	?>
</div>

<div class="well">
		<h3>Promoção Causa</h3>
		<hr>
		<?php
			$promocao_causa = false;

			if (isset($produto)) {
				$promocao_causa = true;
		?>
				<p><strong>Referente ao Produto: </strong> <?php echo $produto['Produto']['descricao'] ?> - #<?php echo $produto['Produto']['id'] ?> </p>

		<?php
			} else if (isset($colecao)){
				$promocao_causa = true;
		?>
				<p><strong>Referente a Colecao: </strong> <?php echo $colecao['Colecao']['descricao'] ?> - #<?php echo $colecao['Colecao']['id'] ?></p>

		<?php
			} else if (isset($categoria)){
				$promocao_causa = true;

		?>
				<p><strong>Referente a Categoria: </strong><?php echo $categoria['Categoria']['descricao'] ?> - #<?php echo $categoria['Categoria']['id'] ?></p>

		<?php
			} else if (isset($marca)){
				$promocao_causa = true;

		?>
				<p><strong>Referente a Marca: </strong><?php echo $marca['Marca']['descricao'] ?> - #<?php echo $marca['Marca']['id'] ?> </p>
		<?php

			}

			if ($promocao['PromocaoCausa']['cep_inicio'] != '') {
				$promocao_causa = true;
				$cep_inicio = preg_replace("/^(\d{5})(\d{3})$/", "\\1-\\2", $promocao['PromocaoCausa']['cep_inicio']);
		?>
				<p><strong>Cep de início: </strong><?php echo $cep_inicio ?></p>
		<?php
			}


			if ($promocao['PromocaoCausa']['cep_fim'] != '') {
				$promocao_causa = true;
				$cep_fim = preg_replace("/^(\d{5})(\d{3})$/", "\\1-\\2", $promocao['PromocaoCausa']['cep_fim']);
		?>
				<p><strong>Cep de fim: </strong><?php echo $cep_fim ?></p>
		<?php
			}

			if ($promocao['PromocaoCausa']['cupom'] != '') {
				$promocao_causa = true;
		?>
				<p><strong>Cód. do Cupom : </strong><?php echo $promocao['PromocaoCausa']['cupom']?> </p>
				<p><strong>Cupom Reutilizável: </strong><?php echo $promocao['PromocaoCausa']['cupom_reutilizavel'] ? '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>' : '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>'; ?> </p>
		<?php
			}


			if ($promocao['PromocaoCausa']['valor_acima_de'] != '') {
				$promocao_causa = true;
		?>
				<p><strong>Valor do produto acima de: </strong><?php echo $this->Formatacao->moeda($promocao['PromocaoCausa']['valor_acima_de']); ?></p>
		<?php
			}

			if ($promocao['PromocaoCausa']['carrinho_valor_acima_de'] != '') {
				$promocao_causa = true;
		?>
				<p><strong>Valor do carrinho acima de: </strong><?php echo $this->Formatacao->moeda($promocao['PromocaoCausa']['carrinho_valor_acima_de']); ?></p>
		<?php
			}

			if ($promocao['PromocaoCausa']['qtd_pecas'] != '') {
				$promocao_causa = true;
		?>
				<p><strong>Qtd do produto acima de: </strong><?php echo $promocao['PromocaoCausa']['qtd_pecas']; ?></p>
		<?php
			}

			if ($promocao['PromocaoCausa']['carrinho_qtd_pecas'] != '') {
				$promocao_causa = true;
		?>
				<p><strong>Qtd de peças no carrinho acima de: </strong><?php echo $promocao['PromocaoCausa']['carrinho_qtd_pecas']; ?></p>
		<?php
			}

			if ($promocao['PromocaoCausa']['utm_source'] != '') {
				$promocao_causa = true;
		?>
				<p><strong>utm_source: </strong><?php echo $promocao['PromocaoCausa']['utm_source']; ?></p>
		<?php
			}


			if ($promocao['PromocaoCausa']['utm_medium'] != '') {
				$promocao_causa = true;
		?>
				<p><strong>utm_medium: </strong><?php echo $promocao['PromocaoCausa']['utm_medium']; ?></p>
		<?php
			}


			if ($promocao['PromocaoCausa']['utm_campaign'] != '') {
				$promocao_causa = true;
		?>
				<p><strong>utm_campaign: </strong><?php echo $promocao['PromocaoCausa']['utm_campaign']; ?></p>
		<?php
			}

			if (!$promocao_causa) {
		?>
				<small class="text-muted">Nenhum item cadastrado.</small>

		<?php
			}

		 ?>
</div>
<br>

<div class="well">
	<h3>Promoção Efeito</h3>
	<hr>
	<?php
		$efeito = null;
		if ($promocao['PromocaoEfeito']['desconto_frete']) {
			$efeito = 'Desconto no Frete';

		} else if ($promocao['PromocaoEfeito']['desconto_pedido']) {
			$efeito = 'Desconto no Pedido';

		} else if ($promocao['PromocaoEfeito']['desconto_produto']) {
			$efeito = 'Desconto no Produto';

		}

	?>

		<p><strong>Efeito: </strong><?php echo $efeito  ?></p>

	<?php

		$tipo_valor = null;
		if ($promocao['PromocaoEfeito']['tipo_valor'] == 'M') {
			$tipo_valor = '(R$) Em reais';
	?>
			<p><strong>Tipo de valor: </strong><?php echo $tipo_valor ?></p>
			<p><strong>Valor: </strong><?php echo $this->Formatacao->moeda($promocao['PromocaoEfeito']['valor'])?></p>

	<?php

		} else if ($promocao['PromocaoEfeito']['tipo_valor'] == 'P') {
			$tipo_valor = '(%) Percentual';
	?>
			<p><strong>Tipo de valor: </strong><?php echo $tipo_valor ?></p>
			<p><strong>Valor: </strong><?php echo intval($promocao['PromocaoEfeito']['valor'])?> %</p>

	<?php

		}
		else if ($promocao['PromocaoEfeito']['tipo_valor'] == 'U') {
			$tipo_valor = 'Unidade';

	?>
			<p><strong>Tipo de valor: </strong><?php echo $tipo_valor ?></p>
			<p><strong>Valor: </strong><?php echo intval($promocao['PromocaoEfeito']['valor'])?></p>

	<?php

		}
	?>


	<?php
		if ($promocao['PromocaoEfeito']['brinde']) {
	?>
		<p><strong>Brinde </strong><?php echo $promocao['PromocaoEfeito']['brinde_descricao']?></p>
	<?php
		}
	 ?>

</div>





