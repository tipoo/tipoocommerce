<?php
	$this->Html->script(array('jquery.price_format.2.0.min', 'jquery.numeric.js','jquery.maskedinput.min', 'bootstrap-datepicker', 'bootstrap-datepicker.pt-BR'), array('inline' => false));
	$this->Html->css(array('datepicker'), 'stylesheet', array('inline' => false));
?>

<?php
	echo $this->Form->create('Promocao');
	echo $this->Form->input('element', array('type' => 'hidden', 'value' => $element));

	echo $this->element('Promocoes/' . $element);

	echo $this->Form->actions();
	echo $this->Form->end();
?>
