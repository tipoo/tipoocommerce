<?php
	$this->Html->css(array('Views/Cms/admin_editor'), 'stylesheet', array('inline' => false));
	$this->Html->script(array('ace-builds/src-min-noconflict/ace', 'Views/Cms/admin_editor'), array('inline' => false));
?>

<div class="row">
	<div class="col-sm-10">
		<ol class="breadcrumb">
			<li>CMS</li>
			<li class="active"><span><?php echo PROJETO . DS . $nome_arquivo; ?></span></li>
		</ol>
	</div>
	<div class="col-sm-2">
		<?php echo $this->Html->link('<span class="glyphicon glyphicon-floppy-disk"></span> Salvar', '#', array('class' => 'btn btn-primary btn-md disabled btn-salvar form-control', 'escape' => false)); ?>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<pre id="editor" ><?php echo htmlentities($conteudo_arquivo, ENT_COMPAT, 'UTF-8'); ?></pre>
	</div>
</div>
<?php 
	echo $this->Form->create('Cms');
	echo $this->Form->input('nome_arquivo', array('type' => 'hidden'));
	echo $this->Form->input('conteudo_arquivo', array('type' => 'textarea'));
	echo $this->Form->end('Salvar');
?>