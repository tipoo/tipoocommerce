<?php 
	$this->Html->css(array(
		'jquery-file-upload/css/jquery.fileupload',
		'Views/Cms/admin_index'
	), 'stylesheet', array('inline' => false));
	
	$this->Html->scriptBlock('var DS = "'.DS.'";', array('inline' => false));

	$this->Html->script(array(
		'jquery-file-upload/js/vendor/jquery.ui.widget',
		'jquery-file-upload/js/jquery.iframe-transport',
		'jquery-file-upload/js/jquery.fileupload',
		'jquery.tinysort/jquery.tinysort.min',
		'Views/Cms/admin_index'
	), array('inline' => false));
?>

<div class="row">
	<div class="col-md-4 arvore">
		<ul class="breadcrumb">
			<li class="active"><span>Arquivos</span></li>
		</ul>
		<?php echo $this->Cms->imprimirArvore(); ?>	
	</div>
	<div class="col-md-8 acoes">
		<div class="row">
			<div class="col-md-12">
				<ul class="breadcrumb">
					<li class="active"><span>Ações para: </span><span class="local-atual" data-local="" data-caminho=""></span></li>
				</ul>
				<?php echo $this->element('Cms/collapse_raiz'); ?>
				<?php echo $this->element('Cms/collapse_pasta'); ?>
				<?php echo $this->element('Cms/collapse_arquivo_imagem'); ?>
				<?php echo $this->element('Cms/collapse_arquivo_editor'); ?>
				<?php echo $this->element('Cms/collapse_arquivo'); ?>
			</div>
		</div>
	</div>
</div>
