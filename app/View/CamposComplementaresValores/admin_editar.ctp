<?php
	$this->Html->script(array('Views/CamposComplementaresValores/admin_editar'), array('inline' => false));
?>

<ol class="breadcrumb">
	<li><?php echo $this->Html->link('Campos Complementares', array('controller' => 'camposComplementares', 'action' => 'backToPaginatorIndex', 'admin' => true)); ?></li>
	<li><?php echo $this->Html->link('Valores', array('action' => 'backToPaginatorIndex', 'admin' => true)); ?></li>
	<li class="active">Editar</li>
</ol>

<?php
	echo $this->Form->create('CamposComplementaresValor');
	echo $this->Form->input('descricao', array('row' => true, 'div' => 'col-md-4', 'label' => 'Descrição*'));
	echo $this->Form->actions();
	echo $this->Form->end();
?>