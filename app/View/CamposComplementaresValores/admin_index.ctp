<ol class="breadcrumb">
	<li ><?php echo $this->Html->link('Campos Complementares', array('controller' => 'camposComplementares', 'action' => 'backToPaginatorIndex', 'admin' => true)); ?></li>
	<li class="active">Valores</li>
</ol>

<div class="table-responsive">
	<table class="table table-striped table-hover">
		<thead>
			<tr>
				<th class="col-md-1 text-center">#</th>
				<th class="col-md-10">Descrição</th>
				<th class="text-center"><?php echo $this->Html->link('<span class="glyphicon glyphicon-plus"></span>', array('action' => 'adicionar', $campos_complementar_id), array('class' => 'btn btn-sm btn-primary', 'escape' => false,'data-placement' => 'left', 'rel' => 'tooltip', 'title' => 'Adicionar Novo')); ?></th>
			</tr>
		</thead>

		<tbody>
			<?php
				foreach ($campos_complementares_valores as $campos_complementares_valor) {
			?>
				<tr>
					<td class="text-center"><?php echo $campos_complementares_valor['CamposComplementaresValor']['id'] ?></td>
					<td><?php echo $campos_complementares_valor['CamposComplementaresValor']['descricao'] ?></td>
					<td class="text-center">
						<div class="btn-group">
							<button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu pull-right text-left" role="menu">
								<li><?php echo $this->Html->link('<span class="glyphicon glyphicon-edit"></span> Editar', array('action' => 'editar', $campos_complementares_valor['CamposComplementaresValor']['id']), array('escape' => false)); ?></li>
								<li><?php echo $this->Form->postLink('<span class="glyphicon glyphicon-trash"></span> Excluir', array('action' => 'excluir', $campos_complementares_valor['CamposComplementaresValor']['id']), array('escape' => false), 'Deseja realmente excluir o valor #' . $campos_complementares_valor['CamposComplementaresValor']['id'] . '?'); ?></li>
							</ul>
						</div>
					</td>
				</tr>
			<?php
				}
			?>
		</tbody>
	</table>
</div>

<?php echo $this->element('Paginacao/admin_paginacao'); ?>