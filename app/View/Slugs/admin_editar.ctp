<?php 	
	$this->Html->script(array('Views/Slugs/admin_editar'), array('inline' => false));
	$this->Html->css(array('Views/Slugs/admin_editar'), 'stylesheet', array('inline' => false)); ?>


<ol class="breadcrumb">
	<li ><?php echo $this->Html->link('Urls', array('action' => 'backToPaginatorIndex', 'admin' => true)); ?></li>
	<li class="active">Editar</li>
</ol>

<?php
	echo $this->Form->create('Slug');
	echo $this->Form->input('id');
?>

	<div class="well">
		<h4><i class="glyphicon glyphicon-exclamation-sign"></i> Aviso:
	       <small> ao editar a url ela perde a indexação atual, fazendo com que o Google e outras ferramentas de busca não consigam listá-la temporariamente.</small>
		</h4>
		<hr>
		<div class="row">
			<div class="col-md-12">
				<div class="form-inline"><label>Url*: </label>
				  <?php echo  Router::url('/', true); ?>
				  <input name="data[Slug][url]" row="1" class="form-control" type="text" value="<?php echo substr($this->request->data['Slug']['url'], 1) ?>" id="SlugUrl">
				</div>
			</div>
		</div>
	</div>
	<br>


<?php
	echo $this->Form->input('controller', array('row' => true, 'div' => 'col-md-4', 'label' => 'Controller*'));
	echo $this->Form->input('action', array('row' => true, 'div' => 'col-md-4', 'label' => 'Action*'));
	echo $this->Form->input('custom_parser_class', array('row' => true, 'div' => 'col-md-4', 'label' => 'Custom Parser Class*'));
	echo $this->Form->actions();
	echo $this->Form->end();
?>