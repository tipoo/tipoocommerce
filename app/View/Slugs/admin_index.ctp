<ol class="breadcrumb">
	<li class="active">Urls</li>
</ol>


<div class="table-responsive">
	<table class="table table-striped table-hover">
		<thead>
			<tr>
				<th class="col-md-1 text-center">#</th>
				<th class="col-md-8">Url</th>
				<th class="col-md-2"></th>
				<th class="text-center"></th>
			</tr>
		</thead>

		<tbody>
			<?php
				foreach ($slugs as $slug) {

					if (!($slug['Slug']['controller'] == 'paginas' && ($slug['Pagina'][0]['sistema']))) {
			?>
						<tr>
							<td class="text-center"><?php echo $slug['Slug']['id'] ?></td>
							<td><?php echo $slug['Slug']['url'] ?>
								<br>
								<?php
									if (!$slug['Slug']['ativo']) {
										$status = '<span class="label label-danger">Inativo</span>';
									} else {
										$status = '<span class="label label-success">Ativo</span> ';
									}

									echo $status;
								?>
							</td>
							<td><?php echo $slug['Slug']['action'] ?></td>
							<td class="text-center">
								<div class="btn-group">
									<button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
										<span class="caret"></span>
									</button>
									<ul class="dropdown-menu pull-right text-left" role="menu">
										<li><?php echo $this->Html->link('<span class="glyphicon glyphicon-edit"></span> Editar', array('action' => 'editar', $slug['Slug']['id']), array('escape' => false)); ?></li>
									</ul>
								</div>
							</td>
						</tr>

			<?php
					}
		
				}
			?>
		</tbody>
	</table>
</div>

<?php echo $this->element('Paginacao/admin_paginacao'); ?>
