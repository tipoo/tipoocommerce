<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $title_for_layout; ?>
	</title>
	<?php
		echo $this->Html->meta('icon');
		echo $this->fetch('meta');

		echo $this->Html->scriptBlock("var WEBROOT = '".$this->webroot."'");
		echo $this->Html->css(array('cadastro'));
		echo $this->Html->script(array('jquery-1.9.1.min'));

		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
</head>
<body>
	<?php echo $this->Session->flash(); ?>
	<?php echo $this->fetch('content'); ?>

	<?php
		if (AMBIENTE == AMBIENTE_DEV && SQL_DUMP) {
			echo $this->element('sql_dump'); 
		}
	?>
</body>
</html>
