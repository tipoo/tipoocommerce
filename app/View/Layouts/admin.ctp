<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $title_for_layout; ?>
	</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<?php
		echo $this->Html->meta('icon');

		echo $this->Html->scriptBlock("var WEBROOT = '".$this->webroot."'; var PROJETO = '".PROJETO."'");
		echo $this->Html->css(array(
			'bootstrap/css/bootstrap.min',
			'admin'
		));

		echo $this->Html->script(array(
			'jquery-1.10.2.min',
			'jquery/ui/1.10.3/jquery-ui',
			'jquery-migrate-1.2.1.min',
			'bootstrap/js/bootstrap.min',
			'validate/jquery.validate.min',
			'validate/messages_ptbr',
			'validate/jquery.validate.bootstrap',
			'bootbox',
			'jquery.maskedinput.min.js',
			'admin'
		));

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>

	<script>
	/*** Handle jQuery plugin naming conflict between jQuery UI and Bootstrap ***/
	$.widget.bridge('uibutton', $.ui.button);
	$.widget.bridge('uitooltip', $.ui.tooltip);
	</script>
</head>
<body>
	<?php echo $this->element('Navbar/admin_menu'); ?>

	<div class="main-container" id="main-container">

		<div class="main-container-inner">

			<div class="container">
				<?php echo $this->Session->flash(); ?>
				<?php echo $this->fetch('content'); ?>
			</div><!-- /.main-content -->


		</div><!-- /.main-container-inner -->
	</div><!-- /.main-container -->
</body>
</html>
