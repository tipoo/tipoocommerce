<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $title_for_layout; ?>
	</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<?php
		echo $this->Html->meta('icon');

		echo $this->Html->scriptBlock("var WEBROOT = '".$this->webroot."'");
		echo $this->Html->css(array(
			'bootstrap/css/bootstrap.min',
			'bootstrap/css/signin',
			'admin'
		));
	?>

	<?php
		echo $this->Html->script(array(
			'jquery-1.10.2.min',
			'bootstrap/js/bootstrap.min',
			'validate/jquery.validate.min',
			'validate/messages_ptbr',
			'validate/jquery.validate.bootstrap'
		));
	?>

	<?php
		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>

</head>
<body class="login-layout">

	<div class="main-container" id="main-container">
		<div class="main-container-inner">

			<div class="container">
				<?php echo $this->Session->flash(); ?>
				<?php echo $this->fetch('content'); ?>
			</div><!-- /.main-content -->


		</div><!-- /.main-container-inner -->
	</div><!-- /.main-container -->
</body>
</html>