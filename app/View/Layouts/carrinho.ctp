<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $title_for_layout; ?>
	</title>
	<?php
		echo $this->Html->meta('icon');
		echo $this->fetch('meta');

		echo $this->Html->scriptBlock("var WEBROOT = '".$this->webroot."'");
		echo $this->Html->css(array('carrinho'));
		echo $this->Html->script(array('jquery-1.9.1.min'));

		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
	<tipoo:config />
	<?php
		echo $this->Html->css(array(
			'normalize',
			'catalogo',
			'carrinho'
		));

	?>
</head>
<body>

<div class="header">

		<div class="center width relative">
			<a href="<?php echo Router::url('/') ?>" class="logo block" alt="Mídia Móveis"></a>
			<div class="busca absolute">
				<tipoo:vitrineProdutosBuscaForm />
			</div>
			<p class="seguro absolute">Compra <strong>100% segura</strong></p>
			<div class="meu-carrinho absolute">
				<?php echo $this->Html->link('Meu Carrinho', array('controller' => 'carrinho', 'action' => 'index')) ?>
				<!--<tipoo:carrinhoTotalItens /> itens-->
			</div>
		</div>
		<div class="bem-vindo">
			<div class="center width relative">
				<div class="bem-vindo-chamada">
					<tipoo:bemVindo
					visitante="Olá visitante! Faça seu #{linkLogin} ou #{linkCadastro}"
					txtLogin="login"
					txtCadastro="cadastre-se"
					autenticado="Olá #{nome}. Se não é você clique aqui para #{linkLogout}."
					txtLogout="sair"
					/>
				</div>
				<div class="bem-vindo-menu">
					<?php echo $this->Html->link('Meus Pedidos', str_replace('http://', 'https://', $this->Html->url(array('controller' => 'clientes', 'action' => 'meus_pedidos'), true)), array('class' => 'meus-pedidos')); ?>
					<?php echo $this->Html->link('Minha Conta', str_replace('http://', 'https://', $this->Html->url(array('controller' => 'clientes', 'action' => 'painel'), true)), array('class' => 'minha-conta')); ?>
				</div>
				<div class="icon_telefone absolute"></div>
				<div class="telefone absolute">(48) <strong>3238-2819</strong></div>
			</div>
		</div>
	</div>

	<div class="width center">
		<?php echo $this->Session->flash(); ?>
		<?php echo $this->fetch('content'); ?>
	</div>

	<div id="footer" class="width center">
		<div class="rodape relative">
			<div class="box-1 informacoes absolute">
				ATENDIMENTO
				<p>(48) 3238-2819</p>
				<a href="mailto:contato@midiamoveis.com.br">contato@midiamoveis.com.br</a>
			</div>
			<div class="box-2 informacoes absolute">
				ENDEREÇO<br/>
				Rua João Cancio Jacques, 49<br/>
				Costeira do Pirajubaé<br/>
				Florianópolis - Santa Catarina
			</div>
			<div class="texto absolute">
				MÍDIA MÓVEIS
				<p>Nossa empresa nasceu em 2010 em Florianópolis, com sede na sobre loja do supermercado Bistek, situada na Rua João Cancio Jaques, 49, no bairro da costeira do pirajubaé.</p>
				<p>A MIDIA MOVEIS nasce com um novo conceito em loja, LOJA SHOW ROOM, em Colchões e Estofados, onde com toda a comodidade e para uma melhor demonstração dos produtos e atendimento de clientes da grande Florianópolis.</p>
				<p><a href="#">Conheça nossa história</a></p>
			</div>
			<div class="pagamento absolute">
				FORMAS DE PAGAMENTO
				<?php echo $this->Html->image('icon_pagamentos.jpg'); ?>
			</div>
			<div class="detalhes absolute">
				MAIS INFORMAÇÕES
				<ul class="coluna-1 absolute">
					<li><a href="#">Assistência Técnica</a></li>
					<li><a href="#">Política de Privacidade</a></li>
					<li><a href="#">Montagem de Móveis</a></li>
				</ul>
				<ul class="coluna-2 absolute">
					<li><a href="#">Trocas e Devoluções</a></li>
					<li><a href="#">Política de entrega</a></li>
					<li><a href="#">Fale conosco</a></li>
				</ul>
			</div>
		</div>
		<div class="assinatura width">
			<p>Mídia Móveis, traduzinho seus sonhos em conforto e bem estar!</p>
			<a class="tipoo-commerce" href="#"><?php echo $this->Html->image('tipoo_commerce.png'); ?></a>
		</div>
	</div>

</body>
</html>
