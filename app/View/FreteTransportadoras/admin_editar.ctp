<?php
	$this->Html->script(array('jquery.numeric','Views/FreteTransportadoras/admin_editar'),array('inline' =>false));
?>

<ol class="breadcrumb">
	<li ><?php echo $this->Html->link('Transportadora', array('action' =>'backToPaginatorIndex', 'admin' =>true));?></li>
	<li class="active">Editar</li>
</ol>

<?php
	echo $this->Form->create('FreteTransportadora');
	echo $this->Form->input('id');
	echo $this->Form->input('descricao',array('row' => true, 'div' => 'col-md-4', 'label' => 'Descrição*'));
	echo $this->Form->input('prazo_carencia',array('type' => 'text', 'row' => true, 'div' => 'col-md-2', 'label' => 'Carência em dias*'));
	echo $this->Form->input('frete_tipo', array('row' => true, 'div' => 'col-md-2','type' => 'select','options' => $tipos, 'empty' => 'Selecione', 'label' => 'Tipo de Frete*'));
	?>

	<?php
		if ($this->request->data['FreteTransportadora']['frete_tipo'] == 'C') {
			$display_codigo_correio = '';
			$display_lojas = 'style="display: none;"';
		} else {
			$display_codigo_correio = 'style="display: none;"';
			$display_lojas = '';
		}
	?>
	<div class="codigo-servico-correio" <?php echo $display_codigo_correio; ?>>
		<?php
			echo $this->Form->input('correios_codigo_servico',array('row' => true, 'div' => 'col-md-4','label' => 'Código do serviço*'));
		?>
	</div>

	<div class="lojas" <?php echo $display_lojas; ?>>
		<h4 class="form-titulos-admin">Lojas em que a transpostadora será utilizada</h4>
		<?php
			echo $this->Form->input('loja', array('row' => true, 'div' => 'col-md-4','type' => 'checkbox', 'label' => 'Loja Principal'));
			if ($this->Configuracoes->get('mercado_livre_ativo')) {
				echo $this->Form->input('mercado_livre', array('row' => true, 'div' => 'col-md-4','type' => 'checkbox', 'label' => 'Mercado Livre'));
			}
		?>
	</div>
	<?php
	echo $this->Form->actions();
	echo $this->Form->end();
?>