<ol class="breadcrumb">
	<li class="active">Transportadoras</li>
</ol>

<div class="table-responsive">
	<table class="table table-striped table-hover">
		<thead>
			<tr>
				<th class="col-md-1 text-center">#</th>
				<th class="col-md-5">Descrição</th>
				<th class="col-md-2">Tipo</th>
				<th class="col-md-2">Lojas</th>
				<th class="col-md-1">Carência</th>
				<th class="text-center"><?php echo $this->Html->link('<span class="glyphicon glyphicon-plus"></span>', array('action' => 'adicionar'), array('class' => 'btn btn-sm btn-primary', 'escape' => false,'data-placement' => 'left', 'rel' => 'tooltip', 'title' => 'Adicionar Novo')); ?></th>
			</tr>
		</thead>

		<tbody>
			<?php
				foreach ($frete_transportadoras as $frete_transportadora) {
			?>
				<tr>
					<td class="text-center"><?php echo $frete_transportadora['FreteTransportadora']['id']?></td>
					<td><?php echo $frete_transportadora['FreteTransportadora']['descricao']?></td>
		 			<td><?php echo $tipos[$frete_transportadora['FreteTransportadora']['frete_tipo']]?></td>
		 			<td>
		 				<?php
		 					if ($frete_transportadora['FreteTransportadora']['frete_tipo'] == 'T') {
		 						if ($frete_transportadora['FreteTransportadora']['loja']) {
		 							echo $lojas['loja'] . '<br>';
		 						}
		 						if ($frete_transportadora['FreteTransportadora']['mercado_livre']) {
		 							echo $lojas['mercado_livre'] . '<br>';
		 						}
		 					}
		 				?>
		 			</td>
		 			<td><?php echo $frete_transportadora['FreteTransportadora']['prazo_carencia']; ?></td>
					<td class="text-center">
						<div class="btn-group">
							<button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu pull-right text-left" role="menu">
								<li><?php echo $this->Html->link('<span class="glyphicon glyphicon-edit"></span> Editar', array('action' => 'editar', $frete_transportadora['FreteTransportadora']['id']), array('escape' => false)); ?></li>
								<li><?php echo $this->Form->postLink('<span class="glyphicon glyphicon-trash"></span> Excluir', array('action' => 'excluir', $frete_transportadora['FreteTransportadora']['id']), array('escape' => false), 'Deseja realmente excluir o Frete da Transportadora #' . $frete_transportadora['FreteTransportadora']['id'] . '?'); ?></li>
								<?php
									if ($frete_transportadora['FreteTransportadora']['frete_tipo'] == 'T') {
								?>
										<li><?php echo $this->Html->link('<span class="glyphicon"></span> Valores', array('controller' => 'freteValores','action' => 'index', $frete_transportadora['FreteTransportadora']['id']), array('escape' => false)); ?></li>
										<li><?php echo $this->Html->link('<span class="glyphicon"></span> Importar CSV', array('controller' => 'freteValores','action' => 'importar', $frete_transportadora['FreteTransportadora']['id']), array('escape' => false)); ?></li>
								<?php
									}
								 ?>

							</ul>
						</div>
					</td>
				</tr>
			<?php
				}
			?>
		</tbody>
	</table>
</div>

<?php echo $this->element('Paginacao/admin_paginacao'); ?>