<?php
	$this->Html->script(array('jquery.numeric','Views/FreteTransportadoras/admin_adicionar'),array('inline' =>false));
?>

<ol class="breadcrumb">
	<li ><?php echo $this->Html->link('Transportadora', array('action' =>'backToPaginatorIndex', 'admin' =>true));?></li>
	<li class="active">Adicionar</li>
</ol>

<?php
	echo $this->Form->create('FreteTransportadora');
	echo $this->Form->input('descricao',array('row' => true, 'div' => 'col-md-4', 'label' => 'Descrição*'));
	echo $this->Form->input('prazo_carencia',array('type' => 'text', 'row' => true, 'div' => 'col-md-2', 'label' => 'Carência em dias*', 'value' => 0));
	echo $this->Form->input('frete_tipo', array('row' => true, 'div' => 'col-md-2','type' => 'select','options' => $tipos, 'empty' => 'Selecione', 'label' => 'Tipo de Frete*'));
?>
	<div class="codigo-servico-correio" style="display: none">
		<?php
			echo $this->Form->input('correios_codigo_servico',array('row' => true, 'div' => 'col-md-4','label' => 'Código do serviço*'));
		?>
	</div>

	<div class="lojas" style="display: none">
		<h4 class="form-titulos-admin">Lojas em que a transpostadora será utilizada</h4>
		<?php
			echo $this->Form->input('loja', array('row' => true, 'div' => 'col-md-4','type' => 'checkbox', 'label' => 'Loja Principal', 'checked' => 'checked'));
			if ($this->Configuracoes->get('mercado_livre_ativo')) {
				echo $this->Form->input('mercado_livre', array('row' => true, 'div' => 'col-md-4','type' => 'checkbox', 'label' => 'Mercado Livre'));
			}
		?>
	</div>
<?php
	echo $this->Form->actions();
	echo $this->Form->end();
?>