<h3>Pagamento não autorizado</h3>
<p>
	O pagamento do seu pedido <strong>não foi autorizado</strong> pela instituição financeira. Entre em contato com a mesma para
	saber os motivos e faça a sua compra novamente.
</p>
