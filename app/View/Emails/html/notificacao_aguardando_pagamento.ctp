<h3>Confirmação de recebimento de pedido</h3>
<p>Seu pedido #<?php echo $pedido['Pedido']['id']; ?> foi recebido e está aguardando pagamento. Segue abaixo o resumo:</p>
<table cellspacing="0" cellpadding="10" border="0" width="600">
	<tr>
		<td><strong>Produto</strong></td>
		<td><strong>Marca</strong></td>
		<td><strong>Qtd.</strong></td>
		<td><strong>Preço Un.</strong></td>
		<td><strong>Preço Total</strong></td>
	</tr>
	<?php
		foreach ($pedido['PedidoSku'] as $pedido_sku) {

	?>
			<tr>
				<td><?php echo $pedido_sku['descricao_produto'].' '.$pedido_sku['descricao_sku']  ?>
					<br />
					<?php

						if (isset($pedido_sku['CompreJuntoPromocao'][0])) {

						?>
							<small class="text-muted">Desconto: "Promoção Compre Junto"</small>
							<br>

						<?php

						}

						if (isset($pedido_sku['Promocao'][0])) {

							foreach ($pedido_sku['Promocao']as $promocao) {
								
						?>
								<small class="text-muted">Desconto: "Promoção <?php echo $promocao['descricao']?>"</small>
								<br>
						<?php
							}
						}
					?>
				</td>
				<td><?php echo $pedido_sku['descricao_marca']?></td>
				<td><?php echo $pedido_sku['qtd']?></td>
				<td>

				<?php 
					if ($pedido_sku['preco_unitario_desconto'] == '') {

						if ($pedido_sku['Sku']['Produto']['preco_de'] != '') {
				?>
							<small ><strike><?php echo 'R$' . number_format( $pedido_sku['Sku']['Produto']['preco_de'], 2, ',', '.'); ?></strike></small>
							<br>
				<?php
						}

						echo 'R$' . number_format($pedido_sku['preco_unitario'], 2, ',', '.');
					} else {
						
						if ($pedido_sku['preco_unitario'] == '') {
				?>
							<small ><strike><?php echo 'R$' . number_format($pedido_sku['Sku']['Produto']['preco_por'], 2, ',', '.'); ?></strike></small>
				<?php
						} else {
				?>
							<small ><strike><?php echo 'R$' . number_format($pedido_sku['preco_unitario'], 2, ',', '.'); ?></strike></small>
				<?php
						}

				?>

						<br>
				<?php
						echo 'R$' . number_format($pedido_sku['preco_unitario_desconto'], 2, ',', '.'); 
					}
				 ?>
				</td>
				<td><?php echo 'R$' . number_format($pedido_sku['preco_total'], 2, ',', '.');?></td>
			</tr>
	<?php
		}
	?>

	<?php 
		$tem_desconto_pedido = false;
		if (isset($pedido['Promocao'])) {

			foreach ($pedido['Promocao'] as $promocao) {
				
				if ($promocao['element'] == 'desconto_pedido') {
					$tem_desconto_pedido = true;

					$desconto_pedido_descricao = '<small class="text-muted"> Promoção no Pedido : ' . $promocao['descricao'] . '</small>';

					if ($promocao['PromocaoEfeito']['tipo_valor'] == 'P') {
						$desconto_pedido_valor = $pedido['Pedido']['valor_produtos'] * $promocao['PromocaoEfeito']['valor'] / 100;
					} else {
						$desconto_pedido_valor = $promocao['PromocaoEfeito']['valor'];
					}

				}
			}
		}

		$tem_cupom = false;
		if (isset($pedido['Promocao'])) {

			foreach ($pedido['Promocao'] as $promocao) {
			
				if ($promocao['element'] == 'cupom_desconto') {
					$tem_cupom = true;

					$cupom_descricao = '<small class="text-muted"> Cupom de Desconto : ' . $promocao['descricao'] . ' (Cód. -' . $promocao['PromocaoCausa']['cupom'] . ')</small>';

					if ($promocao['PromocaoEfeito']['tipo_valor'] == 'P') {

						if ($tem_desconto_pedido) {
							$valor_parcial_pedido = $pedido['Pedido']['valor_produtos'] - $desconto_pedido_valor;
						} else {
							$valor_parcial_pedido = $pedido['Pedido']['valor_produtos'];
						}

						$cupom_valor = $valor_parcial_pedido * $promocao['PromocaoEfeito']['valor'] / 100;

					} else {
						$cupom_valor = $promocao['PromocaoEfeito']['valor'];
					}
					
				}
			}
		}


	?>

	<tr>
		<td>
		<?php 
			if ($tem_desconto_pedido) {
				 echo $desconto_pedido_descricao;
			?>
			<br>
		<?php
			}
			if ($tem_cupom) {
				 echo $cupom_descricao;
			}
		 ?>
		</td>
		<td></td>
		<td>
		<?php 
			if ($tem_desconto_pedido) {
		?>
			<small> - <?php echo 'R$' . number_format($desconto_pedido_valor, 2, ',', '.'); ?></small> 
			<br>
		<?php
			}
			if ($tem_cupom) {
		?>
			<small> - <?php echo 'R$' . number_format($cupom_valor, 2, ',', '.'); ?></small> 
			<br>
		<?php
			}
		?>
		</td>
		<td><strong>Sub-total:</strong> </td>
		<td>			
		<?php 
			$sub_total = 0;
			if (($pedido['Pedido']['valor_desconto'] == '' || !$tem_desconto_pedido) && !$tem_cupom) {
				$sub_total = $pedido['Pedido']['valor_produtos'];
			} else {
		?>
				<small><strike><?php echo $pedido['Pedido']['valor_produtos']; ?></strike></small>
				<br>
		<?php
				$sub_total = $pedido['Pedido']['valor_produtos'] - $pedido['Pedido']['valor_desconto'];
			}
		 	echo 'R$' . number_format($sub_total, 2, ',', '.'); 
		?>
		</td>
	</tr>
	<tr>
		<td>
		<?php 
			if (isset($pedido['Promocao'])) {

				foreach ($pedido['Promocao'] as $promocao) {
				
					if ($promocao['element'] == 'desconto_frete') {

		?>
						<small class="text-muted"> Promoção no Frete : " <?php echo $promocao['descricao']?>"</small>
		<?php
					}
				}

			}
		?>
		</td>
		<td></td>
		<td></td>
		<td><strong>Frete:</strong> </td>
		<td><?php echo 'R$' . number_format($pedido['Pedido']['valor_frete'], 2, ',', '.'); ?></td>
	</tr>
	<tr>
		<td></td>
		<td></td>
		<td></td>
		<td><strong>Total:</strong> </td>
		<td><?php echo 'R$' . number_format($pedido['Pedido']['valor_total'], 2, ',', '.'); ?></td>
	</tr>
</table>
