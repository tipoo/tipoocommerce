<?php
	$this->Html->script(array('bootstrap-datepicker', 'bootstrap-datepicker.pt-BR','Views/Newsletters/admin_index'),array('inline' =>false));
	$this->Html->css(array('datepicker','Views/Newsletters/admin_index'), 'stylesheet', array('inline' => false));
?>
<ol class="breadcrumb">
	<li class="active">Newsletters</li>
</ol>

<div class="alert alert-danger hide" id="alert-datepicker">
	<span>Mensagem de erro: Intervalo entre datas incorreto.</span>
</div>

<div class="row">
	<div class="col-md-9 col-lg-10">
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="row">
					<div class="col-md-3 input-append date" id="dp1" data-date-format="dd/mm/yyyy">
						<label>De</label>
						<div class="input-group">
							<span class="input-group-btn">
								<button class="btn btn-default add-on" type="button">
									<span class="glyphicon glyphicon-calendar"></span>
								</button>
							</span>
							<input name="data[Filtro][data_inicio]" id="dp1-mask" size="16" class="form-control" type="text" data-date="<?php echo $this->Formatacao->data($de) ?>" value="<?php echo $this->Formatacao->data($de) ?>" >
						</div>
					</div>
					<div class="col-md-3 input-append date" id="dp2" data-date-format="dd/mm/yyyy">
						<label>Até</label>
						<div class="input-group">
							<span class="input-group-btn">
								<button class="btn btn-default add-on" type="button">
									<span class="glyphicon glyphicon-calendar"></span>
								</button>
							</span>
							<input name="data[Filtro][data_fim]" id="dp2-mask" size="16" class="form-control" type="text" data-date="<?php echo $this->Formatacao->data($ate) ?>" value="<?php echo $this->Formatacao->data($ate) ?>" >
						</div>
					</div>
					<div class="col-md-2">
						<?php echo $this->Form->input('Filtrar', array('type' => 'button', 'label' => '&nbsp;', 'div' => false, 'class' => 'btn btn-primary', 'id' => 'btnFiltrar')); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-3 col-lg-2 text-right">
		<div class="panel panel-default">
			<div class="panel-body">
				<label for="btnFiltrar" class="control-label hidden-sm hidden-xs">&nbsp;</label>
				<?php
					$urlNewsletter = array('controller' => 'newsletters', 'action' => 'baixar_csv', 'admin' => true, 'plugin' => null);

					if ($this->Permissoes->check($urlNewsletter)) {
						echo $this->Html->link('<span class="glyphicon glyphicon-download-alt"></span> Baixar CSV', $urlNewsletter, array('class ' => 'btn btn-success form-control baixar-csv', 'escape' => false));
					}
				?>
			</div>
		</div>
	</div>
</div>

<h5>Total de clientes cadastrados : <?php echo $total_cadastrados;?></h5>

<div class="table-responsive">
	<table class="table table-striped table-hover">
		<thead>
			<tr>
				<th class="col-md-1 text-center">#</th>
				<th class="col-md-4">Nome</th>
				<th class="col-md-5">E-mail</th>
				<th class="col-md-2 text-center">Data de Cadastro</th>
			</tr>
		</thead>

		<tbody>
			<?php
				foreach ($newsletters as $newsletter) {
			?>
				<tr>
					<td class="text-center"><?php echo $newsletter['Newsletter']['id']?></td>
					<td><?php echo $newsletter['Newsletter']['nome']?></td>
		 			<td><?php echo $newsletter['Newsletter']['email']?></td>
		 			<td class="text-center"><?php echo $this->Formatacao->dataHora($newsletter['Newsletter']['created'])?></td>
				</tr>
			<?php
				}
			?>
		</tbody>
	</table>
</div>

<?php echo $this->element('Paginacao/admin_paginacao'); ?>