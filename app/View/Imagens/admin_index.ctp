<?php
	$this->Html->script(array('Views/Imagens/admin_index'), array('inline' => false));
?>

<ol class="breadcrumb">
	<li><?php echo $this->Html->link('Produtos', array('controller' => 'produtos', 'action' => 'backToPaginatorIndex', 'admin' => true)); ?></li>
	<?php
		if ($referente == 'sku_id') {
	?>
			<li ><?php echo $this->Html->link('Sku', array('controller' => 'skus', 'action' => 'backToPaginatorIndex', 'admin' => true)); ?></li>
	<?php
		}
	?>
	<li class="active">Imagens</li>
</ol>

<div class="page-header">
	<?php
		echo $this->Form->input('referente_id', array('type' => 'hidden', 'value' => $referente_id));
		echo $this->Form->input('referente', array('type' => 'hidden', 'value' => $referente));

		if ($referente == 'produto_id') {
	?>
			<div class="page-header">
				<h4><?php echo $produto['Produto']['descricao']; ?></h4>
			</div>

			<div class="alert alert-info">
				<i class="glyphicon glyphicon-info-sign"></i> <strong>ORDENAÇÃO: </strong> Arraste para ordenar as imagens. Obs: A primeira imagem sempre será a imagem principal na página do produto.
			</div>

			<div class="alert alert-info info-imagem-principal">
				<i class="glyphicon glyphicon-info-sign"></i> <strong>ORDENAÇÃO: </strong> Obs: A primeira imagem sempre será a imagem principal na página do produto.
			</div>



	<?php
		}
	?>

	<?php
		if ($referente == 'sku_id') {
	?>

			<div class="page-header">
				<h4><?php echo $sku['Produto']['descricao']; ?></h4>
				<p><strong>Sku: </strong><?php echo $sku['Sku']['descricao']; ?></p>
			</div>

			<div class="alert alert-info">
				<i class="glyphicon glyphicon-info-sign"></i> <strong>ORDENAÇÃO: </strong> Arraste para ordenar as imagens.
			</div>
	<?php
		}
	?>
</div>

<div class="table-responsive">
	<table class="table table-striped table-hover">
		<thead>
			<tr>
				<th class="col-md-1 text-center">#</th>
				<th class="col-xs-8 col-sm-8 col-md-10">Imagem</th>
				<th class="visible-xs visible-sm"></th>
				<th class="visible-xs visible-sm"></th>
				<th class="text-center"><?php echo $this->Html->link('<span class="glyphicon glyphicon-plus"></span>', array('action' => 'adicionar', 'referente' => $referente, $referente_id), array('class' => 'btn btn-sm btn-primary', 'escape' => false,'data-placement' => 'left', 'rel' => 'tooltip', 'title' => 'Adicionar Novo')); ?></th>
			</tr>
		</thead>
		<tbody id="sortable">
			<?php
				foreach ($imagens as $imagem) {
			?>
					<tr class="ui-state-default" data-id="<?php echo $imagem['Imagem']['id']; ?>">
						<td class="text-center"><?php echo $imagem['Imagem']['id'] ?></td>
						<td><a  href="<?php echo $this->Imagens->gerarUrlImagemProduto('big', APP . 'webroot' . DS . 'files' . DS . PROJETO . DS . 'produtos' . DS . $imagem['Imagem']['imagem'], $imagem['Imagem']['imagem']) ?>" class="generic-modal" target="_blank" rel="<?php echo $imagem['Imagem']['imagem'] ?>"><img src="<?php echo $this->Imagens->gerarUrlImagemProduto('thumb', APP . 'webroot' . DS . 'files' . DS . PROJETO . DS . 'produtos' . DS . $imagem['Imagem']['imagem'], $imagem['Imagem']['imagem']) ?>" /></a></td>
						<td class="visible-xs visible-sm text-center seta-ordenacao" >
							<div class="ordenacao-desce " data-indice="<?php echo $imagem['Imagem']['id']; ?>"><span class="glyphicon glyphicon-arrow-down"></span></div>
						</td>
						<td class="visible-xs visible-sm text-center seta-ordenacao" >
							<div class="ordenacao-sobe " data-indice="<?php echo $imagem['Imagem']['id']; ?>"><span class="glyphicon glyphicon-arrow-up"></span></div>
						</td>
						<td class="text-center">
						<div class="btn-group">
							<button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu pull-right text-left" role="menu">
								<li><?php echo $this->Html->link('<span class="glyphicon glyphicon-edit"></span> Editar', array('action' => 'editar', $imagem['Imagem']['id']), array('escape' => false)); ?></li>
								<li><?php echo $this->Form->postLink('<span class="glyphicon glyphicon-trash"></span> Excluir', array('action' => 'excluir', $imagem['Imagem']['id'], $referente, $referente_id), array('escape' => false), 'Deseja realmente excluir a Imagem #' . $imagem['Imagem']['id'] . '?'); ?></li>
							</ul>
						</div>
					</tr>
			<?php
				}
			?>
		</tbody>
	</table>
</div>
