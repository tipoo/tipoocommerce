<?php
	$this->Html->script(array('Views/Imagens/admin_editar'), array('inline' => false));
?>

<ol class="breadcrumb">
	<li ><?php echo $this->Html->link('Produtos', array('controller' => 'produtos', 'action' => 'backToPaginatorIndex', 'admin' => true)); ?></li>
	<?php
		if ($imagem['Sku']['id'] != '') {
	?>
			<li><?php echo $this->Html->link('Sku', array('controller' => 'skus', 'action' => 'backToPaginatorIndex', 'admin' => true)); ?></li>
	<?php
		}
	?>
	<li><?php echo $this->Html->link('Imagens', array('action' => 'backToPaginatorIndex', 'admin' => true)); ?></li>
	<li class="active">Editar</li>
</ol>

<div class="page-header">
	<?php
		if ($imagem['Produto']['id'] != '') {
	?>
			<h4><?php echo $imagem['Produto']['descricao']; ?></h4>
	<?php
		}
	?>

	<?php
		if ($imagem['Sku']['id'] != '') {
	?>
			<h4><?php echo $imagem['Sku']['Produto']['descricao']; ?></h4>
			<p><strong>Sku: </strong><?php echo $imagem['Sku']['descricao']; ?></p>
	<?php
		}
	?>
</div>

<?php
	echo $this->Form->create('Imagem', array('type' => 'file'));

	echo $this->Form->input('imagem', array('type' => 'file', 'label' => 'Imagem*'));
?>
	<div class="control-group">
		<label class="control-label" for="ImagemImagem"></label>
		<div class="controls">
			<img style="max-width: 300px; max-height: 300px" src="<?php echo $this->Imagens->gerarUrlImagemProduto('big', APP . 'webroot' . DS . 'files' . DS . PROJETO . DS . 'produtos' . DS . $this->request->data['Imagem']['imagem'], $this->request->data['Imagem']['imagem']) ?>" />
		</div>
	</div>
	<br>

<?php
	echo $this->Form->actions();
	echo $this->Form->end();
?>
