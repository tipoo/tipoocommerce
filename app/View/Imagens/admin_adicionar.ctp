<?php
	$this->Html->script(array('Views/Imagens/admin_adicionar'), array('inline' => false));
?>

<ol class="breadcrumb">
	<li><?php echo $this->Html->link('Produtos', array('controller' => 'produtos', 'action' => 'backToPaginatorIndex', 'admin' => true)); ?></li>
	<?php
		if ($referente == 'sku_id') {
	?>
			<li><?php echo $this->Html->link('Sku', array('controller' => 'skus', 'action' => 'backToPaginatorIndex', 'admin' => true)); ?></li>
	<?php
		}
	?>
	<li><?php echo $this->Html->link('Imagens', array('action' => 'backToPaginatorIndex', 'admin' => true)); ?></li>
	<li class="active">Adicionar</li>
</ol>

<div class="page-header">
	<?php
		if ($referente == 'produto_id') {
	?>
			<h4><?php echo $produto['Produto']['descricao']; ?></h4>
	<?php
		}
	?>

	<?php
		if ($referente == 'sku_id') {
	?>
			<h4><?php echo $sku['Produto']['descricao']; ?></h4>
			<p><strong>Sku: </strong><?php echo $sku['Sku']['descricao']; ?></p>
	<?php
		}
	?>
</div>

<?php
	echo $this->Form->create('Imagem', array('type' => 'file'));
	echo $this->Form->input('imagem', array('type' => 'file', 'label' => 'Imagem*'));
	echo $this->Form->actions();
	echo $this->Form->end();
?>
