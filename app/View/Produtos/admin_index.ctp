<?php
	$this->Html->script(array('Views/Produtos/admin_index'), array('inline' => false));
?>

<ol class="breadcrumb">
	<li class="active">Produtos</li>
</ol>

<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-2">
				<label>Categorias</label>
				<?php
					echo $this->MenuCategorias->comboCategorias($combo_categorias, array(
						'empty' => 'Todas',
						'id_selecionado' => $this->request->data['Filtro']['categoria_id'],
						'name' => 'data[Filtro][cateogria_id]',
						'id' => 'FiltroCategoriaId',
						'class' => 'form-control'
					));
				?>
			</div>
			<div class="col-md-2">
				<?php echo $this->Form->input('Filtro.marca_id', array('empty' => 'Todas', 'label' => 'Marcas', 'div' => false)); ?>
			</div>

			<div class="col-md-2">
				<?php echo $this->Form->input('Filtro.nome', array('label' => 'Nome do Produto', 'div' => false)); ?>
			</div>

			<div class="col-md-2">
				<?php echo $this->Form->input('Filtrar', array('type' => 'button', 'label' => '&nbsp;', 'div' => false, 'class' => 'btn btn-primary', 'id' => 'btnFiltrar')); ?>
			</div>

			<div class="col-md-offset-1 col-md-3 campo-buscar-cliente">
				<label>&nbsp;</label>
				<div class="input-group">
					<?php echo $this->Form->input('Filtro.produto', array('id' => 'FiltrarIdProdutoValue', 'type' => 'text', 'placeholder' =>'Cód. do Produto ou SKU' ,'label' => false, 'div' => false)); ?>
					<span class="input-group-btn">
						<?php echo $this->Form->button('<span class="glyphicon glyphicon-search"></span>', array('id' =>'FiltrarIdProduto','class' => 'btn btn-default buscar', 'type' => 'button')); ?>
					</span>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="table-responsive">
	<table class="table table-striped table-hover">
		<thead>
			<tr>
				<th class="col-md-1 text-center">#</th>
				<th class="col-md-1 text-center">Imagem</th>
				<th class="col-md-3">Descrição</th>
				<th class="col-md-3">Skus</th>
				<th class="col-md-3">Coleções</th>
				<th class="text-center"><?php echo $this->Html->link('<span class="glyphicon glyphicon-plus"></span>', array('action' => 'adicionar', $categoria_id), array('class' => 'btn btn-sm btn-primary', 'escape' => false,'data-placement' => 'left', 'rel' => 'tooltip', 'title' => 'Adicionar Novo')); ?></th>
			</tr>
		</thead>

		<tbody>
			<?php
				foreach ($produtos as $produto) {
					$imagem = '';
					if (isset($produto['Imagem']['0']['imagem'])) {
						$imagem = $produto['Imagem']['0']['imagem'];
					}
			?>
				<tr>
					<td class="text-center"><?php echo $produto['Produto']['id'] ?></td>
					<td class="text-center"><a href="<?php echo $this->Imagens->gerarUrlImagemProduto('big', APP . 'webroot' . DS . 'files' . DS . PROJETO . DS . 'produtos' . DS . $imagem, $imagem) ?>" class="generic-modal" target="_blank" rel="<?php echo $produto['Produto']['descricao']; ?>"><img src="<?php echo $this->Imagens->gerarUrlImagemProduto('thumb', APP . 'webroot' . DS . 'files' . DS . PROJETO . DS . 'produtos' . DS . $imagem, $imagem) ?>" /></a></td>
					<td>

						<?php echo $this->Html->link($produto['Produto']['descricao'],array('action' => 'visualizar_detalhes_produto',$produto['Produto']['id']), array('class ' => 'ver-detalhes-produto', 'escape' => false, 'rel' => 'tooltip', 'title' => 'Ver Detalhes')); ?>

						<br />
						<span class="sub-descricao"><?php echo $produto['Marca']['descricao'] ?></span>
						<br />
						<?php
							if (!$produto['Produto']['ativo']) {
								$status = '<span class="label label-danger">Inativo</span>';
							} else {
								$status = '<span class="label label-success">Ativo</span> ';
								$status .= '<a href="' . Router::url($produto['Slug']['url']) . '" target="_blank"><small>Ver no site</small></a>';
							}

							echo $status;
						?>
					</td>
					<td>
						<?php
							if ($produto['Sku']) {
						?>
								<ul class="list-unstyled">
									<?php
										foreach ($produto['Sku'] as $sku) {
									?>
											<li><?php echo '(' . $sku['sku'] . ') ' . $sku['descricao'] ?></li>
									<?php
										}
									?>
								</ul>
						<?php
							}
						?>
					</td>
					<td>
						<?php
							if (count($produto['ColecoesProduto'])) {
						?>
								<ul class="list-unstyled">
									<?php
										foreach ($produto['ColecoesProduto'] as $colecao) {
									?>
											<li><?php echo '#' . $colecao['Colecao']['id'] . ' '  . $colecao['Colecao']['descricao'] ?></li>
									<?php
										}
									?>
								</ul>
						<?php
							}
						?>
					</td>
					<td class="text-center">
						<div class="btn-group">
							<button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu pull-right text-left" role="menu">
								<li><?php echo $this->Html->link('<span class="glyphicon glyphicon-edit"></span> Editar', array('action' => 'editar', $produto['Produto']['id']), array('escape' => false)); ?></li>
								<li><?php echo $this->Html->link('<span class="glyphicon"></span> SKUS', array('controller' => 'skus', 'action' => 'index', $produto['Produto']['id']), array('escape' => false));?></li>
								<li><?php echo $this->Html->link('<span class="glyphicon"></span> Imagens', array('controller' => 'imagens', 'action' => 'index', 'referente' => 'produto_id', $produto['Produto']['id']), array('escape' => false));?></li>

 								<?php
									$urlServicos = array('controller' => 'servicos', 'action' => 'servicos_produto', 'admin' => true, 'plugin' => null);
									if ($this->Permissoes->check($urlServicos)) {
								?>
										<li> <?php echo $this->Html->link('<span class="glyphicon"></span> Serviços', array('controller' => 'servicos', 'action' => 'servicos_produto', $produto['Produto']['id']), array('escape' => false));?></li>
								<?php
									}
								 ?>
								<?php
									$urlCompreJunto = array('controller' => 'produtos', 'action' => 'compre_junto', 'admin' => true, 'plugin' => null);
									if ($this->Permissoes->check($urlCompreJunto)) {
								?>
										<li> <?php echo $this->Html->link('<span class="glyphicon"></span> Compre Junto', array('action' => 'compre_junto', $produto['Produto']['id']), array('escape' => false));?></li>
								<?php
									}
								 ?>

								<?php
									if ($this->Permissoes->check(array('controller' => 'vitrines', 'action' => 'colecoes', 'admin' => true, 'plugin' => null))) {
								?>
										<li> <?php echo $this->Html->link('<span class="glyphicon"></span> Produtos Relacionados', array('controller' => 'vitrines', 'action' => 'colecoes', 'produto_id' => $produto['Produto']['id']), array('escape' => false));?></li>
								<?php
									}
								?>

								<?php
									if ($this->Permissoes->check(array('controller' => 'vitrines', 'action' => 'htmls', 'admin' => true, 'plugin' => null))) {
								?>
										<li> <?php echo $this->Html->link('<span class="glyphicon"></span> Texto/Html', array('controller' => 'vitrines', 'action' => 'htmls', 'produto_id' => $produto['Produto']['id']), array('escape' => false));?></li>
								<?php
									}
								?>

								<?php

									if ($produto['Produto']['ativo']) {
								?>
										<li><?php echo $this->Form->postLink('<span class="glyphicon"></span> Desativar', array('action' => 'excluir', $produto['Produto']['id']), array('escape' => false), 'Deseja realmente desativar o Produto #' . $produto['Produto']['id'] . '?');?></li>
								<?php
									} else {

										if (isset($produto['Sku'][0]) && isset($produto['Imagem'][0]) && $produto['Marca']['ativo']) {
								?>
											<li><?php echo $this->Form->postLink('<span class="glyphicon"></span> Ativar', array('action' => 'ativar', $produto['Produto']['id']), array('escape' => false));?></li>
								<?php
										} else {

											if (!($produto['Marca']['ativo'])) {
												$title = 'A marca desse produto está desativada. Ative a marca para poder ativar o produto.';

											} else if (!isset($produto['Sku'][0])) {
												$title = 'Adicione ao menos um sku para ativar o produto.';

											} else if (!isset($produto['Imagem'][0])) {
												$title = 'Adicione ao menos uma imagem para ativar o produto.';

											}
								?>
												<li class="disabled"><a href="javascript: void(0)" data-placement="left" rel="tooltip" title="" data-original-title="<?php echo $title ?>"><span class="glyphicon"></span> Ativar</a></li>
								<?php
										}

									}
								?>
							</ul>
						</div>
					</td>
				</tr>
			<?php
				}
			?>
		</tbody>
	</table>
</div>

<?php echo $this->element('Paginacao/admin_paginacao'); ?>