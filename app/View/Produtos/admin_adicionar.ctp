<?php
	$this->Html->script(array('jquery.maskedinput.min', 'jquery.price_format.2.0.min', 'jquery.numeric', 'prettify','wysihtml5-0.3.0','bootstrap3-wysihtml5','bootstrap-wysihtml5.pt-BR','jquery.scrollTo-min', 'Views/Produtos/admin_adicionar'), array('inline' => false));
	$this->Html->css(array('bootstrap3-wysiwyg5-color','bootstrap3-wysiwyg5','Views/Produtos/admin_adicionar'), 'stylesheet', array('inline' => false));
?>
<ol class="breadcrumb">
	<li ><?php echo $this->Html->link('Categorias', array('controller' => 'categorias', 'action' => 'backToPaginatorIndex', 'admin' => true)); ?></li>
	<li ><?php echo $this->Html->link('Produtos', array('action' => 'backToPaginatorIndex', 'admin' => true)); ?></li>
	<li class="active">Adicionar</li>
</ol>

<?php
	echo $this->Form->create('Produto', array('type' => 'file'));
?>
<div class="row">
	<div class="col-md-12">
	   <div class="row">
			<div class="col-md-6">
				<?php
					echo $this->Form->input('codigo_de_referencia', array('row' => true, 'div' => 'col-md-8', 'type' => 'text', 'label' => 'Código de referência do produto*'));
					echo $this->Form->input('descricao', array('row' => true, 'div' => 'col-md-8', 'type' => 'text', 'label' => 'Nome do Produto*'));
				?>
				<div class="row">
					<div class="col-md-8 form-group">
						<label class="control-label" for="ProdutoCategoriaId">Categoria</label>
						<?php
							echo $this->MenuCategorias->comboCategorias($combo_categorias, array(
								'empty' => 'Selecione',
								'class' => 'form-control',
								'selecao_produto' => true,
								'name' => 'data[Produto][categoria_id]',
								'id' => 'ProdutoCategoriaId'
							));
						?>
					</div>
				</div>
				<br>
				<?php
					echo $this->Form->input('marca_id', array('row' => true, 'div' => 'col-md-8', 'empty' => 'Selecione', 'label' => 'Marca*'));
					echo $this->Form->input('detalhes', array('row' => true, 'div' => 'col-md-8','label' => 'Descrição'));
					echo $this->Form->input('descricao_resumida', array('row' => true, 'div' => 'col-md-8', 'label' => 'Descrição Resumida'));
				?>

				<h4 class="form-titulos-admin">Preço</h4>
				<?php
					echo $this->Form->input('preco_de', array('row' => true, 'div' => 'col-md-4', 'label' => 'De R$', 'type' => 'text'));
					echo $this->Form->input('preco_por', array('row' => true, 'div' => 'col-md-4', 'label' => 'Por R$*', 'type' => 'text'));
				?>

				<?php
					echo $this->Form->input('mostrar_indisponivel', array('row' => true, 'div' => 'col-md-6', 'type' => 'checkbox', 'label' => 'Mostrar quando indisponível', 'checked'));
				?>

				<?php
					if ($this->Configuracoes->get('informacoes_fiscais')) {
				?>
						<h4 class="form-titulos-admin">Informações fiscais</h4>
						<?php
							echo $this->Form->input('ncm', array('row' => true, 'div' => 'col-md-8', 'type' => 'text', 'label' => 'NCM'));
							echo $this->Form->input('origem', array('row' => true, 'div' => 'col-md-8', 'type' => 'select', 'label' => 'Origem', 'values' => $origens));
						?>
				<?php
					}
				?>

			</div>

			<div class="col-md-6">
				<h4 class="form-titulos-admin">Imagem</h4>
				<?php
					echo $this->Form->input('Imagem.0.imagem', array('type' => 'file', 'label' => 'Imagem Destaque'));
					echo $this->Form->input('Imagem.0.destaque', array('type' => 'hidden', 'value' => true));
				?>

				<h4 class="form-titulos-admin">SEO</h4>
				<?php

					$num_de_caracteres = '<span class="pull-right" id="num_de_caracteres">Nº de caracteres: 160</span>';
					echo $this->Form->input('title', array('row' => true, 'div' => 'col-md-8', 'label' => 'Title'));
					echo $this->Form->input('meta_title', array('row' => true, 'div' => 'col-md-8', 'label' => 'Meta Title'));
					echo $this->Form->input('meta_description', array('row' => true, 'div' => 'col-md-8', 'label' => 'Meta Description','placeholder' => 'Recomendável 160 caracteres.', 'after' => $num_de_caracteres));
					// echo $this->Form->input('meta_keywords', array('row' => true, 'div' => 'col-md-8', 'label' => 'Meta Keywords', 'placeholder' => 'Palavras separadas por vírgula.'));
					echo $this->Form->input('sinonimos', array('row' => true, 'div' => 'col-md-8', 'label' => 'Sinônimos', 'placeholder' => 'Palavras separadas por vírgula.'));
				?>

				<!-- Tags -->
				<div class="row">
					<div class="col-md-8">
						<div class="panel panel-default">
							<div class="panel-heading">
								<div class="row">
									<div class="col-xs-2 col-sm-1 col-md-2">
										<label>Tags</label>
									</div>
									<div class="col-xs-10 col-sm-11 col-md-10">
										<?php
											if ($this->Permissoes->check(array('controller' => 'tags', 'action' => 'ajax_adicionar', 'admin' => true, 'plugin' => null))) {
										?>
												<div class="input-group">
													<?php echo $this->Form->input('Tag.nome', array('id' =>'NovaTag', 'type' => 'text','placeholder' => 'Nova Tag','label' => false, 'div' => false));?>
													<span class="input-group-btn">
														<?php echo $this->Form->button('<span class="glyphicon glyphicon-plus"></span>', array('id' =>'AdicionarTag','class' => 'btn btn-success', 'type' => 'button')); ?>
													</span>
												</div>
										<?php
											}
										 ?>
									</div>
								</div>
							</div>
							<div class="panel-body tags-body">
								<div class="row">
									<ul class="col-md-12 listar-tags">
										<?php
											foreach ($tags as $tag) {
										?>
												<li>
													 <input type="checkbox" name='data[TagsProduto][][tag_id]'  value='<?php echo $tag['Tag']['id'] ?>'/> <?php echo $tag['Tag']['nome'] ?>
												</li>
										<?php
											}
										 ?>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- .end Tags -->

				<!-- Serviçoes -->
				<?php if ($servicos) { ?>
						<div class="row">
							<div class="col-md-8">
								<div class="panel panel-default">
									<div class="panel-heading">
										<label>Serviços</label>
									</div>
									<div class="panel-body tags-body">
										<div class="row">
											<ul class="col-md-12 listar-servicos">
												<?php
													foreach ($servicos as $servico) {
												?>
														<li>
															 <input type="checkbox" name='data[Servico][][servico_id]'  value='<?php echo $servico['Servico']['id'] ?>'/> <?php echo $servico['Servico']['nome'] ?>
														</li>
												<?php
													}
												 ?>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
				<?php } ?>
				<!-- .end Serviços -->

			</div>
		</div>
	</div>
</div>
<hr />

<h3>Características</h3>
<div class="row">
	<div class="col-md-6">
		<h4 class="form-titulos-admin">Características Gerais</h4>
		<div class="row">
			<div id="caracteristicasGerias" class="col-md-8">
				<?php
					if (count($caracteristicas_gerais)) {

						foreach ($caracteristicas_gerais as $key => $caracteristica) {

							echo $this->Form->input('CaracteristicasValoresSelecionado.' . $caracteristica['Caracteristica']['id'] . '.caracteristica_id', array('type' => 'hidden', 'value' => $caracteristica['Caracteristica']['id']));
							echo $this->Form->input('CaracteristicasValoresSelecionado.' . $caracteristica['Caracteristica']['id'] . '.caracteristica_tipo', array('type' => 'hidden', 'value' => $caracteristica['Caracteristica']['tipo']));

							if ($caracteristica['Caracteristica']['tipo'] == 'T') {
								echo $this->Form->input('CaracteristicasValoresSelecionado.' . $caracteristica['Caracteristica']['id'] . '.descricao', array('label' => $caracteristica['Caracteristica']['descricao'], 'type' => 'text'));
							} else if ($caracteristica['Caracteristica']['tipo'] == 'TG') {
								echo $this->Form->input('CaracteristicasValoresSelecionado.' . $caracteristica['Caracteristica']['id'] . '.descricao_grande', array('label' => $caracteristica['Caracteristica']['descricao'], 'type' => 'textarea'));
							} else if ($caracteristica['Caracteristica']['tipo'] == 'L') {
								$listaValores = array();
								foreach ($caracteristica['CaracteristicasValor'] as $caracteristicaValor) {
									$listaValores[$caracteristicaValor['id']] = $caracteristicaValor['descricao'];
								}
								echo $this->Form->input('CaracteristicasValoresSelecionado.' . $caracteristica['Caracteristica']['id'] . '.caracteristicas_valor_id', array('empty' => 'Selecione', 'label' => $caracteristica['Caracteristica']['descricao'], 'options' => $listaValores));
							} else {
								if ($caracteristica['Caracteristica']['mascara'] == 'I') {
									$mascara = 'mascara-inteiro';
								} else {
									$mascara = 'mascara-decimal';
								}
								echo $this->Form->input('CaracteristicasValoresSelecionado.' . $caracteristica['Caracteristica']['id'] . '.descricao', array('class' => 'tipo-numero ' . $mascara, 'label' => $caracteristica['Caracteristica']['descricao']));
							}

							$contador = $key;
						}

						$contador++;
				?>

				<input id="contador" type="hidden" value="<?php echo $contador; ?>">

				<?php
					} else {
				?>
						<div class="controls well">
							<input id="contador" type="hidden" value="0">
							<span>Nenhuma característica geral encontrada.</span>
						</div>
				<?php
					}
				?>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<h4 class="form-titulos-admin">Características da Categoria</h4>
		<div class="row">
			<div id="caracteristicasCategoria" class="col-md-8">
				<div class="controls well">
					<p>Para cadastrar as características da categoria, primeiro selecione uma categoria.</p>
				</div>
			</div>
		</div>
	</div>
</div>

<?php
	echo $this->Form->actions();
	echo $this->Form->end();
?>