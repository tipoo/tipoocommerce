<?php
	if (count($caracteristicas_categoria)) {

		foreach ($caracteristicas_categoria as $key => $caracteristica) {

			echo $this->Form->input('CaracteristicasValoresSelecionado.' . $caracteristica['id'] . '.caracteristica_id', array('type' => 'hidden', 'value' => $caracteristica['id']));
			echo $this->Form->input('CaracteristicasValoresSelecionado.' . $caracteristica['id'] . '.caracteristica_tipo', array('type' => 'hidden', 'value' => $caracteristica['tipo']));

			if ($caracteristica['tipo'] == 'T') {
				echo $this->Form->input('CaracteristicasValoresSelecionado.' . $caracteristica['id'] . '.descricao', array('label' => $caracteristica['descricao'], 'type' => 'text'));
			} else if ($caracteristica['tipo'] == 'TG') {
				echo $this->Form->input('CaracteristicasValoresSelecionado.' . $caracteristica['id'] . '.descricao_grande', array('label' => $caracteristica['descricao'], 'type' => 'textarea'));
			} else if ($caracteristica['tipo'] == 'L') {
				$listaValores = array();
				foreach ($caracteristica['CaracteristicasValor'] as $caracteristicaValor) {
					$listaValores[$caracteristicaValor['id']] = $caracteristicaValor['descricao'];
				}
				echo $this->Form->input('CaracteristicasValoresSelecionado.' . $caracteristica['id'] . '.caracteristicas_valor_id', array('empty' => 'Selecione', 'label' => $caracteristica['descricao'], 'options' => $listaValores));
			} else {
				if ($caracteristica['mascara'] == 'I') {
					$mascara = 'mascara-inteiro';
				} else {
					$mascara = 'mascara-decimal';
				}
				echo $this->Form->input('CaracteristicasValoresSelecionado.' . $caracteristica['id'] . '.descricao', array('class' => 'tipo-numero ' . $mascara, 'label' => $caracteristica['descricao']));
			}

			$contador++;
		}
	} else {
?>
		<div class="controls well">
			<span>Nenhuma característica encontrada para esta categoria.</span>
		</div>
<?php
	}
?>