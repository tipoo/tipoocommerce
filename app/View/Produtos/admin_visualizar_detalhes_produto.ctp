<?php
	if ($produto['Produto']['descricao'] != '') {

  ?>
 		<h4><small></small>#<?php echo $produto['Produto']['id'] . ' -  ' . $produto['Produto']['descricao'] ?></h4>
<?php
	} else {
?>
    	<h4>Produto: <small class="sem-cadastro"> Esse produto não possui descrição cadastrada.</small></h4>
<?php
	}
?>
<div class="well">
	<div class="row">
	  	<div class="col-md-2">

			<?php
				$imagem = '';
				if (isset($produto['Imagem'])) {
					foreach ($produto['Imagem'] as $img) {
						if ($img['destaque']) {
							$imagem = $img['imagem'];
						}
					}
				}

			?>
			<img src="<?php echo $this->Imagens->gerarUrlImagemProduto('thumb', APP . 'webroot' . DS . 'files' . DS . PROJETO . DS . 'produtos' . DS . $imagem, $imagem) ?>" />
		</div>
		<div class="col-md-10">

			<div class="modal-detalhes-produtos">

				<?php
					if ($produto['Produto']['codigo_de_referencia'] != '') {

				?>
			 	 		<p><strong>Código de referência: </strong><?php echo $produto['Produto']['codigo_de_referencia'] ?></p>
				<?php
					} else {
				?>
			        	<p><strong>Código de referência: </strong><small class="sem-cadastro"> Esse produto não possui código de referência cadastrado.</small></p>
				<?php
					}
				?>

				<?php
					if ($produto['Categoria']['descricao'] != '') {

				?>
			 	 		<p><strong>Categoria: </strong><?php echo $produto['Categoria']['descricao'] ?></p>
				<?php
					} else {
				?>
			        	<p><strong>Categoria: </strong><small class="sem-cadastro"> Esse produto não possui categoria cadastrada.</small></p>
				<?php
					}
				?>

				<?php
					if ($produto['Slug']['url'] != '') {

				?>
			 	 		<p><strong>Url: </strong><?php echo $produto['Slug']['url'] ?></p>
				<?php
					} else {
				?>
			        	<p><strong>Url: </strong><small class="sem-cadastro"> Esse produto não possui Url cadastrada.</small></p>
				<?php
					}
				?>

				<?php
					if ($produto['Produto']['detalhes'] != '') {

				?>
			 			<p><strong>Detalhe: </strong><?php echo $produto['Produto']['detalhes'] ?></p>
				<?php
					} else {
				?>
			        	<p><strong>Detalhe: </strong><small class="sem-cadastro"> Esse produto não possui detalhes cadastrado.</small></p>
				<?php
					}
				?>

				<?php
					if ($produto['Marca']['descricao'] != '') {

				?>
			 			<p><strong>Marca: </strong><?php echo $produto['Marca']['descricao'] ?></p>
				<?php
					} else {
				?>
			        	<p><strong>Marca: </strong><small class="sem-cadastro"> Esse produto não possui marca cadastrada.</small></p>
				<?php
					}
				?>
			</div>
		</div>
	</div>
</div>
<h4>Preço</h4>
<div class="well">
		<?php
			if ($produto['Produto']['preco_de'] != '') {

		?>
	 			<p><strong>De: </strong><?php echo $this->Formatacao->moeda($produto['Produto']['preco_de']) ?></p>
		<?php
			} else {
		?>
	        	<p><strong>De: </strong><small class="sem-cadastro"> Esse produto não possui preço "de" cadastrado.</small></p>
		<?php
			}
		?>

		<?php
			if ($produto['Produto']['preco_por'] != '') {

		?>
				<p><strong>Por: </strong><?php echo $this->Formatacao->moeda($produto['Produto']['preco_por']) ?></p>
		<?php
			} else {
		?>
	        	<p><strong>Por: </strong><small class="sem-cadastro"> Esse produto não possui preço "por" cadastrado.</small></p>
		<?php
			}
		?>
</div>

<h4>SEO</h4>
<div class="well">
	<?php
		if ($produto['Produto']['meta_description'] != '') {

	  ?>
 	 		<p><strong>Meta Description </strong></p>
 	 		<p><?php echo $produto['Produto']['meta_description'] ?></p>
	<?php
		} else {
	?>
        	<p><strong>Meta Description </strong></p>
        	<p><small class="sem-cadastro"> Esse produto não possui Meta Description cadastrado.</small></p>
	<?php
		}
	?>

	<?php
		if ($produto['Produto']['meta_keywords'] != '') {

	  ?>
			<p><strong>Meta Keywords</strong></p>
			<p><?php echo $produto['Produto']['meta_keywords'] ?></p>
	<?php
		} else {
	?>
        	<p><strong>Meta Keywords</strong></p>
        	<p><small class="sem-cadastro">Esse produto não possui Meta Keywords cadastrado.</small></p>
	<?php
		}
	?>

	<?php
		if ($produto['Produto']['sinonimos'] != '') {

	  ?>
			<p><strong>Sinônimos</strong></p>
			<p><?php echo $produto['Produto']['sinonimos'] ?></p>
	<?php
		} else {
	?>
        	<p><strong>Sinônimos</strong></p>
        	<p><small class="sem-cadastro">Esse produto não possui sinônimos cadastrados.</small></p>
	<?php
		}
	?>
</div>