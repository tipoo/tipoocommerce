<?php
	$this->Html->script(array('hogan-2.0.0.js', 'jquery.price_format.2.0.min', 'jquery.numeric.js', 'typeahead.min', 'Views/Produtos/admin_compre_junto'), array('inline' => false));
	$this->Html->css(array('typeahead.js-bootstrap'), 'stylesheet', array('inline' => false));
?>

<ol class="breadcrumb">
	<li ><?php echo $this->Html->link('Produtos', array('action' => 'backToPaginatorIndex', 'admin' => true)); ?></li>
	<li class="active">Compre Junto</li>
</ol>

<div class="alert alert-info">
	<i class="glyphicon glyphicon-info-sign"></i> <strong>Sobre o Compre Junto</strong>:<br><br>
	<ul>
		<li><strong>Desconto</strong>: é aplicado somente no produto principal;</li>
		<li><strong>Ordem do desconto</strong>: será de acordo com o valor do produto principal, prevalece o desconto que mais beneficiará o cliente;</li>
		<li><strong>Desconto cumulativo</strong>: quanto maior o número de produtos associados que o cliente comprar, maior será o desconto.</li>
	</ul>
</div>
<div class="alert alert-warning">
	<i class="glyphicon glyphicon-exclamation-sign"></i> <strong>Atenção!</strong> Os produtos e descontos são salvos automaticamente assim que adicionados.
</div>

<div class="row">
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-6">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Produtos</h3>
					</div>
					<div class="panel-body">
						<span class="text-muted">Adicione aqui os produtos que ganharão desconto se forem comprados junto com o produto</span >
							 <strong>"<i><?php echo $produto['Produto']['descricao']?></i>
							 	<small> 
							 		<?php 
							 			if ($produto['Produto']['codigo_de_referencia'] != '') {
						 			?>
							 				(cód. <?php echo $produto['Produto']['codigo_de_referencia']?>)
						 			<?php
							 			} 
							 		?>
							 	</small>"</strong>
						 <span class="text-muted">.</span >
						 <br>
						 <br>
						<?php
							$after = '<span class="lembrete-typeahead">*Apenas produtos ativos</span>';
							echo $this->Form->input('AfinidadeProduto.produto_principal_id', array('type' => 'hidden', 'value' => $produto['Produto']['id']));
							echo $this->Form->input('AfinidadeProduto.descricao', array('row' => true, 'div' => 'col-md-12', 'class' => 'typeahead', 'autocomplete' => 'off', 'label' => 'Adicionar produto', 'placeholder' => 'Digite o nome do produto', 'after' => $after));
						?>

						<div class="produtos-selecionados">
						<?php 
							foreach ($produtos_associados as $produto_associado) {

								$inativo = '';
								if (!$produto_associado['Produto']['ativo']) {
									$inativo = '<small><span class="label label-danger">Inativo</span></small>';
								}

								$produto_ = $produto_associado['Produto']['descricao'] . ' - ' . $produto_associado['Produto']['Marca']['descricao'] . ' - #' . $produto_associado['Produto']['id'];
						?>
								<p><?php echo $produto_ ?> <?php echo $inativo ?> <button value="<?php echo $produto_associado['AfinidadeProduto']['id']?>" type="button" class="close excluir-produto" data-dismiss="alert">&times;</button></p>

						<?php
							}
						?>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-6">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Descontos</h3>
					</div>
					<div class="panel-body">
						<span class="text-muted">Adicione aqui os descontos referentes ao número de produtos comprados no <strong>"<i>Compre Junto</i>"</strong>.</span>
						<br>
						<br>
						<div class="row">
							<?php
								echo $this->Form->input('CompreJuntoPromocao.tipo_valor', array('div' => 'col-md-6', 'type' => 'select', 'options' => array('M' => '(R$) Em reais', 'P' => '(%) Percentual'), 'label' => 'Desconto'));
							?>

							<div class="col-md-6">
								<label>Valor</label>
								<div class="input-group">
									<?php echo $this->Form->input('CompreJuntoPromocao.valor', array('type' => 'text' ,'label' => false, 'div' => false)); ?>
									<span class="input-group-btn">
										<?php echo $this->Form->button('<span class="glyphicon glyphicon-plus"></span>', array('id' =>'AdicionarValorDesconto','class' => 'btn btn-success', 'type' => 'button', 'data-placement' => 'top', 'rel' => 'tooltip', 'title' => 'Adicionar')); ?>
									</span>
								</div>
							</div>
						</div>
						<div class="descontos-selecionados">
						<?php 
							foreach ($descontos_associados as $desconto_associado) {

								if ($desconto_associado['CompreJuntoPromocao']['tipo_valor'] == 'P') {

									$desconto_ = intval($desconto_associado['CompreJuntoPromocao']['valor']) . ' %';

								} else {

									$desconto_ = $this->Formatacao->moeda($desconto_associado['CompreJuntoPromocao']['valor']);

								}

						?>
								<p><?php echo $desconto_ ?><button value="<?php echo $desconto_associado['CompreJuntoPromocao']['id']?>" type="button" class="close excluir-desconto" data-dismiss="alert">&times;</button></p>

						<?php
							}
						?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
