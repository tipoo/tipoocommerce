<?php
App::uses('AppHelper', 'Helper');

class CatalogoHelper extends AppHelper {

	private $produtos;
	private $buscaId;
	private $totalProdutos;
	private $layout;

	private $produto;
	private $produtoImagens;

	public $helpers = array('Html', 'Form');

	public function __construct(View $View, $settings = array()) {
		parent::__construct($View, $settings);
		$this->produtos = $View->getVar('controleProdutosBuscas');
		$this->buscaId = $View->getVar('controleProdutosBuscaId');
		$this->totalProdutos = $View->getVar('controleProdutosTotal');
		$this->produto = $View->getVar('controleProduto');
		$this->produtoImagens = $View->getVar('controleProdutoImagens');
		$this->layout = $View->layout;
	}

}
?>