<?php
App::uses('AppHelper', 'Helper');

class ImagensHelper extends AppHelper {

	public $helpers = array('Html', 'Form');

	public function gerarUrlImagemProduto($tamanho = null, $caminho_imagem = null, $imagem) {

		if (file_exists($caminho_imagem) && $imagem) {
			return Router::url('/files/' . PROJETO . '/produtos/' . $tamanho . '_' . $imagem, true);
		} else {
			return Router::url('/img/' . $tamanho . '_padrao.jpg', true);
		}
	}
}