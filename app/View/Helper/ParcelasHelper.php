<?php
App::uses('AppHelper', 'Helper');

class ParcelasHelper extends AppHelper {

	public $helpers = array('Html', 'Form');

	public function gerarListaParcerlas($parcelas = array(), $total = 0) {
		$retorno = array();

		foreach ($parcelas as $parcela) {
			if ($total) {
				$valor_parcela = ceil(($total / $parcela['parcelas']) * 100) / 100;
				$valor_parcela = number_format($valor_parcela, 2, ',', '.');
				$retorno[$parcela['id']] = $parcela['parcelas'].'X (R$'.$valor_parcela.')';
			} else {
				$retorno[$parcela['id']] = $parcela['parcelas'].'X';
			}
		}

		return $retorno;
	}
}