<?php
App::uses('AppHelper', 'Helper');

class CarrinhoHelper extends AppHelper {

	// retorno um array com skus em pré-venda ou FALSE caso não tenha nenhum sku em pré-venda.
	public function skus_em_pre_venda() {
		$skus = $this->_View->viewVars['skus'];

		$skus_em_pre_venda = array();

		foreach ($skus as $sku) {
			if ($sku['pre_venda']) {
				$skus_em_pre_venda[] = $sku;
			}
		}

		if (count($skus_em_pre_venda)) {
			return $skus_em_pre_venda;
		} else {
			return false;
		}
	}

	// retorna a maior data de entrega prevista para todos os produtos em pre venda no carrinho
	// pode ser passada a previsão de entrega calculada para o cep especificado, para que esta também seja comparada
	public function data_entrega_pre_venda($previsao_entrega = '0000-00-00') {
		$skus = $this->_View->viewVars['skus'];

		foreach ($skus as $sku) {
			if ($sku['pre_venda']) {
				if ($sku['Sku']['previsao_entrega_pre_venda'] > $previsao_entrega) {
					$previsao_entrega = $sku['Sku']['previsao_entrega_pre_venda'];
				}
			}
		}

		return $previsao_entrega;
	}

}
?>