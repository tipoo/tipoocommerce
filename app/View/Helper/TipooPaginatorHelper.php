<?php
App::uses('PaginatorHelper', 'View/Helper');

class TipooPaginatorHelper extends PaginatorHelper {

	public function getValor($chave = 'pages', $model = null) {
		$valores = $this->counter(array('format' => '{:page},{:pages},{:current},{:count},{:start},{:end},{:model}'));
		$valores = explode(',', $valores);
		$mapaValores = array(
			'page' => (int) $valores[0],
			'pages' => (int) $valores[1],
			'current' => (int) $valores[2],
			'count' => (int) $valores[3],
			'start' => (int) $valores[4],
			'end' => (int) $valores[5],
			'model' => $valores[6]
		);

		return $mapaValores[$chave];
	}

}

?>