<?php

App::uses('AppHelper', 'Helper');

class ControlesHelper extends AppHelper {

	public $helpers = array('MenuCategorias', 'Catalogo');

	public function getMenuCategoriasConfig() {
		return $this->MenuCategorias->getMenuCategoriasConfig();
	}

	public function getMenuCategorias($class) {
		return $this->MenuCategorias->getMenuCategorias($class);
	}

	public function getProdutosBuscas($opts = array()) {
		return $this->Catalogo->getProdutosBuscas($opts);
	}

	public function getProdutosBuscasOrdenacao() {
		return $this->Catalogo->getProdutosBuscasOrdenacao();
	}

	public function getProdutosBuscasQtd() {
		return $this->Catalogo->getProdutosBuscasQtd();
	}

	public function getFormBusca() {
		return $this->Catalogo->getFormBusca();
	}

}
?>