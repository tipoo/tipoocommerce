<?php
App::uses('AppHelper', 'Helper');

class BannersHelper extends AppHelper {

	public $helpers = array('Html', 'Form');

	public function gerarHtmlBanners($banner, $attr) {

		$limite_banner = $banner['BannerTipo']['qtd'];
		$i = 0;

		if ($attr['tipo'] == 'slide') {

			$this->Html->script(array('jquery.scrollTo-min', 'tipoo.banner-scroll'), array('inline' => false));

			$html = '<div id="banner-scroll" class="banner-scroll">';
			$html .=	'<div class="banner-container">';
			$html .=		'<ul class="banner-content">';

			$nav = '';
			foreach ($banner['Banner'] as $attr['chave'] => $imagem) {

				if ($limite_banner != '') {
					if ($i == $limite_banner) {
						break;
					} else {
						$i ++;
					}
				}

				$target = '';
				if($imagem['nova_aba']){
					$target = '_blank';
				}


				$html .= '<li class="banner banner-' .  $attr['chave'] . '">';
				$html .= 	'<div class="banner-inner">';

				if ($imagem['link'] != '') {
					$html .= 	'<a href="' . $imagem['link'] . '" target="' . $target . '">';
				} else {
					$html .= 	'<a target="' . $target . '">';
				}
				$html .= 			'<img src="' . $this->webroot . 'files/' . PROJETO . '/banners/' . $imagem['imagem'] . '" alt="' . $imagem['descricao'] . '" />';
				$html .= 		'</a>';
				$html .= 		'<div class="banner-info">';

				if($imagem['label'] != ''){

					$label_banner = $imagem['label'];

					$html .= 		'<h2 class="caption">';
					$html .= 			'<a>' . $label_banner;
					$html .= 			'</a>';
					$html .= 		'</h2>';

				}

				if($imagem['texto'] != ''){

					$texto_banner = $imagem['texto'];

					$html .= 		'<a class="descricao">';
					$html .= 			'<span>' . $texto_banner;
					$html .= 			'</span>';
					$html .= 		'</a>';

				}
				$html .= 		'</div>';
				$html .= 	'</div>';
				$html .= '</li>';

				if ($attr['chave'] == 0) {
					$nav .= '<li><a href="#" class="abas aba-banner-atual"  rel="' . ($attr['chave'] + 1) . '"><span>1</span></a></li>';
				} else {
					$nav .= '<li><a href="#" class="abas"  rel="' . ($attr['chave'] + 1) . '"><span>' . ($attr['chave'] + 1) . '</span></a></li>';
				}

			}

			$html .=		'</ul>';

			$html .=	'</div>';

			$html .= 	'<div class="banner-nav">';
			$html .=		'<ul>';
			$html .=			$nav;
			$html .=		'</ul>';
			$html .=	'</div>';

			$html .= 	'<div class="banner-arrow prev">';
			$html .=		'<a href="#" class="arrow prev" rel="-"><span>Anterior</span></a>';
			$html .=	'</div>';

			$html .= 	'<div class="banner-arrow next">';
			$html .=		'<a href="#" class="arrow next" rel="+"><span>Próximo</span></a>';
			$html .=	'</div>';

			$html .= '</div>';


		} else if ($attr['tipo'] == 'lista') {

			$html  = 	'<ul id="' . $attr['chave'] . '" class="banner">';

			foreach ($banner['Banner'] as $attr['chave'] => $imagem) {

				if ($limite_banner != '') {
					if ($i == $limite_banner) {
						break;
					} else {
						$i ++;
					}
				}

				$target = '';
				if($imagem['nova_aba']){
					$target = '_blank';
				}


				$html .= 	'<li>';
				$html .= 		'<div class="banner">';

				if ($imagem['link'] != '') {
					$html .= 		'<a href="' . $imagem['link'] . '" target="' . $target . '">';
				} else {
					$html .= 		'<a target="' . $target . '">';
				}
				$html .= 				'<img src="' . $this->webroot . 'files/' . PROJETO . '/banners/' . $imagem['imagem'] . '" alt="' . $imagem['descricao'] . '"  />';
				$html .= 			'</a>';
				$html .= 			'<div class="banner-info">';

				if($imagem['label'] != ''){

					$label_banner = $imagem['label'];

					$html .= 			'<h2 class="caption">';
					$html .= 				'<a>' . $label_banner;
					$html .= 				'</a>';
					$html .= 			'</h2>';

				}

				if($imagem['texto'] != ''){

					$texto_banner = $imagem['texto'];

					$html .= 			'<a class="descricao">';
					$html .= 				'<span>' . $texto_banner;
					$html .= 				'</span>';
					$html .= 			'</a>';

				}
				$html .= 			'</div>';
				$html .= 		'</div>';
				$html .= 	'</li>';
			}

			$html .= 	'</ul>';

		} else if ($attr['tipo'] == 'unico') {
			$html   = '<div id="' . $attr['chave'] . '">';

			foreach ($banner['Banner'] as $attr['chave'] => $imagem) {

				if ($limite_banner != '') {
					if ($i == $limite_banner) {
						break;
					} else {
						$i ++;
					}
				}

				$target = '';
				if($imagem['nova_aba']){
					$target = '_blank';
				}

				$html .= 	'<div class="banner">';

				if ($imagem['link'] != '') {
					$html .= 	'<a href="' . $imagem['link'] . '" target="' . $target . '">';
				} else {
					$html .= 	'<a target="' . $target . '">';
				}
				$html .= 			'<img src="' . $this->webroot . 'files/' . PROJETO . '/banners/' . $imagem['imagem'] . '" alt="' . $imagem['descricao'] . '" target="' . $target . '" />';
				$html .= 		'</a>';
				$html .= 		'<div class="banner-info">';

				if($imagem['label'] != ''){

					$label_banner = $imagem['label'];

					$html .= 		'<h2 class="caption">';
					$html .= 			'<a>' . $label_banner;
					$html .= 			'</a>';
					$html .= 		'</h2>';

				}

				if($imagem['texto'] != ''){

					$texto_banner = $imagem['texto'];

					$html .= 		'<a class="descricao">';
					$html .= 			'<span>' . $texto_banner;
					$html .= 			'</span>';
					$html .= 		'</a>';

				}
				$html .= 		'</div>';
				$html .= 	'</div>';

			}

			$html .= '</div>';
		}

		return $html;
	}
}

?>