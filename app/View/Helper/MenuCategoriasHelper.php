<?php
App::uses('AppHelper', 'Helper');

class MenuCategoriasHelper extends AppHelper {

	public $helpers = array('Html');

	private $categorias;

	public function __construct(View $View, $settings = array()) {
		parent::__construct($View, $settings);
		$this->categorias = $View->getVar('controlesMenuCategorias');
		$this->categoria = $View->getVar('controlesMenuCategoria'); // Categoria atual do site
	}

	public function getMenuCategoriasConfig() {
		$config = $this->Html->css(array('superfish/css/superfish', 'superfish_custom'), 'stylesheet');
		$config .= $this->Html->script(array('superfish/hoverIntent', 'superfish/superfish'));
		$config .= $this->Html->scriptBlock('$(document).ready(function() {$("ul.sf-menu").superfish();});');

		return $config;
	}

	public function getMenuCategorias($class = '') {
		if (count($this->categorias)) {
			return $this->gerar($this->categorias, $class);
		} else {
			return '<!-- Menu de categorias vazio -->';
		}
	}

	public function getMenuCategoria($class) {
		if ($this->categoria) {
			$html = '<h2>'.$this->categoria['Categoria']['descricao'].'</h2>';
			$html .= $this->gerar($this->categoria['children'], $class, '', 0, true);
			return $html;
		} else {
			return '<!-- Menu de categoria vazio -->';
		}
	}

	// Metodo recursivo que monta o menu propriamente dito
	private function gerar($categorias, $class = '', $html = '', $nivel = 0, $incluiHeader = false) {
		if ($nivel == 0) {
			$html .= '<ul class="controle-menu-categorias nivel-'.$nivel.' '.$class.'">';
		} else {
			$html .= '<ul class="controle-menu-categorias nivel-'.$nivel.'">';
		}

		$nivelHeader = $nivel + 3;
		if ($nivelHeader > 6) {
			$nivelHeader = 6;
		}

		foreach ($categorias as $categoria) {
			$html .= '<li>';

			if ($incluiHeader) {
				$html .= '<h'.$nivelHeader.'><a href="'.Router::url($categoria['Slug']['url']).'">'.$categoria['Categoria']['descricao'].'</a></h'.$nivelHeader.'>';
			} else {
				$html .= '<a href="'.Router::url($categoria['Slug']['url']).'">'.$categoria['Categoria']['descricao'].'</a>';
			}

			if (count($categoria['children'])) {
				$html = $this->gerar($categoria['children'], '', $html, ($nivel + 1), $incluiHeader);
			}

			$html .= '</li>';
		}

		$html .= '</ul>';
		return $html;
	}

	/*
		$options
			empty
			id_selecionado
			selecao_produto
			todos os demais itens serão adicionados como parâmetros html do select, por exemplo: name, id, class data-..., etc.

	*/
	public function comboCategorias($categorias, $options = array()) {
		$options_padrao = array(
			'id_selecionado' => false,
			'selecao_produto' => false, // Desabilita para a seleção todas as categorias que tiverem filhos, ou seja, que não são de ultimo nível.
			'empty' => false //
		);
		$options = array_merge($options_padrao, $options);

		$html = '<select';

		foreach ($options as $key => $value) {
			if (!array_key_exists($key, $options_padrao)) {
				$html .= ' '.$key.'="'.$value.'"';
			}
		}

		$html .='>';

		if ($options['empty'] !== false) {
			$html .= '<option value="">'.$options['empty'].'</option>';
		}

		$html .= $this->comboCategoriasOptions($categorias, 0, $options['id_selecionado'], $options['selecao_produto']);

		$html .='</select>';

		return $html;
	}

	private function comboCategoriasOptions($categorias, $nivel, $id_selecionado, $selecao_produto) {
		$html = '';

		foreach ($categorias as $categoria) {
			$descricao = '';
			$descricao = str_pad($descricao, $nivel * 3 * strlen("&nbsp;"), "&nbsp;", STR_PAD_LEFT);
			/*if ($descricao) {
				$descricao = $descricao.'&#x251c;&#x2500;';
			}*/
			$descricao = $descricao.$categoria['Categoria']['descricao'];

			$html .= '<option value="'.$categoria['Categoria']['id'].'"';

			if ($categoria['Categoria']['descendente'] == 'C' && $selecao_produto) {
				$html .= ' disabled';
			}

			if ($id_selecionado !== false && $id_selecionado == $categoria['Categoria']['id']) {
				$html .= ' selected';
			}

			$html .= '>'.$descricao.'</option>';

			if (count($categoria['children'])) {
				$html .= $this->comboCategoriasOptions($categoria['children'], $nivel + 1, $id_selecionado, $selecao_produto);
			}


		}

		return $html;
	}

}
?>