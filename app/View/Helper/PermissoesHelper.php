<?php

App::uses('AppHelper', 'Helper');

class PermissoesHelper extends AppHelper {

	private $permissoesSistema;
	private $usuarioAutenticado;

	public function __construct(View $View, $settings = array()) {
		parent::__construct($View, $settings);
		$this->permissoesSistema = $this->_View->getVar('permissoesSistema');
		$this->usuarioAutenticado = $this->_View->getVar('usuarioAutenticado');
	}

	public function check(array $url) {
		$plugin = isset($url['plugin']) ? $url['plugin'] : null;
		$controller = isset($url['controller']) ? $url['controller'] : 'paginas';
		$action = isset($url['action']) ? $url['action'] : 'index';

		if (isset($url['admin']) && $url['admin']) {
			$action = 'admin_'.$action;
		}

		if (isset($this->permissoesSistema[$plugin]) &&
				isset($this->permissoesSistema[$plugin][$controller]) &&
				isset($this->permissoesSistema[$plugin][$controller][$action])) {

			return true;
		} else {
			return false;
		}
	}

	public function checkGroupOr() {
		// OR - Se pelo menos uma das urls validar, já retorna true
		$urls = func_get_args();

		foreach ($urls as $url) {
			if ($this->check($url)) {
				return true;
			}
		}

		return false;
	}

	public function checkGroupAnd() {
		// AND - Se pelo menos uma das urls não validar, já retorna false
		$urls = func_get_args();

		foreach ($urls as $url) {
			if (!$this->check($url)) {
				return false;
			}
		}

		return true;
	}
}

?>
