<?php
App::uses('AppHelper', 'Helper');

class ConfiguracoesHelper extends AppHelper {

	private $configuracao;

	public $helpers = array('Html', 'Form');

	public function __construct(View $View, $settings = array()) {
		parent::__construct($View, $settings);
		$this->configuracao = $View->getVar('db_config');
	}

	public function get($chave) {
		return $this->configuracao['Configuracao'][$chave];
	}
}
?>