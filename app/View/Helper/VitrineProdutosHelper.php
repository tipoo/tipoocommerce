<?php
App::uses('AppHelper', 'Helper');

class VitrineProdutosHelper extends AppHelper {

	public $helpers = array('Html', 'Form');

	public function gerarHtmlVitrineProduto($content, $attr, $produto) {

		$html_content = $content;

		$html_content = str_replace('#{produto_id}', $produto['Produto']['id'], $html_content);
		$html_content = str_replace('#{nome_produto}', $produto['Produto']['descricao'], $html_content);

		$avaliacao_form_content = '<div class="avaliacao-form">';

			$avaliacao_form_content .= $this->Form->create('Avaliacao', array('class' => 't-avaliacao-produto-form'));
			$avaliacao_form_content .= $this->Form->input('produto_id', array('type' => 'hidden', 'value' => $produto['Produto']['id']));
			$avaliacao_form_content .= $this->Form->input('nome', array('class' => 'avaliacao-nome', 'label' => 'Nome*: ', 'placeholder' => 'Digite seu nome'));
			$avaliacao_form_content .= $this->Form->input('email', array('class' => 'avaliacao-email', 'label' => 'E-mail*: ', 'placeholder' => 'Digite seu email'));
			$avaliacao_form_content .= '<div class="avaliacao">';
			$avaliacao_form_content .= 	'<label>Avaliação</label>';
			$avaliacao_form_content .= 	'<div class="star"></div>';
			$avaliacao_form_content .= '</div>';
			$avaliacao_form_content .= $this->Form->submit('Avaliar');
			$avaliacao_form_content .= $this->Form->end();

		$avaliacao_form_content .= '</div>';

		$html_content = str_replace('#{avaliacaoForm}', $avaliacao_form_content, $html_content);

		$avaliacao_media_content = '<div class="avaliacao-media">';

			if ($produto['Produto']['avaliacao'] == '') {
				$media_avaliacao = 0;

			} else {
				$media = $produto['Produto']['avaliacao'];
				$decimais = explode('.', $media);

				if ($decimais[1] >= 50) {
					$media_avaliacao = ($decimais[0] + 1) * 10;
				} else {
					$media_avaliacao = $decimais[0] * 10;
				}
			}

			$avaliacao_media_content .= '<div class="avaliacao">';
			$avaliacao_media_content .= 	'<label>Avaliação dos Consumidores</label>';
			$avaliacao_media_content .= 	'<span class="media-avaliacao media-' . $media_avaliacao . '" data-produto="' . $produto['Produto']['id'] . '"></span>';
			$avaliacao_media_content .= '</div>';

		$avaliacao_media_content .= '</div>';

		$html_content = str_replace('#{avaliacaoMedia}', $avaliacao_media_content, $html_content);

		$html_content = str_replace('#{link_produto}', $this->Html->link($produto['Produto']['descricao'], $produto['Slug']['url']), $html_content);
		$html_content = str_replace('#{url_produto}', Router::url($produto['Slug']['url'], true), $html_content);
		$html_content = str_replace('#{descricao_resumida}', $produto['Produto']['descricao_resumida'], $html_content);

		$html_content = str_replace('#{img}', $this->Html->link('<img alt="' . $produto['Produto']['descricao'] . '" title="' . $produto['Produto']['descricao'] . '" src="' . $this->webroot . 'files/' . PROJETO . '/produtos/' . $attr['tamanhoImagem'] . '_' . $produto['Imagem'][0]['imagem'] . '">', $produto['Slug']['url'], array('escape' => false)), $html_content);

		if (strrpos($html_content, '#{galeria_img}') !== false) {
			$galeria_content = '<ul>';
			foreach ($produto['Imagem'] as $key => $imagem) {
				$galeria_content .= '<li><a href="' . $this->webroot . 'files/' . PROJETO . '/produtos/' . $attr['tamanhoImagem'] . '_' . $imagem['imagem'] . '" rel="' . $key . '"><span>' . $key . '</span></a></li>';
			}
			$galeria_content .= '</ul>';

			$html_content = str_replace('#{galeria_img}', $galeria_content, $html_content);
		}

		$html_content = str_replace('#{sku_destaque_id}', $produto['Sku'][0]['id'], $html_content);
		$html_content = str_replace('#{nome_sku_destaque}', $produto['Sku'][0]['descricao'], $html_content);

		$html_content = str_replace('#{marca}', $produto['Marca']['descricao'], $html_content);
		$html_content = str_replace('#{categoria}', $produto['Categoria']['descricao'], $html_content);

		// Preço de
		if (strrpos($html_content, '#{if:preco_de}') !== false) {
			$preco_de_content = explode('#{if:preco_de}', $html_content);
			$preco_de_content = explode('#{end:preco_de}', $preco_de_content[1]);
			$preco_de_base = $preco_de_content = $preco_de_content[0];

			if ($produto['Produto']['preco_de'] != '' || $produto['Produto']['preco_por'] != $produto['Produto']['preco_por_original']) {
				if ($produto['Produto']['preco_de'] != '') {
					$preco_de = number_format($produto['Produto']['preco_de'], 2, ',', '.');
				} else {
					$preco_de = number_format($produto['Produto']['preco_por_original'], 2, ',', '.');
				}

				$preco_de_content = str_replace('#{preco_de}', $preco_de, $preco_de_content);

				$html_content = str_replace($preco_de_base, $preco_de_content, $html_content);
			} else {
				$html_content = str_replace($preco_de_base, '', $html_content);
			}

			$html_content = str_replace('#{if:preco_de}', '', $html_content);
			$html_content = str_replace('#{end:preco_de}', '', $html_content);
		}

		$html_content = str_replace('#{preco_por}', number_format($produto['Produto']['preco_por'], 2, ',', '.'), $html_content);

		// Parcelamento
		if (strrpos($html_content, '#{if:parcelamento}') !== false) {

			$parcelamento_content = explode('#{if:parcelamento}', $html_content);
			$parcelamento_content = explode('#{end:parcelamento}', $parcelamento_content[1]);
			$parcelamento_base = $parcelamento_content = $parcelamento_content[0];

			if ($produto['Produto']['parcelas'] >= $attr['quantidadeMininaParcelas']) {
				$parcelas = $produto['Produto']['parcelas'];
				$valor_parcelas = number_format($produto['Produto']['valor_parcelas'], 2, ',', '.');
				$parcelamento_content = str_replace('#{parcelas}', $parcelas, $parcelamento_content);
				$parcelamento_content = str_replace('#{valorParcelas}', $valor_parcelas, $parcelamento_content);

				$html_content = str_replace($parcelamento_base, $parcelamento_content, $html_content);
			} else {
				$html_content = str_replace($parcelamento_base, '', $html_content);
			}

			$html_content = str_replace('#{if:parcelamento}', '', $html_content);
			$html_content = str_replace('#{end:parcelamento}', '', $html_content);
		}

		// Valor Economizado
		if (strrpos($html_content, '#{if:economize}') !== false) {
			$economize_content = explode('#{if:economize}', $html_content);
			$economize_content = explode('#{end:economize}', $economize_content[1]);
			$economize_base = $economize_content = $economize_content[0];

			if ($produto['Produto']['economize'] > 0) {
				$economize = number_format($produto['Produto']['economize'], 2, ',', '.');
				$economize_content = str_replace('#{economize}', $economize, $economize_content);

				$html_content = str_replace($economize_base, $economize_content, $html_content);
			} else {
				$html_content = str_replace($economize_base, '', $html_content);
			}

			$html_content = str_replace('#{if:economize}', '', $html_content);
			$html_content = str_replace('#{end:economize}', '', $html_content);
		}

		$selos_content = '<div class="selos">';

		if (isset($produto['Promocao']['melhor_desconto_produto'])) {
			// Melhor promoção de desconto no produto.
			if ($produto['Promocao']['melhor_desconto_produto']['promocao']['Promocao']['selo']) {
				$selos_content .= '<span class="selo selo-desconto-produto selo-' . strtolower(Inflector::slug($produto['Promocao']['melhor_desconto_produto']['promocao']['Promocao']['descricao'], '-')) . '-' . $produto['Promocao']['melhor_desconto_produto']['promocao']['Promocao']['id'] . '">' . $produto['Promocao']['melhor_desconto_produto']['promocao']['Promocao']['descricao'] . '</span>';
			}
		}

		if (isset($produto['Promocao']['leve_x_pague_y']) && count($produto['Promocao']['leve_x_pague_y']) > 0) {
			// Promoção leve X e pague Y, mostra todas as promoção e não verifica qual a melhor promoção.
			foreach ($produto['Promocao']['leve_x_pague_y'] as $promocao) {
				if ($promocao['Promocao']['selo']) {
					$selos_content .= '<span class="selo selo-leve-x-pague-y selo-' . strtolower(Inflector::slug($promocao['Promocao']['descricao'], '-')) . '-' . $promocao['Promocao']['id'] . '">' . $promocao['Promocao']['descricao'] . '</span>';
				}
			}
		}

		if (isset($produto['Promocao']['desconto_frete']) && count($produto['Promocao']['desconto_frete']) > 0) {
			// Promoção desconto no frete, mostra todas as promoção e não verifica qual a melhor promoção.
			foreach ($produto['Promocao']['desconto_frete'] as $promocao) {
				if ($promocao['Promocao']['selo']) {
					$selos_content .= '<span class="selo selo-desconto-frete selo-' . strtolower(Inflector::slug($promocao['Promocao']['descricao'], '-')) . '-' . $promocao['Promocao']['id'] . '">' . $promocao['Promocao']['descricao'] . '</span>';
				}
			}
		}

		$selos_content .= '</div>';

		$html_content = str_replace('#{selos}', $selos_content, $html_content);

		$tag_link = $attr['tagsUrl'];

		$tags_content = '<div class="tags">';
		if (isset($produto['TagsProduto'][0])) {
			foreach ($produto['TagsProduto'] as $tag) {

				if ($tag_link === true) {

					$tags_content  .= '<a href="' . Router::url($tag['Tag']['Slug']['url']) . '" class="tag tag-' . $tag['Tag']['nome'] . '-' . $tag['Tag']['id'] . '"> ' . $tag['Tag']['nome'] . '</a>';
				} else {

					$tags_content  .= '<span class="tag tag-' . $tag['Tag']['nome'] . '-' . $tag['Tag']['id'] . '"> ' . $tag['Tag']['nome'] . '</span>';
				}
				//$tags_content .= '<a href="' . Router::url($tag['Tag']['Slug']['url']) . '" class="tag-' . $tag['Tag']['nome'] . '-' . $tag['Tag']['id'] . '"> ' . $tag['Tag']['nome'] . '</a>';
			}
		}
		$tags_content .= '</div>';

		$html_content = str_replace('#{tags}', $tags_content, $html_content);

		// Modelos
		if (strrpos($html_content, '#{if:modelos}') !== false) {

			$modelos_content = explode('#{if:modelos}', $html_content);
			$modelos_content = explode('#{end:modelos}', $modelos_content[1]);
			$modelos_base = $modelos_content = $modelos_content[0];

			$class_hide = '';
			if (count($produto['Sku']) == 1) {
				$class_hide = ' hide'; // deixar o espaço ali para não dar problema ao concatenar com as outras classes
			}

			$modelos_html_content = '<select class="sku t-skus-selecao sku-selecionado' . $class_hide . '" data-tipo="select">';

			// Se existir mais de um sku o cliente deve escolher o model, caso contrário o modelo já vem pré-selecionado
			$class_hide = '';
			if (count($produto['Sku']) > 1) {
				$modelos_html_content .= '<option value="">Selecione o modelo</option>';
				$class_hide = 'hide';
			}

			foreach ($produto['Sku'] as $sku) {
				$disabled = '';
				if ($sku['situacao_estoque'] == 'AviseMe') {
					$disabled = 'disabled';
				}

				$modelos_html_content .= '<option data-situacao-estoque="' . $sku['situacao_estoque'] . '" value="' . $sku['id'] . '" ' . $disabled . '>' . $sku['descricao'] . '</option>';
			}
			$modelos_html_content .= '</select>';

			$modelos_content = str_replace('#{modelos}', $modelos_html_content, $modelos_content);
			$html_content = str_replace($modelos_base, $modelos_content, $html_content);

			$html_content = str_replace('#{if:modelos}', '', $html_content);
			$html_content = str_replace('#{end:modelos}', '', $html_content);
		}

		// Estoque
		if (!$produto['Produto']['esgotado']) {
			if (strrpos($html_content, '#{if:estoque}') !== false) {
				$html_content = str_replace('#{if:estoque}', '', $html_content);
				$html_content = str_replace('#{end:estoque}', '', $html_content);
			}

			if (strrpos($html_content, '#{if:not:estoque}') !== false) {
				$esgotado_content = explode('#{if:not:estoque}', $html_content);
				$esgotado_content = explode('#{end:not:estoque}', $esgotado_content[1]);
				$esgotado_base = $esgotado_content = $esgotado_content[0];

				$html_content = str_replace($esgotado_base, '', $html_content);
				$html_content = str_replace('#{if:not:estoque}', '', $html_content);
				$html_content = str_replace('#{end:not:estoque}', '', $html_content);
			}
		} else {
			if (strrpos($html_content, '#{if:not:estoque}') !== false) {
				$html_content = str_replace('#{if:not:estoque}', '', $html_content);
				$html_content = str_replace('#{end:not:estoque}', '', $html_content);
			}

			if (strrpos($html_content, '#{if:estoque}') !== false) {
				$estoque_content = explode('#{if:estoque}', $html_content);
				$estoque_content = explode('#{end:estoque}', $estoque_content[1]);
				$estoque_base = $estoque_content = $estoque_content[0];

				$html_content = str_replace($estoque_base, '', $html_content);
				$html_content = str_replace('#{if:estoque}', '', $html_content);
				$html_content = str_replace('#{end:estoque}', '', $html_content);
			}

		}

		// Caracteristicas
		$caracteristicas_content = '<table>';

		foreach ($produto['CaracteristicasValoresSelecionado'] as $caracteristica) {
			$caracteristicas_content .= '<tr>';
			$caracteristicas_content .= 	'<th class="caracteristica-titulo ' . Inflector::slug($caracteristica['Caracteristica']['descricao'], '-') . '">' . $caracteristica['Caracteristica']['descricao'] . '</th>';
			$caracteristicas_content .= 	'<td class="caracteristica-valor ' . Inflector::slug($caracteristica['Caracteristica']['descricao'], '-') . '">';
			$caracteristicas_content .= 		$caracteristica['descricao'];
			$caracteristicas_content .= 		nl2br($caracteristica['descricao_grande']);

			if (isset($caracteristica['CaracteristicasValor']['descricao'])) {
				$caracteristicas_content .=	$caracteristica['CaracteristicasValor']['descricao'];
			}

			$caracteristicas_content .= 	'</td>';
			$caracteristicas_content .= '</tr>';
		}

		$caracteristicas_content .= '</table>';

		if (count($produto['CaracteristicasValoresSelecionado']) == 0) {
			$caracteristicas_content = '';
		}

		$html_content = str_replace('#{caracteristicas}', $caracteristicas_content, $html_content);

		// Caracteristicas id
		foreach ($produto['CaracteristicasValoresSelecionado'] as $caracteristica) {
			$caracteristica_id_content = '<ul class="caracteristica-id-' . $caracteristica['Caracteristica']['id'] . '">';
			$caracteristica_id_content .= 	'<li class="caracteristica-titulo ' . Inflector::slug($caracteristica['Caracteristica']['descricao'], '-') . '">' . $caracteristica['Caracteristica']['descricao'] . '</li>';
			$caracteristica_id_content .= 	'<li class="caracteristica-valor ' . Inflector::slug($caracteristica['Caracteristica']['descricao'], '-') . '">';
			$caracteristica_id_content .= 		$caracteristica['descricao'];
			$caracteristica_id_content .= 		nl2br($caracteristica['descricao_grande']);

			if (isset($caracteristica['CaracteristicasValor']['descricao'])) {
				$caracteristica_id_content .=	$caracteristica['CaracteristicasValor']['descricao'];
			}

			$caracteristica_id_content .= 	'</li>';

			$caracteristica_id_content .= '</ul>';

			if (count($produto['CaracteristicasValoresSelecionado']) == 0) {
				$caracteristica_id_content = '';
			}

			$html_content = str_replace('#{caracteristica:' . $caracteristica['Caracteristica']['id'] . '}', $caracteristica_id_content, $html_content);
		}

		if (strrpos($html_content, '#{caracteristica:') !== false) {

			$caracteristica_id_content_base_explode = explode('#{caracteristica:', $html_content);

			foreach ($caracteristica_id_content_base_explode as $key => $caracteristica_id_content_base_value) {
				if ($key > 0) {
					$posicao = strpos($caracteristica_id_content_base_value, '}');
					$caracteristica_id = substr($caracteristica_id_content_base_value, 0, $posicao);

					$html_content = str_replace('#{caracteristica:' . $caracteristica_id . '}', '', $html_content);
				}
			}
		}

		return $html_content;
	}

}

?>