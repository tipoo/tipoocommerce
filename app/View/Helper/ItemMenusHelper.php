<?php
App::uses('AppHelper', 'Helper');

class ItemMenusHelper extends AppHelper {

	public $helpers = array('Html');

	private $menus;

	public function __construct(View $View, $settings = array()) {
		parent::__construct($View, $settings);
		$this->menus = $View->getVar('controleMenus');
	}

	public function getMenu($id = '') {

		if (count($this->menus)) {
			return $this->gerar($this->menus, $id);
		} else {
			return '<!-- Menu vazio -->';
		}

	}

	private function gerar($menus, $id = '', $html = '', $nivel = 0, $incluiHeader = false) {

		if ($nivel == 0) {
			$html .= '<ul class="'.$menus[$id]['Menu']['classe_css'].' nivel-'.$nivel.' ">';
		} else {
			$html .= '<ul class="nivel-'.$nivel.'">';
		}

		$nivelHeader = $nivel + 3;
		if ($nivelHeader > 6) {
			$nivelHeader = 6;
		}

		foreach ($menus as $item_menu) {

			if ($item_menu['Menu']['id'] == $id) {

				$html .= '<li class="'.$item_menu['ItemMenu']['classe_css'].'">';

				$target = '';
				if ($item_menu['ItemMenu']['nova_aba']) {
					$target = '_blank';
				}

				if ($incluiHeader) {
					$html .= '<h'.$nivelHeader.'><a href="'.$item_menu['ItemMenu']['link'].'" target="'.$target.'">'.$item_menu['ItemMenu']['nome'].'</a></h'.$nivelHeader.'>';
				} else {
					$html .= '<a href="'.$item_menu['ItemMenu']['link'].'" target="'.$target.'">'.$item_menu['ItemMenu']['nome'].'</a>';
				}

				if (count($item_menu['children'])) {
					$html = $this->gerar($item_menu['children'], $id, $html, ($nivel + 1), $incluiHeader);
				}

				$html .= '</li>';

			}

		}

		$html .= '</ul>';
		return $html;
	}

}
?>