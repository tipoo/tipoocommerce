<?php
App::uses('AppHelper', 'Helper');

class CustomTimeHelper extends AppHelper {

	public function formatar($data, $formato) {
		$time = strtotime($data);
		return date($formato, $time);
	}

	public function formatarDataHora($data) {
		return $this->formatar($data, 'd/m/Y H:i:s');
	}

	public function formatarData($data) {
		return $this->formatar($data, 'd/m/Y');
	}

	public function formatarHora($data, $com_segundos = true) {
		if ($com_segundos) {
			return $this->formatar($data, 'H:i:s');
		} else {
			return $this->formatar($data, 'H:i');
		}
	}

}

?>