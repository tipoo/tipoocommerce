<?php
App::uses('FormHelper', 'View/Helper');
class TipooTBFormHelper extends FormHelper {

	private $hasLegend = false;

	public function __construct(View $View, $settings = array()) {
		parent::__construct($View, $settings);
	}

	public function input($fieldName, $options = array()) {

		if (isset($options['type']) && $options['type'] == 'checkbox') {
			if (isset($options['div']) && $options['div'] !== FALSE &&
					strpos($options['div'], 'form-group') === FALSE) {

				$options['div'] .= ' form-group';

			} elseif (!isset($options['div'])) {
				$options['div'] = 'form-group';
			}

			// Forçando a ordem na qual os elementos sao criados.
			if (!isset($options['format'])) {
				$options['format'] = array('before', 'input', 'after');
			}
		

			$out = '<div class="'.$options['div'].'">';
			$out .= '<div class="checkbox"><label>';

			$options['div'] = false;
			$out .= parent::input($fieldName, $options);

			if (isset($options['label'])) {
				$out .= ' '.$options['label'];
			}

			$out .= '</label></div></div>';

		} else {
			// Forçando a ordem na qual os elementos sao criados.
			if (!isset($options['format'])) {
				$options['format'] = array('before', 'label', 'between', 'input', 'after', 'error');
			}

			if (isset($options)) {

				if (isset($options['div']) && $options['div'] !== FALSE &&
						strpos($options['div'], 'form-group') === FALSE) {

					$options['div'] .= ' form-group';

				} elseif (!isset($options['div'])) {
					$options['div'] = 'form-group';
				}

				if (isset($options['label']) && $options['label'] !== FALSE) {
					if (is_array($options['label'])) {
						if (isset($options['label']['class'])) {
							if (strpos($options['label']['class'], 'control-label') === FALSE) {
								$options['label']['class'] .= ' control-label';
							}
						} else {
							$options['label']['class'] = 'control-label';
						}
					} else {
						$text = $options['label'];

						$options['label'] = array(
							'text' => $text,
							'class' => 'control-label'
						);
					}
				} elseif (!isset($options['label'])) {
					$options['label']['class'] = 'control-label';
				}
			}

			if (isset($options) && $options !== FALSE) {
				if (is_array($options)) {
					if (isset($options['class'])) {
						if (strpos($options['class'], 'form-control') === FALSE) {
							$options['class'] .= ' form-control';
						}
					} else {
						if (isset($options['type'])) {
							if ($options['type'] != 'file') {
								$options['class'] = 'form-control';
							}
						} else {
							$options['class'] = 'form-control';
						}
					}
				}
			}

			$out = parent::input($fieldName, $options);
		}

		if (isset($options) &&  isset($options['row']) && $options['row'] !== FALSE) {
			if (isset($options['help'])) {
				$out = '<div class="row help-form-group">'.$out.'</div>';
		
				$help = '<div class="row help">';

				if (isset($options['help']['class'])) {
					$help .= '<div class="'.$options['help']['class'].'">';
				} else {
					$help .= '<div>';
				}

				$help .= '<span class="help-inline">';

				if (isset($options['help']['text'])) {
					$help .= $options['help']['text'];
				}

				$help .= '</span>';
				$help .= '</div>';
				$help .= '</div>';

				$out .= $help;

			} else {
				$out = '<div class="row">'.$out.'</div>';
			}
		}

		return $out;

	}

	/**
	 * Método criado para mostrar uma barra de ações ao final do formulário
	 * com os botões salvar e cancelar. Caso necessite de customização esta
	 * deverá ser feita direto no .ctp.
	 *
	 * @param array $options
	 * - submit padrão "Salvar"
	 * - reset padrão "Cancelar" se for false o botão não é exibido
	 */
	public function actions($options = null) {
		$default = array('submit' => 'Salvar', 'reset' => false);
		$options = isset($options) && is_array($options) ? array_merge($default, $options) : $default;

		$out = '<div class="form-actions well">';
		if ($options['reset'] !== false) {
			$out .= '<button type="reset" class="btn">'.$options['reset'].'</button>&nbsp;';
		}
		$out .= '<input type="submit" class="btn btn-primary btn-action-save-default" data-loading-text="Salvando..." value="'.$options['submit'].'">';
		$out .= '</div>';

		return $out;
	}
}
?>
