<?php
App::uses('AppHelper', 'Helper');

class CmsHelper extends AppHelper {

	public $helpers = array('Html');

	private $arvore;

	public function __construct(View $View, $settings = array()) {
		parent::__construct($View, $settings);
		$this->arvore = $View->getVar('arvore');
	}

	public function imprimirArvore() {
		echo '<span class="glyphicon glyphicon-hdd"></span> ' . $this->Html->link($_SERVER['SERVER_NAME'], '#', array('data-local' => $_SERVER['SERVER_NAME'], 'data-caminho' => '', 'data-tipo' => 'raiz'));
		$this->montarArvore($this->arvore);
	}

	private function montarArvore($arvore = array(), $local = false, $caminho = false) {
		if ($local) {
			$class = str_replace(DS, '-', $local);
			echo '<ul class="'.$class.'">';
			$local = $local . DS;
		} else {
			echo '<ul class="'.str_replace('.', '-', $_SERVER['SERVER_NAME']).' raiz">';
		}

		if ($caminho) {
			$caminho = $caminho . '.DS.';
		}

		foreach ($arvore['pastas'] as $nome_dir => $arvore_pasta) {
			echo '<li><span class="glyphicon glyphicon-folder-open text-info"></span> &nbsp;' . $this->Html->link($nome_dir, '#', array('data-local' => $local . $nome_dir, 'data-caminho' => $caminho . $nome_dir, 'data-tipo' => 'pasta', 'data-prioridade-arvore' => '0'));
			if (count($arvore_pasta['pastas']) || count($arvore_pasta['arquivos'])) {
				$this->montarArvore($arvore_pasta, $local . $nome_dir, $caminho . $nome_dir);
			}
			echo '</li>';
		}

		foreach ($arvore['arquivos'] as $nome_arq => $dir) {
			if (strpos($nome_arq, ".ctp") !== FALSE ||
					strpos($nome_arq, ".js") !== FALSE ||
					strpos($nome_arq, ".css") !== FALSE) {
				$tipo_arquivo = 'arquivo_editor';
			} else if (strpos($nome_arq, ".jpg") !== FALSE ||
					strpos($nome_arq, ".jpeg") !== FALSE ||
					strpos($nome_arq, ".png") !== FALSE ||
					strpos($nome_arq, ".gif") !== FALSE) {
				$tipo_arquivo = 'arquivo_imagem';
			} else {
				$tipo_arquivo = 'arquivo';
			}
			echo '<li><span class="glyphicon glyphicon-file text-primary"></span> ' . $this->Html->link($nome_arq, '#', array('data-local' => $local . $nome_arq, 'data-caminho' => $caminho . $nome_arq, 'data-tipo' => $tipo_arquivo, 'data-prioridade-arvore' => '1')) . '</li>';
		}

		echo '</ul>';
	}

}
?>