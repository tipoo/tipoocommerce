<?php
	$this->Html->script(array('Views/Marcas/admin_editar'), array('inline' => false));
	$this->Html->css(array('Views/Marcas/admin_editar'), 'stylesheet', array('inline' => false));
?>

<ol class="breadcrumb">
	<li><?php echo $this->Html->link('Marcas', array('action' => 'backToPaginatorIndex', 'admin' => true)); ?></li>
	<li class="active">Editar</li>
</ol>

<?php
	echo $this->Form->create('Marca', array('type' => 'file'));
?>
<!-- 	<div class="well hide">
		<h4><i class="glyphicon glyphicon-exclamation-sign"></i> Aviso:
	       <small> ao editar a url ela perde a indexação atual, fazendo com que o Google e outras ferramentas de busca não consigam listá-la temporariamente.</small>
		</h4>
		<hr>
		<div class="row">
			<div class="col-md-12">
				<div class="form-inline"><label>Url*: </label>
				  <?php //echo  Router::url('/', true); ?>
				  <input name="data[Slug][url]" row="1" class="form-control" type="text" value="<?php //echo substr($this->request->data['Slug']['url'], 1) ?>" id="SlugUrl">
				</div>
			</div>
		</div>
	</div>
	<br> -->

<?php
	echo $this->Form->input('descricao', array('row' => true, 'div' => 'col-md-4', 'label' => 'Descrição*'));
	echo $this->Form->input('imagem', array('type' => 'file'));

	if (isset($this->request->data['Marca']['imagem'])) {
?>
		<div class="control-group">
			<label class="control-label" for="MarcaImagem"></label>
			<div class="controls">
				<img style="max-width: 300px; max-height: 300px" src="<?php echo $this->webroot ?>files/<?php echo PROJETO ?>/marcas/<?php echo $this->request->data['Marca']['imagem'] ?>" />
			</div>
		</div>
		<br>
<?php
	}

	$num_de_caracteres = '<span class="pull-right" id="num_de_caracteres_marca">Nº de caracteres: 160</span>';
	echo $this->Form->input('meta_description', array('row' => true, 'div' => 'col-md-4', 'label' => 'Meta Description','placeholder' => 'Recomendável 160 caracteres', 'after' => $num_de_caracteres));
	echo $this->Form->input('meta_keywords', array('row' => true, 'div' => 'col-md-4', 'label' => 'Meta Keywords'));
	echo $this->Form->actions();
	echo $this->Form->end();
?>