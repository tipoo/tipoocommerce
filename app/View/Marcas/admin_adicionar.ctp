<?php
	$this->Html->script(array('Views/Marcas/admin_adicionar'), array('inline' => false));
?>

<ol class="breadcrumb">
	<li><?php echo $this->Html->link('Marcas', array('action' => 'backToPaginatorIndex', 'admin' => true)); ?></li>
	<li class="active">Adicionar</li>
</ol>

<?php
	echo $this->Form->create('Marca', array('type' => 'file'));
	echo $this->Form->input('descricao', array('row' => true, 'div' => 'col-md-4', 'label' => 'Descrição*'));
	echo $this->Form->input('imagem', array('type' => 'file'));
	$num_de_caracteres = '<span class="pull-right" id="num_de_caracteres_marca">Nº de caracteres: 160</span>';
	echo $this->Form->input('meta_description', array('row' => true, 'div' => 'col-md-4', 'label' => 'Meta Description','placeholder' => 'Recomendável 160 caracteres', 'after' => $num_de_caracteres));
	echo $this->Form->input('meta_keywords', array('row' => true, 'div' => 'col-md-4', 'label' => 'Meta Keywords'));
	echo $this->Form->actions();
	echo $this->Form->end();
?>

