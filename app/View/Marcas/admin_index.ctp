<ol class="breadcrumb">
	<li class="active">Marcas</li>
</ol>

<div class="table-responsive">
	<table class="table table-striped table-hover">
		<thead>
			<tr>
				<th class="col-md-1 text-center">#</th>
				<th class="col-md-5">Descrição</th>
				<th class="col-md-5">Url</th>
				<th class="text-center"><?php echo $this->Html->link('<span class="glyphicon glyphicon-plus"></span>', array('action' => 'adicionar'), array('class' => 'btn btn-sm btn-primary', 'escape' => false,'data-placement' => 'left', 'rel' => 'tooltip', 'title' => 'Adicionar Novo')); ?></th>
			</tr>
		</thead>

		<tbody>
			<?php
				foreach ($marcas as $marca) {
			?>
				<tr>
					<td class="text-center"><?php echo $marca['Marca']['id'] ?></td>
					<td><?php echo $marca['Marca']['descricao'] ?>
						<br>
						<?php
							if (!$marca['Marca']['ativo']) {
								$status = '<span class="label label-danger">Inativo</span>';
							} else {
								$status = '<span class="label label-success">Ativo</span> ';
							}

							echo $status;
						?>
					</td>
					<td><?php echo $marca['Slug']['url'] ?></td>
					<td class="text-center">

						<div class="btn-group">
							<button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu pull-right text-left" role="menu">
								<li><?php echo $this->Html->link('<span class="glyphicon glyphicon-edit"></span> Editar', array('action' => 'editar',$marca['Marca']['id']), array('escape' => false)); ?></li>
								<?php
									if ($this->Permissoes->check(array('controller' => 'produtos', 'action' => 'index', 'admin' => true, 'plugin' => null))) {
								?>
										<li> <?php echo $this->Html->link('<span class="glyphicon "></span>Produtos', array('controller' => 'produtos', 'action' => 'index', 'marca_id' => $marca['Marca']['id']), array('escape' => false));?></li>
								<?php
								 	}
								?>

								<?php
							 		if ($this->Permissoes->check(array('controller' => 'vitrines', 'action' => 'colecoes', 'admin' => true, 'plugin' => null))) {
					 			?>
										<li> <?php echo $this->Html->link('<span class="glyphicon "></span>Vitrines de Coleções', array('controller' => 'vitrines', 'action' => 'colecoes', 'marca_id' => $marca['Marca']['id']), array('escape' => false));?></li>
								<?php
									}
								?>

								<?php
							 		if ($this->Permissoes->check(array('controller' => 'vitrines', 'action' => 'htmls', 'admin' => true, 'plugin' => null))) {
					 			?>
										<li> <?php echo $this->Html->link('<span class="glyphicon "></span>Texto/Html', array('controller' => 'vitrines', 'action' => 'htmls', 'marca_id' => $marca['Marca']['id']), array('escape' => false));?></li>
								<?php
									}
								?>

								<?php
									if ($marca['Marca']['ativo']) {
								?>
										<li><?php echo $this->Form->postLink('<span class="glyphicon"></span>Desativar', array('action' => 'excluir', $marca['Marca']['id']), array('escape' => false), 'Deseja realmente desativar a Marca #' . $marca['Marca']['id'] . '?');?></li>
								<?php
									} else {

								?>
										<li><?php echo $this->Form->postLink('<span class="glyphicon"></span>Ativar', array('action' => 'ativar', $marca['Marca']['id']), array('escape' => false), 'Deseja realmente ativar a Marca #' . $marca['Marca']['id'] . '?');?></li>
								<?php
									}
								?>
							</ul>
						</div>
					</td>
				</tr>
			<?php
				}
			?>
		</tbody>
	</table>
</div>

<?php echo $this->element('Paginacao/admin_paginacao'); ?>