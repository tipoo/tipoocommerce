<?php echo '<?xml version="1.0" encoding="UTF-8"?>'?>
<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
	<sitemap>
		<loc><?php echo $this->Html->url('/', true);?>sitemap-categorias.xml</loc>
	</sitemap>
	<sitemap>
		<loc><?php echo $this->Html->url('/', true);?>sitemap-marcas.xml</loc>
	</sitemap>
	<sitemap>
		<loc><?php echo $this->Html->url('/', true);?>sitemap-produtos.xml</loc>
	</sitemap>
	<?php if ($slugs_tags) { ?>
			<sitemap>
				<loc><?php echo $this->Html->url('/', true);?>sitemap-tags.xml</loc>
			</sitemap>
	<?php } ?>
	<sitemap>
		<loc><?php echo $this->Html->url('/', true);?>sitemap-paginas.xml</loc>
	</sitemap>
</sitemapindex>
