<ol class="breadcrumb">
	<li ><?php echo $this->Html->link('Configurações', array('action' => 'backToPaginatorIndex', 'admin' => true)); ?></li>
	<li class="active">Editar</li>
</ol>

<?php
	if ($permissao_configuracao_analytics ||
		$permissao_configuracao_api_pass ||
		$permissao_configuracao_api_user ||
		$permissao_configuracao_frete ||
		$permissao_configuracao_imagens ||
		$permissao_configuracao_meta_description ||
		$permissao_configuracao_meta_keywords ||
		$permissao_configuracao_parcelamento)
	{
 ?>
			<?php
				$this->Html->script(array('jquery.price_format.2.0.min', 'jquery.numeric.js','jquery.maskedinput.min', 'Views/Configuracoes/admin_editar'),array('inline' =>false));
			?>

			<?php
				echo $this->Form->create('Configuracao');
				echo $this->Form->input('id');
			 ?>

			<div class="row">

			<?php
				if ($permissao_configuracao_frete ||
					$permissao_configuracao_imagens ||
					$permissao_configuracao_parcelamento)
				{
			?>

					<div class="col-md-6">

						<?php
							if ($permissao_configuracao_parcelamento) {
						?>
								<div class="panel panel-default">
									<div class="panel-heading">
										<h3 class="panel-title">Parcelamento</h3>
									</div>
									<ul class="list-group">
										<li class="list-group-item">
											<h5 class="form-titulos-admin">Número máximo de parcelas </h5>
											<div class="row">
												<div class="col-md-2">
													<?php
														echo $this->Form->input('parc_parcelas', array('type' => 'text', 'label' => false));
													?>
												</div>
											</div>
										</li>

										<li class="list-group-item">
											<h5 class="form-titulos-admin">Valor mínimo da parcela </h5>
											<div class="row">
												<div class="col-md-4">
													<?php
														echo $this->Form->input('parc_valor_min_parcela', array('type' => 'text', 'label' => false));
													?>
												</div>
											</div>
										</li>

										<li class="list-group-item">
											<h5 class="form-titulos-admin">Juros</h5>
											<div class="row">
												<div class="col-md-4">
													<?php
														echo $this->Form->input('parc_juros', array('type' => 'text', 'label' => false));
													?>
												</div>
											</div>
										</li>
									</ul>
								</div>
						<?php
							}
						 ?>

						<?php
							if ($permissao_configuracao_imagens) {
						?>
								<div class="panel panel-default">
									<div class="panel-heading">
										<h3 class="panel-title">Imagens</h3>
									</div>
									<ul class="list-group">
										<li class="list-group-item">
											<h5 class="form-titulos-admin">Thumb </h5>
											<div class="row">
												<div class="col-md-12">
													<div class="col-md-3">
														<?php echo $this->Form->input('img_thumb_w', array('type' => 'text', 'label' => 'Largura')); ?>
													</div>
													<div class="col-md-1"><br><br>X</div>
													<div class="col-md-3">
														<?php echo $this->Form->input('img_thumb_h', array('type' => 'text', 'label' => 'Altura')); ?>
													</div>
												</div>
											</div>
										</li>

										<li class="list-group-item">
											<h5 class="form-titulos-admin">Small </h5>
											<div class="row">
												<div class="col-md-12">
													<div class="col-md-3">
														<?php echo $this->Form->input('img_small_w', array('type' => 'text', 'label' => 'Largura')); ?>
													</div>
													<div class="col-md-1"><br><br>X</div>
													<div class="col-md-3">
														<?php echo $this->Form->input('img_small_h', array('type' => 'text', 'label' => 'Altura')); ?>
													</div>
												</div>
											</div>
										</li>

										<li class="list-group-item">
											<h5 class="form-titulos-admin">Normal </h5>
											<div class="row">
												<div class="col-md-12">
													<div class="col-md-3">
														<?php echo $this->Form->input('img_normal_w', array('type' => 'text', 'label' => 'Largura')); ?>
													</div>
													<div class="col-md-1"><br><br>X</div>
													<div class="col-md-3">
														<?php echo $this->Form->input('img_normal_h', array('type' => 'text', 'label' => 'Altura')); ?>
													</div>
												</div>
											</div>
										</li>

										<li class="list-group-item">
											<h5 class="form-titulos-admin">Medium </h5>
											<div class="row">
												<div class="col-md-12">
													<div class="col-md-3">
														<?php echo $this->Form->input('img_medium_w', array('type' => 'text', 'label' => 'Largura')); ?>
													</div>
													<div class="col-md-1"><br><br>X</div>
													<div class="col-md-3">
														<?php echo $this->Form->input('img_medium_h', array('type' => 'text', 'label' => 'Altura')); ?>
													</div>
												</div>
											</div>
										</li>

										<li class="list-group-item">
											<h5 class="form-titulos-admin">Big </h5>
											<div class="row">
												<div class="col-md-12">
													<div class="col-md-3">
														<?php echo $this->Form->input('img_big_w', array('type' => 'text', 'label' => 'Largura')); ?>
													</div>
													<div class="col-md-1"><br><br>X</div>
													<div class="col-md-3">
														<?php echo $this->Form->input('img_big_h', array('type' => 'text', 'label' => 'Altura')); ?>
													</div>
												</div>
											</div>
										</li>

										<li class="list-group-item">
											<h5 class="form-titulos-admin">Imagem do carrinho </h5>
											<div class="row">
												<div class="col-md-12">
												<?php
													echo $this->Form->input('img_carrinho', array('options' => $tamanho, 'empty' => 'Selecione','row' => true, 'div' => 'col-md-4','label' => false));
												?>
												</div>
											</div>
										</li>
									</ul>
								</div>
						<?php
							}
						 ?>

				 		<?php
							if ($permissao_configuracao_frete) {
						?>
								<div class="panel panel-default">
									<div class="panel-heading">
										<h3 class="panel-title">Frete</h3>
									</div>
									<ul class="list-group">

										<li class="list-group-item">
											<h5 class="form-titulos-admin">Cep de origem </h5>
											<div class="row">
												<div class="col-md-4">
													<?php
														echo $this->Form->input('frete_cep_origem', array('type' => 'text', 'label' => false));
													?>
												</div>
											</div>
										</li>

										<li class="list-group-item">
											<h5 class="form-titulos-admin">Código de serviço da Empresa </h5>
											<div class="row">
												<div class="col-md-8">
													<?php
														echo $this->Form->input('frete_correios_codigo_empresa', array('type' => 'text', 'label' => false));
													?>
												</div>
											</div>
										</li>

										<li class="list-group-item">
											<h5 class="form-titulos-admin">Senha do Correio </h5>
											<div class="row">
												<div class="col-md-8">
													<?php
														echo $this->Form->input('frete_correios_senha', array('type' => 'text', 'label' => false));
													?>
												</div>
											</div>
										</li>
									</ul>
								</div>
						<?php
							}
						 ?>
					</div>
			<?php
				}
			?>

			<?php
				if ($permissao_configuracao_analytics ||
					$permissao_configuracao_api_pass ||
					$permissao_configuracao_api_user ||
					$permissao_configuracao_meta_description ||
					$permissao_configuracao_meta_keywords)
				{
			?>

					<div class="col-md-6">

						<?php
							if ($permissao_configuracao_analytics) {
						?>
								<div class="panel panel-default">
									<div class="panel-heading">
										<h3 class="panel-title">Google Analytics</h3>
									</div>
									<ul class="list-group">
										<li class="list-group-item">
											<div class="row">
												<div class="col-md-12">
												<?php
													echo $this->Form->input('google_analytics', array('rows' => 11, 'type' => 'textarea', 'label' => false));
												?>
												</div>
											</div>
										</li>
									</ul>
								</div>
						<?php
							}
						 ?>

						 <?php
							if ($permissao_configuracao_robots) {
						?>
								<div class="panel panel-default">
									<div class="panel-heading">
										<h3 class="panel-title">robots.txt</h3>
									</div>
									<ul class="list-group">
										<li class="list-group-item">
											<div class="row">
												<div class="col-md-12">
												<?php
													echo $this->Form->input('robots', array('rows' => 11, 'type' => 'textarea', 'label' => false));
												?>
												</div>
											</div>
										</li>
									</ul>
								</div>
						<?php
							}
						 ?>

				  		<?php
							if ($permissao_configuracao_meta_description) {
						?>
								<div class="panel panel-default">
									<div class="panel-heading">
										<h3 class="panel-title">Meta Description</h3>
									</div>
									<ul class="list-group">
										<li class="list-group-item">
											<div class="row">
												<div class="col-md-12">
													<?php
														$num_de_caracteres = '<span class="pull-right" id="num_de_caracteres">Nº de caracteres: 160</span>';
														echo $this->Form->input('meta_description', array('type' => 'textarea','placeholder' => 'Recomendável 160 caracteres.', 'label' => false, 'after' => $num_de_caracteres));
													?>
												</div>
											</div>
										</li>
									</ul>
								</div>
						<?php
							}
						 ?>

				   		<?php
							if ($permissao_configuracao_meta_keywords) {
						?>
								<div class="panel panel-default">
									<div class="panel-heading">
										<h3 class="panel-title">Meta Keywords</h3>
									</div>
									<ul class="list-group">
										<li class="list-group-item">
											<div class="row">
												<div class="col-md-12">
													<?php
														echo $this->Form->input('meta_keywords', array('type' => 'textarea','placeholder' => 'Palavras separadas por vírgula.', 'label' => false));
													?>
												</div>
											</div>
										</li>
									</ul>
								</div>
						<?php
							}
						 ?>

						<?php
							if ($permissao_configuracao_api_user) {
						?>
								<div class="panel panel-default">
									<div class="panel-heading">
										<h3 class="panel-title">Api User</h3>
									</div>
									<ul class="list-group">
										<li class="list-group-item">
											<div class="row">
												<div class="col-md-12">
													<?php
														echo $this->Form->input('api_user', array('type' => 'text', 'label' => false));
													?>
												</div>
											</div>
										</li>
									</ul>
								</div>
						<?php
							}
						 ?>

						<?php
							if ($permissao_configuracao_api_pass) {
						?>
								<div class="panel panel-default">
									<div class="panel-heading">
										<h3 class="panel-title">Api Pass</h3>
									</div>
									<ul class="list-group">
										<li class="list-group-item">
											<div class="row">
												<div class="col-md-12">
													<?php
														echo $this->Form->input('api_pass', array('type' => 'text', 'label' => false));
													?>
												</div>
											</div>
										</li>
									</ul>
								</div>
						<?php
							}
						 ?>
					</div>

			<?php
				}
			?>
			</div>
			<?php
				echo $this->Form->actions();
				echo $this->Form->end();
			?>

<?php
	} else {
?>
		<div class="text-center">

			<p class="counter well">
				<?php
						echo 'Verifique sua permissão com o suporte técnico.';
				?>
			</p>
		</div>
<?php
	}
 ?>
