<?php echo '<?xml version="1.0" encoding="UTF-8"?>'?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1">
<?php foreach($produtos as $produto) { ?>
		<url>
			<loc><?php echo $this->Html->url($produto['Slug']['url'], true);?></loc>
			<image:image>
				<image:loc><?php echo $this->Html->url('/', true) . 'files/' . PROJETO . '/produtos/' . $produto['Imagem'][0]['imagem'] ;?></image:loc>
				<image:title><?php echo $produto['Produto']['descricao'];?></image:title>
			</image:image>
			<priority>0.4</priority>
		</url>
<?php } ?>
</urlset>