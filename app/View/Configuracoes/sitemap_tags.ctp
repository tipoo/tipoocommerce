<?php echo '<?xml version="1.0" encoding="UTF-8"?>'?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
<?php foreach($slugs_tags as $slug_tag) { ?>
		<url>
			<loc><?php echo $this->Html->url($slug_tag['Slug']['url'], true);?></loc>
			<priority>0.5</priority>
		</url>
<?php } ?>
</urlset>