<?php echo '<?xml version="1.0" encoding="UTF-8"?>'?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
<?php foreach($slugs_paginas as $slug_pagina) { ?>
		<url>
			<loc><?php echo $this->Html->url($slug_pagina['Slug']['url'], true);?></loc>
			<priority><?php echo $slug_pagina['Slug']['url'] == '/' ? 1 : 0.2 ?></priority>
		</url>
<?php } ?>
</urlset>