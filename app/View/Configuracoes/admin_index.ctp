<ol class="breadcrumb">
	<li class="active">Configurações</li>
</ol>

<?php
	if ($permissao_configuracao_analytics ||
		$permissao_configuracao_api_pass ||
		$permissao_configuracao_api_user ||
		$permissao_configuracao_frete ||
		$permissao_configuracao_imagens ||
		$permissao_configuracao_meta_description ||
		$permissao_configuracao_meta_keywords ||
		$permissao_configuracao_parcelamento)
	{
 ?>
			<div class="row">
				<div class="col-md-12">
					<?php
						echo $this->Html->link('<span class="glyphicon glyphicon-edit"></span> Editar', array('action' => 'editar'), array('class ' => 'btn btn-sm btn-success pull-right', 'escape' => false));
					?>
				</div>
			</div>
			<br>

			<div class="row">

			<?php
				if ($permissao_configuracao_frete ||
					$permissao_configuracao_imagens ||
					$permissao_configuracao_parcelamento)
				{
			?>
					<div class="col-md-6">

					<?php
						if ($permissao_configuracao_parcelamento) {
					?>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">Parcelamento</h3>
								</div>
								<ul class="list-group">
									<li class="list-group-item">
										<h5 class="form-titulos-admin">Número máximo de parcelas </h5>
										<div class="row">
											<div class="col-md-12">
											<?php
												if ($configuracao['Configuracao']['parc_parcelas'] != '') {

													echo $configuracao['Configuracao']['parc_parcelas'];

												} else {

													echo '-';

												}

											?>
											</div>
										</div>
									</li>

									<li class="list-group-item">
										<h5 class="form-titulos-admin">Valor mínimo da parcela </h5>
										<div class="row">
											<div class="col-md-12">
											<?php
												if ($configuracao['Configuracao']['parc_valor_min_parcela'] != '') {

													echo $this->Formatacao->moeda($configuracao['Configuracao']['parc_valor_min_parcela']);

												} else {

													echo '-';

												}

											?>
											</div>
										</div>
									</li>

									<li class="list-group-item">
										<h5 class="form-titulos-admin">Juros </h5>
										<div class="row">
											<div class="col-md-12">
											<?php
												if ($configuracao['Configuracao']['parc_juros'] != '') {

													echo $configuracao['Configuracao']['parc_juros'] . ' %';

												} else {

													echo '-';

												}

											?>
											</div>
										</div>
									</li>
								</ul>
							</div>
					<?php
						}
					?>

					<?php
						if ($permissao_configuracao_imagens) {
					?>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">Imagens</h3>
								</div>
								<ul class="list-group">
									<li class="list-group-item">
										<h5 class="form-titulos-admin">Thumb </h5>
										<div class="row">
											<div class="col-md-12">
											<?php
												if ($configuracao['Configuracao']['img_thumb_w'] != '' && $configuracao['Configuracao']['img_thumb_h'] != '') {

													echo $configuracao['Configuracao']['img_thumb_w'];
													echo ' X ' ;
													echo $configuracao['Configuracao']['img_thumb_h'] ;

												} else {

													echo '-';

												}
											?>
											</div>
										</div>
									</li>

									<li class="list-group-item">
										<h5 class="form-titulos-admin">Small </h5>
										<div class="row">
											<div class="col-md-12">
											<?php
												if ($configuracao['Configuracao']['img_small_w'] != '' && $configuracao['Configuracao']['img_small_h'] != '') {

													echo $configuracao['Configuracao']['img_small_w'];
													echo ' X ' ;
													echo $configuracao['Configuracao']['img_small_h'] ;

												} else {

													echo '-';

												}
											?>
											</div>
										</div>
									</li>

									<li class="list-group-item">
										<h5 class="form-titulos-admin">Normal </h5>
										<div class="row">
											<div class="col-md-12">
											<?php
												if ($configuracao['Configuracao']['img_normal_w'] != '' && $configuracao['Configuracao']['img_normal_h'] != '') {

													echo $configuracao['Configuracao']['img_normal_w'];
													echo ' X ' ;
													echo $configuracao['Configuracao']['img_normal_h'] ;

												} else {

													echo '-';

												}
											?>
											</div>
										</div>
									</li>

									<li class="list-group-item">
										<h5 class="form-titulos-admin">Medium </h5>
										<div class="row">
											<div class="col-md-12">
											<?php
												if ($configuracao['Configuracao']['img_medium_w'] != '' && $configuracao['Configuracao']['img_medium_h'] != '') {

													echo $configuracao['Configuracao']['img_medium_w'];
													echo ' X ' ;
													echo $configuracao['Configuracao']['img_medium_h'] ;

												} else {

													echo '-';

												}
											?>
											</div>
										</div>
									</li>

									<li class="list-group-item">
										<h5 class="form-titulos-admin">Big </h5>
										<div class="row">
											<div class="col-md-12">
											<?php
												if ($configuracao['Configuracao']['img_big_w'] != '' && $configuracao['Configuracao']['img_big_h'] != '') {

													echo $configuracao['Configuracao']['img_big_w'];
													echo ' X ' ;
													echo $configuracao['Configuracao']['img_big_h'] ;

												} else {

													echo '-';

												}
											?>
											</div>
										</div>
									</li>

									<li class="list-group-item">
										<h5 class="form-titulos-admin">Imagem do carrinho </h5>
										<div class="row">
											<div class="col-md-12">
											<?php
												if ($configuracao['Configuracao']['img_carrinho'] != '') {

												 	echo $tamanho[$configuracao['Configuracao']['img_carrinho']];

												} else {

													echo '-';
												}

											?>
											</div>
										</div>
									</li>
								</ul>
							</div>
					<?php
						}
					 ?>

					<?php
						if ($permissao_configuracao_frete) {
					?>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">Frete</h3>
								</div>
								<ul class="list-group">
									<li class="list-group-item">
										<h5 class="form-titulos-admin">Cep de origem </h5>
										<div class="row">
											<div class="col-md-12">
											<?php
												if ($configuracao['Configuracao']['frete_cep_origem'] != '') {

													echo $configuracao['Configuracao']['frete_cep_origem'];

												} else {

													echo '-';

												}
											?>
											</div>
										</div>
									</li>

									<li class="list-group-item">
										<h5 class="form-titulos-admin">Código de serviço da Empresa </h5>
										<div class="row">
											<div class="col-md-12">
											<?php
												if ($configuracao['Configuracao']['frete_correios_codigo_empresa'] != '') {

													echo $configuracao['Configuracao']['frete_correios_codigo_empresa'];

												} else {

													echo '-';

												}

											?>
											</div>
										</div>
									</li>

									<li class="list-group-item">
										<h5 class="form-titulos-admin">Senha do Correio </h5>
										<div class="row">
											<div class="col-md-12">
											<?php
												if ($configuracao['Configuracao']['frete_correios_senha'] != '') {

													echo $configuracao['Configuracao']['frete_correios_senha'];

												} else {

													echo '-';

												}

											?>
											</div>
										</div>
									</li>

								</ul>
							</div>

					<?php
						}
					 ?>
				</div>
			<?php
				}
			 ?>


			<?php
				if ($permissao_configuracao_analytics ||
					$permissao_configuracao_api_pass ||
					$permissao_configuracao_api_user ||
					$permissao_configuracao_meta_description ||
					$permissao_configuracao_meta_keywords)
				{
			?>
						<div class="col-md-6">

						<?php
							if ($permissao_configuracao_analytics) {
						?>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">Google Analytics</h3>
								</div>
								<ul class="list-group">
									<li class="list-group-item">
										<div class="row">
											<div class="col-md-12">
											<?php
												if ($configuracao['Configuracao']['google_analytics'] != '') {
											?>
													<textarea class="form-control" readonly disabled rows="11">
														<?php echo $configuracao['Configuracao']['google_analytics'];?>
													</textarea>
											<?php

												} else {

													echo '-';

												}
											?>
											</div>
										</div>
									</li>
								</ul>
							</div>

						<?php
							}
						 ?>

						 <?php
							if ($permissao_configuracao_robots) {
						?>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">robots.txt</h3>
								</div>
								<ul class="list-group">
									<li class="list-group-item">
										<div class="row">
											<div class="col-md-12">
											<?php
												if ($configuracao['Configuracao']['robots'] != '') {
											?>
													<textarea class="form-control" readonly disabled rows="11">
														<?php echo $configuracao['Configuracao']['robots'];?>
													</textarea>
											<?php

												} else {

													echo '-';

												}
											?>
											</div>
										</div>
									</li>
								</ul>
							</div>

						<?php
							}
						 ?>

				 		<?php
							if ($permissao_configuracao_meta_description) {
						?>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">Meta Description</h3>
								</div>
								<ul class="list-group">
									<li class="list-group-item">
										<div class="row">
											<div class="col-md-12">
											<?php
												if ($configuracao['Configuracao']['meta_description'] != '') {

													echo $configuracao['Configuracao']['meta_description'];

												} else {

													echo '-';

												}
											?>
											</div>
										</div>
									</li>
								</ul>
							</div>
						<?php
							}
						 ?>

				  		<?php
							if ($permissao_configuracao_meta_keywords) {
						?>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">Meta Keywords</h3>
								</div>
								<ul class="list-group">
									<li class="list-group-item">
										<div class="row">
											<div class="col-md-12">
											<?php
												if ($configuracao['Configuracao']['meta_keywords'] != '') {

													echo $configuracao['Configuracao']['meta_keywords'];

												} else {

													echo '-';

												}
											?>
											</div>
										</div>
									</li>
								</ul>
							</div>
						<?php
							}
						 ?>

				   		<?php
							if ($permissao_configuracao_api_user) {
						?>
								<div class="panel panel-default">
									<div class="panel-heading">
										<h3 class="panel-title">Api User</h3>
									</div>
									<ul class="list-group">
										<li class="list-group-item">
											<div class="row">
												<div class="col-md-12">
												<?php
													if ($configuracao['Configuracao']['api_user'] != '') {

														echo $configuracao['Configuracao']['api_user'];

													} else {

														echo '-';

													}
												?>
												</div>
											</div>
										</li>
									</ul>
								</div>
						<?php
							}
						 ?>

				   		<?php
							if ($permissao_configuracao_api_pass) {
						?>
								<div class="panel panel-default">
									<div class="panel-heading">
										<h3 class="panel-title">Api Pass</h3>
									</div>
									<ul class="list-group">
										<li class="list-group-item">
											<div class="row">
												<div class="col-md-12">
												<?php
													if ($configuracao['Configuracao']['api_pass'] != '') {

														echo $configuracao['Configuracao']['api_pass'];

													} else {

														echo '-';

													}
												?>
												</div>
											</div>
										</li>
									</ul>
								</div>
						<?php
							}
						 ?>

					</div>

			<?php
				}
			 ?>

			</div>

<?php
	} else {
?>
		<div class="text-center">

			<p class="counter well">
				<?php
						echo 'Verifique sua permissão com o suporte técnico.';
				?>
			</p>
		</div>
<?php
	}
 ?>