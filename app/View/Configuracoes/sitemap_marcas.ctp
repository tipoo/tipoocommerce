<?php echo '<?xml version="1.0" encoding="UTF-8"?>'?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
<?php foreach($slugs_marcas as $slug_marca) { ?>
		<url>
			<loc><?php echo $this->Html->url($slug_marca['Slug']['url'], true);?></loc>
			<priority>0.6</priority>
		</url>
<?php } ?>
</urlset>