<?php
	$this->Html->script(array('Views/Paginas/admin_editar'), array('inline' => false));
	$this->Html->css(array('Views/Paginas/admin_editar'), 'stylesheet', array('inline' => false));
?>

<ol class="breadcrumb">
	<li><?php echo $this->Html->link('Páginas', array('action' => 'backToPaginatorIndex', 'admin' => true)); ?></li>
	<li class="active">Editar</li>
</ol>

<?php
	echo $this->Form->create('Pagina');
?>
	<div class="well hide">
		<h4><i class="glyphicon glyphicon-exclamation-sign"></i> Aviso:
	       <small> ao editar a url ela perde a indexação atual, fazendo com que o Google e outras ferramentas de busca não consigam listá-la temporariamente.</small>
		</h4>
		<hr>
		<div class="row">
			<div class="col-md-12">
				<div class="form-inline"><label>Url*: </label>
				  <?php echo  Router::url('/', true); ?>
				  <input name="data[Slug][url]" row="1" placeholder="/espcial/dia-das-maes" class="form-control" type="text" value="<?php echo substr($this->request->data['Slug']['url'], 1) ?>" id="SlugUrl">
				  <input type="hidden" name="data[Slug][id]" class="form-control" value="<?php echo $this->request->data['Slug']['id'] ?>" id="SlugId">
				</div>
			</div>
		</div>
	</div>
	<br>

<?php
	echo $this->Form->input('Pagina.id');
	echo $this->Form->input('Pagina.descricao', array('row' => true, 'div' => 'col-md-4', 'label' => 'Descrição*'));
	echo $this->Form->input('Pagina.layout_ctp', array('row' => true, 'div' => 'col-md-4', 'label' => 'Layout CTP*', 'help' => array('text' => 'Use default ou clean (nativos da plataforma) ou crie um.',  'class' => 'col-md-12')));
	echo $this->Form->input('Pagina.arquivo_ctp', array('row' => true, 'div' => 'col-md-4', 'label' => 'Arquivo CTP*'));
?>
	<h4 class="form-titulos-admin">SEO</h4>
<?php

	$num_de_caracteres = '<span class="pull-right" id="num_de_caracteres_pagina">Nº de caracteres: 160</span>';
	echo $this->Form->input('title', array('row' => true, 'div' => 'col-md-4', 'label' => 'Title'));
	echo $this->Form->input('meta_title', array('row' => true, 'div' => 'col-md-4', 'label' => 'Meta Title'));
	echo $this->Form->input('meta_description', array('row' => true, 'div' => 'col-md-4', 'label' => 'Meta Description','placeholder' => 'Recomendável 160 caracteres', 'after' => $num_de_caracteres));

	echo $this->Form->actions();
	echo $this->Form->end();
?>
