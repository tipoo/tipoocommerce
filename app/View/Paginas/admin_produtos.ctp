<?php 
	$this->Html->css(array('Views/Paginas/admin_produtos'), 'stylesheet', array('inline' => false));
	$this->Html->script(array('Views/Paginas/admin_produtos'), array('inline' => false));
?>

<ol class="breadcrumb">
	<li ><?php echo $this->Html->link('Páginas', array('action' => 'backToPaginatorIndex', 'admin' => true)); ?></li>
	<li class="active">Produtos - <strong><?php echo $pagina['Pagina']['descricao'] ?></strong></li>
</ol>


<?php 
	echo $this->Form->create('PaginasProduto');
?>
<h3 class="form-titulos-admin">Adicionar produto</h3>
<?php
	echo $this->Form->input('pagina_id', array('type' => 'hidden'));
	echo $this->Form->input('produto_id', array('type' => 'hidden'));
	echo $this->Form->input('chave', array('row' => true, 'div' => 'col-md-4','type' => 'text'));
	echo $this->Form->input('descricao', array('row' => true, 'div' => 'col-md-8','class' => 'typeahead', 'autocomplete' => 'off', 'label' => 'Localizar produto', 'placeholder' => 'Digite o nome do produto'));
	echo $this->Form->end();
?>

<hr>
<?php
	if ($pagina['PaginasProduto']) {
?>

		<div class="table-responsive">
			<table class="table table-striped table-hover">
				<thead>
					<tr>
						<th class="col-md-1 text-center">#</th>
						<th class="col-md-5">Descrição</th>
						<th class="col-md-5">Chave</th>
						<th class="text-center"><?php echo $this->Html->link('<span class="glyphicon glyphicon-plus"></span>', array('action' => 'adicionar'), array('class' => 'btn btn-sm btn-primary', 'escape' => false,'data-placement' => 'left', 'rel' => 'tooltip', 'title' => 'Adicionar Novo')); ?></th>
					</tr>
				</thead>

				<tbody>
					<?php
						foreach ($pagina['PaginasProduto'] as $paginasProduto) {
					?>
						<tr>
							<td class="text-center"><?php echo $paginasProduto['Produto']['id'] ?></td>
							<td><?php echo $paginasProduto['Produto']['descricao'] ?></td>
							<td><?php echo $paginasProduto['chave'] ?></td>
							<td class="text-center">
								<div class="btn-group">
									<button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
										<span class="caret"></span>
									</button>
									<ul class="dropdown-menu pull-right text-left" role="menu">
										<li>
											<?php
												echo $this->Form->postLink('<span class="glyphicon glyphicon-trash"></span> Excluir', array('action' => 'excluir_produto', $pagina['Pagina']['id'], $paginasProduto['id']), array('escape' => false), 'Deseja realmente excluir esse produto da Página?');
											 ?>
										</li>
									</ul>
								</div>
							</td>
						</tr>
					<?php
						}
					?>
				</tbody>
			</table>
		</div>

<?php
	}
?>