<?php
	$this->Html->script(array('Views/Paginas/admin_adicionar'), array('inline' => false));
?>

<ol class="breadcrumb">
	<li><?php echo $this->Html->link('Páginas', array('action' => 'backToPaginatorIndex', 'admin' => true)); ?></li>
	<li class="active">Adicionar</li>
</ol>

<?php
	echo $this->Form->create('Pagina');
	echo $this->Form->input('Pagina.descricao', array('row' => true, 'div' => 'col-md-4', 'label' => 'Descrição*'));
	echo $this->Form->input('Slug.url', array('row' => true, 'div' => 'col-md-4','type' => 'text','label' => 'Url*', 'placeholder' => '/espcial/dia-das-maes'));
	echo $this->Form->input('Pagina.layout_ctp', array('row' => true, 'div' => 'col-md-4', 'label' => 'Layout CTP*', 'help' => array('text' => 'Use default ou clean (nativos da plataforma) ou crie um.',  'class' => 'col-md-12')));
	echo $this->Form->input('Pagina.arquivo_ctp', array('row' => true, 'div' => 'col-md-4', 'label' => 'Arquivo CTP*'));
?>
	<h4 class="form-titulos-admin">SEO</h4>
<?php

	$num_de_caracteres = '<span class="pull-right" id="num_de_caracteres_pagina">Nº de caracteres: 160</span>';
	echo $this->Form->input('title', array('row' => true, 'div' => 'col-md-4', 'label' => 'Title'));
	echo $this->Form->input('meta_title', array('row' => true, 'div' => 'col-md-4', 'label' => 'Meta Title'));
	echo $this->Form->input('meta_description', array('row' => true, 'div' => 'col-md-4', 'label' => 'Meta Description','placeholder' => 'Recomendável 160 caracteres', 'after' => $num_de_caracteres));

	echo $this->Form->actions();
	echo $this->Form->end();
?>
