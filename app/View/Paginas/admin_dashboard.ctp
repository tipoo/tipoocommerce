<?php
$this->Html->scriptBlock("var dataChartTermosMaisBuscados = " . $termos_buscados_json . ";", array('inline' => false));

$this->Html->script(array(
	'highcharts/js/highcharts.js',
	'Views/Paginas/admin_dashboard'
), array('inline' => false));
?>

<ol class="breadcrumb">
	<li>
		<h5>Olá, <strong><?php echo $usuarioAutenticado['nome'] ?></strong>. Veja abaixo o resumo das informações da loja <strong><?php echo Configure::read('Loja.nome') ?></strong>.</h5>
	</li>
</ol>

<div class="panel panel-default">
	<div class="panel-heading">
		<h4 class="panel-title">Vendas</h4>
	</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="well">
					<strong>Receita Bruta (c/ frete)</strong>
					<br />
					<p class="lead text-success">R$ <?php echo number_format($receita_bruta, 2, ',', '.') ?></p>
				</div>
			</div>
			<div class="col-md-4">
				<div class="well">
					<strong>Ticket Médio</strong>
					<br />
					<p class="lead text-success">R$ <?php echo number_format($ticket_medio, 2, ',', '.') ?></p>
				</div>
			</div>
			<div class="col-md-4">
				<div class="well">
					<strong>Quantidade de Vendas</strong>
					<br />
					<p class="lead text-success"><?php echo $qtd_vendas ?></p>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="panel panel-default" style="display: none">
	<div class="panel-heading">
		<h4 class="panel-title">Estatísticas</h4>
	</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-6">
				<div class="well">
					<strong>Vendas</strong>

				</div>
			</div>
			<div class="col-md-6">
				<div class="chart-termos-mais-buscados">
					<!-- Gráfico termos mais buscados --> 
				</div>
			</div>
		</div>
	</div>
</div>


<div class="panel panel-default">
	<div class="panel-heading">
		<h4 class="panel-title">Catálogo</h4>
	</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-3">
				<div class="well">
					<strong>Produtos Ativos</strong>
					<br />
					<p class="lead text-success"><?php echo $qtd_produtos ?></p>
				</div>
			</div>
			<div class="col-md-3">
				<div class="well">
					<strong>SKU's Ativos</strong>
					<br />
					<p class="lead text-success"><?php echo $qtd_skus ?></p>
				</div>
			</div>
			<div class="col-md-3">
				<div class="well">
					<strong>Categorias Ativas</strong>
					<br />
					<p class="lead text-success"><?php echo $qtd_categorias ?></p>
				</div>
			</div>
			<div class="col-md-3">
				<div class="well">
					<strong>Marcas Ativas</strong>
					<br />
					<p class="lead text-success"><?php echo $qtd_marcas ?></p>
				</div>
			</div>
		</div>
	</div>
</div>