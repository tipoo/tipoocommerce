<?php
$this->Html->script(array('Views/Paginas/admin_index'), array('inline' => false));
?>

<ol class="breadcrumb">
	<li class="active">Páginas</li>
</ol>

<div class="row filtro">
	<div class="col-md-11 col-lg-12">
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="row">
					<div class="col-md-3">
						<label>&nbsp;</label>
						<div class="input-group">
							<?php echo $this->Form->input('Filtro.pagina', array( 'type' => 'text', 'placeholder' =>'Busca por página' ,'label' => false, 'div' => false)); ?>
							<span class="input-group-btn">
								<?php echo $this->Form->button('<span class="glyphicon glyphicon-search"></span>', array('id' =>'FiltrarBuscaPagina','class' => 'btn btn-default buscar', 'type' => 'button')); ?>
							</span>
						</div>
					</div>
					<div class="col-md-2">
						<?php echo $this->Form->input('Todas', array('type' => 'button', 'label' => '&nbsp;', 'div' => false, 'class' => 'btn btn-primary limpar', 'id' => 'btnLimpar')); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="table-responsive">
	<table class="table table-striped table-hover">
		<thead>
			<tr>
				<th class="col-md-1 text-center">#</th>
				<th class="col-md-2">Descrição</th>
				<th class="col-md-4">Url</th>
				<th class="col-md-2">Layout CTP</th>
				<th class="col-md-2">Arquivo CTP</th>
				<th class="text-center"><?php echo $this->Html->link('<span class="glyphicon glyphicon-plus"></span>', array('action' => 'adicionar'), array('class' => 'btn btn-sm btn-primary', 'escape' => false,'data-placement' => 'left', 'rel' => 'tooltip', 'title' => 'Adicionar Novo')); ?></th>
			</tr>
		</thead>

		<tbody>
			<?php
				foreach ($paginas as $pagina) {
			?>
				<tr>
					<td class="text-center"><?php echo $pagina['Pagina']['id'] ?></td>
					<td><?php echo $pagina['Pagina']['descricao'] ?></td>
					<td><?php echo $pagina['Slug']['url'] ?></td>
					<td><?php echo $pagina['Pagina']['layout_ctp'] ?></td>
					<td><?php echo $pagina['Pagina']['arquivo_ctp'] ?></td>
					<td class="text-center">
						<div class="btn-group">
							<button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu pull-right text-left" role="menu">
								<?php
									if (!$pagina['Pagina']['sistema']) {
								?>
									<li> <?php echo $this->Html->link('<span class="glyphicon glyphicon-edit"></span> Editar', array('action' => 'editar', $pagina['Pagina']['id']), array('escape' => false));?></li>
									<li> <?php echo $this->Form->postLink('<span class="glyphicon glyphicon-trash"></span> Excluir', array('action' => 'excluir', $pagina['Pagina']['id'], $pagina['Slug']['id']), array('escape' => false), 'Deseja realmente excluir a Pagina #' . $pagina['Pagina']['id'] . '?');?></li>
								<?php
									} else {

									?>
										<li class="disabled"><a href="javascript: void(0)" data-placement="left" rel="tooltip" title="" data-original-title="Não é possível editar esta página, pois a mesma é nativa da plataforma."><span class="glyphicon glyphicon-edit"></span> Editar</a></li>
										<li class="disabled"><a href="javascript: void(0)" data-placement="left" rel="tooltip" title="" data-original-title="Não é possível excluir esta página, pois a mesma é nativa da plataforma."><span class="glyphicon glyphicon-trash"></span> Excluir</a></li>
								<?php
									}
								?>

								<?php
									if ($this->Permissoes->check(array('controller' => 'vitrines', 'action' => 'colecoes', 'admin' => true, 'plugin' => null))) {
								?>
										<li> <?php echo $this->Html->link('<span class="glyphicon"></span> Vitrines de Coleções', array('controller' => 'vitrines', 'action' => 'colecoes', 'pagina_id' => $pagina['Pagina']['id']), array('escape' => false));?></li>
								<?php
							 		}
								?>

								<?php
									if ($this->Permissoes->check(array('controller' => 'vitrines', 'action' => 'htmls', 'admin' => true, 'plugin' => null))) {
								?>
										<li> <?php echo $this->Html->link('<span class="glyphicon"></span> Texto/Html', array('controller' => 'vitrines', 'action' => 'htmls', 'pagina_id' => $pagina['Pagina']['id']), array('escape' => false));?></li>
								<?php
							 		}
								?>
							</ul>
						</div>
					</td>
				</tr>
			<?php
				}
			?>
		</tbody>
	</table>
</div>

<?php echo $this->element('Paginacao/admin_paginacao'); ?>