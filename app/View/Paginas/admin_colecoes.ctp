<?php
	$this->Html->css(array('Views/Paginas/admin_colecoes'), 'stylesheet', array('inline' => false));
	$this->Html->script(array('Views/Paginas/admin_colecoes'), array('inline' => false));
?>

<ol class="breadcrumb">
	<li><?php echo $this->Html->link('Páginas', array('action' => 'backToPaginatorIndex', 'admin' => true)); ?></li>
	<li  class="active">Coleções - <strong><?php echo $pagina['Pagina']['descricao'] ?></strong></li>
</ol>

<?php
	echo $this->Form->create('ColecoesLocal');
?>
<h3 class="form-titulos-admin">Adicionar Coleção</h3>
<?php
	echo $this->Form->input('pagina_id', array('type' => 'hidden'));
	echo $this->Form->input('colecao_id', array('type' => 'hidden'));
	echo $this->Form->input('descricao', array('row' => true, 'div' => 'col-md-4','type' => 'text'));
	echo $this->Form->input('chave', array('row' => true, 'div' => 'col-md-4','type' => 'text'));
	echo $this->Form->input('colecao', array('row' => true, 'div' => 'col-md-8', 'class' => ' typeahead', 'autocomplete' => 'off', 'label' => 'Localizar coleção', 'placeholder' => 'Digite o nome do coleção'));
	echo $this->Form->end();
?>

<hr>
<?php
	if ($pagina['ColecoesLocal']) {
?>
		<div class="table-responsive">
			<table class="table table-striped table-hover">
				<thead>
					<tr>
						<th class="col-md-1 text-center">#</th>
						<th class="col-md-4">Descrição</th>
						<th class="col-md-4">Coleção</th>
						<th class="col-md-2">Chave</th>
						<th class="col-md-1 "></th>
					</tr>
					<?php
						foreach ($pagina['ColecoesLocal'] as $paginasColecao) {
					?>
						<tr>
							<td class="text-center"><?php echo $paginasColecao['Colecao']['id'] ?></td>
							<td><?php echo $paginasColecao['descricao'] ?></td>
							<td><?php echo $paginasColecao['Colecao']['descricao'] ?></td>
							<td><?php echo $paginasColecao['chave'] ?></td>
							<td class="text-center">
								<div class="btn-group">
									<button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
										<span class="caret"></span>
									</button>
									<ul class="dropdown-menu pull-right text-left" role="menu">
										<li>
											<?php
												echo $this->Form->postLink('<span class="glyphicon glyphicon-trash"></span> Excluir', array('action' => 'excluir_colecao', $pagina['Pagina']['id'], $paginasColecao['id']), array('escape' => false), 'Deseja realmente excluir essa coleção da Página?');
											 ?>
										</li>
									</ul>
								</div>
							</td>
						</tr>
					<?php
						}
					?>
				</tbody>
			</table>
		</div>

<?php
	}
?>