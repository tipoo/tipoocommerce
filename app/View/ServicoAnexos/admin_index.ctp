<ol class="breadcrumb">
	<li><?php echo $this->Html->link('Serviços', array('action' => 'backToPaginatorIndex', 'Servicos', 'admin' => true)); ?></li>
	<li class="active">Anexos</li>
</ol>

<div class="table-responsive">
	<table class="table table-striped table-hover">
		<thead>
			<tr>
				<th class="col-md-1 text-center">#</th>
				<th class="col-md-7">Nome</th>
				<th class="col-md-3">Tipo</th>
				<th class="text-center"><?php echo $this->Html->link('<span class="glyphicon glyphicon-plus"></span>', array('action' => 'adicionar', $servico_id), array('class' => 'btn btn-primary btn-sm','data-placement' => 'left', 'escape' => false, 'rel' => 'tooltip', 'title' => 'Adicionar Novo')); ?></th>
			</tr>
		</thead>

		<tbody>
			<?php
				foreach ($servico_anexos as $anexo) {
			?>
				<tr>
					<td class="text-center"><?php echo $anexo['ServicoAnexo']['id'] ?></td>
					<td><?php echo $anexo['ServicoAnexo']['nome'] ?></td>
					<td><?php echo $tipos[$anexo['ServicoAnexo']['tipo']] ?></td>
					<td class="text-center">
						<div class="btn-group">
							<button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu pull-right text-left" role="menu">
								<li><?php echo $this->Html->link('<span class="glyphicon glyphicon-edit"></span> Editar', array('action' => 'editar', $anexo['ServicoAnexo']['id']), array('escape' => false)); ?></li>
								<li><?php echo $this->Form->postLink('<span class="glyphicon"></span> Excluir', array('action' => 'excluir', $anexo['ServicoAnexo']['id']), array('escape' => false), 'Deseja realmente excluir o Anexo #' . $anexo['ServicoAnexo']['id'] . '?');?></li>
							</ul>
						</div>
					</td>
				</tr>
			<?php
				}
			?>
		</tbody>
	</table>
</div>

<?php echo $this->element('Paginacao/admin_paginacao'); ?>