<?php
	$this->Html->script(array('Views/ServicoAnexos/admin_adicionar'), array('inline' => false));
?>

<ol class="breadcrumb">
	<li><?php echo $this->Html->link('Serviços', array('controller' => 'servicos', 'action' => 'backToPaginatorIndex', 'admin' => false)); ?></li>
	<li><?php echo $this->Html->link('Anexos', array('action' => 'backToPaginatorIndex', 'admin' => false)); ?></li>
	<li class="active">Adicionar</li>
</ol>

<?php
	echo $this->Form->create('ServicoAnexo');
	echo $this->Form->input('nome', array('row' => true, 'div' => 'col-md-4', 'label' => 'Nome*'));
	echo $this->Form->input('tipo', array('row' => true, 'div' => 'col-md-4', 'type' => 'select', 'empty' => 'Selecione', 'label' => 'Tipo'));
	echo $this->Form->input('valores_possiveis', array('row' => true, 'div' => 'col-md-4', 'label' => 'Valores Possíveis'/*, 'after' => '<span class="text-muted">Em caso de seleção única ou multipla, os valores possíveis devem estar separados 1 por linha.</span>'*/));
	echo $this->Form->actions();
	echo $this->Form->end();
?>