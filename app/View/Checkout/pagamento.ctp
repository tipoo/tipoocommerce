<?php
	$this->Html->css(array('Views/Checkout/pagamento'), 'stylesheet', array('inline' => false));
	$this->Html->script(array(
		'validate/jquery.validate.min',
		'validate/jquery.validate.additional',
		'validate/messages_ptbr',
		'jquery.maskedinput.min',
		'Views/Checkout/pagamento'),
	array('inline' => false));
?>
<div id="cadastro-container" class="pagamento">
	<h1>Pagamento</h1>

	<!-- Resumo do Pedido -->
	<h2>Dados de pagamento</h2>
	<input class="t-valor-total" type="hidden" value="<?php echo $pedido['Pedido']['valor_total'] ?>">

	<div class="display-table">

		<div>
			<p><strong>Número do Pedido: <?php echo $pedido['Pedido']['id'] ?> </strong></p>
			<p><strong>Data do Pedido: </strong><?php echo $this->Formatacao->dataHora($pedido['Pedido']['data_hora']) ?></p>

			<table class="table carrinho-skus" style="display: none;">
				<thead>
					<tr>
						<th colspan="2" class="col-produtos">Produto</th>
						<th class="col-quantidade">Quantidade</th>
						<th class="col-val-unit">Valor Unitário</th>
						<th class="col-val-total">Valor Total</th>
					</tr>
				</thead>
				<tbody>
					<?php
						foreach ($pedido['PedidoSku'] as $sku) {
							if (isset($sku['Imagem'][0]['imagem'])) {
								$imagem_produto = $sku['Sku']['Imagem'][0]['imagem'];
							} else {
								$imagem_produto = $sku['Sku']['Produto']['Imagem'][0]['imagem'];
							}
					?>
							<tr class="produtos-skus">
								<td class="col-img">
									<img src="<?php echo $this->webroot ?>files/<?php echo PROJETO ?>/produtos/<?php echo $this->Configuracoes->get('img_carrinho') ?>_<?php echo $imagem_produto ?>">
								</td>
								<td class="col-produtos">
									<span><?php echo $sku['descricao_produto'] . ' - ' . $sku['descricao_sku'] ?></span>
								</td>

								<td class="col-quantidade">
									<?php echo $sku['qtd']; ?>
								</td>

								<td class="col-val-unit">
									<?php
										if ($sku['preco_unitario_desconto'] == '') {
									?>
											<?php echo $this->Formatacao->moeda($sku['preco_unitario']); ?>
									<?php
										} else {
									?>
											<small class="desconto"><?php echo $this->Formatacao->moeda($sku['preco_unitario']); ?></small>
											<?php echo $this->Formatacao->moeda($sku['preco_unitario_desconto']); ?>
									<?php
										}
									?>

								</td>

								<td class="col-val-total">
									<?php echo $this->Formatacao->moeda($sku['preco_total']); ?>
								</td>
							</tr>
					<?php
						}
					?>
				</tbody>
			</table>

			<table class="table resumo-pedido">
				<tr class="valor-total">
					<td>Valor total dos produtos</td>
					<td></td>
					<td></td>
					<td></td>
					<td><?php echo $this->Formatacao->moeda($pedido['Pedido']['valor_produtos']); ?></td>
				</tr>
				<tr class="linha-branca valor-servicos">
					<td>Valor total dos serviços adicionais</td>
					<td></td>
					<td></td>
					<td></td>
					<td><?php echo $this->Formatacao->moeda($pedido['Pedido']['valor_servicos']); ?></td>
				</tr>
				<tr class="linha-branca valor-desconto">
					<td>Valore total do desconto</td>
					<td></td>
					<td></td>
					<td></td>
					<td><?php echo $this->Formatacao->moeda($pedido['Pedido']['valor_desconto'], array('before' => 'R$ -')); ?></td>
				</tr>
				<tr class="linha-branca valor-frete">
					<td>Despesas com frete</td>
					<td></td>
					<td></td>
					<td></td>
					<td><?php echo $this->Formatacao->moeda($pedido['Pedido']['valor_frete']); ?></td>
				</tr>
				<tr class="linha-branca valor-total">
					<th>Valor total a Pagar</th>
					<td></td>
					<td></td>
					<td></td>
					<td><?php echo $this->Formatacao->moeda($pedido['Pedido']['valor_total']); ?></td>
				</tr>
			</table>

			<div class="endereco-entrega" style="display: none">
				<div>
					<p><strong><?php echo $pedido['FreteTransportadora']['descricao'] ?></strong></p>
					<p>Entrega em até <?php echo $pedido['Pedido']['prazo_frete'] ?> dia(s) útil(eis)</p>

					<h3>Endereço de Entrega</h3>

					<p>
						<?php
							if ($pedido['Endereco']['complemento'] != '') {
								$complemento = ' - complemento ' . $pedido['Endereco']['complemento'];
							} else {
								$complemento = '';
							}
							echo $pedido['Endereco']['endereco'] . ', n&ordm; ' . $pedido['Endereco']['numero'] . $complemento;
						?>
						<br />
						<?php echo $pedido['Endereco']['bairro']; ?>,
						<br />
						<?php echo $pedido['Endereco']['Cidade']['nome'] . '/' . $pedido['Endereco']['Cidade']['uf']; ?>,
						<br />
						<?php echo $pedido['Endereco']['cep']; ?>
					</p>
				</div>
			</div>
		</div>
	</div>

	<!-- /Resumo do Pedido -->

	<?php
	if ($this->Configuracoes->get('cielo_ativo')) {
		echo $this->element('Checkout/cielo');
	} else if (isset($dados_moip)) {
		echo $this->element('Checkout/moip');
	} else if ($this->Configuracoes->get('pagseguro_transparente_ativo')) {
		echo $this->element('Checkout/pagseguro_transparente');
	}
	?>


</div>