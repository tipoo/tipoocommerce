<?php
	$this->Html->css(array('Views/Checkout/compra-finalizada'), 'stylesheet', array('inline' => false));
	$this->Html->script(array('Views/Checkout/compra-finalizada'), array('inline' => false));
?>

<?php
	if (isset($pedido) && count($pedido) > 0) {
?>
<script>
dataLayer = [{
	'transactionId': '<?php echo $pedido['Pedido']['id'] ?>',
	'transactionAffiliation': 'Tipoo Commerce',
	'transactionTotal': <?php echo $pedido['Pedido']['valor_produtos'] - $pedido['Pedido']['valor_desconto'] ?>,
	'transactionShipping': <?php echo $pedido['Pedido']['valor_frete'] ?>,
	'transactionProducts': [
		<?php
			foreach ($pedido['PedidoSku'] as $sku) {
		?>
				{
					'sku': '<?php echo $sku['sku']; ?>',
					'name': '<?php echo $sku['descricao_produto'] ?> - <?php echo $sku['descricao_sku']; ?>',
					'category': '<?php echo $sku['Sku']['Produto']['Categoria']['descricao'] ?>',
					'price': <?php echo $sku['preco_total'] ?>,
					'quantity': <?php echo $sku['qtd'] ?>
				},
		<?php
			}
		?>
	]
}];
</script>
<?php
	}
?>

<!-- Moip -->

<?php
	if (isset($dados_moip) && $dados_moip['forma'] == 'BoletoBancario') {
?>
		<p class="txt-moip-boleto">Pedido realizado com sucesso. Efetue o pagamento do boleto abaixo para finalizar a sua compra.</p>
		<p class="txt-numero-pedido">Pedido nº <span><?php echo $dados_moip['pedido_id'] ?></span></p>
		<p class="txt-processamento">Este pagamento está sendo processado por <a href="https://site.moip.com.br/" target="_blank">Moip Pagamentos</a></p>
		<iframe src="<?php echo $dados_moip['retorno']->url ?>" border="0" width="660" height="500"></iframe>

<?php
	} else if (isset($dados_moip) && $dados_moip['forma'] == 'CartaoCredito') {
?>
		<p class="txt-moip-cartao-credito">Pedido realizado com sucesso. Seu pagamento está sendo processado.</p>
		<p class="txt-numero-pedido">Pedido nº <span><?php echo $dados_moip['pedido_id'] ?></span></p>
		<p class="txt-moip-dados">Código Moip: <span><?php echo $dados_moip['retorno']->CodigoMoIP ?></span> - Status Moip: <span><?php echo $dados_moip['retorno']->Status ?></span></p>
		<p class="txt-processamento">Este pagamento está sendo processado por <a href="https://site.moip.com.br/" target="_blank">Moip Pagamentos</a></p>
<?php
	}
?>

<!-- PagSeguro -->

<?php
	if ((isset($pedido) && isset($dados_pagseguro)) && $pedido['Pedido']['forma_pagseguro'] == 'BoletoBancario') {
?>
		<p class="txt-pagseguro-transparente-boleto">Pedido realizado com sucesso. Efetue o pagamento do boleto abaixo para finalizar a sua compra.</p>
		<p class="txt-numero-pedido">Pedido nº <span><?php echo $pedido['Pedido']['id'] ?></span></p>
		<div class="boleto pagseguro-transparente-boleto">
			<iframe src="<?php echo $dados_pagseguro->paymentLink ?>" border="0" width="660" height="500"></iframe>
		</div>
<?php
	} else if ((isset($pedido) && isset($dados_pagseguro)) && $pedido['Pedido']['forma_pagseguro'] == 'DebitoBancario') {
?>
		<p class="txt-pagseguro-transparente-debito-bancario">Pedido realizado com sucesso. Efetue o pagamento através de seu banco abaixo para finalizar a sua compra.</p>
		<p class="txt-numero-pedido">Pedido nº <span><?php echo $pedido['Pedido']['id'] ?></span></p>
		<div class="debito-bancario pagseguro-transparente-debito-bancario">
			<iframe src="<?php echo $dados_pagseguro->paymentLink ?>" border="0" width="660" height="500"></iframe>
		</div>
<?php
	} else if ((isset($pedido) && isset($dados_pagseguro)) && $pedido['Pedido']['forma_pagseguro'] == 'CartaoCredito') {
?>
		<p class="txt-pagseguro-transparente-cartao-credito">Pedido realizado com sucesso. Seu pagamento está sendo processado.</p>
		<p class="txt-numero-pedido">Pedido nº <span><?php echo $pedido['Pedido']['id'] ?></span></p>
		<p class="txt-pagseguro-transparente-dados">Código PagSeguro: <span><?php echo $dados_pagseguro->code ?></span></p>
		<div class="txt-pagseguro-transparente-status">
			<p>Status PagSeguro: <span><?php echo $pedido['StatusPedido']['descricao'] ?></span></p>
		</div>
<?php
	}
?>

<!-- Moip -->

<!-- Cielo -->
<div class="mensagem-alerta mensagem-pagamento mensagem-cielo">
	<tipoo:mensagem chave="cielo" />
</div>
<!-- .end Cielo -->

<p class="txt-meus-pedidos">Para mais detalhes do seu pedido, acesse a área Meus Pedidos clicando no botão abaixo.</p>
<?php echo $this->Html->link('Meus pedidos', array('controller' => 'clientes', 'action' => 'meus_pedidos'), array('class' => 'bt-meus-pedidos')); ?>
