<?php
	$this->Html->script(array(
			'validate/jquery.validate.min',
			'validate/jquery.validate.additional',
			'validate/messages_ptbr',
			'jquery.maskedinput.min',
			'Views/Checkout/entrega'
		),
		array('inline' => false)
	);

	$this->Html->css(array('Views/Checkout/entrega'), 'stylesheet', array('inline' => false));
?>
<div id="cadastro-container" class="entrega">
	<h1>Entrega<span><?php //echo $this->Html->link('Adicionar novo endereço', array('controller' => 'clientes', 'action' => 'adicionar_endereco', '?' => array('redirect' => '/checkout/entrega'))) ?></span></h1>

	<?php
		foreach ($enderecos as $endereco) {
	?>

			<div class="enderecos">
				<div class="content">
					<p>
						<strong><?php echo $endereco['Endereco']['descricao'] ?></strong>
					</p>
					<br />
					<p>
						Endereço: <?php echo $endereco['Endereco']['endereco'] . ', ' . $endereco['Endereco']['numero'] . ' - ' . $endereco['Endereco']['complemento']; ?>
						<br />
						Bairro: <?php echo  $endereco['Endereco']['bairro']; ?>
						<br />
						Cidade: <?php echo $endereco['Cidade']['nome'] . ' - ' . $endereco['Cidade']['uf'] ?>
						<br />CEP: <?php echo $endereco['Endereco']['cep']; ?>
					</p>

					<!-- Frete -->
					<div class="frete">
						<?php
							if (isset($endereco['FreteTransportadora']) && count($endereco['FreteTransportadora'])) {
						?>
								<div class="frete-transportadoras">
									<h5>Selecione a forma de entrega:</h5>
									<ul class="transportadoras">
										<?php
											foreach ($endereco['FreteTransportadora'] as $frete_transportadora) {
												$transportadora_nome = $frete_transportadora['FreteTransportadora']['descricao'];
												$transportadora_prazo_entrega = $frete_transportadora['FreteTransportadora']['prazo_entrega_frete'];

												if ($transportadora_valor_frete = $frete_transportadora['FreteTransportadora']['valor_frete'] <= 0) {
													$transportadora_valor_frete = 'Frete Grátis';
													$transportadora_valor_total_frete = $this->Formatacao->moeda($endereco['Pedido']['valor_total']);
												} else {
													$transportadora_valor_frete = $this->Formatacao->moeda($frete_transportadora['FreteTransportadora']['valor_frete']);
													$transportadora_valor_total_frete = $this->Formatacao->moeda($endereco['Pedido']['valor_total'] + $frete_transportadora['FreteTransportadora']['valor_frete']);
												}
										?>
												<li class="transportadora transportadora-<?php echo strtolower(Inflector::slug($frete_transportadora['FreteTransportadora']['descricao'], '-')); ?>">
													<p class="transportadora-nome"><strong><?php echo $transportadora_nome; ?></strong></p>
													<p class="transportadora-valor-frete">Valor do frete: <span><?php echo $transportadora_valor_frete; ?></span></p>
													<p class="transportadora-valor-total-frete">Valor total com frete: <span><?php echo $transportadora_valor_total_frete; ?></span></p>
													<p class="transportadora-prazo-entrega">Prazo de entrega: <span><?php echo $transportadora_prazo_entrega; ?></span> <span>dia(s) útil(eis)</span></p>
													<?php echo $this->Html->link('Finalizar compra', array('controller' => 'checkout', 'action' => 'entrega', $endereco['Endereco']['id'], $frete_transportadora['FreteTransportadora']['id']), array('class' => 't-finalizar-compra')); ?>
												</li>
										<?php
											}
										?>
									</ul>
								</div>
						<?php
							} else {
						?>
								<div class="endreco-sem-entrega">
									<h5>Selecione a forma de entrega:</h5>
									<p>Não possuimos entrega para este endereço.</p>
								</div>
						<?php
							}
						?>
					</div>
					<!-- .end Frete -->
				</div>
			</div>
	<?php
		}
	?>
</div>

<div class="display-table">
	<div class="endereco endereco-adicionar">
		<h2>Adicionar Novo Endereço</h2>
		<div class="content">
			<?php
				echo $this->Form->create('Endereco', array('url' => array('controller' => 'clientes', 'action' => 'adicionar_endereco', '?' => array('redirect' => '/checkout/entrega'))));
			?>
			<div class="container-endereco-left">
				<?php
					echo $this->Form->input('Endereco.descricao', array('label' => 'Descrição*'));
					$nao_sei_meu_cep = '<a id="nao-sei-meu-cep" href="http://www.buscacep.correios.com.br/" target="_blank">Não sei meu CEP</a>';
					echo $this->Form->input('Endereco.cep', array('label' => 'CEP*', 'after' => $nao_sei_meu_cep));
				?>
				<div class="dados-endereco" style="display: none;">
					<?php
						echo $this->Form->input('Endereco.endereco', array('label' => 'Endereço*', 'type' => 'text'));
						echo $this->Form->input('Endereco.numero', array('label' => 'Número*'));
					?>
				</div>
				<p class="bt busca-cep">Buscar CEP</p>
				<div class="cep-erro">
					<p></p>
					<a href="<?php echo Router::url('/faleconosco'); ?>" target="_blank">Entre em contato</a>
				</div>
			</div>
			<div class="container-endereco-right">
				<div class="dados-endereco" style="display: none;">
					<?php
						echo $this->Form->input('Endereco.complemento', array('label' => 'Complemento'));
						echo $this->Form->input('Endereco.bairro', array('label' => 'Bairro*'));
						echo $this->Form->input('Endereco.cidade', array('label' => 'Cidade*', 'readonly' => true));
						echo $this->Form->input('Endereco.estado', array('label' => 'Estado*', 'readonly' => true));
						echo $this->Form->input('Endereco.cidade_id', array('type' => 'hidden'));
					?>
				</div>
			</div>
			<?php
				echo $this->Form->submit('Salvar', array('class' => 'bt-salvar'));
				echo $this->Form->end();
			?>
		</div>
	</div>
</div>