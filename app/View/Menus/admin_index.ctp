
<ol class="breadcrumb">
	<li class="active"><span>Menus</span></li>
</ol>

<div class="table-responsive">
	<table class="table table-striped table-hover">
		<thead>
			<tr>
				<th class="col-md-1 text-center">#</th>
				<th class="col-md-10">Descrição</th>
				<th class="text-right">
					<?php echo $this->Html->link('<span class="glyphicon glyphicon-plus"></span>', array('action' => 'adicionar'), array('class' => 'btn btn-sm btn-primary', 'escape' => false,'data-placement' => 'bottom', 'rel' => 'tooltip', 'title' => 'Adicionar Novo')); ?>
				</th>
			</tr>
		</thead>

		<tbody>
			<?php
				foreach ($menus as $menu) {
			?>
				<tr>
					<td class="text-center"><?php echo $menu['Menu']['id'] ?></td>
					<td><?php echo $menu['Menu']['descricao'] ?>
						<br>
						<?php
							if ($menu['Menu']['ativo']) {
								$status = '<span class="label label-success">Ativo</span> ';
							} else {
								$status = '<span class="label label-danger">Inativo</span>';
							}

							echo $status;
						?>
					</td>
					<td class="text-right">
						<div class="btn-group">
							<button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu pull-right text-left" role="menu">
								<li><?php echo $this->Html->link('<span class="glyphicon glyphicon-edit"></span> Editar', array('action' => 'editar', $menu['Menu']['id']), array('escape' => false));?></li>
								<li><?php echo $this->Html->link('<span class="glyphicon "></span>Itens do menu', array('controller' => 'itemMenus','action' => 'index', $menu['Menu']['id']), array('escape' => false));?></li>
							 	<?php 
									if ($menu['Menu']['ativo']) {
								?>
										<li><?php echo $this->Form->postLink('<span class="glyphicon"></span> Desativar', array('action' => 'excluir', $menu['Menu']['id']), array('escape' => false));?></li>
								<?php

									} else {
								?>
										<li><?php echo $this->Form->postLink('<span class="glyphicon"></span> Ativar', array('action' => 'ativar', $menu['Menu']['id']), array('escape' => false));?></li>

								<?php
									}
							 	 ?>

							</ul>
						</div>
					</td>
				</tr>
			<?php
				}
			?>
		</tbody>
	</table>
</div>

<?php echo $this->element('Paginacao/admin_paginacao'); ?>