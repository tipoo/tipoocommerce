<?php
	$this->Html->script(array('Views/Menus/admin_editar'), array('inline' => false));
?>

<ol class="breadcrumb">
	<li><?php echo $this->Html->link('Menus', array('action' => 'backToPaginatorIndex', 'admin' => true)); ?></li>
	<li class="active">Editar</li>
</ol>


<?php
	echo $this->Form->create('Menu');
	echo $this->Form->input('descricao', array('row' => true, 'div' => 'col-md-4','type' => 'text', 'label' => 'Nome*'));
	echo $this->Form->input('ativo', array('row' => true, 'div' => 'col-md-2', 'type' => 'checkbox', 'label' => 'Ativo'));
	echo $this->Form->input('classe_css', array('row' => true, 'div' => 'col-md-4','type' => 'text', 'label' => 'Classe Css'));
	echo $this->Form->actions();
	echo $this->Form->end();
?>