<?php
$this->Html->script(array('Views/StatusPedidos/admin_index'), array('inline' => false));
?>

<ol class="breadcrumb">
	<li class="active">Status de Pedidos</li>
</ol>

<div class="alert alert-info">
	<i class="glyphicon glyphicon-info-sign"></i> <strong>ORDENAÇÃO: </strong> Arraste para ordenar os status.
</div>

<div class="table-responsive">
	<table class="table table-striped table-hover">
		<thead>
			<tr>
				<th class="col-md-1 text-center">#</th>
				<th class="col-sm-2 col-md-4">Descrição</th>
				<th class="col-md-2">Notifica Cliente</th>
				<th class="col-md-2">Layout E-mail</th>
				<th class="col-md-2">Template E-mail</th>
				<th class="visible-xs visible-sm"></th>
				<th class="visible-xs visible-sm"></th>
				<th class="text-center"><?php echo $this->Html->link('<span class="glyphicon glyphicon-plus"></span>', array('action' => 'adicionar'), array('class' => 'btn btn-sm btn-primary', 'escape' => false,'data-placement' => 'left', 'rel' => 'tooltip', 'title' => 'Adicionar Novo')); ?></th>
			</tr>
		</thead>

		<tbody id="sortable">
			<?php
				foreach ($statusPedidos as $statusPedido) {
			?>
					<tr class="ui-state-default" data-id="<?php echo $statusPedido['StatusPedido']['id']; ?>">
						<td class="text-center"><?php echo $statusPedido['StatusPedido']['id'] ?></td>
						<td><?php echo $statusPedido['StatusPedido']['descricao'] ?></td>
						<td><?php echo $statusPedido['StatusPedido']['email_notifica_cliente'] ? 'Sim' : 'Não' ?></td>
						<td><?php echo $statusPedido['StatusPedido']['email_layout'] ?></td>
						<td><?php echo $statusPedido['StatusPedido']['email_template'] ?></td>
						<td class="visible-xs visible-sm text-center seta-ordenacao" >
							<div class="ordenacao-desce " data-indice="<?php echo $statusPedido['StatusPedido']['id']; ?>"><span class="glyphicon glyphicon-arrow-down"></span></div>
						</td>
						<td class="visible-xs visible-sm text-center seta-ordenacao" >
							<div class="ordenacao-sobe " data-indice="<?php echo $statusPedido['StatusPedido']['id']; ?>"><span class="glyphicon glyphicon-arrow-up"></span></div>
						</td>
						<td class="text-center">
							<div class="btn-group">
								<button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu pull-right text-left" role="menu">
									<li><?php echo $this->Html->link('<span class="glyphicon glyphicon-edit"></span> Editar', array('action' => 'editar', $statusPedido['StatusPedido']['id']), array('escape' => false)); ?></li>
									<li><?php echo $this->Form->postLink('<span class="glyphicon glyphicon-trash"></span> Excluir', array('action' => 'excluir', $statusPedido['StatusPedido']['id']), array('escape' => false), 'Deseja realmente excluir o Status de Pedido #' . $statusPedido['StatusPedido']['id'] . '?'); ?></li>
								</ul>
							</div>
						</td>
					</tr>
			<?php
				}
			?>
		</tbody>
	</table>
</div>

<?php echo $this->element('Paginacao/admin_paginacao'); ?>