<?php
	$this->Html->script(array('Views/StatusPedidos/admin_editar'), array('inline' => false));
?>

<ol class="breadcrumb">
	<li><?php echo $this->Html->link('Status de Pedidos', array('action' => 'backToPaginatorIndex', 'admin' => true)); ?></li>
	<li class="active">Editar</li>
</ol>

<?php
	echo $this->Form->create('StatusPedido');
	echo $this->Form->input('id');
?>

<div class="row">
	<div class="col-md-6">
		<?php
			echo $this->Form->input('descricao', array('row' => true, 'div' => 'col-md-12', 'label' => 'Descrição*'));
			echo $this->Form->input('email_assunto', array('row' => true, 'div' => 'col-md-12', 'label' => 'Assunto do e-mail'));
		?>
		<div class="row">
			<?php
				echo $this->Form->input('email_notifica_cliente', array('row' => false, 'div' => 'col-md-4', 'type' => 'checkbox', 'label' => 'Notifica cliente'));
				echo $this->Form->input('email_notifica_loja', array('row' => false, 'div' => 'col-md-8', 'type' => 'checkbox', 'label' => 'Notifica Loja'));
			?>
		</div>
		<?php
			echo $this->Form->input('email_layout', array('row' => true, 'div' => 'col-md-10', 'label' => 'Layout do e-mail'));
			echo $this->Form->input('email_template', array('row' => true, 'div' => 'col-md-10', 'label' => 'Template do e-mail'));
		?>
		<div class="row">
			<?php
				echo $this->Form->input('status_inicial', array('row' => false, 'div' => 'col-md-4','type' => 'checkbox', 'label' => 'Status Inicial'));
				echo $this->Form->input('status_finalizado', array('row' => false, 'div' => 'col-md-4','type' => 'checkbox', 'label' => 'Status Finalizado'));
				echo $this->Form->input('status_cancelado', array('row' => false, 'div' => 'col-md-4','type' => 'checkbox', 'label' => 'Status Cancelado'));
			?>
		</div>
		<div class="row">
			<?php
				echo $this->Form->input('confirma_sku_pedido', array('row' => false, 'div' => 'col-md-4','type' => 'checkbox', 'label' => 'Confirmar Pedido'));
				echo $this->Form->input('retorna_estoque', array('row' => false, 'div' => 'col-md-4','type' => 'checkbox', 'label' => 'Retornar Estoque'));
				echo $this->Form->input('venda_concluida', array('row' => false, 'div' => 'col-md-4','type' => 'checkbox', 'label' => 'Venda Concluída'));
			?>
		</div>
		<?php
			echo $this->Form->input('status_cor', array('row' => true, 'div' => 'col-md-4', 'label' => 'Cor do Status'));
			echo $this->Form->input('codigo_rastreamento', array('row' => true, 'div' => 'col-md-12','type' => 'checkbox', 'label' => 'Código de Rastreamento'));
		?>
	</div>
	<div class="col-md-6">
		<!-- Cielo -->
		<div class="well">
			<h4 class="form-titulos-admin">Cielo</h4>
			<?php
				echo $this->Form->input('status_cielo_trans_aut', array('row' => true, 'div' => 'col-md-12','type' => 'checkbox', 'label' => 'Transação Autorizada'));
				echo $this->Form->input('status_cielo_trans_nao_aut', array('row' => true, 'div' => 'col-md-12','type' => 'checkbox', 'label' => 'Transação Não Autorizada'));
				echo $this->Form->input('status_cielo_requisicao_captura', array('row' => true, 'div' => 'col-md-12','type' => 'checkbox', 'label' => 'Requisição da Captura'));
			?>
		</div>
		<!-- .end Cielo -->

		<!-- PagSeguro -->
		<div class="well">
			<h4 class="form-titulos-admin">PagSeguro</h4>
			<?php
				echo $this->Form->input('status_pagseg_trans_aut', array('row' => true, 'div' => 'col-md-12','type' => 'checkbox', 'label' => 'Transação Autorizada'));
				echo $this->Form->input('status_pagseg_disponivel', array('row' => true, 'div' => 'col-md-12','type' => 'checkbox', 'label' => 'Disponível'));
				echo $this->Form->input('status_pagseg_em_disputa', array('row' => true, 'div' => 'col-md-12','type' => 'checkbox', 'label' => 'Em Disputa'));
				echo $this->Form->input('status_pagseg_devolvida', array('row' => true, 'div' => 'col-md-12','type' => 'checkbox', 'label' => 'Devolvida'));
				echo $this->Form->input('status_pagseg_trans_nao_aut_pagseguro', array('row' => true, 'div' => 'col-md-12','type' => 'checkbox', 'label' => 'Transação Não Autorizada'));
				echo $this->Form->input('status_pagseg_trans_nao_aut_inst_fin', array('row' => true, 'div' => 'col-md-12','type' => 'checkbox', 'label' => 'Transação Não Autorizada - Instituição Financeira'));
			?>
		</div>
		<!-- .end PagSeguro -->

		<!-- Moip -->
		<div class="well">
			<h4 class="form-titulos-admin">Moip (Checkout Transparente)</h4>
			<?php
				echo $this->Form->input('status_moip_pag_autorizado', array('row' => true, 'div' => 'col-md-12','type' => 'checkbox', 'label' => 'Pagamento Autorizado (1)'));
				echo $this->Form->input('status_moip_boleto_impresso', array('row' => true, 'div' => 'col-md-12','type' => 'checkbox', 'label' => 'Boleto Impresso (3)'));
				echo $this->Form->input('status_moip_pag_creditado', array('row' => true, 'div' => 'col-md-12','type' => 'checkbox', 'label' => 'Pagamento Creditado (4)'));
				echo $this->Form->input('status_moip_pag_nao_autorizado', array('row' => true, 'div' => 'col-md-12','type' => 'checkbox', 'label' => 'Pagamento Não Autorizado (5)'));
				echo $this->Form->input('status_moip_pag_estornado', array('row' => true, 'div' => 'col-md-12','type' => 'checkbox', 'label' => 'Pagamento Estornado (7)'));
				echo $this->Form->input('status_moip_pag_reembolsado', array('row' => true, 'div' => 'col-md-12','type' => 'checkbox', 'label' => 'Pagamento Reembolsado (9)'));
				echo $this->Html->link('Documentação Moip', 'https://labs.moip.com.br/parametro/statuspagamento/');
			?>
		</div>
		<!-- .end Moip -->

		<!-- Bling -->
		<div class="well">
			<h4 class="form-titulos-admin">Bling</h4>
			<?php
				echo $this->Form->input('status_bling_gera_nf', array('row' => true, 'div' => 'col-md-12','type' => 'checkbox', 'label' => 'Gera e envia Nota Fiscal para o cliente'));
				echo $this->Html->link('Documentação Bling', 'http://www.bling.com.br/manuais-api');
			?>
		</div>
		<!-- .end Bling -->

		<!-- AntiFraude -->
		<div class="well">
			<h4 class="form-titulos-admin">Anti-Fraude</h4>
			<?php
				echo $this->Form->input('status_clear_sale_start', array('row' => true, 'div' => 'col-md-12','type' => 'checkbox', 'label' => 'ClearSale Start'));
			?>
		</div>
		<!-- .end AntiFraude -->

		<!-- Mercado Livre -->
		<div class="well">
			<h4 class="form-titulos-admin">Mercado Livre (Marketplace)</h4>
			<?php
				echo $this->Form->input('status_mercado_livre_confirmed', array('row' => true, 'div' => 'col-md-12','type' => 'checkbox', 'label' => 'Status Inicial (confirmed)'));
				echo $this->Form->input('status_mercado_livre_payment_required', array('row' => true, 'div' => 'col-md-12','type' => 'checkbox', 'label' => '(payment_required)'));
				echo $this->Form->input('status_mercado_livre_payment_in_process', array('row' => true, 'div' => 'col-md-12','type' => 'checkbox', 'label' => 'Pagamento Autorizado (payment_in_process)'));
				echo $this->Form->input('status_mercado_livre_paid', array('row' => true, 'div' => 'col-md-12','type' => 'checkbox', 'label' => 'Pagamento Creditado (paid)'));
				echo $this->Form->input('status_mercado_livre_cancelled', array('row' => true, 'div' => 'col-md-12','type' => 'checkbox', 'label' => 'Cancelado (cancelled)'));
			?>

			<h4 class="form-titulos-admin">Frete</h4>
			<?php
				echo $this->Form->input('status_mercado_livre_frete_pending', array('row' => true, 'div' => 'col-md-12','type' => 'checkbox', 'label' => 'Status Inicial (pending)'));
				echo $this->Form->input('status_mercado_livre_frete_ready_top_ship', array('row' => true, 'div' => 'col-md-12','type' => 'checkbox', 'label' => 'Pronto para Entrega (ready_top_ship)'));
				echo $this->Form->input('status_mercado_livre_frete_shipped', array('row' => true, 'div' => 'col-md-12','type' => 'checkbox', 'label' => 'Enviado (shipped) - Código de Rastreamento'));
				echo $this->Form->input('status_mercado_livre_frete_delivered', array('row' => true, 'div' => 'col-md-12','type' => 'checkbox', 'label' => 'Entregue (delivered)'));
				echo $this->Form->input('status_mercado_livre_frete_not_delivered', array('row' => true, 'div' => 'col-md-12','type' => 'checkbox', 'label' => 'Não Entregue (not_delivered)'));
				echo $this->Form->input('status_mercado_livre_frete_cancelled', array('row' => true, 'div' => 'col-md-12','type' => 'checkbox', 'label' => 'Cancelado (cancelled)'));
			?>
		</div>
		<!-- .end Mercado Livre -->
	</div>
</div>

<?php
	echo $this->Form->actions();
	echo $this->Form->end();
?>