<?php
	$this->Html->script(array('Views/Lojas/admin_editar'), array('inline' => false));
	$this->Html->css(array('Views/Lojas/admin_adicionar'), 'stylesheet', array('inline' => false));
?>

<ol class="breadcrumb">
	<li><?php echo $this->Html->link('Lojas', array('action' => 'backToPaginatorIndex', 'admin' => true)); ?></li>
	<li class="active">Editar</li>
</ol>

<?php echo $this->Form->create('Loja'); ?>
<?php echo $this->Form->input('id'); ?>
<div class="row">
	<div class="col-md-11 col-lg-12">
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="row">
					<div class="col-md-3">
						<div class="input-group">
							<?php echo $this->Form->input('cep', array('type' => 'text', 'placeholder' =>'Digite o CEP' ,'label' => false,'div' => false)); ?>
							<span class="input-group-btn">
								<?php echo $this->Form->button('<span class="glyphicon glyphicon-search"></span>', array('id' =>'LojasBuscarCep','class' => 'btn btn-default buscar-cep', 'type' => 'button')); ?>
							</span>
						</div>
					</div>
				</div>
				<small><a id="nao-sei-meu-cep" href="http://www.buscacep.correios.com.br/" target="_blank"> Buscar CEP no site dos Correios</a></small>
				<br>
				<br>

				<div class="well add-logradouro hide">
					<h4>Cep não encontrado</h4>
					<p>
						Por favor, cadastre o logradouro 
						<?php echo $this->Html->link('aqui.', array('controller' => 'enderecos', 'action' => 'adicionar_logradouro'), array('escape' => false)); ?>
					</p>
				</div>

				<div class="dados-endereco">
					<hr>
					<?php
						echo $this->Form->input('descricao', array('row' => true, 'div' => 'col-md-4', 'label' => 'Descrição*'));
						echo $this->Form->input('endereco', array('row' => true, 'div' => 'col-md-6','label' => 'Endereço*'));
						echo $this->Form->input('numero', array('row' => true, 'div' => 'col-md-2','label' => 'Número*'));
						echo $this->Form->input('complemento', array('row' => true, 'div' => 'col-md-4', 'label' => 'Complemento'));
						echo $this->Form->input('bairro', array('row' => true, 'div' => 'col-md-4','label' => 'Bairro*'));
						echo $this->Form->input('cidade', array('row' => true, 'div' => 'col-md-4','label' => 'Cidade*', 'readonly' => true, 'value' => $loja['Cidade']['nome']));
						echo $this->Form->input('estado', array('row' => true, 'div' => 'col-md-4','label' => 'Estado*', 'readonly' => true, 'value' => $loja['Cidade']['Estado']['uf']));
						echo $this->Form->input('cidade_id', array('type' => 'hidden'));
					?>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="salvar-loja">
<?php
	echo $this->Form->actions();
	echo $this->Form->end();
?>
</div>



