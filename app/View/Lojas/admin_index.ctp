<ol class="breadcrumb">
	<li class="active">Lojas</li>
</ol>

<div class="table-responsive">
	<table class="table table-striped table-hover">
		<thead>
			<tr>
				<th class="col-md-1 text-center">#</th>
				<th class="col-md-4">Descrição</th>
				<th class="col-md-4">Cidade</th>
				<th class="col-md-2">Estado</th>
				<th class="text-center"><?php echo $this->Html->link('<span class="glyphicon glyphicon-plus"></span>', array('action' => 'adicionar'), array('class' => 'btn btn-sm btn-primary', 'escape' => false,'data-placement' => 'left', 'rel' => 'tooltip', 'title' => 'Adicionar Novo')); ?></th>
			</tr>
		</thead>

		<tbody>
			<?php
				foreach ($lojas_fisicas as $loja) {
			?>
				<tr>
					<td class="text-center"><?php echo $loja['Loja']['id'] ?></td>
					<td><?php echo $loja['Loja']['descricao'] ?></td>
					<td><?php echo $loja['Cidade']['nome'] ?></td>
					<td><?php echo $loja['Cidade']['Estado']['uf'] ?></td>
					<td class="text-center">
						<div class="btn-group">
							<button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu pull-right text-left" role="menu">
								<li><?php echo $this->Html->link('<span class="glyphicon glyphicon-edit"></span> Editar', array('action' => 'editar', $loja['Loja']['id']), array('escape' => false)); ?></li>
								<li><?php echo $this->Form->postLink('<span class="glyphicon glyphicon-trash"></span> Excluir', array('action' => 'excluir', $loja['Loja']['id']), array('escape' => false), 'Deseja realmente excluir a Loja #' . $loja['Loja']['id'] . '?'); ?></li>
							</ul>
						</div>
					</td>
				</tr>
			<?php
				}
			?>
		</tbody>
	</table>
</div>

<?php echo $this->element('Paginacao/admin_paginacao'); ?>
