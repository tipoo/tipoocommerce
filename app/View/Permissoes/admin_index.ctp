<?php
	$this->Html->script(array('Views/Permissoes/admin_index'), array('inline' => false));
?>

<ul class="breadcrumb">
	<li class="active">Permissões</li>
</ul>
<div class="row">
	<div class="col-md-11 col-lg-12">
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="col-md-3">
					<?php
						echo $this->Form->create('Permissao');
						echo $this->Form->input('perfil', array('empty' => 'Selecione', 'options' => $perfis));
						echo $this->Form->end();
					?>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="table-responsive">
	<table class="table table-striped table-hover">
		<thead>
			<tr>
				<th class="col-md-1 text-center">Plugin</th>
				<th class="col-md-5">Controller</th>
				<th class="col-md-5">Action</th>
				<th class="col-md-1 text-center">Permitir</th>
			</tr>
		</thead>

		<tbody>
			<?php
				foreach ($objetos as $objeto) {
			?>
				<tr>
					<td class="text-center">
						<?php echo $objeto['Objeto']['plugin']?>
					</td>
					<td>
						<?php echo $objeto['Objeto']['controller']?>
					</td>
					<td>
						<?php echo $objeto['Objeto']['action']?>
					</td>
					<td class="text-center">
						<input type="checkbox" class="checkBoxGeral" data-objeto-id="<?php echo $objeto['Objeto']['id']?>" id="Objeto<?php echo $objeto['Objeto']['id'];?>" value="<?php echo $objeto['Objeto']['id'];?>">
					</td>
				</tr>
			<?php
				 }
			 ?>
		</tbody>
	</table>
</div>
