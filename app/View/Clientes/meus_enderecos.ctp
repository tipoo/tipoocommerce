<?php
	$this->Html->css(array('Views/Clientes/meus-enderecos'), 'stylesheet', array('inline' => false));
	$this->Html->script(array(
			'validate/jquery.validate.min',
			'validate/jquery.validate.additional',
			'validate/messages_ptbr',
			'jquery.maskedinput.min',
			'Views/Clientes/meus-enderecos'
		),
		array('inline' => false)
	);
?>

<div id="painel-container" class="meus-enderecos">
	<h1>Meus Endereços</h1>

	<?php echo $this->element('Cadastro/menu_painel_controle') ?>

	<div class="painel-content">

		<div class="endereco-adicionar">
			<h2>Adicionar Novo Endereço</h2>
			<div class="form-endereco-adicionar">
				<?php
					echo $this->Form->create('Endereco', array('url' => array('controller' => 'clientes', 'action' => 'adicionar_endereco')));
				?>
				<div class="box-1">
					<?php
						echo $this->Form->input('Endereco.descricao', array('label' => 'Descrição*'));
						$nao_sei_meu_cep = '<a id="nao-sei-meu-cep" href="http://www.buscacep.correios.com.br/" target="_blank">Não sei meu CEP</a>';
						echo $this->Form->input('Endereco.cep', array('label' => 'CEP*', 'after' => $nao_sei_meu_cep));
					?>
					<div class="dados-endereco" style="display: none;">
						<?php
							echo $this->Form->input('Endereco.endereco', array('label' => 'Endereço*', 'type' => 'text'));
							echo $this->Form->input('Endereco.numero', array('label' => 'Número*'));
						?>
					</div>
					<p class="bt busca-cep">Buscar CEP</p>
					<div class="cep-erro">
						<p></p>
						<a href="<?php echo Router::url('/faleconosco'); ?>" target="_blank">Entre em contato</a>
					</div>
				</div>
				<div class="dados-endereco box-2" style="display: none;">
					<?php
						echo $this->Form->input('Endereco.complemento', array('label' => 'Complemento'));
						echo $this->Form->input('Endereco.bairro', array('label' => 'Bairro*'));
						echo $this->Form->input('Endereco.cidade', array('label' => 'Cidade*', 'readonly' => true));
						echo $this->Form->input('Endereco.estado', array('label' => 'Estado*', 'readonly' => true));
						echo $this->Form->input('Endereco.cidade_id', array('type' => 'hidden'));
					?>
				</div>
				<?php
					echo $this->Form->submit('Salvar', array('class' => 'bt-salvar'));
					echo $this->Form->end();
				?>
			</div>
		</div>

		<div class="mensagem-alerta">
			<tipoo:mensagem chave="endereco" />
		</div>

		<?php
			foreach ($enderecos as $endereco) {
		?>
				<div class="endereco">
					<p>
						<strong><?php echo $endereco['Endereco']['descricao'] ?></strong>
						<br />
						<p>Endereço: <?php echo $endereco['Endereco']['endereco'] . ', ' . $endereco['Endereco']['numero'] . ' - ' . $endereco['Endereco']['complemento']; ?>
						<br />
						Bairro: <?php echo  $endereco['Endereco']['bairro']; ?>
						<br />
						Cidade: <?php echo $endereco['Cidade']['nome'] . ' - ' . $endereco['Cidade']['uf'] ?>
						<br />CEP: <?php echo $endereco['Endereco']['cep']; ?>
					</p>

					<?php echo $this->Html->link('Alterar', array('controller' => 'clientes', 'action' => 'alterar_endereco', $endereco['Endereco']['id']), array('class' => 'bt-acoes-endereco editar')) ?>
					<?php echo $this->Form->postLink('Excluir', array('action' => 'excluir_endereco', $endereco['Endereco']['id']), array('class' => 'bt-acoes-endereco excluir'), 'Deseja realmente excluir este endereço?'); ?>
				</div>

		<?php
			}
		?>

	</div>
</div>