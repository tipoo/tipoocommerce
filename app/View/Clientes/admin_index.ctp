<?php
	$this->Html->script(array('Views/Clientes/admin_index'),array('inline' =>false));
?>

<ol class="breadcrumb">
	<li class="active">Clientes</li>
</ol>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-body">

				<div class="row">
					<div class="col-md-4">
						<?php echo $this->Form->input('Filtro.nome', array('label' => 'Nome', 'div' => false)); ?>
					</div>
					<div class="col-md-4">
						<?php echo $this->Form->input('Filtro.email', array('label' => 'Email', 'placeholder' => 'exemplo@exemplo.com.br', 'div' => false)); ?>
					</div>
					<div class="col-md-4">
						<label>Data de Nascimento</label>
						<div class="row">
							<div class="col-md-3">
								<?php echo $this->Form->input('Filtro.dia_aniversario', array('options' => $dia_aniversario, 'empty' => 'Dia' , 'label' => false, 'div' => false)); ?>
							</div>
							<div class="col-md-5">
								<?php echo $this->Form->input('Filtro.mes_aniversario', array('options' => $mes_aniversario, 'empty' => 'Mês' , 'label' => false, 'div' => false)); ?>
							</div>
							<div class="col-md-4">
								<?php echo $this->Form->input('Filtro.ano_aniversario', array('options' => $ano_aniversario, 'empty' => 'Ano' , 'label' => false, 'div' => false)); ?>
							</div>
						</div>
					</div>
				</div>
				<br>

				<div class="row">
					<div class="col-md-2">
						<?php echo $this->Form->input('Filtro.tipo_pessoa', array('options' => $tipo_pessoa, 'empty' => 'Todos', 'label' => 'Tipo de Pessoa', 'div' => false)); ?>
					</div>

					<div class="col-md-2 campo-genero">
						<?php echo $this->Form->input('Filtro.genero', array('options' => $sexos, 'empty' => 'Todos', 'label' => 'Gênero', 'div' => false)); ?>
					</div>

					<div class="col-md-3 campo-cpf">
						<label>&nbsp;</label>
						<div class="input-group">
							<?php echo $this->Form->input('Filtro.cpf', array('id' => 'FiltrarCpf', 'type' => 'text', 'placeholder' =>'CPF' ,'label' => false, 'div' => false)); ?>
							<span class="input-group-btn">
								<?php echo $this->Form->button('<span class="glyphicon glyphicon-search"></span>', array('id' =>'FiltrarBuscaCpf','class' => 'btn btn-default buscar', 'type' => 'button')); ?>
							</span>
						</div>
					</div>

					<div class="col-md-3 campo-cnpj">
						<label>&nbsp;</label>
						<div class="input-group">
							<?php echo $this->Form->input('Filtro.cnpj', array('id' => 'FiltrarCnpj', 'type' => 'text', 'placeholder' =>'CNPJ' ,'label' => false, 'div' => false)); ?>
							<span class="input-group-btn">
								<?php echo $this->Form->button('<span class="glyphicon glyphicon-search"></span>', array('id' =>'FiltrarBuscaCnpj','class' => 'btn btn-default buscar', 'type' => 'button')); ?>
							</span>
						</div>
					</div>
				</div>
				<br>

				<div class="row">
					<div class="col-md-2">
						<?php echo $this->Form->input('Filtro.status_cliente', array('options' => $status_cliente, 'empty' => 'Todos' , 'label' => 'Status do Cliente', 'div' => false)); ?>
					</div>

					<div class="col-md-2">
						<?php echo $this->Form->input('Filtro.receber_newsletter', array('options' => $receber_newsletter, 'empty' => 'Todos' , 'label' => 'Newsletter', 'div' => false)); ?>
					</div>
				</div>
				<br>

				<div class="row">
					<div class="col-md-2">
						<?php echo $this->Form->input('Filtro.cep', array('label' => 'Cep', 'div' => false)); ?>
					</div>

					<div class="col-md-2">
						<?php echo $this->Form->input('Filtro.estado_id', array('options' => $estados, 'empty' => 'Selecione', 'label' => 'Estado', 'div' => false)); ?>
					</div>

					<div class="col-md-3">
						<?php echo $this->Form->input('Filtro.cidade_id', array('options' => $cidade, 'empty' => 'Selecione', 'label' => 'Cidades', 'div' => false)); ?>
					</div>

					<div class="col-md-2">
						<?php echo $this->Form->input('Filtrar', array('type' => 'button', 'label' => '&nbsp;', 'div' => false, 'class' => 'btn btn-primary', 'id' => 'btnFiltrar')); ?>
					</div>

					<div class="col-md-2">
						<label class="control-label">&nbsp;</label>
						<?php
							$urlClientes = array('controller' => 'clientes', 'action' => 'baixar_csv', 'admin' => true, 'plugin' => null);

							if ($this->Permissoes->check($urlClientes)) {
								echo $this->Html->link('<span class="glyphicon glyphicon-download-alt"></span> Baixar CSV', $urlClientes, array('id' => 'baixar-csv', 'class ' => 'btn btn-success form-control', 'escape' => false, 'rel' => 'tooltip', 'title' => 'Download'));
							}
						?>
					</div>
				</div>
				<br>
			</div>
		</div>
	</div>
</div>


<h5>Total de clientes encontrados: <?php echo count($clientes);?></h5>

<div class="table-responsive">
	<table class="table table-striped table-hover">
		<thead>
			<tr>
				<th class="col-md-1 text-center">#</th>
				<th class="col-md-3">Nome</th>
				<th class="col-md-2">Cpf/Cnpj</th>
				<th class="col-md-2">E-mail</th>
				<th class="col-md-2">Cidade/UF</th>
				<th class="col-md-1">Data de Cadastro</th>
				<th class="col-md-1 text-center"></th>
			</tr>
		</thead>

		<tbody>
			<?php
				foreach ($clientes as $cliente) {

					$cliente_ativo = true;
					if (!$cliente['Cliente']['ativo']) {
						$cliente_ativo = false;
					}
			?>
					<tr>
						<td class="text-center"><?php echo $cliente['Cliente']['id']?></td>
						<td>
							<?php
								if ($cliente['Cliente']['nome'] != '') {
									echo $cliente['Cliente']['nome'];
								} else {
									echo $cliente['Cliente']['nome_fantasia'];
								}

								if ($cliente_ativo) {
									$status = '<span class="label label-success">Ativo</span> ';
								} else {
									$status = '<span class="label label-danger">Inativo</span>';
								}
							?>
							<p> <?php echo $status; ?> </p>

						</td>
						<td>
							<?php
								if ($cliente['Cliente']['tipo_pessoa'] == 'F') {
									echo $cliente['Cliente']['cpf'];
								} else {
									echo $cliente['Cliente']['cnpj'];
								}
							?>
						</td>
						<td><?php echo $cliente['Cliente']['email']?></td>
						<td>
							<?php
								foreach ($cliente['Endereco'] as $endereco) {
									if($endereco['descricao'] == 'Principal') {
										if (isset($endereco['Cidade']['nome'])) {
											echo $endereco['Cidade']['nome'];
										} else {
											echo 'Não Especificado';
										}
								?>
									/
								<?php
										if (isset($endereco['Cidade']['uf'])) {
											echo $endereco['Cidade']['uf'];
										} else {
											echo 'Não Especificado';
										}
								?>
									<br />
								<?php
									}
				 				}
			 				?>
						</td>
						<td><?php echo $this->Formatacao->data($cliente['Cliente']['created'])?></td>
						<td class="text-center">
							<div class="btn-group">
								<button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu pull-right text-left" role="menu">
									<li>
										<?php echo $this->Html->link('<span class="glyphicon"></span> Visualizar', array('action' => 'ajax_visualizar', $cliente['Cliente']['id']), array('class' => 'ver-detalhes-cliente', 'escape' => false));?>
									</li>

									<?php
										if ($cliente_ativo) {
									?>
											<li><?php echo $this->Form->postLink('<span class="glyphicon"></span> Desativar', array('action' => 'desativar', $cliente['Cliente']['id']), array('escape' => false));?></li>
									<?php
										} else {
									?>
											<li><?php echo $this->Form->postLink('<span class="glyphicon"></span> Ativar', array('action' => 'ativar', $cliente['Cliente']['id']), array('escape' => false));?></li>
									<?php
										}
									 ?>

								</ul>
							</div>
						</td>
					</tr>
			<?php
				}
			?>
		</tbody>
	</table>
</div>

<?php echo $this->element('Paginacao/admin_paginacao'); ?>