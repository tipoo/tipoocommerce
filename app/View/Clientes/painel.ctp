<?php
	$this->Html->css(array('Views/Clientes/painel'), 'stylesheet', array('inline' => false));
	$this->Html->script(array('Views/Clientes/painel'),	array('inline' => false));
?>

<div id="painel-container" class="painel">
	<h1>Painel de controle</h1>

	<?php echo $this->element('Cadastro/menu_painel_controle') ?>

	<div class="painel-content">
		<div class="mensagem-alerta">
			<?php echo $this->Session->flash('meus_dados'); ?>
		</div>
	</div>
</div>