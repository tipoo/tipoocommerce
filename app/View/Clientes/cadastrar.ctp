<?php
	$this->Html->css(array('Views/Clientes/cadastrar'), 'stylesheet', array('inline' => false));
	$this->Html->script(array(
			'validate/jquery.validate.min',
			'validate/jquery.validate.additional',
			'validate/messages_ptbr',
			'jquery.maskedinput.min',
			'jquery.price_format.2.0.min',
			'jquery.numeric',
			'Views/Clientes/cadastrar'
		),
		array('inline' => false)
	);
?>
<div id="cadastro-container"  class="cadastro">
	<div id="tipo-cadastro">
		<a href="#" class="bt-pessoa-fisica">Pessoa Física</a> | <a href="#" class="bt-pessoa-juridica">Pessoa Jurídica</a>
	</div>
	<?php
		echo $this->Form->create('Cliente', array('url' => array('controller' => 'clientes', 'action' => 'cadastrar', 'return' => $this->request->params['controller'])));
		echo $this->Form->input('tipo_pessoa', array('type' => 'hidden', 'value' => 'F'));
	?>

	<div class="container-cadastro-left">
		<div class="dados-pessoais">
			<h3>Dados Pessoais</h3>
			<div class="content">
				<?php
					echo $this->Form->input('nome', array('label' => 'Nome*'));
					echo $this->Form->input('data_nascto', array('type' => 'text', 'label' => 'Data de nascimento*'));
					echo $this->Form->input('sexo', array('type' => 'select', 'options' => $sexos));
					echo $this->Form->input('cpf', array('type' => 'text', 'label' => 'CPF*'));
					echo $this->Form->input('tel_residencial', array('type' => 'text', 'label' => 'Telefone residencial*'));
					echo $this->Form->input('tel_comercial_pf', array('type' => 'text', 'label' => 'Telefone comercial'));
					echo $this->Form->input('celular', array('type' => 'text', 'label' => 'Celular'));
				?>

				<!-- Campos Complementares -->
				<?php
					if (count($campos_complementares) > 0) {

						foreach ($campos_complementares as $key => $campo_complementar) {

							if ($campo_complementar['CamposComplementar']['referente'] == 'PF') {

								echo $this->Form->input('CamposComplementaresValoresSelecionado.' . $campo_complementar['CamposComplementar']['id'] . '.campos_complementar_id', array('type' => 'hidden', 'value' => $campo_complementar['CamposComplementar']['id']));
								echo $this->Form->input('CamposComplementaresValoresSelecionado.' . $campo_complementar['CamposComplementar']['id'] . '.campos_complementar_tipo', array('type' => 'hidden', 'value' => $campo_complementar['CamposComplementar']['tipo']));

								if ($campo_complementar['CamposComplementar']['tipo'] == 'T') {
									echo $this->Form->input('CamposComplementaresValoresSelecionado.' . $campo_complementar['CamposComplementar']['id'] . '.descricao', array('label' => $campo_complementar['CamposComplementar']['descricao'], 'type' => 'text', 'data-required' => $campo_complementar['CamposComplementar']['campo_obrigatorio'], 'class' => 'campo-complementar pf'));
								} else if ($campo_complementar['CamposComplementar']['tipo'] == 'TG') {
									echo $this->Form->input('CamposComplementaresValoresSelecionado.' . $campo_complementar['CamposComplementar']['id'] . '.descricao_grande', array('label' => $campo_complementar['CamposComplementar']['descricao'], 'type' => 'textarea', 'data-required' => $campo_complementar['CamposComplementar']['campo_obrigatorio'], 'class' => 'campo-complementar pf'));
								} else if ($campo_complementar['CamposComplementar']['tipo'] == 'L') {
									$lista_valores = array();
									foreach ($campo_complementar['CamposComplementaresValor'] as $campo_complementar_valor) {
										$lista_valores[$campo_complementar_valor['id']] = $campo_complementar_valor['descricao'];
									}
									echo $this->Form->input('CamposComplementaresValoresSelecionado.' . $campo_complementar['CamposComplementar']['id'] . '.campos_complementares_valor_id', array('empty' => 'Selecione', 'label' => $campo_complementar['CamposComplementar']['descricao'], 'options' => $lista_valores, 'data-required' => $campo_complementar['CamposComplementar']['campo_obrigatorio'], 'class' => 'campo-complementar pf'));
								} else {
									if ($campo_complementar['CamposComplementar']['mascara'] == 'I') {
										$mascara = 'mascara-inteiro';
									} else {
										$mascara = 'mascara-decimal';
									}
									echo $this->Form->input('CamposComplementaresValoresSelecionado.' . $campo_complementar['CamposComplementar']['id'] . '.descricao', array('class' => 'campo-complementar pf tipo-numero ' . $mascara, 'label' => $campo_complementar['CamposComplementar']['descricao'], 'data-required' => $campo_complementar['CamposComplementar']['campo_obrigatorio']));
								}
							}
						}

					}
				?>
				<!-- Fim - Campos Complementares -->
			</div>
		</div>
		<div class="dados-empresariais" style="display: none">
			<h3>Dados Empresariais</h3>
			<div class="content">
				<?php
					echo $this->Form->input('razao_social', array('label' => 'Razão social*'));
					echo $this->Form->input('nome_fantasia', array('label' => 'Nome fantasia*'));
					echo $this->Form->input('cnpj', array('type' => 'text', 'label' => 'CNPJ*'));
					echo $this->Form->input('inscricao_estadual', array('type' => 'text', 'label' => 'Inscrição estadual*'));
					echo $this->Form->input('tel_comercial_pj', array('type' => 'text', 'label' => 'Telefone comercial*'));
				?>

				<!-- Campos Complementares -->
				<?php
					if (count($campos_complementares) > 0) {

						foreach ($campos_complementares as $key => $campo_complementar) {

							if ($campo_complementar['CamposComplementar']['referente'] == 'PJ') {

								echo $this->Form->input('CamposComplementaresValoresSelecionado.' . $campo_complementar['CamposComplementar']['id'] . '.campos_complementar_id', array('type' => 'hidden', 'value' => $campo_complementar['CamposComplementar']['id']));
								echo $this->Form->input('CamposComplementaresValoresSelecionado.' . $campo_complementar['CamposComplementar']['id'] . '.campos_complementar_tipo', array('type' => 'hidden', 'value' => $campo_complementar['CamposComplementar']['tipo']));

								if ($campo_complementar['CamposComplementar']['tipo'] == 'T') {
									echo $this->Form->input('CamposComplementaresValoresSelecionado.' . $campo_complementar['CamposComplementar']['id'] . '.descricao', array('label' => $campo_complementar['CamposComplementar']['descricao'], 'type' => 'text', 'data-required' => $campo_complementar['CamposComplementar']['campo_obrigatorio'], 'class' => 'campo-complementar pj'));
								} else if ($campo_complementar['CamposComplementar']['tipo'] == 'TG') {
									echo $this->Form->input('CamposComplementaresValoresSelecionado.' . $campo_complementar['CamposComplementar']['id'] . '.descricao_grande', array('label' => $campo_complementar['CamposComplementar']['descricao'], 'type' => 'textarea', 'data-required' => $campo_complementar['CamposComplementar']['campo_obrigatorio'], 'class' => 'campo-complementar pj'));
								} else if ($campo_complementar['CamposComplementar']['tipo'] == 'L') {
									$lista_valores = array();
									foreach ($campo_complementar['CamposComplementaresValor'] as $campo_complementar_valor) {
										$lista_valores[$campo_complementar_valor['id']] = $campo_complementar_valor['descricao'];
									}
									echo $this->Form->input('CamposComplementaresValoresSelecionado.' . $campo_complementar['CamposComplementar']['id'] . '.campos_complementares_valor_id', array('empty' => 'Selecione', 'label' => $campo_complementar['CamposComplementar']['descricao'], 'options' => $lista_valores, 'data-required' => $campo_complementar['CamposComplementar']['campo_obrigatorio'], 'class' => 'campo-complementar pj'));
								} else {
									if ($campo_complementar['CamposComplementar']['mascara'] == 'I') {
										$mascara = 'mascara-inteiro';
									} else {
										$mascara = 'mascara-decimal';
									}
									echo $this->Form->input('CamposComplementaresValoresSelecionado.' . $campo_complementar['CamposComplementar']['id'] . '.descricao', array('class' => 'campo-complementar pj tipo-numero ' . $mascara, 'label' => $campo_complementar['CamposComplementar']['descricao'], 'data-required' => $campo_complementar['CamposComplementar']['campo_obrigatorio']));
								}
							}
						}

					}
				?>
				<!-- Fim - Campos Complementares -->
			</div>
		</div>
	</div>

	<div class="container-cadastro-right">
		<div class="endereco cadastrar">
			<h3>Endereço</h3>
			<div class="content">
				<?php
					$nao_sei_meu_cep = '<a id="nao-sei-meu-cep" href="http://www.buscacep.correios.com.br/" target="_blank">Não sei meu CEP</a>';
					echo $this->Form->input('Endereco.0.cep', array('label' => 'CEP*', 'after' => $nao_sei_meu_cep));
				?>
				<p class="bt busca-cep">Buscar CEP</p>
				<div class="cep-erro" style="display: none">
					<p></p>
					<a href="<?php echo Router::url('/faleconosco'); ?>" target="_blank">Entre em contato</a>
				</div>
				<div class="dados-endereco" style="display: none">
					<?php
						echo $this->Form->input('Endereco.0.descricao', array('type' => 'hidden', 'value' => 'Principal'));
						echo $this->Form->input('Endereco.0.endereco', array('label' => 'Endereço*'));
						echo $this->Form->input('Endereco.0.numero', array('label' => 'Número*'));
						echo $this->Form->input('Endereco.0.complemento', array('label' => 'Complemento'));
						echo $this->Form->input('Endereco.0.bairro', array('label' => 'Bairro*'));
						echo $this->Form->input('Endereco.0.cidade', array('label' => 'Cidade*', 'readonly' => true));
						echo $this->Form->input('Endereco.0.estado', array('label' => 'Estado*', 'readonly' => true));
						echo $this->Form->input('Endereco.0.cidade_id', array('type' => 'hidden'));
					?>
				</div>
			</div>
		</div>
		<div class="dados-acesso">
			<h3>Dados de acesso</h3>
			<div class="content">
				<?php
					echo $this->Form->input('email', array('label' => 'E-mail*'));
					echo $this->Form->input('email_confirmacao', array('label' => 'Confirmação de e-mail*'));
					echo $this->Form->input('senha', array('label' => 'Senha*', 'type' => 'password'));
					echo $this->Form->input('senha_confirmacao', array('label' => 'Confirmação de senha*', 'type' => 'password'));
					echo $this->Form->input('email_ofertas', array('label' => 'Desejo receber ofertas por e-mail', 'type' => 'checkbox', 'checked' => true));
					echo $this->Form->submit('Cadastrar', array('class' => 'bt-cadastrar'));
				?>
			</div>
		</div>
	</div>
	<?php
		echo $this->Form->end();
	?>
</div>