<?php
	if ($cliente['Cliente']['ativo'] ) {
		$status = '<span class="label label-success">Ativo</span> ';
	} else {
		$status = '<span class="label label-danger">Inativo</span>';
	}

	if ($cliente['Cliente']['receber_newsletter'] ) {
		$recebe_newsletter = 'Sim';
	} else {
		$recebe_newsletter = 'Não';
	}

 ?>
<p><strong>#<?php echo $cliente['Cliente']['id'] ?> </strong>  - <strong> Data de Cadastro : </strong> <?php echo $this->Formatacao->data($cliente['Cliente']['created']) ?> - <?php echo $status; ?></p>

<div class="row">
	<div class="col-sm-6">
		<?php
			if ($cliente['Cliente']['tipo_pessoa'] == 'F') {
		?>
				<h3>Pessoa Física</h3>

				<p><strong>Cliente : </strong><?php echo $cliente['Cliente']['nome'] ?></p>
				<p><strong>Data de nascimento : </strong><?php echo $this->Formatacao->data($cliente['Cliente']['data_nascto']) ?></p>
				<p><strong>Sexo : </strong><?php echo $sexos[$cliente['Cliente']['sexo']]?></p>
				<p><strong>E-mail : </strong><?php echo $cliente['Cliente']['email'] ?></p>
				<p><strong>CPF : </strong><?php echo $cliente['Cliente']['cpf'] ?></p>
				<p><strong>Tel Residencial : </strong><?php echo $cliente['Cliente']['tel_residencial'] ?></p>
				<p><strong>Tel Comercial : </strong><?php echo $cliente['Cliente']['tel_comercial_pf'] ?></p>
				<p><strong>Celular : </strong><?php echo $cliente['Cliente']['celular'] ?></p>
		<?php
			} else {
		?>
				<h3>Pessoa Jurídica</h3>
				<p><strong>Razão social : </strong><?php echo $cliente['Cliente']['razao_social'] ?></p>
				<p><strong>Nome fantasia : </strong><?php echo $cliente['Cliente']['nome_fantasia'] ?></p>
				<p><strong>CNPJ : </strong><?php echo $cliente['Cliente']['cnpj'] ?></p>
				<p><strong>Inscrição Estadual : </strong><?php echo $cliente['Cliente']['inscricao_estadual'] ?></p>
				<p><strong>CNPJ : </strong><?php echo $cliente['Cliente']['tel_comercial_pj'] ?></p>
				<p><strong>Tel Comercial: </strong><?php echo $cliente['Cliente']['cnpj'] ?></p>
				<p><strong>E-mail : </strong><?php echo $cliente['Cliente']['email'] ?></p>
		<?php
			}
		?>
		<p><strong>Recebe E-mail de Ofertas : </strong><?php echo $recebe_newsletter ?></p>
	</div>
	<div class="col-sm-6">
		<!-- Campos Complementares -->
		<?php
			if (isset($cliente['CamposComplementaresValoresSelecionado']) && count($cliente['CamposComplementaresValoresSelecionado']) > 0) {
		?>
				<h3 class="lead">Campos Complementares</h3>
				<div class="well">
					<?php
						foreach ($cliente['CamposComplementaresValoresSelecionado'] as $campo_complementar_valor_selecionado) {

							if ($campo_complementar_valor_selecionado['descricao'] != '') {
								$valor_selecionado = $campo_complementar_valor_selecionado['descricao'];
							} else if ($campo_complementar_valor_selecionado['descricao_grande'] != '') {
								$valor_selecionado = $campo_complementar_valor_selecionado['descricao_grande'];
							} else if ($campo_complementar_valor_selecionado['campos_complementares_valor_id'] != '' && count($campo_complementar_valor_selecionado['CamposComplementaresValor']) > 0) {
								$valor_selecionado = $campo_complementar_valor_selecionado['CamposComplementaresValor']['descricao'];
							} else {
								$valor_selecionado = '';
							}
					?>
							<p><strong><?php echo $campo_complementar_valor_selecionado['CamposComplementar']['descricao'] ?>: </strong><?php echo $valor_selecionado ?></p>
					<?php
						}
					?>
				</div>
		<?php
			}
		?>
		<!-- .end Campos Complementares -->
	</div>
</div>
<hr>
<h3>Endereços de Entrega</h3>
<div class="row">
	<?php
		foreach ($cliente['Endereco'] as $endereco) {
	?>

			<div class="col-sm-6">
				<div class="well">
					<p><strong>Descrição: </strong><?php echo $endereco['descricao'] ?></p>
					<p><strong>Cidade : </strong>
						<?php
							if (isset($endereco['Cidade']['nome'])) {
								echo $endereco['Cidade']['nome'];
							}
						?>
					</p>
					<p><strong>UF : </strong>
						<?php
							if (isset($endereco['Cidade']['uf'])) {
								echo $endereco['Cidade']['uf'];
							}
						?>
					</p>
					<p><strong>Bairro : </strong><?php echo $endereco['bairro'] ?></p>
					<p><strong>Endereço : </strong><?php echo $endereco['endereco'] ?>, nº <?php echo $endereco['numero'] ?></p>
					<p><strong>Complemento : </strong><?php echo $endereco['complemento'] ?></p>
					<p><strong>CEP : </strong><?php echo $endereco['cep'] ?></p>
				</div>
			</div>
	<?php
		}
	?>
</div>
<hr>

<h3>Histórico</h3>
<?php
if (isset($cliente['Pedido'][0])) {

	foreach ($cliente['Pedido'] as $key => $pedido) {
?>

	<?php
		$status_cor = '#333';
		if (!empty($pedido['StatusPedido']['status_cor'])) {
			$status_cor = $pedido['StatusPedido']['status_cor'];
		}
	?>
		<h5>Pedido #<?php echo $pedido['id'] ?> <span class="label" style="background: <?php echo $status_cor; ?>"><?php echo $pedido['StatusPedido']['descricao']; ?></span> <small> - data: <?php echo $this->Formatacao->dataHora($pedido['data_hora']) ?></small> </h5>
		<div class="well">
			<div class="table-responsive">
				<table class="table table-striped table-hover">
					<thead>
						<tr>
							<th>Produto</th>
							<th>Sku</th>
							<th>Qtd</th>
							<th>Valor Unitário</th>
							<th>Estoque</th>
							<th>Subtotal</th>
						</tr>
					</thead>

					<tbody>

					<?php
						$total = 0;
						foreach ($pedido['PedidoSku'] as $sku) {
					?>
							<tr>
								<td>
									<?php echo $sku['descricao_produto']; ?>
									<br />
									<?php echo $sku['descricao_marca']; ?>
									<br />
									<br />
									<?php
										foreach ($sku['PedidoSkuServico'] as $servico) {
									?>
											<small class="text-muted">
												<?php echo $servico['servico']; ?>
												<?php echo $servico['anexo'] == '' ? '' : ': ' . $servico['valor_anexo'] ?>
												(<?php echo $this->Formatacao->moeda($servico['valor']); ?>)
											</small>
											<br>
									<?php
										}
										if (isset($sku['CompreJuntoPromocao'][0])) {

									?>
											<small class="text-muted">Desconto: "Promoção Compre Junto"</small>
											<br>

									<?php

										}

										if (isset($sku['Promocao'][0])) {

											foreach ($sku['Promocao']as $promocao) {

									?>
												<small class="text-muted">Desconto: "Promoção <?php echo $promocao['descricao']?>"</small>
												<br>
									<?php
											}
										}
									?>
								</td>
								<td>
									<?php echo $sku['descricao_sku']; ?>
								</td>
								<td><?php echo $sku['qtd']; ?></td>
								<td>
								<?php
									if ($sku['preco_unitario_desconto'] == '') {

										if ($sku['Sku']['Produto']['preco_de'] != '') {
								?>
											<small class="preco-sem-desconto"><?php echo $this->Formatacao->moeda($sku['Sku']['Produto']['preco_de'])?></small>
											<br>
								<?php
										}
										echo $this->Formatacao->moeda($sku['preco_unitario']);
									} else {

										if ($sku['preco_unitario'] == '') {
								?>
											<small class="preco-sem-desconto"><?php echo $this->Formatacao->moeda($sku['Sku']['Produto']['preco_por']); ?></small>
								<?php
										} else {
								?>
											<small class="preco-sem-desconto"><?php echo $this->Formatacao->moeda($sku['preco_unitario']); ?></small>
								<?php
										}

								?>

										<br>
									<?php
										echo $this->Formatacao->moeda($sku['preco_unitario_desconto']);
									}
								 ?>
								</td>
								<td><?php echo $sku['situacao_estoque']; ?></td>
								<td><?php echo $this->Formatacao->moeda($sku['preco_total']); ?></td>
							</tr>

					<?php
							$total += $sku['preco_total'];
						}
					?>

					<?php

						$tem_desconto_pedido = false;
						if (isset($pedido['Promocao'])) {

							foreach ($pedido['Promocao'] as $promocao) {

								if ($promocao['element'] == 'desconto_pedido') {
									$tem_desconto_pedido = true;

									$desconto_pedido_descricao = '<small class="text-muted"> Promoção no Pedido : ' . $promocao['descricao'] . '</small>';

									if ($promocao['PromocaoEfeito']['tipo_valor'] == 'P') {
										$desconto_pedido_valor = $pedido['valor_produtos'] * $promocao['PromocaoEfeito']['valor'] / 100;
									} else {
										$desconto_pedido_valor = $promocao['PromocaoEfeito']['valor'];
									}

								}
							}
						}

						$tem_cupom = false;
						if (isset($pedido['Promocao'])) {

							foreach ($pedido['Promocao'] as $promocao) {

								if ($promocao['element'] == 'cupom_desconto') {
									$tem_cupom = true;

									$cupom_descricao = '<small class="text-muted"> Cupom de Desconto : ' . $promocao['descricao'] . ' (Cód. -' . $promocao['PromocaoCausa']['cupom'] . ')</small>';

									if ($promocao['PromocaoEfeito']['tipo_valor'] == 'P') {

										if ($tem_desconto_pedido) {
											$valor_parcial_pedido = $pedido['valor_produtos'] - $desconto_pedido_valor;
										} else {
											$valor_parcial_pedido = $pedido['valor_produtos'];
										}

										$cupom_valor = $valor_parcial_pedido * $promocao['PromocaoEfeito']['valor'] / 100;

									} else {
										$cupom_valor = $promocao['PromocaoEfeito']['valor'];
									}

								}
							}
						}
					?>
					<tr>
						<td>
							<?php
								if ($tem_desconto_pedido) {
									 echo $desconto_pedido_descricao;
 							?>
								<br>
							<?php
								}

								if ($tem_cupom) {
									 echo $cupom_descricao;
								}
							 ?>
						</td>
						<td></td>
						<td></td>
						<td>
						<?php
							if ($tem_desconto_pedido) {
						?>
							<small> - <?php echo $this->Formatacao->moeda($desconto_pedido_valor) ?></small>
							<br>
						<?php
							}
							if ($tem_cupom) {
						?>
							<small> - <?php echo $this->Formatacao->moeda($cupom_valor) ?></small>
						<br>
						<?php
							}
						?>
						</td>
						<th>Subtotal</th>
						<td>
						<?php
							$sub_total = 0;

							if (($pedido['valor_desconto'] == '' || !$tem_desconto_pedido) && !$tem_cupom) {
								$sub_total = $pedido['valor_produtos'];
							} else {
						?>
							<small class="preco-sem-desconto"><?php echo $this->Formatacao->moeda($pedido['valor_produtos']); ?></small>

							<br>
						<?php
								$sub_total = $pedido['valor_produtos'] - $pedido['valor_desconto'];
							}
						 	echo $this->Formatacao->moeda($sub_total);
						?>
						</td>
					</tr>
					<tr class="linha-branca">
						<td>
						<?php
							if (isset($pedido['Promocao'])) {

								foreach ($pedido['Promocao'] as $promocao) {

									if ($promocao['element'] == 'desconto_frete') {

						?>
										<small class="text-muted"> Promoção no Frete : " <?php echo $promocao['descricao']?>"</small>
						<?php
									}
								}

							}
						?>
						</td>
						<td></td>
						<td></td>
						<td></td>
						<th>Frete</th>
						<td><?php echo $this->Formatacao->moeda($pedido['valor_frete']); ?></td>
					</tr>
					<tr class="linha-branca">
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<th>Total</th>
						<td><?php echo $this->Formatacao->moeda($pedido['valor_total']); ?></td>
					</tr>
				</tbody>
			</table>
		</div>

	</div>
<?php
	}
} else {
?>
	<div class="well">
		<p class="sem-cadastro">Sem registro de compras.</p>
	</div>

<?php
}
?>

