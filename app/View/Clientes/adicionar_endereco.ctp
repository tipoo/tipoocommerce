<?php
	$this->Html->css(array('Views/Clientes/adicionar_endereco'), 'stylesheet', array('inline' => false));
	$this->Html->script(array(
			'validate/jquery.validate.min',
			'validate/jquery.validate.additional',
			'validate/messages_ptbr',
			'jquery.maskedinput.min',
			'Views/Clientes/adicionar_endereco'
		),
		array('inline' => false)
	);
?>

<div id="cadastro-container"  class="adicionar-endereco">
	<h1>Adicionar novo endereço</h1>

	<div class="display-table">
		<div class="endereco endereco-adicionar">
			<h2>Adicionar Novo Endereço</h2>
			<div class="content">
				<?php
					echo $this->Form->create('Endereco', array('url' => array('controller' => 'clientes', 'action' => 'adicionar_endereco')));
				?>
				<div class="container-endereco-left">
					<?php
						echo $this->Form->input('Endereco.descricao', array('label' => 'Descrição*'));
						echo $this->Form->input('Endereco.cep', array('label' => 'CEP*'));
					?>
					<div class="dados-endereco">
						<?php
							echo $this->Form->input('Endereco.endereco', array('label' => 'Endereço*', 'type' => 'text'));
							echo $this->Form->input('Endereco.numero', array('label' => 'Número*'));
						?>
					</div>
					<div class="cep-erro">
						CEP não encontrado
					</div>
				</div>
				<div class="container-endereco-right">
					<div class="dados-endereco">
						<?php
							echo $this->Form->input('Endereco.complemento', array('label' => 'Complemento'));
							echo $this->Form->input('Endereco.bairro', array('label' => 'Bairro*'));
							echo $this->Form->input('Endereco.cidade', array('label' => 'Cidade*', 'readonly' => true));
							echo $this->Form->input('Endereco.estado', array('label' => 'Estado*', 'readonly' => true));
							echo $this->Form->input('Endereco.cidade_id', array('type' => 'hidden'));
						?>
					</div>
				</div>
				<?php
					echo $this->Form->submit('Salvar', array('class' => 'bt-salvar'));
					echo $this->Form->end();
				?>
			</div>
		</div>
	</div>
</div>