<?php
	$this->Html->css(array('Views/Clientes/alterar-endereco'), 'stylesheet', array('inline' => false));
	$this->Html->script(array(
			'validate/jquery.validate.min',
			'validate/jquery.validate.additional',
			'validate/messages_ptbr',
			'Views/Clientes/alterar-endereco'
		),
		array('inline' => false)
	);
?>
<div id="painel-container"  class="alterar-endereco">
	<h1>Alterar endereço</h1>

	<?php echo $this->element('Cadastro/menu_painel_controle') ?>

	<div class="painel-content">
		<div class="endereco-editar">
			<h2>Alterar Endereço</h2>
			<div class="form-endereco-editar">
				<?php
					echo $this->Form->create('Endereco');
					echo $this->Form->input('Endereco.id');
				?>
				<div class="box-1">
					<?php
						echo $this->Form->input('Endereco.descricao', array('label' => 'Descrição*'));
						echo $this->Form->input('Endereco.cep', array('label' => 'CEP*', 'readonly' => true));
					?>
					<div class="dados-endereco">
						<?php
							echo $this->Form->input('Endereco.endereco', array('label' => 'Endereço*'));
							echo $this->Form->input('Endereco.numero', array('label' => 'Número*'));
						?>
					</div>
				</div>
				<div class="dados-endereco box-2">
					<?php
						echo $this->Form->input('Endereco.complemento', array('label' => 'Complemento'));
						echo $this->Form->input('Endereco.bairro', array('label' => 'Bairro*'));
						echo $this->Form->input('Cidade.nome', array('label' => 'Cidade*', 'readonly' => true));
						echo $this->Form->input('Cidade.Estado.nome', array('label' => 'Estado*', 'readonly' => true));
						echo $this->Form->input('Endereco.cidade_id', array('type' => 'hidden'));
					?>
				</div>
				<?php
					echo $this->Form->submit('Salvar', array('class' => 'bt-salvar'));
					echo $this->Form->end();
				?>
			</div>
		</div>
	</div>
</div>