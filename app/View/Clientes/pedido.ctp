<?php
	$this->Html->css(array('Views/Clientes/pedido'), 'stylesheet', array('inline' => false));
	$this->Html->script(array('Views/Clientes/pedido'),	array('inline' => false));
?>

<div id="painel-container" class="meus-pedidos">
	<h1>Meu Pedido</h1>

	<?php echo $this->element('Cadastro/menu_painel_controle') ?>

	<div class="painel-content">

		<div>

			<p><strong>Número do Pedido: <?php echo $pedido['Pedido']['id'] ?> </strong></p>
			<p><strong>Cliente: </strong><?php echo $pedido['Cliente']['nome'] ?></p>

			<p><strong>Data do Pedido: </strong><?php echo $this->Formatacao->dataHora($pedido['Pedido']['data_hora']) ?></p>
			<p><strong>Prazo: </strong><?php echo $pedido['Pedido']['prazo_frete'] ?> dia(s) útil(eis)</p>
			<p><strong>Transportadora: </strong><?php echo $pedido['FreteTransportadora']['descricao'] ?></p>

			<?php
				if ($pedido['Pedido']['codigo_rastreamento'] != '') {
			?>
					<p><strong>Código de Rastreamento: </strong><?php echo $pedido['Pedido']['codigo_rastreamento'] ?></p>
			<?php
				}
			?>

			<?php
				if (isset($nota_fiscal) && isset($nota_fiscal->retorno->notasfiscais[0]->notafiscal->linkDanfe) && $nota_fiscal->retorno->notasfiscais[0]->notafiscal->linkDanfe) {
			?>
					<p class="nota-fiscal"><strong>Nota fiscal: </strong><?php echo $this->Html->link('Imprimir', array('controller' => 'clientes', 'action' => 'notafiscal', $pedido['Pedido']['id']), array('target' => '_blank')) ?></p>
			<?php
				}
			?>

			<table class="table">
				<tr>
					<th>Endereço de Entrega</th>
				</tr>

				<tr>
					<td>
						<?php
							if ($pedido['Endereco']['complemento'] != '') {
								$complemento = ' - complemento ' . $pedido['Endereco']['complemento'];
							} else {
								$complemento = '';
							}
							echo $pedido['Endereco']['endereco'] . ', n&ordm; ' . $pedido['Endereco']['numero'] . $complemento;
						?>
						<br />
						<?php echo $pedido['Endereco']['bairro']; ?>,
						<br />
						<?php echo $pedido['Endereco']['Cidade']['nome'] . '/' . $pedido['Endereco']['Cidade']['uf']; ?>,
						<br />
						<?php echo $pedido['Endereco']['cep']; ?>
					</td>
				</tr>
			</table>

			<table class="table">
				<tr>
					<th>Produto</th>
					<th>Sku</th>
					<th>Qtd</th>
					<th>Valor Unitário</th>
					<th>Subtotal</th>
				</tr>
				<?php
					foreach ($pedido['PedidoSku'] as $sku) {
				?>
						<tr>
							<td>
								<span class="produto"><?php echo $sku['descricao_produto']; ?></span>
								<span class="marca"><?php echo $sku['descricao_marca']; ?></span>
								<div class="caracteristicas">
									<?php
										foreach ($sku['PedidoCaracteristica'] as $caracteristica) {
											if ($caracteristica['referente'] == 'P') {
												echo $caracteristica['caracteristica'] . ': '. $caracteristica['valor'] . '<br />';
											}
										}
									?>
								</div>

								<div class="servicos">
									<?php
										foreach ($sku['PedidoSkuServico'] as $servico) {
									?>
											<small class="text-muted">
												<?php echo $servico['servico']; ?>
												<?php echo $servico['anexo'] == '' ? '' : ': ' . $servico['valor_anexo'] ?>
												(<?php echo $this->Formatacao->moeda($servico['valor']); ?>)
											</small>
											<br>
									<?php
										}
									?>
								</div>

								<div class="promocoes">
									<?php
										if (isset($sku['CompreJuntoPromocao'][0])) {
									?>
											<div class="promocao-compre-junto">
												<small class="text-muted">Desconto: "Promoção Compre Junto"</small>
											</div>
									<?php

										}

										if (isset($sku['Promocao'][0])) {

											foreach ($sku['Promocao']as $promocao) {
									?>
												<div class="promocao-produto">
													<small class="text-muted">Desconto: "Promoção <?php echo $promocao['descricao']?>"</small>
												</div>
									<?php
											}
										}
									?>
								</div>
							</td>
							<td>
								<?php echo $sku['descricao_sku']; ?>
								<br />
								<br />
								<?php
									foreach ($sku['PedidoCaracteristica'] as $caracteristica) {
										if ($caracteristica['referente'] == 'S') {
											echo $caracteristica['caracteristica'] . ': '. $caracteristica['valor'] . '<br />';
										}
									}
								?>
							</td>
							<td><?php echo $sku['qtd']; ?></td>
							<td>
								<?php
									if ($sku['preco_unitario_desconto'] == '') {

										if ($sku['Sku']['Produto']['preco_de'] != '') {
								?>
											<small class="preco-sem-desconto"><?php echo $this->Formatacao->moeda($sku['Sku']['Produto']['preco_de'])?></small>
											<br>
								<?php
										}
										echo $this->Formatacao->moeda($sku['preco_unitario']);
									} else {

										if ($sku['preco_unitario'] == '') {
								?>
											<small class="preco-sem-desconto"><?php echo $this->Formatacao->moeda($sku['Sku']['Produto']['preco_por']); ?></small>
								<?php
										} else {
								?>
											<small class="preco-sem-desconto"><?php echo $this->Formatacao->moeda($sku['preco_unitario']); ?></small>
								<?php
										}

								?>

										<br>
									<?php
										echo $this->Formatacao->moeda($sku['preco_unitario_desconto']);
									}
								 ?>
							</td>
							<td><?php echo $this->Formatacao->moeda($sku['preco_total']); ?></td>
						</tr>
				<?php
					}
				?>

				<?php

					$tem_desconto_pedido = false;
					if (isset($pedido['Promocao'])) {

						foreach ($pedido['Promocao'] as $promocao) {

							if ($promocao['element'] == 'desconto_pedido') {
								$tem_desconto_pedido = true;

								$desconto_pedido_descricao = '<small class="text-muted"> Promoção no Pedido : ' . $promocao['descricao'] . '</small>';
								if ($promocao['PromocaoEfeito']['tipo_valor'] == 'P') {
									$desconto_pedido_valor = $pedido['Pedido']['valor_produtos'] * $promocao['PromocaoEfeito']['valor'] / 100;
								} else {
									$desconto_pedido_valor = $promocao['PromocaoEfeito']['valor'];
								}

							}
						}
					}
					$tem_cupom = false;
					if (isset($pedido['Promocao'])) {

						foreach ($pedido['Promocao'] as $promocao) {

							if ($promocao['element'] == 'cupom_desconto') {
								$tem_cupom = true;

								$cupom_descricao = '<small class="text-muted"> Cupom de Desconto : ' . $promocao['descricao'] . ' (Cód. -' . $promocao['PromocaoCausa']['cupom'] . ')</small>';
								if ($promocao['PromocaoEfeito']['tipo_valor'] == 'P') {

									if ($tem_desconto_pedido) {
										$valor_parcial_pedido = $pedido['Pedido']['valor_produtos'] - $desconto_pedido_valor;
									} else {
										$valor_parcial_pedido = $pedido['Pedido']['valor_produtos'];
									}

									$cupom_valor = $valor_parcial_pedido * $promocao['PromocaoEfeito']['valor'] / 100;

								} else {
									$cupom_valor = $promocao['PromocaoEfeito']['valor'];
								}

							}
						}
					}


				?>
				<tr>
					<td>
						<?php
							if ($tem_desconto_pedido) {
								 echo $desconto_pedido_descricao;
							?>
							<br>
						<?php
							}

							if ($tem_cupom) {
								 echo $cupom_descricao;
							}
						 ?>
					</td>
					<td></td>
					<td>
					<?php
						if ($tem_desconto_pedido) {
					?>
						<small> - <?php echo $this->Formatacao->moeda($desconto_pedido_valor) ?></small>
						<br>
					<?php
						}
						if ($tem_cupom) {
					?>
						<small> - <?php echo $this->Formatacao->moeda($cupom_valor) ?></small>
					<br>
					<?php
						}
					?>
					</td>
					<th>Subtotal</th>
					<td class="valor valor-desconto">
						<?php
							$sub_total = 0;
							if (($pedido['Pedido']['valor_desconto'] == '' || !$tem_desconto_pedido) && !$tem_cupom) {
								$sub_total = $pedido['Pedido']['valor_produtos'];
							} else {
						?>
								<small class="preco-sem-desconto"><?php echo $this->Formatacao->moeda($pedido['Pedido']['valor_produtos']); ?></small>
								<br>
						<?php
								$sub_total = $pedido['Pedido']['valor_produtos'] - $pedido['Pedido']['valor_desconto'];
							}
						 	echo $this->Formatacao->moeda($sub_total);
						?>
					</td>
				</tr>
				<tr class="linha-branca">
					<td></td>
					<td></td>
					<td></td>
					<th>Serviços</th>
					<td class="valor valor-servicos"><?php echo $this->Formatacao->moeda($pedido['Pedido']['valor_servicos']); ?></td>
				</tr>
				<tr class="linha-branca">
					<td>
					<?php
						if (isset($pedido['Promocao'])) {

							foreach ($pedido['Promocao'] as $promocao) {

								if ($promocao['element'] == 'desconto_frete') {
					?>
									<small class="text-muted"> Promoção no Frete : " <?php echo $promocao['descricao']?>"</small>
					<?php
								}
							}

						}
					?>
					</td>
					<td></td>
					<td></td>
					<th>Frete</th>
					<td class="valor valor-frete"><?php echo $this->Formatacao->moeda($pedido['Pedido']['valor_frete']); ?></td>
				</tr>
				<tr class="linha-branca">
					<td></td>
					<td></td>
					<td></td>
					<th>Total</th>
					<td class="valor valor-total"><?php echo $this->Formatacao->moeda($pedido['Pedido']['valor_total']); ?></td>
				</tr>
			</table>
		</div>


	</div>

</div>