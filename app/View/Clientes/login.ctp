<?php
	$this->Html->css(array('Views/Clientes/login'), 'stylesheet', array('inline' => false));
	$this->Html->script(array(
			'validate/jquery.validate.min',
			'validate/messages_ptbr',
			'Views/Clientes/login'
		),
		array('inline' => false)
	);
?>

<div id="cadastro-container" class="login">

	<!--  Mensagem referente ao login  -->
	<div class="mensagem-alerta mensagem-login">
		<tipoo:mensagem chave="login" />
	</div>
	<!--  /Mensagem referente ao login  -->

	<!--  Mensagem referente ao esqueci minha senha  -->
	<div class="mensagem-alerta mensagem-esqueci-minha-senha">
		<tipoo:mensagem chave="esqueci_minha_senha" />
	</div>
	<!--  /Mensagem referente ao esqueci minha senha  -->

	<!--  Mensagem referente ao cliente já cadastrado  -->
	<div class="mensagem-alerta mensagem-cliente-ja-cadastrado">
		<tipoo:mensagem chave="cliente_ja_cadastrado" />
	</div>
	<!--  /Mensagem referente ao cliente já cadastrado  -->

	<!--  Mensagem referente ao cupom de desconto  -->
	<div class="mensagem-alerta mensagem-cupom-desconto">
		<tipoo:mensagem chave="cupom_desconto" />
	</div>
	<!--  /Mensagem referente ao cupom de desconto  -->

	<h1>Identificação</h1>
	<div class="form-login">
		<h3>Já sou cadastrado</h3>
		<div class="content">
			<?php
				$esqueci_minha_senha = $this->Html->link('Esqueci minha senha', array('action' => 'esqueci_minha_senha'), array('class' => 'esqueci-minha-senha'));

				echo $this->Form->create('Cliente', array('url' => array('controller' => 'clientes', 'action' => 'login', 'return' => $this->request->params['controller'])));
				echo $this->Form->input('email', array('label' => 'E-mail:'));
				echo $this->Form->input('senha', array('label' => 'Senha:', 'type' => 'password', 'after' => $esqueci_minha_senha));
				echo $this->Form->end('Entrar');
			?>
		</div>
	</div>
	<div class="cadastre-se">
		<h3>Ainda não sou cadastrado</h3>
		<div class="content">
			<h4>Vantagens</h4>
			<ul>
				<li>Rápido e Fácil</li>
				<li>Entrega Garantida</li>
				<li>Site 100% seguro</li>
			</ul>
			<?php 
				if (isset($this->request->query['redirect'])) {
					echo $this->Html->link('Quero me cadastrar', '/clientes/cadastrar?redirect=' . $this->request->query['redirect'], array('class' => 'quero-me-cadastrar'));
				} else {
					echo $this->Html->link('Quero me cadastrar', array('action' => 'cadastrar'), array('class' => 'quero-me-cadastrar'));
				}
			?>
		</div>
	</div>
</div>