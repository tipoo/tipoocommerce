<?php
	$this->Html->css(array('Views/Clientes/meus-dados'), 'stylesheet', array('inline' => false));
	$this->Html->script(array(
			'validate/jquery.validate.min',
			'validate/jquery.validate.additional',
			'validate/messages_ptbr',
			'jquery.maskedinput.min',
			'jquery.price_format.2.0.min',
			'jquery.numeric',
			'Views/Clientes/meus-dados'
		),
		array('inline' => false)
	);
?>

<div id="painel-container" class="meus-dados">
	<h1>Meus dados</h1>
	<?php echo $this->element('Cadastro/menu_painel_controle') ?>

	<div class="mensagem-alerta">
		<tipoo:mensagem chave="meus_dados" />
	</div>

	<div class="painel-content">

		<?php
		if ($this->data['Cliente']['tipo_pessoa'] == 'F') {
		?>
			<div class="dados-cadastrais">
				<h2>Alterar dados cadastrais</h2>
				<div class="form-dados-cadastrais">
					<?php
						echo $this->Form->create('Cliente', array('id' => 'ClienteDadosCadastraisForm'));
						echo $this->Form->input('id', array('type' => 'hidden'));
						echo $this->Form->input('tipo_pessoa', array('type' => 'hidden'));
						echo $this->Form->input('nome', array('label' => 'Nome*'));
						echo $this->Form->input('email', array('label' => 'Email*'));
						echo $this->Form->input('data_nascto', array('type' => 'text', 'label' => 'Data de nascimento*', 'value' => $this->Formatacao->data($this->data['Cliente']['data_nascto'])));
						echo $this->Form->input('sexo', array('type' => 'select', 'options' => $sexos));
						echo $this->Form->input('cpf', array('type' => 'text', 'label' => 'CPF*', 'readonly' => true));
						echo $this->Form->input('tel_residencial', array('type' => 'text', 'label' => 'Telefone residencial*'));
						echo $this->Form->input('tel_comercial_pf', array('type' => 'text', 'label' => 'Telefone comercial'));
						echo $this->Form->input('celular', array('type' => 'text', 'label' => 'Celular'));
					?>

					<!-- Campos Complementares -->
					<?php
						if (count($campos_complementares) > 0) {

							foreach ($campos_complementares as $key => $campo_complementar) {

								if ($campo_complementar['CamposComplementar']['referente'] == 'PF') {

									echo $this->Form->input('CamposComplementaresValoresSelecionado.' . $campo_complementar['CamposComplementar']['id'] . '.campos_complementar_id', array('type' => 'hidden', 'value' => $campo_complementar['CamposComplementar']['id']));
									echo $this->Form->input('CamposComplementaresValoresSelecionado.' . $campo_complementar['CamposComplementar']['id'] . '.campos_complementar_tipo', array('type' => 'hidden', 'value' => $campo_complementar['CamposComplementar']['tipo']));

									if ($campo_complementar['CamposComplementar']['tipo'] == 'T') {
										echo $this->Form->input('CamposComplementaresValoresSelecionado.' . $campo_complementar['CamposComplementar']['id'] . '.descricao', array('label' => $campo_complementar['CamposComplementar']['descricao'], 'type' => 'text', 'data-required' => $campo_complementar['CamposComplementar']['campo_obrigatorio'], 'class' => 'campo-complementar pf'));
									} else if ($campo_complementar['CamposComplementar']['tipo'] == 'TG') {
										echo $this->Form->input('CamposComplementaresValoresSelecionado.' . $campo_complementar['CamposComplementar']['id'] . '.descricao_grande', array('label' => $campo_complementar['CamposComplementar']['descricao'], 'type' => 'textarea', 'data-required' => $campo_complementar['CamposComplementar']['campo_obrigatorio'], 'class' => 'campo-complementar pf'));
									} else if ($campo_complementar['CamposComplementar']['tipo'] == 'L') {
										$lista_valores = array();
										foreach ($campo_complementar['CamposComplementaresValor'] as $campo_complementar_valor) {
											$lista_valores[$campo_complementar_valor['id']] = $campo_complementar_valor['descricao'];
										}
										echo $this->Form->input('CamposComplementaresValoresSelecionado.' . $campo_complementar['CamposComplementar']['id'] . '.campos_complementares_valor_id', array('empty' => 'Selecione', 'label' => $campo_complementar['CamposComplementar']['descricao'], 'options' => $lista_valores, 'data-required' => $campo_complementar['CamposComplementar']['campo_obrigatorio'], 'class' => 'campo-complementar pf'));
									} else {
										if ($campo_complementar['CamposComplementar']['mascara'] == 'I') {
											$mascara = 'mascara-inteiro';
										} else {
											$mascara = 'mascara-decimal';
										}
										echo $this->Form->input('CamposComplementaresValoresSelecionado.' . $campo_complementar['CamposComplementar']['id'] . '.descricao', array('class' => 'campo-complementar pf tipo-numero ' . $mascara, 'label' => $campo_complementar['CamposComplementar']['descricao'], 'data-required' => $campo_complementar['CamposComplementar']['campo_obrigatorio']));
									}
								}
							}

						}
					?>
					<!-- Fim - Campos Complementares -->

					<?php
						echo $this->Form->submit('Salvar', array('class' => 'bt-salvar'));
						echo $this->Form->end();
					?>
				</div>
			</div>
		<?php
		} else {
		?>
			<div class="dados-cadastrais">
				<h2>Alterar dados cadastrais</h2>
				<div class="form-dados-cadastrais">
					<?php
						echo $this->Form->create('Cliente', array('id' => 'ClienteDadosCadastraisForm'));
						echo $this->Form->input('id', array('type' => 'hidden'));
						echo $this->Form->input('tipo_pessoa', array('type' => 'hidden'));
						echo $this->Form->input('razao_social', array('label' => 'Razão social*'));
						echo $this->Form->input('nome_fantasia', array('label' => 'Nome fantasia*'));
						echo $this->Form->input('email', array('label' => 'Email*'));
						echo $this->Form->input('cnpj', array('type' => 'text', 'label' => 'CNPJ*', 'readonly' => true));
						echo $this->Form->input('inscricao_estadual', array('type' => 'text', 'label' => 'Inscrição estadual*'));
						echo $this->Form->input('tel_comercial_pj', array('type' => 'text', 'label' => 'Telefone comercial*'));
						echo $this->Form->submit('Salvar', array('class' => 'bt-salvar'));
						echo $this->Form->end();
					?>
				</div>
			</div>
		<?php
		}
		?>

		<div class="senha-container">
			<h2>Alterar senha</h2>
			<div class="form-senha">
				<?php
					echo $this->Form->create('Cliente', array('id' => 'ClienteAlterarSenhaForm'));
					echo $this->Form->input('id', array('type' => 'hidden'));
					echo $this->Form->input('senha_atual', array('type' => 'password', 'label' => 'Senha atual*'));
					echo $this->Form->input('senha', array('label' => 'Nova senha*', 'type' => 'password'));
					echo $this->Form->input('senha_confirmacao', array('label' => 'Repita a nova senha*', 'type' => 'password'));
				?>

				<!-- Campos Complementares -->
				<?php
					if (count($campos_complementares) > 0) {

						foreach ($campos_complementares as $key => $campo_complementar) {

							if ($campo_complementar['CamposComplementar']['referente'] == 'PJ') {

								echo $this->Form->input('CamposComplementaresValoresSelecionado.' . $campo_complementar['CamposComplementar']['id'] . '.campos_complementar_id', array('type' => 'hidden', 'value' => $campo_complementar['CamposComplementar']['id']));
								echo $this->Form->input('CamposComplementaresValoresSelecionado.' . $campo_complementar['CamposComplementar']['id'] . '.campos_complementar_tipo', array('type' => 'hidden', 'value' => $campo_complementar['CamposComplementar']['tipo']));

								if ($campo_complementar['CamposComplementar']['tipo'] == 'T') {
									echo $this->Form->input('CamposComplementaresValoresSelecionado.' . $campo_complementar['CamposComplementar']['id'] . '.descricao', array('label' => $campo_complementar['CamposComplementar']['descricao'], 'type' => 'text', 'data-required' => $campo_complementar['CamposComplementar']['campo_obrigatorio'], 'class' => 'campo-complementar pf'));
								} else if ($campo_complementar['CamposComplementar']['tipo'] == 'TG') {
									echo $this->Form->input('CamposComplementaresValoresSelecionado.' . $campo_complementar['CamposComplementar']['id'] . '.descricao_grande', array('label' => $campo_complementar['CamposComplementar']['descricao'], 'type' => 'textarea', 'data-required' => $campo_complementar['CamposComplementar']['campo_obrigatorio'], 'class' => 'campo-complementar pf'));
								} else if ($campo_complementar['CamposComplementar']['tipo'] == 'L') {
									$lista_valores = array();
									foreach ($campo_complementar['CamposComplementaresValor'] as $campo_complementar_valor) {
										$lista_valores[$campo_complementar_valor['id']] = $campo_complementar_valor['descricao'];
									}
									echo $this->Form->input('CamposComplementaresValoresSelecionado.' . $campo_complementar['CamposComplementar']['id'] . '.campos_complementares_valor_id', array('empty' => 'Selecione', 'label' => $campo_complementar['CamposComplementar']['descricao'], 'options' => $lista_valores, 'data-required' => $campo_complementar['CamposComplementar']['campo_obrigatorio'], 'class' => 'campo-complementar pf'));
								} else {
									if ($campo_complementar['CamposComplementar']['mascara'] == 'I') {
										$mascara = 'mascara-inteiro';
									} else {
										$mascara = 'mascara-decimal';
									}
									echo $this->Form->input('CamposComplementaresValoresSelecionado.' . $campo_complementar['CamposComplementar']['id'] . '.descricao', array('class' => 'campo-complementar pf tipo-numero ' . $mascara, 'label' => $campo_complementar['CamposComplementar']['descricao'], 'data-required' => $campo_complementar['CamposComplementar']['campo_obrigatorio']));
								}
							}
						}

					}
				?>
				<!-- Fim - Campos Complementares -->

				<?php
					echo $this->Form->submit('Salvar', array('class' => 'bt-salvar'));
					echo $this->Form->end();
				?>
			</div>
		</div>

	</div>
</div>