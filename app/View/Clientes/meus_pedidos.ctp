<?php
	$this->Html->css(array('Views/Clientes/meus-pedidos'), 'stylesheet', array('inline' => false));
	$this->Html->script(array('Views/Clientes/meus-pedidos'),	array('inline' => false));
?>

<div id="painel-container" class="meus-pedidos">
	<h1>Meus Pedidos</h1>

	<?php echo $this->element('Cadastro/menu_painel_controle') ?>

	<div class="painel-content">
		<table>
			<thead>
				<tr>
					<th>Pedido</th>
					<th>Data do Pedido</th>
					<th>Forma de Pagamento</th>
					<th>Valor Total</th>
					<th>Status</th>
					<th>Detalhes do Pedido</th>
				</tr>
			</thead>
			<tbody>
				<?php
					foreach ($pedidos as $pedido) {
				?>
					<tr>
						<td><?php echo $pedido['Pedido']['id'] ?></td>
						<td><?php echo $this->Formatacao->dataHora($pedido['Pedido']['data_hora']) ?></td>
						<td><?php echo $pedido['Pedido']['forma_pagamento']; ?></td>
						<td><?php echo $this->Formatacao->moeda($pedido['Pedido']['valor_total']); ?></td>
						<td><?php echo $pedido['StatusPedido']['descricao']; ?></td>
						<td>
							<?php echo $this->Html->link('Visualizar', array('controller' => 'clientes', 'action' => 'pedido', $pedido['Pedido']['id'])) ?>
							<?php
								if ($pedido['StatusPedido']['status_inicial']) {
									echo $this->Html->link('Pagar', array('controller' => 'checkout', 'action' => 'pagamento', $pedido['Pedido']['id']));
								}
							?>
						</td>
					</tr>
				<?php
					}
				?>
			</tbody>
		</table>
		<?php echo $this->element('Paginacao/paginacao'); ?>
	</div>
</div>