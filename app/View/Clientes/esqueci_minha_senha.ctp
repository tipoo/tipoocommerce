<?php
	$this->Html->css(array('Views/Clientes/esqueci-minha-senha'), 'stylesheet', array('inline' => false));
	$this->Html->script(array('Views/Clientes/esqueci-minha-senha'),	array('inline' => false));
?>
<div id="cadastro-container" class="esqueci-minha-senha">
	<h1>Esqueci minha senha</h1>

	<div class="enviar-senha">
		<p>
			Informe o endereço e-mail que você utilizou para se cadastrar e clique no botão
			<strong>Gerar nova senha</strong> para receber uma mensagem com uma nova senha.
		</p>
	<?php
		echo $this->Form->create('Esqueci', array('url' => array('controller' => 'clientes', 'action' => 'esqueci_minha_senha', 'return' => $this->request->params['controller'])));
		echo $this->Form->input('email', array('label' => 'E-mail'));
	?>
		<tipoo:mensagem chave="esqueci_minha_senha" />
	<?php
		echo $this->Form->end('Gerar nova senha');
	?>
	</div>

</div>