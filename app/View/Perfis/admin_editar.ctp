<?php
	$this->Html->script(array('Views/Perfis/admin_editar'), array('inline' => false));
?>

<ul class="breadcrumb">
	<li ><?php echo $this->Html->link('Perfis', array('action' => 'backToPaginatorIndex', 'admin' => true)); ?></li>
	<li class="active">Editar</li>
</ul>

<?php
	echo $this->Form->create('Perfil');
	echo $this->Form->input('id');
	echo $this->Form->input('descricao', array('row' => true, 'div' => 'col-md-4', 'label' => 'Descrição*'));
	echo $this->Form->actions();
	echo $this->Form->end();
?>
