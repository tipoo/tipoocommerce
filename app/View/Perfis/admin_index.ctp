<?php
$this->Html->script(array('Views/Perfis/admin_index'), array('inline' => false));
?>

<ol class="breadcrumb">
	<li class="active">Perfis</li>
</ol>


<div class="table-responsive">
	<table class="table table-striped table-hover">
		<thead>
			<tr>
				<th class="col-md-1 text-center">#</th>
				<th class="col-md-10">Descrição</th>
				<th class="text-center"><?php echo $this->Html->link('<span class="glyphicon glyphicon-plus"></span>', array('action' => 'adicionar'), array('class' => 'btn btn-sm btn-primary', 'escape' => false,'data-placement' => 'left', 'rel' => 'tooltip', 'title' => 'Adicionar Novo')); ?></th>
			</tr>
		</thead>

		<tbody>
			<?php
				foreach ($perfis as $perfil) {
			?>
				<tr>
					<td class="text-center"><?php echo $perfil['Perfil']['id'] ?></td>
					<td><?php echo $perfil['Perfil']['descricao'] ?></td>
					<td class="text-center">
						<div class="btn-group">
							<button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu pull-right text-left" role="menu">
								<li><?php echo $this->Html->link('<span class="glyphicon glyphicon-edit"></span> Editar', array('action' => 'editar', $perfil['Perfil']['id']), array('escape' => false)); ?></li>
								<li><?php echo $this->Form->postLink('<span class="glyphicon glyphicon-trash"></span> Excluir', array('action' => 'excluir', $perfil['Perfil']['id']), array('escape' => false), 'Deseja realmente excluir o Perfil #' . $perfil['Perfil']['id'] . '?'); ?></li>
							</ul>
						</div>
					</td>
				</tr>
			<?php
				}
			?>
		</tbody>
	</table>
</div>

<?php echo $this->element('Paginacao/admin_paginacao'); ?>