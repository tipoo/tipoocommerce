<?php
	$this->Html->script(array('jquery.numeric', 'Views/Estoque/admin_movimentacao'), array('inline' => false));
?>

<ol class="breadcrumb">
	<li><?php echo $this->Html->link('Categorias', array('controller' => 'categorias', 'action' => 'backToPaginatorIndex', 'admin' => true)); ?></li>
	<li><?php echo $this->Html->link('Produtos', array('controller' => 'produtos', 'action' => 'backToPaginatorIndex', 'admin' => true)); ?></li>
	<li><?php echo $this->Html->link('Sku', array('controller' => 'skus', 'action' => 'backToPaginatorIndex', 'admin' => true)); ?></li>
	<li class="active">Movimentação</li>
</ol>

<div class="alert alert-info">
	<i class="glyphicon glyphicon-info-sign"></i> <strong>Sobre as quantidades dos SKU's</strong>:<br><br>
	<ul>
		<li><strong>Disponíveis</strong>: são os SKU's que estão no estoque e disponíveis para novas vendas;</li>
		<li><strong>Reservados</strong>: são os SKU's que estão no estoque, mas encontram-se reservados em pedidos que ainda não foram pagos;</li>
		<li><strong>Em Pré-Venda</strong>: são os SKU's que foram vendidos antecipamente, ou seja, não estão no estoque ainda, mas já foram comercializados e precisam ser entregues quando derem entrada no estoque;</li>
	</ul>
</div>

<div class="row">
	<div class="col-md-6">
		<p><strong>Produto: </strong>#<?php echo $sku['Produto']['id'] . ' - ' . $sku['Produto']['descricao']; ?></p>
		<p><strong>Sku: </strong>#<?php echo $sku['Sku']['id'] . ' - ' . $sku['Sku']['descricao']; ?></p>
		<p><strong>Marca: </strong><?php echo $sku['Produto']['Marca']['descricao']; ?></p>
	</div>
	<div class="col-md-6">
		<p><strong>Disponíveis: </strong><?php echo $sku['Sku']['qtd_estoque']; ?></p>
		<p><strong>Reservados: </strong><?php echo $qtd_reservas['qtd_reservas']; ?></p>
		<p><strong>Em Pré-Venda: </strong><?php echo $qtd_pre_vendas['qtd_pre_vendas']; ?></p>
	</div>
</div>
<hr>
<div class="row">
	<div class="col-md-6">
		<?php
			echo $this->Form->create('Adicionar', array('url' => array('controller' => 'estoque', 'action' => 'adicionar', $sku['Sku']['id'])));
		?>
			<h5 class="form-titulos-admin">Adicionar</h5>
		<?php
			echo $this->Form->input('qtd', array('label' => 'Quantidade', 'type' => 'number', 'div' => 'col-lg-2', 'row' => true));
			echo $this->Form->actions();
			echo $this->Form->end();
		?>
	</div>

	<div class="col-md-6">
		<?php
			echo $this->Form->create('Remover', array('url' => array('controller' => 'estoque', 'action' => 'remover', $sku['Sku']['id'])));
		?>
			<h5 class="form-titulos-admin">Remover</h5>
		<?php
			echo $this->Form->input('qtd', array('label' => 'Quantidade', 'type' => 'number', 'div' => 'col-lg-2', 'row' => true));
			echo $this->Form->actions();
			echo $this->Form->end();
		?>
	</div>
</div>