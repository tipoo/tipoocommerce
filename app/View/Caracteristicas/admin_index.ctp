<ol class="breadcrumb">
	<li><?php echo $this->Html->link('Categorias', array('controller' => 'categorias', 'action' => 'backToPaginatorIndex', 'admin' => true)); ?></li>
	<li class="active">Características</li>
</ol>


<div class="table-responsive">
	<table class="table table-striped table-hover">
		<thead>
			<tr>
				<th class="col-md-1 text-center">#</th>
				<th class="col-md-4">Descrição</th>
				<th class="col-md-3">Tipo</th>
				<th class="col-md-3">Referente à</th>
				<th class="text-center"><?php echo $this->Html->link('<span class="glyphicon glyphicon-plus"></span>', array('action' => 'adicionar', $categoria_id), array('class' => 'btn btn-primary btn-sm','data-placement' => 'left', 'escape' => false, 'rel' => 'tooltip', 'title' => 'Adicionar Novo')); ?></th>
			</tr>
		</thead>

		<tbody>
			<?php
				foreach ($caracteristicas as $caracteristica) {
			?>
				<tr>
					<td class="text-center"><?php echo $caracteristica['Caracteristica']['id'] ?></td>
					<td><?php echo $caracteristica['Caracteristica']['descricao'] ?></td>
					<td><?php echo $tipo[$caracteristica['Caracteristica']['tipo']]; ?></td>
					<td><?php echo $referente[$caracteristica['Caracteristica']['referente']]; ?></td>
					<td class="text-center">
						<div class="btn-group">
							<button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu pull-right text-left" role="menu">
								<li><?php echo $this->Html->link('<span class="glyphicon glyphicon-edit"></span> Editar', array('action' => 'editar', $caracteristica['Caracteristica']['id']), array('escape' => false)); ?></li>
								<li><?php echo $this->Form->postLink('<span class="glyphicon glyphicon-trash"></span> Excluir', array('action' => 'excluir', $caracteristica['Caracteristica']['id']), array('escape' => false), 'Deseja realmente excluir a Caracteristica #' . $caracteristica['Caracteristica']['id'] . '?');?></li>
								<?php
									if ($caracteristica['Caracteristica']['tipo'] == 'L') {
								?>
										<li><?php echo $this->Html->link('<span class="glyphicon "></span>Valores', array('controller' => 'caracteristicasValores', 'action' => 'index', $caracteristica['Caracteristica']['id']), array('escape' => false));?></li>
								<?php
									}
								?>
							</ul>
						</div>
					</td>
				</tr>
			<?php
				}
			?>
		</tbody>
	</table>
</div>

<?php echo $this->element('Paginacao/admin_paginacao'); ?>