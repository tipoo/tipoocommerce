<?php
	$this->Html->script(array('Views/Caracteristicas/admin_editar'), array('inline' => false));
?>

<ol class="breadcrumb">
	<li><?php echo $this->Html->link('Características', array('action' => 'backToPaginatorIndex', 'admin' => false)); ?></li>
	<li class="active">Editar</li>
</ol>

<div class="alert alert-warning">
	<i class="glyphicon glyphicon-exclamation-sign"></i> <strong>Atenção!</strong> Para editar campos bloqueados, por favor entre em contato com o suporte técnico.
</div>

<?php
	echo $this->Form->create('Caracteristica');
	echo $this->Form->input('nao_editavel', array('disabled' => true, 'placeholder' => $referente[$caracteristica['Caracteristica']['referente']], 'row' => true, 'div' => 'col-md-4','type' => 'text', 'label' => 'Referente à:'));
	echo $this->Form->input('nao_editavel', array('disabled' => true, 'placeholder' => $tipo[$caracteristica['Caracteristica']['tipo']], 'row' => true, 'div' => 'col-md-4','type' => 'text', 'label' => 'Tipo :'));
?>
<!-- <div class="control-group">
	<label class="control-label"><strong>Referente à: </strong></label>
	<div class="controls">
		<p class="no-edit"><?php //echo $referente[$caracteristica['Caracteristica']['referente']] ?></p>
	</div>
</div>
<div class="control-group">
	<label class="control-label"><strong>Tipo: </strong></label>
	<div class="controls">
		<p class="no-edit"><?php //echo $tipo[$caracteristica['Caracteristica']['tipo']] ?></p>
	</div>
</div> -->

<?php
	if ($caracteristica['Caracteristica']['mascara'] != '') {

		echo $this->Form->input('nao_editavel', array('disabled' => true, 'placeholder' => $mascara[$caracteristica['Caracteristica']['mascara']], 'row' => true, 'div' => 'col-md-4','type' => 'text', 'label' => 'Máscara :'));
?>
	<!-- 	<div class="control-group">
			<label class="control-label"><strong>Máscara: </strong></label>
			<div class="controls">
				<p class="no-edit"><?php //echo $mascara[$caracteristica['Caracteristica']['mascara']] ?></p>
			</div>
		</div> -->
<?php
	}
?>

<?php
	echo $this->Form->input('descricao', array( 'row' => true, 'div' => 'col-md-4', 'label' => 'Descrição*'));
	echo $this->Form->actions();
	echo $this->Form->end();
?>
