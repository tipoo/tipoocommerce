<?php
	$this->Html->script(array('Views/Caracteristicas/admin_adicionar'), array('inline' => false));
?>

<ol class="breadcrumb">
	<li><?php echo $this->Html->link('Características', array('action' => 'backToPaginatorIndex', 'admin' => false)); ?></li>
	<li class="active">Adicionar</li>
</ol>
<?php
	echo $this->Form->create('Caracteristica');
	echo $this->Form->input('referente', array('row' => true, 'div' => 'col-md-4', 'label' => 'Referente à*', 'type' => 'select', 'options' => $referente, 'empty' => 'Selecione'));
	echo $this->Form->input('descricao', array('row' => true, 'div' => 'col-md-4', 'label' => 'Descrição*'));
	echo $this->Form->input('tipo', array('row' => true, 'div' => 'col-md-4', 'type' => 'select', 'options' => $tipo, 'empty' => 'Selecione', 'label' => 'Tipo*'));
?>
	<div class="campo-mascara">
	<?php
		echo $this->Form->input('mascara', array('row' => true, 'div' => 'col-md-4', 'type' => 'select', 'options' => $mascara, 'empty' => 'Selecione', 'label' => 'Máscara'));
	?>
	</div>
<?php
	echo $this->Form->actions();
	echo $this->Form->end();
?>