<?php
$this->Html->script(array('Views/Categorias/admin_index'), array('inline' => false));
?>

<?php
	if (isset($id)) {
?>
	<ol class="breadcrumb">
		<li ><?php echo $this->Html->link('Categorias', array('action' => 'index', 'admin' => true)); ?></li>
		<li class="active"><span>Sub-Categoria</span></li>
	</ol>

<?php
	} else {

?>
	<ol class="breadcrumb">
		<li class="active"><span>Categorias</span></li>
	</ol>

<?php

	}
 ?>

<div class="table-responsive">
	<table class="table table-striped table-hover">
		<thead>
			<tr>
				<th class="col-md-1 text-center">#</th>
				<th class="col-md-4">Categoria</th>
				<th class="col-md-4">Url</th>
				<th class="text-right">
					<?php
						if (!isset($id)) {
							echo $this->Html->link('<span class="glyphicon glyphicon-plus"></span> Características Gerais', array('controller' => 'caracteristicas', 'action' => 'index'), array('class' => 'btn btn-sm btn-primary', 'escape' => false, 'data-placement' => 'left', 'rel' => 'tooltip', 'title' => 'Adicionar Nova Característica'));

						}
					 ?>
					<?php echo $this->Html->link('<span class="glyphicon glyphicon-plus"></span>', array('action' => 'adicionar', $id), array('class' => 'btn btn-sm btn-primary', 'escape' => false,'data-placement' => 'bottom', 'rel' => 'tooltip', 'title' => 'Adicionar Novo')); ?>
				</th>
			</tr>
		</thead>

		<tbody>
			<?php
				foreach ($categorias as $categoria) {
			?>
				<tr>
					<td class="text-center"><?php echo $categoria['Categoria']['id'] ?></td>
					<td>
						<?php echo $categoria['Categoria']['descricao'] ?>
						<?php
							if ($this->Configuracoes->get('mercado_livre_ativo')) {
								if ($categoria['Categoria']['mercado_livre_categoria_descricao'] != '') {
									echo '<br>(MeLi) ' . $categoria['Categoria']['mercado_livre_categoria_descricao'];
								}
							}
						?>

						<br>
						<?php
							if (!$categoria['Categoria']['ativo']) {
								$status = '<span class="label label-danger">Inativo</span>';
							} else {
								$status = '<span class="label label-success">Ativo</span> ';
							}

							echo $status;
						?>
					</td>
					<td><?php echo $categoria['Slug']['url'] ?></td>
					<td class="text-right">
						<div class="btn-group">
							<button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu pull-right text-left" role="menu">
								<li>
								 <?php echo $this->Html->link('<span class="glyphicon glyphicon-edit"></span> Editar', array('action' => 'editar', $categoria['Categoria']['id']), array('escape' => false));?>
							 	</li>

								<?php
									if ($categoria['Categoria']['descendente'] == '' || $categoria['Categoria']['descendente'] == 'C') {
								?>
										<li><?php echo $this->Html->link('<span class="glyphicon "></span>Sub-Categorias', array('controller' => 'categorias', 'action' => 'index', $categoria['Categoria']['id']), array('escape' => false));?></li>
								<?php
									}

									if ($this->Permissoes->check(array('controller' => 'caracteristicas', 'action' => 'index', 'admin' => true, 'plugin' => null))) {
								?>
										 <li><?php echo $this->Html->link('<span class="glyphicon "></span>Características', array('controller' => 'caracteristicas', 'action' => 'index', $categoria['Categoria']['id']), array('escape' => false));?></li>
								<?php
									}
								?>

								<?php
									if ($this->Permissoes->check(array('controller' => 'vitrines', 'action' => 'colecoes', 'admin' => true, 'plugin' => null))) {
										if (!$categoria['Categoria']['categoria_pai_id']) {
											$categoria_id = 'departamento_id';
										} else {
											$categoria_id = 'categoria_id';
										}
								?>
										<li><?php echo $this->Html->link('<span class="glyphicon "></span>Vitrines de Coleções', array('controller' => 'vitrines', 'action' => 'colecoes', $categoria_id => $categoria['Categoria']['id']), array('escape' => false));?></li>
								<?php
								 	}
								?>

								<?php
									if ($this->Permissoes->check(array('controller' => 'vitrines', 'action' => 'htmls', 'admin' => true, 'plugin' => null))) {
										if (!$categoria['Categoria']['categoria_pai_id']) {
											$categoria_id = 'departamento_id';
										} else {
											$categoria_id = 'categoria_id';
										}
								?>
										<li><?php echo $this->Html->link('<span class="glyphicon "></span>Texto/Html', array('controller' => 'vitrines', 'action' => 'htmls', $categoria_id => $categoria['Categoria']['id']), array('escape' => false));?></li>
								<?php
								 	}
								?>

								<?php
									if ($categoria['Categoria']['descendente'] == '') {
								?>

										<?php
											if ($categoria['Categoria']['ativo']) {
										?>
												<li><?php echo $this->Form->postLink('<span class="glyphicon"></span> Desativar', array('action' => 'excluir', $categoria['Categoria']['id']), array('escape' => false), 'Deseja realmente desativar a Categoria #' . $categoria['Categoria']['id'] . '?');?></li>
										<?php
											} else {

										?>
												<li><?php echo $this->Form->postLink('<span class="glyphicon"></span> Ativar', array('action' => 'ativar', $categoria['Categoria']['id']), array('escape' => false), 'Deseja realmente ativar a Categoria #' . $categoria['Categoria']['id'] . '?');?></li>
										<?php
											}
										?>
								<?php
									} else {

										if ($categoria['Categoria']['descendente'] == 'C') {


											$filha_ativa = false;

											if (isset($categoria['CategoriaFilha'][0])) {
												foreach ($categoria['CategoriaFilha'] as $categoria_filha) {
													if ($categoria_filha['ativo']) {
														$filha_ativa = true;
													}
												}
											}

											if ($categoria['Categoria']['ativo']) {
												if ($filha_ativa) {
								?>
													<li class="disabled"><a href="javascript: void(0)" data-placement="left" rel="tooltip" title="" data-original-title="Para destivar essa categoria é necessário desativar as sub-categorias."><span class="glyphicon"></span> Desativar</a></li>

								<?php
												} else {
								?>
													<li><?php echo $this->Form->postLink('<span class="glyphicon"></span> Desativar', array('action' => 'excluir', $categoria['Categoria']['id']), array('escape' => false), 'Deseja realmente desativar a Categoria #' . $categoria['Categoria']['id'] . '?');?></li>
								<?php
												}

											} else {
								?>
												<li><?php echo $this->Form->postLink('<span class="glyphicon"></span> Ativar', array('action' => 'ativar', $categoria['Categoria']['id']), array('escape' => false), 'Deseja realmente ativar a Categoria #' . $categoria['Categoria']['id'] . '?');?></li>
								<?php

											}
										} else {

											if ($categoria['Categoria']['descendente'] == 'P') {
												if ($categoria['Categoria']['ativo']) {
								?>
													<li><?php echo $this->Form->postLink('<span class="glyphicon"></span> Desativar', array('action' => 'excluir', $categoria['Categoria']['id']), array('escape' => false), 'ATENÇÃO! Ao desativar essa categoria, todos os produtos pertencentes a ela também serão desativados. Deseja realmente desativar a Categoria #' . $categoria['Categoria']['id'] . '?');?></li>
								<?php
												} else {

								?>
													<li><?php echo $this->Form->postLink('<span class="glyphicon"></span> Ativar', array('action' => 'ativar', $categoria['Categoria']['id']), array('escape' => false), 'Deseja realmente ativar a Categoria #' . $categoria['Categoria']['id'] . '?');?></li>
								<?php

												}

											}
										}

									}

								 ?>


							</ul>
						</div>
					</td>
				</tr>
			<?php
				}
			?>
		</tbody>
	</table>
</div>

<?php echo $this->element('Paginacao/admin_paginacao'); ?>