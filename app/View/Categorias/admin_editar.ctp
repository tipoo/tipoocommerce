<?php
	$this->Html->script(array('Views/Categorias/admin_editar'), array('inline' => false));
	$this->Html->css(array('Views/Categorias/admin_editar'), 'stylesheet', array('inline' => false));
?>

<ol class="breadcrumb">
	<li ><?php echo $this->Html->link('Categorias', array('action' => 'backToPaginatorIndex', 'admin' => true)); ?></li>
	<li class="active">Editar</li>
</ol>

<?php
	echo $this->Form->create('Categoria');
	echo $this->Form->input('id');
?>
<!-- 	<div class="well hide">
			<h4><i class="glyphicon glyphicon-exclamation-sign"></i> Aviso:
		       <small> ao editar a url ela perde a indexação atual, fazendo com que o Google e outras ferramentas de busca não consigam listá-la temporariamente.</small>
			</h4>
			<hr>
			<?php
				//if (isset($slug_completo[2])) {

			?>
					<div class="row">
						<div class="col-md-12">
							<div class="form-inline"><label>Url*: </label>
							  <?php //echo  Router::url($url_nao_editavel, true); ?>/
							  <input name="data[Slug][url]" value="<?php //echo $url_editavel ?>" class="form-control" type="text" id="SlugUrl">
							</div>
						</div>
					</div>
			<?php

				//} else {
			?>
					<div class="row">
						<div class="col-md-12">
							<div class="form-inline"><label>Url*: </label>
							  <?php //echo  Router::url('/', true); ?>
							  <input name="data[Slug][url]" value="<?php //echo substr($this->request->data['Slug']['url'], 1) ?>" class="form-control" type="text" id="SlugUrl">
							</div>
						</div>
					</div>

			<?php
				//}

			?>
	</div>
	<br> -->
<div class="row">
	<div class="col-md-6">
		<?php
			echo $this->Form->input('descricao', array('row' => true, 'div' => 'col-md-8','type' => 'text', 'label' => 'Descrição*'));
			// echo $this->Form->input('Slug.id', array('type' => 'hidden', 'value' => $this->request->data['Slug']['id']));
			echo $this->Form->input('menu', array('type' => 'checkbox', 'label' => 'Menu <small class="text-muted"> (Categoria aparece no menu superior e no menu lateral da loja)</small class="text-muted">'));
		?>
			<h4 class="form-titulos-admin">SEO</h4>

		<?php
			$num_de_caracteres = '<span class="pull-right" id="num_de_caracteres_categoria">Nº de caracteres: 160</span>';
			echo $this->Form->input('title', array('row' => true, 'div' => 'col-md-8', 'label' => 'Title'));
			echo $this->Form->input('meta_title', array('row' => true, 'div' => 'col-md-8', 'label' => 'Meta Title'));
			echo $this->Form->input('meta_description', array('row' => true, 'div' => 'col-md-8', 'label' => 'Meta Description','placeholder' => 'Recomendável 160 caracteres', 'after' => $num_de_caracteres));
			// echo $this->Form->input('meta_keywords', array('row' => true, 'div' => 'col-md-8', 'label' => 'Meta Keywords'));
			echo $this->Form->input('sinonimos', array('row' => true, 'div' => 'col-md-8', 'label' => 'Sinônimos', 'placeholder' => 'Palavras separadas por vírgula.'));
		?>
	</div>

	<div class="col-md-6">
		<?php
			if ($this->Configuracoes->get('mercado_livre_ativo')) {
		?>
				<div class="panel panel-default marketplace-mercado-livre">
					<div class="panel-heading">
						<label>Mercado Livre (Categorias)</label>
					</div>
					<ul class="list-group">
						<li class="list-group-item">
							<div class="alert alert-warning col-md-12">
								<i class="glyphicon glyphicon-exclamation-sign"></i> <strong>Atenção!</strong>
								As categorias do Mercado Livre devem ser cadastradas apenas nas CATEGORIA DE ÚLTIMO NÍVEL, sendo nesse caso campo OBRIGATÓRIO.
								<br>
								<strong>Importante!</strong> Após Salvar, esse campo não podera ser editado.
							</div>
							<div class="clear"></div>
						</li>
						<li class="list-group-item mercado-livre-categorias-list-group-item">
							<!-- Categorias Mercado Livre -->
							<?php
								if ($this->request->data['Categoria']['mercado_livre_categoria_id'] == '') {
							?>
									<ul class="mercado-livre-categorias">
										<?php
											foreach ($mercado_livre_categorias as $key => $mercado_livre_categoria) {
										?>
												<li>
													<a class="" href="#" data-categoria-id="<?php echo $key ?>" data-ajax="false" data-has-childrens="true">
														<?php echo $this->Html->image('admin-ajax-loader-min-blue.gif', array('class' => 'carregando', 'style' => 'display: none')); ?>
														<span class="glyphicon glyphicon-plus-sign"></span> <?php echo $mercado_livre_categoria ?>
													</a>
												</li>
										<?php
											}
											echo $this->Form->input('mercado_livre_categoria_id', array('type' => 'hidden'));
											echo $this->Form->input('mercado_livre_categoria_descricao', array('type' => 'hidden'));
											echo $this->Form->input('mercado_livre_categoria_tipo_atributo', array('type' => 'hidden'));
										?>
									</ul>
							<?php
								} else {
									echo $this->Form->input('mercado_livre_categoria_id', array('type' => 'text', 'disabled' => true, 'label' => 'Mercado Livre Categoria Id'));
									echo $this->Form->input('mercado_livre_categoria_id', array('type' => 'hidden'));
									echo $this->Form->input('mercado_livre_categoria_descricao', array('type' => 'text', 'disabled' => true, 'label' => 'Mercado Livre Categoria Descrição'));
									echo $this->Form->input('mercado_livre_categoria_descricao', array('type' => 'hidden'));
								}
							?>
							<!-- .end Categorias Mercado Livre -->
						</li>
					</ul>
				</div>
		<?php
			}
		?>
	</div>
</div>

<?php
	echo $this->Form->actions();
	echo $this->Form->end();
?>
