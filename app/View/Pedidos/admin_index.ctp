<?php
	$this->Html->script(array('bootstrap-datepicker', 'bootstrap-datepicker.pt-BR', 'xdate', 'Views/Pedidos/admin_index'), array('inline' => false));
	$this->Html->css(array('datepicker', 'Views/Pedidos/admin_index'), 'stylesheet', array('inline' => false));
?>

<div class="alert alert-danger hide" id="alert-datepicker">
	<span>Mensagem de erro: Intervalo entre datas incorreto.</span>
</div>

<ol class="breadcrumb">
	<li class="active">Pedidos</li>
</ol>

<div class="row">
	<div class="col-md-9 col-lg-10">
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="row">
					<div class="col-md-3 input-append date" id="dp1" data-date-format="dd/mm/yyyy">
						<label>De</label>
						<div class="input-group">
							<span class="input-group-btn">
								<button class="btn btn-default add-on" type="button">
									<span class="glyphicon glyphicon-calendar"></span>
								</button>
							</span>
							<input name="data[Filtro][produto]" id="dp1-mask" size="16" class="form-control" type="text" data-date="<?php echo $this->Formatacao->data($de) ?>" value="<?php echo $this->Formatacao->data($de) ?>" >
						</div>
					</div>
					<div class="col-md-3 input-append date" id="dp2" data-date-format="dd/mm/yyyy">
						<label>Até</label>
						<div class="input-group">
							<span class="input-group-btn">
								<button class="btn btn-default add-on" type="button">
									<span class="glyphicon glyphicon-calendar"></span>
								</button>
							</span>
							<input name="data[Filtro][produto]" id="dp2-mask" size="16" class="form-control" type="text" data-date="<?php echo $this->Formatacao->data($ate) ?>" value="<?php echo $this->Formatacao->data($ate) ?>" >
						</div>
					</div>
					<div class="col-md-3 input-append date" id="dp2" data-date="<?php echo $this->Formatacao->data($ate) ?>" data-date-format="dd/mm/yyyy">
						<?php echo $this->Form->input('statusPedidoFiltroId', array('options' => $statusPedidoFiltro, 'empty' => 'Todos os Status', 'label' => 'Status', 'div' => false)); ?>
					</div>
					<div class="col-md-2">
						<label for="enviarFiltros" class="control-label">&nbsp;</label>
						<button class="btn btn-primary form-control" id="enviarFiltros" type="button">Filtrar</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-3 col-lg-2 text-right">
		<div class="panel panel-default">
			<div class="panel-body">
				<label class="control-label">&nbsp;</label>
				<button class="btn btn-primary form-control btn-imprimir"><span class="glyphicon glyphicon-print"></span> Imprimir</button>
			</div>
		</div>
	</div>
</div>

<div class="table-responsive">
	<?php
		echo $this->Form->create('Pedido', array('url' => array('controller' => 'pedidos', 'action' => 'imprimir', 'admin' => true)));
	?>
	<table class="table table-striped table-hover imprimir table-pedidos">
		<thead>
			<tr>
				<th class="text-center"></th>
				<th class="text-center">#</th>
				<th>Cliente</th>
				<th class="hidden-xs">Cidade</th>
				<th class="hidden-xs">Data</th>
				<th>Status</th>
				<th>Pagamento</th>
				<th class="hidden-xs">Subtotal</th>
				<th></th>
			</tr>
		</thead>

		<tbody>
			<?php
				foreach ($pedidos as $key => $pedido) {
					if ($pedido['Pedido']['estoque_disponivel'] && !$pedido['StatusPedido']['status_finalizado']) {
						$disponivel = 'success';
					} else {
						$disponivel = '';
					}
			?>
					<tr class="<?php echo $disponivel; ?>">
						<td class="text-center">
							<input type='checkbox' name='data[Pedido][<?php echo $key; ?>]' id='Pedido<?php echo $key; ?>check' value='<?php echo $pedido['Pedido']['id']; ?>'>
						</td>
						<td class="text-center"><?php echo $pedido['Pedido']['id'] ?></td>
						<td>
							<?php
								if ($pedido['Cliente']['tipo_pessoa'] == 'F') {
									echo $pedido['Cliente']['nome'];
								} else {
									echo $pedido['Cliente']['nome_fantasia'];
								}
							?>
							<p class="visible-xs"><?php echo $pedido['Endereco']['Cidade']['nome'] . '/' . $pedido['Endereco']['Cidade']['uf']; ?></p>
						</td>
						<td class="hidden-xs"><?php echo $pedido['Endereco']['Cidade']['nome'] . '/' . $pedido['Endereco']['Cidade']['uf']; ?></td>
						<td class="hidden-xs"><?php echo $this->Formatacao->dataHora($pedido['Pedido']['data_hora']) ?></td>
						<td id="status_<?php echo $pedido['Pedido']['id'] ?>">
							<?php
								$status_cor = '#333';
								if (!empty($pedido['StatusPedido']['status_cor'])) {
									$status_cor = $pedido['StatusPedido']['status_cor'];
								}
							?>
							<input type="hidden" class="pedido-status-id" value="<?php echo $pedido['StatusPedido']['id']; ?>" />
							<span class="label" style="background: <?php echo $status_cor; ?>"><?php echo $pedido['StatusPedido']['descricao']; ?></span><br />

							<?php
								if (!$pedido['StatusPedido']['status_finalizado']) {
							?>

									<a class="mudar-status" href="#" rel="<?php echo $pedido['Pedido']['id']; ?>">Mudar Status</a>
							<?php
								} else {
							?>
									<br />
							<?php
								}
							?>
						</td>
						<td class="text-center">
							<?php
								switch($pedido['Pedido']['forma_pagamento']){

									case 'PagSeguro':
							?>
										<img src="<?php echo $this->webroot ?>img/pagseguro.png" class="img-responsive" />

							<?php
										break;

									case 'PagSeguroTransparente':
							?>
										<img src="<?php echo $this->webroot ?>img/pagseguro.png" class="img-responsive" />

							<?php
										break;


									case 'Cielo':
										$dados_cielo = unserialize ($pedido['Pedido']['retorno_cielo']);
										$bandeira = $dados_cielo['transacao']['forma-pagamento']['bandeira'];

							?>
										<img src="<?php echo $this->webroot ?>img/cielo/<?php echo $bandeira ?>.png" class="img-responsive" />
							<?php

										break;

									case 'Moip':
							?>
										<img src="<?php echo $this->webroot ?>img/moip.png" class="img-responsive" />

							<?php
										break;

								}

							?>
						</td>
						<td class="hidden-xs">
						<?php
							$sub_total = 0;
							if ($pedido['Pedido']['valor_desconto'] == '') {
								$sub_total = $pedido['Pedido']['valor_produtos'] + $pedido['Pedido']['valor_servicos'];
							} else {
								$sub_total = $pedido['Pedido']['valor_produtos'] + $pedido['Pedido']['valor_servicos'] - $pedido['Pedido']['valor_desconto'];
							}
						 	echo $this->Formatacao->moeda($sub_total);
						?>
						</td>
						<td class="text-center">
							<div class="btn-group">
								<button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu pull-right text-left" role="menu">
									<li>
										<a  href="<?php echo $this->webroot?>admin/pedidos/ajax_visualizar/<?php echo $pedido['Pedido']['id'] ?>" class="generic-modal" target="_blank">Visualizar</a>
									</li>
								</ul>
							</div>
						</td>
					</tr>
			<?php
				}
			?>
		</tbody>
	</table>
	<?php echo $this->Form->end(); ?>
</div>

<?php echo $this->element('Paginacao/admin_paginacao'); ?>