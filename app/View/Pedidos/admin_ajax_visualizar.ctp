<h3 class="lead">Detalhes do Pedido</h3>
<?php
	$status_cor = '#333';
	if (!empty($pedido['StatusPedido']['status_cor'])) {
		$status_cor = $pedido['StatusPedido']['status_cor'];
	}
 ?>

<p><strong>Nº Pedido: </strong><span class="lead">#<?php echo $pedido['Pedido']['id'] ?></span></p>
<p>	<span class="label" style="background: <?php echo $status_cor; ?>"><?php echo $pedido['StatusPedido']['descricao']; ?></span></p>
<p><strong>Data do Pedido: </strong><?php echo $this->Formatacao->dataHora($pedido['Pedido']['data_hora']) ?></p>
<p><strong>Prazo: </strong><?php echo $pedido['Pedido']['prazo_frete'] ?> dia(s) útil(eis)</p>
<p><strong>Transportadora: </strong><?php echo $pedido['FreteTransportadora']['descricao'] ?></p>

<?php
	$dados_pagamento = '';
	switch($pedido['Pedido']['forma_pagamento']) {

		case 'PagSeguro':

			$dados_pag_seguro = unserialize($pedido['Pedido']['retorno_pagseguro']);
			$dados_pagamento = $dados_pag_seguro[0]['dados_pagamento']['metodo'];

			$img_pag = 'pagseguro.png';

			break;

		case 'PagSeguroTransparente':

			$dados_pag_seguro = json_decode($pedido['Pedido']['retorno_pagseguro']);
			$metodo_pagamento_tipo = '';
			$metodo_pagamento_codigo = '';
			$metodo_pagamento_parcelas = '';

			if ($dados_pag_seguro != '' && !isset($dados_pag_seguro->error)) {
				$metodo_pagamento_tipo = $dados_pag_seguro->paymentMethod->type;
				$metodo_pagamento_codigo = $dados_pag_seguro->paymentMethod->code;
				$metodo_pagamento_parcelas = $dados_pag_seguro->installmentCount;
			} else if ($dados_pag_seguro != '' && isset($dados_pag_seguro->error)) {
				$metodo_pagamento_tipo = 'error';
			}


			switch ($metodo_pagamento_tipo) {
				case 'error':
					$dados_pagamento = 'Ocorreu um erro ao enviar os dados ao PagSeguro, por favor informe o suporte!';
					break;
				case 1:
					$dados_pagamento = 'Cartão de Crédito ' . $bandeiras_pagseguro[$metodo_pagamento_codigo] . ' - ' . $metodo_pagamento_parcelas . 'x';
					break;
				case 2:
					$dados_pagamento = 'Boleto Bancário';
					break;

				case 3:
					$dados_pagamento = 'Débito Bancário ' . $bandeiras_pagseguro[$metodo_pagamento_codigo];
					break;

				default:
					$dados_pagamento = 'Aguardando pagamento.';
					break;
			}

			$img_pag = 'pagseguro.png';

			break;

		case 'Cielo':

			$dados_cielo = unserialize($pedido['Pedido']['retorno_cielo']);
			$dados_pagamento = 'Cartão ' . $dados_cielo['transacao']['forma-pagamento']['bandeira'] . ' - ' . $dados_cielo['transacao']['forma-pagamento']['parcelas'] . 'X';
			$bandeira = $dados_cielo['transacao']['forma-pagamento']['bandeira'];

			$img_pag = 'cielo/' . $bandeira . '.png';


			break;

		case 'Moip':
/*			$dados_pagamento = 'Moip em construção ...';
			$dados_moip = unserialize ($pedido['Pedido']['transacao_moip']);
			$this->log($dados_moip, LOG_DEBUG);*/

			$img_pag = 'moip.png';

			break;

		default:

			$dados_pagamento = 'Aguardando pagamento.';

			break;

	}

?>

<p><strong>Tipo de pagamento: </strong><?php echo $dados_pagamento ?></p>
<?php if (isset($img_pag)) { ?>
		<p><img src="<?php echo $this->webroot ?>img/<?php echo $img_pag ?>" /></p>
<?php } ?>


<?php
	if (isset($pedido['Pedido']['codigo_rastreamento'])) {
?>
		<p><strong>Cod. de rastreamento: </strong><?php echo $pedido['Pedido']['codigo_rastreamento']?></p>
<?php
	}
?>

<hr>

<div class="table-responsive">
	<table class="table table-striped table-hover">
		<thead>
		<tr>
			<th>Produto</th>
			<th>Sku</th>
			<th>Qtd</th>
			<th>Valor Unitário</th>
			<th>Estoque</th>
			<th>Subtotal</th>
		</tr>
		</thead>

		<tbody>
	<?php
		$total = 0;
		foreach ($pedido['PedidoSku'] as $sku) {
	?>
			<tr>
				<td>
					<?php echo $sku['descricao_produto']; ?>
					<br />
					<?php echo $sku['descricao_marca']; ?>
					<br />
					<br />
					<?php
						foreach ($sku['PedidoSkuServico'] as $servico) {
					?>
							<small class="text-muted">
								<?php echo $servico['servico']; ?>
								<?php echo $servico['anexo'] == '' ? '' : ': ' . $servico['valor_anexo'] ?>
								(<?php echo $this->Formatacao->moeda($servico['valor']); ?>)
							</small>
							<br>
					<?php
						}

						if (isset($sku['CompreJuntoPromocao'][0])) {
					?>
							<small class="text-muted">Desconto: "Promoção Compre Junto"</small>
							<br>

					<?php
						}

						if (isset($sku['Promocao'][0])) {

							foreach ($sku['Promocao']as $promocao) {

						?>
								<small class="text-muted">Desconto: "Promoção <?php echo $promocao['descricao']?>"</small>
								<br>
						<?php
							}
						}
					?>

				</td>
				<td>
					(<?php echo $sku['sku']; ?>)
					<?php echo $sku['descricao_sku']; ?>
				</td>
				<td><?php echo $sku['qtd']; ?></td>
				<td>
				<?php
					if ($sku['preco_unitario_desconto'] == '') {


						if ($sku['Sku']['Produto']['preco_de'] != '') {
				?>
							<small class="preco-sem-desconto"><?php echo $this->Formatacao->moeda($sku['Sku']['Produto']['preco_de'])?></small>
							<br>
				<?php
						}

						echo $this->Formatacao->moeda($sku['preco_unitario']);
					} else {

						if ($sku['preco_unitario'] == '') {
				?>
							<small class="preco-sem-desconto"><?php echo $this->Formatacao->moeda($sku['Sku']['Produto']['preco_por']); ?></small>
				<?php
						} else {
				?>
							<small class="preco-sem-desconto"><?php echo $this->Formatacao->moeda($sku['preco_unitario']); ?></small>
				<?php
						}

				?>
						<br>
				<?php
						echo $this->Formatacao->moeda($sku['preco_unitario_desconto']);
					}
				 ?>
				</td>
				<td><?php echo $sku['situacao_estoque']; ?></td>
				<td><?php echo $this->Formatacao->moeda($sku['preco_total']);?></td>
			</tr>
	<?php
			$total += $sku['preco_total'];
		}
	?>

		<?php
			$tem_desconto_pedido = false;
			if (isset($pedido['Promocao'])) {

				foreach ($pedido['Promocao'] as $promocao) {

					if ($promocao['element'] == 'desconto_pedido') {
						$tem_desconto_pedido = true;

						$desconto_pedido_descricao = '<small class="text-muted"> Promoção no Pedido : ' . $promocao['descricao'] . '</small>';

						if ($promocao['PromocaoEfeito']['tipo_valor'] == 'P') {
							$desconto_pedido_valor = $pedido['Pedido']['valor_produtos'] * $promocao['PromocaoEfeito']['valor'] / 100;
						} else {
							$desconto_pedido_valor = $promocao['PromocaoEfeito']['valor'];
						}

					}
				}
			}

			$tem_cupom = false;
			if (isset($pedido['Promocao'])) {

				foreach ($pedido['Promocao'] as $promocao) {

					if ($promocao['element'] == 'cupom_desconto') {
						$tem_cupom = true;

						$cupom_descricao = '<small class="text-muted"> Cupom de Desconto : ' . $promocao['descricao'] . ' (Cód. -' . $promocao['PromocaoCausa']['cupom'] . ')</small>';

						if ($promocao['PromocaoEfeito']['tipo_valor'] == 'P') {

							if ($tem_desconto_pedido) {
								$valor_parcial_pedido = $pedido['Pedido']['valor_produtos'] - $desconto_pedido_valor;
							} else {
								$valor_parcial_pedido = $pedido['Pedido']['valor_produtos'];
							}

							$cupom_valor = $valor_parcial_pedido * $promocao['PromocaoEfeito']['valor'] / 100;

						} else {
							$cupom_valor = $promocao['PromocaoEfeito']['valor'];
						}

					}
				}
			}

		?>
		<tr>
			<td>
			<?php
				if ($tem_desconto_pedido) {
					 echo $desconto_pedido_descricao;
 			?>
				<br>
			<?php
				}
				if ($tem_cupom) {
					 echo $cupom_descricao;
				}
			 ?>
			</td>
			<td></td>
			<td></td>
			<td>
			<?php
				if ($tem_desconto_pedido) {
			?>
				<small> - <?php echo $this->Formatacao->moeda($desconto_pedido_valor) ?></small>
				<br>
			<?php
				}
				if ($tem_cupom) {
			?>
				<small> - <?php echo $this->Formatacao->moeda($cupom_valor) ?></small>
			<br>
			<?php
				}
			?>
			</td>
			<th>Subtotal</th>
			<td>
			<?php
				$sub_total = 0;
				if (($pedido['Pedido']['valor_desconto'] == '' || !$tem_desconto_pedido) && !$tem_cupom) {
					$sub_total = $pedido['Pedido']['valor_produtos'];
				} else {
			?>
					<small class="preco-sem-desconto"><?php echo $this->Formatacao->moeda($pedido['Pedido']['valor_produtos']); ?></small>
					<br>
			<?php
					$sub_total = $pedido['Pedido']['valor_produtos'] - $pedido['Pedido']['valor_desconto'];
				}
			 	echo $this->Formatacao->moeda($sub_total);
			?>
			</td>
		</tr>

		<tr class="linha-branca">
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<th>Serviços</th>
			<td><?php echo $this->Formatacao->moeda($pedido['Pedido']['valor_servicos']); ?></td>
		</tr>

		<tr class="linha-branca">
			<td>
				<?php
					if (isset($pedido['Promocao'])) {

						foreach ($pedido['Promocao'] as $promocao) {

							if ($promocao['element'] == 'desconto_frete') {

				?>
								<small class="text-muted"> Promoção no Frete : " <?php echo $promocao['descricao']?>"</small>
				<?php
							}
						}

					}
				?>
			</td>
			<td></td>
			<td></td>
			<td></td>
			<th>Frete</th>
			<td><?php echo $this->Formatacao->moeda($pedido['Pedido']['valor_frete']); ?></td>
		</tr>

		<tr class="linha-branca">
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<th>Total</th>
			<td><?php echo $this->Formatacao->moeda($pedido['Pedido']['valor_total']); ?></td>
		</tr>
		</tbody>
	</table>
</div>
<hr>
<div class="row">
	<div class="col-md-6">
		<div class="well pedido">
			<h3 class="lead">Dados do Cliente</h3>
			<?php
				if ($pedido['Cliente']['tipo_pessoa'] == 'F') {
			?>
					<p><strong>Cliente: </strong><?php echo $pedido['Cliente']['nome'] ?></p>
					<p><strong>Cpf: </strong><?php echo $pedido['Cliente']['cpf'] ?></p>
					<p><strong>E-mail: </strong><?php echo $pedido['Cliente']['email'] ?></p>
					<p><strong>Telefone Residencial: </strong><?php echo $pedido['Cliente']['tel_residencial'] ?></p>
					<?php
						$tel_comercial_pf = $pedido['Cliente']['tel_comercial_pf'];
						if ($pedido['Cliente']['tel_comercial_pf'] == '') {
							$tel_comercial_pf = '<small class="text-muted">Não informado</small>';
						}
					 ?>
						<?php
						$tel_celular = $pedido['Cliente']['celular'];
						if ($pedido['Cliente']['celular'] == '') {
							$tel_celular = '<small class="text-muted">Não informado</small>';
						}
					 ?>
					<p><strong>Telefone Comercial: </strong><?php  echo $tel_comercial_pf?></p>
					<p><strong>Celular: </strong><?php echo $tel_celular ?></p>
			<?php
				} else {
			?>
					<p><strong>Nome Fantasia: </strong><?php echo $pedido['Cliente']['nome_fantasia'] ?></p>
					<p><strong>Razão Social: </strong><?php echo $pedido['Cliente']['razao_social'] ?></p>
					<p><strong>CNPJ: </strong><?php echo $pedido['Cliente']['cnpj'] ?></p>
					<p><strong>Inscrição Estadual: </strong><?php echo $pedido['Cliente']['inscricao_estadual'] ?></p>
					<p><strong>Telefone Comercial: </strong><?php echo $pedido['Cliente']['tel_comercial_pj'] ?></p>
			<?php
				}
			?>
		</div>

		<!-- Campos Complementares -->
		<?php
			if (isset($pedido['Cliente']['CamposComplementaresValoresSelecionado']) && count($pedido['Cliente']['CamposComplementaresValoresSelecionado']) > 0) {
		?>
				<div class="well pedido">
					<h3 class="lead">Campos Complementares</h3>
					<?php
						foreach ($pedido['Cliente']['CamposComplementaresValoresSelecionado'] as $campo_complementar_valor_selecionado) {

							if ($campo_complementar_valor_selecionado['descricao'] != '') {
								$valor_selecionado = $campo_complementar_valor_selecionado['descricao'];
							} else if ($campo_complementar_valor_selecionado['descricao_grande'] != '') {
								$valor_selecionado = $campo_complementar_valor_selecionado['descricao_grande'];
							} else if ($campo_complementar_valor_selecionado['campos_complementares_valor_id'] != '' && count($campo_complementar_valor_selecionado['CamposComplementaresValor']) > 0) {
								$valor_selecionado = $campo_complementar_valor_selecionado['CamposComplementaresValor']['descricao'];
							} else {
								$valor_selecionado = '';
							}
					?>
							<p><strong><?php echo $campo_complementar_valor_selecionado['CamposComplementar']['descricao'] ?>: </strong><?php echo $valor_selecionado ?></p>
					<?php
						}
					?>
				</div>
		<?php
			}
		?>
		<!-- .end Campos Complementares -->
	</div>

	<div class="col-md-6">
		<div class="well pedido">
			<h3 class="lead">Endereço de Entrega</h3>

			<?php
				if ($pedido['Endereco']['complemento'] != '') {
					$complemento = ' - complemento ' . $pedido['Endereco']['complemento'];
				} else {
					$complemento = '';
				}
				echo $pedido['Endereco']['endereco'] . ', n&ordm; ' . $pedido['Endereco']['numero'] . $complemento;
			?>
				<br />
				<?php echo $pedido['Endereco']['bairro']; ?>,
				<br />
				<?php echo $pedido['Endereco']['Cidade']['nome'] . '/' . $pedido['Endereco']['Cidade']['uf']; ?>,
				<br />
				<?php echo $pedido['Endereco']['cep']; ?>
		</div>
	</div>
</div>


<!-- Mercado Livre - Feedback -->
<?php echo $this->element('Marketplaces/MercadoLivre/feedback'); ?>
<!-- .end Mercado Livre - Feedback -->

<hr>

<!-- Clear Sale Start -->
<?php if ($this->Configuracoes->get('clear_sale_ativo')) { ?>
		<h3>Anti-Fraude</h3>
		<div class="clearSale">
			<?php if (!$pedido['StatusPedido']['status_inicial'] && !$pedido['StatusPedido']['status_cielo_trans_nao_aut']) { ?>

					<?php
						if ($this->Configuracoes->get('clear_sale_ambiente_producao')) {
							$url_clear_sale = $this->Configuracoes->get('clear_sale_url_producao');
						} else {
							$url_clear_sale = $this->Configuracoes->get('clear_sale_url_sandbox');
						}
					?>
					<iframe height="80px" width="261px" scrolling="no" marginwidth="0px" marginheight="0px" src="<?php echo $url_clear_sale ?>?codigointegracao=<?php echo $this->Configuracoes->get('clear_sale_codigo') ?>&PedidoID=<?php echo $pedido['Pedido']['id'] ?>">
						 <p>Seu Browser não suporta iframes</p>
					</iframe>

			<?php } else { ?>
					Aguardando autorização
			<?php } ?>
		</div>
<?php } ?>
<!-- .end Clear Sale Start -->