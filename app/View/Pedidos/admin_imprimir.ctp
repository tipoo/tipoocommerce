<?php
	$this->Html->css(array('Views/Pedidos/admin_imprimir'), 'stylesheet', array('inline' => false));
	$this->Html->script(array('Views/Pedidos/admin_imprimir'), array('inline' => false));
?>

<div id="pedidos-imprimir">
	<?php
		foreach ($pedidos as $pedido) {
	?>
		<div>
			<p><strong>#<?php echo $pedido['Pedido']['id'] ?> </strong></p>

			<?php
				if ($pedido['Cliente']['tipo_pessoa'] == 'F') {
			?>
					<p><strong>Cliente: </strong><?php echo $pedido['Cliente']['nome'] ?></p>
					<p><strong>Cpf: </strong><?php echo $pedido['Cliente']['cpf'] ?></p>
					<br >
					<p><strong>Telefone Residencial: </strong><?php echo $pedido['Cliente']['tel_residencial'] ?></p>
					<p><strong>Telefone Comercial: </strong><?php echo $pedido['Cliente']['tel_comercial_pf'] ?></p>
					<p><strong>Celular: </strong><?php echo $pedido['Cliente']['celular'] ?></p>
			<?php
				} else {
			?>
					<p><strong>Nome Fantasia: </strong><?php echo $pedido['Cliente']['nome_fantasia'] ?></p>
					<p><strong>Razão Social: </strong><?php echo $pedido['Cliente']['razao_social'] ?></p>
					<br />
					<p><strong>CNPJ: </strong><?php echo $pedido['Cliente']['cnpj'] ?></p>
					<p><strong>Inscrição Estadual: </strong><?php echo $pedido['Cliente']['inscricao_estadual'] ?></p>
					<br >
					<p><strong>Telefone Comercial: </strong><?php echo $pedido['Cliente']['tel_comercial_pj'] ?></p>
			<?php
				}
			?>

			<br />
			<p><strong>Data do Pedido: </strong><?php echo $this->Formatacao->dataHora($pedido['Pedido']['data_hora']) ?></p>
			<br />

			<?php
				if ($pedido['Cliente']['tipo_pessoa'] == 'F') {
			?>
					<p><strong>Nome: </strong><?php echo $pedido['Cliente']['nome'] ?></p>
			<?php
				} else {
			?>
					<p><strong>Nome Fantasia: </strong><?php echo $pedido['Cliente']['nome_fantasia'] ?></p>
					<p><strong>Razão Social: </strong><?php echo $pedido['Cliente']['razao_social'] ?></p>
			<?php
				}
			?>

			<table class="table">
				<tr>
					<th>Endereço de Entrega</th>
				</tr>

				<tr>
					<td>
						<?php
							if ($pedido['Endereco']['complemento'] != '') {
								$complemento = ' - complemento ' . $pedido['Endereco']['complemento'];
							} else {
								$complemento = '';
							}
							echo $pedido['Endereco']['endereco'] . ', n&ordm; ' . $pedido['Endereco']['numero'] . $complemento;
						?>
						<br />
						<?php echo $pedido['Endereco']['bairro']; ?>,
						<br />
						<?php echo $pedido['Endereco']['Cidade']['nome'] . '/' . $pedido['Endereco']['Cidade']['uf']; ?>,
						<br />
						<?php echo $pedido['Endereco']['cep']; ?>
					</td>
				</tr>
			</table>

			<table class="table">
				<tr>
					<th>Produto</th>
					<th>Sku</th>
					<th>Qtd</th>
					<th>Valor Unitário</th>
					<th>Subtotal</th>
				</tr>
			<?php
				$total = 0;
				foreach ($pedido['PedidoSku'] as $sku) {
			?>
					<tr>
						<td>
							<?php echo $sku['descricao_produto']; ?>
							<br />
							<?php echo $sku['descricao_marca']; ?>
							<br />
							<br />
							<?php
								foreach ($sku['PedidoSkuServico'] as $servico) {
							?>
									<small class="text-muted">
										<?php echo $servico['servico']; ?>
										<?php echo $servico['anexo'] == '' ? '' : ': ' . $servico['valor_anexo'] ?>
										(<?php echo $this->Formatacao->moeda($servico['valor']); ?>)
									</small>
									<br>
							<?php
								}
								if (isset($sku['CompreJuntoPromocao'][0])) {

								?>
									<small class="text-muted">Desconto: "Promoção Compre Junto"</small>
									<br>

								<?php

								}

								if (isset($sku['Promocao'][0])) {

									foreach ($sku['Promocao']as $promocao) {

								?>
										<small class="text-muted">Desconto: "Promoção <?php echo $promocao['descricao']?>"</small>
										<br>
							<?php
									}
								}
							?>
						</td>
						<td>
							(<?php echo $sku['sku']; ?>)
							<?php echo $sku['descricao_sku']; ?>
						</td>
						<td><?php echo $sku['qtd']; ?></td>
						<td>
						<?php
							if ($sku['preco_unitario_desconto'] == '') {

								if ($sku['Sku']['Produto']['preco_de'] != '') {
						?>
									<small class="preco-sem-desconto"><?php echo $this->Formatacao->moeda($sku['Sku']['Produto']['preco_de'])?></small>
									<br>
						<?php
								}
								echo $this->Formatacao->moeda($sku['preco_unitario']);
							} else {

								if ($sku['preco_unitario'] == '') {
						?>
									<small class="preco-sem-desconto"><?php echo $this->Formatacao->moeda($sku['Sku']['Produto']['preco_por']); ?></small>
						<?php
								} else {
						?>
									<small class="preco-sem-desconto"><?php echo $this->Formatacao->moeda($sku['preco_unitario']); ?></small>
						<?php
								}

						?>
								<br>
							<?php
								echo $this->Formatacao->moeda($sku['preco_unitario_desconto']);
							}
						 ?>
						</td>
						<td><?php echo $this->Formatacao->moeda($sku['preco_total']); ?></td>
					</tr>
			<?php
					$total += $sku['preco_total'];
				}
			?>

			<?php
				$tem_desconto_pedido = false;
				if (isset($pedido['Promocao'])) {

					foreach ($pedido['Promocao'] as $promocao) {

						if ($promocao['element'] == 'desconto_pedido') {
							$tem_desconto_pedido = true;

							$desconto_pedido_descricao = '<small class="text-muted"> Promoção no Pedido : ' . $promocao['descricao'] . '</small>';

							if ($promocao['PromocaoEfeito']['tipo_valor'] == 'P') {
								$desconto_pedido_valor = $pedido['Pedido']['valor_produtos'] * $promocao['PromocaoEfeito']['valor'] / 100;
							} else {
								$desconto_pedido_valor = $promocao['PromocaoEfeito']['valor'];
							}

						}
					}
				}

				$tem_cupom = false;
				if (isset($pedido['Promocao'])) {

					foreach ($pedido['Promocao'] as $promocao) {

						if ($promocao['element'] == 'cupom_desconto') {
							$tem_cupom = true;

							$cupom_descricao = '<small class="text-muted"> Cupom de Desconto : ' . $promocao['descricao'] . ' (Cód. -' . $promocao['PromocaoCausa']['cupom'] . ')</small>';

							if ($promocao['PromocaoEfeito']['tipo_valor'] == 'P') {

								if ($tem_desconto_pedido) {
									$valor_parcial_pedido = $pedido['Pedido']['valor_produtos'] - $desconto_pedido_valor;
								} else {
									$valor_parcial_pedido = $pedido['Pedido']['valor_produtos'];
								}

								$cupom_valor = $valor_parcial_pedido * $promocao['PromocaoEfeito']['valor'] / 100;

							} else {
								$cupom_valor = $promocao['PromocaoEfeito']['valor'];
							}

						}
					}
				}


			?>
				<tr>
					<td>
					<?php
						if ($tem_desconto_pedido) {
							 echo $desconto_pedido_descricao;
		 			?>
						<br>
					<?php
						}
						if ($tem_cupom) {
							 echo $cupom_descricao;
						}
					 ?>
					</td>
					<td>
					<?php
						if ($tem_desconto_pedido) {
					?>
						<small> - <?php echo $this->Formatacao->moeda($desconto_pedido_valor) ?></small>
						<br>
					<?php
						}
						if ($tem_cupom) {
					?>
						<small> - <?php echo $this->Formatacao->moeda($cupom_valor) ?></small>
					<br>
					<?php
						}
					?>
					</td>
					<th>Subtotal</th>
					<td></td>
					<td>
					<?php
						$sub_total = 0;
						if (($pedido['Pedido']['valor_desconto'] == '' || !$tem_desconto_pedido) && !$tem_cupom) {
							$sub_total = $pedido['Pedido']['valor_produtos'];
						} else {
					?>
							<small class="preco-sem-desconto"><?php echo $this->Formatacao->moeda($pedido['Pedido']['valor_produtos']); ?></small>
							<br>
					<?php
							$sub_total = $pedido['Pedido']['valor_produtos'] - $pedido['Pedido']['valor_desconto'];
						}
					 	echo $this->Formatacao->moeda($sub_total);
					?>
					</td>
				</tr>
				<tr class="linha-branca">
					<td></td>
					<td></td>
					<th>Serviços</th>
					<td></td>
					<td><?php echo $this->Formatacao->moeda($pedido['Pedido']['valor_servicos']); ?></td>
				</tr>
				<tr class="linha-branca">
					<td>
					<?php
						if (isset($pedido['Promocao'])) {

							foreach ($pedido['Promocao'] as $promocao) {

								if ($promocao['element'] == 'desconto_frete') {

					?>
									<small class="text-muted"> Promoção no Frete : " <?php echo $promocao['descricao']?>"</small>
					<?php
								}
							}

						}
					?>
					</td>
					<td></td>
					<th>Frete</th>
					<td></td>
					<td><?php echo $this->Formatacao->moeda($pedido['Pedido']['valor_frete']); ?></td>
				</tr>
				<tr class="linha-branca">
					<td></td>
					<td></td>
					<th>Total</th>
					<td></td>
					<td><?php echo $this->Formatacao->moeda($pedido['Pedido']['valor_total']); ?></td>
				</tr>
			</table>
		</div>

		<div style="page-break-before: always"></div>
	<?php
		}
	?>
</div>

<div class="text-center voltar-pedidos">
	<?php echo $this->Html->link('Após imprimir, <br /> clique aqui para voltar.', array('action' => 'backToPaginatorIndex'), array('class' => 'btn btn-primary btn-lg', 'id' => 'btn-imprimir', 'escape' => false)); ?>
</div>
