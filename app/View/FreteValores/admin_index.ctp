<ol class="breadcrumb">
	<li><?php echo $this->Html->link('Transportadoras', array('controller' => 'freteTransportadoras','action' => 'backToPaginatorIndex', 'admin' => true)); ?></li>
	<li class="active">Valores</li>
</ol>

<div class="page-header">
	<h4><?php echo $transportadora['FreteTransportadora']['descricao']?></h4>
	<p><strong>Tipo:</strong>  <?php echo $tipos[$transportadora['FreteTransportadora']['frete_tipo']]?></p>
</div>

<div class="table-responsive">
	<table class="table table-striped table-hover">
		<thead>
			<tr>
				<th class="col-md-1 text-center">#</th>
				<th class="col-md-2">Cep inicial</th>
				<th class="col-md-2">Cep final</th>
				<th class="col-md-1">Peso inicial</th>
				<th class="col-md-1">Peso final</th>
				<th class="col-md-1">Valor</th>
				<th class="col-md-1">Prazo</th>
				<th class="col-md-2">Prazo c/ carência</th>
				<th class="text-center"><?php echo $this->Html->link('<span class="glyphicon glyphicon-plus"></span>', array('action' => 'adicionar',$transportadora['FreteTransportadora']['id']), array('class' => 'btn btn-sm btn-primary', 'escape' => false,'data-placement' => 'left', 'rel' => 'tooltip', 'title' => 'Adicionar Novo')); ?></th>
			</tr>
		</thead>

		<tbody>
			<?php

				foreach ($frete_valores as $frete_valor) {

				$cep_inicio = $frete_valor['FreteValor']['cep_inicio'];
				$cep_inicio = substr($cep_inicio, 0, 5) . '-' . substr($cep_inicio, 5, 3);
				$cep_fim = $frete_valor['FreteValor']['cep_fim'];
				$cep_fim = substr($cep_fim, 0, 5) . '-' . substr($cep_fim, 5, 3);
			?>
				<tr>
					<td class="text-center"><?php echo $frete_valor['FreteValor']['id']?></td>
					<td><?php echo $cep_inicio?></td>
					<td><?php echo $cep_fim?></td>
					<td><?php echo $frete_valor['FreteValor']['peso_inicio']?></td>
					<td><?php echo $frete_valor['FreteValor']['peso_fim']?></td>
					<td><?php echo $this->Formatacao->moeda($frete_valor['FreteValor']['valor'])?></td>
		 			<td><?php echo $frete_valor['FreteValor']['prazo']?></td>
		 			<td><?php echo $frete_valor['FreteValor']['prazo_carencia']?></td>
					<td class="text-center">
						<div class="btn-group">
							<button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu pull-right text-left" role="menu">
								<li><?php echo $this->Html->link('<span class="glyphicon glyphicon-edit"></span> Editar', array('action' => 'editar', $frete_valor['FreteValor']['id']), array('escape' => false)); ?></li>
								<li><?php echo $this->Form->postLink('<span class="glyphicon glyphicon-trash"></span> Excluir', array('action' => 'excluir', $frete_valor['FreteValor']['id']), array('escape' => false), 'Deseja realmente excluir o Valor do Frete #' . $frete_valor['FreteValor']['id'] . '?'); ?></li>
							</ul>
						</div>
					</td>
				</tr>
			<?php
				}
			?>
		</tbody>
	</table>
</div>

<?php echo $this->element('Paginacao/admin_paginacao'); ?>