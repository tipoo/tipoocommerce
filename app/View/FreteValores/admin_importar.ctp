<?php
	$this->Html->script(array('Views/FreteValores/admin_importar'),array('inline' =>false));
?>

<ol class="breadcrumb">
	<li ><?php echo $this->Html->link('Transportadora', array( 'controller' => 'freteTransportadoras','action' =>'index', 'admin' =>true));?></li>
	<li class="active">Importar CSV</li>
</ol>

<div class="alert alert-warning">
	<i class="glyphicon glyphicon-exclamation-sign"></i> ATENÇÃO !  Importe um arquivo com extensão "csv". 
	<?php
		$urlBaixarCsvPadrao = array('controller' => 'freteValores', 'action' => 'baixar_csv_padrao', 'admin' => true, 'plugin' => null);

		if ($this->Permissoes->check($urlBaixarCsvPadrao)) {
			echo $this->Html->link(' Baixe aqui ', $urlBaixarCsvPadrao, array('id' => 'baixar-csv', 'escape' => false));
		}
	?>
	o modelo padrão. Se você está usando um Mac, deverá salvar o arquivo no formato "Windows Comma Separated (.csv)".
</div>


<div class="row">
	<div class="col-md-12  text-left">
		<?php
		    echo $this->Form->create('FreteValor', array('type' => 'file') );
		    echo $this->Form->input('arquivo_csv', array('label'=>'','type'=>'file'));
			echo $this->Form->actions();
			echo $this->Form->end();
		?>
	</div>
</div>

