<?php
	$this->Html->script(array('jquery.price_format.2.0.min', 'jquery.numeric','Views/FreteValores/admin_editar'),array('inline' =>false));
?>

<ol class="breadcrumb">
	<li><?php echo $this->Html->link('Transportadoras', array('controller' => 'freteTransportadoras','action' => 'backToPaginatorIndex', 'admin' => true)); ?></li>
	<li ><?php echo $this->Html->link('Valores', array('action' =>'backToPaginatorIndex', 'admin' =>true));?></li>
	<li class="active">Adicionar</li>
</ol>

<div class="page-header">
	<h4><?php echo $transportadora['FreteTransportadora']['descricao']?></h4>
	<p><strong>Tipo:</strong>  <?php echo $tipos[$transportadora['FreteTransportadora']['frete_tipo']]?></p>
</div>

<?php
	echo $this->Form->create('FreteValor');
	echo $this->Form->input('id');
	echo $this->Form->input('cep_inicio',array('row' => true, 'div' => 'col-md-2','type' => 'text','label' => 'CEP inicial*'));
	echo $this->Form->input('cep_fim',array('row' => true, 'div' => 'col-md-2','type' => 'text','label' => 'CEP final*'));
	echo $this->Form->input('peso_inicio',array('row' => true, 'div' => 'col-md-2','type' => 'text','label' => 'Peso inicial*'));
	echo $this->Form->input('peso_fim',array('row' => true, 'div' => 'col-md-2','type' => 'text','label' => 'Peso final*'));
	echo $this->Form->input('valor',array('row' => true, 'div' => 'col-md-2','type' => 'text','label' => 'Valor*'));
	echo $this->Form->input('prazo',array('row' => true, 'div' => 'col-md-1','type' => 'text','label' => 'Prazo*'));
	echo $this->Form->actions();
	echo $this->Form->end();
?>