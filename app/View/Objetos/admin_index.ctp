<?php
$this->Html->script(array('Views/Objetos/admin_index'), array('inline' => false));
?>

<ol class="breadcrumb">
	<li class="active">Objetos</li>
</ol>

<div class="table-responsive">
	<table class="table table-striped table-hover">
		<thead>
			<tr>
				<th class="col-md-1 text-center">#</th>
				<th class="col-md-2">Plugin</th>
				<th class="col-md-4">Controller</th>
				<th class="col-md-4">Action</th>
				<th class="text-center"><?php echo $this->Html->link('<span class="glyphicon glyphicon-plus"></span>', array('action' => 'adicionar'), array('class' => 'btn btn-sm btn-primary', 'escape' => false,'data-placement' => 'left', 'rel' => 'tooltip', 'title' => 'Adicionar Novo')); ?></th>
			</tr>
		</thead>

		<tbody>
			<?php
				foreach ($objetos as $objeto) {
			?>
				<tr>
					<td class="text-center"><?php echo $objeto['Objeto']['id'] ?></td>
					<td><?php echo $objeto['Objeto']['plugin'] ?></td>
					<td><?php echo $objeto['Objeto']['controller'] ?></td>
					<td><?php echo $objeto['Objeto']['action'] ?></td>
					<td class="text-center">
						<div class="btn-group">
							<button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu pull-right text-left" role="menu">
								<li><?php echo $this->Html->link('<span class="glyphicon glyphicon-edit"></span> Editar', array('action' => 'editar', $objeto['Objeto']['id']), array('escape' => false)); ?></li>
								<li><?php echo $this->Form->postLink('<span class="glyphicon glyphicon-trash"></span> Excluir', array('action' => 'excluir', $objeto['Objeto']['id']), array('escape' => false), 'Deseja realmente excluir o Objeto #' . $objeto['Objeto']['id'] . '?'); ?></li>
							</ul>
						</div>
					</td>
				</tr>
			<?php
				}
			?>
		</tbody>
	</table>
</div>

<?php echo $this->element('Paginacao/admin_paginacao'); ?>