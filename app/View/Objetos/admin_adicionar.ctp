<?php
	$this->Html->script(array('Views/Objetos/admin_adicionar'), array('inline' => false));
?>

<ol class="breadcrumb">
	<li><?php echo $this->Html->link('Objetos', array('action' => 'backToPaginatorIndex', 'admin' => true)); ?></li>
	<li class="active">Adicionar</li>
</ol>

<?php
	echo $this->Form->create('Objeto');
	echo $this->Form->input('plugin', array('row' => true, 'div' => 'col-md-4', 'label' => 'Plugin'));
	echo $this->Form->input('controller', array('row' => true, 'div' => 'col-md-4', 'label' => 'Controller*'));
	echo $this->Form->input('action', array('row' => true, 'div' => 'col-md-4', 'label' => 'Action*'));
	echo $this->Form->actions();
	echo $this->Form->end();
?>
