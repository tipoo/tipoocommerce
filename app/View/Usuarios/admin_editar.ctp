<?php
   $this->Html->script(array('Views/Usuarios/admin_editar'),array('inline' => false));
 ?>

 <ol class="breadcrumb">
	<li ><?php echo $this->Html->link('Usuários',array('action' => 'backToPaginatorIndex', 'admin' =>true)); ?></li>
	<li class="active">Editar</li>
 </ol>

<?php
	echo $this->Form->create('Usuario');
	echo $this->Form->input('id');
	echo $this->Form->input('nome',array('row' => true, 'div' => 'col-md-4','label' => 'Nome*'));
	echo $this->Form->input('email',array('row' => true, 'div' => 'col-md-4','label' => 'E-mail*'));
	echo $this->Form->input('perfil_id',array('row' => true, 'div' => 'col-md-4','label' => 'Perfil*', 'empty' => 'Selecione'));
	echo $this->Form->actions();
	echo $this->Form->end();
   ?>
