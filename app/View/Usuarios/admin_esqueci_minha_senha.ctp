<?php
	$this->Html->css(array('Views/Usuarios/admin_esqueci_minha_senha'), 'stylesheet', array('inline' => false));
	$this->Html->script(array('Views/Usuarios/admin_esqueci_minha_senha'), array('inline' => false));
?>

<div class="text-center">
	<h1>
		<?php echo $this->Html->image('logo_tipoo_commerce.png'); ?>
	</h1>
</div>

<?php
	echo $this->Form->create('EsqueciMinhaSenha', array('class' => 'form-signin'));
?>
<h3 class="form-signin-heading text-center">Esqueci minha Senha</h3>

<?php
	echo $this->Form->input('email', array('type' => 'email', 'label' => false, 'class' => 'form-control', 'placeholder' => 'E-mail', 'autofocus'));
	echo $this->Form->submit('Gerar nova senha', array('class' => 'btn btn-lg btn-danger btn-block', 'div' => false, 'label' => false, 'type' => 'submit', 'escape' => false));
?>

<br />

<div class="row">
	<div class="col-md-12">
		<?php echo $this->Html->link('Voltar', array('action' =>'login'), array('class' => 'btn btn-default')); ?>
	</div>
</div>

<?php
	echo $this->Form->end();
?>


