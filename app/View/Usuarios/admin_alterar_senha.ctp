<?php
	$this->Html->script(array('Views/Usuarios/admin_alterar_senha'),array('inline' =>false));
?>

<ol class="breadcrumb">
	<li><?php echo $this->Html->link('Usuários', array('action' =>'backToPaginatorIndex', 'admin' =>true));?></li>
	<li class="active">Alterar Senha</li>
</ol>

<?php
	echo $this->Form->create('Usuario');
	echo $this->Form->input('senha_atual', array('row' => true, 'div' => 'col-md-4','type' => 'password', 'label' => 'Senha atual*'));
	echo $this->Form->input('senha', array('row' => true, 'div' => 'col-md-4','label' => 'Nova senha*', 'type' => 'password'));
	echo $this->Form->input('senha_confirmacao', array('row' => true, 'div' => 'col-md-4','label' => 'Repita a nova senha*', 'type' => 'password'));
	echo $this->Form->actions();
	echo $this->Form->end();
?>
