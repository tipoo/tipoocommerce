<?php
	$this->Html->css(array('Views/Usuarios/admin_login'), 'stylesheet', array('inline' => false));
	$this->Html->script(array('Views/Usuarios/admin_login'), array('inline' => false));
?>

<div class="text-center">
	<h1>
		<?php echo $this->Html->image('logo_tipoo_commerce.png'); ?>
	</h1>
</div>

<?php
	echo $this->Form->create('Usuario', array('class' => 'form-signin'));
?>

<h3 class="form-signin-heading text-center">Shop Manager</h3>

<?php
	echo $this->Form->input('email', array('type' => 'email', 'placeholder' => 'E-mail', 'autofocus', 'required', 'div' => false, 'label' => false));
	echo $this->Form->input('senha', array('type' => 'password', 'div' => false, 'placeholder' => 'Senha', 'required', 'label' => false));
?>

<div class="row">
	<div class="col-md-12">
		<?php echo $this->Html->link('Esqueci minha senha', array('action' => 'esqueci_minha_senha')); ?>
	</div>
</div>

<br />

<?php
	echo $this->Form->button('Entrar', array('class' => 'btn btn-lg btn-danger btn-block', 'div' => false, 'label' => false, 'type' => 'submit', 'escape' => false));
	echo $this->Form->end();
?>





