<?php
	$this->Html->script(array('Views/Usuarios/admin_index'),array('inline' =>false));
?>
<ol class="breadcrumb">
	<li class="active">Usuários</li>
</ol>

<div class="row">
	<div class="col-md-11 col-lg-12">
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="col-md-3">
					<?php
						echo $this->Form->create('Usuario', array('legend' => false, 'url' => array('page' => false)));
						echo $this->Form->input('perfil_id', array('empty' => 'Todos', 'label' => 'Perfil', 'div' => false));
						echo $this->Form->end();
					?>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="table-responsive">
	<table class="table table-striped table-hover">
		<thead>
			<tr>
				<th class="col-md-1 text-center">#</th>
				<th class="col-md-3">Login</th>
				<th class="col-md-4">E-mail</th>
				<th class="col-md-3">Perfil</th>
				<th class="text-center"><?php echo $this->Html->link('<span class="glyphicon glyphicon-plus"></span>', array('action' => 'adicionar'), array('class' => 'btn btn-sm btn-primary', 'escape' => false,'data-placement' => 'left', 'rel' => 'tooltip', 'title' => 'Adicionar Novo')); ?></th>
			</tr>
		</thead>

		<tbody>
			<?php
				foreach ($usuarios as $usuario) {
			?>
				<tr>
					<td class="text-center"><?php echo $usuario['Usuario']['id']?></td>
					<td><?php echo $usuario['Usuario']['nome']?></td>
		 			<td><?php echo $usuario['Usuario']['email']?></td>
		 			<td><?php echo $usuario['Perfil']['descricao']?></td>
					<td class="text-center">
						<div class="btn-group">
							<button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu pull-right text-left" role="menu">
								<li><?php echo $this->Html->link('<span class="glyphicon glyphicon-edit"></span> Editar', array('action' => 'editar', $usuario['Usuario']['id']), array('escape' => false)); ?></li>
								<li><?php echo $this->Form->postLink('<span class="glyphicon glyphicon-trash"></span> Excluir', array('action' => 'excluir', $usuario['Usuario']['id']), array('escape' => false), 'Deseja realmente excluir o Usuario #' . $usuario['Usuario']['id'] . '?'); ?></li>
							</ul>
						</div>
					</td>
				</tr>
			<?php
				}
			?>
		</tbody>
	</table>
</div>

<?php echo $this->element('Paginacao/admin_paginacao'); ?>